package hardware;

import android.content.Context;

import com.ehsm.recore.model.CDSSalesModel;


public class CDSController {

    private static final String TAG = "CDSTEST";
    private static CDSController mInstance;
    private Context mContext;

    public static CDSController getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new CDSController(context);
        }
        return mInstance;
    }

    private CDSController(Context context) {
        mContext = context;
    }



    public void DisplayImageData(Context context) {

    }

    public void DisplayNewSaleData(Context context, String Screenname, CDSSalesModel mCDSSalesModel, String displayMsg, boolean isCancelDialogShow, String mInvoiceNumber, String from){

    }

    public void DisplayPaymentProcessScreen(Context context, String mInvoiceNumber) {

    }

    public void DisplayPaymentDoneScreen(Context context, String displayMsg, boolean isPrintreceipt, String receiptemail, String mInvoiceNumber) {

    }

    public void DisplayRefundScreen(Context context, CDSSalesModel mCDSSalesModel, String displayMsg, String mInvoiceNumber) {

    }
    public void DisplayPaymentCancel(Context context, CDSSalesModel mCDSSalesModel,String displayMsg){

    }

    public void onPauseCDSScreen(Context context) {

    }

    public void onResumeCDSScreen(Context context) {

    }


}