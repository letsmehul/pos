package hardware;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.ehsm.recore.R;
import com.ehsm.recore.model.KitchenPrinterData;
import com.ehsm.recore.utils.logger.LogUtil;
import com.epson.epos2.Epos2Exception;
import com.epson.epos2.discovery.DeviceInfo;
import com.epson.epos2.discovery.Discovery;
import com.epson.epos2.discovery.DiscoveryListener;
import com.epson.epos2.discovery.FilterOption;
import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;

import java.util.ArrayList;
import java.util.HashMap;

public class ePOSPrinter implements ReceiveListener {

    private static final int DISCONNECT_INTERVAL = 500;//millseconds
    private static ePOSPrinter mInstance;
    private Context mContext;

    private FilterOption mFilterOption = null;
    private ArrayList<HashMap<String, String>> mPrinterList = new ArrayList<>();

    public static Printer  mPrinter = null;

    public ePOSPrinter(Context context) {
        mContext = context;

    }

    public static ePOSPrinter getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ePOSPrinter(context);
        }
        return mInstance;
    }


    public void getKitchenPrinterList(){

    }



    public ArrayList<HashMap<String, String>> getKitchenPrinterdata(){

        return mPrinterList;
    }



    public boolean PrintKitchenReceipt(KitchenPrinterData mKitchenPrinterData) {

        return true;

    }




    @Override
    public void onPtrReceive(final Printer printerObj, final int code, final PrinterStatusInfo status, final String printJobId) {

    }


}
