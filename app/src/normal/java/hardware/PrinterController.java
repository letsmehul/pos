package hardware;

import android.content.Context;

import com.ehsm.recore.activities.ParentActivity;
import com.ehsm.recore.model.PrinterClockOutModel;
import com.ehsm.recore.model.PrinterDayCloseModel;
import com.ehsm.recore.model.PrinterDayStartModel;
import com.ehsm.recore.model.PrinterPayInOutModel;
import com.ehsm.recore.model.PrinterRefundModel;
import com.ehsm.recore.model.PrinterSalesModel;
import com.ehsm.recore.utils.logger.LogUtil;
import com.google.gson.Gson;

public class PrinterController {

    private static final String TAG = PrinterController.class.getClass().getName();
    private static PrinterController sInstance = null;
    private Context mContext;
    private int mConnectedFlag;
    private boolean mIsPrinterConnected;


    private PrinterController(Context context) {
        mContext = context;
    }

    public static PrinterController getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new PrinterController(context);
        }
        return sInstance;
    }

    public void setPrinterStatusChangeLister(PrinterStatusChangeListener statusChangeLister) {

    }

    public boolean isPrinterConnected() {
        return mIsPrinterConnected;
    }


    public boolean connectPrinter() {

        return mIsPrinterConnected;
    }

    public void disconnectPrinter() {

    }

    public void printDayStartTicket(PrinterDayStartModel mPrinterDayStartModel) {
        LogUtil.e(TAG, "mPrinterDayStartModel  -    " + new Gson().toJson(mPrinterDayStartModel));
    }

    public void printDayCloseTicket(PrinterDayCloseModel mPrinterDayCloseModel) {
        LogUtil.e(TAG, "mPrinterDayCloseModel  -    " + new Gson().toJson(mPrinterDayCloseModel));
    }



    public void printPayInTicket(PrinterPayInOutModel mPrinterPayInOutModel, String ReceiptTitle) {
        LogUtil.e(TAG, "mPrinterPayInOutModel  -    " + new Gson().toJson(mPrinterPayInOutModel));
    }

    public void PrintClockOutReceipt(PrinterClockOutModel mPrinterClockOutModel) {
        LogUtil.e(TAG, "mPrinterClockOutModel  -    " + new Gson().toJson(mPrinterClockOutModel));
    }
    public void printNewSalesTicket(ParentActivity mActivity, PrinterSalesModel mPrinterSalesModel, String PrintType) {
        LogUtil.e(TAG, "mPrinterClockOutModel  -    " + new Gson().toJson(mPrinterSalesModel));
    }

    public void printRefundTicket(ParentActivity mActivity, PrinterRefundModel mPrinterRefundModel) {

    }
    public void printVoidOrderTicket(ParentActivity mActivity, PrinterSalesModel mPrinterSalesModel) {

    }
    public void cutPrinter() {

    }


    public interface PrinterStatusChangeListener {
        void OnPrinterStatusChangeLister(final int status);
    }

}
