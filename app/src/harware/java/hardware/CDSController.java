package hardware;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.hardware.display.DisplayManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ehsm.recore.activities.BaseActivity;
import com.ehsm.recore.adapter.NewSaleCDSProductAdapter;
import com.ehsm.recore.hardware.MediaPresentation;
import com.ehsm.recore.hardware.PlaybackService;
import com.ehsm.recore.hardware.VideoPlayService;
import com.ehsm.recore.model.CDSSalesModel;
import com.ehsm.recore.model.PrinterSalesProductsModel;
import com.ehsm.recore.utils.AppConstant;
import com.ehsm.recore.utils.CM;
import com.ehsm.recore.utils.CV;
import com.ehsm.recore.utils.logger.LogUtil;


public class CDSController {
    private String from = "";
    private static final String TAG = "CDSTEST";
    private static CDSController mInstance;
    private Context mContext;
    private BaseActivity mActivity;

    private DisplayManager mDisplayManager;
    private Display mCurrDisplay;
    private MediaPresentation mMediaPresentation;

    private boolean mIsBind = false;
    private Handler handler;
    private Runnable myRunnable;
    private SurfaceView mSurfaceView;

    private SurfaceHolder vidHolder;
    PlaybackService mPlaybackService;
    private Intent playIntent;
    private boolean videoBound = false;
    String mPromoContentUrl, mPromoVideoUrl;

    private String LAST_SCREEN = "";
    private CDSSalesModel mSavedCDSSalesModel;
    String mScreenName = "";
    String mDisplayMsg = "";
    boolean IsCancelDialogShow = false;
    String invoiceNumber = "";
    String mfrom = "";


    public static CDSController getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new CDSController(context);
        }
        return mInstance;
    }

    private CDSController(Context context) {
        mContext = context;
        initPresentation();
    }

    private void initPresentation() {
        mDisplayManager = (DisplayManager) mContext.getSystemService(Context.DISPLAY_SERVICE);
        Display[] displays = mDisplayManager.getDisplays();

        if (displays.length >= 2) {
        mCurrDisplay = displays[1];

        mMediaPresentation = new MediaPresentation(mContext, mCurrDisplay, true);
        mMediaPresentation.setOnDismissListener(new DialogInterface.OnDismissListener() {

            @Override
            public void onDismiss(DialogInterface dialog) {
                LogUtil.e(TAG, "MediaPresentation:onDismiss");
            }
        });

        mMediaPresentation.setOnCancelListener(new DialogInterface.OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                LogUtil.e(TAG, "MediaPresentation:onCancel");
            }
        });

        mMediaPresentation.getWindow().setType(WindowManager.LayoutParams.TYPE_PHONE);
        mMediaPresentation.show();

        }
    }

    public void DisplayImageData(Context context) {
        LAST_SCREEN = AppConstant.CDS_NORMAL_SCREEN;
        mActivity = (BaseActivity) context;
        if(videoBound){
            try {
                mPlaybackService.stopVideo();
                mActivity.unbindService(musicConnection);
            } catch (IllegalArgumentException e) {

            }
            mPlaybackService = null;
            videoBound = false;
        }

        if (mIsBind) {
            context.unbindService(mServiceConnection);
            mIsBind = false;
            mMediaPresentation = null;
        }

        if (mMediaPresentation == null) {
            LogUtil.e(TAG, "mMediaPresentation is null");
            initPresentation();
        }

        mMediaPresentation.getLayoutParent().setClickable(false);
        mMediaPresentation.getLayoutNewSale().setVisibility(View.GONE);
        mMediaPresentation.getImageView().setVisibility(View.VISIBLE);
        mMediaPresentation.getLayoutPaymentProgress().setVisibility(View.GONE);
        mMediaPresentation.getLayoutPaymentDone().setVisibility(View.GONE);


        ImageView imageView = mMediaPresentation.getImageView();
//        Bitmap mBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_cicis_pizza);
        imageView.setImageBitmap(mActivity.getBitmapFromLocal(CV.INSTANCE.getFULL_SCREEN_LOCAL_PATH(), 0));

        mMediaPresentation.show();
    }

    public void DisplayNewSaleData(Context context, String Screenname, CDSSalesModel mCDSSalesModel, String displayMsg, boolean isCancelDialogShow, String mInvoiceNumber, String from,boolean process_dialog) {
        LAST_SCREEN = AppConstant.CDS_NEWSALE_SCREEN;
        onStoreCDSScreenData(context, Screenname, mCDSSalesModel, displayMsg, isCancelDialogShow, mInvoiceNumber, from);

        if (process_dialog)
        {
            mMediaPresentation.getLlPaymentProgress1().setVisibility(View.VISIBLE);
            mMediaPresentation.getTvChangeDueSale().setText((mCDSSalesModel.getChange_due()!=null)?"Change Due "+mCDSSalesModel.getChange_due():"");
        }else{
            mMediaPresentation.getLlPaymentProgress1().setVisibility(View.GONE);
            mMediaPresentation.getTvChangeDueSale().setText("");
        }

        mActivity = (BaseActivity) context;
        if (mIsBind) {
            mActivity.unbindService(mServiceConnection);
            mIsBind = false;
            mMediaPresentation = null;
        }

        if (mMediaPresentation == null) {
            LogUtil.e(TAG, "mMediaPresentation is null");
            initPresentation();
        }

        mMediaPresentation.getLayoutParent().setClickable(false);

        if(isCancelDialogShow){
            mMediaPresentation.getPaymentCanceDialoglLayout().setVisibility(View.VISIBLE);

            mMediaPresentation.geTotalCartTV().setText("Total Cart  "+mCDSSalesModel.getGrandTotal());
            mMediaPresentation.getPaymentErrorMsgTV().setText(displayMsg);

        }else {
            mMediaPresentation.getPaymentCanceDialoglLayout().setVisibility(View.GONE);

            if (displayMsg.length() > 0) {
                mMediaPresentation.getLayoutError().setVisibility(View.VISIBLE);
                mMediaPresentation.getDisplayMsgTV().setText(displayMsg);
                ShowImageScreen();
            } else {
                stopRunnableHandler();
                mMediaPresentation.getLayoutError().setVisibility(View.GONE);
            }
        }

        if(mCDSSalesModel.getSalesProducts().size() > 0){
            mMediaPresentation.getLayoutError().setVisibility(View.GONE);
            mMediaPresentation.getLayoutDetails().setVisibility(View.VISIBLE);
        }else{
            mMediaPresentation.getLayoutDetails().setVisibility(View.GONE);
            mMediaPresentation.getLayoutError().setVisibility(View.VISIBLE);
        }
        if (Screenname==AppConstant.CDS_REFUND_SCREEN)
        {
            mMediaPresentation.getLabelScreenNameTV().setVisibility(View.VISIBLE);
            mMediaPresentation.getLabelScreenNameTV().setText("Refund Order");
        }else{
            mMediaPresentation.getLabelScreenNameTV().setVisibility(View.GONE);
        }

        if (mInvoiceNumber.length() > 0) {


            mMediaPresentation.getOrderIdTV().setVisibility(View.VISIBLE);
            mMediaPresentation.getOrderIdTV().setText("Invoice ID : " + mInvoiceNumber);

        } else {

            mMediaPresentation.getOrderIdTV().setVisibility(View.GONE);
        }

        mMediaPresentation.getLayoutNewSale().setVisibility(View.VISIBLE);
        mMediaPresentation.getImageView().setVisibility(View.GONE);
        mMediaPresentation.getLayoutPaymentProgress().setVisibility(View.GONE);
        mMediaPresentation.getLayoutPaymentDone().setVisibility(View.GONE);

        mMediaPresentation.getWalkinCustomerTV().setText(mCDSSalesModel.getBilledPersonName());
        mMediaPresentation.getCashierNameTV().setText(mCDSSalesModel.getCashierName());
        mMediaPresentation.getOrderDateTimeTV().setText(mCDSSalesModel.getSaleDateTime());

//        mMediaPresentation.getSquareLogoImageView().setImageBitmap(mActivity.getBitmapFromLocal(CV.INSTANCE.getSQUARE_LOGO_LOCAL_PATH(), 200));
        Glide.with(mActivity)
                .load(CM.INSTANCE.getSp(mActivity, CV.INSTANCE.getSQUARE_LOGO_PATH(), ""))
                .into(mMediaPresentation.getSquareLogoImageView());

//        Log.e("DEBUG", "---- "+CM.INSTANCE.getSp(mActivity, CV.INSTANCE.getSQUARE_LOGO_PATH(), ""));


        mPromoVideoUrl = (String) CM.INSTANCE.getSp(mActivity, CV.INSTANCE.getPROMO_VIDEO_PATH(), "");
        mPromoContentUrl = (String) CM.INSTANCE.getSp(mActivity, CV.INSTANCE.getPROMO_CONTENT_PATH(), "");
        String mPromoType = (String) CM.INSTANCE.getSp(mActivity, CV.INSTANCE.getPROMO_CONTENT_TYPE(), "");

//        Log.e("DEBUG", "mPromoContentUrl  ---- "+mPromoContentUrl);

//        Image/Video/GIF
        switch (mPromoType) {
            case "Image":
                mMediaPresentation.getSurface().setVisibility(View.GONE);
                mMediaPresentation.getPromotionImageView().setVisibility(View.VISIBLE);
                Glide.with(mActivity)
                        .load(mPromoContentUrl)
                        .into(mMediaPresentation.getPromotionImageView());
                break;
            case "Video":
                mMediaPresentation.getSurface().setVisibility(View.VISIBLE);
                mMediaPresentation.getPromotionImageView().setVisibility(View.GONE);
                mSurfaceView = mMediaPresentation.getSurface();
                PlayPromoVideo(mSurfaceView);

                break;
            case "GIF":
                mMediaPresentation.getSurface().setVisibility(View.GONE);
                mMediaPresentation.getPromotionImageView().setVisibility(View.VISIBLE);

                Glide.with(mActivity)
                        .load(mPromoContentUrl)
                        .into(mMediaPresentation.getPromotionImageView());
                break;
        }

        ViewTreeObserver viewTreeObserver = mMediaPresentation.getPromotionImageView().getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    mMediaPresentation.getPromotionImageView().getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                    viewWidth = mMediaPresentation.getPromotionImageView().getWidth();
//                    viewHeight = mMediaPresentation.getPromotionImageView().getHeight();

//                    LogUtil.e("DEBUG", "mMediaPresentation.getPromotionImageView().getWidth() ----  "+mMediaPresentation.getPromotionImageView().getWidth());
//                    LogUtil.e("DEBUG", "mMediaPresentation.getPromotionImageView().getHeight() ----  "+mMediaPresentation.getPromotionImageView().getHeight());
                }
            });
        }

        int totalqty = 0;
        if(!mCDSSalesModel.getSalesProducts().isEmpty()){
            for (PrinterSalesProductsModel mPrinterSalesProductsModel : mCDSSalesModel.getSalesProducts()) {
                totalqty += Integer.parseInt(mPrinterSalesProductsModel.getQuanity());
            }
        }


        if(!TextUtils.isEmpty(mInvoiceNumber)){
            mMediaPresentation.getOrderIdTV().setVisibility(View.VISIBLE);
            mMediaPresentation.getOrderIdTV().setText("Invoice ID : " + mInvoiceNumber);
        }else {
            mMediaPresentation.getOrderIdTV().setVisibility(View.GONE);
        }

        mMediaPresentation.getTotalQtyTV().setText(String.valueOf(totalqty));
        mMediaPresentation.getSubtotalTV().setText(mCDSSalesModel.getSubtotal());
        mMediaPresentation.getTaxTV().setText(mCDSSalesModel.getTaxApplied());
        mMediaPresentation.getDiscountTV().setText(mCDSSalesModel.getDiscount());
        Log.e("=== ### ==="+mCDSSalesModel.getDiscountRemark(),"===="+from);

        if(!TextUtils.isEmpty(mCDSSalesModel.getDiscountRemark())){
            mMediaPresentation.getDiscountRemarkTV().setText(mCDSSalesModel.getDiscountRemark());
        }else {
            mMediaPresentation.getDiscountRemarkTV().setText("");
        }

//        if (mInvoiceNumber.length() > 0) {
//            mMediaPresentation.getLayoutTotalSaving().setVisibility(View.GONE);
//        }else {
//
//            mMediaPresentation.getLayoutTotalSaving().setVisibility(View.VISIBLE);
//        }

        if(Screenname.equalsIgnoreCase(AppConstant.CDS_COMPLETE_SCREEN)){
            mMediaPresentation.getLayoutTotalSaving().setVisibility(View.VISIBLE);
        }else if(Screenname.equalsIgnoreCase(AppConstant.CDS_NEWSALE_SCREEN)){
            mMediaPresentation.getLayoutTotalSaving().setVisibility(View.VISIBLE);
        }else if(Screenname.equalsIgnoreCase(AppConstant.CDS_REFUND_SCREEN)){
            mMediaPresentation.getLayoutTotalSaving().setVisibility(View.GONE);
        }else if(Screenname.equalsIgnoreCase(AppConstant.CDS_HOLD_SCREEN)){
            mMediaPresentation.getLayoutTotalSaving().setVisibility(View.VISIBLE);
        }

        if(!TextUtils.isEmpty(mCDSSalesModel.getTotalSavings())){
            mMediaPresentation.getTotalSavings().setVisibility(View.VISIBLE);
            mMediaPresentation.getTotalSavings().setText(mCDSSalesModel.getTotalSavings());
        }else {
            mMediaPresentation.getTotalSavings().setText("");
            mMediaPresentation.getTotalSavings().setVisibility(View.GONE);
        }

        mMediaPresentation.getTotalTV().setText(mCDSSalesModel.getGrandTotal());
        mMediaPresentation.getGrandtotalTV().setText(mCDSSalesModel.getGrandTotal());

        RecyclerView rvNewSale = mMediaPresentation.getRecyclerViewNewSale();
        NewSaleCDSProductAdapter mAdapter = new NewSaleCDSProductAdapter(mCDSSalesModel.getSalesProducts(),from);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        rvNewSale.setLayoutManager(mLayoutManager);
        rvNewSale.setAdapter(mAdapter);

        mMediaPresentation.show();
    }

    private void onStoreCDSScreenData(Context context, String Screenname, CDSSalesModel mCDSSalesModel, String displayMsg, boolean isCancelDialogShow, String mInvoiceNumber, String from) {
        mSavedCDSSalesModel = mCDSSalesModel;
        mScreenName = Screenname;
        mDisplayMsg = displayMsg;
        IsCancelDialogShow = isCancelDialogShow;
        invoiceNumber = mInvoiceNumber;
        mfrom = from;
    }

    public void onPauseCDSScreen(Context context) {
        mActivity = (BaseActivity) context;
        if(videoBound){
            try {
                mPlaybackService.stopVideo();
                mActivity.unbindService(musicConnection);
            } catch (IllegalArgumentException e) {

            }
            mPlaybackService = null;
            videoBound = false;
        }

        if (mIsBind) {
            context.unbindService(mServiceConnection);
            mIsBind = false;
            mMediaPresentation = null;
        }

        if (mMediaPresentation == null) {
            LogUtil.e(TAG, "mMediaPresentation is null");
            initPresentation();
        }

        mMediaPresentation.getLayoutParent().setClickable(false);
        mMediaPresentation.getLayoutNewSale().setVisibility(View.GONE);
        mMediaPresentation.getImageView().setVisibility(View.VISIBLE);
        mMediaPresentation.getLayoutPaymentProgress().setVisibility(View.GONE);
        mMediaPresentation.getLayoutPaymentDone().setVisibility(View.GONE);


        ImageView imageView = mMediaPresentation.getImageView();
//        Bitmap mBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_cicis_pizza);
        imageView.setImageBitmap(mActivity.getBitmapFromLocal(CV.INSTANCE.getFULL_SCREEN_LOCAL_PATH(), 0));

        mMediaPresentation.show();
    }

    public void onResumeCDSScreen(Context context) {
            if(LAST_SCREEN.equals(AppConstant.CDS_NEWSALE_SCREEN)){
                DisplayNewSaleData(context, mScreenName, mSavedCDSSalesModel, mDisplayMsg, IsCancelDialogShow, invoiceNumber, mfrom,false);
            }
    }

    private void PlayPromoVideo(SurfaceView mSurfaceView){

        playIntent = new Intent(mActivity, PlaybackService.class);
        mActivity.bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
        mActivity.startService(playIntent);
        vidHolder = mSurfaceView.getHolder();
        vidHolder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                if (mPlaybackService != null && mPlaybackService.getMediaPlayer() != null) {
                    mPlaybackService.getMediaPlayer().setDisplay(vidHolder);
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

            }
        });

    }

    private ServiceConnection musicConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            PlaybackService.VideoBinder binder = (PlaybackService.VideoBinder) service;
            mPlaybackService = binder.getService();
            mPlaybackService.setUrl(mPromoVideoUrl);
            videoBound = true;
            if (vidHolder != null && mPlaybackService.getMediaPlayer() != null) {
                try {
                    mPlaybackService.getMediaPlayer().setDisplay(vidHolder);
                    mPlaybackService.playVideo();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
            }
        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
            videoBound = false;
        }
    };


    public void DisplayPaymentProcessScreen(Context context, String mInvoiceNumber) {
        mActivity  = (BaseActivity) context;
        if(videoBound){
            try {
                mPlaybackService.stopVideo();
                mActivity.unbindService(musicConnection);
            } catch (IllegalArgumentException e) {
                Log.e("DEBUG", "IllegalArgumentException  -   "+e);
            }
            mPlaybackService = null;
            videoBound = false;
        }
        if (mIsBind) {
            context.unbindService(mServiceConnection);
            mIsBind = false;
            mMediaPresentation = null;
        }

        if (mMediaPresentation == null) {
            LogUtil.e(TAG, "mMediaPresentation is null");
            initPresentation();
        }

        stopRunnableHandler();

        mMediaPresentation.getLayoutParent().setClickable(false);

        if (mInvoiceNumber.length() > 0) {
            mMediaPresentation.getProcessLabelTV().setText("Processing Refund");
        } else {
            mMediaPresentation.getProcessLabelTV().setText("Processing Payment");
        }

        mMediaPresentation.getLayoutNewSale().setVisibility(View.GONE);
        mMediaPresentation.getImageView().setVisibility(View.GONE);
        mMediaPresentation.getLayoutPaymentDone().setVisibility(View.GONE);
        mMediaPresentation.getLayoutPaymentProgress().setVisibility(View.VISIBLE);

        mActivity = (BaseActivity) context;
        mMediaPresentation.getProcessSquareImageView().setImageBitmap(mActivity.getBitmapFromLocal(CV.INSTANCE.getSQUARE_LOGO_LOCAL_PATH(), 200));

        mMediaPresentation.show();
    }

    public void DisplayPaymentDoneScreen(Context context, String displayMsg, boolean isPrintreceipt, String receiptemail, String mInvoiceNumber,String ChangeDue,String Totalpaidamount) {
        if (mIsBind) {
            context.unbindService(mServiceConnection);
            mIsBind = false;
            mMediaPresentation = null;
        }

        if (mMediaPresentation == null) {
            LogUtil.e(TAG, "mMediaPresentation is null");
            initPresentation();
        }

        mMediaPresentation.getLayoutParent().setClickable(false);

        mMediaPresentation.getLayoutNewSale().setVisibility(View.GONE);
        mMediaPresentation.getImageView().setVisibility(View.GONE);
        mMediaPresentation.getLayoutPaymentProgress().setVisibility(View.GONE);
        mMediaPresentation.getLayoutPaymentDone().setVisibility(View.VISIBLE);

        mMediaPresentation.getSuccessMsgTV().setText(displayMsg);

        if (isPrintreceipt) {
            mMediaPresentation.getReceiptPrintedTV().setVisibility(View.VISIBLE);
            mMediaPresentation.getReceiptPrinttickImage().setVisibility(View.VISIBLE);
        } else {
            mMediaPresentation.getReceiptPrintedTV().setVisibility(View.INVISIBLE);
            mMediaPresentation.getReceiptPrinttickImage().setVisibility(View.INVISIBLE);
        }

        if (receiptemail.length() > 0) {
            mMediaPresentation.getEmailTV().setVisibility(View.VISIBLE);
            mMediaPresentation.getEmailTV().setText("Receipt will be sent via email to  " + receiptemail);
        } else {
            mMediaPresentation.getEmailTV().setVisibility(View.INVISIBLE);
        }

        if (mInvoiceNumber.length() > 0) {
            mMediaPresentation.getNewSaleOrderIDTV().setVisibility(View.VISIBLE);
            mMediaPresentation.getNewSaleOrderIDTV().setText("Invoice ID : " + mInvoiceNumber);
        } else {
            mMediaPresentation.getNewSaleOrderIDTV().setVisibility(View.INVISIBLE);
        }
        mMediaPresentation.getTvChangeDue().setText("Change Due "+ChangeDue);
        mMediaPresentation.getTvTotalpaidAmount().setText("Total Paid Amount "+Totalpaidamount);
        mActivity = (BaseActivity) context;
        mMediaPresentation.getThankyouSquareImageView().setImageBitmap(mActivity.getBitmapFromLocal(CV.INSTANCE.getSQUARE_LOGO_LOCAL_PATH(), 200));

        ShowImageScreen();
        mMediaPresentation.show();
    }

    public void DisplayRefundScreen(Context context, CDSSalesModel mCDSSalesModel, String displayMsg, String mInvoiceNumber) {
        mActivity  = (BaseActivity) context;
        if(videoBound){
            mActivity.unbindService(musicConnection);
            mPlaybackService = null;
            videoBound = false;
        }


        if (mIsBind) {
            context.unbindService(mServiceConnection);
            mIsBind = false;
            mMediaPresentation = null;
        }

        if (mMediaPresentation == null) {
            LogUtil.e(TAG, "mMediaPresentation is null");
            initPresentation();
        }

        if (displayMsg.length() > 0) {
            mMediaPresentation.getDisplayMsgTV().setVisibility(View.VISIBLE);
            mMediaPresentation.getDisplayMsgTV().setText(displayMsg);
            mInvoiceNumber = "";
            mMediaPresentation.getWalkinCustomerTV().setText("Walk In Customer");
            ShowImageScreen();
        } else {
            mMediaPresentation.getDisplayMsgTV().setVisibility(View.GONE);
            mMediaPresentation.getWalkinCustomerTV().setText(mCDSSalesModel.getBilledPersonName());
            stopRunnableHandler();
        }

        mMediaPresentation.getLayoutNewSale().setVisibility(View.VISIBLE);
        mMediaPresentation.getImageView().setVisibility(View.GONE);
        mMediaPresentation.getLayoutPaymentProgress().setVisibility(View.GONE);
        mMediaPresentation.getLayoutPaymentDone().setVisibility(View.GONE);

        mMediaPresentation.getOrderIdTV().setVisibility(View.VISIBLE);
        mMediaPresentation.getOrderIdTV().setText("Invoice ID : " + mInvoiceNumber);
        mMediaPresentation.getCashierNameTV().setText(mCDSSalesModel.getCashierName());
        mMediaPresentation.getOrderDateTimeTV().setText(mCDSSalesModel.getSaleDateTime());

        int totalqty = 0;
        for (PrinterSalesProductsModel mPrinterSalesProductsModel : mCDSSalesModel.getSalesProducts()) {
            totalqty += Integer.parseInt(mPrinterSalesProductsModel.getQuanity());
        }


        mMediaPresentation.getTotalQtyTV().setText(String.valueOf(totalqty));
        mMediaPresentation.getSubtotalTV().setText(mCDSSalesModel.getSubtotal());
        mMediaPresentation.getTaxTV().setText(mCDSSalesModel.getTaxApplied());

        mMediaPresentation.getDiscountTV().setText(mCDSSalesModel.getDiscount());

        mMediaPresentation.getTotalTV().setText(mCDSSalesModel.getGrandTotal());
        mMediaPresentation.getGrandtotalTV().setText(mCDSSalesModel.getGrandTotal());

        RecyclerView rvNewSale = mMediaPresentation.getRecyclerViewNewSale();
        NewSaleCDSProductAdapter mAdapter = new NewSaleCDSProductAdapter(mCDSSalesModel.getSalesProducts(),from);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        rvNewSale.setLayoutManager(mLayoutManager);
        rvNewSale.setAdapter(mAdapter);

        mMediaPresentation.show();
    }

    private void ShowImageScreen() {
        handler = new Handler(Looper.getMainLooper());
        myRunnable = new Runnable() {
            public void run() {
                // do something
                DisplayImageData(mActivity);
            }
        };
        handler.postDelayed(myRunnable, 5000);
    }

    private void stopRunnableHandler() {
        if (handler != null) {
            handler.removeCallbacks(myRunnable);
        }
    }


    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LogUtil.e(TAG, "onServiceConnected");
            if (mMediaPresentation != null) {
                mMediaPresentation.dismiss();
                mMediaPresentation = null;
            }
            mIsBind = true;

            VideoPlayService.MyBinder binder = (VideoPlayService.MyBinder) service;
            VideoPlayService bindService = binder.getService();

            mMediaPresentation = (MediaPresentation) bindService.getPresentation();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            LogUtil.e(TAG, "onServiceDisconnected");

            mIsBind = false;
        }
    };

}
