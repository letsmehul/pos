package hardware;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.ehsm.recore.R;
import com.ehsm.recore.activities.SettingsActivity;
import com.ehsm.recore.interfaces.OnstatusChangeListener;
import com.ehsm.recore.model.KitchenPrinterData;
import com.ehsm.recore.utils.AppConstant;
import com.ehsm.recore.utils.CM;
import com.ehsm.recore.utils.logger.LogUtil;
import com.epson.epos2.Epos2CallbackCode;
import com.epson.epos2.Epos2Exception;
import com.epson.epos2.discovery.DeviceInfo;
import com.epson.epos2.discovery.Discovery;
import com.epson.epos2.discovery.DiscoveryListener;
import com.epson.epos2.discovery.FilterOption;
import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;
import com.epson.epos2.printer.StatusChangeListener;

import java.util.ArrayList;
import java.util.HashMap;

public class ePOSPrinter implements ReceiveListener {

    private static final int DISCONNECT_INTERVAL = 500;//millseconds
    private static ePOSPrinter mInstance;
    private Context mContext;
    private Activity msettingContext=null;
    private FilterOption mFilterOption = null;
    private ArrayList<HashMap<String, String>> mPrinterList = new ArrayList<>();

    public static Printer mPrinter = null;
    private OnstatusChangeListener onstatusChangeListener;

    public ePOSPrinter(Context context,OnstatusChangeListener onstatusChangeListener) {
        mContext = context;
        this.onstatusChangeListener=onstatusChangeListener;

        finalizeObject();
        initializeObject();
    }

    public static ePOSPrinter getInstance(Context context,OnstatusChangeListener onstatusChangeListener) {
        if (mInstance == null) {
            mInstance = new ePOSPrinter(context,onstatusChangeListener);
            Log.d("Data_aaa","data_aaa");
        }
        return mInstance;
    }


    public void getKitchenPrinterList() {
        mPrinterList.clear();
        mFilterOption = new FilterOption();
        mFilterOption.setDeviceType(Discovery.TYPE_PRINTER);
        mFilterOption.setEpsonFilter(Discovery.FILTER_NAME);
        try {
            Discovery.stop();
        }
        catch (Epos2Exception e) {
            e.printStackTrace();
        }
        try {
            Log.e("KITCHEN_DEBUG", "NEW Device Discovery Started Sucessfully --- ");
            Discovery.start(mContext, mFilterOption, mDiscoveryListener);
        } catch (Exception e) {
            showException(e, "start", mContext);
        }
    }

    public void setsettingcontext(Activity activity)
    {
        this.msettingContext=activity;
    }
    public void clearsettingcontext()
    {
        this.msettingContext=null;
    }

    private DiscoveryListener mDiscoveryListener = new DiscoveryListener() {
        @Override
        public void onDiscovery(final DeviceInfo deviceInfo) {

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    HashMap<String, String> item = new HashMap<String, String>();
                    item.put("PrinterName", deviceInfo.getDeviceName());
                    item.put("Target", deviceInfo.getTarget());
                    Log.e("KITCHEN_DEBUG", "NEW Device Detected --- " + deviceInfo.getDeviceName());
                    mPrinterList.add(item);
                }
            });
        }
    };

    public ArrayList<HashMap<String, String>> getKitchenPrinterdata() {

        return mPrinterList;
    }

    private boolean initializeObject() {
        try {
            mPrinter = new Printer(Printer.TM_M30, Printer.MODEL_ANK, mContext);
        } catch (Exception e) {
            showException(e, "Printer", mContext);
            return false;
        }
        mPrinterList = new ArrayList<>();
        mPrinter.setReceiveEventListener(this);
        /*mPrinter.setStatusChangeEventListener(new StatusChangeListener() {
            @Override
            public void onPtrStatusChange(Printer printer, int i) {
                onstatusChangeListener.statuschangeListener(makePrinterErrorMessage(i));
                Log.d("status_details", String.valueOf(i));

            }
        });

        try {
            SharedPreferences sharedPref = CM.INSTANCE.getEncryptedSharedPreferences(mContext);
            String SEL_KITCHEN_PRINTER_IP = sharedPref.getString(AppConstant.KITCHEN_PRINTER_IP, "");
            mPrinter.connect(SEL_KITCHEN_PRINTER_IP, Printer.PARAM_DEFAULT);
            mPrinter.startMonitor();
        } catch (Epos2Exception e) {
            Log.d("Exception_detail",e.getMessage()+"   "+String.valueOf(e.getErrorStatus()));
            e.printStackTrace();
        }*/

        return true;
    }

    private void finalizeObject() {
        if (mPrinter == null) {
            return;
        }

        mPrinter.setReceiveEventListener(null);
        /*try {
            mPrinter.setStatusChangeEventListener(null);
            mPrinter.stopMonitor();
        } catch (Epos2Exception e) {
            e.printStackTrace();
        }*/
        mPrinterList.clear();
        mPrinter = null;
    }

    public boolean PrintKitchenReceipt(KitchenPrinterData mKitchenPrinterData) {
        String method = "";

        StringBuilder textData = new StringBuilder();

        if (mPrinter == null) {
            return false;
        }

        try {

            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            textData.append("\n");
            textData.append(mKitchenPrinterData.getPOSName());
            textData.append("\n");
            method = "addTextSize";
            mPrinter.addTextSize(2, 2);
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            textData.append("\n");
            textData.append(mKitchenPrinterData.getInvoiceNo());
            textData.append("\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            textData.append("\n");
            textData.append("Billed To: " + mKitchenPrinterData.getCustomerName());
            textData.append("\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            textData.append(mKitchenPrinterData.getRollName() + " : " + mKitchenPrinterData.getBilledPersonName());
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("\n");
            textData.append("Billed Date : " + mKitchenPrinterData.getOrderDate());
            textData.append("\n");
            mPrinter.addText(textData.toString());
            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            textData.delete(0, textData.length());

            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            textData.append("-----------------------");
            textData.append("\n");
            mPrinter.addText(textData.toString());
            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            textData.delete(0, textData.length());

            for (int i = 0; i < mKitchenPrinterData.getmPrinterKitchenProductsModel().size(); i++) {
                mPrinter.addTextAlign(Printer.ALIGN_CENTER);
                method = "addTextSize";
                mPrinter.addTextSize(2, 2);
                textData.append(mKitchenPrinterData.getmPrinterKitchenProductsModel().get(i).getQuantity() + " x " + mKitchenPrinterData.getmPrinterKitchenProductsModel().get(i).getProductName());
                textData.append("\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());

                String instruction=mKitchenPrinterData.getmPrinterKitchenProductsModel().get(i).getInstruction();

                if (instruction != null && !instruction.equals("")) {
                    mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                    method = "addTextSize";
                    mPrinter.addTextSize(1, 1);
                    textData.append("\n");
                    textData.append("Notes : " +instruction);
                    textData.append("\n");
                    mPrinter.addText(textData.toString());
                    textData.delete(0, textData.length());
                }


                String ingrName = mKitchenPrinterData.getmPrinterKitchenProductsModel().get(i).getModifierIngredient();

                if (ingrName != null && !ingrName.equals("")) {
                    mPrinter.addTextAlign(Printer.ALIGN_CENTER);
                    method = "addTextSize";
                    mPrinter.addTextSize(1, 1);
                    textData.append("\n");
                    textData.append(ingrName);
                    mPrinter.addText(textData.toString());
                    textData.delete(0, textData.length());
                }


                mPrinter.addTextAlign(Printer.ALIGN_CENTER);
                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                textData.append("\n");
                textData.append("-----------------------");
                textData.append("\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            }


            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            method = "addTextSize";
            mPrinter.addTextSize(2, 2);
            textData.append("\n");
            textData.append("* KITCHEN RECEIPT *");
            textData.append("\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            if(mKitchenPrinterData.getOrderStatus().equals(AppConstant.ON_HOLD)){
                mPrinter.addTextAlign(Printer.ALIGN_CENTER);
                method = "addTextSize";
                mPrinter.addTextSize(2, 2);
                textData.append("\n");
                textData.append("* UNPAID *");
                textData.append("\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            }


            mPrinter.addFeedLine(2);

            method = "addCut";
            mPrinter.addCut(Printer.CUT_FEED);

        } catch (Exception e) {

            Log.e("TAG", "PrintKitchenReceipt: " + e.getMessage());
            mPrinter.clearCommandBuffer();
            showException(e, method, mContext);
            return false;
        }

        if (printData()) {
            return true;
        } else {
            return false;
        }

    }

    public boolean CheckKitchenReceipt(String testmessage) {
        String method = "";

        StringBuilder textData = new StringBuilder();

        if (mPrinter == null) {
            return false;
        }
        Log.d("Kitchen_printer", String.valueOf(mPrinter.getStatus().getConnection()));

        try {

            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            textData.append("\n");
            textData.append(testmessage);
            textData.append("\n");
            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addText(textData.toString());
            mPrinter.addFeedLine(2);

            method = "addCut";
            mPrinter.addCut(Printer.CUT_FEED);

        } catch (Exception e) {

            Log.e("TAG", "PrintKitchenReceipt: " + e.getMessage());
            mPrinter.clearCommandBuffer();
            showException(e, method, mContext);
            return false;
        }

        if (printData()) {
            return true;
        } else {
            return false;
        }

    }

    private boolean printData() {
        if (mPrinter == null) {
            return false;
        }

        if (!connectPrinter()) {
            mPrinter.clearCommandBuffer();
            SharedPreferences sharedPref = CM.INSTANCE.getEncryptedSharedPreferences(mContext);
            sharedPref.edit().remove(AppConstant.KITCHEN_PRINTER_IP).commit();
            mPrinterList.clear();
            //finalizeObject();
            return false;
        }

        try {
            mPrinter.sendData(Printer.PARAM_DEFAULT);
        } catch (Exception e) {
            mPrinter.clearCommandBuffer();
            showException(e, "sendData", mContext);
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
                showException(e, "printData", mContext);
            }
            return false;
        }

        return true;
    }
    public class AsyncTaskRunner extends AsyncTask<String, String, Boolean>{
        @Override
        protected void onPreExecute() {
            Log.d("Onpostexecute","Onpostexecute");
            ((SettingsActivity) msettingContext).updatebuttonstatus(false);
            ((SettingsActivity) msettingContext).showKcsDialog();

        }
        @Override
        protected Boolean doInBackground(String... params) {
            String method = "";

            StringBuilder textData = new StringBuilder();

            if (mPrinter == null) {
                return false;
            }
            Log.d("Kitchen_printer", String.valueOf(mPrinter.getStatus().getConnection()));

            try {

                method = "addTextAlign";
                mPrinter.addTextAlign(Printer.ALIGN_CENTER);
                textData.append("\n");
                textData.append(params[0]);
                textData.append("\n");
                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addText(textData.toString());
                mPrinter.addFeedLine(2);

                method = "addCut";
                mPrinter.addCut(Printer.CUT_FEED);

            } catch (Exception e) {

                Log.e("TAG", "PrintKitchenReceipt: " + e.getMessage());
                mPrinter.clearCommandBuffer();
                showException(e, method, mContext);
                return false;
            }

            if (printData()) {
                return true;
            } else {
                return false;
            }
        }
        @Override
        protected void onPostExecute(Boolean result) {
            // execution of result of Long time consuming operation
            Log.d("OnPreexecute",result.toString());
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ((SettingsActivity) msettingContext).updatebuttonstatus(true);
                    ((SettingsActivity) msettingContext).dismissKcsDialog();
                }
            },2000);
            if (!result) {
                ((SettingsActivity) msettingContext).KitchenPrinterNotFoundDialog();
            }


        }

    }

    public boolean connectPrinter() {
        if (mPrinter == null) {
            return false;
        }
        /*try {
            mPrinter.stopMonitor();
            mPrinter.disconnect();
        } catch (Epos2Exception e) {
            e.printStackTrace();
        }*/

        try {
            SharedPreferences sharedPref = CM.INSTANCE.getEncryptedSharedPreferences(mContext);
            String SEL_KITCHEN_PRINTER_IP = sharedPref.getString(AppConstant.KITCHEN_PRINTER_IP, "");
            mPrinter.connect(SEL_KITCHEN_PRINTER_IP, Printer.PARAM_DEFAULT);
            /*mPrinter.setStatusChangeEventListener(new StatusChangeListener() {
                @Override
                public void onPtrStatusChange(Printer printer, int i) {
                    onstatusChangeListener.statuschangeListener(makePrinterErrorMessage(i));
                    Log.d("status_details1", String.valueOf(i));

                }
            });
            mPrinter.startMonitor();*/
        } catch (Exception e) {
            showException(e, "connect", mContext);
            return false;
        }

        return true;
    }

    private void disconnectPrinter() {
        if (mPrinter == null) {
            return;
        }

        while (true) {
            try {
                mPrinter.disconnect();
                break;
            } catch (final Exception e) {
                if (e instanceof Epos2Exception) {
                    //Note: If printer is processing such as printing and so on, the disconnect API returns ERR_PROCESSING.
                    if (((Epos2Exception) e).getErrorStatus() == Epos2Exception.ERR_PROCESSING) {
                        try {
                            Thread.sleep(DISCONNECT_INTERVAL);
                        } catch (Exception ex) {
                        }
                    } else {
//                        runOnUiThread(new Runnable() {
//                            public synchronized void run() {
                        showException(e, "disconnect", mContext);
//                            }
//                        });
                        break;
                    }
                } else {
//                    runOnUiThread(new Runnable() {
//                        public synchronized void run() {
                    showException(e, "disconnect", mContext);
//                        }
//                    });
                    break;
                }
            }
        }

        mPrinter.clearCommandBuffer();
    }

    @Override
    public void onPtrReceive(final Printer printerObj, final int code, final PrinterStatusInfo status, final String printJobId) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public synchronized void run() {
        showResult(code, makeErrorMessage(status), mContext);
//
        dispPrinterWarnings(status);
//
//                updateButtonState(true);
//
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
        disconnectPrinter();
        mPrinter.clearCommandBuffer();
        if (msettingContext!=null && msettingContext instanceof SettingsActivity )
        {
            Log.d("EposPrinter","data_success");
            msettingContext.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((SettingsActivity) msettingContext).updatebuttonstatus(true);
                }
            });
        }
//                    }
//                }).start();
//            }
//        });
    }

    private void dispPrinterWarnings(PrinterStatusInfo status) {
        String warningsMsg = "";

        if (status == null) {
            return;
        }

        if (status.getPaper() == Printer.PAPER_NEAR_END) {
            warningsMsg += mContext.getString(R.string.handlingmsg_warn_receipt_near_end);
        }

        if (status.getBatteryLevel() == Printer.BATTERY_LEVEL_1) {
            warningsMsg += mContext.getString(R.string.handlingmsg_warn_battery_near_end);
        }

//        edtWarnings.setText(warningsMsg);
    }

    private String makePrinterErrorMessage(int status)
    {
        String msg = "";
        if (status==Printer.EVENT_COVER_OPEN)
        {
            Log.d("ePOSPrinter","Cover open");
        }else if (status==Printer.EVENT_COVER_CLOSE)
        {
            Log.d("ePOSPrinter","Cover close");
        }
        else if (status==Printer.EVENT_PAPER_EMPTY) {
            msg = mContext.getString(R.string.handlingmsg_err_receipt_end);
            Log.d("ePOSPrinter", "Paper Is Empty");
        }
        else if (status==Printer.EVENT_PAPER_NEAR_END) {
            msg = mContext.getString(R.string.handlingmsg_warn_receipt_near_end);
            Log.d("ePOSPrinter", "Paper Is Empty");
        }
        else if (status==Printer.EVENT_OFFLINE)
        {
            msg = mContext.getString(R.string.handlingmsg_err_offline);
            Log.d("ePOSPrinter","Printer Is Disconnect");
        }
        else if (status==Printer.EVENT_ONLINE)
        {
            msg = mContext.getString(R.string.handlingmsg_err_online);
            Log.d("ePOSPrinter","Printer Is Reconnect");
        }
        else if (status==Printer.EVENT_POWER_OFF)
        {
            msg = mContext.getString(R.string.handlingmsg_err_offline);
            Log.d("ePOSPrinter","Printer Power Off");
        }
        return msg;
    }

    private String makeErrorMessage(PrinterStatusInfo status) {
        String msg = "";

        if (status.getOnline() == Printer.FALSE) {
            msg += mContext.getString(R.string.handlingmsg_err_offline);
        }
        if (status.getConnection() == Printer.FALSE) {
            msg += mContext.getString(R.string.handlingmsg_err_no_response);
        }
        if (status.getCoverOpen() == Printer.TRUE) {
            msg += mContext.getString(R.string.handlingmsg_err_cover_open);
        }
        if (status.getPaper() == Printer.PAPER_EMPTY) {
            msg += mContext.getString(R.string.handlingmsg_err_receipt_end);
        }
        if (status.getPaperFeed() == Printer.TRUE || status.getPanelSwitch() == Printer.SWITCH_ON) {
            msg += mContext.getString(R.string.handlingmsg_err_paper_feed);
        }
        if (status.getErrorStatus() == Printer.MECHANICAL_ERR || status.getErrorStatus() == Printer.AUTOCUTTER_ERR) {
            msg += mContext.getString(R.string.handlingmsg_err_autocutter);
            msg += mContext.getString(R.string.handlingmsg_err_need_recover);
        }
        if (status.getErrorStatus() == Printer.UNRECOVER_ERR) {
            msg += mContext.getString(R.string.handlingmsg_err_unrecover);
        }
        if (status.getErrorStatus() == Printer.AUTORECOVER_ERR) {
            if (status.getAutoRecoverError() == Printer.HEAD_OVERHEAT) {
                msg += mContext.getString(R.string.handlingmsg_err_overheat);
                msg += mContext.getString(R.string.handlingmsg_err_head);
            }
            if (status.getAutoRecoverError() == Printer.MOTOR_OVERHEAT) {
                msg += mContext.getString(R.string.handlingmsg_err_overheat);
                msg += mContext.getString(R.string.handlingmsg_err_motor);
            }
            if (status.getAutoRecoverError() == Printer.BATTERY_OVERHEAT) {
                msg += mContext.getString(R.string.handlingmsg_err_overheat);
                msg += mContext.getString(R.string.handlingmsg_err_battery);
            }
            if (status.getAutoRecoverError() == Printer.WRONG_PAPER) {
                msg += mContext.getString(R.string.handlingmsg_err_wrong_paper);
            }
        }
        if (status.getBatteryLevel() == Printer.BATTERY_LEVEL_0) {
            msg += mContext.getString(R.string.handlingmsg_err_battery_real_end);
        }

        return msg;
    }

    public static void showException(Exception e, String method, Context context) {
        String msg = "";
        if (e instanceof Epos2Exception) {
            msg = String.format(
                    "%s\n\t%s\n%s\n\t%s",
                    context.getString(R.string.title_err_code),
                    getEposExceptionText(((Epos2Exception) e).getErrorStatus()),
                    context.getString(R.string.title_err_method),
                    method);
        } else {
            msg = e.toString();
        }

        LogUtil.d("ePOSPrinter", msg);
    }

    public static void showResult(int code, String errMsg, Context context) {
        String msg = "";
        if (errMsg.isEmpty()) {
            msg = String.format(
                    "\t%s\n\t%s\n",
                    context.getString(R.string.title_msg_result),
                    getCodeText(code));
        } else {
            msg = String.format(
                    "\t%s\n\t%s\n\n\t%s\n\t%s\n",
                    context.getString(R.string.title_msg_result),
                    getCodeText(code),
                    context.getString(R.string.title_msg_description),
                    errMsg);
        }
        LogUtil.d("ePOSPrinter", msg);
    }

    private static String getEposExceptionText(int state) {
        String return_text = "";
        switch (state) {
            case Epos2Exception.ERR_PARAM:
                return_text = "ERR_PARAM";
                break;
            case Epos2Exception.ERR_CONNECT:
                return_text = "ERR_CONNECT";
                break;
            case Epos2Exception.ERR_TIMEOUT:
                return_text = "ERR_TIMEOUT";
                break;
            case Epos2Exception.ERR_MEMORY:
                return_text = "ERR_MEMORY";
                break;
            case Epos2Exception.ERR_ILLEGAL:
                return_text = "ERR_ILLEGAL";
                break;
            case Epos2Exception.ERR_PROCESSING:
                return_text = "ERR_PROCESSING";
                break;
            case Epos2Exception.ERR_NOT_FOUND:
                return_text = "ERR_NOT_FOUND";
                break;
            case Epos2Exception.ERR_IN_USE:
                return_text = "ERR_IN_USE";
                break;
            case Epos2Exception.ERR_TYPE_INVALID:
                return_text = "ERR_TYPE_INVALID";
                break;
            case Epos2Exception.ERR_DISCONNECT:
                return_text = "ERR_DISCONNECT";
                break;
            case Epos2Exception.ERR_ALREADY_OPENED:
                return_text = "ERR_ALREADY_OPENED";
                break;
            case Epos2Exception.ERR_ALREADY_USED:
                return_text = "ERR_ALREADY_USED";
                break;
            case Epos2Exception.ERR_BOX_COUNT_OVER:
                return_text = "ERR_BOX_COUNT_OVER";
                break;
            case Epos2Exception.ERR_BOX_CLIENT_OVER:
                return_text = "ERR_BOX_CLIENT_OVER";
                break;
            case Epos2Exception.ERR_UNSUPPORTED:
                return_text = "ERR_UNSUPPORTED";
                break;
            case Epos2Exception.ERR_FAILURE:
                return_text = "ERR_FAILURE";
                break;
            default:
                return_text = String.format("%d", state);
                break;
        }
        return return_text;
    }

    private static String getCodeText(int state) {
        String return_text = "";
        switch (state) {
            case Epos2CallbackCode.CODE_SUCCESS:
                return_text = "PRINT_SUCCESS";
                break;
            case Epos2CallbackCode.CODE_PRINTING:
                return_text = "PRINTING";
                break;
            case Epos2CallbackCode.CODE_ERR_AUTORECOVER:
                return_text = "ERR_AUTORECOVER";
                break;
            case Epos2CallbackCode.CODE_ERR_COVER_OPEN:
                return_text = "ERR_COVER_OPEN";
                break;
            case Epos2CallbackCode.CODE_ERR_CUTTER:
                return_text = "ERR_CUTTER";
                break;
            case Epos2CallbackCode.CODE_ERR_MECHANICAL:
                return_text = "ERR_MECHANICAL";
                break;
            case Epos2CallbackCode.CODE_ERR_EMPTY:
                return_text = "ERR_EMPTY";
                break;
            case Epos2CallbackCode.CODE_ERR_UNRECOVERABLE:
                return_text = "ERR_UNRECOVERABLE";
                break;
            case Epos2CallbackCode.CODE_ERR_FAILURE:
                return_text = "ERR_FAILURE";
                break;
            case Epos2CallbackCode.CODE_ERR_NOT_FOUND:
                return_text = "ERR_NOT_FOUND";
                break;
            case Epos2CallbackCode.CODE_ERR_SYSTEM:
                return_text = "ERR_SYSTEM";
                break;
            case Epos2CallbackCode.CODE_ERR_PORT:
                return_text = "ERR_PORT";
                break;
            case Epos2CallbackCode.CODE_ERR_TIMEOUT:
                return_text = "ERR_TIMEOUT";
                break;
            case Epos2CallbackCode.CODE_ERR_JOB_NOT_FOUND:
                return_text = "ERR_JOB_NOT_FOUND";
                break;
            case Epos2CallbackCode.CODE_ERR_SPOOLER:
                return_text = "ERR_SPOOLER";
                break;
            case Epos2CallbackCode.CODE_ERR_BATTERY_LOW:
                return_text = "ERR_BATTERY_LOW";
                break;
            case Epos2CallbackCode.CODE_ERR_TOO_MANY_REQUESTS:
                return_text = "ERR_TOO_MANY_REQUESTS";
                break;
            case Epos2CallbackCode.CODE_ERR_REQUEST_ENTITY_TOO_LARGE:
                return_text = "ERR_REQUEST_ENTITY_TOO_LARGE";
                break;
            case Epos2CallbackCode.CODE_CANCELED:
                return_text = "CODE_CANCELED";
                break;
            case Epos2CallbackCode.CODE_ERR_NO_MICR_DATA:
                return_text = "ERR_NO_MICR_DATA";
                break;
            case Epos2CallbackCode.CODE_ERR_ILLEGAL_LENGTH:
                return_text = "ERR_ILLEGAL_LENGTH";
                break;
            case Epos2CallbackCode.CODE_ERR_NO_MAGNETIC_DATA:
                return_text = "ERR_NO_MAGNETIC_DATA";
                break;
            case Epos2CallbackCode.CODE_ERR_RECOGNITION:
                return_text = "ERR_RECOGNITION";
                break;
            case Epos2CallbackCode.CODE_ERR_READ:
                return_text = "ERR_READ";
                break;
            case Epos2CallbackCode.CODE_ERR_NOISE_DETECTED:
                return_text = "ERR_NOISE_DETECTED";
                break;
            case Epos2CallbackCode.CODE_ERR_PAPER_JAM:
                return_text = "ERR_PAPER_JAM";
                break;
            case Epos2CallbackCode.CODE_ERR_PAPER_PULLED_OUT:
                return_text = "ERR_PAPER_PULLED_OUT";
                break;
            case Epos2CallbackCode.CODE_ERR_CANCEL_FAILED:
                return_text = "ERR_CANCEL_FAILED";
                break;
            case Epos2CallbackCode.CODE_ERR_PAPER_TYPE:
                return_text = "ERR_PAPER_TYPE";
                break;
            case Epos2CallbackCode.CODE_ERR_WAIT_INSERTION:
                return_text = "ERR_WAIT_INSERTION";
                break;
            case Epos2CallbackCode.CODE_ERR_ILLEGAL:
                return_text = "ERR_ILLEGAL";
                break;
            case Epos2CallbackCode.CODE_ERR_INSERTED:
                return_text = "ERR_INSERTED";
                break;
            case Epos2CallbackCode.CODE_ERR_WAIT_REMOVAL:
                return_text = "ERR_WAIT_REMOVAL";
                break;
            case Epos2CallbackCode.CODE_ERR_DEVICE_BUSY:
                return_text = "ERR_DEVICE_BUSY";
                break;
            case Epos2CallbackCode.CODE_ERR_IN_USE:
                return_text = "ERR_IN_USE";
                break;
            case Epos2CallbackCode.CODE_ERR_CONNECT:
                return_text = "ERR_CONNECT";
                break;
            case Epos2CallbackCode.CODE_ERR_DISCONNECT:
                return_text = "ERR_DISCONNECT";
                break;
            case Epos2CallbackCode.CODE_ERR_MEMORY:
                return_text = "ERR_MEMORY";
                break;
            case Epos2CallbackCode.CODE_ERR_PROCESSING:
                return_text = "ERR_PROCESSING";
                break;
            case Epos2CallbackCode.CODE_ERR_PARAM:
                return_text = "ERR_PARAM";
                break;
            case Epos2CallbackCode.CODE_RETRY:
                return_text = "RETRY";
                break;
            case Epos2CallbackCode.CODE_ERR_DIFFERENT_MODEL:
                return_text = "ERR_DIFFERENT_MODEL";
                break;
            case Epos2CallbackCode.CODE_ERR_DIFFERENT_VERSION:
                return_text = "ERR_DIFFERENT_VERSION";
                break;
            case Epos2CallbackCode.CODE_ERR_DATA_CORRUPTED:
                return_text = "ERR_DATA_CORRUPTED";
                break;
            default:
                return_text = String.format("%d", state);
                break;
        }
        return return_text;
    }
}
