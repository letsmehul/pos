package hardware;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;

import com.ehsm.recore.activities.ParentActivity;
import com.ehsm.recore.hardware.USBPrinterPT80KMController;
import com.ehsm.recore.model.PaymentRequestModel;
import com.ehsm.recore.model.PrinterClockOutModel;
import com.ehsm.recore.model.PrinterDayCloseModel;
import com.ehsm.recore.model.PrinterDayStartModel;
import com.ehsm.recore.model.PrinterPayInOutModel;
import com.ehsm.recore.model.PrinterPaymentModel;
import com.ehsm.recore.model.PrinterPaymentSalesModel;
import com.ehsm.recore.model.PrinterRefundModel;
import com.ehsm.recore.model.PrinterSalesModel;
import com.ehsm.recore.utils.AppConstant;
import com.ehsm.recore.utils.BarcodeGenerator;
import com.ehsm.recore.utils.CM;
import com.ehsm.recore.utils.CV;
import com.ehsm.recore.utils.ImagesCache;
import com.ehsm.recore.utils.logger.LogUtil;

import android.util.*;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;

public class PrinterController {

    private static final String TAG = "PRINT_DATA_LOG";
    private static PrinterController sInstance = null;
    private PrinterStatusChangeListener mPrinterStatusChangeLister;
    private Context mContext;
    private int mConnectedFlag;
    private boolean mIsPrinterConnected;
    private USBPrinterPT80KMController mUsbPrinterPT80KMController;

    private BarcodeGenerator mBarcodeGenerator;
    ImagesCache mImagesCache = ImagesCache.getInstance();//Singleton instance handled in ImagesCache class.

    private USBPrinterPT80KMController.UsbStatusCallback usbPrinterStatusCallback =
            new USBPrinterPT80KMController.UsbStatusCallback() {
                @Override
                public void statusCallback(int value) {
                    if (mPrinterStatusChangeLister != null) {
                        mPrinterStatusChangeLister.OnPrinterStatusChangeLister(value);
                    }
                }
            };

    private PrinterController(Context context) {
        mContext = context;
        mUsbPrinterPT80KMController = USBPrinterPT80KMController.getInstance(mContext);
        mBarcodeGenerator = new BarcodeGenerator(mContext);
        mImagesCache.initializeCache();
    }

    //Recommended to make this class a Singleton as we need to use the same object
    // across the app
    public static PrinterController getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new PrinterController(context);
        }
        return sInstance;
    }

    public void setPrinterStatusChangeLister(PrinterStatusChangeListener statusChangeLister) {
        mPrinterStatusChangeLister = statusChangeLister;
        if (mUsbPrinterPT80KMController != null) {
            mPrinterStatusChangeLister = statusChangeLister;
            mUsbPrinterPT80KMController.setStatusCallback(usbPrinterStatusCallback);
        }
    }

    public boolean isPrinterConnected() {
        return mIsPrinterConnected;
    }


    public boolean connectPrinter() {

        if (mUsbPrinterPT80KMController == null) {
            LogUtil.d("HARDWARE_DEBUG", "USBPrinterController is null, again creating it");
        }

        mConnectedFlag = mUsbPrinterPT80KMController.PrinterController_Open();
        if (mConnectedFlag == 0) {
            mIsPrinterConnected = true;
            LogUtil.d("HARDWARE_DEBUG", "Printer Connected");
        } else {
            mIsPrinterConnected = false;
            LogUtil.d("HARDWARE_DEBUG", "Printer Connection Failed");
        }
        return mIsPrinterConnected;
    }

    public void disconnectPrinter() {
        if (!isPrinterConnected()) {
            return;
        }

        mConnectedFlag = mUsbPrinterPT80KMController.PrinterController_Close();
        if (mConnectedFlag == 0) {
            mIsPrinterConnected = false;
        }

    }

    public void printDayStartTicket(PrinterDayStartModel mPrinterDayStartModel) {

        connectPrinter();

        String mCashierName = mPrinterDayStartModel.getEmpRole() + " : " + mPrinterDayStartModel.getCashierName();
        String mDateTime = mPrinterDayStartModel.getDayStartDate();

        String mTitleParticularsAndAmount = "Particulars" + GetEmptySpace(CalculateRequiredSpace("Particulars", GetSpecificlength(10, "Amount"))) + AddEmptySpaceLeft(10, "Amount");

        String mCashinDrawer = "Cash in Drawer" +
                GetEmptySpace(CalculateRequiredSpace("Cash in Drawer", GetSpecificlength(10, FormatDigitTwoDesimal(mPrinterDayStartModel.getCashInDrawer())))) +
                AddEmptySpaceLeft(10, FormatDigitTwoDesimal(mPrinterDayStartModel.getCashInDrawer()));

        String mAddCashInDrawer = "Add Cash in Drawer" +
                GetEmptySpace(CalculateRequiredSpace("Add Cash in Drawer", GetSpecificlength(10, FormatDigitTwoDesimal(mPrinterDayStartModel.getAddCashAmount())))) +
                AddEmptySpaceLeft(10, FormatDigitTwoDesimal(mPrinterDayStartModel.getAddCashAmount()));

        String mTotalCashInDrawer = "TOTAL" +
                GetEmptySpace(CalculateRequiredSpace("TOTAL", GetSpecificlength(10, FormatDigitTwoDesimal(mPrinterDayStartModel.getTotalCashIndrawer())))) +
                AddEmptySpaceLeft(10, FormatDigitTwoDesimal(mPrinterDayStartModel.getTotalCashIndrawer()));

        LogUtil.e(TAG, "mTitleParticularsAndAmount   -    " + mTitleParticularsAndAmount);
        LogUtil.e(TAG, "mCashinDrawer   -    " + mCashinDrawer);
        LogUtil.e(TAG, "mAddCashInDrawer   -    " + mAddCashInDrawer);
        LogUtil.e(TAG, "mTotalCashInDrawer   -    " + mTotalCashInDrawer);
        LogUtil.e(TAG, "POSStationName   -    " + mPrinterDayStartModel.getPOSStationNo());


        mUsbPrinterPT80KMController.PrinterController_Take_The_Paper(1);
        mUsbPrinterPT80KMController.PrinterController_PrinterLanguage(0);

        mUsbPrinterPT80KMController.PrinterController_Font_Double_width();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print("Shift Start Receipt".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mDateTime.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mCashierName.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mPrinterDayStartModel.getPOSStationNo().getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());
        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mTitleParticularsAndAmount.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();

        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mCashinDrawer.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mAddCashInDrawer.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mTotalCashInDrawer.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print("ReCore".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Print("Point Of Sale".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        cutPrint();

    }

    public void printDayCloseTicket(PrinterDayCloseModel mPrinterDayCloseModel) {

        connectPrinter();

        String mCashierName = mPrinterDayCloseModel.getEmpRole() + " : " + mPrinterDayCloseModel.getCashierName();
        String mDateTime = mPrinterDayCloseModel.getDayCloseDateTime();

        String mTitleParticularsAndAmount = "Particulars" + GetEmptySpace(CalculateRequiredSpace("Particulars", GetSpecificlength(10, "Amount"))) + AddEmptySpaceLeft(10, "Amount");

        String mDayOpeningCash = "Shift Opening Cash" + GetEmptySpace(CalculateRequiredSpace("Shift Opening Cash", GetSpecificlength(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getDayOpningCash())))) + AddEmptySpaceLeft(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getDayOpningCash()));

        LogUtil.e(TAG, "getDayOpeningCash  -    " + mPrinterDayCloseModel.getDayOpningCash());
        LogUtil.e(TAG, "TotalTransactionAmount   -    " + FormatDigitTwoDesimal(mPrinterDayCloseModel.getTotalTransactionAmount()));
        LogUtil.e(TAG, "getTotalRefund   -    " + mPrinterDayCloseModel.getTotalRefund());
        LogUtil.e(TAG, "getTotalPayIn   -    " + mPrinterDayCloseModel.getTotalPayIn());
        LogUtil.e(TAG, "getTotalPayOut   -    " + mPrinterDayCloseModel.getTotalPayOut());
        LogUtil.e(TAG, "getWithdrawCash   -    " + mPrinterDayCloseModel.getWithdrawCash());
        LogUtil.e(TAG, "getDayCloseCash   -    " + mPrinterDayCloseModel.getDayCloseCash());

        LogUtil.e(TAG, "getTotalSaleByCash   -    " + mPrinterDayCloseModel.getTotalSaleByCash());

//        String mTotalTransactionNoandAmount = GetSpecificlength(17, GetSpecificlength(7, mPrinterDayCloseModel.getTotalNumberofTransactionAmount()) + AddEmptySpaceLeft(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getTotalTransactionAmount())));
        String mTotalTransactionAmount = "Total Transaction Amount" + GetEmptySpace(CalculateRequiredSpace("Total Transaction Amount", AddEmptySpaceLeft(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getTotalTransactionAmount())))) + AddEmptySpaceLeft(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getTotalTransactionAmount()));

        String mTotalSaleByCashNoandAmount = GetSpecificlength(17, GetSpecificlength(7, mPrinterDayCloseModel.getTotalNumberofSaleByCash()) + AddEmptySpaceLeft(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getTotalSaleByCash())));
        String mTotalSaleByCash = "Total Sale By Cash" + GetEmptySpace(CalculateRequiredSpace("Total Sale By Cash", mTotalSaleByCashNoandAmount)) + mTotalSaleByCashNoandAmount;

        String mTotalSaleByCardNoandAmount = GetSpecificlength(17, GetSpecificlength(7, mPrinterDayCloseModel.getTotalNumberofSaleByCard()) + AddEmptySpaceLeft(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getTotalSaleByCard())));
        String mTotalSaleByCard = "Total Sale By Card" + GetEmptySpace(CalculateRequiredSpace("Total Sale By Card", mTotalSaleByCardNoandAmount)) + mTotalSaleByCardNoandAmount;

        String mTotalSaleBySplit = "Total Sale By Split" + GetEmptySpace(CalculateRequiredSpace("Total Sale By Split", GetSpecificlength(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getTotalSaleBySplit())))) + AddEmptySpaceLeft(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getTotalSaleBySplit()));

        String mTotalSaleByOtherNoandAmount = GetSpecificlength(17, GetSpecificlength(7, mPrinterDayCloseModel.getTotalNumberofSaleByOther()) + AddEmptySpaceLeft(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getTotalSaleByOther())));
        String mTotalSaleByOther = "Total Sale By Other" + GetEmptySpace(CalculateRequiredSpace("Total Sale By Other", mTotalSaleByOtherNoandAmount)) + mTotalSaleByOtherNoandAmount;

        String mTotalRefundNoandAmount = GetSpecificlength(17, GetSpecificlength(7, mPrinterDayCloseModel.getTotalNumberofRefund()) + AddEmptySpaceLeft(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getTotalRefund())));
        String mTotalRefund = "Total Refund" + GetEmptySpace(CalculateRequiredSpace("Total Refund", mTotalRefundNoandAmount)) + mTotalRefundNoandAmount;

        String mTotalPayIn = "Total PayIn" + GetEmptySpace(CalculateRequiredSpace("Total PayIn", GetSpecificlength(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getTotalPayIn())))) + AddEmptySpaceLeft(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getTotalPayIn()));
        String mTotalPayOut = "Total PayOut" + GetEmptySpace(CalculateRequiredSpace("Total PayOut", GetSpecificlength(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getTotalPayOut())))) + AddEmptySpaceLeft(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getTotalPayOut()));
        String mWithdrawCash = "Withdraw Cash" + GetEmptySpace(CalculateRequiredSpace("Withdraw Cash", GetSpecificlength(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getWithdrawCash())))) + AddEmptySpaceLeft(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getWithdrawCash()));
        String mDayCloseCash = "Shift Close Cash" + GetEmptySpace(CalculateRequiredSpace("Shift Close Cash", GetSpecificlength(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getDayCloseCash())))) + AddEmptySpaceLeft(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getDayCloseCash()));
        String highCash = "Over Cash" + GetEmptySpace(CalculateRequiredSpace("Over Cash", GetSpecificlength(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getDayCloseCash())))) + AddEmptySpaceLeft(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getHighCash()));
        String shortCash = "Short Cash" + GetEmptySpace(CalculateRequiredSpace("Short Cash", GetSpecificlength(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getDayCloseCash())))) + AddEmptySpaceLeft(10, FormatDigitTwoDesimal(mPrinterDayCloseModel.getShortCash()));


        LogUtil.e(TAG, "mCashierName -                           " + mCashierName);
        LogUtil.e(TAG, "mDateTime -                              " + mDateTime);
        LogUtil.e(TAG, "mTitleParticularsAndAmount -             " + mTitleParticularsAndAmount);
        LogUtil.e(TAG, "mDayOpeningCash  -                       " + mDayOpeningCash);
        LogUtil.e(TAG, "mTotalSaleByCash -                       " + mTotalSaleByCash);
        LogUtil.e(TAG, "mTotalSaleByCard -                       " + mTotalSaleByCard);
        LogUtil.e(TAG, "mTotalSaleBySplit -                       " + mTotalSaleBySplit);
        LogUtil.e(TAG, "mTotalSaleByOther  -                     " + mTotalSaleByOther);
        LogUtil.e(TAG, "mTotalRefund  -                          " + mTotalRefund);
        LogUtil.e(TAG, "mTotalTransactionAmount -                " + mTotalTransactionAmount);
        LogUtil.e(TAG, "mTotalPayIn -                            " + mTotalPayIn);
        LogUtil.e(TAG, "mTotalPayOut  -                          " + mTotalPayOut);
        LogUtil.e(TAG, "mWithdrawCash  -                         " + mWithdrawCash);
        LogUtil.e(TAG, "mDayCloseCash  -                         " + mDayCloseCash);


        mUsbPrinterPT80KMController.PrinterController_Take_The_Paper(1);
        mUsbPrinterPT80KMController.PrinterController_PrinterLanguage(0);

        mUsbPrinterPT80KMController.PrinterController_Font_Double_width();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print("Shift Close Receipt".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mDateTime.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mCashierName.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mPrinterDayCloseModel.getPOSStationNo().getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());
        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mTitleParticularsAndAmount.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();

        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mDayOpeningCash.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mTotalSaleByCash.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mTotalSaleByCard.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mTotalSaleBySplit.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mTotalSaleByOther.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mTotalRefund.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mTotalTransactionAmount.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mTotalPayIn.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mTotalPayOut.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mWithdrawCash.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mDayCloseCash.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(highCash.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(shortCash.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print("ReCore".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Print("Point Of Sale".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        cutPrint();
    }

    public void printPayInTicket(PrinterPayInOutModel mPrinterPayInOutModel, String ReceiptTitle) {

        connectPrinter();

        String mCashierName = mPrinterPayInOutModel.getEmpRole() + " : " + mPrinterPayInOutModel.getCashierName();
        String mDateTime = mPrinterPayInOutModel.getDayStartDate();

        String mReason = ReceiptTitle + " Reason";
        ReceiptTitle = ReceiptTitle + " Receipt";

        String mTitlePayReasonAndAmount = mReason + GetEmptySpace(CalculateRequiredSpace(mReason, GetSpecificlength(10, "Amount"))) + AddEmptySpaceLeft(10, "Amount");

        String mPayReasonAndAmount = GetReasonLines(mPrinterPayInOutModel.getReason(), 1) +
                GetEmptySpace(CalculateRequiredSpace(GetReasonLines(mPrinterPayInOutModel.getReason(), 1), GetSpecificlength(10, FormatFourDigitTwoDesimal(mPrinterPayInOutModel.getPayInOutAmount()))))
                + AddEmptySpaceLeft(10, mPrinterPayInOutModel.getPayInOutAmount());


        String mTotal = GetSpecificlength(20, "Total") +
                GetEmptySpace(CalculateRequiredSpace(GetSpecificlength(20, "Total"), GetSpecificlength(10, mPrinterPayInOutModel.getPayInOutAmount())))
                + AddEmptySpaceLeft(10, mPrinterPayInOutModel.getPayInOutAmount());


        LogUtil.e(TAG, "ReceiptTitle -                          " + ReceiptTitle);
        LogUtil.e(TAG, "mCashierName -                          " + mCashierName);
        LogUtil.e(TAG, "mDateTime -                             " + mDateTime);
        LogUtil.e(TAG, "mTitlePayReasonAndAmount -              " + mTitlePayReasonAndAmount);

        LogUtil.e(TAG, "mPayReasonAndAmount  -                  " + mPayReasonAndAmount);
        LogUtil.e(TAG, "mTotal -                                " + mTotal);


        mUsbPrinterPT80KMController.PrinterController_Take_The_Paper(1);
        mUsbPrinterPT80KMController.PrinterController_PrinterLanguage(0);

        mUsbPrinterPT80KMController.PrinterController_Font_Double_width();
        mUsbPrinterPT80KMController.PrinterController_Font_Double_width();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(ReceiptTitle.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mDateTime.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mCashierName.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mPrinterPayInOutModel.getPOSStationNo().getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());
        mUsbPrinterPT80KMController.PrinterController_Linefeed();


        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mTitlePayReasonAndAmount.getBytes());


        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();

        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mPayReasonAndAmount.getBytes());

        if (mPrinterPayInOutModel.getReason().length() > 25) {
            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Print(GetReasonLines(mPrinterPayInOutModel.getReason(), 2).getBytes());
            LogUtil.e(TAG, "mPayReason    Line2  -                  " + GetReasonLines(mPrinterPayInOutModel.getReason(), 2));
        }

        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());


        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mTotal.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print("ReCore".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Print("Point Of Sale".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        cutPrint();

    }

    public void PrintClockOutReceipt(PrinterClockOutModel mPrinterClockOutModel) {

        connectPrinter();

        String mPosStationNo = "POS Station No :" + mPrinterClockOutModel.getPOSStationNo();
        String mDateTime = "Date :" + mPrinterClockOutModel.getDayCloseDate();

        String mReceiptTitle = "Clock Out Receipt";

        String mPosStationNoAndDateTime = GetSpecificlength(27, mPosStationNo) +
                GetEmptySpace(3) +
                GetSpecificlength(17, mDateTime);

        String mEmployeeName = AddEmptySpace(20, "Employee Name : ") + GetEmptySpace(7) + AddEmptySpace(20, mPrinterClockOutModel.getEmpName());
        String mEmployeeRole = AddEmptySpace(20, "Employee Role : ") + GetEmptySpace(7) + AddEmptySpace(20, mPrinterClockOutModel.getEmpRole());
        String mClockIn = AddEmptySpace(20, "Clock In : ") + GetEmptySpace(7) + AddEmptySpace(20, mPrinterClockOutModel.getClockInTime());
        String mClockOut = AddEmptySpace(20, "Clock Out : ") + GetEmptySpace(7) + AddEmptySpace(20, mPrinterClockOutModel.getClockOutTime());
        String mHoursWorked = AddEmptySpace(20, "Hours Worked: ") + GetEmptySpace(7) + AddEmptySpace(20, mPrinterClockOutModel.getTotalHours());

        String mClockOutNote1 = "Note : This receipt shows employee daily hours";
        String mClockOutNote2 = "worked information. Keep this for your records.";

        LogUtil.e(TAG, "ReceiptTitle -                          " + mReceiptTitle);
        LogUtil.e(TAG, "mPosStationNoAndDateTime -              " + mPosStationNoAndDateTime);
        LogUtil.e(TAG, "mEmployeeName -                         " + mEmployeeName);
        LogUtil.e(TAG, "mEmployeeRole -                         " + mEmployeeRole);
        LogUtil.e(TAG, "mClockIn  -                             " + mClockIn);
        LogUtil.e(TAG, "mClockOut -                             " + mClockOut);
        LogUtil.e(TAG, "mHoursWorked -                          " + mHoursWorked);
        LogUtil.e(TAG, "mClockOutNote -                         " + mClockOutNote1);
        LogUtil.e(TAG, "mClockOutNote2 -                        " + mClockOutNote2);


        mUsbPrinterPT80KMController.PrinterController_Take_The_Paper(1);
        mUsbPrinterPT80KMController.PrinterController_PrinterLanguage(0);

        mUsbPrinterPT80KMController.PrinterController_Font_Double_width();
        mUsbPrinterPT80KMController.PrinterController_Font_Double_width();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mReceiptTitle.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mPosStationNoAndDateTime.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Print("----------------------------------------------".getBytes());
        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mEmployeeName.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mEmployeeRole.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mClockIn.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mClockOut.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mHoursWorked.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());
        mUsbPrinterPT80KMController.PrinterController_Linefeed();


        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mClockOutNote1.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mClockOutNote2.getBytes());


        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        cutPrint();

    }

    public void printNewSalesTicket(ParentActivity mActivity, PrinterSalesModel mPrinterSalesModel, String from) {

        connectPrinter();

        String mCashierName = mPrinterSalesModel.getEmpRole() + " : " + mPrinterSalesModel.getCashierName();
        String mBilledName = "Billed To: " + mPrinterSalesModel.getBilledPersonName();
        String mDateTime = mPrinterSalesModel.getReceiptDateTime();
        String mBilledDateTime = "Billed Date: " + mPrinterSalesModel.getReceiptDateTime();
        String mInvoiceNo = "Invoice No: " + mPrinterSalesModel.getInvoiceNo();

        String mItemTitles = "Particulars " + GetEmptySpace(2) + "Qty" + GetEmptySpace(2) + "Rate  " + GetEmptySpace(2) + "Discount" + GetEmptySpace(2) + "     Total";

        String ProductsDetails, ProductModifier,Productinstruction;

        String mTotalQty = AddEmptySpaceLeft(47, AddEmptySpaceLeft(15, "Total Qty") + AddEmptySpaceLeft(10, mPrinterSalesModel.getTotalQty()));
        String mSubTotal = AddEmptySpaceLeft(47, AddEmptySpaceLeft(15, "Subtotal") + AddEmptySpaceLeft(10, mPrinterSalesModel.getSubtotal()));
        String mTotalTax = AddEmptySpaceLeft(47, GetSpecificlength(22, GetDiscountRemarkLines(mPrinterSalesModel.getDiscountRemark(), 2)) + AddEmptySpaceLeft(15, "TAX Applied") + AddEmptySpaceLeft(10, mPrinterSalesModel.getTaxApplied()));
        String mTotalDiscount = AddEmptySpaceLeft(47, GetSpecificlength(22, GetDiscountRemarkLines(mPrinterSalesModel.getDiscountRemark(), 1)) + AddEmptySpaceLeft(15, "Discount") + AddEmptySpaceLeft(10, mPrinterSalesModel.getDiscount()));
        String mTotalSavings = AddEmptySpaceLeft(47, AddEmptySpaceLeft(15, "Total Savings") + AddEmptySpaceLeft(10, mPrinterSalesModel.getTotalSavings()));
//        String mGrandTotal = AddEmptySpaceLeft(47,  GetSpecificlength(22, GetDiscountRemarkLines(mPrinterSalesModel.getDiscountRemark(), 2)) +AddEmptySpaceLeft(15, "Grand Total") + AddEmptySpaceLeft(10, mPrinterSalesModel.getGrandTotal()));

        String mGrandTotal = AddEmptySpaceLeft(47, AddEmptySpaceLeft(15, "Grand Total") + AddEmptySpaceLeft(10, mPrinterSalesModel.getGrandTotal()));
        String mTenderedAmount = AddEmptySpaceLeft(47, AddEmptySpaceLeft(15, "Tendered Amount") + AddEmptySpaceLeft(10, mPrinterSalesModel.getCustomerPaid()));
        String mTotalPayment = "Total Payment" + GetEmptySpace(CalculateRequiredSpace("Total Payment", AddEmptySpaceLeft(9, mPrinterSalesModel.getCustomerPaid()))) + AddEmptySpaceLeft(9, mPrinterSalesModel.getCustomerPaid());

        /*String mPaymentMode = null;
        String mCardType = null;
        String mCardNumber = null;
        String mCardHolderName = null;
        String mCustomerPaid = null;
        String mChangeDue = null;
        String mTransactionIdDate = null;*/
        ArrayList<PrinterPaymentModel> printPayments = new ArrayList<>();
        for (int i = 0; i < mPrinterSalesModel.getPaymentModes().size(); i++) {
            PrinterPaymentSalesModel printerPaymentSalesModel = mPrinterSalesModel.getPaymentModes().get(i);
            PrinterPaymentModel printerPaymentModel = new PrinterPaymentModel();
            printerPaymentModel.setmTenderedAmount(AddEmptySpaceLeft(47, AddEmptySpaceLeft(15, "Tendered Amount") + AddEmptySpaceLeft(10, printerPaymentSalesModel.getmTenderedAmount())));
            printerPaymentModel.setmPaymentMode(printerPaymentSalesModel.getmPaymentMode());
            if (printerPaymentSalesModel.getmPaymentMode().equalsIgnoreCase(AppConstant.CASH)) {
                printerPaymentModel.setmCustomerPaid(AddEmptySpaceLeft(47, AddEmptySpaceLeft(15, "Customer Paid") + AddEmptySpaceLeft(10, printerPaymentSalesModel.getmCustomerPaid())));
                printerPaymentModel.setmChangeDue(AddEmptySpaceLeft(47, AddEmptySpaceLeft(15, "Change Due") + AddEmptySpaceLeft(10, FormatDigitTwoDesimal(printerPaymentSalesModel.getmChangeDue()))));
                printerPaymentModel.setPaymentModeTitle("Mode of Payment : Cash");
                printerPaymentModel.setmCardType(printerPaymentSalesModel.getmCardType());
            } else if (printerPaymentSalesModel.isOtherCashPayment()) {
                printerPaymentModel.setOtherCashPayment(true);
                printerPaymentModel.setmCustomerPaid(AddEmptySpaceLeft(47, AddEmptySpaceLeft(15, "Customer Paid") + AddEmptySpaceLeft(10, printerPaymentSalesModel.getmCustomerPaid())));
                printerPaymentModel.setmChangeDue(AddEmptySpaceLeft(47, AddEmptySpaceLeft(15, "Change Due") + AddEmptySpaceLeft(10, FormatDigitTwoDesimal(printerPaymentSalesModel.getmChangeDue()))));
                printerPaymentModel.setPaymentModeTitle("Mode of Payment : "+printerPaymentSalesModel.getmPaymentMode());
                printerPaymentModel.setmCardType(printerPaymentSalesModel.getmCardType());
            }else if (printerPaymentSalesModel.getmPaymentMode().equalsIgnoreCase(AppConstant.APPLE_PAY)) {
                printerPaymentModel.setmCustomerPaid("Total Payment" + GetEmptySpace(CalculateRequiredSpace("Total Payment", AddEmptySpaceLeft(9, mPrinterSalesModel.getCustomerPaid()))) + AddEmptySpaceLeft(9, printerPaymentSalesModel.getmCustomerPaid()));
                printerPaymentModel.setPaymentModeTitle("Mode of Payment : APPLE PAY");
                printerPaymentModel.setmCardType(printerPaymentSalesModel.getmCardType());
                if (!TextUtils.isEmpty(printerPaymentSalesModel.getmCardNumber())) {
                    printerPaymentModel.setmCardNumber("Card : XXXX-XXXX-XXXX-" + printerPaymentSalesModel.getmCardNumber());
                } else {
                    printerPaymentModel.setmCardNumber("Card : XXXX-XXXX-XXXX-" + "  N/A");
                }

                if (!TextUtils.isEmpty(printerPaymentSalesModel.getmCardHolderName())) {
                    printerPaymentModel.setmCardHolderName("Cardholder : " + RemoveSpecialCharacter(printerPaymentSalesModel.getmCardHolderName()));
                } else {
                    printerPaymentModel.setmCardHolderName("Cardholder : " + "    N/A");
                }

                if (!TextUtils.isEmpty(printerPaymentSalesModel.getmTransactionNumber())) {
                    printerPaymentModel.setmTransactionIdDate("Transaction : " + printerPaymentSalesModel.getmCardNumber() + GetEmptySpace(CalculateRequiredSpace("Transaction : " + printerPaymentSalesModel.getmTransactionNumber(), printerPaymentSalesModel.getmTransactionIdDate())) + printerPaymentSalesModel.getmTransactionIdDate());
                } else {
                    printerPaymentModel.setmTransactionIdDate("Transaction : " + "Not available" + GetEmptySpace(CalculateRequiredSpace("Transaction : " + "Not available", printerPaymentSalesModel.getmTransactionIdDate())) + printerPaymentSalesModel.getmTransactionIdDate());
                }
            } else {
                printerPaymentModel.setmTransactionNumber(printerPaymentSalesModel.getmTransactionNumber());
                printerPaymentModel.setmStrSignature(printerPaymentSalesModel.getmStrSignature());
                printerPaymentModel.setmCustomerPaid("Total Payment" + GetEmptySpace(CalculateRequiredSpace("Total Payment", AddEmptySpaceLeft(9, mPrinterSalesModel.getCustomerPaid()))) + AddEmptySpaceLeft(9, printerPaymentSalesModel.getmCustomerPaid()));
                printerPaymentModel.setPaymentModeTitle("Mode of Payment : "+printerPaymentSalesModel.getmPaymentMode()+" Card");
                printerPaymentModel.setmCardType(printerPaymentSalesModel.getmCardType());

                if (!TextUtils.isEmpty(printerPaymentSalesModel.getmCardNumber())) {
                    printerPaymentModel.setmCardNumber("Card : XXXX-XXXX-XXXX-" + printerPaymentSalesModel.getmCardNumber());
                } else {
                    printerPaymentModel.setmCardNumber("Card : XXXX-XXXX-XXXX-" + "  N/A");
                }

                if (!TextUtils.isEmpty(printerPaymentSalesModel.getmCardHolderName())) {
                    printerPaymentModel.setmCardHolderName("Cardholder : " + RemoveSpecialCharacter(printerPaymentSalesModel.getmCardHolderName()));
                } else {
                    printerPaymentModel.setmCardHolderName("Cardholder : " + "    N/A");
                }

                if (!TextUtils.isEmpty(printerPaymentSalesModel.getmTransactionNumber())) {
                    printerPaymentModel.setmTransactionIdDate("Transaction : " + printerPaymentSalesModel.getmTransactionNumber() + GetEmptySpace(CalculateRequiredSpace("Transaction : " + printerPaymentSalesModel.getmTransactionNumber(), printerPaymentSalesModel.getmTransactionIdDate())) + printerPaymentSalesModel.getmTransactionIdDate());
                } else {
                    printerPaymentModel.setmTransactionIdDate("Transaction : " + "Not available" + GetEmptySpace(CalculateRequiredSpace("Transaction : " + "Not available", printerPaymentSalesModel.getmTransactionIdDate())) + printerPaymentSalesModel.getmTransactionIdDate());
                }
            }
            printPayments.add(printerPaymentModel);
        }
        /*if (mPrinterSalesModel.getPaymentMode().equalsIgnoreCase(AppConstant.CASH)) {
            mCustomerPaid = AddEmptySpaceLeft(47, AddEmptySpaceLeft(15, "Customer Paid") + AddEmptySpaceLeft(10, mPrinterSalesModel.getCustomerPaid()));
            mChangeDue = AddEmptySpaceLeft(47, AddEmptySpaceLeft(15, "Change Due") + AddEmptySpaceLeft(10, FormatDigitTwoDesimal(mPrinterSalesModel.getRemainingAmount())));
            mPaymentMode = "Mode of Payment : Cash";
        } else {
            mPaymentMode = "Mode of Payment : Card";
            if (mPrinterSalesModel.getPaymentMode().equalsIgnoreCase(AppConstant.CREDIT)) {
                mCardType = "Credit Card";
            } else {
                mCardType = "Debit Card";
            }

            if (!TextUtils.isEmpty(mPrinterSalesModel.getCardLastFoutDigits())) {
                mCardNumber = "Card : XXXX-XXXX-XXXX-" + mPrinterSalesModel.getCardLastFoutDigits();
            } else {
                mCardNumber = "Card : XXXX-XXXX-XXXX-" + "  N/A";
            }

            if (!TextUtils.isEmpty(mPrinterSalesModel.getCardHolderName())) {
                mCardHolderName = "Cardholder : " + RemoveSpecialCharacter(mPrinterSalesModel.getCardHolderName());
            } else {
                mCardHolderName = "Cardholder : " + "    N/A";
            }

            if (!TextUtils.isEmpty(mPrinterSalesModel.getCardTransactionNo())) {
                mTransactionIdDate = "Transaction : " + mPrinterSalesModel.getCardTransactionNo() + GetEmptySpace(CalculateRequiredSpace("Transaction : " + mPrinterSalesModel.getCardTransactionNo(), mDateTime)) + mDateTime;
            } else {
                mTransactionIdDate = "Transaction : " + "Not available" + GetEmptySpace(CalculateRequiredSpace("Transaction : " + "Not available", mDateTime)) + mDateTime;
            }
        }*/

        LogUtil.e(TAG, "StoreLogoUrl -             " + mPrinterSalesModel.getStoreLogoUrl());
        LogUtil.e(TAG, "StoreName -                " + mPrinterSalesModel.getStoreName());
        LogUtil.e(TAG, "AddressLine1 -             " + mPrinterSalesModel.getAddressLine1());
        LogUtil.e(TAG, "AddressLine2 -             " + mPrinterSalesModel.getAddressLine2());
        LogUtil.e(TAG, "Tel Number -               " + mPrinterSalesModel.getTelNumber());

        LogUtil.e(TAG, "mBilledPerson   -          " + mBilledName);
        LogUtil.e(TAG, "mBilledDateTime   -        " + mBilledDateTime);
        LogUtil.e(TAG, "mCashierName -             " + mCashierName);
        LogUtil.e(TAG, "mInvoiceNo -               " + mInvoiceNo);

        LogUtil.e(TAG, "mItemTitles -              " + mItemTitles);

        mUsbPrinterPT80KMController.PrinterController_Take_The_Paper(1);
        mUsbPrinterPT80KMController.PrinterController_PrinterLanguage(0);

        mUsbPrinterPT80KMController.PrinterController_Set_Center();

        if (mImagesCache.getImageFromWarehouse(CV.INSTANCE.getSQUARE_IMAGE_NAME()) != null) {
//            mUsbPrinterPT80KMController.PrinterController_Bitmap(mActivity.getBitmapFromLocal(mPrinterSalesModel.getStoreLogoLocalPath(), 200));
            mUsbPrinterPT80KMController.PrinterController_Bitmap(mImagesCache.getImageFromWarehouse(CV.INSTANCE.getSQUARE_IMAGE_NAME()));
        }

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Double_width();
        mUsbPrinterPT80KMController.PrinterController_Font_Double_width();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mPrinterSalesModel.getStoreName().getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mPrinterSalesModel.getAddressLine1().getBytes());

        if (mPrinterSalesModel.getAddressLine2().length() > 0) {
            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
            mUsbPrinterPT80KMController.PrinterController_Set_Center();
            mUsbPrinterPT80KMController.PrinterController_Print(mPrinterSalesModel.getAddressLine2().getBytes());
        }

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mPrinterSalesModel.getTelNumber().getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Double_width();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print("Sales Receipt".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mPrinterSalesModel.getPOSStationNo().getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mBilledName.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mBilledDateTime.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mCashierName.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mInvoiceNo.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mItemTitles.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());

//        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();

        for (int i = 0; i < mPrinterSalesModel.getSalesProducts().size(); i++) {
            Log.e(TAG, mPrinterSalesModel.getSalesProducts().get(i).getProductName().length() + "");

            if (mPrinterSalesModel.getSalesProducts().get(i).getProductName().length() <= 12) {
                Log.e(TAG, mPrinterSalesModel.getSalesProducts().get(i).getProductName().length() + "");
                ProductsDetails =
                        GetSpecificlength(12, mPrinterSalesModel.getSalesProducts().get(i).getProductName()) +
                                GetEmptySpace(1) + GetSpecificlength(3, mPrinterSalesModel.getSalesProducts().get(i).getQuanity()) +
                                GetEmptySpace(2) + GetSpecificlength(6, FormatTwoDigitTwoDesimal(mPrinterSalesModel.getSalesProducts().get(i).getRate())) +
                                GetEmptySpace(2) + GetSpecificlength(8, FormatTwoDigitTwoDesimal(mPrinterSalesModel.getSalesProducts().get(i).getDiscount())) +
                                GetEmptySpace(2) + AddEmptySpaceLeft(11, FormatTwoDigitTwoDesimal(mPrinterSalesModel.getSalesProducts().get(i).getTotal()));

            } else {
                if (mPrinterSalesModel.getSalesProducts().get(i).getProductName().length() > 12 && mPrinterSalesModel.getSalesProducts().get(i).getProductName().length() < 44) {
                    ProductsDetails =
                            GetSpecificlength(44, mPrinterSalesModel.getSalesProducts().get(i).getProductName()) + "\n" +
                                    GetEmptySpace(12) +
                                    GetEmptySpace(1) + GetSpecificlength(3, mPrinterSalesModel.getSalesProducts().get(i).getQuanity()) +
                                    GetEmptySpace(2) + GetSpecificlength(6, FormatTwoDigitTwoDesimal(mPrinterSalesModel.getSalesProducts().get(i).getRate())) +
                                    GetEmptySpace(2) + GetSpecificlength(8, FormatTwoDigitTwoDesimal(mPrinterSalesModel.getSalesProducts().get(i).getDiscount())) +
                                    GetEmptySpace(2) + AddEmptySpaceLeft(11, FormatTwoDigitTwoDesimal(mPrinterSalesModel.getSalesProducts().get(i).getTotal()));
                } else {
                    ProductsDetails =
                            GetSpecificlength(47, ellipsize(mPrinterSalesModel.getSalesProducts().get(i).getProductName())) + "\n" +
                                    GetEmptySpace(12) +
                                    GetEmptySpace(1) + GetSpecificlength(3, mPrinterSalesModel.getSalesProducts().get(i).getQuanity()) +
                                    GetEmptySpace(2) + GetSpecificlength(6, FormatTwoDigitTwoDesimal(mPrinterSalesModel.getSalesProducts().get(i).getRate())) +
                                    GetEmptySpace(2) + GetSpecificlength(8, FormatTwoDigitTwoDesimal(mPrinterSalesModel.getSalesProducts().get(i).getDiscount())) +
                                    GetEmptySpace(2) + AddEmptySpaceLeft(11, FormatTwoDigitTwoDesimal(mPrinterSalesModel.getSalesProducts().get(i).getTotal()));
                }
            }
//            ProductsDetails =
//                    GetSpecificlength(44,  ellipsize(mPrinterSalesModel.getSalesProducts().get(i).getProductName())) +
//                            GetEmptySpace(2) + GetSpecificlength(3, mPrinterSalesModel.getSalesProducts().get(i).getQuanity()) +
//                            GetEmptySpace(2) + GetSpecificlength(6, FormatTwoDigitTwoDesimal(mPrinterSalesModel.getSalesProducts().get(i).getRate())) +
//                            GetEmptySpace(2) + GetSpecificlength(8, FormatTwoDigitTwoDesimal(mPrinterSalesModel.getSalesProducts().get(i).getDiscount())) +
//                            GetEmptySpace(2) + AddEmptySpaceLeft(10, FormatTwoDigitTwoDesimal(mPrinterSalesModel.getSalesProducts().get(i).getTotal()));

            android.util.Log.d("productssss", ProductsDetails);


            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Print(ProductsDetails.getBytes());

            if (!TextUtils.isEmpty(mPrinterSalesModel.getSalesProducts().get(i).getInstruction())) {

                Productinstruction = "K. Notes : "+mPrinterSalesModel.getSalesProducts().get(i).getInstruction().trim();

                mUsbPrinterPT80KMController.PrinterController_Linefeed();
                mUsbPrinterPT80KMController.PrinterController_Print(Productinstruction.getBytes());

            }

            if (!TextUtils.isEmpty(mPrinterSalesModel.getSalesProducts().get(i).getModifierIngredient())) {

                ProductModifier = mPrinterSalesModel.getSalesProducts().get(i).getModifierIngredient().trim();
                LogUtil.e(TAG, "long print issue ModifierIngredient  -         " + ProductModifier);

                mUsbPrinterPT80KMController.PrinterController_Linefeed();
                mUsbPrinterPT80KMController.PrinterController_Print(ProductModifier.getBytes());

            }

            if (!TextUtils.isEmpty(mPrinterSalesModel.getSalesProducts().get(i).getDiscountRemark())) {
                String mDiscountRemark = "";
                mUsbPrinterPT80KMController.PrinterController_Linefeed();
                if (from.equals(CV.PRINT)) {
                    mDiscountRemark = mPrinterSalesModel.getSalesProducts().get(i).getDiscountRemark() + CV.APPLIED_SUCCESSFULLY;

                } else {
                    mDiscountRemark = mPrinterSalesModel.getSalesProducts().get(i).getDiscountRemark() + CV.WAS_APPLIED;
                }
                mUsbPrinterPT80KMController.PrinterController_Print(mDiscountRemark.getBytes());
                LogUtil.e(TAG, "DiscountRemark  -         " + mDiscountRemark);
            }
        }


        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Right();
        mUsbPrinterPT80KMController.PrinterController_Print(mTotalQty.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Right();
        mUsbPrinterPT80KMController.PrinterController_Print(mSubTotal.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Right();
        mUsbPrinterPT80KMController.PrinterController_Print(mTotalDiscount.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Right();
        mUsbPrinterPT80KMController.PrinterController_Print(mTotalTax.getBytes());


        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Right();
        mUsbPrinterPT80KMController.PrinterController_Print(mTotalSavings.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Right();
        mUsbPrinterPT80KMController.PrinterController_Print(mGrandTotal.getBytes());

       /* mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Right();
        mUsbPrinterPT80KMController.PrinterController_Print("-------------------------".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Right();
        mUsbPrinterPT80KMController.PrinterController_Print(mTenderedAmount.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Right();
        mUsbPrinterPT80KMController.PrinterController_Print("-------------------------".getBytes());*/
        LogUtil.e(TAG, "mSubTotal  -               " + mSubTotal);
        LogUtil.e(TAG, "mTotalTax -                " + mTotalTax);
        LogUtil.e(TAG, "mTotalDiscount -           " + mTotalDiscount);
        LogUtil.e(TAG, "mTotalSavings  -           " + mTotalSavings);
        LogUtil.e(TAG, "mGrandTotal -              " + mGrandTotal);
        LogUtil.e(TAG, "Tendered Amount-           " + mTenderedAmount);

        for (int i = 0; i < printPayments.size(); i++) {
            PrinterPaymentModel printerPaymentModel = printPayments.get(i);
            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
            mUsbPrinterPT80KMController.PrinterController_Font_Bold();
            mUsbPrinterPT80KMController.PrinterController_Set_Left();
            mUsbPrinterPT80KMController.PrinterController_Print(printerPaymentModel.getPaymentModeTitle().getBytes());

            LogUtil.e(TAG, "mPayment Mode  -           " + printerPaymentModel.getPaymentModeTitle());

            /*Start of Print Cash and Card Case Details*/
            if (printerPaymentModel.getmPaymentMode().equals(AppConstant.CASH) || printerPaymentModel.isOtherCashPayment()) {
                LogUtil.e(TAG, "mCustomerPaid  -           " + printerPaymentModel.getmCustomerPaid());
                LogUtil.e(TAG, "mChangeDue  -              " + printerPaymentModel.getmChangeDue());
                LogUtil.e(TAG, "mTenderedAmount  -              " + printerPaymentModel.getmTenderedAmount());


                mUsbPrinterPT80KMController.PrinterController_Linefeed();

                mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
                mUsbPrinterPT80KMController.PrinterController_Font_Bold();
                mUsbPrinterPT80KMController.PrinterController_Set_Right();
                mUsbPrinterPT80KMController.PrinterController_Print("-------------------------".getBytes());


                /*mUsbPrinterPT80KMController.PrinterController_Linefeed();
                mUsbPrinterPT80KMController.PrinterController_Set_Right();
                mUsbPrinterPT80KMController.PrinterController_Print(printerPaymentModel.getmTenderedAmount().getBytes());*/

                mUsbPrinterPT80KMController.PrinterController_Linefeed();
                mUsbPrinterPT80KMController.PrinterController_Set_Right();
                mUsbPrinterPT80KMController.PrinterController_Print(printerPaymentModel.getmCustomerPaid().getBytes());

                if (printPayments.size() == 1) {
                    mUsbPrinterPT80KMController.PrinterController_Linefeed();
                    mUsbPrinterPT80KMController.PrinterController_Set_Right();
                    mUsbPrinterPT80KMController.PrinterController_Print(printerPaymentModel.getmChangeDue().getBytes());
                }

                mUsbPrinterPT80KMController.PrinterController_Linefeed();
                mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
                mUsbPrinterPT80KMController.PrinterController_Font_Bold();
                mUsbPrinterPT80KMController.PrinterController_Set_Right();
                mUsbPrinterPT80KMController.PrinterController_Print("-------------------------".getBytes());
            } else {
                LogUtil.e(TAG, "mCardType  -               " + printerPaymentModel.getmCardType());
                LogUtil.e(TAG, "mCardNumber  -             " + printerPaymentModel.getmCardNumber());
                LogUtil.e(TAG, "mCardHolderName  -         " + printerPaymentModel.getmCardHolderName());
                LogUtil.e(TAG, "mTotalPayment  -           " + printerPaymentModel.getmCustomerPaid());
                LogUtil.e(TAG, "SIGNATURE                  " + "CARDHOLDER SIGNATURE :");

                mUsbPrinterPT80KMController.PrinterController_Linefeed();
                mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());

                mUsbPrinterPT80KMController.PrinterController_Linefeed();
                mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
                mUsbPrinterPT80KMController.PrinterController_Set_Left();
                mUsbPrinterPT80KMController.PrinterController_Print(printerPaymentModel.getmCardType().getBytes());

                mUsbPrinterPT80KMController.PrinterController_Linefeed();
                mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
                mUsbPrinterPT80KMController.PrinterController_Set_Left();
                mUsbPrinterPT80KMController.PrinterController_Print(printerPaymentModel.getmCardNumber().getBytes());

                mUsbPrinterPT80KMController.PrinterController_Linefeed();
                mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();

                mUsbPrinterPT80KMController.PrinterController_Set_Left();
                mUsbPrinterPT80KMController.PrinterController_Print(printerPaymentModel.getmCardHolderName().getBytes());

                mUsbPrinterPT80KMController.PrinterController_Linefeed();
                mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
                mUsbPrinterPT80KMController.PrinterController_Set_Left();
                mUsbPrinterPT80KMController.PrinterController_Print(printerPaymentModel.getmCustomerPaid().getBytes());

                mUsbPrinterPT80KMController.PrinterController_Linefeed();
                mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());

                if (printerPaymentModel.getmTransactionIdDate() != null) {
                    mUsbPrinterPT80KMController.PrinterController_Linefeed();
                    mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
                    mUsbPrinterPT80KMController.PrinterController_Set_Left();
                    mUsbPrinterPT80KMController.PrinterController_Print(printerPaymentModel.getmTransactionIdDate().getBytes());
                }

                mUsbPrinterPT80KMController.PrinterController_Linefeed();
                mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());


                if (printerPaymentModel.getmPaymentMode().equals(AppConstant.CREDIT)) {

                    if (!TextUtils.isEmpty(printerPaymentModel.getmStrSignature())) {
                        LogUtil.e(TAG, "signatureBase64 Not Empty -             " + printerPaymentModel.getmStrSignature());

                        mUsbPrinterPT80KMController.PrinterController_Linefeed();
                        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
                        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
                        mUsbPrinterPT80KMController.PrinterController_Set_Left();
                        mUsbPrinterPT80KMController.PrinterController_Print("CARDHOLDER SIGNATURE : ".getBytes());


                        if (mImagesCache.getImageFromWarehouse(printerPaymentModel.getmTransactionNumber()) != null) {
                            mUsbPrinterPT80KMController.PrinterController_Linefeed();
                            mUsbPrinterPT80KMController.PrinterController_Set_Center();
                            mUsbPrinterPT80KMController.PrinterController_Bitmap(mImagesCache.getImageFromWarehouse(printerPaymentModel.getmTransactionNumber()));
                        }

                        mUsbPrinterPT80KMController.PrinterController_Linefeed();
                        mUsbPrinterPT80KMController.PrinterController_Set_Center();
                        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
                        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());

                    } else {
                        LogUtil.e(TAG, "signatureBase64 Empty -             ");

                    }

                }

            }
            /*End of Print Card Case Details */
        }
        if (printPayments.size() > 1) {
            String mtotalcustomer="Total Customer Paid : "+mPrinterSalesModel.getGrandTotal();
            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Set_Right();
            mUsbPrinterPT80KMController.PrinterController_Print(mtotalcustomer.getBytes());
        }


//        mUsbPrinterPT80KMController.PrinterController_Linefeed();
//        mUsbPrinterPT80KMController.PrinterController_Set_Center();
//        mUsbPrinterPT80KMController.PrinterController_Bitmap(mBarcodeGenerator.GenerateBarcode(mPrinterSalesModel.getInvoiceNo()));
        if (!TextUtils.isEmpty(mPrinterSalesModel.getStrSocialMedia())) {
            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
            mUsbPrinterPT80KMController.PrinterController_Set_Center();
            mUsbPrinterPT80KMController.PrinterController_Print(mPrinterSalesModel.getStrSocialMedia().getBytes());
        }
        if (mImagesCache.getImageFromWarehouse(CV.INSTANCE.getBARCODE_IMAGE_NAME()) != null) {
            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Set_Center();
            mUsbPrinterPT80KMController.PrinterController_Bitmap(mImagesCache.getImageFromWarehouse(CV.INSTANCE.getBARCODE_IMAGE_NAME()));
        }

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print("ReCore".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Print("Point Of Sale".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print("Thank you... visit again!".getBytes());

       /* mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();

        if (!TextUtils.isEmpty(mPrinterSalesModel.getInstagram())) {
            mUsbPrinterPT80KMController.PrinterController_Print(mPrinterSalesModel.getInstagram().getBytes());
        }
        if (!TextUtils.isEmpty(mPrinterSalesModel.getFacebook())) {
            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Print(mPrinterSalesModel.getFacebook().getBytes());
        }
        if (!TextUtils.isEmpty(mPrinterSalesModel.getTwitter())) {
            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Print(mPrinterSalesModel.getTwitter().getBytes());
        }
        if (!TextUtils.isEmpty(mPrinterSalesModel.getYoutube())) {
            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Print(mPrinterSalesModel.getYoutube().getBytes());
        }*/


        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        cutPrint();
    }

    public void printRefundTicket(ParentActivity mActivity, PrinterRefundModel mPrinterRefundModel) {
        connectPrinter();

        String RefundINV = mPrinterRefundModel.getInvoiceNo();
        String mRefundInvoiceNo = "Refund Inv No: INV-" + RefundINV.substring(RefundINV.lastIndexOf("-") + 1);
        String mPOSNo = "POS Station : " + mPrinterRefundModel.getPOSStationNo();

        String mBilledName = "Billed To: " + mPrinterRefundModel.getBilledPersonName();
        String mBilledDateTime = "Billed Date : " + mPrinterRefundModel.getReceiptDateTime();

        String mCashierName = mPrinterRefundModel.getEmpRole() + " : " + mPrinterRefundModel.getCashierName();
        String mInvoiceNo = "Invoice No: " + mPrinterRefundModel.getInvoiceNo();

        String mItemTitles = "Particulars" + GetEmptySpace(5) + "Qty" + GetEmptySpace(4) + "Discount" + GetEmptySpace(5) + "Paid Amount";

        String ProductsDetails;

        String RefundReason = "Reason for Refund : " + GetRefundReasonLines(mPrinterRefundModel.getRefundReason(), 1);
        String mRefundPaymentMode = null;
        if (mPrinterRefundModel.getPaymentMode().equalsIgnoreCase(AppConstant.CREDIT)) {
            mRefundPaymentMode = "Credit Card";
        } else if (mPrinterRefundModel.getPaymentMode().equalsIgnoreCase(AppConstant.DEBIT)) {
            mRefundPaymentMode = "Debit Card";
        } else if (mPrinterRefundModel.getPaymentMode().equalsIgnoreCase(AppConstant.CASH)) {
            mRefundPaymentMode = "Cash";
        } else if (mPrinterRefundModel.isOtherPaymentMode()) {
            mRefundPaymentMode = mPrinterRefundModel.getPaymentMode();
        } else if (mPrinterRefundModel.getPaymentMode().equalsIgnoreCase(AppConstant.APPLE_PAY)) {
            mRefundPaymentMode = "APPLE PAY";
        } else if (mPrinterRefundModel.getPaymentMode().equalsIgnoreCase(AppConstant.MANUAL_CARD)) {
            mRefundPaymentMode = "Manual Card";
        }else{
            mRefundPaymentMode = mPrinterRefundModel.getPaymentMode();
        }

        String RefundPaymentMode = "Refunded to original source of payment  -";

        String mTotalQty = AddEmptySpaceLeft(47, AddEmptySpaceLeft(15, "Total Qty") + AddEmptySpaceLeft(10, mPrinterRefundModel.getTotalQty()));
        String SubTotal = AddEmptySpaceLeft(47, AddEmptySpaceLeft(15, "Subtotal") + AddEmptySpaceLeft(10, mPrinterRefundModel.getSubtotal()));
        String TotalTax = AddEmptySpaceLeft(47, GetSpecificlength(22, GetDiscountRemarkLines(mPrinterRefundModel.getDiscountRemark(), 2)) + AddEmptySpaceLeft(15, "TAX Applied") + AddEmptySpaceLeft(10, mPrinterRefundModel.getTaxApplied()));
        String mTotalDiscount = AddEmptySpaceLeft(47, GetSpecificlength(22, GetDiscountRemarkLines(mPrinterRefundModel.getDiscountRemark(), 1)) + AddEmptySpaceLeft(15, "Discount") + AddEmptySpaceLeft(10, mPrinterRefundModel.getDiscount()));

        //String TotalTax = AddEmptySpaceLeft(47, AddEmptySpaceLeft(15, "TAX Applied") + AddEmptySpaceLeft(10, mPrinterRefundModel.getTaxApplied()));
       // String mTotalDiscount = AddEmptySpaceLeft(47, AddEmptySpaceLeft(15, "Discount") + AddEmptySpaceLeft(10, mPrinterRefundModel.getDiscount()));
        String GrandTotal = AddEmptySpaceLeft(47, AddEmptySpaceLeft(15, "Grand Total") + AddEmptySpaceLeft(10, mPrinterRefundModel.getGrandTotal()));
        String mRefundAmount = AddEmptySpaceLeft(47, AddEmptySpaceLeft(15, "Refund Amount") + AddEmptySpaceLeft(10, mPrinterRefundModel.getRefundAmount()));


        LogUtil.e(TAG, "StoreLogoUrl -             " + mPrinterRefundModel.getStoreLogoUrl());

        LogUtil.e(TAG, "StoreName -                " + mPrinterRefundModel.getStoreName());
        LogUtil.e(TAG, "AddressLine1 -             " + mPrinterRefundModel.getAddressLine1());
        LogUtil.e(TAG, "AddressLine2 -             " + mPrinterRefundModel.getAddressLine2());
        LogUtil.e(TAG, "Tel Number -               " + mPrinterRefundModel.getTelNumber());

        LogUtil.e(TAG, "mPOSNo -                   " + mPOSNo);
        LogUtil.e(TAG, "mRefundInvoiceNo  -        " + mRefundInvoiceNo);
        LogUtil.e(TAG, "mBilledPerson   -          " + mBilledName);
        LogUtil.e(TAG, "mBilledDateTime   -        " + mBilledDateTime);
        LogUtil.e(TAG, "mCashierandInvoice -       " + mCashierName);
        LogUtil.e(TAG, "mInvoiceNo -               " + mInvoiceNo);

        LogUtil.e(TAG, "mItemTitles -              " + mItemTitles);


        mUsbPrinterPT80KMController.PrinterController_Take_The_Paper(1);

        mUsbPrinterPT80KMController.PrinterController_PrinterLanguage(0);

//        if (mActivity.getBitmapFromLocal(mPrinterRefundModel.getStoreLogoLocalPath(), 200) != null) {
//            mUsbPrinterPT80KMController.PrinterController_Set_Center();
//            mUsbPrinterPT80KMController.PrinterController_Bitmap(mActivity.getBitmapFromLocal(mPrinterRefundModel.getStoreLogoLocalPath(), 200));
//        }
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        if (mImagesCache.getImageFromWarehouse(CV.INSTANCE.getSQUARE_IMAGE_NAME()) != null) {
            mUsbPrinterPT80KMController.PrinterController_Bitmap(mImagesCache.getImageFromWarehouse(CV.INSTANCE.getSQUARE_IMAGE_NAME()));
        }

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Double_width();
        mUsbPrinterPT80KMController.PrinterController_Font_Double_width();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mPrinterRefundModel.getStoreName().getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mPrinterRefundModel.getAddressLine1().getBytes());

        if (mPrinterRefundModel.getAddressLine2().length() > 0) {
            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
            mUsbPrinterPT80KMController.PrinterController_Set_Center();
            mUsbPrinterPT80KMController.PrinterController_Print(mPrinterRefundModel.getAddressLine2().getBytes());
        }

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mPrinterRefundModel.getTelNumber().getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Double_width();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print("Refund Receipt".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mPOSNo.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mRefundInvoiceNo.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mBilledName.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mBilledDateTime.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mCashierName.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mInvoiceNo.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mItemTitles.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();

        for (int i = 0; i < mPrinterRefundModel.getRefundProducts().size(); i++) {
            Log.e("aaaa", mPrinterRefundModel.getRefundProducts().get(i).getProductName().length() + "");
            Log.d("aaaa", mPrinterRefundModel.getRefundProducts().get(i).getProductName().length() + "");

//            int productNumber = mPrinterRefundModel.getRefundProducts().get(i).getProductName().length();
            if (mPrinterRefundModel.getRefundProducts().get(i).getProductName().length() <= 12) {
                Log.e(TAG, mPrinterRefundModel.getRefundProducts().get(i).getProductName().length() + "");

                ProductsDetails =
                        GetSpecificlength(12, mPrinterRefundModel.getRefundProducts().get(i).getProductName()) +
                                GetEmptySpace(3) + AddEmptySpaceLeft(3, mPrinterRefundModel.getRefundProducts().get(i).getQuanity()) +
                                GetEmptySpace(4) + AddEmptySpaceLeft(10, FormatTwoDigitTwoDesimal(mPrinterRefundModel.getRefundProducts().get(i).getDiscount())) +
                                GetEmptySpace(4) + AddEmptySpaceLeft(11, FormatTwoDigitTwoDesimal(mPrinterRefundModel.getRefundProducts().get(i).getPaidAmount()));

            } else {
                if (mPrinterRefundModel.getRefundProducts().get(i).getProductName().length() > 12 && mPrinterRefundModel.getRefundProducts().get(i).getProductName().length() < 44) {
                    ProductsDetails =
                            GetSpecificlength(44, mPrinterRefundModel.getRefundProducts().get(i).getProductName()) + "\n" +
                                    GetEmptySpace(12) +
                                    GetEmptySpace(3) + AddEmptySpaceLeft(3, mPrinterRefundModel.getRefundProducts().get(i).getQuanity()) +
                                    GetEmptySpace(4) + AddEmptySpaceLeft(10, FormatTwoDigitTwoDesimal(mPrinterRefundModel.getRefundProducts().get(i).getDiscount())) +
                                    GetEmptySpace(4) + AddEmptySpaceLeft(11, FormatTwoDigitTwoDesimal(mPrinterRefundModel.getRefundProducts().get(i).getPaidAmount()));

                } else {
                    ProductsDetails =
                            GetSpecificlength(47, ellipsize(mPrinterRefundModel.getRefundProducts().get(i).getProductName())) + "\n" +
                                    GetEmptySpace(12) +
                                    GetEmptySpace(3) + AddEmptySpaceLeft(3, mPrinterRefundModel.getRefundProducts().get(i).getQuanity()) +
                                    GetEmptySpace(4) + AddEmptySpaceLeft(10, FormatTwoDigitTwoDesimal(mPrinterRefundModel.getRefundProducts().get(i).getDiscount())) +
                                    GetEmptySpace(4) + AddEmptySpaceLeft(11, FormatTwoDigitTwoDesimal(mPrinterRefundModel.getRefundProducts().get(i).getPaidAmount()));

                }
            }
//            ProductsDetails =
//                    GetSpecificlength(44, ellipsize(mPrinterRefundModel.getRefundProducts().get(i).getProductName())) +
//                            GetEmptySpace(5) + AddEmptySpaceLeft(3, mPrinterRefundModel.getRefundProducts().get(i).getQuanity()) +
//                            GetEmptySpace(5) + AddEmptySpaceLeft(8, FormatTwoDigitTwoDesimal(mPrinterRefundModel.getRefundProducts().get(i).getDiscount())) +
//                            GetEmptySpace(5) + AddEmptySpaceLeft(10, FormatTwoDigitTwoDesimal(mPrinterRefundModel.getRefundProducts().get(i).getPaidAmount()));

            LogUtil.e(TAG, "ProductsDetails  -         " + ProductsDetails);

            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Print(ProductsDetails.getBytes());
//            mUsbPrinterPT80KMController.PrinterController_Linefeed();

            if (!TextUtils.isEmpty(mPrinterRefundModel.getRefundProducts().get(i).getModifierIngredient())) {
                LogUtil.e(TAG, "ProductsDetails  -         " + mPrinterRefundModel.getRefundProducts().get(i).getModifierIngredient());
                mUsbPrinterPT80KMController.PrinterController_Linefeed();
                mUsbPrinterPT80KMController.PrinterController_Print(mPrinterRefundModel.getRefundProducts().get(i).getModifierIngredient().trim().getBytes());
            }

            if (!TextUtils.isEmpty(mPrinterRefundModel.getRefundProducts().get(i).getDiscountRemark())) {
                mUsbPrinterPT80KMController.PrinterController_Linefeed();
                String mDiscountRemark = mPrinterRefundModel.getRefundProducts().get(i).getDiscountRemark() + CV.WAS_APPLIED;
                mUsbPrinterPT80KMController.PrinterController_Print(mDiscountRemark.getBytes());
                LogUtil.e(TAG, "DiscountRemark  -         " + mDiscountRemark);
            }
        }

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Right();
        mUsbPrinterPT80KMController.PrinterController_Print(mTotalQty.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Right();
        mUsbPrinterPT80KMController.PrinterController_Print(SubTotal.getBytes());


        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Right();
        mUsbPrinterPT80KMController.PrinterController_Print(mTotalDiscount.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Right();
        mUsbPrinterPT80KMController.PrinterController_Print(TotalTax.getBytes());


        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Right();
        mUsbPrinterPT80KMController.PrinterController_Print(GrandTotal.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Right();
        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Print(mRefundAmount.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Right();
        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Print(RefundPaymentMode.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Print(mRefundPaymentMode.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Print(RefundReason.getBytes());

        if (mPrinterRefundModel.getRefundReason().length() > 27) {

            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Set_Right();
            mUsbPrinterPT80KMController.PrinterController_Font_Bold();
            mUsbPrinterPT80KMController.PrinterController_Print(GetRefundReasonLines(mPrinterRefundModel.getRefundReason(), 2).getBytes());
        }

//        mUsbPrinterPT80KMController.PrinterController_Linefeed();
//        mUsbPrinterPT80KMController.PrinterController_Set_Center();
//        mUsbPrinterPT80KMController.PrinterController_Bitmap(mBarcodeGenerator.GenerateBarcode(mPrinterRefundModel.getInvoiceNo()));
        if (!TextUtils.isEmpty(mPrinterRefundModel.getStrSocialMedia())) {
            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
            mUsbPrinterPT80KMController.PrinterController_Set_Center();
            mUsbPrinterPT80KMController.PrinterController_Print(mPrinterRefundModel.getStrSocialMedia().getBytes());
        }
        if (mImagesCache.getImageFromWarehouse(CV.INSTANCE.getBARCODE_IMAGE_NAME()) != null) {
            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Set_Center();
            mUsbPrinterPT80KMController.PrinterController_Bitmap(mImagesCache.getImageFromWarehouse(CV.INSTANCE.getBARCODE_IMAGE_NAME()));
        }
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print("ReCore".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Print("Point Of Sale".getBytes());

        /*mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();

        if (!TextUtils.isEmpty(mPrinterRefundModel.getInstagram())) {
            mUsbPrinterPT80KMController.PrinterController_Print(mPrinterRefundModel.getInstagram().getBytes());
        }
        if (!TextUtils.isEmpty(mPrinterRefundModel.getFacebook())) {
            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Print(mPrinterRefundModel.getFacebook().getBytes());
        }
        if (!TextUtils.isEmpty(mPrinterRefundModel.getTwitter())) {
            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Print(mPrinterRefundModel.getTwitter().getBytes());
        }
        if (!TextUtils.isEmpty(mPrinterRefundModel.getYoutube())) {
            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Print(mPrinterRefundModel.getYoutube().getBytes());
        }*/

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        LogUtil.e(TAG, "TotalQty -                 " + mTotalQty);
        LogUtil.e(TAG, "SubTotal -                 " + SubTotal);
        LogUtil.e(TAG, "TotalTax -                 " + TotalTax);
        LogUtil.e(TAG, "mTotalDiscont -            " + mTotalDiscount);
        LogUtil.e(TAG, "GrandTotal -               " + GrandTotal);
        LogUtil.e(TAG, "mRefundAmount -            " + mRefundAmount);

        LogUtil.e(TAG, "RefundPaymentMode -        " + RefundPaymentMode);
        LogUtil.e(TAG, "RefundPaymentMode -        " + mRefundPaymentMode);
        LogUtil.e(TAG, "mRefundReason -            " + RefundReason);

        cutPrint();
    }

    public static String ellipsize(String input) {
        return input.substring(0, 44).concat("...");
    }

    public void printVoidOrderTicket(ParentActivity mActivity, PrinterSalesModel mPrinterSalesModel) {

        connectPrinter();

        String mCashierName = mPrinterSalesModel.getEmpRole() + " : " + mPrinterSalesModel.getCashierName();
        String mBilledName = "Billed To: " + mPrinterSalesModel.getBilledPersonName();
        String mBilledDateTime ="Billed Date: " + mPrinterSalesModel.getReceiptDateTime();
        String mInvoiceNo = "Invoice No: " + mPrinterSalesModel.getInvoiceNo();

        String mItemTitles = GetSpecificlength(24, "Particulars") + GetEmptySpace(3) + AddEmptySpaceLeft(4, "Qty") + GetEmptySpace(6) + AddEmptySpaceLeft(10, "Total");

        String mTotalQty = "Item Count" + GetEmptySpace(CalculateRequiredSpace("Item Count", GetSpecificlength(10, mPrinterSalesModel.getTotalQty()))) + AddEmptySpaceLeft(10, mPrinterSalesModel.getTotalQty());
//        String mTotalDiscont = AddEmptySpaceLeft(47, GetSpecificlength(20, mPrinterSalesModel.getDiscountRemark()) + AddEmptySpaceLeft(15, "Discount") + AddEmptySpaceLeft(10, mPrinterSalesModel.getDiscount()));

        LogUtil.e(TAG, "StoreLogoUrl -             " + mPrinterSalesModel.getStoreLogoUrl());
        LogUtil.e(TAG, "StoreName -                " + mPrinterSalesModel.getStoreName());
        LogUtil.e(TAG, "AddressLine1 -             " + mPrinterSalesModel.getAddressLine1());
        LogUtil.e(TAG, "AddressLine2 -             " + mPrinterSalesModel.getAddressLine2());
        LogUtil.e(TAG, "Tel Number -               " + mPrinterSalesModel.getTelNumber());

        LogUtil.e(TAG, "mBilledPerson   -          " + mBilledName);
        LogUtil.e(TAG, "mBilledDateTime   -        " + mBilledDateTime);
        LogUtil.e(TAG, "mCashierName -             " + mCashierName);
        LogUtil.e(TAG, "mInvoiceNo -               " + mInvoiceNo);

        LogUtil.e(TAG, "mItemTitles -              " + mItemTitles);

        mUsbPrinterPT80KMController.PrinterController_Take_The_Paper(1);
        mUsbPrinterPT80KMController.PrinterController_PrinterLanguage(0);

        mUsbPrinterPT80KMController.PrinterController_Set_Center();

//        if (mActivity.getBitmapFromLocal(mPrinterSalesModel.getStoreLogoLocalPath(), 200) != null) {
//            mUsbPrinterPT80KMController.PrinterController_Bitmap(mActivity.getBitmapFromLocal(mPrinterSalesModel.getStoreLogoLocalPath(), 200));
//        }

        if (mImagesCache.getImageFromWarehouse(CV.INSTANCE.getSQUARE_IMAGE_NAME()) != null) {
            mUsbPrinterPT80KMController.PrinterController_Bitmap(mImagesCache.getImageFromWarehouse(CV.INSTANCE.getSQUARE_IMAGE_NAME()));
        }

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Double_width();
        mUsbPrinterPT80KMController.PrinterController_Font_Double_width();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mPrinterSalesModel.getStoreName().getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mPrinterSalesModel.getAddressLine1().getBytes());

        if (mPrinterSalesModel.getAddressLine2().length() > 0) {
            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
            mUsbPrinterPT80KMController.PrinterController_Set_Center();
            mUsbPrinterPT80KMController.PrinterController_Print(mPrinterSalesModel.getAddressLine2().getBytes());
        }

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mPrinterSalesModel.getTelNumber().getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Double_width();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print("Void Receipt".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mPrinterSalesModel.getPOSStationNo().getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mBilledName.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mBilledDateTime.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mCashierName.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mInvoiceNo.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print(mItemTitles.getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();

        String ProductsDetails;

        for (int i = 0; i < mPrinterSalesModel.getSalesProducts().size(); i++) {

            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Print("**voided".getBytes());

            ProductsDetails = GetSpecificlength(24, mPrinterSalesModel.getSalesProducts().get(i).getProductName()) + GetEmptySpace(3) + AddEmptySpaceLeft(4, mPrinterSalesModel.getSalesProducts().get(i).getQuanity()) + GetEmptySpace(6) + AddEmptySpaceLeft(10, FormatTwoDigitTwoDesimal(mPrinterSalesModel.getSalesProducts().get(i).getTotal()));

            LogUtil.e(TAG, "ProductsDetails  -         " + "**voided");
            LogUtil.e(TAG, "ProductsDetails  -         " + ProductsDetails);

            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Print(ProductsDetails.getBytes());
            mUsbPrinterPT80KMController.PrinterController_Linefeed();

            if (!TextUtils.isEmpty(mPrinterSalesModel.getSalesProducts().get(i).getModifierIngredient())) {
                LogUtil.e(TAG, "ProductsDetails  -         " + mPrinterSalesModel.getSalesProducts().get(i).getModifierIngredient());
                mUsbPrinterPT80KMController.PrinterController_Linefeed();
                mUsbPrinterPT80KMController.PrinterController_Print(mPrinterSalesModel.getSalesProducts().get(i).getModifierIngredient().getBytes());
            }
        }

//        LogUtil.e(TAG, "mTotalQty  -               " + mTotalDiscont);
        LogUtil.e(TAG, "mTotalQty  -               " + mTotalQty);
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Print("-----------------------------------------------".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Left();
        mUsbPrinterPT80KMController.PrinterController_Print(mTotalQty.getBytes());

//        mUsbPrinterPT80KMController.PrinterController_Linefeed();
//        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
//        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
//        mUsbPrinterPT80KMController.PrinterController_Set_Left();
//        mUsbPrinterPT80KMController.PrinterController_Print(mTotalDiscont.getBytes());

//        mUsbPrinterPT80KMController.PrinterController_Linefeed();
//        mUsbPrinterPT80KMController.PrinterController_Set_Center();
//        mUsbPrinterPT80KMController.PrinterController_Bitmap(mBarcodeGenerator.GenerateBarcode(mPrinterSalesModel.getInvoiceNo()));

        if (!TextUtils.isEmpty(mPrinterSalesModel.getStrSocialMedia())) {
            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
            mUsbPrinterPT80KMController.PrinterController_Set_Center();
            mUsbPrinterPT80KMController.PrinterController_Print(mPrinterSalesModel.getStrSocialMedia().getBytes());
        }
        if (mImagesCache.getImageFromWarehouse(CV.INSTANCE.getBARCODE_IMAGE_NAME()) != null) {
            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Set_Center();
            mUsbPrinterPT80KMController.PrinterController_Bitmap(mImagesCache.getImageFromWarehouse(CV.INSTANCE.getBARCODE_IMAGE_NAME()));
        }
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Bold();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print("ReCore".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Print("Point Of Sale".getBytes());

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();
        mUsbPrinterPT80KMController.PrinterController_Print("Thank you... visit again!".getBytes());


        /*mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Font_Normal_mode();
        mUsbPrinterPT80KMController.PrinterController_Set_Center();

        if (!TextUtils.isEmpty(mPrinterSalesModel.getInstagram())) {
            mUsbPrinterPT80KMController.PrinterController_Print(mPrinterSalesModel.getInstagram().getBytes());
        }
        if (!TextUtils.isEmpty(mPrinterSalesModel.getFacebook())) {
            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Print(mPrinterSalesModel.getFacebook().getBytes());
        }
        if (!TextUtils.isEmpty(mPrinterSalesModel.getTwitter())) {
            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Print(mPrinterSalesModel.getTwitter().getBytes());
        }
        if (!TextUtils.isEmpty(mPrinterSalesModel.getYoutube())) {
            mUsbPrinterPT80KMController.PrinterController_Linefeed();
            mUsbPrinterPT80KMController.PrinterController_Print(mPrinterSalesModel.getYoutube().getBytes());
        }*/

        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();
        mUsbPrinterPT80KMController.PrinterController_Linefeed();

        cutPrint();
    }

    public void cutPrint() {
        if (null != mUsbPrinterPT80KMController) {
            mUsbPrinterPT80KMController.PrinterController_Cut();
        }
    }

    private String FormatTwoDigitTwoDesimal(String value) {

        String dollarsign = value.substring(0, Math.min(value.length(), 1));
        String amount = value.substring(2, value.length());
        String AmountAndDollorsign = dollarsign + GetTwoDigitTwoDesimal(amount);

        return AmountAndDollorsign;
    }

    private String FormatFourDigitTwoDesimal(String value) {

        String dollarsign = value.substring(0, Math.min(value.length(), 1));
        String amount = value.substring(2, value.length() - 1);

        String AmountAndDollorsign = dollarsign + GetFourDigitTwoDesimal(amount);

        return AmountAndDollorsign;
    }

    private String FormatDigitTwoDesimal(String value) {
        String AmountAndDollorsign;
        String dollarsign = value.substring(0, Math.min(value.length(), 1));
        String amount = value.substring(1, value.length()).trim();

        if (amount.equals("0.0") || amount.equals("0") || amount.equals("0.00") || amount.equals(".00")) {
            AmountAndDollorsign = dollarsign + " 0.00";
        } else {

            AmountAndDollorsign = dollarsign + " " + Math.abs(Double.parseDouble(GetDigitTwoDesimal(amount)));
        }
        return AmountAndDollorsign;
    }

    private String GetOneDigitTwoDesimal(String value) {
        DecimalFormat df = new DecimalFormat("0.00");
        return df.format(Double.parseDouble(value));
    }

    private String GetTwoDigitTwoDesimal(String value) {
        DecimalFormat df = new DecimalFormat("00.00");
        return df.format(Double.parseDouble(value));
    }

    private String GetFourDigitTwoDesimal(String value) {
        DecimalFormat df = new DecimalFormat("0000.00");
        return df.format(Double.parseDouble(value));
    }

    private String GetDigitTwoDesimal(String value) {
        String output;
        if (value.equals("0.0") || value.equals("0") || value.equals("0.00")) {
            output = " 0.00";
        } else {
            DecimalFormat df = new DecimalFormat("#0.00");
            output = df.format(Double.parseDouble(value));
        }
        return output;
    }

    private String GetReasonLines(String reason, int lines) {

        String outputlines = reason;

        if (lines == 1) {
            outputlines = reason.substring(0, Math.min(reason.length(), 25));
            return GetSpecificlength(25, outputlines);
        } else if (lines == 2) {
            if (reason.length() > 25) {
                outputlines = reason.substring(25, Math.min(reason.length(), 50));
                return GetSpecificlength(25, outputlines);
            } else {
                return " ";
            }
        }

        return "";
    }

    private String GetRefundReasonLines(String reason, int lines) {

        String outputlines = reason;

        if (lines == 1) {
            outputlines = reason.substring(0, Math.min(reason.length(), 27));
            return GetSpecificlength(27, outputlines);
        } else if (lines == 2) {
            if (reason.length() > 27) {
                outputlines = reason.substring(27, Math.min(reason.length(), 54));
                return GetSpecificlength(27, outputlines);
            } else {
                return " ";
            }
        }

        return "";
    }

    private String GetDiscountRemarkLines(String mDiscountRemark, int lines) {

        String outputlines = mDiscountRemark;

        if (lines == 1) {
            outputlines = mDiscountRemark.substring(0, Math.min(mDiscountRemark.length(), 22));
            return GetSpecificlength(22, outputlines);
        } else if (lines == 2) {
            if (mDiscountRemark.length() > 22) {
                outputlines = mDiscountRemark.substring(22, Math.min(mDiscountRemark.length(), 44));
                return GetSpecificlength(22, outputlines);
            } else {
                return " ";
            }
        }

        return "";
    }

    private String GetEmptySpace(int no) {
        String RequireSpaceformat = "%" + no + "s";
        return String.format(RequireSpaceformat, " ");
    }

    private String GetSpecificlength(int no, String mValueString) {

        String RequireSpaceformat = "%." + no + "s";
        String OutputString;

        if (mValueString.length() > no) {

            OutputString = String.format(RequireSpaceformat, mValueString);

        } else {

            OutputString = AddEmptySpace(no, mValueString);
        }

        return OutputString;
    }

    private int CalculateRequiredSpace(String mValueString, String mValueString2) {

        int TotalCharacterSize = 47;

        int mstringlength = mValueString.length() + mValueString2.length();

        return TotalCharacterSize - mstringlength;
    }

    private String AddEmptySpace(int no, String mValueString) {

        String RequireSpaceformat = "%-" + no + "s";

        return String.format(RequireSpaceformat, mValueString);
    }

    private String AddEmptySpaceLeft(int no, String mValueString) {

        String RequireSpaceformat = "%" + no + "s";

        return String.format(RequireSpaceformat, mValueString);
    }

    public interface PrinterStatusChangeListener {
        void OnPrinterStatusChangeLister(final int status);
    }

    public String RemoveSpecialCharacter(String input) {
        String result = input.replaceAll("[-+.^:,/]", "");
        return result.trim();
    }

}
