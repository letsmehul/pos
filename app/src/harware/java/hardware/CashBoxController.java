package hardware;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.ehsm.recore.hardware.BBV2_CashboxController;

public class CashBoxController {

    public interface CashBoxStatusListener {
        void OnCashBoxOpen();

        void OnCashBoxError();
    }

    private static CashBoxController mInstance;
    private BBV2_CashboxController mCashBoxController;
    private Context mContext;

    public static CashBoxController getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new CashBoxController(context);
        }
        return mInstance;
    }

    private CashBoxController(Context context) {
        mContext = context;
        initBBV2CashBoxController();
    }

    private void initBBV2CashBoxController() {
        if (mCashBoxController == null) {
            mCashBoxController = BBV2_CashboxController.getInstance(mContext);
        }
    }

    public void openCashDrawer(final CashBoxStatusListener listener) {
        initBBV2CashBoxController();

        int flagOne = mCashBoxController.CashboxController_Controller(1);
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            listener.OnCashBoxError();
            e.printStackTrace();
        }

        int flagTwo = mCashBoxController.CashboxController_Controller(0);
        if (flagOne == -1) {
            listener.OnCashBoxError();
        } else if (flagTwo == 0) {
            listener.OnCashBoxOpen();
        }
    }

    public int isCashBoxOpen(){

        return mCashBoxController.GetCashboxControllerStatus();

    }



}
