package com.ehsm.recore.interfaces
interface ItemCallBack<T> {
    fun onItemClick(action:String, position: Int, orderTransModel: T)
}