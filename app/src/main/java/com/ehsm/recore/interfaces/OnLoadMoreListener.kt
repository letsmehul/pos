package com.ehsm.recore.interfaces

interface OnLoadMoreListener {
    fun onLoadMore()
}