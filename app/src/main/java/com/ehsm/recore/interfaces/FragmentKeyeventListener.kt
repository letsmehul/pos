package com.ehsm.recore.interfaces

import android.view.KeyEvent

interface FragmentKeyeventListener {

    fun onFragmentBarcodeResult(barcode: String)
}