package com.ehsm.recore.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ehsm.recore.R
import com.ehsm.recore.databinding.ListItemCategoryBinding
import com.ehsm.recore.model.CategoryModel

class CategoryListAdapter(
    private val mContext: AppCompatActivity,
    private val mList: List<CategoryModel>,
    private val clickListener: ItemClickCategory?
) : RecyclerView.Adapter<CategoryListAdapter.MyViewHolder>() {


    private var row_index = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(mContext)

        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater,
            R.layout.list_item_category,
            parent,
            false
        )
        return MyViewHolder(binding as ListItemCategoryBinding)
    }

    interface ItemClickCategory {
        fun listItemClickedCategory(position: Int, category_id: Int)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.mBinding.model = mList[position]


        holder.mBinding.layout.setOnClickListener {
            row_index = position
            notifyDataSetChanged()
            clickListener!!.listItemClickedCategory(
                position,
                mList.get(position).iProductCategoryID
            )
        }

        Glide.with(mContext)
            .load(mList.get(position).strProductCategoryImage)
            .placeholder(R.drawable.icon)
            .error(R.drawable.company)
            .into(holder.mBinding.ivCategory)

        if (row_index == position) {
            holder.mBinding.layout.background = mContext.resources.getDrawable(R.drawable.btn_bg)
            holder.mBinding.tvName.setTextColor(Color.parseColor("#ffffff"))
            holder.mBinding.layout.elevation = 5.0F
        } else {
            holder.mBinding.layout.background =
                mContext.resources.getDrawable(R.drawable.btn_background)
            holder.mBinding.tvName.setTextColor(Color.parseColor("#A9A9A9"))
            holder.mBinding.layout.elevation = 0.0F
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class MyViewHolder(internal val mBinding: ListItemCategoryBinding) :
        RecyclerView.ViewHolder(mBinding.root) {

        init {
            mBinding.root.setOnClickListener {

            }
        }
    }
}
