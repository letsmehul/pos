package com.ehsm.recore.adapter

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View.OnFocusChangeListener
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.ehsm.recore.R
import com.ehsm.recore.activities.ParentActivity
import com.ehsm.recore.databinding.ListItemSeletedRefund2Binding
import com.ehsm.recore.model.OrderProductsModel
import com.ehsm.recore.model.OrderTransModel
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CV


class RefundSelectedItemListAdapter2(
    private val mContext: Context,
    private val mList: MutableList<OrderProductsModel>,
    private val clickListener: ListItemChanged?

) : RecyclerView.Adapter<RefundSelectedItemListAdapter2.MyViewHolder>() {

    var list_temp =mList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(mContext)
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater,
            R.layout.list_item_seleted_refund2,
            parent,
            false
        )
        return MyViewHolder(binding as ListItemSeletedRefund2Binding)
    }

    interface ListItemChanged {
        fun listItemChanged(position: Int, orderTransModel: OrderProductsModel, qty: String)

    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.mBinding.model = mList[position]

        holder.mBinding.tvRate.setText(
            String.format(
                "%.2f",
                mList.get(position).dblPrice.toDouble()
            )
        )
        holder.mBinding.tvTax.setText(mList.get(position).dblItemTaxAmount)
        holder.mBinding.tvQtyLeft.setText(mList.get(position).dblItemQty)
        holder.mBinding.ivDelete.setOnClickListener {
            mList.remove(mList.get(position))

            notifyItemRemoved(position)
            notifyItemRangeChanged(position, mList.size)
        }
        var tax_per_item =
            mList.get(position).dblItemTaxAmount.toDouble() / mList.get(position).dblItemQty.toDouble()
        val qty = mList.get(position).dblItemQty.toDouble()
        holder.mBinding.edtQty.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                if (!s.toString().isEmpty()) {
                    if (holder.mBinding.edtQty.text.toString().toDouble() > qty) {
                        holder.mBinding.edtQty.setText("")
                    } else {
                        val remain_qty =
                            mList.get(position).dblItemQty.toDouble() - s.toString().toDouble()
                        holder.mBinding.tvQtyLeft.setText(remain_qty.toString())
                        holder.mBinding.tvTax.setText(
                            String.format(
                                "%.2f",
                                tax_per_item * s.toString().toDouble()
                            )
                        )
                        val amount =
                            mList.get(position).dblPrice.toDouble() * s.toString().toDouble()
                        holder.mBinding.tvAmount.setText(String.format("%.2f", amount))
                        mList.get(position).dblItemTaxAmount = holder.mBinding.tvTax.text.toString()
                        mList.get(position).dblAmount = holder.mBinding.tvAmount.text.toString()
                        //list_temp = mList
                       // list_temp.get(position).dblItemQty = s.toString()
                        var tt = 0.0
                        for (i in mList.indices) {
                            tt = tt + amount
                            Log.e("===  total 1 ===", "====" + tt)
                        }

                        clickListener!!.listItemChanged(
                            position,
                            mList.get(position),
                            s.toString()
                        )
                    }
                } else {
                    holder.mBinding.tvQtyLeft.setText(mList.get(position).dblItemQty)
                    holder.mBinding.tvTax.setText(mList.get(position).dblItemTaxAmount)
                    holder.mBinding.tvAmount.setText("00.00")

                    mList.get(position).dblItemTaxAmount = holder.mBinding.tvTax.text.toString()
                    mList.get(position).dblAmount = holder.mBinding.tvAmount.text.toString()
                  //  list_temp = mList
                 //   list_temp.get(position).dblItemQty = "0"
                    var tt = 0.0
                    for (i in mList.indices) {
                        tt = tt + mList.get(position).dblAmount.toDouble()
                        Log.e("===  total 2 ===", "====" + tt)
                    }

                    clickListener!!.listItemChanged(
                        position,
                        mList.get(position),
                        ""
                    )
                }

            }


            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })

    }

    override fun getItemCount(): Int {
        return mList.size
    }

    fun getDataset(): List<OrderProductsModel> {
        return list_temp
    }

    inner class MyViewHolder(internal val mBinding: ListItemSeletedRefund2Binding) :
        RecyclerView.ViewHolder(mBinding.root) {

        init {
            mBinding.root.setOnClickListener {

            }
        }
    }

}
