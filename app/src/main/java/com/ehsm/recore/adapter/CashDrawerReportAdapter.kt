package com.ehsm.recore.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.databinding.DataBindingUtil
import com.ehsm.recore.R
import com.ehsm.recore.databinding.ListItemCashDrawerReportBinding
import com.ehsm.recore.model.CashDrawerReportModel


class CashDrawerReportAdapter(private val mContext: Context, private val itemList: List<CashDrawerReportModel>) :
    BaseAdapter() {

    private val inflater: LayoutInflater

    init {
        inflater = LayoutInflater.from(mContext)
    }


    override fun getCount(): Int {
        return itemList.size
    }

    override fun getItem(i: Int): String {
        return itemList.get(i).id
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    private class MyViewHolder internal constructor(val binding: ListItemCashDrawerReportBinding) {
        var view: View

        init {
            this.view = binding.getRoot()
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val holder: MyViewHolder

        if (convertView == null) {
            val itemBinding = DataBindingUtil.inflate<ListItemCashDrawerReportBinding>(LayoutInflater.from(parent.context),
             R.layout.list_item_cash_drawer_report,
                parent,
                false
            )

            holder = MyViewHolder(itemBinding)
            holder.view = itemBinding.getRoot()
            holder.view.setTag(holder)
        } else {
            holder = convertView.tag as MyViewHolder
        }

        holder.binding.model = itemList.get(position)

        return holder.view
    }





}