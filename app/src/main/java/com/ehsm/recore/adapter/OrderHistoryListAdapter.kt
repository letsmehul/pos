package com.ehsm.recore.adapter

import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.ehsm.recore.R
import com.ehsm.recore.databinding.ListItemOrderHistoryBinding
import com.ehsm.recore.model.OrderHistoryModel
import com.ehsm.recore.utils.AppConstant
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CV
import com.ehsm.recore.utils.logger.LogUtil
import java.lang.Double.valueOf

class OrderHistoryListAdapter(
    private val mContext: Context,
    private val mList: List<OrderHistoryModel>,
    private val clickListener: ListItemClickedOrder?

) : RecyclerView.Adapter<OrderHistoryListAdapter.MyViewHolder>() {
    private var currencySymbol = "$"

    init {
        currencySymbol = CM.getSp(mContext, CV.CURRENCY_CODE, "") as String
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(mContext)
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater,
            R.layout.list_item_order_history,
            parent,
            false
        )
        return MyViewHolder(binding as ListItemOrderHistoryBinding)
    }

    fun removeAllChildViews(viewGroup: ViewGroup) {
        for (i in 0 until viewGroup.childCount) {
            val child = viewGroup.getChildAt(i)
            if (child is ViewGroup) {
                if (child is AdapterView<*>) {
                    viewGroup.removeView(child)
                    return
                }
                removeAllChildViews(child)
            } else {
                viewGroup.removeView(child)
            }
        }
    }
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val orderHistoryModel = mList.get(position)
        orderHistoryModel?.run {
            holder.mBinding.model = this
            holder.mBinding.currencySymbol = currencySymbol
            holder.mBinding.tvRefund.visibility = View.GONE

            if (this.status == AppConstant.ON_HOLD) {
                holder.mBinding.tvCreatedandModifieduser.visibility=View.VISIBLE
                holder.mBinding.tvStatus.setTextColor(mContext.resources.getColor(R.color.colorPrimary))
                holder.mBinding.tvNo.text = this.strInvoiceNumber
                Log.e("=====", "==== customer ===" + this.strFirstName)
                holder.mBinding.tvDate.text =
                    CM.convertDateFormate(
                        CV.WS_DATE_FORMATTE_AM_PM,
                        CV.DISPLAY_DATE_FORMATTE_AMPM,
                        this.datetimeCheckBusinessDate
                    )

                var isDiscountAppliedOnCart = false

                this.orderDiscounts?.let {
                    if (it.size > 0) {
                        isDiscountAppliedOnCart = true
                    } else {
                        isDiscountAppliedOnCart = false
                    }
                }
                var isDiscountAppliedOnItem = false
                for(i in this.orderProducts!!.indices){
                    if(!this.orderProducts!!.get(i).iDiscountId.equals("0")){
                        isDiscountAppliedOnItem = true
                        break
                    }
                }
                if (isDiscountAppliedOnItem || isDiscountAppliedOnCart) {
                    holder.mBinding.tvDiscount.visibility = View.VISIBLE
                    holder.mBinding.tvDiscount.text = mContext.getString(R.string.order_discount_applied)
                } else {
                    holder.mBinding.tvDiscount.visibility = View.GONE
                }
                if (!TextUtils.isEmpty(orderHistoryModel.createdByUser) && !TextUtils.isEmpty(orderHistoryModel.modifiedByUser))
                {
                    holder.mBinding.tvCreatedandModifieduser.text="Created From POS ID : ${orderHistoryModel.createdByUser} & Last Updated From POS ID : ${orderHistoryModel.modifiedByUser}"
                }else if(!TextUtils.isEmpty(orderHistoryModel.createdByUser)){
                    holder.mBinding.tvCreatedandModifieduser.text="Created From POS ID : ${orderHistoryModel.createdByUser}"
                }

                val amountNetTotal: String = orderHistoryModel.dblCheckNetTotal.substring(1, orderHistoryModel.dblCheckNetTotal.length).trim({ it <= ' ' })
                var cartTotal = amountNetTotal.toDouble() + orderHistoryModel.dblCheckTaxTotal.toDouble()
                holder.mBinding.tvAmount.setText(String.format("%s%s",currencySymbol, orderHistoryModel.dblCheckGrossTotal))

            } else {
                holder.mBinding.tvCreatedandModifieduser.visibility=View.GONE
                holder.mBinding.tvStatus.setTextColor(mContext.resources.getColor(R.color.colorGreen))
                holder.mBinding.tvNo.text = this.strInvoiceNumber
                holder.mBinding.tvDate.text = CM.convertDateFormate(
                    CV.WS_DATE_FORMATTE_AM_PM,
                    CV.DISPLAY_DATE_FORMATTE_AMPM,
                    this.datetimeCheckBusinessDate
                )


                var total_refund_amount : String? ="0"
                this.orderRefund?.let {

                    if (it.size > 0) {

                        this.orderPayments?.let {
                            for (i in orderHistoryModel.orderRefund!!)
                            {
                                total_refund_amount= (total_refund_amount!!.toDouble()+i.dblRefundAmount.toDouble()).toString()

                            }
                            val threedigitamout=String.format("%.3f", total_refund_amount!!.toDouble())
                            total_refund_amount=String.format("%.2f", threedigitamout!!.toDouble())
                            LogUtil.e("total_refund_amount",total_refund_amount)
                            if(it.get(0).strPaymentMethod.equals(CV.PAYMENT_TYPE_CASH)){
                                if(orderHistoryModel.dblCheckGrossTotal.equals(total_refund_amount)){
                                    holder.mBinding.tvRefund.text = mContext.getString(R.string.order_refunded)
                                }else{
                                    holder.mBinding.tvRefund.text = mContext.getString(R.string.order_partially_refunded)
                                }
                            }else{
                                if(orderHistoryModel.dblCheckGrossTotal.equals(total_refund_amount)){
                                    holder.mBinding.tvRefund.text = mContext.getString(R.string.order_refunded)
                                }else{
                                    holder.mBinding.tvRefund.text = mContext.getString(R.string.order_partially_refunded)
                                }
                            }
                        }

                        holder.mBinding.tvRefund.visibility = View.VISIBLE
                    } else {
                        holder.mBinding.tvRefund.visibility = View.GONE
                    }
                }
                var isDiscountAppliedOnCart = false

                this.orderDiscounts?.let {
                    if (it.size > 0) {
                        isDiscountAppliedOnCart = true
                    } else {
                        isDiscountAppliedOnCart = false
                    }
                }
                var isDiscountAppliedOnItem = false
                for(i in this.orderProducts!!.indices){
                    if(!this.orderProducts!!.get(i).iDiscountId.equals("0")){
                        isDiscountAppliedOnItem = true
                        break
                    }
                }
                    if (isDiscountAppliedOnItem || isDiscountAppliedOnCart) {
                        holder.mBinding.tvDiscount.visibility = View.VISIBLE
                        holder.mBinding.tvDiscount.text = mContext.getString(R.string.order_discount_applied)
                    } else {
                        holder.mBinding.tvDiscount.visibility = View.GONE
                    }
                if(orderHistoryModel.orderRefund!!.isEmpty()){
                    holder.mBinding.tvAmount.setText(String.format("%s%s",currencySymbol, orderHistoryModel.dblCheckGrossTotal))
                }else{
                    holder.mBinding.tvAmount.setText(String.format("%s%s",currencySymbol, total_refund_amount))
                }
            }



            holder.mBinding.tvPaymentMode.visibility =View.GONE
            this.orderPayments?.let {
                    list ->
                if(list.isNotEmpty()) {
                    holder.mBinding.tvPaymentMode.visibility = View.VISIBLE
                    if (list.size > 1) {
                        holder.mBinding.tvPaymentMode.text =
                            " ${holder.mBinding.root.context.getString(R.string.by)} ${mContext.getString(R.string.split_payment)}"
                    } else {
                        val last4Digit = list[0].strPaymentCardLast4
                        if (TextUtils.isEmpty(last4Digit)) {
                            val paymentMode = list[0].strPaymentMethod
                            holder.mBinding.tvPaymentMode.text =
                                " ${holder.mBinding.root.context.getString(R.string.by)} ${paymentMode.toLowerCase()}"
                        } else {
                            val paymentMode = list[0].strPaymentMethod
                            holder.mBinding.tvPaymentMode.text =
                                " ${holder.mBinding.root.context.getString(R.string.by)} ${mContext.getString(R.string.card)} #$last4Digit"
                        }
                    }
                }

            }
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }


    interface ListItemClickedOrder {
        fun listItemClickedOrder(position: Int, orderHistoryModel: OrderHistoryModel)
    }

    inner class MyViewHolder(internal val mBinding: ListItemOrderHistoryBinding) :
        RecyclerView.ViewHolder(mBinding.root) {
        init {
            mBinding.root.setOnClickListener {
                if (!mList[adapterPosition].status.equals(AppConstant.HistoryStatus.INPROCESSSTATUS.value)) {
                    clickListener!!.listItemClickedOrder(
                        adapterPosition,
                        mList[adapterPosition]
                    )
                }
            }
        }
    }

}
