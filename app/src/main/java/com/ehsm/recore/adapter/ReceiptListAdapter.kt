package com.ehsm.recore.adapter

import android.content.*
import android.graphics.Color
import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.ehsm.recore.R
import com.ehsm.recore.activities.ReceiptActivity
import com.ehsm.recore.databinding.ListItemReceiptBinding
import com.ehsm.recore.model.ReceiptModel

class ReceiptListAdapter(
    private val mContext: Context,
    private val mList: List<ReceiptModel>,
    private val clickListener: ItemClickCategory?
) : RecyclerView.Adapter<ReceiptListAdapter.MyViewHolder>() {


    private var row_index = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(mContext)

        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater,
            R.layout.list_item_receipt,
            parent,
            false
        )
        return MyViewHolder(binding as ListItemReceiptBinding)
    }

    interface ItemClickCategory {
        fun listItemClickedCategory(position: Int, category_id: String)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.mBinding.model = mList[position]
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class MyViewHolder(internal val mBinding: ListItemReceiptBinding) :
        RecyclerView.ViewHolder(mBinding.root) {

        init {
            mBinding.root.setOnClickListener {
                clickListener!!.listItemClickedCategory(position, mList.get(position).receipt_id)
            }
        }
    }


}
