package com.ehsm.recore.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ehsm.recore.R
import com.ehsm.recore.interfaces.ItemCallBack
import com.ehsm.recore.model.DiscountMaster
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CV
import kotlinx.android.synthetic.main.list_item_discount.view.*

class DiscountListAdapter(
    private val itemsCells: List<DiscountMaster>,
    private val itemCallBack: ItemCallBack<DiscountMaster>?
) :
    RecyclerView.Adapter<DiscountListAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_item_discount, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return itemsCells.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // Add data to cells
        holder.itemView.tvName.text = itemsCells.get(position).name
        if(itemsCells.get(position).strType.equals(CV.PERCENT)){
            holder.itemView.tvValue.text = itemsCells.get(position).dblValue.toString() + " %"
        }else{
            holder.itemView.tvValue.text = CM.getSp(holder.itemView.context, CV.CURRENCY_CODE, "") as String + " "+ itemsCells[position].dblValue.toString()
        }
        holder.itemView.setOnClickListener {
            itemCallBack?.onItemClick("", position, itemsCells.get(position))
        }
    }

}