package com.ehsm.recore.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.ehsm.recore.R
import com.ehsm.recore.databinding.ItemModifierBinding
import com.ehsm.recore.model.ModifierMasterModel
import com.ehsm.recore.utils.CV

class ModifierListAdapter(
    private val mContext: Context,
    private val mList: List<ModifierMasterModel>,
    private val clickListener: ItemClickModifier?
) : RecyclerView.Adapter<ModifierListAdapter.MyViewHolder>() {


    private var row_index = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(mContext)

        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater,
            R.layout.item_modifier,
            parent,
            false
        )
        return MyViewHolder(binding as ItemModifierBinding)
    }

    interface ItemClickModifier {
        fun listItemClickedModifier(position: Int, modifierId: String)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.mBinding.model = mList[position]

        if(mList[position].strIsRequired.equals(CV.REQUIRED)){
            holder.mBinding.tvName.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(mContext,R.drawable.ic_star), null)
        }else{
            holder.mBinding.tvName.setCompoundDrawablesWithIntrinsicBounds(null, null,null, null)
        }

        holder.mBinding.layout.setOnClickListener {
            if (mList.size>0) {
                if (!mList.get(position).strModifierGroupName.equals("")) {
                    row_index = position
                    notifyDataSetChanged()
                    clickListener!!.listItemClickedModifier(
                        position,
                        mList.get(position).iModifierGroupId.toString()
                    )
                }
            }

        }

        if (row_index == position && !mList.get(position).strModifierGroupName.equals("")) {
            holder.mBinding.layout.background = mContext.resources.getDrawable(R.drawable.btn_stroke)
            holder.mBinding.tvName.setTextColor(Color.parseColor("#FA7F46"))
        } else {
            holder.mBinding.layout.background = mContext.resources.getDrawable(R.drawable.btn_bg)
            holder.mBinding.tvName.setTextColor(Color.parseColor("#ffffff"))
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class MyViewHolder(internal val mBinding: ItemModifierBinding) :
        RecyclerView.ViewHolder(mBinding.root) {

        init {
            mBinding.root.setOnClickListener {

            }
        }
    }

}
