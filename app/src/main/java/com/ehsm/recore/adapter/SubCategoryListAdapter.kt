package com.ehsm.recore.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.ehsm.recore.R
import com.ehsm.recore.databinding.ListItemSubCategoryBinding
import com.ehsm.recore.model.SubCategoryModel

class SubCategoryListAdapter(
    private val mContext: AppCompatActivity,
    private val mList: List<SubCategoryModel>,
    private val clickListener: ItemClickSubCategory?
) : RecyclerView.Adapter<SubCategoryListAdapter.MyViewHolder>() {


    private var row_index = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(mContext)

        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater,
            R.layout.list_item_sub_category,
            parent,
            false
        )
        return MyViewHolder(binding as ListItemSubCategoryBinding)
    }

    interface ItemClickSubCategory {
        fun listItemClickedSubCategory(position: Int, category_id: Int)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.mBinding.model = mList[position]


        holder.mBinding.layout.setOnClickListener {
            row_index = position
            notifyDataSetChanged()
            clickListener!!.listItemClickedSubCategory(position, mList.get(position).iProductCategoryID)

        }
        if (row_index == position) {
            holder.mBinding.layout.alpha = 1.0F
        } else {
            holder.mBinding.layout.alpha = 0.5F

        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class MyViewHolder(internal val mBinding: ListItemSubCategoryBinding) :
        RecyclerView.ViewHolder(mBinding.root) {

        init {
            mBinding.root.setOnClickListener {
            }
        }
    }

}
