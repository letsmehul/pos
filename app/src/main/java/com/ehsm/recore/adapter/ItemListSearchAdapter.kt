package com.ehsm.recore.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ehsm.recore.R
import com.ehsm.recore.model.ItemModel
import kotlinx.android.synthetic.main.list_item_search.view.*


class ItemListSearchAdapter(
    private val mContext: Context,
    private val mList :ArrayList<ItemModel>,
    private val clickListener: ListItemClickedItem?
) : RecyclerView.Adapter<ItemListSearchAdapter.MyViewHolder>() {

    private val products =ArrayList<ItemModel>()

    init {
        products.addAll(mList)
    }

   override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
       val inflater = LayoutInflater.from(parent.context)
       val view = inflater.inflate(R.layout.list_item_search, parent, false)
//       val rootView: View =
//           LayoutInflater.from(parent.context).inflate(R.layout.list_item_search, parent, false)
//       val lp = RelativeLayout.LayoutParams(
//           ViewGroup.LayoutParams.MATCH_PARENT,
//           ViewGroup.LayoutParams.WRAP_CONTENT
//       )
//       rootView.layoutParams = lp

//       rootView.requestLayout()
//       return RecyclerViewHolder(rootView)
       return MyViewHolder(view)
   }

   /* */

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) = holder.bind(products[position])

   /* override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        //holder.mBinding.model = mList[position]
        val itemModel =products[position]
        itemModel?.let {product->
              Glide.with(mContext)
                .load(product.strProductImage)
                .placeholder(R.drawable.icon)
                .error(R.drawable.company)
                .into(holder.mBinding.ivProductImage)
            holder.mBinding.tvProduct.text = "Item : "+ product.strProductName
            holder.mBinding.tvRate.text = "Price : "+ product.dblRetailPrice
            holder.mBinding.tvCategory.text = "Category : "+ product.strProductCategoryName + "/ Sub category : "+ product.subCategoryName+ " /code : "+product.strProductCode
        }
    }*/
   fun setData(list: ArrayList<ItemModel>) {
       this.products.addAll(list)
       notifyDataSetChanged()
   }

    fun addData(list: ArrayList<ItemModel>) {
        val diffCallback = ProductDiffCallBack(products,list)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        this.products.clear()
        this.products.addAll(list)
        diffResult.dispatchUpdatesTo(this)

    }
    override fun getItemCount(): Int {
        return products.size
    }

    interface ListItemClickedItem {
        fun listItemClickedItem(position: Int, category_id: Int)

        fun listItemClickedItem(itemModel : ItemModel)
    }

    /*inner class MyViewHolder(internal val mBinding: ListItemSearchBinding) :
        RecyclerView.ViewHolder(mBinding.root) {

        init {
            mBinding.root.setOnClickListener {
                  clickListener?.let {
                    it.listItemClickedItem(products[adapterPosition])
                }
            }
        }
    }*/

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(product: ItemModel) {

            with(itemView) {


               Glide.with(mContext)
                    .load(product.strProductImage)
                    .placeholder(R.drawable.icon)
                    .error(R.drawable.company)
                    .into(ivProductImage)
                tvProduct.text = "Item : "+ product.strProductName
                tvRate.text = "Price : "+ product.dblRetailPrice
//                tvCategory.text = "Category : "+ product.strProductCategoryName + "/ Sub category : "+ product.subCategoryName+ " /code : "+product.strProductCode
                tvCategory.text = String.format(mContext.getString(R.string.format_product_desc),product.strProductCategoryName,product.subCategoryName,product.strProductCode ?: "")

            }
        }

        init {
            itemView.setOnClickListener {
                clickListener?.let {
                    it.listItemClickedItem(products[adapterPosition])
                }
            }
        }
/*        private fun textForDiscount(discount: Discount): String {
            return when (discount) {
                is Discount.Percentage -> "discount ${discount.value}%"
                is Discount.Cash -> "discount ${discount.value}$"
                Discount.Free -> "free item"
            }
        }*/
    }

   /* override fun onBindViewHolder(holder: MyViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isNotEmpty()) {
            val item: ItemModel = products[position]
            for (payload in payloads) {
//                holder.bind(item)

            }
        } else {
            // in this case regular onBindViewHolder will be called
            super.onBindViewHolder(holder, position, payloads)
        }
    }*/
}
