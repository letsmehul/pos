package com.ehsm.recore.adapter

import android.content.Context
import android.graphics.Color
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.ehsm.recore.R
import com.ehsm.recore.databinding.ItemIngredientBinding
import com.ehsm.recore.model.IngredientMasterModel
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CV
import kotlinx.android.synthetic.main.item_ingredient.view.*

class IngredientListAdapter(
    private val mContext: Context,
    private val mList: List<IngredientMasterModel>,
    private val clickListener: ItemClickIngredient?
) : RecyclerView.Adapter<IngredientListAdapter.MyViewHolder>() {

    private var row_index = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(mContext)

        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater,
            R.layout.item_ingredient,
            parent,
            false
        )
        return MyViewHolder(binding as ItemIngredientBinding)
    }

    interface ItemClickIngredient {
        fun listItemClickedIngredient(position: Int, ingredientId: String)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.mBinding.model = mList[position]
        if (!TextUtils.isEmpty(mList[position].dblOptionPrice)) {
            val currency =
                CM.getSp(mContext, CV.CURRENCY_CODE, "") as String + mList[position].dblOptionPrice
            holder.mBinding.tvPrice.text = currency
        }

        if (mList[position].isIngredientHighlited) {
            holder.mBinding.layout.background =
                mContext.resources.getDrawable(R.drawable.btn_selected_premodifire)
            holder.mBinding.tvName.setTextColor(ContextCompat.getColor(mContext,R.color.colorAccent))
            holder.mBinding.tvPrice.setTextColor(ContextCompat.getColor(mContext,R.color.colorAccent))
        } else {
            holder.mBinding.layout.background =
                mContext.resources.getDrawable(R.drawable.btn_background_yellow)
            holder.mBinding.tvName.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite))
            holder.mBinding.tvPrice.setTextColor(ContextCompat.getColor(mContext,R.color.colorWhite))
        }
        /*if (row_index == position) {
            holder.mBinding.layout.background =
                mContext.resources.getDrawable(R.drawable.btn_stroke_yellow)
            holder.mBinding.tvName.setTextColor(Color.parseColor("#ffce04"))
            holder.mBinding.tvPrice.setTextColor(Color.parseColor("#ffce04"))
        } else {
            holder.mBinding.layout.background =
                mContext.resources.getDrawable(R.drawable.btn_background_yellow)
            holder.mBinding.tvName.setTextColor(Color.parseColor("#ffffff"))
            holder.mBinding.tvPrice.setTextColor(Color.parseColor("#ffffff"))
        }

        if (mList.get(position).isSelected) {
            holder.mBinding.layout.background =
                mContext.resources.getDrawable(R.drawable.btn_stroke_yellow)
            holder.mBinding.tvName.setTextColor(Color.parseColor("#ffce04"))
            holder.mBinding.tvPrice.setTextColor(Color.parseColor("#ffce04"))
        } else {
            holder.mBinding.layout.background =
                mContext.resources.getDrawable(R.drawable.btn_background_yellow)
            holder.mBinding.tvName.setTextColor(Color.parseColor("#ffffff"))
            holder.mBinding.tvPrice.setTextColor(Color.parseColor("#ffffff"))
        }*/

        holder.mBinding.layout.setOnClickListener {
            row_index = position
            //   mList.get(position).isSelected = true
            //   notifyDataSetChanged()
            clickListener!!.listItemClickedIngredient(
                position,
                mList.get(position).iModifierGroupOptionId
            )
        }

    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class MyViewHolder(internal val mBinding: ItemIngredientBinding) :
        RecyclerView.ViewHolder(mBinding.root) {

        init {
            mBinding.root.setOnClickListener {

            }
        }
    }

}
