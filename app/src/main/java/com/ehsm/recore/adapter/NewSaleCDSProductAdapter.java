package com.ehsm.recore.adapter;


import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ehsm.recore.R;
import com.ehsm.recore.model.PrinterSalesProductsModel;
import com.ehsm.recore.utils.AppConstant;
import com.ehsm.recore.utils.CV;

import java.util.List;

public class NewSaleCDSProductAdapter  extends RecyclerView.Adapter<NewSaleCDSProductAdapter.MyViewHolder> {

    private List<PrinterSalesProductsModel> moviesList;
    private String from;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mProductName,mPrductNameBig, mProductPrice, mProductQty, mProductTotal, mModifierIngredient, mProductDiscount, mDiscountRemark,minstruction;
        public LinearLayout mContainerModifierIngredient, mDiscountRemarkContainer,mcontainerinstruction;

        public MyViewHolder(View view) {
            super(view);
            mProductName = (TextView) view.findViewById(R.id.tv_itemname);
            mPrductNameBig = (TextView) view.findViewById(R.id.tv_itemnameBig);
            mProductQty = (TextView) view.findViewById(R.id.tv_itemqty);
            mProductPrice = (TextView) view.findViewById(R.id.tv_itemprice);

            mProductTotal = (TextView) view.findViewById(R.id.tv_itemtotal);

            mContainerModifierIngredient = (LinearLayout) view.findViewById(R.id.ll_modifierIngredient);
            mModifierIngredient = (TextView) view.findViewById(R.id.tv_item_modifierIngredient);
            mProductDiscount = (TextView) view.findViewById(R.id.tv_itemdiscount);
            mDiscountRemarkContainer = (LinearLayout) view.findViewById(R.id.ll_discountremark);
            mDiscountRemark = (TextView) view.findViewById(R.id.tv_itemdiscountremark);
            minstruction=(TextView) view.findViewById(R.id.tv_instruction);
            mcontainerinstruction=(LinearLayout) view.findViewById(R.id.ll_instruction);
        }
    }


    public NewSaleCDSProductAdapter(List<PrinterSalesProductsModel> moviesList, String from) {
        this.moviesList = moviesList;
        this.from = from;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_newsale_cds, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        PrinterSalesProductsModel mPrinterSalesProductsModel = moviesList.get(position);
        if(mPrinterSalesProductsModel.getProductName().length()<31){
            holder.mProductName.setText(mPrinterSalesProductsModel.getProductName());
        }else{
            holder.mProductName.setVisibility(View.INVISIBLE);
            holder.mPrductNameBig.setVisibility(View.VISIBLE);
            holder.mPrductNameBig.setText(mPrinterSalesProductsModel.getProductName());
        }
        holder.mProductPrice.setText(mPrinterSalesProductsModel.getRate());
        holder.mProductQty.setText(mPrinterSalesProductsModel.getQuanity());
        holder.mProductDiscount.setText(mPrinterSalesProductsModel.getDiscount());

        holder.mProductTotal.setText(mPrinterSalesProductsModel.getTotal());


        if(TextUtils.isEmpty(mPrinterSalesProductsModel.getDiscountRemark())){

            holder.mDiscountRemarkContainer.setVisibility(View.GONE);
        }else {
            Log.e("=====","=== from ==="+from);
            if(from.equals(AppConstant.NEW_SALE)){
                holder.mDiscountRemark.setText(mPrinterSalesProductsModel.getDiscountRemark()+ CV.APPLIED_SUCCESSFULLY);
            }else {
                holder.mDiscountRemark.setText(mPrinterSalesProductsModel.getDiscountRemark()+ CV.WAS_APPLIED);
            }
            holder.mDiscountRemarkContainer.setVisibility(View.VISIBLE);
        }

        if(TextUtils.isEmpty(mPrinterSalesProductsModel.getModifierIngredient())){
            holder.mContainerModifierIngredient.setVisibility(View.GONE);
            holder.mModifierIngredient.setText("");
        }else {
            holder.mContainerModifierIngredient.setVisibility(View.VISIBLE);
            holder.mModifierIngredient.setText(mPrinterSalesProductsModel.getModifierIngredient().trim());
        }
        if(TextUtils.isEmpty(mPrinterSalesProductsModel.getInstruction())){
            holder.mcontainerinstruction.setVisibility(View.GONE);
            holder.minstruction.setText("");
        }else {
            holder.mcontainerinstruction.setVisibility(View.VISIBLE);
            holder.minstruction.setText(mPrinterSalesProductsModel.getInstruction().trim());
        }
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}