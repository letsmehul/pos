package com.ehsm.recore.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.ehsm.recore.R
import com.ehsm.recore.databinding.ListItemAllRefundBinding
import com.ehsm.recore.fragments.NewSalesFragment
import com.ehsm.recore.model.OrderTransModel
import com.ehsm.recore.utils.CV
import com.ehsm.recore.utils.NewSaleFragmentCommon
import java.util.ArrayList


class RefundAllItemListAdapter(
    private val mContext: Context,
    private val mList: MutableList<OrderTransModel>,
    private val clickListener: ListItemClicked?,
    private val fragment: NewSalesFragment


) : RecyclerView.Adapter<RefundAllItemListAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(mContext)

        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater,
            R.layout.list_item_all_refund,
            parent,
            false
        )
        return MyViewHolder(binding as ListItemAllRefundBinding)
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.mBinding.model = mList[position]
        if(mList.get(position).itemName.length<=13){
            holder.mBinding.tvUserName.setText(mList.get(position).itemName)
        }else{
            holder.mBinding.tvUserName.visibility=View.INVISIBLE
            holder.mBinding.tvUserNameBig.visibility=View.VISIBLE
            holder.mBinding.tvUserNameBig.text=mList.get(position).itemName
        }
        holder.mBinding.tvQty.setText(mList.get(position).itemQty.toString())
        holder.mBinding.tvQtyLeft.setText(mList.get(position).itemQtyLeft.toString())
        holder.mBinding.tvRate.setText(String.format("%.2f", mList.get(position).itemRate))

        if (!mList.get(position).ingredientName.isNullOrEmpty()) {
            holder.mBinding.tvModifierIngredient.visibility = View.VISIBLE
            holder.mBinding.tvModifierIngredient.setText(mList.get(position).ingredientName)
        } else {
            holder.mBinding.tvModifierIngredient.visibility = View.GONE
        }

        if (!mList.get(position).discountRemark.isNullOrEmpty()) {
            holder.mBinding.tvDiscountRemark.visibility = View.VISIBLE
            holder.mBinding.tvDiscountRemark.setText(mList[position].discountRemark + CV.WAS_APPLIED)
        } else {
            holder.mBinding.tvDiscountRemark.visibility = View.GONE
        }

        val itemQty = mList[position].itemQty
        val itemRate = mList[position].itemRate
        var itemPrice = 0.0
        var discountAmount = 0.0
        if (mList[position].itemQtyLeft > 0) {
            itemPrice = mList[position].itemQtyLeft * itemRate
            discountAmount =
                mList[position].itemQtyLeft * (String.format("%.2f", mList[position].discountAmount)
                    .toDouble() / itemQty)
        } else {
            itemPrice = itemQty * itemRate
            discountAmount = String.format("%.2f", mList[position].discountAmount).toDouble()
        }


        var totalAmount = itemPrice - discountAmount
        val taxRate = mList[position].taxPercentage.toDouble()
        var taxAmount = 0.0
        /*if (discountAmount == 0.0) {
            taxAmount = fragment.getTaxAmountAppliedDiscount(mList[position])
            taxAmount = String.format("%.2f", taxAmount).toDouble()
            if (mList[position].itemQtyLeft > 0) {
                taxAmount = (taxAmount * mList[position].itemQtyLeft) / itemQty
            } else {
                taxAmount = taxAmount
            }
            if (taxAmount == 0.0) {
                taxAmount = String.format("%.2f", totalAmount / 100.0f * taxRate).toDouble()
            }
        } else {
            taxAmount = String.format("%.2f", totalAmount / 100.0f * taxRate).toDouble()
        }*/

        if (mList[position].iDiscountId != 0) {
            //taxAmount = String.format("%.2f", totalAmount / 100.0f * taxRate).toDouble()
            taxAmount = NewSaleFragmentCommon.taxAmountCalculation(mList[position], discountAmount)
        } else {
            taxAmount = fragment.getTaxAmountAppliedDiscount(mList[position])
            taxAmount = String.format("%.2f", taxAmount).toDouble()
        }
        if (mList[position].itemQtyLeft > 0) {
            taxAmount = (taxAmount * mList[position].itemQtyLeft) / itemQty
        } else {
            taxAmount = taxAmount
        }

        holder.mBinding.tvTax.setText(String.format("%.2f", taxAmount))
        holder.mBinding.tvDiscount.setText(String.format("%.2f", discountAmount))
        holder.mBinding.tvAmount.setText(String.format("%.2f", totalAmount + taxAmount))



        if (fragment.checkIfExist(mList.get(position))) {
            holder.mBinding.root.setBackgroundColor(mContext.resources.getColor(R.color.colorGray))
        } else {
            if (mList.get(position).isRefunded) {
                holder.mBinding.root.setBackgroundColor(mContext.resources.getColor(R.color.colorGray))
            } else {
                holder.mBinding.root.setBackgroundColor(mContext.resources.getColor(R.color.colorWhite))
            }
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    interface ListItemClicked {
        fun listItemClicked(position: Int, orderTransModel: OrderTransModel)

    }


    inner class MyViewHolder(internal val mBinding: ListItemAllRefundBinding) :
        RecyclerView.ViewHolder(mBinding.root) {

        init {
            mBinding.root.setOnClickListener {
                /*remove highlighted color while select on item*/
                mBinding.root.setBackgroundColor(mContext.resources.getColor(R.color.colorGray))
                if (!mList.get(adapterPosition).isRefunded) {
                    clickListener!!.listItemClicked(
                        adapterPosition,
                        mList.get(adapterPosition)
                    )
                }
            }
        }
    }

    fun updateData(list: ArrayList<OrderTransModel>) {
        var temp = ArrayList<OrderTransModel>()
        temp.addAll(list)
        mList.clear()
        mList.addAll(temp)
        notifyDataSetChanged()
    }
}
