package com.ehsm.recore.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.ehsm.recore.R
import com.ehsm.recore.databinding.ListItemItemBinding
import com.ehsm.recore.databinding.ListItemSpinnerBinding
import com.ehsm.recore.model.ItemModel
import com.ehsm.recore.model.StateModel


class StateListAdapter(private val mContext: Context, private val itemList: List<StateModel>) :
    BaseAdapter() {

    private val inflater: LayoutInflater

    init {
        inflater = LayoutInflater.from(mContext)
    }


    override fun getCount(): Int {
        return itemList.size
    }

    override fun getItem(i: Int): String {
        return itemList.get(i).strStateName
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    private class MyViewHolder internal constructor(val binding: ListItemSpinnerBinding) {
        var view: View

        init {
            this.view = binding.getRoot()
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val holder: MyViewHolder

        if (convertView == null) {
            val itemBinding = DataBindingUtil.inflate<ListItemSpinnerBinding>(LayoutInflater.from(parent.context),
             R.layout.list_item_spinner,
                parent,
                false
            )

            holder = MyViewHolder(itemBinding)
            holder.view = itemBinding.getRoot()
            holder.view.setTag(holder)
        } else {
            holder = convertView.tag as MyViewHolder
        }
       // holder.binding.model = itemList.get(position)
        holder.binding.tvName.setText(itemList.get(position).strStateName)

        return holder.view
    }





}