package com.ehsm.recore.adapter

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.ehsm.recore.R
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.databinding.ListItemCartBinding
import com.ehsm.recore.interfaces.ItemCallBack

import com.ehsm.recore.model.DiscountGeneric
import com.ehsm.recore.model.OrderTransModel
import com.ehsm.recore.utils.*
import java.lang.Exception


class OrderItemListAdapter(
    private val mContext: Context,
    private val mDatabase: RecoreDB,
    private var mList: MutableList<OrderTransModel>,
    private val itemCallBack: ItemCallBack<OrderTransModel>,
    private val from: String,
    private val orderId: String

) : RecyclerView.Adapter<OrderItemListAdapter.MyViewHolder>() {

    var view_holder: MyViewHolder? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(mContext)

        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater,
            R.layout.list_item_cart,
            parent,
            false
        )
        return MyViewHolder(binding as ListItemCartBinding)
    }

    private fun formatAmount(amount: Double) = "${String.format("%.2f", (amount))}"

    private fun formatAmount(amount: Float) = "${String.format("%.2f", (amount))}"

    override fun onBindViewHolder(holder: OrderItemListAdapter.MyViewHolder, position: Int) {
        view_holder = holder
        val transModel = mList[position]
        transModel?.run {

            holder.mBinding.model = transModel
            holder.mBinding.executePendingBindings()
            var ingredient_name = ""
            // holder.mBinding.tvModifier.setText(this.ingredientName)
//            if(!this.ingredientName.equals("") && this.ingredientName.contains(CV.LITE) &&  this.ingredientName.contains(CV.ON_SIDE)){
//                val before_onside = this.ingredientName.split(CV.ON_SIDE).get(0)
//                val after_onside = this.ingredientName.split(CV.ON_SIDE).get(1)
//
//                val before_lite = before_onside.split(CV.LITE).get(0)
//                val after_lite = before_onside.split(CV.LITE).get(1)
//                ingredient_name = before_onside + "|" + before_lite + CV.ON_SIDE + " : " + CV.LITE +after_lite
//
//            }else if(!this.ingredientName.equals("") && this.ingredientName.contains(CV.ON_SIDE)){
//                val before_onside = this.ingredientName.split(CV.ON_SIDE).get(0)
//                val after_onside = this.ingredientName.split(CV.ON_SIDE).get(1)
//                ingredient_name = after_onside + "|" + before_onside.replace(":","") + " : "+CV.ON_SIDE
//            }else{
//                ingredient_name = this.ingredientName
//            }


            holder.mBinding.tvModifier.setText(this.ingredientName)
            if (from == AppConstant.REFUND) {
                holder.mBinding.ivUpArrow.visibility = View.INVISIBLE
                holder.mBinding.ivDownArrow.visibility = View.INVISIBLE
                holder.mBinding.ivDelete.visibility = View.INVISIBLE
                holder.mBinding.tvQty.isEnabled=false
                if (this.isRefunded) {
                    holder.mBinding.root.setBackgroundColor(mContext.resources.getColor(R.color.colorGray))
                }
            }
            holder.mBinding.tvPrice.setText("@" + formatAmount(this.itemRate))
            holder.mBinding.tvName.text = this.itemName
            if (!TextUtils.isEmpty(this.strInstruction)) {
                holder.mBinding.tvinstruction.text = this.strInstruction
                holder.mBinding.tvinstruction.visibility=View.VISIBLE
            }
            else{
                holder.mBinding.tvinstruction.visibility=View.GONE
            }

            val itemQty = transModel.itemQty
            val itemRate = transModel.itemRate
            var itemPrice = itemQty * itemRate
            if(transModel.tIsOpenDiscount == 1 && transModel.iDiscountId <= 0 && CM.getSp(mContext,CV.EMP_OPEN_DISCOUNT, 0) == 1){
                holder.mBinding.ivOpenDiscount.visibility = View.VISIBLE
            }else{
                holder.mBinding.ivOpenDiscount.visibility = View.GONE
            }
            holder.mBinding.tvAmount.text = formatAmount(itemPrice)
            if (this.taxPercentage.isNullOrEmpty()) {
                this.taxPercentage = "0.0"
            }
            val tax_amount = (this.itemRate * this.taxPercentage.toDouble() / 100)
            holder.mBinding.tvTax.text = formatAmount(tax_amount)
            holder.mBinding.tvQty.text = this.itemQty.toString()
            holder.mBinding.tvQty.setOnClickListener {
                try {
                    itemCallBack.onItemClick("manually", position, mList[position])
                }catch (e: Exception) {
                    e.printStackTrace()
                }
            }


            holder.mBinding.cbDiscountAppliedOnItem.hide()
            holder.mBinding.tvDiscount.text = "0.0"
            this.discountAmount.let {
                if (it > 0) {
                    holder.mBinding.cbDiscountAppliedOnItem.show()
                    holder.mBinding.tvDiscount.text = formatAmount(it)
                    holder.mBinding.tvAmount.text = formatAmount(itemPrice.toFloat() - it)
                    if(from == AppConstant.REFUND){
                        holder.mBinding.cbDiscountAppliedOnItem.text =
                            this.discountRemark + CV.WAS_APPLIED
                    }else{
                        holder.mBinding.cbDiscountAppliedOnItem.text =
                            this.discountRemark + CV.APPLIED_SUCCESSFULLY
                    }

                }
            }

            if (mDatabase.daoModifierGroupList()
                    .getModifierGroupList(this.itemId).size > 0 && !(from == AppConstant.REFUND)
            ) {
                holder.mBinding.ivModifier.visibility = View.VISIBLE
                holder.mBinding.tvModifier.visibility = View.VISIBLE
            } else {
                holder.mBinding.ivModifier.visibility = View.INVISIBLE
            }

            if (mDatabase.daoModifierGroupList()
                    .getModifierGroupList(this.itemId).size > 0 && !(from == AppConstant.REFUND)
            ) {
                holder.mBinding.ivModifier.visibility = View.VISIBLE
                holder.mBinding.tvModifier.visibility = View.VISIBLE
            } else {
                holder.mBinding.ivModifier.visibility = View.INVISIBLE
            }

            holder.mBinding.ivDownArrow.setOnClickListener {
                try {
                    itemCallBack.onItemClick("sub", position, mList[position])
                }catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            if (mList.get(position).strPricingType.equals(CV.IN_STORE)) {
                holder.mBinding.tvPrice.background =
                    mContext.getDrawable(R.drawable.edt_bg_without_corner)
            } else {
                holder.mBinding.tvPrice.background = null
            }
            holder.mBinding.tvPrice.setOnClickListener {
                try {
                    if (mList.get(position).strPricingType.equals(CV.IN_STORE)) {
                        itemCallBack.onItemClick("price", position, mList[position])
                    }
                }catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            holder.mBinding.ivUpArrow.setOnClickListener {
                try {
                    itemCallBack.onItemClick("add", position, mList[position])
                }catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            holder.mBinding.ivModifier.setOnClickListener {
                try {
                    itemCallBack.onItemClick("modifier", position, mList[position])
                }catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            holder.mBinding.ivDelete.setOnClickListener {
                try {
                    itemCallBack.onItemClick("delete", position, transModel)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            holder.mBinding.ivOpenDiscount.setOnClickListener {
                try {
                    itemCallBack.onItemClick("openDiscount", position, transModel)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        }
    }

    fun showAppliedDiscount(hashMap: HashMap<Int, DiscountGeneric>) {
        notifyDataSetChanged()
    }

    fun removeItemFromSet(position: Int) {
        mList.removeAt(position)
        notifyDataSetChanged()
    }

    fun updateItem(position: Int, orderTransMode: OrderTransModel) {
        if (mList != null) {
            mList[position] = orderTransMode
            notifyItemChanged(position)
        }
    }

    fun updateItem(orderTrasitionList: MutableList<OrderTransModel>) {
        mList = orderTrasitionList
        notifyDataSetChanged()
    }


    override fun getItemCount(): Int {
        return mList.size
    }

    fun getDataset(): List<OrderTransModel> {
        return mList
    }

    inner class MyViewHolder(internal val mBinding: ListItemCartBinding) :
        RecyclerView.ViewHolder(mBinding.root) {

        init {
            mBinding.root.setOnClickListener {
            }
        }
    }
}