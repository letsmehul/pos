package com.ehsm.recore.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ehsm.recore.R
import com.ehsm.recore.customview.ExpandableLayout
import com.ehsm.recore.interfaces.ItemCallBack
import com.ehsm.recore.model.Discount
import com.ehsm.recore.model.DiscountMaster
import com.ehsm.recore.utils.AppConstant
import com.ehsm.recore.utils.CV
import kotlinx.android.synthetic.main.item_discount.view.*
import java.util.*


class DiscountAdapter(
    private val itemsCells: SortedMap<String, List<DiscountMaster>>,
    private val itemCallBack: ItemCallBack<DiscountMaster>?
) :
    RecyclerView.Adapter<DiscountAdapter.ViewHolder>() {

    // Save the expanded row position
    private val expandedPositionSet: HashSet<Int> = HashSet()
    lateinit var context: Context
    private val simple_discount_view_type = 1
    private val coupon_discount_view_type = 2

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v =
            LayoutInflater.from(parent.context).inflate(R.layout.item_discount, parent, false)
        val vh = ViewHolder(v)
        context = parent.context
        return vh
    }

    override fun getItemCount(): Int {
        return itemsCells.size
    }

    override fun getItemViewType(position: Int): Int {
        // key
        val key: String = ArrayList(itemsCells.keys)[position]
        // value
        if (key == AppConstant.SIMPLE_DISCOUNT) {
            return simple_discount_view_type
        } else {
            return coupon_discount_view_type
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val llm = LinearLayoutManager(context)

        holder.itemView.rvCart.setHasFixedSize(true)
        holder.itemView.rvCart.layoutManager = llm

        val dividerItemDecoration = DividerItemDecoration(
            holder.itemView.rvCart.context,
            llm.getOrientation()
        )
        dividerItemDecoration.setDrawable(
            ContextCompat.getDrawable(
                holder.itemView.rvCart.context,
                R.drawable.divider
            )!!
        )
        holder.itemView.rvCart.addItemDecoration(dividerItemDecoration)

        // Add data to cells
        if (getItemViewType(position) == simple_discount_view_type) {
            setData(simple_discount_view_type, holder)
        } else {
            setData(coupon_discount_view_type, holder)
        }


        // Expand when you click on cell
        holder.itemView.expand_layout.setOnExpandListener(object :
            ExpandableLayout.OnExpandListener {
            override fun onExpand(expanded: Boolean) {
                if (expandedPositionSet.contains(position)) {
                    expandedPositionSet.remove(position)
                    holder.itemView.ivExpand.rotation = 0.0F
                } else {
                    expandedPositionSet.add(position)
                    holder.itemView.ivExpand.rotation = 90.0F

                }
            }
        })
        holder.itemView.expand_layout.setExpand(expandedPositionSet.contains(position))
    }

    private fun setData(viewType: Int, holder: ViewHolder) {
        if (viewType == simple_discount_view_type) {
            holder.itemView.question_textview.text =
                itemsCells.getValue(AppConstant.SIMPLE_DISCOUNT)[0].discountType
            val adapter = DiscountListAdapter(itemsCells.getValue(AppConstant.SIMPLE_DISCOUNT), itemCallBack)
            holder.itemView.rvCart.adapter = adapter

        } else {
            holder.itemView.question_textview.text =
                itemsCells.getValue(AppConstant.COUPON_DISCOUNT)[0].discountType
            val adapter = DiscountListAdapter(itemsCells.getValue(AppConstant.COUPON_DISCOUNT), itemCallBack)
            holder.itemView.rvCart.adapter = adapter

        }

    }

}