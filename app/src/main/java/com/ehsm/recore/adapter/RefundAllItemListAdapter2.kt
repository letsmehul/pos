package com.ehsm.recore.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.ehsm.recore.R
import com.ehsm.recore.databinding.ListItemAllRefund2Binding
import com.ehsm.recore.databinding.ListItemAllRefundBinding
import com.ehsm.recore.model.OrderProductsModel
import com.ehsm.recore.model.OrderTransModel


class RefundAllItemListAdapter2(
    private val mContext: Context,
    private val mList: MutableList<OrderProductsModel>,
    private val clickListener: ListItemClicked?


) : RecyclerView.Adapter<RefundAllItemListAdapter2.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(mContext)

        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater,
            R.layout.list_item_all_refund2,
            parent,
            false
        )
        return MyViewHolder(binding as ListItemAllRefund2Binding)
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.mBinding.model = mList[position]
        if(mList.get(position).strProductName.length<=13){
            holder.mBinding.tvUserName.setText(mList.get(position).strProductName)
        }else{
            holder.mBinding.tvUserName.visibility=View.INVISIBLE
            holder.mBinding.tvUserNameBig.visibility=View.VISIBLE
            holder.mBinding.tvUserNameBig.text=mList.get(position).strProductName
        }

        holder.mBinding.tvQty.setText(mList.get(position).dblItemQty)
        holder.mBinding.tvRate.setText(String.format("%.2f", mList.get(position).dblPrice.toDouble()))
        holder.mBinding.tvTax.setText(String.format("%.2f", mList.get(position).dblItemTaxAmount.toDouble()))
        holder.mBinding.tvAmount.setText(String.format("%.2f", mList.get(position).dblAmount.toDouble()))
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    interface ListItemClicked {
        fun listItemClicked(position: Int, orderTransModel: OrderProductsModel)

    }

    inner class MyViewHolder(internal val mBinding: ListItemAllRefund2Binding) :
        RecyclerView.ViewHolder(mBinding.root) {

        init {
            mBinding.root.setOnClickListener {

                mBinding.root.setBackgroundColor(mContext.resources.getColor(R.color.colorGray))
                clickListener!!.listItemClicked(
                    position,
                    mList.get(position)
                )
            }
        }
    }

}
