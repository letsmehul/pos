package com.calculator.mathematicaloperation

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import com.ehsm.recore.R
import com.ehsm.recore.model.ItemModel


class MyInvoicesAdapter(private val activity: Context, private var species: ArrayList<ItemModel>) : ArrayAdapter<ItemModel>(activity, R.layout.support_simple_spinner_dropdown_item, species) {

    var filtered = ArrayList<ItemModel>()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return convertView ?: createView(position, parent)
    }

    private fun createView(position: Int, parent: ViewGroup?): View {
        val view = LayoutInflater.from(context).inflate(R.layout.support_simple_spinner_dropdown_item, parent, false)
        return view.findViewById(android.R.id.text1)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        convertView ?: LayoutInflater.from(context).inflate(R.layout.support_simple_spinner_dropdown_item, parent, false)
        convertView?.findViewById<TextView>(android.R.id.text1)?.text = filtered[position].strProductName
        return super.getDropDownView(position, convertView, parent)
    }

    override fun getCount() = filtered.size

    override fun getItem(position: Int) = filtered[position]

    override fun getFilter() = filter

    private var filter: Filter = object : Filter() {

        override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
            val results = FilterResults()

            val query = if (constraint != null && constraint.isNotEmpty()) autocomplete(constraint.toString())
            else arrayListOf()

            results.values = query
            results.count = query.size

            return results
        }

        private fun autocomplete(input: String): ArrayList<ItemModel> {
            val results = arrayListOf<ItemModel>()

            for (specie in species) {
                if (specie.strProductName.toLowerCase().contains(input.toLowerCase())) results.add(specie)
            }

            return results
        }

        override fun publishResults(constraint: CharSequence?, results: Filter.FilterResults) {
            filtered = results.values as ArrayList<ItemModel>
            notifyDataSetInvalidated()
        }

        override fun convertResultToString(result: Any) = (result as ItemModel).strProductName
    }
}

//class MyInvoicesAdapter(context: Context,
//                        private val layoutResource: Int,
//                         private val values: List<ItemModel>) : ArrayAdapter<ItemModel>(context, layoutResource, values) {
//
//    private var objects = values
//
//    override fun getItem(position: Int): ItemModel = values[position]
//
//    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
//        val view = createViewFromResource(convertView, parent, layoutResource)
//
//        return bindData(getItem(position), view)
//    }
//
//    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
//        val view = createViewFromResource(convertView, parent, android.R.layout.simple_spinner_dropdown_item)
//
//        return bindData(getItem(position), view)
//    }
//
//    private fun createViewFromResource(convertView: View?, parent: ViewGroup, layoutResource: Int): TextView {
//        val context = parent.context
//        val view = convertView ?: LayoutInflater.from(context).inflate(layoutResource, parent, false)
//        return view.findViewById(android.R.id.text1)
//    }
//
//    private fun bindData(value: ItemModel, view: TextView): TextView {
//        view.text = value.strProductName
//        return view
//    }
//
//        override fun getFilter(): Filter {
//            return myFilter
//    }
//
//    var myFilter: Filter = object : Filter() {
//        override fun performFiltering(constraint: CharSequence?): FilterResults? {
//            val filterResults = FilterResults()
//            val tempList: ArrayList<ItemModel> = ArrayList<ItemModel>()
//            //constraint is the result from text you want to filter against.
////objects is your data set you will filter from
//            if (constraint != null && objects != null) {
//                val length: Int = objects.size
//                var i = 0
//                while (i < length) {
//                    val item: ItemModel = objects.get(i)
//                    //do whatever you wanna do here
////adding result set output array
//                    tempList.add(item)
//                    i++
//                }
//                //following two lines is very important
////as publish result can only take FilterResults objects
//                filterResults.values = tempList
//                filterResults.count = tempList.size
//            }
//            return filterResults
//        }
//
//        override fun publishResults(
//            contraint: CharSequence?,
//            results: FilterResults
//        ) {
//            objects = results.values as ArrayList<ItemModel>
//            if (results.count > 0) {
//                notifyDataSetChanged()
//            } else {
//                notifyDataSetInvalidated()
//            }
//        }
//    }
//}

//class MyInvoicesAdapter(
//    context: Context,
//    objects: ArrayList<String>
//) :
//    BaseAdapter(), Filterable {
//    var context: Context
//    private val mLock = Any()
//    private var fullList: ArrayList<String>
//    private var mOriginalValues: ArrayList<String>? = null
//    private var mFilter: ArrayFilter? = null
//    private val mSelectListener: OnItemSelectClickListener? =
//        null
//    private val mDeleteListener: OnItemDeleteClickListener? =
//        null
//    var itemName: TextView? = null
//    var itemDel: TextView? = null
//    private val mInflater: LayoutInflater
//
//    interface OnItemSelectClickListener {
//        fun onItemSelectClicked()
//    }
//
//    interface OnItemDeleteClickListener {
//        fun onItemDeleteClicked()
//    }
//
//    override fun getCount(): Int {
//        return fullList.size
//    }
//
//    override fun getItem(position: Int): String {
//        return fullList[position]
//    }
//
//    override fun getItemId(position: Int): Long {
//        return position.toLong()
//    }
//
//    override fun getView(
//        position: Int,
//        convertView: View,
//        parent: ViewGroup
//    ): View {
//        return getCustomView(position, convertView, parent)
//    }
//
//    fun getCustomView(
//        position: Int,
//        convertView: View?,
//        parent: ViewGroup?
//    ): View {
//        val inflater = (context as Activity).layoutInflater
//        val rowView: View =
//            inflater.inflate(R.layout.custom_adapter_row, parent, false)
//        itemName = rowView.findViewById<View>(R.id.itemName) as TextView
//        itemDel = rowView.findViewById<View>(R.id.itemDel) as TextView
//        itemName!!.text = getItem(position)
//        //clear button click listener
//        itemDel!!.setOnClickListener {
//            fullList.remove(getItem(position))
//            notifyDataSetChanged()
//            Toast.makeText(context, "Removed", Toast.LENGTH_SHORT).show()
//        }
//        return rowView
//    }
//
//    override fun getFilter(): Filter {
//        if (mFilter == null) {
//            mFilter = ArrayFilter()
//        }
//        return mFilter as ArrayFilter
//    }
//
//    //filter items which does not contain typed text
//    private inner class ArrayFilter : Filter() {
//        override fun performFiltering(prefix: CharSequence): FilterResults {
//            val results = FilterResults()
//            if (mOriginalValues == null) {
//                synchronized(
//                    mLock
//                ) { mOriginalValues = ArrayList(fullList) }
//            }
//            if (prefix == null || prefix.length == 0) {
//                var list: ArrayList<String>
//                synchronized(mLock) { list = ArrayList(mOriginalValues!!) }
//                results.values = list
//                results.count = list.size
//            } else {
//                val prefixString = prefix.toString().toLowerCase()
//                var values: ArrayList<String>
//                synchronized(
//                    mLock
//                ) { values = ArrayList(mOriginalValues!!) }
//                results.values = values
//                results.count = values.size
//                val count = values.size
//                val newValues =
//                    ArrayList<String>()
//                for (i in 0 until count) {
//                    val item = values[i]
//                    if (item.toLowerCase().contains(prefixString)) {
//                        newValues.add(item)
//                    }
//                }
//                results.values = newValues
//                results.count = newValues.size
//            }
//            return results
//        }
//
//        override fun publishResults(
//            constraint: CharSequence,
//            results: FilterResults
//        ) {
//            fullList = results.values as ArrayList<String>
//            if (results.count > 0) {
//                notifyDataSetChanged()
//            } else {
//                notifyDataSetInvalidated()
//            }
//        }
//    }
//
//    /**
//     * Set listener for clicks on the footer item
//     */
///*public void setOnItemSelectClickListener(OnItemSelectClickListener listener) {
//        mSelectListener = listener;
//    }*/
///*public void setOnItemDeleteClickListener(OnItemDeleteClickListener listener) {
//        mDeleteListener = listener;
//    }*/
//    init {
//        mInflater =
//            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
//        fullList = objects
//        this.context = context
//    }
//}