package com.ehsm.recore.adapter

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.ehsm.recore.R
import com.ehsm.recore.databinding.ListItemCustomerBinding
import com.ehsm.recore.model.CustomerModel

class CustomerListAdapter(
    private val mContext: Context,
    private val mList: List<CustomerModel>,
    private val clickListener: ListItemClickedCustomer?
) : RecyclerView.Adapter<CustomerListAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(mContext)

        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater,
            R.layout.list_item_customer,
            parent,
            false
        )
        return MyViewHolder(binding as ListItemCustomerBinding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.mBinding.model = mList[position]
     //   holder.mBinding.tvNo.setText(mList.get(position).strFirstName + " " + mList.get(position).strLastName)
        if(TextUtils.isEmpty(mList[position].strLastName)){
            holder.mBinding.tvNo.text = mList[position].strFirstName
        }else{
            holder.mBinding.tvNo.text = "${mList[position].strLastName} , ${mList[position].strFirstName}"
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    interface ListItemClickedCustomer {
        fun listItemClickedCustomer(position: Int, category_id: Int)
    }

    inner class MyViewHolder(internal val mBinding: ListItemCustomerBinding) :
        RecyclerView.ViewHolder(mBinding.root) {

        init {
            mBinding.root.setOnClickListener {
                clickListener!!.listItemClickedCustomer(
                    adapterPosition,
                    mList.get(adapterPosition).iCustomerId
                )
            }
        }
    }
}
