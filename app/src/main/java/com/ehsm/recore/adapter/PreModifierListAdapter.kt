package com.ehsm.recore.adapter

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.ehsm.recore.R
import com.ehsm.recore.databinding.ItemPreModifierBinding
import com.ehsm.recore.model.PreModifierMasterModel
import com.ehsm.recore.utils.CV

class PreModifierListAdapter(
    private val mContext: Context,
    private val mList: List<PreModifierMasterModel>,
    private val clickListener: ItemClickPreModifier?
) : RecyclerView.Adapter<PreModifierListAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(mContext)

        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater,
            R.layout.item_pre_modifier,
            parent,
            false
        )
        return MyViewHolder(binding as ItemPreModifierBinding)
    }

    interface ItemClickPreModifier {
        fun listItemClickedPreModifier(position: Int, pre_modifierId: String)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.mBinding.model = mList[position]

        if (mList.get(position).strPreModifierName.equals(CV.ADD) && !mList.get(position).isPreModifireSelected) {
            holder.mBinding.layout.background =
                mContext.resources.getDrawable(R.drawable.btn_background_green)
            holder.mBinding.tvName.setTextColor(
                ContextCompat.getColor(
                    mContext,
                    R.color.colorWhite
                )
            )
        } else if (mList.get(position).strPreModifierName.equals(CV.ADD) && mList.get(position).isPreModifireSelected) {
            holder.mBinding.layout.background =
                mContext.resources.getDrawable(R.drawable.btn_selected_premodifire)
            holder.mBinding.tvName.setTextColor(
                ContextCompat.getColor(
                    mContext,
                    R.color.colorAccent
                )
            )
        } else if (mList.get(position).strPreModifierName.equals(CV.NO) && !mList.get(position).isPreModifireSelected) {
            holder.mBinding.layout.background =
                mContext.resources.getDrawable(R.drawable.btn_background_red)
            holder.mBinding.tvName.setTextColor(
                ContextCompat.getColor(
                    mContext,
                    R.color.colorWhite
                )
            )
        } else if (mList.get(position).strPreModifierName.equals(CV.NO) && mList.get(position).isPreModifireSelected) {
            holder.mBinding.layout.background =
                mContext.resources.getDrawable(R.drawable.btn_selected_premodifire)
            holder.mBinding.tvName.setTextColor(
                ContextCompat.getColor(
                    mContext,
                    R.color.colorAccent
                )
            )
        } else if (mList.get(position).strPreModifierName.equals(CV.LITE) && !mList.get(position).isPreModifireSelected) {
            holder.mBinding.layout.background =
                mContext.resources.getDrawable(R.drawable.btn_background_yellow)
            holder.mBinding.tvName.setTextColor(
                ContextCompat.getColor(
                    mContext,
                    R.color.colorWhite
                )
            )
        } else if (mList.get(position).strPreModifierName.equals(CV.LITE) && mList.get(position).isPreModifireSelected) {
            holder.mBinding.layout.background =
                mContext.resources.getDrawable(R.drawable.btn_selected_premodifire)
            holder.mBinding.tvName.setTextColor(
                ContextCompat.getColor(
                    mContext,
                    R.color.colorAccent
                )
            )
        } else if (mList.get(position).strPreModifierName.equals(CV.EXTRA) && !mList.get(position).isPreModifireSelected) {
            holder.mBinding.layout.background =
                mContext.resources.getDrawable(R.drawable.btn_background_turquoise)
            holder.mBinding.tvName.setTextColor(
                ContextCompat.getColor(
                    mContext,
                    R.color.colorWhite
                )
            )
        } else if (mList.get(position).strPreModifierName.equals(CV.EXTRA) && mList.get(position).isPreModifireSelected) {
            holder.mBinding.layout.background =
                mContext.resources.getDrawable(R.drawable.btn_selected_premodifire)
            holder.mBinding.tvName.setTextColor(
                ContextCompat.getColor(
                    mContext,
                    R.color.colorAccent
                )
            )
        } else if (mList.get(position).strPreModifierName.equals(CV.ON_SIDE) && !mList.get(position).isPreModifireSelected) {
            holder.mBinding.layout.background =
                mContext.resources.getDrawable(R.drawable.btn_background_purple)
            holder.mBinding.tvName.setTextColor(
                ContextCompat.getColor(
                    mContext,
                    R.color.colorWhite
                )
            )
        } else if (mList.get(position).strPreModifierName.equals(CV.ON_SIDE) && mList.get(position).isPreModifireSelected) {
            holder.mBinding.layout.background =
                mContext.resources.getDrawable(R.drawable.btn_selected_premodifire)
            holder.mBinding.tvName.setTextColor(
                ContextCompat.getColor(
                    mContext,
                    R.color.colorAccent
                )
            )
        } else if (mList.get(position).strPreModifierName.equals(CV.SUB) && !mList.get(position).isPreModifireSelected) {
            holder.mBinding.layout.background =
                mContext.resources.getDrawable(R.drawable.btn_background_blue)
            holder.mBinding.tvName.setTextColor(
                ContextCompat.getColor(
                    mContext,
                    R.color.colorWhite
                )
            )
        } else if (mList.get(position).strPreModifierName.equals(CV.SUB) && mList.get(position).isPreModifireSelected) {
            holder.mBinding.layout.background =
                mContext.resources.getDrawable(R.drawable.btn_selected_premodifire)
            holder.mBinding.tvName.setTextColor(
                ContextCompat.getColor(
                    mContext,
                    R.color.colorAccent
                )
            )
        } else {
            holder.mBinding.tvName.setTextColor(
                ContextCompat.getColor(
                    mContext,
                    R.color.colorWhite
                )
            )
            holder.mBinding.layout.background =
                mContext.resources.getDrawable(R.drawable.btn_bg_gray)
        }

        holder.mBinding.layout.setOnClickListener {
            if(!TextUtils.isEmpty(holder.mBinding.tvName.text)){
                for ((index, item) in mList.withIndex()) {
                    if (mList.get(position).iPreModifierId == item.iPreModifierId) {
                        mList.get(index).isPreModifireSelected = true
                    } else {
                        mList.get(index).isPreModifireSelected = false
                    }
                }
                notifyDataSetChanged()
                clickListener!!.listItemClickedPreModifier(
                    position,
                    mList.get(position).iPreModifierId
                )
            }
        }

    }

    override fun getItemCount(): Int {
        return mList.size

    }

    inner class MyViewHolder(internal val mBinding: ItemPreModifierBinding) :
        RecyclerView.ViewHolder(mBinding.root) {

        init {
            mBinding.root.setOnClickListener {

            }
        }
    }

}
