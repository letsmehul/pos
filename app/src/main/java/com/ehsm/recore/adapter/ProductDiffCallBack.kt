package com.ehsm.recore.adapter


import androidx.recyclerview.widget.DiffUtil
import com.ehsm.recore.model.ItemModel



class ProductDiffCallBack(private val oldTaskList: ArrayList<ItemModel>, private val newTaskList: ArrayList<ItemModel>) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldTask = oldTaskList[oldItemPosition]
        val newTask = newTaskList[newItemPosition]
        return oldTask == newTask
    }

    override fun getOldListSize() = oldTaskList.size

    override fun getNewListSize() = newTaskList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldTask = oldTaskList[oldItemPosition]
        val newTask = newTaskList[newItemPosition]
        return newTask.strProductName == oldTask.strProductName
                && newTask.dblRetailPrice == oldTask.dblRetailPrice
                && newTask.strProductCategoryName == oldTask.strProductCategoryName
                && newTask.subCategoryName == oldTask.subCategoryName
                && newTask.strProductCode == oldTask.strProductCode
    }

   /* override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val newTask = newTaskList[newItemPosition]
        val oldTask = oldTaskList[oldItemPosition]

        val diff = Bundle()
        if (newTask.strProductName != oldTask.strProductName) {
            diff.putString("productName", newTask.strProductName)
        }
        if (newTask.dblRetailPrice != oldTask.dblRetailPrice) {
            diff.putString("retailPrice", newTask.dblRetailPrice)
        }

        if (newTask.strProductCategoryName != oldTask.strProductCategoryName) {
            diff.putString("productCategoryName", newTask.strProductCategoryName)
        }
        if (newTask.strProductName != oldTask.strProductName) {
            diff.putString("categoryName", newTask.strProductName)
        }
        return if (diff.size() == 0) {
            null
        } else diff
    }*/

}