package com.ehsm.recore.adapter

import android.content.Context
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.ehsm.recore.R
import com.ehsm.recore.activities.ParentActivity
import com.ehsm.recore.fragments.NewSalesFragment
import com.ehsm.recore.model.CartRefundSummaryData
import com.ehsm.recore.model.OrderTransModel
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CV
import com.ehsm.recore.utils.NewSaleFragmentCommon

class RefundSelectedItemListAdapter(
    private val mFragment: NewSalesFragment,
    private val mContext: Context,
    private val mList: MutableList<OrderTransModel>,
    private val tvTotalRight: TextView,
    private val containerRefundSelectedDiscount: LinearLayout,
    private val containerRefundSelectedDiscountRemark: LinearLayout,
    private val tvRefundselectedCartDiscount: TextView,
    private val tvRefundselectedCartDiscountRemark: TextView,
    private val mRefundItemClickListner: RefundItemClicked?

) : RecyclerView.Adapter<RefundSelectedItemListAdapter.MyViewHolder>() {
    private var currency_symbol = ""

    init {
        currency_symbol = CM.getSp(mContext, CV.CURRENCY_CODE, "") as String
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.list_item_seleted_refund, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mData = mList[position]
        if(mData.itemName.length<=13){
            holder.tvUserName.visibility=View.VISIBLE
            holder.tvUserNameBig.visibility=View.INVISIBLE
            holder.tvUserName.text = mData.itemName
        }else{
           holder.tvUserName.visibility=View.INVISIBLE
           holder.tvUserNameBig.visibility=View.VISIBLE
            holder.tvUserNameBig.text=mData.itemName
        }

        holder.tvRate.text = String.format("%.2f", mData.itemRate)

        //mData.itemQty = mData.itemQtyLeft
        var itemPrice = mData.itemQty * mData.itemRate
        val discountAmount =
            String.format("%.2f", (mData.discountAmount * mData.itemQty) / mData.totalItemQty)
                .toDouble()
        var totalAmount = itemPrice - discountAmount

        holder.tvDiscount.text = String.format("%.2f", discountAmount)
        val taxRate = mData.taxPercentage.toDouble()

        var taxAmount = 0.0
        /*if (discountAmount == 0.0) {
            taxAmount = mFragment.getTaxAmountAppliedDiscount(mData)
            taxAmount = String.format("%.2f", taxAmount).toDouble()
            if (taxAmount == 0.0) {
                taxAmount = totalAmount / 100.0f * taxRate
            }
        } else {
            taxAmount = totalAmount / 100.0f * taxRate
        }*/

        //mData.taxAmount = String.format("%.2f", taxAmount)
        //taxAmount = NewSaleFragmentCommon.taxAmountCalculation(mData,discountAmount)
        if (mData.iDiscountId != 0) {
            //taxAmount = String.format("%.2f", totalAmount / 100.0f * taxRate).toDouble()
            taxAmount = NewSaleFragmentCommon.taxAmountCalculation(mData,discountAmount)
        }else{
            taxAmount = mFragment.getTaxAmountAppliedDiscount(mData)
        }
        holder.tvTax.text = String.format("%.2f", taxAmount)
        holder.tvAmount.text = String.format("%.2f", (totalAmount + taxAmount))

        holder.edtReason.setText(mData.reason)

        if (mData.itemQtyLeft != 0 && position == holder.adapterPosition) {
            holder.edtQty.setText(mData.itemQty.toString())
        } else {
            holder.edtQty.setText("")
        }

        if (!mData.ingredientName.isNullOrEmpty()) {
            holder.tvModifierIngredient.visibility = View.VISIBLE
            holder.tvModifierIngredient.text = mData.ingredientName
            holder.edtQty.isFocusable = true
        } else {
            holder.tvModifierIngredient.visibility = View.GONE
        }


        if (!mData.discountRemark.isNullOrEmpty()) {
            holder.edtQty.isFocusable = true
            holder.tvDiscountRemark.visibility = View.VISIBLE
            holder.tvDiscountRemark.text = mData.discountRemark + CV.WAS_APPLIED
        } else {
            holder.tvDiscountRemark.visibility = View.GONE
        }

        holder.edtQty.isFocusable = true
        setGrandTotal()

        val remaing_qty = mData.itemQtyLeft - if (holder.edtQty.text.toString() == "") {
            0
        } else {
            holder.edtQty.text.toString().toInt()
        }
        holder.tvQtyLeft.text = remaing_qty.toString()
        if (!TextUtils.isEmpty(holder.edtQty.text.toString()))
            mData.itemQty = holder.edtQty.text.toString().toInt()

        holder.mBtnDelete.setOnClickListener {
            mRefundItemClickListner!!.RefundItemClicked("delete", position, mList)
        }


        holder.edtQty.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                val view = ParentActivity().currentFocus
                if (view != null) {
                    val inputManager =
                        ParentActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    inputManager.hideSoftInputFromWindow(view.windowToken, 0)
                }
            }
        }

        /*holder.edtQty.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (v != null) {
                val inputManager =
                    v.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputManager.hideSoftInputFromWindow(v.windowToken, 0)
            }
        }*/


        holder.edtQty.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                val imm =
                    v.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(v.windowToken, 0)
                return@OnEditorActionListener true
            }
            false
        })

        holder.edtQty.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (position == holder.adapterPosition) {
                    if (!TextUtils.isEmpty(s.toString())) {
                        var item = mList[position]
                        if (s.toString().toInt() > item.itemQtyLeft) {
                            holder.edtQty.setText("")
                        } else {
                            holder.tvQtyLeft.text =
                                (item.itemQtyLeft - s.toString().toInt()).toString()
                            item.itemQty = s.toString().toInt()
                            val itemQty = item.itemQty
                            val itemRate = item.itemRate
                            val itemPrice = itemQty * itemRate
                            val discountAmount =
                                ((item.discountAmount * item.itemQty) / item.totalItemQty).toDouble()
                            val totalAmount = itemPrice - discountAmount

                            /*val taxAmount: Double = (totalAmount * taxRate) / 100*/

                            var taxAmount = 0.0
                            val amount = (itemQty * totalAmount) - discountAmount
                            //taxAmount = NewSaleFragmentCommon.taxAmountCalculation(item,discountAmount)
                            if (item.iDiscountId != 0) {
                                //taxAmount = String.format("%.2f", totalAmount / 100.0f * taxRate).toDouble()
                                taxAmount = NewSaleFragmentCommon.taxAmountCalculation(item,discountAmount)
                            }else{
                                taxAmount = mFragment.getTaxAmountAppliedDiscount(item)
                            }
                            //taxAmount = (item.taxAmount.toDouble() * itemQty)/item.totalItemQty
                            holder.tvDiscount.text = String.format("%.2f", discountAmount)
                            holder.tvTax.text = String.format("%.2f", taxAmount)
                            holder.tvAmount.text = String.format("%.2f", totalAmount + taxAmount)
                            /*item.taxAmount =
                                String.format("%.2f", taxAmount)
                            item.itemAmount = totalAmount*/
                            setGrandTotal()
                        }
                    } else {
                        var item = mList[position]
                        holder.tvQtyLeft.text = item.itemQtyLeft.toString()
                        if (TextUtils.isEmpty(holder.edtQty.text.toString())) {
                            item.itemQty = 0
                        } else {
                            item.itemQty = s.toString().toInt()
                        }
                        setGrandTotal()
                    }
                    mRefundItemClickListner!!.RefundItemClicked("edit", position, mList)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {


            }
        })

        holder.edtReason.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                mList[holder.adapterPosition].reason = s.toString()
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvUserName: TextView
        var tvUserNameBig: TextView
        var tvRate: TextView
        var edtQty: AppCompatEditText
        var tvQtyLeft: TextView
        var tvTax: TextView
        var tvDiscount: TextView
        var edtReason: AppCompatEditText
        var tvAmount: TextView
        var mBtnDelete: AppCompatImageView
        var tvModifierIngredient: TextView
        var tvDiscountRemark: TextView

        init {
            tvUserName = itemView.findViewById(R.id.tvUserName)
            tvUserNameBig = itemView.findViewById(R.id.tvUserNameBig)
            tvRate = itemView.findViewById(R.id.tvRate)
            edtQty = itemView.findViewById(R.id.edtQty)
            tvQtyLeft = itemView.findViewById(R.id.tv_qty_left)
            tvTax = itemView.findViewById(R.id.tvTax)
            tvDiscount = itemView.findViewById(R.id.tvDiscount)
            edtReason = itemView.findViewById(R.id.edtReason)
            tvAmount = itemView.findViewById(R.id.tvAmount)
            mBtnDelete = itemView.findViewById(R.id.ivDelete)

            tvModifierIngredient = itemView.findViewById(R.id.tvModifierIngredient)
            tvDiscountRemark = itemView.findViewById(R.id.tvDiscountRemark)
        }
    }

    private fun setGrandTotal() {
        mFragment.calculateCartSelectedRefundProcedure(mList) { cart: CartRefundSummaryData ->
            tvRefundselectedCartDiscount.text =
                currency_symbol + " " + String.format("%.2f", (cart.totalDiscount))
            tvTotalRight.text = currency_symbol + " " + String.format(
                "%.2f",
                ((cart.totalAmount + cart.taxAmount) + (cart.saleTotalAmount + cart.saleTaxAmount)) - cart.totalDiscount
            )
        }

    }

    private fun getCartDiscount(): Double {
        var totalDiscount = 0.0
        mFragment.calculateCartSelectedRefundProcedure(mList) { cart: CartRefundSummaryData ->
            totalDiscount = cart.totalDiscount.toDouble() + cart.saleDiscountAmount.toDouble()
        }
        return totalDiscount
    }

    fun removeItemFromSet(position: Int) {

        try {
            mList.removeAt(position)
            notifyItemRemoved(position)

            setGrandTotal()
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }
    }

    interface RefundItemClicked {
        fun RefundItemClicked(
            action: String,
            position: Int,
            orderTransList: MutableList<OrderTransModel>
        )

    }


}
