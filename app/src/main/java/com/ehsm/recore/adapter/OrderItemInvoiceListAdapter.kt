package com.ehsm.recore.adapter

import android.content.*
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.ehsm.recore.R
import com.ehsm.recore.databinding.ListItemCartBinding
import com.ehsm.recore.databinding.ListItemOrderBinding
import com.ehsm.recore.model.OrdeItemModel

class OrderItemInvoiceListAdapter(
    private val mContext: Context,
    private val mList: List<OrdeItemModel>,
    private val clickListener: ItemClick?

    ) : RecyclerView.Adapter<OrderItemInvoiceListAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(mContext)

        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater,
            R.layout.list_item_order,
            parent,
            false
        )
        return MyViewHolder(binding as ListItemOrderBinding)
    }

    interface ItemClick {
        fun listItemClicked(position: Int,category_id : String)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.mBinding.model = mList[position]

    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class MyViewHolder(internal val mBinding: ListItemOrderBinding) :
        RecyclerView.ViewHolder(mBinding.root) {

        init {
            mBinding.root.setOnClickListener {
                clickListener!!.listItemClicked(position,mList.get(position).order_id)
            }
        }
    }

}
