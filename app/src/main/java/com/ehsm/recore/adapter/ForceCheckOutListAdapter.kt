package com.ehsm.recore.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.ehsm.recore.R
import com.ehsm.recore.databinding.ListItemPosStatusBinding
import com.ehsm.recore.model.ForceCheckOutModel
import com.ehsm.recore.model.OrderTransModel
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CV
import java.text.SimpleDateFormat
import java.util.*


class ForceCheckOutListAdapter(
    private val mContext: Context,
    private val mList: List<ForceCheckOutModel>,
    private val clickListener: ItemClick?
) : RecyclerView.Adapter<ForceCheckOutListAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(mContext)

        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater,
            R.layout.list_item_pos_status,
            parent,
            false
        )
        return MyViewHolder(binding as ListItemPosStatusBinding)
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.mBinding.model = mList[position]
        Log.e("===", "=== date time ===" + mList.get(position).dateTimeCreated)

        val date = CM.convertDateFormate(
            CV.WS_DATE_FORMATTE_AM_PM,
            CV.DISPLAY_DATE_FORMATTE_AMPM,
            mList.get(position).dateTimeCreated
        )
        holder.mBinding.tvDateTime.setText(date)

        holder.mBinding.tvEmpName.setText(mList.get(position).strFirstName + " " +mList.get(position).strLastName)
        if (mList.get(position).pos_status.equals("Offline")) {
            holder.mBinding.text.background = mContext.resources.getDrawable(R.drawable.ic_circle_offline)
        } else {
            holder.mBinding.text.background = mContext.resources.getDrawable(R.drawable.ic_circle_online)

        }
        holder.mBinding.switchForceCheckOut.setOnClickListener {
            clickListener!!.listItemClicked(position, mList.get(position))
        }
//        holder.mBinding.switchForceCheckOut.setOnCheckedChangeListener({ buttonView, isChecked ->
//            if (isChecked) {
//                clickListener!!.listItemClicked(position, mList.get(position))
//
//            }
//        })

    }

    override fun getItemCount(): Int {
        return mList.size
    }


    interface ItemClick {
        fun listItemClicked(position: Int, forceCheckOutModel: ForceCheckOutModel)
    }

    inner class MyViewHolder(internal val mBinding: ListItemPosStatusBinding) :
        RecyclerView.ViewHolder(mBinding.root) {

        init {
            mBinding.root.setOnClickListener {

            }
        }
    }

}
