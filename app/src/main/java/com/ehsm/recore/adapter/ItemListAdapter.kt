package com.ehsm.recore.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.ehsm.recore.R
import com.ehsm.recore.databinding.ListItemItemBinding
import com.ehsm.recore.model.ItemModel
import com.ehsm.recore.utils.AppConstant
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CV


class ItemListAdapter(private val mContext: Context, private val itemList: List<ItemModel>) :
    BaseAdapter() {

    private val inflater: LayoutInflater
    private var currency_symbol = "$"

    init {
        inflater = LayoutInflater.from(mContext)
        currency_symbol = CM.getSp(mContext, CV.CURRENCY_CODE, "") as String

    }


    override fun getCount(): Int {
        return itemList.size
    }

    override fun getItem(i: Int): String {
        return itemList.get(i).strProductName
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    private class MyViewHolder internal constructor(val binding: ListItemItemBinding) {
        var view: View

        init {
            this.view = binding.getRoot()
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val holder: MyViewHolder
        val item = itemList.get(position)
        if (convertView == null) {
            val itemBinding = DataBindingUtil.inflate<ListItemItemBinding>(
                LayoutInflater.from(parent.context),
                R.layout.list_item_item,
                parent,
                false
            )

            holder = MyViewHolder(itemBinding)
            holder.view = itemBinding.getRoot()
            holder.view.setTag(holder)
        } else {
            holder = convertView.tag as MyViewHolder
        }
        if (item != null) {
            holder.binding.currencySymbol = currency_symbol
            holder.binding.model = item
            if (!item.strProductImage.equals(AppConstant.IMAGE_PLACEHOLDER_PRODUCT)) {
                holder.binding.ivUserProfilePic.visibility = View.VISIBLE
                holder.binding.tvItemName.visibility = View.VISIBLE
                holder.binding.tvPrice.visibility = View.VISIBLE
                holder.binding.tvItemNameLarge.visibility = View.GONE
                Glide.with(mContext)
                    .load(item.strProductImage)
                    .into(holder.binding.ivUserProfilePic)
            } else {
                holder.binding.ivUserProfilePic.visibility = View.GONE
                holder.binding.tvItemName.visibility = View.GONE
                holder.binding.tvPrice.visibility = View.VISIBLE
                holder.binding.tvItemNameLarge.visibility = View.VISIBLE
            }

        }
        return holder.view
    }


}