package com.ehsm.recore.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.ehsm.recore.R
import com.ehsm.recore.databinding.ItemPaymentBinding
import com.ehsm.recore.model.PaymentModel

class PaymentListAdapter(
    private val mContext: AppCompatActivity,
    private val mList: List<PaymentModel>,
    private val currency_symbol: String
) : RecyclerView.Adapter<PaymentListAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(mContext)
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater,
            R.layout.item_payment,
            parent,
            false
        )
        return MyViewHolder(binding as ItemPaymentBinding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.mBinding.model = mList[position]
        holder.mBinding.splitLable = "Split-${position + 1}"
    }

    override fun getItemCount(): Int {
        return mList.size
    }

     inner class MyViewHolder(internal val mBinding: ItemPaymentBinding) :
        RecyclerView.ViewHolder(mBinding.root) {

        init {
            mBinding.root.setOnClickListener {

            }
        }
    }
}
