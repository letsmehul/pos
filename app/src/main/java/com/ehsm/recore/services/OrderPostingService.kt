package com.ehsm.recore.services

import android.app.Service
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.util.Log
import androidx.annotation.Nullable
import com.ehsm.recore.model.ResponseModel
import com.ehsm.recore.network.RestClient
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CV
import com.google.gson.Gson
import com.kcspl.divyangapp.network.CustomApiCallback
import java.text.SimpleDateFormat
import java.util.*

class OrderPostingService : Service() {
    lateinit var mainHandler : Handler

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        // Send a notification that service is started
        mainHandler = Handler(Looper.getMainLooper())
        mainHandler.post(runnable)

        return START_NOT_STICKY
    }

    val runnable = object : Runnable {
        override fun run() {
            if (!CM.isInternetAvailable(this@OrderPostingService)) {

            }else{
                val sdf = SimpleDateFormat(CV.WS_DATE_FORMATTE_AM_PM)

                RestClient.getService()?.statusUpdate(sdf.format(Calendar.getInstance().time))
                    ?.enqueue(object : CustomApiCallback<ResponseModel>() {

                        override fun handleResponseData(data: ResponseModel?) {
                            Log.e("=====", "=== sucess ====")
                            Log.e("OrderPostingService","RESPONE: "+ Gson().toJson(data))
                        }

                        override fun showErrorMessage(errormessage: String?) {
                            Log.e("=====", "=== fail ====")
                        }
                    })
            }
            mainHandler.postDelayed(this, 600000)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        //        Toast.makeText(this, "Service Stopped", Toast.LENGTH_SHORT).show();
        mainHandler.removeCallbacks(runnable)
    }

    @Nullable
    override fun onBind(intent: Intent): IBinder? {
        throw UnsupportedOperationException("Not yet implemented")
    }
}