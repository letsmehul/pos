package com.ehsm.recore.services

/**
 * Created by accusol on 8/3/2017.
 */
interface CallBack {
    fun updateUI()
}