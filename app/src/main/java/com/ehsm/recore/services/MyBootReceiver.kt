package com.ehsm.recore.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class MyBootReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val service = Intent(context, OrderPostingService::class.java)
        context.startService(service)
    }
}