package com.ehsm.recore.dialogs

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.View
import android.view.Window
import android.widget.*
import com.ehsm.recore.R
import com.ehsm.recore.utils.AppConstant
import com.ehsm.recore.utils.DecimalInputFilter

class CheckOutDialog(context: Context,var paymentType:String,val totalValue:Double,val strAmount: ArrayList<Int> ) :Dialog(context){

    private lateinit var btnCash :Button
    private lateinit var llCash :LinearLayout
    private lateinit var relCash :RelativeLayout


    private lateinit var btnExternal :Button
    private lateinit var llExternal :LinearLayout
    private lateinit var btnPay :Button


    private lateinit var btnOther :Button
    private lateinit var btnCash1 :Button
    private lateinit var btnCash2 :Button
    private lateinit var btnCash3 :Button

    private lateinit var edtAmount :EditText

    private lateinit var tvAmountEdit :TextView

    init {
        setCancelable(false)
    }

    override fun create() {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_checkout)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val ivCancel = findViewById<ImageView>(R.id.ivCancel)
        val btnTender = findViewById<Button>(R.id.btnTender)

        btnCash = findViewById<Button>(R.id.btnCash)
        llCash = findViewById<LinearLayout>(R.id.llCash)
        relCash = findViewById<RelativeLayout>(R.id.relCash)

        btnExternal = findViewById<Button>(R.id.btnCredit)
        llExternal = findViewById<LinearLayout>(R.id.llExternal)
        btnPay = findViewById<Button>(R.id.btnProceed)

        btnOther = findViewById<Button>(R.id.btnOther)

        btnCash1 = findViewById<Button>(R.id.btnCash1)
        btnCash2 = findViewById<Button>(R.id.btnCash2)
        btnCash3 = findViewById<Button>(R.id.btnCash3)


        edtAmount = findViewById<EditText>(R.id.edtAmount)
        edtAmount.filters = arrayOf<InputFilter>(DecimalInputFilter())
        tvAmountEdit = findViewById<TextView>(R.id.tvAmountEdit)

        edtAmount.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                if (s.isNotEmpty()) {
                    tvAmountEdit.setText(s.toString())
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })

        val tvAmount = findViewById<TextView>(R.id.tvAmount)


        tvAmount.setText(String.format("%.2f", (totalValue)))
        tvAmountEdit.setText(String.format("%.2f", (totalValue)))

        btnCash1.text = strAmount[0].toString()
        btnCash2.text = strAmount.get(1).toString()
        btnCash3.text = strAmount.get(2).toString()

        btnCash1.setOnClickListener {
            tvAmountEdit.text = btnCash1.text.toString()
        }

        btnCash2.setOnClickListener {
            tvAmountEdit.text = btnCash2.text.toString()
        }

        btnCash3.setOnClickListener {
            tvAmountEdit.text = btnCash3.text.toString()
        }

        btnCash.setOnClickListener {
            paymentType = AppConstant.CASH
            manageViews()
        }

        btnExternal.setOnClickListener {
           // paymentType = AppConstant.EXTERNAL
            manageViews()
        }

        ivCancel.setOnClickListener {
            openPaymentCancellationDialog(this)
        }
        btnTender.setOnClickListener {
            try {
                //HardwareBase.BBV2_Controller(mActivity)

                if (tvAmountEdit.text.toString() == "0.") {
                    Toast.makeText(
                        context,
                        context.getString(R.string.enter_valid_amount),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                    return@setOnClickListener
                } else if (tvAmountEdit.text.toString().toDouble() <= 0) {
                    Toast.makeText(
                        context,
                        context.getString(R.string.enter_valid_amount),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                    return@setOnClickListener
                }

//                var activityTemp = (activity as ParentActivity)
//                activityTemp.openCashBox()
                //HardwareBase.BBV2_Controller(mActivity)

                if (tvAmountEdit.text.toString() == "0.") {
                    Toast.makeText(context, context.getString(R.string.enter_valid_amount), Toast.LENGTH_SHORT)
                        .show()
                    return@setOnClickListener
                }
                else if (tvAmountEdit.text.toString().toDouble() <=0 ) {
                    Toast.makeText(context, context.getString(R.string.enter_valid_amount), Toast.LENGTH_SHORT)
                        .show()
                    return@setOnClickListener
                }



                var remaining_amount =
                    tvAmountEdit.text.toString().toDouble() - totalValue
                if (remaining_amount < 0) {
                    Toast.makeText(
                        context,
                        "Please pay remaining amount." + String.format(
                            "%.2f",
                            (remaining_amount * -1)
                        ),
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    dismiss()
//                    openPassCodeDialog("NewSale",remaining_amount, tvAmountEdit.text.toString()) {}

                }
            } catch (e: NumberFormatException) {
                e.printStackTrace()
                Toast.makeText(context, "Please enter proper amount", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun openPaymentCancellationDialog(dialog_previous: Dialog) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setContentView(R.layout.dialog_cancel_payment)

        dialog.setCancelable(false)
        dialog.show()

        val btnNo = dialog.findViewById<Button>(R.id.btnNo)
        val btnYes = dialog.findViewById<Button>(R.id.btnYes)
        btnNo.setOnClickListener {
            dialog.dismiss()
        }

        btnYes.setOnClickListener {
            dialog.dismiss()
            dialog_previous.dismiss()
        }

    }

    private  fun manageViews()
    {
        if (paymentType == AppConstant.CASH) {
            btnCash.background = context.resources.getDrawable(R.drawable.btn_bg)
            btnCash.setTextColor(context.getColor(R.color.colorWhite))

            btnExternal.setTextColor(context.getColor(R.color.colorPrimary))
            btnExternal.background = context.getDrawable(R.drawable.edt_bg)

            btnOther.setTextColor(context.getColor(R.color.colorPrimary))
            btnOther.background = context.getDrawable(R.drawable.edt_bg)

            llCash.visibility = View.VISIBLE
            relCash.visibility = View.VISIBLE
            llExternal.visibility = View.GONE
            btnPay.visibility = View.GONE
        } else {
            btnExternal.background = context.resources.getDrawable(R.drawable.btn_bg)
            btnExternal.setTextColor(context.getColor(R.color.colorWhite))

            btnCash.setTextColor(context.getColor(R.color.colorPrimary))
            btnCash.background = context.getDrawable(R.drawable.edt_bg)

            btnOther.setTextColor(context.getColor(R.color.colorPrimary))
            btnOther.background = context.getDrawable(R.drawable.edt_bg)

            llCash.visibility = View.GONE
            relCash.visibility = View.GONE
            llExternal.visibility = View.VISIBLE
            btnPay.visibility = View.VISIBLE
        }
    }
}