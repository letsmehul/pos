package com.ehsm.recore.utils

import android.app.Dialog
import android.content.Context
import android.text.TextUtils
import android.view.View
import android.view.Window
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.widget.AppCompatSpinner
import com.ehsm.recore.R
import com.ehsm.recore.Recore
import com.ehsm.recore.model.TerminalModel

class TerminalDialog(context: Context,terminals: List<TerminalModel>) : Dialog(context){
    var selectedItem :TerminalModel?=null
    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_show_terminals)
        setCancelable(false)

        window!!.setBackgroundDrawableResource(android.R.color.transparent)
        val spinnerTerminals = findViewById<AppCompatSpinner>(R.id.spinnerTerminal)
        val adapterTerminals =
            ArrayAdapter(context, R.layout.item_spinner_terminal, terminals)
        adapterTerminals.setDropDownViewResource(android.R.layout.simple_list_item_single_choice)
        spinnerTerminals.adapter = adapterTerminals
        var terminal: String? = ""
        Recore.getApp()?.applicationContext?.let {
            val sharedPref = CM.getEncryptedSharedPreferences(it)
            terminal = sharedPref.getString(AppConstant.HSN, "")
        }

        if (!TextUtils.isEmpty(terminal)) {

            for(item :TerminalModel in terminals){
                if(item.hsn == terminal)
                {
                    selectedItem =item
                }
            }
            selectedItem?.let {
                val index = terminals.indexOf(it)
                spinnerTerminals.setSelection(index)
            }
        }

        spinnerTerminals?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long) {
                selectedItem = terminals[position]
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
    }
}