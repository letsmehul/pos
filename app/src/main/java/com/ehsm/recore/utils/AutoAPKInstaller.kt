package com.ehsm.recore.utils

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.util.Log
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProviders
import com.ehsm.recore.BuildConfig
import com.ehsm.recore.R
import com.ehsm.recore.activities.LandingPageActivity
import com.ehsm.recore.network.RestClient
import com.ehsm.recore.viewmodel.APKVersionCheckViewModel
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*

object AutoAPKInstaller {

    private var mAPKVersionCheckViewModel : APKVersionCheckViewModel? = null

    fun webCallAPKVersionCheck(mContext: LandingPageActivity) {
        var VersionCode = BuildConfig.VERSION_CODE
        Log.e("DEBUG", "Web Call Version Code -- "+ VersionCode)
        mAPKVersionCheckViewModel = ViewModelProviders.of(mContext).get(APKVersionCheckViewModel::class.java)
        mAPKVersionCheckViewModel?.getAPKVersion(VersionCode.toString())?.observe(mContext, androidx.lifecycle.Observer { mAPKVersionModel ->
            if (mAPKVersionModel.data == null) {
                Log.e("DEBUG", "latest_apk_url   "+ mAPKVersionModel.latest_apk_url)
                DialogUtility.openSingleActionDialog(mContext.getString(R.string.auto_install_apk_msg), "Install", mContext){
                    if(it) {
                        DownloadLatestAPK(mContext, mAPKVersionModel.latest_apk_url)
                    }
                }
            }
        })
    }

    fun DownloadLatestAPK(mContext: Activity, latestApkUrl: String?) {
        var activityTemp = (mContext as LandingPageActivity)
        activityTemp.showKcsDialog()
        Log.e("DEBUG", "DownloadLatestAPK latest_apk_url   " + latestApkUrl)
        RestClient.getService()?.downloadFileFromUrl(latestApkUrl)?.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                val writtenToDisk: Boolean = writeResponseBodyToDisk(response.body()!!, mContext)
                Log.e("DEBUG", "File Downloded onResponse Status - "+writtenToDisk)
                if(writtenToDisk){
                    mContext.dismissKcsDialog()
                    installLatestAPK(mContext)
                }

            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                CM.showToast(
                    mContext,
                    "APK Download Failed",
                    R.drawable.icon,
                    null
                )
                Log.e("DEBUG", "File Download onFailure - "+t.message)
            }
        })
    }

    fun installLatestAPK(mContext: Activity) {
        val file = File(
            mContext!!.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            "Recore_AutoInstall.apk"
        )
        Log.e("APK_DEBUG", "filepath - " + file.absolutePath)
        if (file.exists()) {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.setDataAndType(
                uriFromFile(File(file.absolutePath), mContext),
                "application/vnd.android.package-archive"
            )
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            try {
                startActivityForResult(mContext!!, intent, CV.INSTALL_APK_CODE, null)
            } catch (e: ActivityNotFoundException) {
                e.printStackTrace()
                CM.showToast(
                    mContext,
                    "Error in opening the file!",
                    R.drawable.icon,
                    null
                )
                Log.e("APK_DEBUG", "Error in opening the file!")
            }
        }
    }

    fun uriFromFile(file: File?, mContext: Activity): Uri? {
        var mFileUri: Uri? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (file != null) {
                mFileUri = FileProvider.getUriForFile(
                    mContext!!,
                    mContext!!.getPackageName() + ".provider",
                    file
                );
            }
        } else {
            mFileUri = Uri.fromFile(file)
        }
        return mFileUri
    }

    fun writeResponseBodyToDisk(body: ResponseBody, mContext: Activity): Boolean {
        return try {
            val mLatestAPKFile = File(
                mContext!!.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                "Recore_AutoInstall.apk"
            )
            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null
            try {
                val fileReader = ByteArray(4096)
                val fileSize: Long = body.contentLength()
                var fileSizeDownloaded: Long = 0
                inputStream = body.byteStream()
                outputStream = FileOutputStream(mLatestAPKFile)
                while (true) {
                    val read = inputStream.read(fileReader)
                    if (read == -1) {
                        break
                    }
                    outputStream!!.write(fileReader, 0, read)
                    fileSizeDownloaded += read.toLong()
                    Log.d(
                        "DEBUG",
                        "file download: $fileSizeDownloaded of $fileSize"
                    )
                }
                outputStream!!.flush()
                true
            } catch (e: IOException) {
                Log.e("DEBUG", "Exeption -  " + e)

                CM.showToast(
                    mContext,
                    "APK File not found",
                    R.drawable.icon,
                    null
                )
                false
            } finally {
                inputStream?.close()
                outputStream?.close()
            }
        } catch (e: IOException) {
            Log.e("DEBUG", "IOException -  " + e)
            CM.showToast(
                mContext,
                "APK File not found",
                R.drawable.icon,
                null
            )
            false
        }
    }
}