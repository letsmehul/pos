package com.ehsm.recore.utils

import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.widget.EditText
import java.math.BigDecimal

class GenericTextWatcher(editText: EditText) : TextWatcher {
    var edt : EditText = editText
    private var lastValue = ""
    override fun beforeTextChanged(
        charSequence: CharSequence,
        i: Int,
        i1: Int,
        i2: Int
    ) {
    }

    override fun onTextChanged(
        charSequence: CharSequence,
        i: Int,
        i1: Int,
        i2: Int
    ) {
    }

    override fun afterTextChanged(editable: Editable) {
        try {
            val newValue: String = edt.getText().toString()

            if(!TextUtils.isEmpty(editable.toString()) && newValue != lastValue){
                lastValue = newValue
                val inputString = editable.toString()
                edt.removeTextChangedListener(this)
                val cleanString = inputString.replace("[.]".toRegex(), "")
                val bigDecimal: BigDecimal = BigDecimal(cleanString).setScale(2, BigDecimal.ROUND_FLOOR).divide(BigDecimal(100), BigDecimal.ROUND_FLOOR)
                val converted: String = bigDecimal.toString()
                edt.setText(converted)
                edt.setSelection(converted.length)
                edt.addTextChangedListener(this)
            }

        }catch (e : Exception){
            e.printStackTrace()
        }

    }

}