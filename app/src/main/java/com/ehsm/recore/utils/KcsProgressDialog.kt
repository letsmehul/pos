package com.ehsm.recore.utils

import android.app.Dialog
import android.content.Context
import android.view.Window
import com.bumptech.glide.Glide
import com.ehsm.recore.R
import kotlinx.android.synthetic.main.kcs_progress_percentage.*


class KcsProgressDialog(context: Context, isCancelable: Boolean) : Dialog(context) {

    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.kcs_progress_percentage)
        Glide.with(context).asGif().load(R.raw.loader_gif).into(ivGifImage);
        /*if(isProductSync){
            setContentView(R.layout.kcs_progress_percentage)
        }else {
            setContentView(R.layout.kcs_progress_dialog)
        }*/
        setCancelable(isCancelable)
        window!!.setBackgroundDrawableResource(android.R.color.transparent)
    }
}

