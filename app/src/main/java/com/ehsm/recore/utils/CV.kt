package com.ehsm.recore.utils

import com.ehsm.recore.model.OrderTransModel
import java.util.*

object CV {
    var ISSYNCOPERATIONFINISHED: String = "IS_SYNC_OPERATION_FINISHED"
    val HEADER_CONTENT_KEY = "Content-Type"
    val HEADER_CONTENT_TYPE = "application/x-www-form-urlencoded"
    //var BASE_URL = "http://letsenkindle.in/recore/" //Live Url
    var SYMBOL = "A"

    //var BASE_URL = "http://172.16.3.122/recore/"
    var BASE_URL = "http://recorepos.com/"
    var BASE_24_7_URL = "https://zoopapps.com/"
    //You can Also Change SYMBOL Variable (SYMBOL variable means 'L' FOR LOCAL AND 'A' FOR AWS) When Change BASE_URL
    var API_KEY = "Api-Key"
    var ISLOGIN = "isLogin"
    var ISLOGINCALLED = "ISLOGINCALLED"
    var IS_PASSCODE_SUBMITED = "isPasscodeSubmitted"
    var ORDER_ID = "order_id"
    var EMP_NAME = "EMP_NAME"
    var EMP_IMAGE = "EMP_IMAGE"
    var EMP_OPEN_DISCOUNT = "EMP_OPEN_DISCOUNT"
    var EMP_ID = "EMP_ID"
    var EMP_ROLE = "EMP_ROLE"
    var POS_NAME = "POS_NAME"
    var POS_NAME_SPLITTER = "POS"
    var CURRENCY_CODE = "CURRENCY_CODE"
    var PAYMENT_TYPE = "PAYMENT_TYPE"
    var STORE_ID = "STORE_ID"
    var POS_ID = "POS_ID"
    var ISCHECKIN = "isCheckIn"
    var SHIFTSTARTEMPID = "SHIFT_START_EMPID"
    var SHIFTSTARTEMPNAME = "SHIFT_START_EMPNAME"
    var SHIFTSTARTEMPROLE = "SHIFT_START_EMPROLE"
    var STRFACEBOOK = "strFacebook"
    var STRINSTAGRAM = "strInstagram"
    var STRTWITTER = "strTwitter"
    var STRYOUTUBE = "strYoutube"
    var STRSOCIALMEDIA = "strSocialMedia"
    var STRTIMEZONE="strTimeZone"

    //    var IS_HOLD_ORDER = "IS_HOLD_ORDER"
    var CHECKINUSER = "CheckInUser"
    val DISPLAY_DATE_FORMATTE_AMPM = "MM-dd-yyyy  hh:mm a"
    val DISPLAY_DATE_FORMATTE = "MM-dd-yyyy"
    val LOCALE_USE_DATEFORMAT = Locale.US
    val WS_DATE_FORMATTE = "yyyy-MM-dd"
    val WS_DATE_FORMATTE_AM_PM = "yyyy-MM-dd HH:mm:ss"

    var STORE_NAME = "StoreName"
    var STORE_ADD_LINE1 = "AddLine1"
    var STORE_ADD_LINE2 = "AddLine2"
    var STORE_TEL_NUM = "StoreTelNum"
    var STORE_LOGO_URL = "StoreLogo"
    var SIGNATURE_AMOUNT = "SignatureAmt"
    var IS_ADMIN_APPROVAL_REQUIRED_FOR_TRANSACTION = "isAdminApprovalRequiredForTransaction"

    var PROMO_CONTENT_TYPE = "PromoContenttype"

    var STORE_LOGO_LOCAL_PATH = "LocalStoredLogo"
    var FULL_SCREEN_LOCAL_PATH = "LocalStoredFullScreen"
    var SQUARE_LOGO_PATH = "SquareLogo"
    var SQUARE_LOGO_LOCAL_PATH = "LocalStoredSquareLogo"
    var PROMO_VIDEO_PATH = "PromoVideoURL"
    var PROMO_CONTENT_PATH = "PromoRemoteURL"

    var STORE_LOGO_IMAGE_NAME = "StoredLogo.png"
    var FULL_SCREEN_IMAGE_NAME = "FullScreen.png"
    var SQUARE_LOGO_IMAGE_NAME = "SquareLogo.png"
//    var PROMO_IMAGE_IMAGE_NAME = "PromoImage.png"


    var SIGNATURE_IMAGE_NAME = "Signature"
    var BARCODE_IMAGE_NAME = "Barcode"
    var SQUARE_IMAGE_NAME = "SquareImage"

    var IS_PAY_IN_OUT_ENABLE = "isPayInPayOutEnable"

    //    var SINGLE_DISCOUNT = "simple_discount"
    var PERCENT = "Percent"
    var CHECK = "Check"
    var AMOUNT = "Amount"
    var ITEM = "Item"

//    var COUPON_DISCOUNT = "coupon_discount"

//    var SINGLE_DISCOUNT_TYPE = "Simple Discount"
//    var COUPON_DISCOUNT_TYPE = "Coupon Discount"

    var PAYMENT_TYPE_CASH = "Cash"


    val HEADER_CONTENT_TYPE_BOLT = "application/json"
    val HEADER_Authorization = "Authorization"
    val HEADER_X_CARDCONNECT_SESSIONKEY = "X-CardConnect-SessionKey"

    var IN_STORE = "In Store"
    var ADD = "ADD"
    var NO = "NO"
    var LITE = "LITE"
    var EXTRA = "EXTRA"
    var ON_SIDE = "ON SIDE"
    var SUB = "SUB"
    var REQUIRED = "Required"
    var FORCE_SHOW_OPTIONAL = "Force Show Optional"
    var DEFAULT="DEFAULT"

    var tempHashMap = HashMap<Int, Int>()
    var productRequireHashMap = HashMap<Int, Int>()
    var CASH_IN_DRAWER = "CASH_IN_DRAWER"

    var orderTransList: ArrayList<OrderTransModel> = ArrayList()

    //Sync Date time
    val CAT_SYNC_DATE_TIME: String = "catSyncDateTime"
    val SUB_CAT_SYNC_DATE_TIME: String = "subCatSyncDateTime"
    val PROD_SYNC_DATE_TIME: String = "prodSyncDateTime"
    var LOCK_TIMER = "LOCK_TIMER"

    const val APPLY_DISCOUNT = "ApplyDiscount"

    var PRODUCTS_TYPE_KITCHEN = "Kitchen"
    var PRODUCTS_TYPE_RETAILS = "Retail"

    const val WAS_APPLIED = " was applied"
    const val APPLIED_SUCCESSFULLY = " Applied successfully"
    const val HOLD_ORDER_GENERATED_MSG ="This hold order already generated."
    const val PRINT = "print"
    const val REPRINT = "reprint"
    var STR_RET_REF: String = ""
    var ORDER_DATE: String = ""
    var REMAIN_AMOUNT: Double = 0.0
    var GIVEN_AMOUNT: Double = 0.0
    var server_order_id = 0

    var INSTALL_APK_CODE = 101
}
