package com.ehsm.recore.utils

import com.ehsm.recore.utils.CV.BASE_URL

object AppConstant {

    const val EncryptedSharedPreferenceFileName ="RecoreEncryptedData"

    val MERCHANTID= "merchantId"
    val HEADER_Authorization = "Authorization"
    val HSN = "hsn"
    val HSN_DEFAULT_VALUE ="20114SC25032114";



    // order type
    const val ORDER = "order"
    const val REFUND = "refund"
    const val ON_HOLD = "On Hold"
    const val MAX_ON_HOLD_RECORD =20
    enum class OrderType(val value:Int){
        HOLD(1),
    }

    const val CLOCK_IN = "Clock In"
    const val CLOCK_OUT = "Clock Out"
    const val SHIFT_START = "Shift Start"
    const val SHIFT_CLOSE = "Shift Close"
    const val BREAK_START = "Break Start"
    const val BREAK_END = "Break End"
    var EMP_ID = 0

    const val STAFF = "Staff"
    const val CASHIER = "Cashier"
    const val STORE_ADMIN = "Store Admin"

    const val CHECK_IN_OUT = "check_in_out"
    const val PAY_IN_OUT = "pay_in_out"
    const val DRAWER = "drawer"
    const val NO_SALE = "NoSale"
    const val OTHER = "Other"
    const val NEW_SALE = "NewSale"

    const val CDS_NEWSALE_SCREEN = "NewSale"
    const val CDS_COMPLETE_SCREEN = "Complete"
    const val CDS_REFUND_SCREEN = "Refund"
    const val CDS_HOLD_SCREEN = "Hold"
    const val CDS_PAYMENTDONE_SCREEN = "PaymentDone"
    const val CDS_PAYMENTPROCESS_SCREEN = "PaymentProcess"
    const val CDS_NORMAL_SCREEN = "Normal"
    const val CDS_PAUSE_SCREEN = "Pause"


    const val KITCHEN_PRINTER_IP = "KitchenPrinterIP"

    val IMAGE_PLACEHOLDER_PRODUCT = BASE_URL+"upload/product_images/placeholder.jpg"
    val DISCOUNT_WEBVIEW_URL = BASE_URL+"webview/discount_list/"
    enum class HistoryType(val value:String){
        TODAY("Today"),
        ALL("All"),
        VOID("Void"),
        HOLD("Hold"),
        INPROCESS("In Process"),
    }

    enum class HistoryStatus(val value:String){
        COMPLETEDSTATUS("Completed"),
        HOLDSTATUS("Hold"),
        INPROCESSSTATUS("In Process"),
        VOIDSTATUS("Void"),
    }

    enum class PaymentProcess(val value:Int){
        NONE(0),
        LOADING(1),
        SWIPE(2),
        AUTHENTICATION(3),
        SUCCESS(4),
        FAILED(5),
        CANCEL(6),
        FAILED_REFUND(7),
        SUCCESS_REFUND(8),
        INVALID_CHOICE(9)
    }

    // card type
    const val CASH = "Cash"
    const val CREDIT = "Credit"
    const val DEBIT = "Debit"
    const val APPLE_PAY = "Apple pay"
    const val MANUAL_CARD = "Manual Card"

    //Open DiscountType
    const val AMOUNT = "credit"
    const val PERCENTAGE = "percentage"

    const val SIMPLE_DISCOUNT = "Simple Discount"
    const val COUPON_DISCOUNT = "Coupon Discount"
    const val SALES_DISCOUNT = "Sale Discount"
    const val BOGO_DISCOUNT = "BOGO Discount"

    const val ROLE_ID_ADMIN = 1
    const val ROLE_ID_CASHIER = 2

    const val SPLIT = "Split"
    const val NORMAL = "Normal"



}