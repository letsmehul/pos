package com.ehsm.recore.utils;

import android.content.Context;
import android.graphics.Bitmap;

import com.google.zxing.BarcodeFormat;
import com.journeyapps.barcodescanner.BarcodeEncoder;

public class BarcodeGenerator {

    Context mContext;
    Bitmap bitmap;

    public BarcodeGenerator(Context context){
        this.mContext = context;
    }

    public Bitmap GenerateBarcode(String content){
        try {
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            bitmap = barcodeEncoder.encodeBitmap(content, BarcodeFormat.QR_CODE, 100, 100);

        } catch(Exception e) {

        }

        return bitmap;
    }
}
