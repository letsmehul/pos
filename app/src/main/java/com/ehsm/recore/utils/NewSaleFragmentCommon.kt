package com.ehsm.recore.utils

import android.app.Activity
import android.text.TextUtils
import com.ehsm.recore.viewmodel.DiscountListViewModel
import android.text.Editable
import com.ehsm.recore.model.*
import java.lang.StringBuilder


object NewSaleFragmentCommon {
    fun itemRefundedRemaingQty(
        orderProduct: OrderProductsModel,
        refundItemList: ArrayList<OrderRefundItemModel>
    ): Int {
        var remainingQty = orderProduct.dblItemQty.toInt()
        try {
            val refundTotalProductQty =
                (refundItemList.filter { tItem -> tItem.iProductId == orderProduct.iProductId }).map { it.dblItemQty }
                    .sum()
            return orderProduct.dblItemQty.toInt() - refundTotalProductQty
        } catch (e: Exception) {
            return remainingQty
        }
    }

    fun itemIsRefunded(
        orderProduct: OrderProductsModel,
        refundItemList: ArrayList<OrderRefundItemModel>
    ): Boolean {
        var isRefunded = false
        try {
            val refundTotalProductQty =
                (refundItemList.filter { tItem -> tItem.iProductId == orderProduct.iProductId }).map { it.dblItemQty }
                    .sum()
            if (orderProduct.dblItemQty.toInt() - refundTotalProductQty == 0) {
                isRefunded = true
            }
        } catch (e: Exception) {
            return isRefunded
        }
        return isRefunded
    }

    fun isValidateCartDiscountedProduct(
        discountListViewModel: DiscountListViewModel,
        refundProductId: Int,
        selectedDiscountId: Int
    ): Boolean {
        return discountListViewModel.checkDiscountMappingItemIsAvailable(
            refundProductId.toLong(),
            selectedDiscountId.toLong()
        )
    }

    fun taxAmountCalculation(item: OrderTransModel, discountAmount: Double): Double {
        var taxAmount = 0.0
        val amount = (item.itemQty * item.itemRate) - discountAmount
        var txtRateInPercentage = 0.0
        var taxrate = 0.0
        if (!TextUtils.isEmpty(item.taxPercentage)) {
            txtRateInPercentage = item.taxPercentage.toDouble()
            taxrate = (txtRateInPercentage / 100)
        }

        taxAmount = amount * taxrate
        taxAmount = String.format("%.2f", (taxAmount)).toDouble()
        return taxAmount
    }

    fun floatToDouble(amount: Float): Double {
        return String.format("%.2f", (amount)).toDouble()
    }

    fun floatFormatedAmount(amount: Float): Float {
        return String.format("%.2f", (amount)).toFloat()
    }

    fun isInputCorrect(
        s: Editable,
        size: Int,
        dividerPosition: Int,
        divider: Char
    ): Boolean {
        var isCorrect = s.length <= size
        for (i in 0 until s.length) {
            isCorrect = if (i > 0 && (i + 1) % dividerPosition == 0) {
                isCorrect and (divider == s[i])
            } else {
                isCorrect and Character.isDigit(s[i])
            }
        }
        return isCorrect
    }

    fun concatString(digits: CharArray, dividerPosition: Int, divider: Char): String? {
        val formatted = StringBuilder()
        for (i in digits.indices) {
            if (digits[i].toInt() != 0) {
                formatted.append(digits[i])
                if (i > 0 && i < digits.size - 1 && (i + 1) % dividerPosition == 0) {
                    formatted.append(divider)
                }
            }
        }
        return formatted.toString()
    }

    fun getDigitArray(s: Editable, size: Int): CharArray? {
        val digits = CharArray(size)
        var index = 0
        var i = 0
        while (i < s.length && index < size) {
            val current = s[i]
            if (Character.isDigit(current)) {
                digits[index] = current
                index++
            }
            i++
        }
        return digits
    }

    fun isOtherCashTrasaction(
        paymentMethodList: List<PaymentMethodModelData>?,
        paymentType: String
    ): Boolean {
        if (paymentMethodList != null && paymentMethodList.size > 0)
            return paymentMethodList.any { item -> item.strPaymentMethod == paymentType }
        else
            return false
    }

    fun setShiftDetails(
        activity: Activity,
        loginResponseModel: DataWrapper<POSLastStatusResponseModel>
    ){
        CM.setSp(activity, CV.SHIFTSTARTEMPID, loginResponseModel.data!!.data.iEmployeeId)
        CM.setSp(activity, CV.SHIFTSTARTEMPNAME, loginResponseModel.data!!.data.emp_name)
    }
    fun resetShiftDetails(activity: Activity){
        CM.setSp(activity, CV.SHIFTSTARTEMPID, 0)
    }

}