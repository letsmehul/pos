package com.ehsm.recore.utils

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.CountDownTimer
import android.os.Handler
import android.util.Log
import android.view.Window
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import com.ehsm.recore.R
import com.ehsm.recore.activities.*
import com.ehsm.recore.model.CDSSalesModel
import java.util.*

/*
 * Developer email: gargi.das@hotmail.com
 * GitHub: https://github.com/bxute
 */
object ScreenLockTimer {
    private var START_TIME_IN_MILLIS: Long = 300000
    private var mCountDownTimer: CountDownTimer? = null
    var mTimerRunning = false
    private var mTimeLeftInMillis: Long = 16000
    var handler: Handler = Handler()
    lateinit var  mActivity: Activity

    private fun openTimerDialog(activity: Activity) {
        val dialogTimer = Dialog(activity)
        dialogTimer.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogTimer.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialogTimer.setContentView(R.layout.dialog_timer)
        dialogTimer.setCancelable(true)


        var dismissBtn = dialogTimer.findViewById<AppCompatButton>(R.id.btn_dismiss)

        dialogTimer.setOnCancelListener {
            mCountDownTimer!!.cancel()
            handler.removeCallbacks(runnableCode)
            resetTimer()
            handler.postDelayed(runnableCode, START_TIME_IN_MILLIS)
        }

        dismissBtn.setOnClickListener {
            dialogTimer.dismiss()
            mCountDownTimer!!.cancel()
            handler.removeCallbacks(runnableCode)
            resetTimer()
            handler.postDelayed(runnableCode, START_TIME_IN_MILLIS)
        }

        val tvTimer = dialogTimer.findViewById<AppCompatTextView>(R.id.text_timer)
        updateCountDownText(tvTimer)

        startTimer(tvTimer,dialogTimer)
        dialogTimer.show()
    }

    private fun startTimer(
        timer: AppCompatTextView,
        dialogTimer: Dialog
    ) {
        mCountDownTimer = object : CountDownTimer(mTimeLeftInMillis, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                mTimeLeftInMillis = millisUntilFinished
                mTimerRunning = true
                updateCountDownText(timer)
                Log.e("BASE","Start")

            }

            override fun onFinish() {
                timer.setText("00:00")
                mTimerRunning = false
                dialogTimer.dismiss()
                AppConstant.EMP_ID = CM.getSp(mActivity, CV.EMP_ID, 0) as Int
                CM.startActivity(mActivity, PasscodeActivity::class.java)
                Log.e("BASE","Finish")
            }
        }.start()
    }

    fun stopTimer() {
        if(mTimerRunning){
            mCountDownTimer!!.cancel()
            mTimerRunning = false
            Log.e("BASE","stopTimer")
        }
    }

    private fun resetTimer() {
        mTimeLeftInMillis = 16000
        START_TIME_IN_MILLIS = CM.getSp(mActivity, CV.LOCK_TIMER, 0L) as Long - mTimeLeftInMillis
        updateCountDownText(null)
        Log.e("BASE","Reset: ")
    }

    fun updateCountDownText(timerTextView: AppCompatTextView?){
        val minutes = (mTimeLeftInMillis / 1000).toInt() / 60
        val seconds = (mTimeLeftInMillis / 1000).toInt() % 60
        val timeLeftFormatted: String =
            java.lang.String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds)
        Log.e("BASE", "minutes: $minutes")
        Log.e("BASE", "seconds: $seconds")

        timerTextView?.text = timeLeftFormatted
    }

    fun registerLockListener(lock : AppCompatImageView,Pactivity : Activity){
        lock.setOnClickListener {
            var currentdata : String?=Pactivity.javaClass.simpleName

            if (currentdata.equals("LandingPageActivity")) {
                var baseActivity = (mActivity as LandingPageActivity)
                baseActivity.checkDrawOverlayPermission(
                        AppConstant.CDS_NORMAL_SCREEN,
                        "",
                        false,
                        "",
                        CDSSalesModel(),
                        "",
                        false
                )
            }else if (currentdata.equals("CheckInOutActivity"))
            {
                var baseActivity = (mActivity as CheckInOutActivity)
                baseActivity.checkDrawOverlayPermission(
                        AppConstant.CDS_NORMAL_SCREEN,
                        "",
                        false,
                        "",
                        CDSSalesModel(),
                        "",
                        false
                )

            }else if (currentdata.equals("PayInOutActivity"))
            {
                var baseActivity = (mActivity as PayInOutActivity)
                baseActivity.checkDrawOverlayPermission(
                        AppConstant.CDS_NORMAL_SCREEN,
                        "",
                        false,
                        "",
                        CDSSalesModel(),
                        "",
                        false
                )
            }else if (currentdata.equals("DayStartCloseActivity"))
            {
                var baseActivity = (mActivity as DayStartCloseActivity)
                baseActivity.checkDrawOverlayPermission(
                        AppConstant.CDS_NORMAL_SCREEN,
                        "",
                        false,
                        "",
                        CDSSalesModel(),
                        "",
                        false
                )
            }
            mTimerRunning = false
            stopHandler()
            AppConstant.EMP_ID = CM.getSp(mActivity, CV.EMP_ID, 0) as Int
            CM.setSp(mActivity,CV.IS_PASSCODE_SUBMITED,false)
            CM.startActivity(mActivity, PasscodeActivity::class.java)
            Log.e("BASE","OnLock")
        }
    }

    private val runnableCode = Runnable { // Do something here on the main thread
        openTimerDialog(mActivity)
        Log.e("Handlers", "Called on main thread")
    }

    open fun stopHandler() {
        handler.removeCallbacks(runnableCode)
        stopTimer()
        resetTimer()
    }

    open fun startHandler(activity: Activity) {
        mActivity = activity
        resetTimer()
        START_TIME_IN_MILLIS = CM.getSp(mActivity, CV.LOCK_TIMER, 0L) as Long - mTimeLeftInMillis
        Log.e("====","=== show timer ===="+CM.getSp(mActivity,CV.IS_PASSCODE_SUBMITED,false))
        if(CM.getSp(mActivity,CV.IS_PASSCODE_SUBMITED,false) as Boolean){
            handler.postDelayed(runnableCode, START_TIME_IN_MILLIS)
        }
    }
}