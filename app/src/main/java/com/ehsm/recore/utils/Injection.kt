package com.ehsm.recore.utils

import android.content.Context
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.database.dao.ItemListDAO
import com.ehsm.recore.viewmodelfactory.ViewModelFactory

object Injection {

    private fun provideTaskDataSource(context: Context): ItemListDAO {
        val database = RecoreDB.getInstance(context)!!
        return database.daoItemList()
    }

    fun provideViewModelFactory(context: Context): ViewModelFactory {
        val taskDao = provideTaskDataSource(context)
        return ViewModelFactory(taskDao)
    }
}