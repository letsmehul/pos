package com.ehsm.recore.utils

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getColor
import com.ehsm.recore.R
import com.ehsm.recore.model.PaymentMethodModelData
import com.ehsm.recore.utils.CM.isEmailValid
import kotlinx.android.synthetic.main.custom_toast.*

class DialogUtility {
    companion object {
        val msgIncorrectMPin = "Incorrect M-pin Entered!"
        val actionIncorrectMPin = "Re Enter M-pin"

        fun showPinInvalidateDialog(
            activity: AppCompatActivity?,
            msg: String = msgIncorrectMPin,
            label: String = actionIncorrectMPin
        ) {
            activity?.let {
                val dialog = Dialog(it)
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog.setContentView(R.layout.dialog_wrong_mpin)
                dialog.setCancelable(false)
                dialog.show()

                (dialog.findViewById<TextView>(R.id.tvLabel)).text = msg
                val btnReEnterMpin = dialog.findViewById<Button>(R.id.btnReEnterMpin)
                if (TextUtils.isEmpty(label)) {
                    btnReEnterMpin.text = it.getString(R.string.dismiss)
                } else {
                    btnReEnterMpin.text = label
                }

                btnReEnterMpin.setOnClickListener {
                    dialog.dismiss()
                }
            }

        }

        fun openPaymentCancellationDialog(
            activity: AppCompatActivity?,
            callback: (isYes: Boolean) -> Unit
        ) {
            activity?.let {
                val dialog = Dialog(activity)
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                dialog.setContentView(R.layout.dialog_cancel_payment)

                dialog.setCancelable(false)
                dialog.show()

                val btnNo = dialog.findViewById<Button>(R.id.btnNo)
                val btnYes = dialog.findViewById<Button>(R.id.btnYes)
                btnNo.setOnClickListener {
                    dialog.dismiss()
                    callback(false)
                }

                btnYes.setOnClickListener {
                    dialog.dismiss()
                    callback(true)

                }

            }
        }

        fun openActionDialogwithMessage(
            mTitle: String,
            activity: AppCompatActivity?,
            callback: (isYes: Boolean) -> Unit
        ) {
            activity?.let {
                val dialog = Dialog(activity)
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                dialog.setContentView(R.layout.dialog_cancel_payment)

                dialog.setCancelable(false)
                dialog.show()

                val btnNo = dialog.findViewById<Button>(R.id.btnNo)
                val btnYes = dialog.findViewById<Button>(R.id.btnYes)
                val tvMsg = dialog.findViewById<TextView>(R.id.tvMsg)

                tvMsg.setText(mTitle)
                btnNo.setOnClickListener {
                    dialog.dismiss()
                    callback(false)
                }

                btnYes.setOnClickListener {
                    dialog.dismiss()
                    callback(true)

                }

            }
        }
        fun openActionDialogwithMessage_Retry_Cancel_Btn(
            mTitle: String,
            activity: AppCompatActivity?,
            callback: (isYes: Boolean) -> Unit
        ) {
            activity?.let {
                val dialog = Dialog(activity)
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                dialog.setContentView(R.layout.dialog_retry_cancel_order)

                dialog.setCancelable(false)
                dialog.show()

                val btnRetry = dialog.findViewById<Button>(R.id.btnRetry)
                val btnCancel = dialog.findViewById<Button>(R.id.btnCancel)
                val tvMsg = dialog.findViewById<TextView>(R.id.tvMsg)

                tvMsg.setText(mTitle)
                btnCancel.setOnClickListener {
                    dialog.dismiss()
                    callback(false)
                }

                btnRetry.setOnClickListener {
                    dialog.dismiss()
                    callback(true)

                }

            }
        }

        fun openSingleActionDialog(
            mMessage: String,
            mActionText: String,
            activity: AppCompatActivity?,
            mTitle: String = "Recore",
            callback: (isYes: Boolean) -> Unit
        ) {
            activity?.let {
                val dialog = Dialog(activity)

                Log.e("DEBUG", "New Dialog Created")
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog?.window?.setCallback(
                        UserInteractionAwareCallback(
                                dialog?.window!!.getCallback(),
                                activity
                        )
                )
                dialog.setContentView(R.layout.dialog_common_single_action)
                dialog.setCancelable(false)
                dialog.show()

                val btnYes = dialog.findViewById<Button>(R.id.btnok)
                val tvMsg = dialog.findViewById<TextView>(R.id.tvLabel)
                val tvTitle = dialog.findViewById<TextView>(R.id.tvTitle)

                tvTitle.setText(mTitle)
                tvMsg.setText(mMessage)
                btnYes.setText(mActionText)
                btnYes.setOnClickListener {
                    dialog.dismiss()
                    Log.e("DEBUG", "Dialog Dismiss")
                    callback(true)
                }

            }
        }

        fun openDoubleActionDialog(
            activity: AppCompatActivity?,
            mTitle: String,
            callback: (isYes: Boolean) -> Unit
        ) {
            activity?.let {
                val dialog = Dialog(activity)
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog?.window?.setCallback(
                        UserInteractionAwareCallback(
                                dialog?.window!!.getCallback(),
                                activity
                        )
                )
                dialog.setContentView(R.layout.dialog_common_two_action)

                dialog.setCancelable(false)
                dialog.show()

                val btnNo = dialog.findViewById<Button>(R.id.btnNo)
                val btnYes = dialog.findViewById<Button>(R.id.btnYes)
                val tvTitle = dialog.findViewById<TextView>(R.id.tvTitle)

                tvTitle.setText(mTitle)
                btnYes.setText("Send to kitchen")
                btnNo.setText("Don't Send")
                btnNo.setOnClickListener {
                    dialog.dismiss()
                    callback(true)
                }

                btnYes.setOnClickListener {
                    dialog.dismiss()
                    callback(false)

                }
            }
        }

        fun openDoubleActionModifierDialog(
            activity: AppCompatActivity?,
            mTitle: String,
            mheader: String,
            mIngredient: String,
            callback: (isYes: Boolean) -> Unit
        ) {
            activity?.let {
                val dialog = Dialog(activity)
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog?.window?.setCallback(
                    UserInteractionAwareCallback(
                        dialog?.window!!.getCallback(),
                        activity
                    )
                )
                dialog.setContentView(R.layout.dialog_modifier_common_two_action)
                dialog.show()
                val btnNo = dialog.findViewById<Button>(R.id.btnNo)
                val btnYes = dialog.findViewById<Button>(R.id.btnYes)
                val tvTitle = dialog.findViewById<TextView>(R.id.tvTitle)
                val tvheader = dialog.findViewById<TextView>(R.id.tvheader)
                val tvIngredient = dialog.findViewById<TextView>(R.id.tvIngredient)
                tvTitle.setText(mTitle)
                btnYes.setText(activity.getString(R.string.repeat_last))
                btnNo.setText(activity.getString(R.string.select_new))
                tvheader.setText(mheader)
                tvIngredient.setText(mIngredient)
                btnNo.setOnClickListener {
                    dialog.dismiss()
                    callback(true)
                }
                btnYes.setOnKeyListener { view, i, keyEvent ->
                    return@setOnKeyListener true
                }
                btnYes.setOnClickListener {
                    dialog.dismiss()
                    callback(false)
                }
            }
        }

        fun openRefundCheckOutDialog(
            mActivity: AppCompatActivity?,
            payment_type: String,
            strCardLastDigits: String,
            strCardHolderName: String,
            mTotalRefundAmount: Double,
            mTotalTax: Double,
            paymentMethodList:ArrayList<PaymentMethodModelData>?,
            callBack: (paymentType: String) -> Unit
        ) {
            mActivity?.let { activity ->
                var currency_symbol = CM.getSp(activity, CV.CURRENCY_CODE, "") as String
                val dialogcheckout = Dialog(activity)
                dialogcheckout.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialogcheckout.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialogcheckout.window!!.setCallback(
                        UserInteractionAwareCallback(
                                dialogcheckout.window!!.getCallback(),
                                mActivity
                        )
                )
                dialogcheckout.setContentView(R.layout.dialog_refundcheckout)

                dialogcheckout.setCancelable(false)
                dialogcheckout.show()

                val ivCancel = dialogcheckout.findViewById<ImageView>(R.id.ivCancel)
                val btnok = dialogcheckout.findViewById<Button>(R.id.btnok)

                val btnCash = dialogcheckout.findViewById<Button>(R.id.btnCash)
                val btnOtherPayment = dialogcheckout.findViewById<Button>(R.id.btnOtherPayment)

                val btnCredit = dialogcheckout.findViewById<Button>(R.id.btnCredit)
                val btnDebit = dialogcheckout.findViewById<Button>(R.id.btnDebit)
                val btnApplePay = dialogcheckout.findViewById<Button>(R.id.btnApplePay)
                val btnManualCard = dialogcheckout.findViewById<Button>(R.id.btnManualCard)
                val btnOther = dialogcheckout.findViewById<Button>(R.id.btnOther)
                val llExternalDetail =
                    dialogcheckout.findViewById<LinearLayout>(R.id.llExternalDetail)
                val tvCardNumber = dialogcheckout.findViewById<TextView>(R.id.tvCardNumber)
                val tvCardHolderName = dialogcheckout.findViewById<TextView>(R.id.tvCardHolderName)

                var paymentType = payment_type
                fun manageViews() {
                    if (paymentType == AppConstant.CASH) {
                        btnCash.background = activity.resources.getDrawable(R.drawable.btn_bg)
                        btnCash.setTextColor(activity.getColor(R.color.colorWhite))

                        btnOther.setTextColor(activity.getColor(R.color.colorPrimary))
                        btnOther.background = activity.getDrawable(R.drawable.edt_bg)

                        btnCash.visibility = View.VISIBLE
                        btnCredit.visibility = View.GONE
                        btnDebit.visibility = View.GONE
                        btnApplePay.visibility = View.GONE
                        btnOtherPayment.visibility = View.GONE
                        btnManualCard.visibility = View.GONE

                        llExternalDetail.hide()
                    } else if (NewSaleFragmentCommon.isOtherCashTrasaction(paymentMethodList,paymentType)) {
                        btnOtherPayment.setText(payment_type)
                        btnOtherPayment.background = activity.resources.getDrawable(R.drawable.btn_bg)
                        btnOtherPayment.setTextColor(activity.getColor(R.color.colorWhite))

                        btnCash.setTextColor(activity.getColor(R.color.colorPrimary))
                        btnCash.background = activity.getDrawable(R.drawable.edt_bg)

                        btnOther.setTextColor(activity.getColor(R.color.colorPrimary))
                        btnOther.background = activity.getDrawable(R.drawable.edt_bg)

                        btnCash.visibility = View.GONE
                        btnCredit.visibility = View.GONE
                        btnDebit.visibility = View.GONE
                        btnApplePay.visibility = View.GONE
                        btnManualCard.visibility = View.GONE

                        llExternalDetail.hide()
                    } else if (paymentType == AppConstant.CREDIT) {
                        btnCredit.background = activity.resources.getDrawable(R.drawable.btn_bg)
                        btnCredit.setTextColor(activity.getColor(R.color.colorWhite))

                        btnCash.setTextColor(activity.getColor(R.color.colorPrimary))
                        btnCash.background = activity.getDrawable(R.drawable.edt_bg)

                        btnOtherPayment.setTextColor(activity.getColor(R.color.colorPrimary))
                        btnOtherPayment.background = activity.getDrawable(R.drawable.edt_bg)

                        btnOther.setTextColor(activity.getColor(R.color.colorPrimary))
                        btnOther.background = activity.getDrawable(R.drawable.edt_bg)
                        tvCardNumber.text = String.format(
                            tvCardNumber.context.getString(R.string.mask_card_number),
                            strCardLastDigits
                        )
                        tvCardHolderName.text = String.format(
                            tvCardNumber.context.getString(R.string.mask_card_holder_name),
                            strCardHolderName
                        )

                        btnCash.visibility = View.GONE
                        btnCredit.visibility = View.VISIBLE
                        btnDebit.visibility = View.GONE
                        btnApplePay.visibility = View.GONE
                        btnOtherPayment.visibility = View.GONE
                        btnManualCard.visibility = View.GONE

                        llExternalDetail.show()
                    } else if (paymentType == AppConstant.DEBIT) {
                        btnDebit.background = activity.resources.getDrawable(R.drawable.btn_bg)
                        btnDebit.setTextColor(activity.getColor(R.color.colorWhite))

                        btnCash.setTextColor(activity.getColor(R.color.colorPrimary))
                        btnCash.background = activity.getDrawable(R.drawable.edt_bg)

                        btnOtherPayment.setTextColor(activity.getColor(R.color.colorPrimary))
                        btnOtherPayment.background = activity.getDrawable(R.drawable.edt_bg)

                        btnOther.setTextColor(activity.getColor(R.color.colorPrimary))
                        btnOther.background = activity.getDrawable(R.drawable.edt_bg)
                        tvCardNumber.text = String.format(
                            tvCardNumber.context.getString(R.string.mask_card_number),
                            strCardLastDigits
                        )
                        tvCardHolderName.text = String.format(
                            tvCardNumber.context.getString(R.string.mask_card_holder_name),
                            strCardHolderName
                        )

                        btnCash.visibility = View.GONE
                        btnCredit.visibility = View.GONE
                        btnDebit.visibility = View.VISIBLE
                        btnApplePay.visibility = View.GONE
                        btnOtherPayment.visibility = View.GONE
                        btnManualCard.visibility = View.GONE

                        llExternalDetail.show()
                    } else if (paymentType == AppConstant.APPLE_PAY) {
                        btnApplePay.background = activity.resources.getDrawable(R.drawable.btn_bg)
                        btnApplePay.setTextColor(activity.getColor(R.color.colorWhite))

                        btnCash.setTextColor(activity.getColor(R.color.colorPrimary))
                        btnCash.background = activity.getDrawable(R.drawable.edt_bg)

                        btnOther.setTextColor(activity.getColor(R.color.colorPrimary))
                        tvCardNumber.text = String.format(
                                tvCardNumber.context.getString(R.string.mask_card_number),
                                strCardLastDigits
                        )
                        tvCardHolderName.text = String.format(
                                tvCardNumber.context.getString(R.string.mask_card_holder_name),
                                strCardHolderName
                        )
                        /*btnOther.background = activity.getDrawable(R.drawable.edt_bg)
                        tvCardNumber.text = String.format(
                            tvCardNumber.context.getString(R.string.mask_card_number),
                            strCardLastDigits
                        )
                        tvCardHolderName.text = String.format(
                            tvCardNumber.context.getString(R.string.mask_card_holder_name),
                            strCardHolderName
                        )*/

                        btnCash.visibility = View.GONE
                        btnCredit.visibility = View.GONE
                        btnDebit.visibility = View.GONE
                        btnApplePay.visibility = View.VISIBLE
                        btnOtherPayment.visibility = View.GONE
                        btnManualCard.visibility = View.GONE
                        llExternalDetail.show()
                    } else if (paymentType == AppConstant.MANUAL_CARD) {
                        btnManualCard.background = activity.resources.getDrawable(R.drawable.btn_bg)
                        btnManualCard.setTextColor(activity.getColor(R.color.colorWhite))

                        btnCash.setTextColor(activity.getColor(R.color.colorPrimary))
                        btnCash.background = activity.getDrawable(R.drawable.edt_bg)

                        btnOther.setTextColor(activity.getColor(R.color.colorPrimary))

                        tvCardNumber.text  = activity.getString(R.string.card_info)

                        btnCash.visibility = View.GONE
                        btnCredit.visibility = View.GONE
                        btnDebit.visibility = View.GONE
                        btnApplePay.visibility = View.GONE
                        btnOtherPayment.visibility = View.GONE
                        btnManualCard.visibility = View.VISIBLE
                        llExternalDetail.show()
                    } else {
                        btnCash.background = activity.resources.getDrawable(R.drawable.btn_bg)
                        btnCash.setTextColor(activity.getColor(R.color.colorWhite))

                        btnOther.setTextColor(activity.getColor(R.color.colorPrimary))
                        btnOther.background = activity.getDrawable(R.drawable.edt_bg)
                        btnCash.visibility = View.VISIBLE
                        btnCredit.visibility = View.GONE
                        btnDebit.visibility = View.GONE
                        btnApplePay.visibility = View.GONE
                        btnOtherPayment.visibility = View.GONE
                        llExternalDetail.hide()
                    }
                }

                manageViews()
                btnCash.setOnClickListener {
                    paymentType = AppConstant.CASH
                    manageViews()
                }
                btnOtherPayment.setOnClickListener {
                    paymentType = payment_type
                    manageViews()
                }

                btnCredit.setOnClickListener {
                    paymentType = AppConstant.CREDIT
                    manageViews()
                }
                btnDebit.setOnClickListener {
                    paymentType = AppConstant.DEBIT
                    manageViews()
                }
                btnApplePay.setOnClickListener {
                    paymentType = AppConstant.APPLE_PAY
                    manageViews()
                }
                btnManualCard.setOnClickListener {
                    paymentType = AppConstant.MANUAL_CARD
                    manageViews()
                }

                val TotalRefund = mTotalRefundAmount
                val refundamount =
                    "Refund " + currency_symbol + " " + String.format("%.2f", (TotalRefund))
                val postfix = " to customer"
                val merge = refundamount.plus(postfix)
                val spannableStringBuilder = SpannableStringBuilder(merge)

                val fcsGreen = ForegroundColorSpan(getColor(activity, R.color.colorGreen))

                spannableStringBuilder.setSpan(
                    fcsGreen,
                    6,
                    refundamount.length,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )

                val tvAmount = dialogcheckout.findViewById<TextView>(R.id.tvAmount)

                tvAmount.text = spannableStringBuilder

                ivCancel.setOnClickListener {
                    openPaymentCancellationDialog(mActivity) {
                        if (it) {
                            dialogcheckout.dismiss()
                        }
                    }
                }
                btnok.setOnClickListener {

                    callBack(payment_type)
                    dialogcheckout.dismiss()
                }
            }
        }

        fun openReceiptDialog(
            title: String,
            selectedCustomerEmail: String,
            mActivity: AppCompatActivity,
            callback: (isPrintSuccess: Boolean, email: String) -> Unit
        ) {
            val dialog = Dialog(mActivity)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.window?.setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
            )
            dialog.getWindow()!!.setCallback(
                UserInteractionAwareCallback(
                    dialog.getWindow()!!.getCallback(),
                    mActivity
                )
            )

            dialog.setContentView(R.layout.dialog_reciept)
            dialog.setCancelable(false)
            dialog.show()
            var isPrint = false
            val ivCancel = dialog.findViewById<ImageView>(R.id.ivCancel)
            val btnFinish = dialog.findViewById<Button>(R.id.btnFinish)
            val tvRemainingAmount = dialog.findViewById<TextView>(R.id.tvRemainingAmount)
            tvRemainingAmount.text = title

            val rgEmail = dialog.findViewById<RadioGroup>(R.id.rgEmail)
            val rbEmailYes = dialog.findViewById<RadioButton>(R.id.rbEmailYes)
            val rbEmailNo = dialog.findViewById<RadioButton>(R.id.rbEmailNo)
            val edtEmail = dialog.findViewById<EditText>(R.id.edtEmail)


            rgEmail.setOnCheckedChangeListener { group, checkedId ->
                when (checkedId) {
                    R.id.rbEmailYes -> {
                        edtEmail.isEnabled = true
                    }
                    R.id.rbEmailNo -> {
                        edtEmail.isEnabled = false
                    }

                }
            }
            ivCancel.setOnClickListener {
                DialogUtility.openPaymentCancellationDialog(mActivity) {
                    if (it) {
                        dialog.dismiss()
                    }
                }
            }

            edtEmail.setText(selectedCustomerEmail)


            val rgReceipt = dialog.findViewById<RadioGroup>(R.id.rgReceipt)

            rgReceipt.setOnCheckedChangeListener { group, checkedId ->
                when (checkedId) {
                    R.id.rbPrintYes -> {
                        isPrint = true
                    }
                    R.id.rbPrintNo -> {
                        isPrint = false
                    }
                }
            }

            edtEmail.isEnabled = false

            btnFinish.setOnClickListener {
                CM.onKeyboardClose(edtEmail, mActivity)
                var customerEmail = "";
                if (rbEmailYes.isChecked) {
                    if (edtEmail.text.toString().equals("")) {
                        CM.showToast(
                            mActivity,
                            mActivity.getString(R.string.msg_enter_email),
                            R.drawable.icon,
                            mActivity.custom_toast_container
                        )
                        return@setOnClickListener
                    } else if (!edtEmail.text.toString().isEmailValid()) {
                        CM.showToast(
                            mActivity,
                            mActivity.getString(R.string.msg_enter_valid_email),
                            R.drawable.icon,
                            mActivity.custom_toast_container
                        )
                        return@setOnClickListener
                    }
                    customerEmail = edtEmail.text.toString()
                }
                if (!CM.isInternetAvailable(mActivity)) {
                    CM.showMessageOK(
                        mActivity,
                        "",
                        mActivity.getString(R.string.msg_network_error),
                        null
                    )
                    return@setOnClickListener
                }
                dialog.dismiss()
                callback(isPrint, customerEmail)
            }
        }
    }
}