package com.ehsm.recore.utils

import android.app.Activity
import android.app.Dialog
import android.content.*
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.ehsm.recore.BuildConfig.*
import com.ehsm.recore.R
import com.ehsm.recore.activities.*
import com.ehsm.recore.custom.keyboard.KeyBoardLayout
import com.ehsm.recore.custom.keyboard.NumberDecimalEditText
import com.ehsm.recore.database.RecoreDB
import com.kcspl.divyangapp.viewmodel.ChangeMPinViewModel
import com.kcspl.divyangapp.viewmodel.CheckInOutDataViewModel
import com.kcspl.divyangapp.viewmodel.VerifyMPinViewModel
import kotlinx.android.synthetic.main.custom_toast.*
import kotlinx.android.synthetic.main.custom_toast.view.*
import kotlinx.android.synthetic.main.dialog_cashier_payin.*
import java.io.*
import java.net.URL
import java.net.URLConnection
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.Calendar.*

object CM {

    private var pin = ""
    private var count = 0
    private var minusCount = false
    private lateinit var dialog: Dialog
    private lateinit var dialogNosale: Dialog
    var emp_id = 0
    var CART_DISCOUNT = 0.00
    var emp_name = ""
    private lateinit var ivRoundOneCredential: ImageView
    private lateinit var ivRoundTwoCredential: ImageView
    private lateinit var ivRoundThreeCredential: ImageView
    private lateinit var ivRoundFourCredential: ImageView
    private lateinit var llPinRound: LinearLayout

    fun startActivity(activity: Activity, cls: Class<*>) {
        val intent = Intent(activity, cls)

        activity.startActivity(intent)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
            activity.overridePendingTransition(
                R.anim.push_in_from_left,
                R.anim.push_out_to_right
            )
        }
    }

    fun startActivityWitClearTop(activity: Activity, cls: Class<*>){
        val intent = Intent(activity, cls)
        intent.addFlags(
            Intent.FLAG_ACTIVITY_CLEAR_TOP or
                    Intent.FLAG_ACTIVITY_CLEAR_TASK or
                    Intent.FLAG_ACTIVITY_NEW_TASK
        )
        activity.startActivity(intent)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
            activity.overridePendingTransition(
                R.anim.push_in_from_left,
                R.anim.push_out_to_right
            )
        }
    }

    fun startActivityWithResult(activity: Activity, intent: Intent, reqCode: Int) {

        activity.startActivityForResult(intent, reqCode)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
            activity.overridePendingTransition(
                R.anim.push_in_from_left,
                R.anim.push_out_to_right
            )
        }
    }

    fun finishActivityWithResultOk(activity: Activity) {
        val data = Intent()

        activity.setResult(Activity.RESULT_OK, data)
        activity.finish()
        activity.overridePendingTransition(0, R.anim.push_out_to_left)
    }

    fun startActivityWithIntent(activity: Activity, intent: Intent) {

        activity.startActivity(intent)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
            activity.overridePendingTransition(
                R.anim.push_in_from_left,
                R.anim.push_out_to_right
            )
        }
    }

    fun hideSoftKeyboard(mActivity: Activity) {
        // Check if no view has focus:
        val view = mActivity.currentFocus
        if (view != null) {
            val inputManager =
                mActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun onKeyboardClose(view: View, activity: Activity) {
        val input = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        input.hideSoftInputFromWindow(view.applicationWindowToken, 0)
    }

    fun finishActivity(activity: Activity) {
        activity.finish()
        activity.overridePendingTransition(0, R.anim.push_out_to_left)
    }

    fun removeStatusBar(activity: AppCompatActivity) {
        activity.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }

    fun showPopup(mActivity: Activity,view: View, callBack: (sync_id :Int)-> Unit) {
        val popupMenu = PopupMenu(mActivity, view)
        val inflater = popupMenu.menuInflater
        inflater.inflate(R.menu.menu_new_sales, popupMenu.menu)
        popupMenu.menu.getItem(3).title = GetAppVersionName()
        popupMenu.show()
        popupMenu.setOnMenuItemClickListener({ item: MenuItem? ->

            when (item!!.itemId) {
                R.id.item_settings -> {
                    startActivity(mActivity, SettingsActivity::class.java)
                }
                R.id.item_logout -> {
                    logout(mActivity)
                }
                R.id.item_sync -> {
                    callBack(1)
                }
            }

            true
        })
    }

    private fun GetAppVersionName(): String {
        val VersionName = VERSION_NAME
        var Version = "Version - "+VersionName.substring(0, VersionName.indexOf("-"))
        return Version
    }

    fun logout(mActivity: Activity) {
        val prefs = mActivity.getSharedPreferences(
            mActivity.packageName, Activity.MODE_PRIVATE
        )

        val editor = prefs.edit()
        editor.clear()
        //
        editor.commit()

        //Clear All Events In DB
        val mDatabase = RecoreDB.getInstance(mActivity)!!
        mDatabase.daoCashDrawerManualReportList().deleteCashDrawerManualData()
        mDatabase.daoCashDrawerReportList().deleteCashDrawerData()
        mDatabase.daoCategoryList().deleteCategories()
        mDatabase.daoCityList().deleteCity()
        mDatabase.daoCustomerList().deleteCustomers()
        mDatabase.daoDiscount().deleteDiscountMaster()
        mDatabase.daoDiscount().deleteDiscountTransaction()
        mDatabase.daoDiscount().deleteDiscountApply()
        mDatabase.daoEmployeeList().deleteEmployee()
        mDatabase.daoPaymentList().deletePayments()
        mDatabase.daoForceCheckOutList().deleteForceCheckOut()
        mDatabase.daoIngredientList().deleteIngredient()
        mDatabase.daoIngredientList().deleteModifierMapper()
        mDatabase.daoItemList().deleteProducts()
        mDatabase.daoManualSync().deleteItem()
        mDatabase.daoModifierGroupList().deleteModifier()
        mDatabase.daoModifierMapping().deleteModifierMapping()
        mDatabase.daoOrerHistoryList().deleteOrderHistoryData()
        mDatabase.daoOrderTrans().deleteOrderMasrter()
        mDatabase.daoOrderTrans().deleteOrderTrans()
        mDatabase.daoPreModifierList().deletePreModifier()
        mDatabase.daoProductValidation().deleteAllPreModifier()
        mDatabase.daoStateList().deleteState()
        mDatabase.daoSubCategoryList().deleteSubCategories()
        startActivity(mActivity, LoginActivity::class.java)
        mActivity.finish()
    }

    fun showMessageOK(
        context: Activity?,
        title: String,
        message: String,
        okListener: DialogInterface.OnClickListener?
    ): AlertDialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogTheme)
            .setMessage(message).setCancelable(false)
            .setPositiveButton(context.resources.getString(R.string.common_ok), okListener)
        if (TextUtils.isEmpty(title)) {
            builder.setTitle(title)
        }
        builder.create()
        return builder.show()
    }

    fun showSyncErrorDialog( context: Context,
                             message: String,callBack: (dialog: Dialog) -> Unit
                             ) {
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            dialog.setContentView(R.layout.dialog_restrict)
            val tvLabel = dialog.findViewById<TextView>(R.id.tvLabel)
            tvLabel.setText(message)

            val btnok = dialog.findViewById<Button>(R.id.btnok)
            btnok.text = "Continue"
            dialog.setCancelable(false)
            dialog.show()

            btnok.setOnClickListener{
                callBack(dialog)
            }
    }

    /**
     * Return date in specified format.
     * @param milliSeconds Date in milliseconds
     * @param dateFormat Date format
     * @return String representing date in specified format
     */
    fun convertDateFormate(displayFormate: String, timestamp: Long): String {

        var dateString = ""
        try {
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = timestamp
            val formatter = SimpleDateFormat(displayFormate)
            return formatter.format(calendar.time)
        } catch (e: Exception) {
            e.printStackTrace()
            return dateString
        }

    }

    fun convertStringToDate(newPattern: String, dateString: String): Date {
        val sdf = SimpleDateFormat(newPattern, CV.LOCALE_USE_DATEFORMAT)
        return sdf.parse(dateString)
    }

    fun convertDateFormate(oldpattern: String, newPattern: String, dateString: String?): String {
        if (dateString == null || TextUtils.isEmpty(dateString)) {
            return ""
        }
        if (isValidDate(dateString, newPattern)) {
            return dateString
        } else {
            val sdf = SimpleDateFormat(oldpattern, CV.LOCALE_USE_DATEFORMAT)
            var testDate: Date? = null
            try {
                testDate = sdf.parse(dateString)
                val formatter = SimpleDateFormat(newPattern, CV.LOCALE_USE_DATEFORMAT)
                return formatter.format(testDate)
            } catch (e: Exception) {
                e.printStackTrace()
                return dateString
            }
        }
    }

    fun convertDatetospecifictimezone(context: Activity):String
    {
        val timezone=TimeZone.getTimeZone(CM.getSp(context,CV.STRTIMEZONE,"") as String)
        val holdorderFormat=SimpleDateFormat(CV.DISPLAY_DATE_FORMATTE_AMPM)
        holdorderFormat.timeZone=timezone
        return holdorderFormat.format(Date())
    }

    fun isValidDate(inDate: String, format: String): Boolean {
        val dateFormat = SimpleDateFormat(format)
        dateFormat.isLenient = false
        try {
            dateFormat.parse(inDate.trim { it <= ' ' })
        } catch (pe: ParseException) {
            return false
        }

        return true
    }

    fun setSp(activity: Context?, key: String, value: Any) {
        var prefs: SharedPreferences? = activity!!.getSharedPreferences(
            activity!!.packageName, Activity.MODE_PRIVATE
        )
        var editor: SharedPreferences.Editor? = prefs!!.edit()

        if (value is String) {
            editor!!.putString(key, value)
        } else if (value is Boolean) {
            editor!!.putBoolean(key, value)
        } else if (value is Int) {
            editor!!.putInt(key, value)
        } else if (value is Long) {
            editor!!.putLong(key, value)
        } else if (value is Float) {
            editor!!.putFloat(key, value)
        }
        editor!!.commit()

    }


    fun getSp(activity: Context?, key: String, defaultValue: Any): Any {
        val prefs = activity?.getSharedPreferences(
            activity.packageName, Activity.MODE_PRIVATE
        )
        return (if (defaultValue is String) {
            prefs!!.getString(key, defaultValue)
        } else if (defaultValue is Boolean) {
            prefs!!.getBoolean(key, defaultValue)
        } else if (defaultValue is Int) {
            prefs!!.getInt(key, defaultValue)
        } else if (defaultValue is Long) {
            prefs!!.getLong(key, defaultValue)
        } else {
            prefs!!.getFloat(key, defaultValue as Float)
        })!!

    }

    fun isInternetAvailable(context: Context?): Boolean {
        val connectivityManager =
            context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                return true
            }
        }
        return false
    }

    fun setViewHeightBasedOnItems(
        view: ViewGroup,
        adapter: Adapter?,
        from: String
    ): Boolean {
        return if (adapter != null) {
            val numberOfItems = adapter.count
            var totalItemsHeight = 0
            for (itemPos in 0 until numberOfItems) {
                val item = adapter.getView(itemPos, null, view)
                item.measure(0, 0)
                totalItemsHeight += item.measuredHeight
            }
            val totalDividersHeight: Int
            totalDividersHeight = if (from.equals("list", ignoreCase = true)) {
                (view as ListView).dividerHeight * (numberOfItems - 1)
            } else {
                view.height * (numberOfItems - 1)
            }
            val params = view.layoutParams
            params.height = totalItemsHeight + totalDividersHeight
            view.layoutParams = params
            view.requestLayout()
            true
        } else {
            false
        }
    }

    fun navigateToDashboard(activity: Activity) {
        startActivity(activity, ParentActivity::class.java)
        finishActivity(activity)
    }

    fun getDiffYears(first: Date, last: Date): Int {
        val a: Calendar = getCalendar(first)
        val b: Calendar = getCalendar(last)
        var diff = b[YEAR] - a[YEAR]
        if (a[MONTH] > b[MONTH] ||
            a[MONTH] === b[MONTH] && a[DATE] > b[DATE]
        ) {
            diff--
        }
        return diff
    }

    fun calculateAge(birthdate: Date): Int {
        var age = 0
        try {

            val now = getInstance()
            val dob = getInstance()
            dob.time = birthdate
            val year1 = now[YEAR]
            val year2 = dob[YEAR]
            age = year1 - year2
            val month1 = now[MONTH]
            val month2 = dob[MONTH]
            if (month2 > month1) {
                age--
            } else if (month1 == month2) {
                val day1 = now[DAY_OF_MONTH]
                val day2 = dob[DAY_OF_MONTH]
                if (day2 > day1) {
                    age--
                }
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return age
    }

    fun getCalendar(date: Date): Calendar {
        val cal = Calendar.getInstance(Locale.US)
        cal.time = date
        return cal
    }

    fun openNosaleDialog(mActivity: Activity,viewGroup : ViewGroup?) {
        dialogNosale = Dialog(mActivity)
        dialogNosale.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogNosale.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialogNosale.setContentView(R.layout.dialog_no_sale)

        dialogNosale.setCancelable(false)
        dialogNosale.show()

        val tvMakeChange = dialogNosale.findViewById<TextView>(R.id.tvMakeChange)
        val tvCountDrawer = dialogNosale.findViewById<TextView>(R.id.tvCountDrawer)
        val tvOther = dialogNosale.findViewById<TextView>(R.id.tvOther)
        val ivCancel = dialogNosale.findViewById<ImageView>(R.id.ivCancel)
        ivCancel.setOnClickListener {
            dialogNosale.dismiss()
        }
        tvMakeChange.setOnClickListener {
            openPassCodeDialog("no_sale", "Make Change", mActivity,viewGroup)
        }

        tvCountDrawer.setOnClickListener {
            openPassCodeDialog("no_sale", "Count Drawer", mActivity,viewGroup)
        }
        tvOther.setOnClickListener {
            openPassCodeDialog("no_sale", "Other", mActivity,viewGroup)
        }

    }

    fun openPassCodeDialog(from: String, reason: String, mActivity: Activity,viewGroup: ViewGroup?) {
        dialog = Dialog(mActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setContentView(R.layout.dialog_passcode)

        dialog.setCancelable(false)
        dialog.show()

        val btnOneCredential = dialog.findViewById<Button>(R.id.btnOneCredential)
        val btnTwoCredential = dialog.findViewById<Button>(R.id.btnTwoCredential)
        val btnThreeCredential = dialog.findViewById<Button>(R.id.btnThreeCredential)
        val btnFourCredential = dialog.findViewById<Button>(R.id.btnFourCredential)
        val btnFiveCredential = dialog.findViewById<Button>(R.id.btnFiveCredential)
        val btnSixCredential = dialog.findViewById<Button>(R.id.btnSixCredential)
        val btnSevenCredential = dialog.findViewById<Button>(R.id.btnSevenCredential)
        val btnEightCredential = dialog.findViewById<Button>(R.id.btnEightCredential)
        val btnNineCredential = dialog.findViewById<Button>(R.id.btnNineCredential)
        val btnZeroCredential = dialog.findViewById<Button>(R.id.btnZeroCredential)

        val btnBackSpaceCredential = dialog.findViewById<Button>(R.id.btnBackSpaceCredential)
        val btnResetMpin = dialog.findViewById<Button>(R.id.btnResetMpin)

        ivRoundOneCredential = dialog.findViewById(R.id.ivRoundOneCredential)
        ivRoundTwoCredential = dialog.findViewById(R.id.ivRoundTwoCredential)
        ivRoundThreeCredential = dialog.findViewById(R.id.ivRoundThreeCredential)
        ivRoundFourCredential = dialog.findViewById(R.id.ivRoundFourCredential)

        llPinRound = dialog.findViewById(R.id.llPinRound)

        val tvClose = dialog.findViewById<TextView>(R.id.tvClose)
        tvClose.setOnClickListener {
            dialog.dismiss()
        }
        btnResetMpin.setOnClickListener {
            //  openChangePasscodeDialog(mActivity)
            dialog.dismiss()
        }
        btnOneCredential.setOnClickListener({
            countCall(count)
            fillPasscode("1", from, reason, mActivity,viewGroup)
        })

        btnTwoCredential.setOnClickListener(View.OnClickListener {
            countCall(count)
            fillPasscode("2", from, reason, mActivity,viewGroup)
        })
        btnThreeCredential.setOnClickListener({
            countCall(count)
            fillPasscode("3", from, reason, mActivity,viewGroup)
        })
        btnFourCredential.setOnClickListener({
            countCall(count)
            fillPasscode("4", from, reason, mActivity,viewGroup)
        })
        btnFiveCredential.setOnClickListener({
            countCall(count)
            fillPasscode("5", from, reason, mActivity,viewGroup)
        })
        btnSixCredential.setOnClickListener({
            countCall(count)
            fillPasscode("6", from, reason, mActivity,viewGroup)
        })
        btnSevenCredential.setOnClickListener({
            countCall(count)
            fillPasscode("7", from, reason, mActivity,viewGroup)
        })
        btnEightCredential.setOnClickListener({
            countCall(count)
            fillPasscode("8", from, reason, mActivity,viewGroup)
        })
        btnNineCredential.setOnClickListener({
            countCall(count)
            fillPasscode("9", from, reason, mActivity,viewGroup)
        })
        btnZeroCredential.setOnClickListener({
            countCall(count)
            fillPasscode("0", from, reason, mActivity,viewGroup)
        })
        btnBackSpaceCredential.setOnClickListener({
            dropPasscode()
            if (count > 0) {
                count -= 1
            }
            minusCount = true
            countCall(count)
        })
    }


    private fun webCallChangeMpin(
        mpin: String,
        iMpin_old: String,
        dialogchangempin: Dialog,
        mActivity: AppCompatActivity,
        viewGroup: ViewGroup?
    ) {

        var viewModelChangeMPin =
            ViewModelProviders.of(mActivity).get(ChangeMPinViewModel::class.java)
        viewModelChangeMPin!!.changeMpin(emp_id, mpin.toInt(),iMpin_old.toInt())
            ?.observe(mActivity, androidx.lifecycle.Observer { loginResponseModel ->
                if (loginResponseModel.data == null) {
                    showToast(mActivity, loginResponseModel.message!!,R.drawable.icon,viewGroup)
                } else {
                    showToast(mActivity, loginResponseModel.data!!.message,R.drawable.icon,viewGroup)
                    dialogchangempin.dismiss()
                }

            })


    }

    fun showToast(context: Activity, message: String, imageSrc: Int,viewGroup: ViewGroup?) {
        val toast = Toast(context)
        toast.apply {

            val layout = context.layoutInflater.inflate(R.layout.custom_toast, viewGroup)

            layout.textView.text = message
            layout.imageView.setBackgroundResource(imageSrc)
            setGravity(Gravity.BOTTOM, 0, 0)

            duration = Toast.LENGTH_LONG
            view = layout
            show()
        }

        fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
            return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
        }

        fun Context.toast(
            context: Context = applicationContext,
            message: String,
            toastDuration: Int = Toast.LENGTH_SHORT
        ) {
            Toast.makeText(context, message, toastDuration).show()
        }

        fun Context.stringResToast(
            context: Context = applicationContext,
            message: Int,
            toastDuration: Int = Toast.LENGTH_SHORT
        ) {
            Toast.makeText(context, context.getString(message), toastDuration).show()
        }
    }


    fun openChangePasscodeDialog(mActivity: Activity,viewGroup: ViewGroup?) {

        emp_id = getSp(mActivity, CV.EMP_ID, 0) as Int
        emp_name = getSp(mActivity, CV.EMP_NAME, "") as String

        val dialogchangempin = Dialog(mActivity)
        dialogchangempin.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogchangempin.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogchangempin.window!!.callback=UserInteractionAwareCallback(
                dialogchangempin!!.window!!.getCallback(),
                mActivity
        )

        dialogchangempin.setContentView(R.layout.dialog_change_mpin)

        dialogchangempin.setCancelable(false)
        dialogchangempin.show()

        val tvUserNameLogin = dialogchangempin.findViewById<TextView>(R.id.tvUserNameLogin)
        tvUserNameLogin.setText("User: " + emp_name)
        val mKeyBoardLayout = dialogchangempin.findViewById<KeyBoardLayout>(R.id.keyboard_layout)

        val edtCurrentMPin = dialogchangempin.findViewById<NumberDecimalEditText>(R.id.edtCurrentMPin)
        val edtNewMPin = dialogchangempin.findViewById<NumberDecimalEditText>(R.id.edtNewMPin)
        val edtReEnterMpin = dialogchangempin.findViewById<NumberDecimalEditText>(R.id.edtReEnterMpin)

        edtCurrentMPin.setTypeface(Typeface.DEFAULT)
        edtNewMPin.setTypeface(Typeface.DEFAULT)
        edtReEnterMpin.setTypeface(Typeface.DEFAULT)

        mKeyBoardLayout.addKeyBoardCallback(edtCurrentMPin)
        mKeyBoardLayout.addKeyBoardCallback(edtNewMPin)
        mKeyBoardLayout.addKeyBoardCallback(edtReEnterMpin)
        edtCurrentMPin.requestFocus()
        edtCurrentMPin.setFocusable(true)
        dialogchangempin.getWindow()?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        mKeyBoardLayout.setDrawLine(true)
        mKeyBoardLayout.showKeyBoard()
        val btnSubmit = dialogchangempin.findViewById<Button>(R.id.btnSubmit)
        btnSubmit.setText("Confirm")
      //  val btnFinish = dialogchangempin.findViewById<Button>(R.id.btnFinish)
        btnSubmit.setOnClickListener {
            if (TextUtils.isEmpty(edtCurrentMPin.text.toString())) {
                showToast(mActivity,mActivity.getString(R.string.enter_current_mpin),R.drawable.icon,viewGroup)
            } else if (TextUtils.isEmpty(edtNewMPin.text.toString())) {
                showToast(mActivity,mActivity.getString(R.string.enter_new_mpin),R.drawable.icon,viewGroup)
            } else if (TextUtils.isEmpty(edtReEnterMpin.text.toString())) {
                showToast(mActivity,mActivity.getString(R.string.enter_confirm_mpin),R.drawable.icon,viewGroup)
            }else if (!edtCurrentMPin.text.toString().trim().length.equals(4) ||
                !edtNewMPin.text.toString().trim().length.equals(4) ||
                !edtReEnterMpin.text.toString().trim().length.equals(4) ) {
                showToast(mActivity,mActivity.getString(R.string.invalid_mpin),R.drawable.icon,viewGroup)
            } else if (!edtNewMPin.text.toString().equals(edtReEnterMpin.text.toString())) {
                showToast(mActivity,mActivity.getString(R.string.mpin_not_match),R.drawable.icon,viewGroup)
            } else {
                if (!isInternetAvailable(mActivity)) {
                    CM.showMessageOK(
                        mActivity,
                        "",
                        mActivity.getString(R.string.msg_network_error),
                        null
                    )

                } else {
                    webCallChangeMpin(
                        edtReEnterMpin.text.toString(),
                        edtCurrentMPin.text.toString(),
                        dialogchangempin,
                        mActivity as AppCompatActivity,viewGroup
                    )
                }
            }
        }

        val ivCancel = dialogchangempin.findViewById<ImageView>(R.id.ivCancel)
        ivCancel.setOnClickListener {
            dialogchangempin.dismiss()
        }
    }

    fun fillPasscode(code: String, from: String, reason: String, mActivity: Activity,viewGroup: ViewGroup?) {
        if (count <= 4 && count > 0) {
            pin = pin + code
        }
        if (count == 4) {
            try {
                if (!CM.isInternetAvailable(mActivity)) {
                    val itemListLiveData =
                        RecoreDB.getInstance(mActivity)!!.daoEmployeeList().getAllEmployee()
                    var matchFound = false
                    for (i in itemListLiveData.indices) {
                        Log.e("=== emp name ==", "===" + itemListLiveData.get(i).strFirstName)
                        if (pin.equals(itemListLiveData.get(i).iMpin)) {
                            matchFound = true
                            Log.e(
                                "=== emp name ###==",
                                "###===" + itemListLiveData.get(i).strFirstName
                            )

                            CM.setSp(
                                mActivity,
                                CV.EMP_NAME,
                                itemListLiveData.get(i).strFirstName + " " + itemListLiveData.get(i).strLastName
                            )
                            CM.setSp(mActivity, CV.EMP_IMAGE, "")
                            CM.setSp(mActivity, CV.EMP_ID, itemListLiveData.get(i).iCheckEmployeeId)
                            CM.setSp(
                                mActivity,
                                CV.EMP_ROLE,
                                itemListLiveData.get(i).strEmployeeRoleName
                            )

                            break

                        }
                    }
                    if (matchFound) {
                        dialog.dismiss()
                        count = 0
                        ivRoundOneCredential.setImageResource(R.drawable.roundblankbutton)
                        ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                        ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                        ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                        minusCount = false
                        pin = ""
                        CM.startActivity(mActivity, CheckInOutActivity::class.java)
                    } else {
                        showIncorrectPasscodeDialog(mActivity)
                        val shake =
                            AnimationUtils.loadAnimation(mActivity, R.anim.shake)
                        llPinRound.startAnimation(shake)
                        count = 0
                        ivRoundOneCredential.setImageResource(R.drawable.roundblankbutton)
                        ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                        ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                        ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                        minusCount = false
                        pin = ""

                    }
                } else {
                    webcallVerifyMPin(from, reason, mActivity as AppCompatActivity,viewGroup)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun webcallVerifyMPin(from: String, reason: String, mActivity: AppCompatActivity,viewGroup: ViewGroup?) {
        //showKcsDialog()

        var viewModelVerifyMPin =
            ViewModelProviders.of(mActivity).get(VerifyMPinViewModel::class.java)

        viewModelVerifyMPin!!.verifyMPin(pin)
            ?.observe(mActivity, androidx.lifecycle.Observer { loginResponseModel ->
                //  dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showIncorrectPasscodeDialog(mActivity)
                    val shake =
                        AnimationUtils.loadAnimation(mActivity, R.anim.shake)
                    llPinRound.startAnimation(shake)
                    count = 0
                    ivRoundOneCredential.setImageResource(R.drawable.roundblankbutton)
                    ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                    ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                    ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                    minusCount = false
                    pin = ""

                } else {
                    if (loginResponseModel.data!!.data.lastEmployeeStatus.equals(AppConstant.CLOCK_OUT)) {
                        count = 0
                        minusCount = false
                        pin = ""
                        showToast(
                            mActivity,
                            mActivity.getString(R.string.msg_user_already_clock_out),
                            R.drawable.icon,
                            viewGroup
                        )
                        dialog!!.dismiss()
                        return@Observer
                    }

                    showToast(
                        mActivity,
                        loginResponseModel.data!!.message,
                        R.drawable.icon,
                        viewGroup
                    )
                    dialog.dismiss()
                    emp_id = loginResponseModel.data!!.data.iCheckEmployeeId
                    emp_name = loginResponseModel.data!!.data.strFirstName + " " + loginResponseModel.data!!.data.strLastName
                    if (!from.equals("no_sale")) {
                        // no need to required save data while no sale option selected bcoz dashboard value changed.
                        CM.setSp(
                            mActivity,
                            CV.EMP_NAME,
                            loginResponseModel.data!!.data.strFirstName + " " + loginResponseModel.data!!.data.strLastName
                        )
                        CM.setSp(mActivity, CV.EMP_OPEN_DISCOUNT, loginResponseModel.data!!.data.tIsOpenDiscount)
                        setSp(
                            mActivity,
                            CV.EMP_IMAGE,
                            loginResponseModel.data!!.data.strProfilePhoto
                        )
                        setSp(mActivity, CV.EMP_ID, loginResponseModel.data!!.data.iCheckEmployeeId)
                        setSp(
                            mActivity,
                            CV.EMP_ROLE,
                            loginResponseModel.data!!.data.strEmployeeRoleName
                        )
                    }

                    if (loginResponseModel.data!!.data.strEmployeeRoleName.equals(AppConstant.STORE_ADMIN)) {
                        if (reason.equals(AppConstant.OTHER)) {
                            val dialog = Dialog(mActivity)
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                            dialog.getWindow()!!
                                .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                            dialog.setContentView(R.layout.dialog_other_reason)

                            dialog.setCancelable(true)
                            dialog.show()

                            val btnConfirmReason =
                                dialog.findViewById<Button>(R.id.btnConfirmReason)
                            val edtReason = dialog.findViewById<EditText>(R.id.edtReason)
                            btnConfirmReason.setOnClickListener {
                                if (TextUtils.isEmpty(edtReason.text.toString())) {
                                    showToast(
                                        mActivity,
                                        mActivity.getString(R.string.please_enter_reason),
                                        R.drawable.icon,
                                        viewGroup
                                    )
                                } else {
                                    webCallSendCheckInOutData(
                                        0.0,
                                        0.0,
                                        AppConstant.NO_SALE,
                                        edtReason.text.toString(),
                                        dialog,
                                        mActivity,
                                        viewGroup
                                    )
                                }
                            }
                        } else {
                            webCallSendCheckInOutData(
                                0.0,
                                0.0,
                                AppConstant.NO_SALE,
                                reason,
                                dialog,
                                mActivity,
                                viewGroup
                            )
                        }
                    } else {
                        showRestrictDialog(
                            mActivity,
                            mActivity.getString(R.string.you_do_not_have_the_rights)
                        )
                    }
                }

                count = 0
                minusCount = false
                pin = ""
            })
    }
    fun showRestrictDialog(mActivity: Context, displayMsg: String) {
        val dialog = Dialog(mActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setContentView(R.layout.dialog_restrict)
        val tvLabel = dialog.findViewById<TextView>(R.id.tvLabel)
        tvLabel.setText(displayMsg)
        dialog.setCancelable(false)
        dialog.show()

        val btnok = dialog.findViewById<Button>(R.id.btnok)
        btnok.setOnClickListener {
            dialog.dismiss()
        }
    }

    fun webCallSendCheckInOutData(
        cashInDrawer: Double,
        totalCashInDrawer: Double,
        type: String,
        remark: String,
        dialog: Dialog,
        mActivity: AppCompatActivity,
        viewGroup: ViewGroup?
    ) {

        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.WS_DATE_FORMATTE)
        //   showKcsDialog()
        var checkInOutDataViewModel =
            ViewModelProviders.of(mActivity).get(CheckInOutDataViewModel::class.java)
        checkInOutDataViewModel!!.checkInOutData(
            sdf.format(date),
            cashInDrawer,
            0.0,
            0.0,
            type,
            emp_id,
            remark,
            totalCashInDrawer,
            emp_id,
            0,
            0
        )
            ?.observe(mActivity, androidx.lifecycle.Observer { loginResponseModel ->
                //        dismissKcsDialog()
                if (type.equals("NoSale")) {
                    //Open Cashbox Drawer for NoSale from Header
                    var baseActivity = (mActivity as BaseActivity<*>)
                    baseActivity.openCashBox()
                    dialogNosale.dismiss()
                }
                if (loginResponseModel.data == null) {
                    showToast(mActivity, loginResponseModel.message!!,R.drawable.icon,viewGroup)
                } else {
                    showToast(mActivity, loginResponseModel.data!!.message,R.drawable.icon,viewGroup)
                    dialog.dismiss()
                }

            })
    }

    fun showIncorrectPasscodeDialog(mActivity: Activity) {
        val dialog = Dialog(mActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setContentView(R.layout.dialog_wrong_mpin)

        dialog.setCancelable(false)
        dialog.show()

        val btnReEnterMpin = dialog.findViewById<Button>(R.id.btnReEnterMpin)
        btnReEnterMpin.setOnClickListener {
            dialog.dismiss()
        }

    }

    fun dropPasscode() {
        if (count <= 4 && count > 0) {
            if (pin.length > 0) {
                pin = pin.substring(0, pin.length - 1)
            }
        }
    }

    fun countCall(c: Int) {
        when (c) {
            0 -> if (minusCount) {
                count = 0
                ivRoundOneCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                minusCount = false
            } else {
                count += 1
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

            }
            1 -> if (minusCount) {
                count = 1
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                minusCount = false
            } else {
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                count += 1
            }
            2 -> if (minusCount) {
                count = 2
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                minusCount = false
            } else {
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                count += 1
            }
            3 -> if (minusCount) {
                count = 3
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                minusCount = false
            } else {
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                count += 1
            }
            4 -> if (minusCount) {
                count = 4
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                minusCount = false
            } else {
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                count += 1
            }
            5 -> if (minusCount) {
                count = 5
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                minusCount = false
            } else {
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                count += 1
            }
        }
    }

    fun getEncryptedSharedPreferences(context: Context): SharedPreferences {
        val masterKeyAlias: String = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)
        return EncryptedSharedPreferences.create(
            AppConstant.EncryptedSharedPreferenceFileName,
            masterKeyAlias,
            context,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )
    }


    fun String.isEmailValid(): Boolean {
        return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this)
            .matches()
    }

    fun loadBitmaptoStore(imageurl: String, imagename: String, imagelocalpath: String,mActivity: Activity) {
        if (TextUtils.isEmpty(imageurl))
        {
            removeToInternalStorage(mActivity.applicationContext, imagename, imagelocalpath)
        }else {
            Glide.with(mActivity)
                    .asBitmap()
                    .load(imageurl)
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onLoadCleared(placeholder: Drawable?) {

                        }

                        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                            saveToInternalStorage(mActivity.applicationContext, resource, imagename, imagelocalpath)
                        }
                    })
        }
    }

    fun saveToInternalStorage(activity: Context, bitmapImage: Bitmap, imagename : String, imagepath : String): String? {
        val cw = ContextWrapper(activity)
        // path to /data/data/yourapp/app_data/imageDir
        val directory: File = cw.getDir("imageDir", Context.MODE_PRIVATE)
        // Create imageDir
        val mypath = File(directory, imagename)
        CM.setSp(activity, imagepath, mypath.absolutePath.toString())
//        Log.e("DEBUG", "mypath : "+mypath)
        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream(mypath)
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        } finally {
            try {
                if (fos != null) {
                    fos.close()
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return directory.getAbsolutePath()
    }

    fun removeToInternalStorage(activity: Context, imagename : String, imagepath : String)  {
        val cw = ContextWrapper(activity)
        // path to /data/data/yourapp/app_data/imageDir
        val directory: File = cw.getDir("imageDir", Context.MODE_PRIVATE)
        // Create imageDir
        val mypath = File(directory, imagename)
        if (mypath.exists())
        {
            mypath.delete()
        }
    }

    fun DownloadFromUrl(activity: Context, VideonURL: String?, fileName: String)
    {
        try {
            val url = URL(VideonURL)
            val cw = ContextWrapper(activity.applicationContext)
            val directory: File = cw.getDir("imageDir", Context.MODE_PRIVATE)
            // Create imageDir
            val file = File(directory, fileName)
            CM.setSp(activity, fileName, file.absolutePath.toString())

            val startTime = System.currentTimeMillis()
            Log.d("VideoManager", "downloaded url:"+file.absolutePath.toString())
            val ucon: URLConnection = url.openConnection()

            val fis: InputStream = ucon.getInputStream()
            val bis = BufferedInputStream(fis)

            val buffer = ByteArrayOutputStream()
            val data = ByteArray(50)
            var current = 0
            while (bis.read(data, 0, data.size).also { current = it } != -1) {
                buffer.write(data, 0, current)
            }

            val fos = FileOutputStream(file)
            fos.write(buffer.toByteArray())
            fos.close()
            Log.d("VideoManager", "download ready in"+ (System.currentTimeMillis() - startTime) / 1000+ " sec")
        } catch (e: IOException) {
            Log.d("VideoManager", "Error: $e")
        }
    }
}
