package com.ehsm.recore.utils

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.text.TextUtils
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.text.buildSpannedString
import androidx.core.text.color
import com.ehsm.recore.R
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.list_item_pos_status.view.*

fun TextInputLayout.markRequired() {
    hint = "$hint *"
}

fun TextInputLayout.markRequiredInRed() {
    hint = buildSpannedString {
        append(hint)
        color(Color.RED) { append(" *") } // Mind the space prefix.
    }
}

fun Context.toastShort(message: CharSequence) =
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

fun Context.toastLong(message: CharSequence) =
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()

fun Activity.showMessageOK(
    title: String,
    message: String,
    okListener: DialogInterface.OnClickListener?
): AlertDialog {
    val builder = AlertDialog.Builder(this, R.style.AlertDialogTheme)
        .setMessage(message).setCancelable(false)
        .setPositiveButton(this.resources.getString(R.string.common_ok), okListener)
    if (TextUtils.isEmpty(title)) {
        builder.setTitle(title)
    }
    builder.create()
    return builder.show()
}

/**
 * Show the view  (visibility = View.VISIBLE)
 */
fun View.show() : View {
    if (visibility != View.VISIBLE) {
        visibility = View.VISIBLE
    }
    return this
}


/**
 * Remove the view (visibility = View.GONE)
 */
fun View.hide() : View {
    if (visibility != View.GONE) {
        visibility = View.GONE
    }
    return this
}

fun TextView.formatAmount(currency_symbol:String,amount:Double) {
    this.text = "$currency_symbol ${String.format("%.2f", (amount))}"
}


inline fun <reified T> Gson.fromJson(json: String) = fromJson<T>(json, object: TypeToken<T>() {}.type)