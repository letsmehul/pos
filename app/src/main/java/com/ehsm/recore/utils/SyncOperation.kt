package com.ehsm.recore.utils

import android.content.Context
import android.content.ContextWrapper
import android.os.AsyncTask
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.ehsm.recore.R
import com.ehsm.recore.Recore
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.database.dao.*
import com.ehsm.recore.model.*
import com.ehsm.recore.repository.DiscountListRepository
import com.ehsm.recore.viewmodel.CategoryListViewModel
import com.ehsm.recore.viewmodel.DiscountListViewModel
import com.ehsm.recore.viewmodel.EmployeeListViewModel
import com.ehsm.recore.viewmodel.PaymentViewModel
import com.ehsm.recore.viewmodelfactory.viewModelFactory
import com.kcspl.divyangapp.viewmodel.ItemListViewModel
import com.kcspl.divyangapp.viewmodel.LoginViewModel
import com.kcspl.divyangapp.viewmodel.PaymentMethodViewModel
import com.kcspl.divyangapp.viewmodel.SubCategoryListViewModel
import kotlinx.android.synthetic.main.custom_toast.*
import java.io.*
import java.net.URL
import java.net.URLConnection
import java.util.*

object SyncOperation {
    private lateinit var mDatabase: RecoreDB
    private lateinit var mActivity: AppCompatActivity

    private var categoryListViewModel: CategoryListViewModel? = null
    private var subCategoryListViewModel: SubCategoryListViewModel? = null
    private var itemListViewModel: ItemListViewModel? = null
    private var employeeListViewModel: EmployeeListViewModel? = null
    private var paymentMethodViewModel: PaymentMethodViewModel? = null
    private var discountListViewModel: DiscountListViewModel? = null
    private var loginViewModel: LoginViewModel? = null


    var kcsDialog: KcsProgressDialog? = null
    var isFullDataSync: Boolean = true

    var catSyncDateTime: String = ""
    var subCatSyncDateTime: String = ""
    var prodSyncDateTime: String = ""

    fun syncData(
        activity: AppCompatActivity,
        isFullDataSync: Boolean,
        callBack: (isSyncFinished: Boolean, message: String?) -> Unit
    ) {
        this.mActivity = activity
        this.isFullDataSync = isFullDataSync

        CM.setSp(mActivity, CV.ISSYNCOPERATIONFINISHED, false)

        mDatabase = RecoreDB.getInstance(mActivity)!!
        categoryListViewModel =
            ViewModelProviders.of(mActivity).get(CategoryListViewModel::class.java)
        subCategoryListViewModel =
            ViewModelProviders.of(mActivity).get(SubCategoryListViewModel::class.java)
        itemListViewModel = ViewModelProviders.of(mActivity).get(ItemListViewModel::class.java)
        employeeListViewModel =
            ViewModelProviders.of(mActivity).get(EmployeeListViewModel::class.java)
        paymentMethodViewModel =
            ViewModelProviders.of(mActivity).get(PaymentMethodViewModel::class.java)
        loginViewModel = ViewModelProviders.of(mActivity).get(LoginViewModel::class.java)
        discountListViewModel = ViewModelProviders.of(
            mActivity,
            viewModelFactory {
                DiscountListViewModel(DiscountListRepository(mDatabase))
            }
        ).get(DiscountListViewModel::class.java)
        if (isFullDataSync) {
            catSyncDateTime = ""
            subCatSyncDateTime = ""
            prodSyncDateTime = ""
        } else {
            catSyncDateTime = CM.getSp(mActivity, CV.CAT_SYNC_DATE_TIME, "") as String
            subCatSyncDateTime = CM.getSp(mActivity, CV.SUB_CAT_SYNC_DATE_TIME, "") as String
            prodSyncDateTime = CM.getSp(mActivity, CV.PROD_SYNC_DATE_TIME, "") as String
        }
        webCallProductCategory(callBack)
    }

    /**
     * Call Product API and Database Operation
     */
    private fun webCallProductCategory(callBack: (isSyncFinished: Boolean, message: String?) -> Unit) {
        showKcsDialog()
        categoryListViewModel!!.getCategories(mDatabase, catSyncDateTime)
            ?.observe(mActivity, androidx.lifecycle.Observer { loginResponseModel ->
                if (loginResponseModel.data == null) {
                    dismissKcsDialog()
                    callBack(false, loginResponseModel.message)
                } else {
                    callCategoryDatabaseOperation(loginResponseModel, callBack)
                }
            })
    }

    /**
     * Call Sub Category API and Database Operation
     */
    private fun webCallSubProductCategory(
        iProductCategoryID: String,
        callBack: (isSyncFinished: Boolean, message: String?) -> Unit
    ) {
        subCategoryListViewModel!!.getsubCategories(
            mDatabase, iProductCategoryID,
            subCatSyncDateTime
        )
            ?.observe(mActivity, androidx.lifecycle.Observer { loginResponseModel ->
                if (loginResponseModel.data == null) {
                    dismissKcsDialog()
                    callBack(false, loginResponseModel.message)
                } else {
                    callSubCategoryDatabaseOperation(loginResponseModel, callBack)
                }
            })
    }

    /**
     * Call Product API and Database Operation
     */
    private fun webCallItemList(
        suCategoryId: String,
        callBack: (isSyncFinished: Boolean, message: String?) -> Unit
    ) {
        itemListViewModel!!.getItems(mDatabase, suCategoryId, prodSyncDateTime)
            ?.observe(mActivity, androidx.lifecycle.Observer { loginResponseModel ->
                if (loginResponseModel.data == null) {
                    dismissKcsDialog()
                    callBack(false, loginResponseModel.message)
                } else {
                    callProductItemDatabaseOperation(loginResponseModel, callBack)
                }
            })
    }

    /**
     * Call Emplyee API and Database Operation
     */
    private fun webCallgetEmployeeList(callBack: (isSyncFinished: Boolean, message: String?) -> Unit) {
        employeeListViewModel!!.getEmployeeList(mDatabase)
            ?.observe(mActivity, androidx.lifecycle.Observer { loginResponseModel ->
                if (loginResponseModel.data == null) {
                    callBack(false, loginResponseModel.message)
                } else {
                    callEmployeeDatabaseOperation(loginResponseModel, callBack)
                }
            })
    }

    private fun webCallLoginData(callBack: (isSyncFinished: Boolean, message: String?) -> Unit) {
        loginViewModel!!.logInData()
            ?.observe(mActivity, androidx.lifecycle.Observer { loginResponseModel ->
                if (loginResponseModel.data == null) {
                    callBack(false, loginResponseModel.message)
                } else {
                    loginViewModel!!.loginDataOperation(
                        mActivity,
                        loginResponseModel.data!!
                    ) { isOperationFinished ->
                        webCallgetPaymentList(callBack)
                    }
                }
            })
    }

    /**
     * Call Payment Method API and Database Operation
     */
    private fun webCallgetPaymentList(callBack: (isSyncFinished: Boolean, message: String?) -> Unit) {
        paymentMethodViewModel!!.getPayments()
            ?.observe(mActivity, androidx.lifecycle.Observer { paymentResponseModel ->
                if (paymentResponseModel.data == null) {
                    val paymentListDAO = mDatabase.daoPaymentList()
                    paymentListDAO.deletePayments()
                    discountListCall(callBack)
                } else {
                    callPaymentDatabaseOperation(paymentResponseModel, callBack)
                }
            })
    }


    /**
     * Call Discount API and Database Operation
     */
    fun discountListCall(callBack: (isSyncFinished: Boolean, message: String?) -> Unit) {
        discountListViewModel?.sychWithServer(
            CM.getSp(
                mActivity,
                CV.EMP_ID,
                0
            ) as Int
        ) { isDiscountSuccess, message ->
            dismissKcsDialog()
            if (isDiscountSuccess) {
                callBack(true, mActivity.getString(R.string.sync_success))
            } else {
                callBack(false, message)
            }
        }
    }

    /**
     * this function used for category inserted or update and further process
     */
    fun callCategoryDatabaseOperation(
        loginResponseModel: DataWrapper<CategoryListResponseModel>,
        callBack: (isSyncFinished: Boolean, message: String?) -> Unit
    ) {
        CategoryListViewModel.CategoryDataBaseOperationTask(
            loginResponseModel.data!!.data,
            mDatabase,
            isFullDataSync
        ) { isOperationFinished ->
            CM.setSp(
                mActivity,
                CV.CAT_SYNC_DATE_TIME,
                loginResponseModel.data!!.sync_date_time
            )
            if (CM.isInternetAvailable(mActivity)) {
                webCallSubProductCategory("", callBack)
            } else {
                dismissKcsDialog()
                callBack(false, mActivity.getString(R.string.msg_network_error))
            }
        }.execute()
    }

    /**
     * this function used for subcategory inserted or update and further process
     */
    fun callSubCategoryDatabaseOperation(
        loginResponseModel: DataWrapper<SubCategoryListResponseModel>,
        callBack: (isSyncFinished: Boolean, message: String?) -> Unit
    ) {
        SubCategoryListViewModel.SubCategoryDataBaseOperationTask(
            loginResponseModel.data!!.data,
            mDatabase,
            isFullDataSync
        ) { isOperationFinished ->
            CM.setSp(
                mActivity,
                CV.SUB_CAT_SYNC_DATE_TIME,
                loginResponseModel.data!!.sync_date_time
            )
            if (CM.isInternetAvailable(mActivity)) {
                webCallItemList("", callBack)
            } else {
                dismissKcsDialog()
                callBack(false, mActivity.getString(R.string.msg_network_error))
            }
        }.execute()
    }

    /**
     * this function used for product inserted or update and further process
     */
    fun callProductItemDatabaseOperation(
        loginResponseModel: DataWrapper<ItemListResponseModel>,
        callBack: (isSyncFinished: Boolean, message: String?) -> Unit
    ) {
        kcsDialog!!.findViewById<ProgressBar>(R.id.dialogProgressBar).visibility =
            View.VISIBLE
        kcsDialog!!.findViewById<TextView>(R.id.tvProgressDisplay).visibility =
            View.VISIBLE

        ItemListViewModel.ProductItemDataBaseOperationTask(
            loginResponseModel.data!!.data,
            mDatabase, isFullDataSync
        ) { isOperationFinished, progressUpdateValue ->
            if (isOperationFinished) {
                kcsDialog!!.findViewById<ProgressBar>(R.id.dialogProgressBar).visibility = View.GONE
                kcsDialog!!.findViewById<TextView>(R.id.tvProgressDisplay).visibility = View.GONE
                CM.setSp(
                    mActivity,
                    CV.PROD_SYNC_DATE_TIME,
                    loginResponseModel.data!!.sync_date_time
                )
                if (CM.isInternetAvailable(mActivity)) {
                    webCallgetEmployeeList(callBack)
                } else {
                    dismissKcsDialog()
                    callBack(false, mActivity.getString(R.string.msg_network_error))
                }
            } else {
                kcsDialog!!.findViewById<ProgressBar>(R.id.dialogProgressBar)
                    .setProgress(progressUpdateValue, true)
                kcsDialog!!.findViewById<TextView>(R.id.tvProgressDisplay)
                    .setText("Loading... $progressUpdateValue%")
            }
        }.execute()
    }

    /**
     * this function used for employee inserted or update and further process
     */
    fun callEmployeeDatabaseOperation(
        loginResponseModel: DataWrapper<EmployeeListResponseModel>,
        callBack: (isSyncFinished: Boolean, message: String?) -> Unit
    ) {
        EmployeeListViewModel.EmployeeDatabaseOperationTask(
            loginResponseModel.data!!.data,
            mDatabase
        ) { isOperationFinished ->
            if (CM.isInternetAvailable(mActivity)) {
                webCallLoginData(callBack)
            } else {
                dismissKcsDialog()
                callBack(false, mActivity.getString(R.string.msg_network_error))
            }
        }.execute()
    }

    /**
     * this function used for employee inserted or update and further process
     */
    fun callPaymentDatabaseOperation(
        paymentMethod: DataWrapper<PaymentMethodModel>,
        callBack: (isSyncFinished: Boolean, message: String?) -> Unit
    ) {
        PaymentMethodViewModel.PaymentDatabaseOperationTask(
            paymentMethod.data!!.data,
            mDatabase,
            isFullDataSync
        ) { isOperationFinished ->
            if (CM.isInternetAvailable(mActivity)) {
                discountListCall(callBack)
            } else {
                dismissKcsDialog()
                callBack(false, mActivity.getString(R.string.msg_network_error))
            }
        }.execute()
    }

    fun showKcsDialog() {
        if (mActivity.isFinishing) {
            return
        }
        mActivity.runOnUiThread(Runnable {
            if (mActivity.isFinishing) {
                return@Runnable
            }

            //if dialog not dismiss then first dialog=null.so not to show anytimewhen activtiy finish...
            if (kcsDialog != null) {
                kcsDialog = null
            }
            if (kcsDialog == null)
                kcsDialog = KcsProgressDialog(mActivity, false)

            kcsDialog?.let {
                if (!it.isShowing)
                    it.show()
            }
        })
    }


    fun dismissKcsDialog() {
        if (mActivity.isFinishing) {
            return
        }
        if (kcsDialog != null && kcsDialog!!.isShowing) {
            kcsDialog!!.dismiss()
            kcsDialog = null
        }
    }


}