package com.ehsm.recore.utils

import android.app.Dialog
import android.content.Context
import android.view.Window
import com.ehsm.recore.R


class PaymentProgressDialog(context: Context, isCancelable: Boolean) : Dialog(context) {

    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.payment_progress_dialog)
        setCancelable(isCancelable)
        window!!.setBackgroundDrawableResource(android.R.color.transparent)
    }
}

