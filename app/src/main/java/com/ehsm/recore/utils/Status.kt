package com.ehsm.recore.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}