package com.ehsm.recore.utils

import android.app.Dialog
import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.widget.AppCompatSpinner
import androidx.appcompat.widget.AppCompatTextView
import com.ehsm.recore.R
import com.ehsm.recore.Recore
import java.util.*
import kotlin.collections.ArrayList

class KitchenPrinterSelectionDialog(context: Context, mPrinterdata: ArrayList<HashMap<String, String>>) : Dialog(context){
    var selectedItem :String?=null
    var mKitchenPrinterIP :String?=null
    private var printernamelist : ArrayList<String>? = ArrayList()
    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_show_terminals)
        setCancelable(false)

        window!!.setBackgroundDrawableResource(android.R.color.transparent)

//        vl myMap = HashMap<String, Int>()
        for (item in mPrinterdata) {
            printernamelist?.add(item.values.toString())
        }
//        printernamelist = ArrayList<String>(mPrinterdata.get(0).values)

        val spinnerTerminals = findViewById<AppCompatSpinner>(R.id.spinnerTerminal)

        val dialogTitle = findViewById<AppCompatTextView>(R.id.tvUserNameLogin)
        val dialogsubTitle = findViewById<AppCompatTextView>(R.id.tvLabelSelectTerminal)
        val tvNote = findViewById<AppCompatTextView>(R.id.tvNote)

        dialogTitle.setText("Kitchen Printer")
        dialogsubTitle.setText("Select Kitchen Printer")
        tvNote.setVisibility(View.VISIBLE);
        tvNote.setText("NOTE: If your printer is not appear in the list then there is a possibility that it is turned off , please turn it ON and try again.")

        val adapterKtchenprinters =
            ArrayAdapter(context, R.layout.item_spinner_terminal, printernamelist!!)
        adapterKtchenprinters.setDropDownViewResource(android.R.layout.simple_list_item_single_choice)
        spinnerTerminals.adapter = adapterKtchenprinters

        spinnerTerminals?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long) {

                mKitchenPrinterIP = mPrinterdata.get(position).get("Target")
                selectedItem = printernamelist!![position]
                Log.e("KITCHEN_PRINTER_DEBUG", "mKitchenPrinterIP -- "+mKitchenPrinterIP)
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
    }
}