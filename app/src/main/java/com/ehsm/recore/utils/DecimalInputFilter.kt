package com.ehsm.recore.utils
import android.text.InputFilter
import android.text.Spanned

public class DecimalInputFilter : InputFilter {

    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned,
        dstart: Int,
        dend: Int
    ): CharSequence? {
        val builder = StringBuilder(dest)
        builder.replace(dstart, dend, source.subSequence(start, end).toString())

        var regexString =  Regex("(([0-9]{1})([0-9]{0,4})?)?(\\.[0-9]{0,2})?")
        if(!builder.toString().matches(regexString)){
            if(source.isEmpty()){
                return dest.subSequence(dstart, dend)
            }
            return ""
        }
        return null
    }
}