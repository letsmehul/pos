package com.ehsm.recore

import androidx.room.TypeConverter
import com.ehsm.recore.model.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.Serializable

class DataTypeConverter:Serializable{

    @TypeConverter // note this annotation
    fun fromOptionValuesListTax(ticketValues: List<TaxModel>?): String? {
        if (ticketValues == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<TaxModel>>() {

        }.type
        return gson.toJson(ticketValues, type)
    }

    @TypeConverter // note this annotation
    fun toOptionValuesListTax(ticketValuesString: String?): List<TaxModel>? {
        if (ticketValuesString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<TaxModel>>() {

        }.type
        return gson.fromJson<List<TaxModel>>(ticketValuesString, type)
    }

    @TypeConverter // note this annotation
    fun fromOptionValuesListLastActivity(ticketValues: List<LastActivityModel>?): String? {
        if (ticketValues == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<LastActivityModel>>() {

        }.type
        return gson.toJson(ticketValues, type)
    }

    @TypeConverter // note this annotation
    fun toOptionValuesListLastActivity(ticketValuesString: String?): List<LastActivityModel>? {
        if (ticketValuesString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<LastActivityModel>>() {

        }.type
        return gson.fromJson<List<LastActivityModel>>(ticketValuesString, type)
    }

    @TypeConverter // note this annotation
    fun fromOptionValuesListOrderProducts(ticketValues: List<OrderProductsModel>?): String? {
        if (ticketValues == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<OrderProductsModel>>() {

        }.type
        return gson.toJson(ticketValues, type)
    }

    @TypeConverter // note this annotation
    fun toOptionValuesListOrderProducts(ticketValuesString: String?): List<OrderProductsModel>? {
        if (ticketValuesString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<OrderProductsModel>>() {

        }.type
        return gson.fromJson<List<OrderProductsModel>>(ticketValuesString, type)
    }

    @TypeConverter // note this annotation
    fun fromOptionValuesListOrderDiscounts(ticketValues: List<OrderDiscountsModel>?): String? {
        if (ticketValues == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<OrderDiscountsModel>>() {

        }.type
        return gson.toJson(ticketValues, type)
    }

    @TypeConverter // note this annotation
    fun toOptionValuesListOrderDiscounts(ticketValuesString: String?): List<OrderDiscountsModel>? {
        if (ticketValuesString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<OrderDiscountsModel>>() {

        }.type
        return gson.fromJson<List<OrderDiscountsModel>>(ticketValuesString, type)
    }

    @TypeConverter // note this annotation
    fun fromOptionValuesListOrderPayments(ticketValues: List<OrderPaymentsModel>?): String? {
        if (ticketValues == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<OrderPaymentsModel>>() {

        }.type
        return gson.toJson(ticketValues, type)
    }

    @TypeConverter // note this annotation
    fun toOptionValuesListOrderPayments(ticketValuesString: String?): List<OrderPaymentsModel>? {
        if (ticketValuesString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<OrderPaymentsModel>>() {

        }.type
        return gson.fromJson<List<OrderPaymentsModel>>(ticketValuesString, type)
    }

    @TypeConverter // note this annotation
    fun fromOptionValuesListOrderRefund(ticketValues: List<OrderRefundModel>?): String? {
        if (ticketValues == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<OrderRefundModel>>() {

        }.type
        return gson.toJson(ticketValues, type)
    }

    @TypeConverter // note this annotation
    fun toOptionValuesListOrderRefund(ticketValuesString: String?): List<OrderRefundModel>? {
        if (ticketValuesString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<OrderRefundModel>>() {

        }.type
        return gson.fromJson<List<OrderRefundModel>>(ticketValuesString, type)
    }

    @TypeConverter // note this annotation
    fun toOptionValuesListEmployeeDataModel(ticketValuesString: String?): ArrayList <EmployeeDataModel>? {
        if (ticketValuesString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList <EmployeeDataModel>>() {

        }.type
        return gson.fromJson<ArrayList <EmployeeDataModel>>(ticketValuesString, type)
    }

    @TypeConverter // note this annotation
    fun fromOptionValuesListModifierMasterModel(ticketValues: List<ModifierMasterModel>?): String? {
        if (ticketValues == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<ModifierMasterModel>>() {

        }.type
        return gson.toJson(ticketValues, type)
    }

    @TypeConverter // note this annotation
    fun toOptionValuesListModifierMasterModel(ticketValuesString: String?): List<ModifierMasterModel>? {
        if (ticketValuesString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<ModifierMasterModel>>() {

        }.type
        return gson.fromJson<List<ModifierMasterModel>>(ticketValuesString, type)
    }

    @TypeConverter // note this annotation
    fun fromOptionValuesListPreModifierMasterModel(ticketValues: List<PreModifierMasterModel>?): String? {
        if (ticketValues == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<PreModifierMasterModel>>() {

        }.type
        return gson.toJson(ticketValues, type)
    }

    @TypeConverter // note this annotation
    fun toOptionValuesListPreModifierMasterModel(ticketValuesString: String?): List<PreModifierMasterModel>? {
        if (ticketValuesString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<PreModifierMasterModel>>() {

        }.type
        return gson.fromJson<List<PreModifierMasterModel>>(ticketValuesString, type)
    }


    @TypeConverter // note this annotation
    fun fromOptionValuesListIngredientMasterModel(ticketValues: List<IngredientMasterModel>?): String? {
        if (ticketValues == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<IngredientMasterModel>>() {

        }.type
        return gson.toJson(ticketValues, type)
    }

    @TypeConverter // note this annotation
    fun toOptionValuesListIngredientMasterModel(ticketValuesString: String?): List<IngredientMasterModel>? {
        if (ticketValuesString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<IngredientMasterModel>>() {

        }.type
        return gson.fromJson<List<IngredientMasterModel>>(ticketValuesString, type)
    }
}