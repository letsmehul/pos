package com.ehsm.recore.fragments

import android.app.Activity
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.InputType
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.ehsm.recore.R
import com.ehsm.recore.custom.keyboard.DateKeyboard
import com.ehsm.recore.interfaces.ItemCallBack
import com.ehsm.recore.model.OrderTransModel
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.GenericTextWatcher
import com.ehsm.recore.utils.NormalTextWatcher
import com.ehsm.recore.utils.UserInteractionAwareCallback
import com.ehsm.recore.utils.logger.LogUtil
import kotlinx.android.synthetic.main.custom_toast.*
import kotlin.math.roundToInt

class QuantityChangeDialogFragment(
    val orderTransModel: OrderTransModel,
    val callback: (quantity: Int) -> Unit
) :
    DialogFragment() {
    var callBackListener: ItemCallBack<OrderTransModel>? = null
    lateinit var ivCancel: ImageView
    lateinit var btnSubmit: Button

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        getDialog()?.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        dialog?.window?.setCallback(
            UserInteractionAwareCallback(
                dialog?.window!!.getCallback(),
                activity
            )
        )
        val view = inflater.inflate(R.layout.dialog_change_quantity, container, false)
        ivCancel = view.findViewById(R.id.ivCancel)
        val ivItemname=view.findViewById<TextView>(R.id.tvProductName)
        ivItemname.text=orderTransModel.itemName
        btnSubmit = view.findViewById<Button>(R.id.btnSubmit)
        btnSubmit.setText(getString(R.string.submit))

        val edtQty=view.findViewById<EditText>(R.id.edtQty);
        edtQty.setText(orderTransModel.itemQty.toString())
        edtQty!!.showSoftInputOnFocus = false

        val keyboard: DateKeyboard = view.findViewById(R.id.keyboard) as DateKeyboard
        edtQty.setRawInputType(InputType.TYPE_CLASS_TEXT)
        edtQty.setTextIsSelectable(false)

        edtQty.setSelection(edtQty.text.length)

        val ic = edtQty.onCreateInputConnection(EditorInfo())
        keyboard.setInputConnection(ic)

        edtQty.addTextChangedListener(NormalTextWatcher(edtQty))

        val ivClearAmonut = view.findViewById<ImageView>(R.id.ivClearAmount)
        ivClearAmonut.setOnClickListener {
            edtQty.setText("")
            edtQty.requestFocus()
        }

        ivCancel.setOnClickListener {
            dismiss()
        }

        btnSubmit.setOnClickListener {
            if (!TextUtils.isEmpty(edtQty.text.toString())) {
                try {
                    Log.d("dataquantity",edtQty.text.toString())
                    if (edtQty.text.toString().toInt() <= 0) {
                        CM.showToast(
                            context as Activity,
                            getString(R.string.enter_valid_quantity),
                            R.drawable.icon,
                            custom_toast_container
                        )
                    }else if ((edtQty.text.toString().toInt()) > 999)
                    {
                        CM.showToast(
                            context as Activity,
                            getString(R.string.enter_valid_maximum_enter_999),
                            R.drawable.icon,
                            custom_toast_container
                        )
                    }
                    else {
                        callback(edtQty.text.toString().toInt())
                        dismiss()
                    }

                } catch (e: NumberFormatException) {
                    LogUtil.d("exception",e.message)
                    CM.showToast(
                        context as Activity,
                        getString(R.string.msg_enter_proper_amount),
                        R.drawable.icon,
                        custom_toast_container
                    )
                }


            } else {
                CM.showToast(
                    context as Activity,
                    getString(R.string.enter_valid_quantity),
                    R.drawable.icon,
                    custom_toast_container
                )
            }

        }

        return view
    }


}