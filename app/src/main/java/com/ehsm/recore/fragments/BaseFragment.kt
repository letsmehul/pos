package com.ehsm.recore.fragments

import android.app.Dialog
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.view.View
import android.widget.*
import androidx.appcompat.widget.AppCompatButton
import com.bumptech.glide.Glide
import com.ehsm.recore.R
import com.ehsm.recore.utils.*
import kotlinx.android.synthetic.main.custom_toast.*
import kotlinx.android.synthetic.main.custom_toast.view.*
import kotlinx.android.synthetic.main.list_item_pos_status.view.*


open abstract class BaseFragment : androidx.fragment.app.Fragment(), View.OnClickListener {

    private var kcsDialog: Dialog? = null

    abstract fun initView(view: View, savedInstances: Bundle?)
    private var handlerClose  = Handler()
    private var runnable : Runnable? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView(view, savedInstanceState)
        //initActionBar()
    }

    override fun onClick(v: View?) {

    }


    fun checkInternetOption(): Boolean {
        if (!CM.isInternetAvailable(activity)) {
            val msg = resources.getString(R.string.msg_network_error)
            CM.showMessageOK(
                    activity!!, "", msg,
                    null
            )
            return false
        }
        return true
    }


    fun showKcsDialog() {
        activity?.runOnUiThread(Runnable {
            if (activity!!.isFinishing) {
                return@Runnable
            }

            //if dialog not dismiss then first dialog=null.so not to show anytimewhen activtiy finish...
            if (kcsDialog != null) {
                kcsDialog = null
            }
            if (kcsDialog == null)
                kcsDialog = KcsProgressDialog(activity!!, false)
            if (!kcsDialog!!.isShowing)
                kcsDialog!!.show()
        })
    }

    fun dismissKcsDialog() {
        if(runnable != null){
            handlerClose.removeCallbacks(runnable!!)
            runnable = null
        }

        if (activity == null) {
            return
        }
        if (activity!!.isFinishing) {
            return
        }
        if (kcsDialog != null && kcsDialog!!.isShowing) {
            kcsDialog!!.dismiss()
            kcsDialog = null
        }
    }

    fun showPaymentProgressDialog() {
        activity?.runOnUiThread(Runnable {
            if (activity!!.isFinishing) {
                return@Runnable
            }

            //if dialog not dismiss then first dialog=null.so not to show anytimewhen activtiy finish...
            if (kcsDialog != null) {
                kcsDialog = null
            }
            if (kcsDialog == null)
                kcsDialog = PaymentProgressDialog(activity!!, false)

            if (!kcsDialog!!.isShowing)
                kcsDialog!!.show()

            kcsDialog?.let {
                val imageView = it.findViewById<ImageView>(R.id.ivProgress)
                Glide.with(this).asGif().load(R.drawable.loading).into(imageView);
            }


        })
    }

    fun updatePaymentDialogTitle(msg: String){
        kcsDialog?.let {
            val tvTitle = it.findViewById<TextView>(R.id.tvTitle)
            tvTitle.text = msg
        }
    }

    fun updateDialogMsg(message: String, status: Int?, error: String = "", onDisMissCallBack: (isDismiss: Boolean) -> Unit){



        kcsDialog?.let {
            it.window!!.setCallback(
                    UserInteractionAwareCallback(
                            it.window!!.getCallback(),
                            activity!!
                    )
            )
            val llCardSwipe = it.findViewById<LinearLayout>(R.id.llCardSwipe)
            val llProgress = it.findViewById<LinearLayout>(R.id.llProgress)
            val llSuccess = it.findViewById<LinearLayout>(R.id.llSuccess)
            val llFailed = it.findViewById<LinearLayout>(R.id.llFailed)
            val btnDismiss = it.findViewById<AppCompatButton>(R.id.btnDismiss)
            val tvTitletext = it.findViewById<TextView>(R.id.tvTextTitle)
            val tvFailedOrCancel =it.findViewById<TextView>(R.id.tvFailedOrCancel)
            llCardSwipe.hide()
            llProgress.hide()
            btnDismiss.hide()
            llSuccess.hide()
            llFailed.hide()
            val msgText = it.findViewById<TextView>(R.id.pbText)
            if(msgText!=null){
                msgText.text = message
            }

            btnDismiss.setOnClickListener {
                dismissKcsDialog()
            }
            msgText.hide()

            status?.let {
                when(status)
                {
                    AppConstant.PaymentProcess.LOADING.value -> {
                        tvTitletext.text = getString(R.string.Please_wait)
                        llProgress.show()

                    }
                    AppConstant.PaymentProcess.SWIPE.value -> {
                        llCardSwipe.show()
                        tvTitletext.text = getString(R.string.title_swipe_card)
                        msgText.show()
                    }
                    AppConstant.PaymentProcess.AUTHENTICATION.value -> {
                        tvTitletext.text = getString(R.string.title_auth_card)
                        llProgress.show()
                        msgText.show()
                    }
                    AppConstant.PaymentProcess.SUCCESS.value -> {
                        tvTitletext.text = message
                        tvTitletext.show()
                        msgText.hide()
                        btnDismiss.show()
                        llSuccess.show()
                        btnDismiss.setOnClickListener {
                            dismissKcsDialog()
                            onDisMissCallBack?.let {
                                it(true)
                            }

                        }
                        runnable = Runnable {
                            btnDismiss.performClick()
                        }
                        handlerClose.postDelayed(runnable!!, 5000);

                    }

                    AppConstant.PaymentProcess.FAILED.value -> {
                        tvTitletext.text = message
                        tvTitletext.show()
                        msgText.hide()
                        btnDismiss.show()
                        llFailed.show()
                        tvFailedOrCancel.setText(getString(R.string.failed).toUpperCase())
                    }

                    AppConstant.PaymentProcess.CANCEL.value -> {
                        tvTitletext.text = getString(R.string.title_transaction)
                        tvTitletext.show()
                        msgText.show()
                        btnDismiss.show()
                        llFailed.show()
                        btnDismiss.setOnClickListener {
                            dismissKcsDialog()
                            onDisMissCallBack?.let {
                                it(true)
                            }

                        }
                        tvFailedOrCancel.setText(getString(R.string.cancel).toUpperCase())
                    }
                    AppConstant.PaymentProcess.SUCCESS_REFUND.value -> {
                        tvTitletext.text = message
                        tvTitletext.show()
                        msgText.hide()
                        btnDismiss.show()
                        llSuccess.show()

                    }
                    AppConstant.PaymentProcess.FAILED_REFUND.value -> {
                        tvTitletext.text = message
                        tvTitletext.show()
                        if (!TextUtils.isEmpty(error)) {
                            msgText.text = error
                            msgText.show()
                        }

                        btnDismiss.show()
                        llFailed.show()
                        tvFailedOrCancel.setText(getString(R.string.failed).toUpperCase())
                    }
                    AppConstant.PaymentProcess.INVALID_CHOICE.value -> {
                        tvTitletext.text = getString(R.string.title_wrong_choice)
                        tvTitletext.show()
                        msgText.show()
                        btnDismiss.show()
                        llFailed.hide()
                        btnDismiss.setOnClickListener {
                            onDisMissCallBack?.let {
                                it(true)
                            }

                        }
                    }
                    else -> {
                        llProgress.show()
                    }
                }
            }

        }
    }

    fun dismissPaymentProgressDialog() {
        if (activity == null) {
            return
        }
        if (activity!!.isFinishing) {
            return
        }
        if (kcsDialog != null && kcsDialog!!.isShowing) {
            kcsDialog!!.dismiss()
            kcsDialog = null
        }
    }

}