package com.ehsm.recore.fragments

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import com.ehsm.recore.R
import com.ehsm.recore.activities.ParentActivity
import com.ehsm.recore.adapter.PaymentListAdapter
import com.ehsm.recore.model.PaymentModel
import com.ehsm.recore.utils.*
import com.ehsm.recore.utils.CM.showRestrictDialog
import com.ehsm.recore.utils.logger.LogUtil
import com.ehsm.recore.viewmodel.PaymentViewModel
import kotlinx.android.synthetic.main.custom_toast.*
import kotlinx.android.synthetic.main.dialog_payment_process.*
import kotlinx.android.synthetic.main.dialog_payment_process.ivCancel
import kotlinx.android.synthetic.main.fragment_new_sales.*
import okhttp3.internal.format
import java.util.*
import kotlin.collections.ArrayList

class PaymentProcessDialogFragment(
    val mActivity: AppCompatActivity,
    val actualTotalPrice: Double,
    val totalQty: String,
    var paymentViewModel: PaymentViewModel,
    var emp_role: String
) :
    DialogFragment(){
    /*private var paymentList: ArrayList<PaymentModel> = ArrayList()
    var totalPrice: Double = 0.0
    var balanceDue: Double = 0.0
    var payment_type = ""
    var strAmount: ArrayList<Int> = ArrayList()
    var currency_symbol = ""

    init {
        totalPrice = actualTotalPrice
        balanceDue = actualTotalPrice
        payment_type = CM.getSp(mActivity, CV.PAYMENT_TYPE, AppConstant.CASH) as String
        currency_symbol = CM.getSp(mActivity, CV.CURRENCY_CODE, "") as String
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        getDialog()?.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        val view = inflater.inflate(R.layout.dialog_payment_process, container, false)
        tvCartTotalValue.formatAmount(actualTotalPrice)
        tvTotalQtyValue.text = totalQty

        edtAmount.setText("${String.format("%.2f", (totalPrice))}")
        calculateNearByAmountSlot(totalPrice)
        manageViews()
        btnCash.setOnClickListener(this)
        btnCredit.setOnClickListener(this)
        btnDebit.setOnClickListener(this)
        btnApplePay.setOnClickListener(this)
        btnCash1.setOnClickListener(this)
        btnCash2.setOnClickListener(this)
        btnCash3.setOnClickListener(this)
        btnCancel.setOnClickListener(this)
        btnCalculateChange.setOnClickListener(this)
        ivClearAmonut.setOnClickListener(this)

        edtAmount.addTextChangedListener(GenericTextWatcher(edtAmount))

        paymentListSetUp()

        edtAmount.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                if (s.isNotEmpty()) {
                    //tvAmountEdit.setText(s.toString())
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })

        return view
    }

    private fun calculateNearByAmountSlot(totalPrice: Double) {
        var firstValue = round(totalPrice.toInt())

        strAmount.clear()
        firstValue += 5
        strAmount.add(firstValue)
        strAmount.add(firstValue + 5)
        strAmount.add(firstValue + 10)

        btnCash1.text = currency_symbol + " " + format("%.2f", strAmount[0].toDouble())
        btnCash2.text = currency_symbol + " " + format("%.2f", strAmount[1].toDouble())
        btnCash3.text = currency_symbol + " " + format("%.2f", strAmount[2].toDouble())


    }

    private fun paymentListSetUp() {
        val arrayAdapter = ArrayAdapter(
            mActivity,
            android.R.layout.simple_list_item_1,
            resources.getStringArray(R.array.split_type)
        )
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnSplit.adapter = arrayAdapter

        paymentList.add(PaymentModel("1", "Cash", "$ 1.08"))
        paymentList.add(PaymentModel("2", "Master Card", "$ 100.00"))
        paymentList.add(PaymentModel("3", "Visa Card", "$ 50.00"))


        rvPayment.adapter = PaymentListAdapter(mActivity, paymentList)
    }

    override fun onClick(v: View?) {
        when (v) {
            btnCash -> {
                payment_type = AppConstant.CASH
                manageViews()
            }
            btnCredit -> {
                payment_type = AppConstant.CREDIT
                manageViews()

            }
            btnDebit -> {
                payment_type = AppConstant.DEBIT
                manageViews()
            }
            btnApplePay -> {
                payment_type = AppConstant.APPLE_PAY
                manageViews()
            }
            btnCalculateChange -> {
                calculateAmount()
            }
            btnCash1 -> {
                //tvAmountEdit.text = btnCash1.text.toString().split(" ")[1]
                edtAmount.setText(btnCash1.text.toString().split(" ")[1])
            }
            btnCash2 -> {
                //tvAmountEdit.text = btnCash1.text.toString().split(" ")[1]
                edtAmount.setText(btnCash2.text.toString().split(" ")[1])
            }
            btnCash3 -> {
                //tvAmountEdit.text = btnCash1.text.toString().split(" ")[1]
                edtAmount.setText(btnCash3.text.toString().split(" ")[1])
            }
            ivClearAmonut -> {
                //tvAmountEdit.text = ""
                edtAmount.setText("")
                edtAmount.requestFocus()
                edtAmount.isFocusable = true
            }
            ivCancel -> {
                dismiss()
            }
        }
    }

    *//**
     * After click on calculate submit thn calculate amout and redirect to case or credit or debit
     *//*
    private fun calculateAmount() {
        try {
            val amount = edtAmount.text.toString()
            if (amount == "0.") {
                CM.showToast(
                    mActivity,
                    getString(R.string.enter_valid_amount),
                    R.drawable.icon,
                    custom_toast_container
                )
                return
            } else if (amount.toDouble() <= 0) {
                CM.showToast(
                    mActivity,
                    getString(R.string.enter_valid_amount),
                    R.drawable.icon,
                    custom_toast_container
                )
                return
            }
            balanceDue =
                actualTotalPrice - amount.toDouble()
            if (CM.getSp(context!!, CV.IS_ADMIN_APPROVAL_REQUIRED_FOR_TRANSACTION, 1) == 1) {

                if (emp_role.equals(AppConstant.STAFF)) {
                    showRestrictDialog(
                        activity!!,
                        activity!!.getString(R.string.you_do_not_have_the_rights)
                    )
                } else {
                    if (payment_type == AppConstant.CASH) {
                        calculateAmountForCase()
                    } else {
                        calculateAmountForCreditOrDebit()
                    }
                }

            } else {
                (parentFragment as NewSalesFragment).openPassCodeDialog(
                    AppConstant.NEW_SALE,
                    0.0,
                    ""
                ) {
                    if (payment_type == AppConstant.CASH) {
                        calculateAmountForCase()
                    } else {
                        calculateAmountForCreditOrDebit()
                    }
                }
            }


        } catch (e: NumberFormatException) {
            e.printStackTrace()
            CM.showToast(
                mActivity,
                getString(R.string.msg_enter_proper_amount),
                R.drawable.icon,
                custom_toast_container
            )
        }
    }

    private fun calculateAmountForCreditOrDebit() {
        val amount = edtAmount.text.toString()
        paymentViewModel.payableAmount = String.format("%.0f", (amount.toDouble() * 100))
    }

    private fun calculateAmountForCase() {
        var activityTemp = (activity as ParentActivity)
        activityTemp.openCashBox()
        (parentFragment as NewSalesFragment).orderGenerate(remaining_amount, amount)
        showReceipt()
    }

    private fun showReceipt() {

    }

    private fun connectPaymentGateway(cardType: String) {

        paymentViewModel.getConnect(true).observe(this, androidx.lifecycle.Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        Log.d("Data", "getConnect ${resource.data}")
//                        readConfirmation()
                        readInput(cardType)
                        displayStoreName()
                    }
                    Status.ERROR -> {
                        Log.d("Data", "getConnect ${resource.message}")
                        resource.message?.let { it1 -> handleError(it1) }
                    }
                    Status.LOADING -> {
                        Log.d("Data", "getConnect ${resource.status}")

                        showPaymentProgressDialog()
                        paymentViewModel.getPaymentStatusObserver.value =
                            AppConstant.PaymentProcess.LOADING.value
                        updateProgressDialogTitle(getTotalCartMsg())

                    }
                }
            }
        })
    }
    private fun displayStoreName() {
        paymentViewModel.display(String.format(getString(R.string.welcome_msg), StoreName))
    }

    private fun readInput(cardType: String) {
        if (cardType.equals(AppConstant.CREDIT, true)) {
            payment_mode = AppConstant.CREDIT
            paymentViewModel.selectedPaymentMode = AppConstant.CREDIT
            paymentViewModel.cardRequestModel.includePin = false
            if ((CM.getSp(
                    context!!,
                    CV.SIGNATURE_AMOUNT,
                    0.0f
                ) as Float) < (paymentViewModel.payableAmount.toFloat() / 100)
            ) {
                paymentViewModel.cardRequestModel.includeSignature = true
            } else {
                paymentViewModel.cardRequestModel.includeSignature = false
            }
            placeOrderViewModel?.tIsApplePay = false
        } else if (cardType.equals(AppConstant.APPLE_PAY, true)) {
            payment_mode = AppConstant.CREDIT
            paymentViewModel.selectedPaymentMode = AppConstant.CREDIT
            paymentViewModel.cardRequestModel.includePin = false
            paymentViewModel.cardRequestModel.includeSignature = false
            placeOrderViewModel?.tIsApplePay = true
        } else {
            payment_mode = AppConstant.DEBIT
            paymentViewModel.selectedPaymentMode = AppConstant.DEBIT
            paymentViewModel.cardRequestModel.includePin = true
            paymentViewModel.cardRequestModel.includeSignature = false
            placeOrderViewModel?.tIsApplePay = false
        }
        authCard()
    }

    private fun authCard() {
        paymentViewModel.cardRequestModel.amount = paymentViewModel.payableAmount

        paymentViewModel.authCard().observe(this, androidx.lifecycle.Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        LogUtil.d("DEBIT DEBUG", "authCard ${resource.data}")
                        // call card read data
                        paymentViewModel.cardResponseModel = resource.data

                        paymentViewModel.cardResponseModel?.let { cardResponse ->
                            if (cardResponse.respcode == "000") {
                                mActivity.dismissPaymentProgressDialog()
                                val paidAmt = paymentViewModel.payableAmount.toDouble() / 100
                                orderGenerate(0.0, paidAmt.toString())

                            } else {
                                handleError(cardResponse.resptext)
                            }
                        }

                    }
                    Status.ERROR -> {
                        Log.d("Data", "authCard ${resource.message}")
                        resource.message?.let { it1 -> handleError(it1) }
                    }
                    Status.LOADING -> {
                        Log.d("Data", "authCard ${resource.status}")
                        paymentViewModel.getPaymentStatusObserver.value =
                            AppConstant.PaymentProcess.AUTHENTICATION.value
                    }
                }
            }
        })
    }

    fun manageViews() {
        edtAmount.setRawInputType(InputType.TYPE_CLASS_TEXT)
        edtAmount.setTextIsSelectable(true)
        val ic = edtAmount.onCreateInputConnection(EditorInfo())
        keyboard.setInputConnection(ic)
        if (payment_type.equals(AppConstant.CASH, true)) {
            btnCash.background = mActivity.resources.getDrawable(R.drawable.btn_bg)
            btnCash.setTextColor(resources.getColor(R.color.colorWhite))

            btnCredit.setTextColor(resources.getColor(R.color.colorPrimary))
            btnCredit.background = resources.getDrawable(R.drawable.edt_bg)

            btnDebit.setTextColor(resources.getColor(R.color.colorPrimary))
            btnDebit.background = resources.getDrawable(R.drawable.edt_bg)

            btnApplePay.setTextColor(resources.getColor(R.color.colorPrimary))
            btnApplePay.background = resources.getDrawable(R.drawable.edt_bg)

            if ((tvCartTotalValue.text.toString()).toDouble() <= 0) {
                val isNotOnlyOpenDiscontedProduct = CV.orderTransList.any { it.iDiscountId != -1 }
                if (!isNotOnlyOpenDiscontedProduct) {
                    btnCredit.visibility = View.GONE
                    btnDebit.visibility = View.GONE
                    btnApplePay.visibility = View.GONE
                }

            }
            llCash.visibility = View.VISIBLE

        } else if (payment_type.equals(AppConstant.CREDIT, true)) {
            btnCredit.background = mActivity.resources.getDrawable(R.drawable.btn_bg)
            btnCredit.setTextColor(resources.getColor(R.color.colorWhite))

            btnCash.setTextColor(resources.getColor(R.color.colorPrimary))
            btnCash.background = resources.getDrawable(R.drawable.edt_bg)

            btnDebit.setTextColor(resources.getColor(R.color.colorPrimary))
            btnDebit.background = resources.getDrawable(R.drawable.edt_bg)

            btnApplePay.setTextColor(resources.getColor(R.color.colorPrimary))
            btnApplePay.background = resources.getDrawable(R.drawable.edt_bg)

            llCash.visibility = View.GONE

        } else if (payment_type.equals(AppConstant.DEBIT, true)) {
            btnDebit.background = mActivity.resources.getDrawable(R.drawable.btn_bg)
            btnDebit.setTextColor(resources.getColor(R.color.colorWhite))

            btnCash.setTextColor(resources.getColor(R.color.colorPrimary))
            btnCash.background = resources.getDrawable(R.drawable.edt_bg)

            btnCredit.setTextColor(resources.getColor(R.color.colorPrimary))
            btnCredit.background = resources.getDrawable(R.drawable.edt_bg)

            btnApplePay.setTextColor(resources.getColor(R.color.colorPrimary))
            btnApplePay.background = resources.getDrawable(R.drawable.edt_bg)

            llCash.visibility = View.GONE

        } else if (payment_type.equals(AppConstant.APPLE_PAY, true)) {
            btnApplePay.background = mActivity.resources.getDrawable(R.drawable.btn_bg)
            btnApplePay.setTextColor(resources.getColor(R.color.colorWhite))

            btnCash.setTextColor(resources.getColor(R.color.colorPrimary))
            btnCash.background = resources.getDrawable(R.drawable.edt_bg)

            btnCredit.setTextColor(resources.getColor(R.color.colorPrimary))
            btnCredit.background = resources.getDrawable(R.drawable.edt_bg)

            btnDebit.setTextColor(resources.getColor(R.color.colorPrimary))
            btnDebit.background = resources.getDrawable(R.drawable.edt_bg)

            llCash.visibility = View.GONE

        } else {
            btnCash.background = mActivity.resources.getDrawable(R.drawable.btn_bg)
            btnCash.setTextColor(resources.getColor(R.color.colorWhite))

            btnCredit.setTextColor(resources.getColor(R.color.colorPrimary))
            btnCredit.background = resources.getDrawable(R.drawable.edt_bg)

            btnDebit.setTextColor(resources.getColor(R.color.colorPrimary))
            btnDebit.background = resources.getDrawable(R.drawable.edt_bg)

            btnApplePay.setTextColor(resources.getColor(R.color.colorPrimary))
            btnApplePay.background = resources.getDrawable(R.drawable.edt_bg)

            if ((tvCartTotalValue.text.toString()).toDouble() <= 0) {
                val isNotOnlyOpenDiscontedProduct = CV.orderTransList.any { it.iDiscountId != -1 }
                if (!isNotOnlyOpenDiscontedProduct) {
                    btnCredit.visibility = View.GONE
                    btnDebit.visibility = View.GONE
                    btnApplePay.visibility = View.GONE
                }

            }
            llCash.visibility = View.VISIBLE
        }
    }

    private fun round(n: Int): Int { // Smaller multiple
        return (n + 4) / 5 * 5;
    }

    fun TextView.formatAmount(amount: Double) {
        this.text = "${String.format("%.2f", (amount))}"
    }*/


}
