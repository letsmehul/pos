package com.ehsm.recore.fragments

import android.app.Activity
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.ehsm.recore.R
import com.ehsm.recore.custom.keyboard.BirthDateEditText
import com.ehsm.recore.custom.keyboard.DateKeyboard
import com.ehsm.recore.custom.keyboard.KeyBoardLayout
import com.ehsm.recore.model.ItemModel
import com.ehsm.recore.utils.CM.showToast
import com.ehsm.recore.utils.UserInteractionAwareCallback
import kotlinx.android.synthetic.main.custom_toast.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class BirthDateDialog(var item : ItemModel, val callback: (calendar: Calendar) -> Unit) : DialogFragment() {

    lateinit var ivCancel: ImageView
    lateinit var btnSubmit: Button
    lateinit var ivClearAmonut: ImageView
    private val cal = Calendar.getInstance()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        getDialog()?.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        dialog?.window?.setCallback(
                UserInteractionAwareCallback(
                        dialog?.window!!.getCallback(),
                        activity
                )
        )
        val view = inflater.inflate(R.layout.dialog_birth_date, container, false)

        ivCancel = view.findViewById(R.id.ivCancel)

        val mKeyBoardLayout = view.findViewById<KeyBoardLayout>(R.id.keyboard_layout)
        btnSubmit = view.findViewById<Button>(R.id.btnSubmit)
        btnSubmit.setText(getString(R.string.confirm))
        ivClearAmonut = view.findViewById(R.id.ivClearAmonut)

        val tvProductName = view.findViewById<TextView>(R.id.tvProductName)
        Log.e("DEBUG", "iAgeLimit -- "+item.iAgeLimit)
        Log.e("DEBUG", "getDate -- "+getDate(item.iAgeLimit))

        tvProductName.setText("Product: \n"+ item.strProductName + "\n is required age verification.")
        val edtCashInDrawer = view.findViewById<BirthDateEditText>(R.id.edtCashInDrawer)

        edtCashInDrawer.setText(getDate(item.iAgeLimit))
        edtCashInDrawer.listen()

        val keyboard: DateKeyboard = view.findViewById(R.id.keyboard) as DateKeyboard
        edtCashInDrawer.setRawInputType(InputType.TYPE_CLASS_TEXT)
        edtCashInDrawer.setTextIsSelectable(true)

        val ic = edtCashInDrawer.onCreateInputConnection(EditorInfo())
        keyboard.setInputConnection(ic)

        ivClearAmonut.setOnClickListener {
            edtCashInDrawer.setText("")
            edtCashInDrawer.requestFocus()
        }
        ivCancel.setOnClickListener {
            dismiss()
        }

        btnSubmit.setOnClickListener {

            if (edtCashInDrawer.text.toString().length == 10) {
                val df: DateFormat = SimpleDateFormat("MM-dd-yyyy")
                val cal = Calendar.getInstance()
                cal.time = df.parse(edtCashInDrawer.text.toString())
                callback(cal)
                dismiss()
            } else {
                showToast(context as Activity, getString(R.string.enter_dob), R.drawable.icon, custom_toast_container)
            }

        }



        return view
    }

    fun getDate(year: Int): String{
        val cal = Calendar.getInstance()
        cal.add(Calendar.YEAR, - year)
        cal.add(Calendar.DATE, -1)
        val date = cal.time
        println(SimpleDateFormat("MM-dd-yyyy").format(date))
        return SimpleDateFormat("MM-dd-yyyy").format(date)
    }

}