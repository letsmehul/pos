package com.ehsm.recore.fragments

import android.R.interpolator.linear
import android.app.Activity
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.InputFilter.LengthFilter
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.*
import android.widget.LinearLayout
import androidx.fragment.app.DialogFragment
import com.ehsm.recore.R
import com.ehsm.recore.custom.keyboard.DateKeyboard
import com.ehsm.recore.custom.keyboard.InputFilterMinMax
import com.ehsm.recore.model.OrderTransModel
import com.ehsm.recore.utils.*
import kotlinx.android.synthetic.main.custom_toast.*


class OpenDiscountDialogFragment(
    var mActivity: Activity, var orderTransModel: OrderTransModel,
    var callBack: (discountType: String, amount: String) -> Unit
) :
    DialogFragment() {
    lateinit var ivCancel: ImageView
    lateinit var btnAmount: TextView
    lateinit var btnPercentage: TextView
    lateinit var rlAmount: RelativeLayout
    lateinit var rlPercentage: RelativeLayout
    lateinit var edtOpenDiscountAmount: EditText
    lateinit var edtOpenDiscountPercentage: EditText
    lateinit var btnApplied: Button
    lateinit var tvTitle: TextView
    lateinit var ivClearAmount: ImageView
    lateinit var ivClearPercentage: ImageView
    lateinit var keyboardAmount: DateKeyboard
    lateinit var keyboardPercentage: DateKeyboard

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        dialog?.window?.setCallback(
        UserInteractionAwareCallback(
                dialog?.window!!.getCallback(),
                mActivity
        )
        )
        isCancelable = false
        val view = inflater.inflate(R.layout.dialog_opendiscount, container, false)
        var discountType = AppConstant.AMOUNT
        ivCancel = view.findViewById(R.id.ivCancel)
        btnAmount = view.findViewById(R.id.btnAmount)
        tvTitle = view.findViewById(R.id.tvTitle)
        btnPercentage = view.findViewById(R.id.btnPercentage)
        btnApplied = view.findViewById(R.id.btnApplied)
        edtOpenDiscountAmount = view.findViewById(R.id.edtOpenDiscountAmount)
        rlAmount = view.findViewById(R.id.rlAmount)
        rlPercentage = view.findViewById(R.id.rlPercentage)
        ivClearAmount = view.findViewById(R.id.ivClearAmount)
        ivClearPercentage = view.findViewById(R.id.ivClearPercentage)
        edtOpenDiscountPercentage = view.findViewById(R.id.edtOpenDiscountPercentage)
        keyboardAmount = view.findViewById(R.id.keyboardAmount) as DateKeyboard
        keyboardPercentage = view.findViewById(R.id.keyboardPercentage) as DateKeyboard

        val currency_symbol = CM.getSp(mActivity, CV.CURRENCY_CODE, "") as String

        val ic = edtOpenDiscountAmount.onCreateInputConnection(EditorInfo())
        keyboardAmount.setInputConnection(ic)

        val icAmount = edtOpenDiscountPercentage.onCreateInputConnection(EditorInfo())
        keyboardPercentage.setInputConnection(icAmount)

        tvTitle.text = orderTransModel.itemName + " - " + currency_symbol + String.format("%.2f", orderTransModel.itemRate)

        if (orderTransModel.iDiscountId == -1 && orderTransModel.discountRemark.isNotEmpty()) {
            if (orderTransModel.discountRemark.contains("%")) {
                discountType = AppConstant.PERCENTAGE
                val discount = orderTransModel.discountRemark.replace("%", "")
                edtOpenDiscountPercentage.setText(discount)
            } else {
                discountType = AppConstant.AMOUNT
                edtOpenDiscountAmount.setText(orderTransModel.discountAmount.toString())
            }
        }
        ivCancel.setOnClickListener {
            dismiss()
            //btnApplied.performClick()
        }
        btnAmount.setOnClickListener {
            ivClearAmount.performClick()
            ivClearPercentage.performClick()
            discountType = AppConstant.AMOUNT
            manageViews(discountType, view)
        }

        btnPercentage.setOnClickListener {
            ivClearAmount.performClick()
            ivClearPercentage.performClick()
            discountType = AppConstant.PERCENTAGE
            manageViews(discountType, view)
        }

        btnApplied.setOnClickListener {
            if (discountType.equals(AppConstant.AMOUNT, true)) {
                if (edtOpenDiscountAmount.text.toString().isNotEmpty()) {
                    if (orderTransModel.itemAmount <= edtOpenDiscountAmount.text.toString()
                            .toDouble()
                    ) {
                        with(CM) {
                            showToast(
                                mActivity,
                                getString(R.string.open_discount_validaion),
                                R.drawable.icon,
                                custom_toast_container
                            )
                        }
                    } else {
                        dismiss()
                        callBack(discountType, edtOpenDiscountAmount.text.toString())
                    }
                } else {
                    CM.showToast(
                        mActivity,
                        "Enter amount",
                        R.drawable.icon,
                        custom_toast_container
                    )
                }
            } else {
                if (edtOpenDiscountPercentage.text.toString().isNotEmpty()) {
                    dismiss()
                    callBack(discountType, edtOpenDiscountPercentage.text.toString())
                } else {
                    CM.showToast(
                        mActivity,
                        "Enter Percentage",
                        R.drawable.icon,
                        custom_toast_container
                    )
                }
            }
        }
        manageViews(discountType, view)
        return view
    }

    fun manageViews(discountType: String, view: View) {
        if (discountType.equals(AppConstant.AMOUNT, true)) {
            btnAmount.background = mActivity.resources.getDrawable(R.drawable.btn_bg)
            btnAmount.setTextColor(mActivity.resources.getColor(R.color.colorWhite))

            btnPercentage.setTextColor(mActivity.resources.getColor(R.color.colorPrimary))
            btnPercentage.background = mActivity.resources.getDrawable(R.drawable.edt_bg)

            rlAmount.visibility = View.VISIBLE
            rlPercentage.visibility = View.GONE
            keyboardAmount.visibility = View.VISIBLE
            keyboardPercentage.visibility = View.GONE

            edtOpenDiscountAmount.requestFocus()
            getDialog()?.getWindow()
                ?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


            edtOpenDiscountAmount.setRawInputType(InputType.TYPE_CLASS_TEXT)
            edtOpenDiscountAmount.setTextIsSelectable(false)

            edtOpenDiscountAmount.setSelection(edtOpenDiscountAmount.text!!.length)

            edtOpenDiscountAmount.addTextChangedListener(GenericTextWatcher(edtOpenDiscountAmount))

            ivClearAmount.setOnClickListener {
                edtOpenDiscountAmount.setText("")
                edtOpenDiscountAmount.requestFocus()
            }
        } else if (discountType.equals(AppConstant.PERCENTAGE, true)) {
            btnPercentage.background = mActivity.resources.getDrawable(R.drawable.btn_bg)
            btnPercentage.setTextColor(mActivity.resources.getColor(R.color.colorWhite))

            btnAmount.setTextColor(mActivity.resources.getColor(R.color.colorPrimary))
            btnAmount.background = mActivity.resources.getDrawable(R.drawable.edt_bg)

            rlAmount.visibility = View.GONE
            rlPercentage.visibility = View.VISIBLE
            keyboardAmount.visibility = View.GONE
            keyboardPercentage.visibility = View.VISIBLE
            edtOpenDiscountPercentage.setFilters(
                arrayOf(
                    InputFilterMinMax("0", "100"),
                    LengthFilter(3)
                )
            )
            edtOpenDiscountPercentage.requestFocus()
            getDialog()?.getWindow()
                ?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

            edtOpenDiscountPercentage.setRawInputType(InputType.TYPE_CLASS_TEXT)
            edtOpenDiscountPercentage.setTextIsSelectable(false)

            edtOpenDiscountPercentage.setSelection(edtOpenDiscountPercentage.text!!.length)

            /*edtOpenDiscountPercentage.addTextChangedListener(
                GenericTextWatcher(
                    edtOpenDiscountPercentage
                )
            )*/

            ivClearPercentage.setOnClickListener {
                edtOpenDiscountPercentage.setText("")
                edtOpenDiscountPercentage.requestFocus()
            }
        } else {
            btnAmount.background = mActivity.resources.getDrawable(R.drawable.btn_bg)
            btnAmount.setTextColor(mActivity.resources.getColor(R.color.colorWhite))

            btnPercentage.setTextColor(mActivity.resources.getColor(R.color.colorPrimary))
            btnPercentage.background = mActivity.resources.getDrawable(R.drawable.edt_bg)

            edtOpenDiscountAmount.visibility = View.VISIBLE
            edtOpenDiscountPercentage.visibility = View.GONE

            keyboardAmount.visibility = View.VISIBLE
            keyboardPercentage.visibility = View.GONE
        }
    }

}