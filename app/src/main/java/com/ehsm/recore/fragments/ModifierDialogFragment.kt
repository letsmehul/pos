//package com.ehsm.recore.fragments
//
//import android.graphics.Color
//import android.graphics.drawable.ColorDrawable
//import android.os.Bundle
//import android.util.Log
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import android.widget.ImageView
//import androidx.fragment.app.DialogFragment
//import androidx.recyclerview.widget.RecyclerView
//import com.ehsm.recore.R
//import com.ehsm.recore.adapter.IngredientListAdapter
//import com.ehsm.recore.adapter.ModifierListAdapter
//import com.ehsm.recore.adapter.PreModifierListAdapter
//import com.ehsm.recore.database.RecoreDB
//import com.ehsm.recore.model.IngredientMasterModel
//import com.ehsm.recore.model.ModifierMappingModel
//import com.ehsm.recore.model.ModifierMasterModel
//import com.ehsm.recore.model.PreModifierMasterModel
//
//
//class ModifierDialogFragment(var item_id: Int) : DialogFragment(),
//    ModifierListAdapter.ItemClickModifier,
//    PreModifierListAdapter.ItemClickPreModifier,
//    IngredientListAdapter.ItemClickIngredient {
//    lateinit var ivCancel: ImageView
//    lateinit var rvModifierGroup: RecyclerView
//    lateinit var rvPremodifier: RecyclerView
//    lateinit var rvIngrediant: RecyclerView
//    private var modifierList = ArrayList<ModifierMasterModel>()
//
//    private var preModifierList = ArrayList<PreModifierMasterModel>()
//    private var ingredientList = ArrayList<IngredientMasterModel>()
//    private var selected_modifier: ModifierMasterModel? = null
//    private var selected_pre_modifier: PreModifierMasterModel? = null
//    private var selected_ingredient: IngredientMasterModel? = null
//
//    private lateinit var mDatabase: RecoreDB
//
//    override fun onCreateView(
//        inflater: LayoutInflater,
//        container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View {
//        mDatabase = RecoreDB.getInstance(context!!)!!
//
//        getDialog()?.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
//        val view = inflater.inflate(R.layout.dialog_modifier, container, false)
//        setViews(view)
//        ivCancel.setOnClickListener {
//            val test = ModifierMappingModel(
//                0,
//                item_id.toString(),
//                "",
//                selected_modifier!!.iModifierGroupId.toString(),
//                selected_pre_modifier!!.iPreModifierId,
//                selected_ingredient!!.iModifierGroupOptionId,
//                selected_ingredient!!.dblOptionPrice,
//                selected_pre_modifier!!.strPreModifierName
//            )
//            mDatabase.daoModifierMapping().insertModifierMapping(test)
//            //   callback(test)
//
//            dismiss()
//        }
//        setModifierData()
//
//        return view
//    }
//
//
//    private fun setModifierData() {
//        modifierList.clear()
//        for (i in 0..11) {
//            val test_obj = ModifierMasterModel(i, "", "", "", "",
//                "", 0, 0, 0, 0,0,0,"","","",0,0,"",0)
//            modifierList.add(test_obj)
//        }
//        for (i in mDatabase.daoModifierGroupList().getModifierGroupList(item_id).indices) {
//            modifierList.removeAt(i)
//            modifierList.add(i,mDatabase.daoModifierGroupList().getModifierGroupList(item_id).get(i))
//        }
//
//       // modifierList.addAll(mDatabase.daoModifierGroupList().getModifierGroupList(item_id))
//        rvModifierGroup.adapter = ModifierListAdapter(context!!, modifierList, this)
//        if (modifierList != null && modifierList.size > 0) {
//            selected_modifier = modifierList.get(0)
//            getPreModifierData(modifierList.get(0).iModifierGroupId.toString())
//            getIngredientData(modifierList.get(0).iModifierGroupId.toString())
//
//        }
//    }
//
//    private fun getPreModifierData(modifierIdGroupId: String) {
//        preModifierList.clear()
//
//        for (i in 0..5) {
//            val test_obj = PreModifierMasterModel(i.toString(),"","","","" ,"",0,0)
//            preModifierList.add(test_obj)
//        }
//        for (i in mDatabase.daoPreModifierList().getPreModifierList(modifierIdGroupId.toInt(),item_id).indices) {
//            preModifierList.removeAt(i)
//            preModifierList.add(i,mDatabase.daoPreModifierList().getPreModifierList(modifierIdGroupId.toInt(),item_id).get(i))
//        }
//
//     //   preModifierList.addAll(mDatabase.daoPreModifierList().getPreModifierList(modifierIdGroupId.toInt()))
//        rvPremodifier.adapter = PreModifierListAdapter(context!!, preModifierList, this)
//        if (preModifierList != null && preModifierList.size > 0) {
//            selected_pre_modifier = preModifierList.get(0)
//        }
//
//    }
//
//    private fun getIngredientData(modifierIdGroupId: String) {
//        ingredientList.clear()
//
//        for (i in 0..17) {
//            val test_obj = IngredientMasterModel(i.toString(),0,"","",0,false,0)
//            ingredientList.add(test_obj)
//        }
//        for (i in mDatabase.daoIngredientList().getIngredientList(modifierIdGroupId,item_id).indices) {
//            ingredientList.removeAt(i)
//            ingredientList.add(i,mDatabase.daoIngredientList().getIngredientList(modifierIdGroupId,item_id).get(i))
//        }
//
//       // ingredientList.addAll(mDatabase.daoIngredientList().getIngredientList(modifierIdGroupId))
//        rvIngrediant.adapter = IngredientListAdapter(context!!, ingredientList, this)
//        if (ingredientList != null && ingredientList.size > 0) {
//            selected_ingredient = ingredientList.get(0)
//        }
//    }
//
//    private fun setViews(view: View) {
//        ivCancel = view.findViewById(R.id.ivCancel)
//
//        rvModifierGroup = view.findViewById(R.id.rvModifierGroup)
//        rvPremodifier = view.findViewById(R.id.rvPremodifier)
//        rvIngrediant = view.findViewById(R.id.rvIngrediant)
//
//    }
//
//    override fun listItemClickedModifier(position: Int, modifierId: String) {
//        selected_modifier = modifierList.get(position)
//        getPreModifierData(modifierId)
//        getIngredientData(modifierId)
//    }
//
//    override fun listItemClickedPreModifier(position: Int, pre_modifierId: String) {
//        selected_pre_modifier = preModifierList.get(position)
//    }
//
//    override fun listItemClickedIngredient(position: Int, ingredientId: String) {
//        selected_ingredient = ingredientList.get(position)
//    }
//
//
//}
