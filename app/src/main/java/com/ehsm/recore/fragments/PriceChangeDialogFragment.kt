package com.ehsm.recore.fragments

import android.app.Activity
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.InputType
import android.text.TextUtils
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.annotation.LayoutRes
import androidx.fragment.app.DialogFragment
import com.ehsm.recore.R
import com.ehsm.recore.custom.keyboard.DateKeyboard
import com.ehsm.recore.custom.keyboard.KeyBoardLayout
import com.ehsm.recore.custom.keyboard.NumberDecimalEditText
import com.ehsm.recore.interfaces.ItemCallBack
import com.ehsm.recore.model.OrderTransModel
import com.ehsm.recore.utils.CM.showToast
import com.ehsm.recore.utils.GenericTextWatcher
import com.ehsm.recore.utils.UserInteractionAwareCallback
import kotlinx.android.synthetic.main.custom_toast.*
import kotlinx.android.synthetic.main.custom_toast.view.*

class PriceChangeDialogFragment(
    val orderTransModel: OrderTransModel,
    val callback: (amount: Double) -> Unit
) :
    DialogFragment() {
    var callBackListener: ItemCallBack<OrderTransModel>? = null
    lateinit var ivCancel: ImageView
    lateinit var btnSubmit: Button

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        getDialog()?.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        dialog?.window?.setCallback(
                UserInteractionAwareCallback(
                        dialog?.window!!.getCallback(),
                        activity
                )
        )
        val view = inflater.inflate(R.layout.dialog_change_price, container, false)

        ivCancel = view.findViewById(R.id.ivCancel)
        val tvUserNameLogin = view.findViewById<TextView>(R.id.tvUserNameLogin)
        tvUserNameLogin.setText("Item : " + orderTransModel.itemName)
//        val mKeyBoardLayout = view.findViewById<KeyBoardLayout>(R.id.keyboard_layout)
        btnSubmit = view.findViewById<Button>(R.id.btnSubmit)
        btnSubmit.setText(getString(R.string.submit))

        val edtCashInDrawer = view.findViewById<EditText>(R.id.edtCashInDrawer)
        edtCashInDrawer.setText(String.format("%.2f", orderTransModel.itemRate))
        edtCashInDrawer!!.showSoftInputOnFocus = false

        val keyboard: DateKeyboard = view.findViewById(R.id.keyboard) as DateKeyboard
        edtCashInDrawer.setRawInputType(InputType.TYPE_CLASS_TEXT)
        edtCashInDrawer.setTextIsSelectable(false)

        edtCashInDrawer.setSelection(edtCashInDrawer.text.length)

        val ic = edtCashInDrawer.onCreateInputConnection(EditorInfo())
        keyboard.setInputConnection(ic)

        edtCashInDrawer.addTextChangedListener(GenericTextWatcher(edtCashInDrawer))

        val ivClearAmonut = view.findViewById<ImageView>(R.id.ivClearAmount)
        ivClearAmonut.setOnClickListener {
            edtCashInDrawer.setText("")
            edtCashInDrawer.requestFocus()
        }

        ivCancel.setOnClickListener {
            dismiss()
        }
        btnSubmit.setOnClickListener {
            if (!TextUtils.isEmpty(edtCashInDrawer.text.toString())) {
                try {
                    if (edtCashInDrawer.text.toString().toDouble() <= 0) {
                        showToast(
                            context as Activity,
                            getString(R.string.enter_valid_amount),
                            R.drawable.icon,
                            custom_toast_container
                        )
                    } else if ((orderTransModel.itemQty * edtCashInDrawer.text.toString()
                            .toDouble()) <= orderTransModel.discountAmount
                    ) {
                        showToast(
                            context as Activity,
                            getString(R.string.instore_discount_validation),
                            R.drawable.icon,
                            custom_toast_container
                        )
                    } else {
                        callback(edtCashInDrawer.text.toString().toDouble())
                        dismiss()
                    }

                } catch (e: NumberFormatException) {
                    showToast(
                        context as Activity,
                        getString(R.string.msg_enter_proper_amount),
                        R.drawable.icon,
                        custom_toast_container
                    )
                }


            } else {
                showToast(
                    context as Activity,
                    getString(R.string.enter_valid_amount),
                    R.drawable.icon,
                    custom_toast_container
                )
            }

        }

        return view
    }


    fun setListener(callBackListener: ItemCallBack<OrderTransModel>) {
        this.callBackListener = callBackListener

    }

}
