package com.ehsm.recore.fragments

//import com.paginate.abslistview.LoadingListItemCreator
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.*
import android.text.*
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatSpinner
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ehsm.recore.BuildConfig
import com.ehsm.recore.R
import com.ehsm.recore.activities.AddCustomerActivity
import com.ehsm.recore.activities.DiscountWebViewActivity
import com.ehsm.recore.activities.ParentActivity
import com.ehsm.recore.adapter.*
import com.ehsm.recore.custom.keyboard.DateKeyboard
import com.ehsm.recore.custom.keyboard.KeyBoardLayout
import com.ehsm.recore.custom.keyboard.NumberDecimalEditText
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.database.dao.OrderHistoryListDAO
import com.ehsm.recore.databinding.FragmentNewSalesBinding
import com.ehsm.recore.interfaces.FragmentKeyeventListener
import com.ehsm.recore.interfaces.ItemCallBack
import com.ehsm.recore.model.*
import com.ehsm.recore.network.BoltApiHelper
import com.ehsm.recore.network.BoltRestAPIClient
import com.ehsm.recore.repository.DiscountListRepository
import com.ehsm.recore.repository.PaymentRepository
import com.ehsm.recore.utils.*
import com.ehsm.recore.utils.CM.convertDateFormate
import com.ehsm.recore.utils.CM.showToast
import com.ehsm.recore.utils.CV.BARCODE_IMAGE_NAME
import com.ehsm.recore.utils.CV.SQUARE_IMAGE_NAME
import com.ehsm.recore.utils.CV.orderTransList
import com.ehsm.recore.utils.CV.productRequireHashMap
import com.ehsm.recore.utils.CV.tempHashMap
import com.ehsm.recore.utils.logger.LogUtil
import com.ehsm.recore.viewmodel.CategoryListViewModel
import com.ehsm.recore.viewmodel.DiscountListViewModel
import com.ehsm.recore.viewmodel.PaymentViewModel
import com.ehsm.recore.viewmodel.SearchViewModel
import com.ehsm.recore.viewmodelfactory.viewModelFactory
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.kcspl.divyangapp.viewmodel.*
import com.paginate.Paginate
import kotlinx.android.synthetic.main.custom_toast.*
import kotlinx.android.synthetic.main.dialog_modifier.view.*
import kotlinx.android.synthetic.main.dialog_opendiscount.*
import kotlinx.android.synthetic.main.dialog_search_item.*
import kotlinx.android.synthetic.main.fragment_new_sales.*
import kotlinx.coroutines.*
import okhttp3.internal.format
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.coroutines.CoroutineContext
import kotlin.reflect.jvm.internal.impl.load.java.Constant
import android.text.Editable
import androidx.core.widget.addTextChangedListener
import com.ehsm.recore.utils.CM.convertDatetospecifictimezone
import com.ehsm.recore.utils.CV.PRODUCTS_TYPE_KITCHEN
import kotlinx.android.synthetic.main.dialog_search_order.*
import com.google.firebase.crashlytics.internal.model.CrashlyticsReport.Session.User
import java.math.BigDecimal
import java.time.ZoneId


class NewSalesFragment : BaseFragment(), CategoryListAdapter.ItemClickCategory,
    SubCategoryListAdapter.ItemClickSubCategory,
    CustomerListAdapter.ListItemClickedCustomer,
    ItemListSearchAdapter.ListItemClickedItem,
    ModifierListAdapter.ItemClickModifier,
    PreModifierListAdapter.ItemClickPreModifier,
    IngredientListAdapter.ItemClickIngredient,
    ItemCallBack<OrderTransModel>,
    OrderHistoryListAdapter.ListItemClickedOrder,
    RefundAllItemListAdapter.ListItemClicked,
    RefundSelectedItemListAdapter.RefundItemClicked, CoroutineScope, Paginate.Callbacks,
    FragmentKeyeventListener {
    private lateinit var mBinding: FragmentNewSalesBinding
    private lateinit var mDatabase: RecoreDB
    private var categoryListViewModel: CategoryListViewModel? = null
    private var subCategoryListViewModel: SubCategoryListViewModel? = null
    private var itemListViewModel: ItemListViewModel? = null
    private var customerListViewModel: CustomerListViewModel? = null
    private var viewModelChangeMPin: ChangeMPinViewModel? = null
    private var orderHistoryListViewModel: OrderHistoryViewModel? = null
    private var placeOrderViewModel: PlaceOrderViewModel? = null
    private var paymentRefundViewModel: PaymentRefundViewModel? = null
    private var modifierList = ArrayList<ModifierMasterModel>()
    private var selected_modifier: ModifierMasterModel? = null
    private var ingredientList = ArrayList<IngredientMasterModel>()
    private var refundItemList = ArrayList<OrderRefundItemModel>()

    private var paymentMethodList: ArrayList<PaymentMethodModelData>? = null
    private lateinit var adapter_ingredient: IngredientListAdapter
    val delay = 1000
    var last_text_edit = 0
    override val coroutineContext: CoroutineContext = Dispatchers.Main
    var strAmount: ArrayList<Int> = ArrayList()
    val hashMap = HashMap<Int, DiscountGeneric>()
    private var categoryList = ArrayList<CategoryModel>()
    private var subcategoryList = ArrayList<SubCategoryModel>()
    private var itemList = ArrayList<ItemModel>()
    var allDataModel: ArrayList<ItemModel> = ArrayList()

    private var customerList = ArrayList<CustomerModel>()
    private var orderHistoryList = ArrayList<OrderHistoryModel>()
    private lateinit var orderHistoryListAdapter: OrderHistoryListAdapter
    private val customLoadingListItem = false
    var search = ""

    //var orderTransList: ArrayList<OrderTransModel> = ArrayList()
    var orderAllCartDiscountItems: ArrayList<OrderTransModel> = ArrayList()
    var orderAllSelectCartDiscountItems: ArrayList<OrderTransModel> = ArrayList()

    private var isRefundDiscountApplied = false

    var itemRequestList: ArrayList<OrderTransRequestModel> = ArrayList()
    private var selected_ingredient: IngredientMasterModel? = null
    private var selected_pre_modifier: PreModifierMasterModel? = null

    private var preModifierList = ArrayList<PreModifierMasterModel>()
    private lateinit var ivRoundOneCredential: ImageView
    private lateinit var ivRoundTwoCredential: ImageView
    private lateinit var ivRoundThreeCredential: ImageView
    private lateinit var ivRoundFourCredential: ImageView
    private lateinit var llPinRound: LinearLayout

    private lateinit var dialog: Dialog
    private var count = 0
    private var retry_count = 0
    private var minusCount = false
    private var isRefundDialogOpen = false
    private var pin = ""
    private var viewModelVerifyMPin: VerifyMPinViewModel? = null
    private var dialogRefund: Dialog? = null
    private var order_id = ""
    private var temp_order_id = ""
    private var selectedAge: Int? = null
    private var emp_name = ""
    private var emp_role = ""
    private var emp_image = ""
    private var emp_id = 0
    private var trasaction_emp_id = 0
    private var trasaction_emp_name = ""
    private var trasaction_emp_role = ""
    private var total_tax_amount = 0.0
    private var selected_history: OrderHistoryModel? = null
    private var mActivity = AppCompatActivity()
    private var InvoiceNumber = ""
    private var Btncalculate_change_flag=false
    private var isKitchenPrintServed = true
    private var isbringBackKitchenOrder = false
    private var isOrderContainKitchenProduct = false
    private var isHistoryOrderContainKitchenProduct = false

    //private var mTransactionNumber = ""
    private var isPrint = false
    private var customerListFilter = ArrayList<CustomerModel>()
    private var orderListFilter = ArrayList<OrderHistoryModel>()
    private var tempHistoryList = ArrayList<OrderHistoryModel>()
    private var itemListFilter = ArrayList<ItemModel>()
    private var itemSearchAdapter: ItemListSearchAdapter? = null
    private var categoryListFilter = ArrayList<CategoryModel>()
    lateinit var dialogSearchCustomer: Dialog
    var filterProduct: ArrayList<CustomerModel> = ArrayList()
    private var total = 0.0
    private lateinit var dialogorder: Dialog
    private lateinit var dialogSearchItem: Dialog
    private lateinit var rvOrder: RecyclerView
    private lateinit var tvNodataFound: TextView
    var orderTransListSelected: ArrayList<OrderTransModel> = ArrayList()
    var orderTransRefundList: ArrayList<OrderTransModel> = ArrayList()
    private lateinit var tvTotalRight: TextView
    private lateinit var containerRefundSelectedDiscount: LinearLayout
    private lateinit var containerRefundSelectedDiscountRemark: LinearLayout
    private lateinit var tvRefundselectedCartDiscount: TextView
    private lateinit var tvRefundselectedCartDiscountRemark: TextView
    private lateinit var mRefundSelectedAdapter: RefundSelectedItemListAdapter
    private lateinit var mRefundAdapter: RefundAllItemListAdapter
    private var id = ""
    private var currency_symbol = ""
    private var payment_type = ""
    private var payment_mode = ""
    private var totalPages = 0
    private var currentPageNo = 0
    private var hasNextPage = false
    private var loading = false
    private lateinit var paginate: Paginate
    private var history_type = ""
    private var history_status = ""
    private var start_date_order_history = ""
    private var end_date_order_history = ""
    private var discount_name = ""

    var mPrinterSalesProducts: ArrayList<PrinterSalesProductsModel> = ArrayList()
    var mPrinterRefundProducts: ArrayList<PrinterRefundProductsModel> = ArrayList()

    private var searchViewModel: SearchViewModel? = null

    private var lastFourDigit = ""
    private var cardHolderName = ""
    private var last_action_status = ""
    private var isCheckIn_status = false

    private var StoreName = ""
    private var StoreAddress1 = ""
    private var StoreAddress2 = ""
    private var StorePhoneNum = ""
    private var cartAdapter: OrderItemListAdapter? = null

    private var eventName = AppConstant.ORDER
    private lateinit var paymentViewModel: PaymentViewModel

    private var item_id = 0
    private var order_product_id=""
    private var pos = 0

    private var discountListViewModel: DiscountListViewModel? = null
    val mImagesCache = ImagesCache.getInstance()
    private var mBarcodeGenerator: BarcodeGenerator? = null
    private var lastStatusViewModel: LastStatusDayStartCloseViewModel? = null

    // this var used for divide item level discount
    var coupnDiscountAmount: Float = 0.0f
    var totalApplicableItemDiscount: Double = 0.0
    lateinit var tempPaymentMode: String

    //payment Proces dialog
    var paymentRequestList: ArrayList<PaymentRequestModel> = ArrayList()
    var paymentList: ArrayList<PaymentModel> = ArrayList()
    lateinit var rvPayment: RecyclerView
    var dialogcheckout: Dialog? = null
    lateinit var spnSplit: AppCompatSpinner
    lateinit var spnPaymentMethod: AppCompatSpinner
    lateinit var tvBalanceDueValue: TextView
    var ageRestrictionFragment: BirthDateDialog? = null

    var handler = Handler(Looper.getMainLooper())
    private var lastTimeClicked: Long = 0
    override fun initView(view: View, savedInstances: Bundle?) {
        mActivity = (activity as AppCompatActivity?)!!
        CM.setSp(mActivity, CV.ORDER_ID, "")
        currency_symbol = CM.getSp(mActivity, CV.CURRENCY_CODE, "") as String
        payment_type = CM.getSp(mActivity, CV.PAYMENT_TYPE, AppConstant.CASH) as String

        mDatabase = RecoreDB.getInstance(mActivity)!!
        mImagesCache.initializeCache()

        orderTransList.clear()
        mDatabase.daoOrderTrans().deleteWithoutHoldOrderTrans()
        paymentMethodList =
            mDatabase.daoPaymentList().getPaymentList() as ArrayList<PaymentMethodModelData>
        (activity as ParentActivity).setFragmentKeyeventListener(this)// set the monitor
        orderTransList.clear()
        mBarcodeGenerator = BarcodeGenerator(mActivity)
        categoryListViewModel = ViewModelProviders.of(this).get(CategoryListViewModel::class.java)
        subCategoryListViewModel =
            ViewModelProviders.of(this).get(SubCategoryListViewModel::class.java)
        itemListViewModel = ViewModelProviders.of(this).get(ItemListViewModel::class.java)
        customerListViewModel = ViewModelProviders.of(this).get(CustomerListViewModel::class.java)
        orderHistoryListViewModel =
            ViewModelProviders.of(this).get(OrderHistoryViewModel::class.java)
        viewModelChangeMPin = ViewModelProviders.of(this).get(ChangeMPinViewModel::class.java)
        placeOrderViewModel = ViewModelProviders.of(this).get(PlaceOrderViewModel::class.java)
        paymentRefundViewModel = ViewModelProviders.of(this).get(PaymentRefundViewModel::class.java)
        viewModelVerifyMPin = ViewModelProviders.of(this).get(VerifyMPinViewModel::class.java)
        lastStatusViewModel =
            ViewModelProviders.of(this).get(LastStatusDayStartCloseViewModel::class.java)
        val boltApiHelper = BoltApiHelper(BoltRestAPIClient.getService()!!)

        paymentViewModel = ViewModelProviders.of(this,
            viewModelFactory {
                val paymentViewModel1 =
                    PaymentViewModel(mActivity.application, PaymentRepository(boltApiHelper),this)
                paymentViewModel1
            }
        ).get(PaymentViewModel::class.java)

        discountListViewModel = ViewModelProviders.of(mActivity,
            viewModelFactory {
                DiscountListViewModel(DiscountListRepository(mDatabase))
            }
        ).get(DiscountListViewModel::class.java)


        CM.removeStatusBar(mActivity)

        emp_id = CM.getSp(mActivity, CV.EMP_ID, 0) as Int

        emp_name = CM.getSp(mActivity, CV.EMP_NAME, "") as String
        emp_role = CM.getSp(mActivity, CV.EMP_ROLE, "") as String
        mBinding.tvEmpName.text = emp_name

        emp_image = CM.getSp(mActivity, CV.EMP_IMAGE, "") as String

        StoreName = CM.getSp(mActivity, CV.STORE_NAME, "") as String
        StoreAddress1 = CM.getSp(mActivity, CV.STORE_ADD_LINE1, "") as String
        StoreAddress2 = CM.getSp(mActivity, CV.STORE_ADD_LINE2, "") as String
        StorePhoneNum = CM.getSp(mActivity, CV.STORE_TEL_NUM, "") as String

        Glide.with(mActivity)
            .load(emp_image)
            .placeholder(R.drawable.icon)
            .error(R.drawable.user)
            .into(mBinding.ivUser)

        val categoryListLiveData = mDatabase.daoCategoryList().getCategories()
        categoryListLiveData.observe(this, androidx.lifecycle.Observer { mCategorylist ->
            if (mCategorylist != null) {
                categoryList = mCategorylist as ArrayList<CategoryModel>
                categoryListFilter = categoryList
                mBinding.rvCategory.adapter = CategoryListAdapter(mActivity, categoryList, this)
                if (categoryList.isNotEmpty()) {
                    populateSubcategory(categoryList.get(0).iProductCategoryID)
                    mBinding.tvCategoryValue.setText(categoryList.get(0).strProductCategoryName)
                }
            }
        })

        mBinding.reluser.setOnClickListener {
            openChangePasscodeDialog()
        }

        mBinding.ivAddDiscount.setOnClickListener {
            if (orderTransList.size > 0) {
                if (!CM.isInternetAvailable(mActivity)) {
                    CM.showMessageOK(
                        mActivity,
                        "",
                        resources.getString(R.string.msg_network_error),
                        null
                    )
                } else {
                    discountListViewModel?.let {
                        var filteredNonDiscountOrderList =
                            orderTransList.filter { item -> item.iDiscountId == 0 }
                        if (filteredNonDiscountOrderList.isEmpty()) {
                            showToast(
                                mActivity,
                                getString(R.string.multiple_discount_restriction),
                                R.drawable.icon,
                                custom_toast_container
                            )
                        } else if (it.isCartLevelDiscountApplied(order_id)) {
                            showToast(
                                mActivity,
                                getString(R.string.msg_discount_already_applied),
                                R.drawable.icon, custom_toast_container
                            )
                        } else {
                            showDiscountDialogFragment()
                        }
                    }
                }
            } else {
                showToast(
                    mActivity,
                    getString(R.string.msg_discount_can_not_apply_on_empty_cart),
                    R.drawable.icon,
                    custom_toast_container
                )
            }

        }
        mBinding.btnCancel.setOnClickListener {
            onCancelProcedure()
        }

        mBinding.btnExchange.setOnClickListener {
            if (!selected_history!!.status.equals(
                    AppConstant.HistoryType.VOID.toString().toUpperCase()
                )
            ) {
                //Hardware Code For CDS Display -- Refund Cancel case invoke
//                DisplayRefundDataToCDS(AppConstant.CDS_REFUND_SCREEN, ArrayList<OrderTransModel>(),"This refund order has been canceled.")
                onRefundCancelProcedure()
                DisplayNewSaleDataToCDS(
                    ArrayList<OrderTransModel>(),
                    "This refund order has been canceled.",
                    false
                )
            } else {
                onRefundCancelProcedure()
                var baseActivityPrint = (activity as ParentActivity)
                //Hardware Code For CDS Display -- Normal Image Display Case Invoke
                baseActivityPrint.checkDrawOverlayPermission(
                    AppConstant.CDS_NORMAL_SCREEN,
                    "",
                    false,
                    "",
                    CDSSalesModel(),
                    "",
                    false
                )
            }

        }

        mBinding.llSearchItems.setOnClickListener {
            if (!mBinding.btnRefund.isVisible) {
                if (isModifirePopupClose()) {
                    menuClose()
                    openSearchItemDialog()
                }
            }
        }

        mBinding.ivCancel.setOnClickListener {
            if (checkIsRequiredModifireSelected()) {
                if (gridItem.adapter.count==0)
                {
                    mBinding.tvNoProductFound.visibility=View.VISIBLE
                }
                mBinding.ivCancel.isEnabled = true
                mBinding.gridItem.visibility = View.VISIBLE
                mBinding.layoutModifier.visibility = View.GONE
                modifierList.clear()
            } else {
                showToast(
                    mActivity,
                    getString(R.string.err_select_one_modifier),
                    R.drawable.icon,
                    custom_toast_container
                )
            }
        }


        mBinding.gridItem.setOnItemClickListener { parent, view, position, id ->
            Log.d("BARCODE_SCAN", "CLICK :: " + itemList[position].strBarCode)
            if (!mBinding.btnRefund.isVisible) {
                if (customerListViewModel?.selectedCustomer == null) {
                    showToast(
                        mActivity,
                        getString(R.string.msg_please_select_customer_first),
                        R.drawable.icon, custom_toast_container
                    )
                } else {
                    menuClose()
                    if (!searchViewModel?.isSearching!!) {

                        isCustomerEligible(itemList[position])
                    }
                }
            }
        }

        mBinding.ivAddCustomer.setOnClickListener {
            openSearchCustomerDialog()
        }

        mBinding.layoutHistory.setOnClickListener {
            if (orderTransList.size > 0) {
                showToast(
                    mActivity,
                    getString(R.string.msg_empty_cart_to_proceed),
                    R.drawable.icon, custom_toast_container
                )
            } else {
                openSearchOrderHistoryDialog()
            }
        }
        mBinding.ivAdd.setOnClickListener {
            startActivityForResult(Intent(mActivity, AddCustomerActivity::class.java), 1)
        }

        mBinding.llDiscount.setOnClickListener {
            CM.startActivity(mActivity, DiscountWebViewActivity::class.java)
        }

        mBinding.iBtnToggle.setOnClickListener {
            //Restriction to click when modifire popup open
            if (mBinding.layoutModifier.visibility == View.GONE) {
                if (mBinding.llCategory.visibility == View.GONE &&
                    mBinding.llSubCategory.visibility == View.GONE
                ) {
                    mBinding.llCategory.visibility = View.VISIBLE
                    mBinding.llSubCategory.visibility = View.VISIBLE
                    val swipeLeftToRight =
                        AnimationUtils.loadAnimation(mActivity, R.anim.swipe_left_to_right)
                    mBinding.llCategory.startAnimation(swipeLeftToRight)
                    mBinding.llSubCategory.startAnimation(swipeLeftToRight)

                    swipeLeftToRight.setAnimationListener(object : Animation.AnimationListener {
                        override fun onAnimationRepeat(animation: Animation?) {

                        }

                        override fun onAnimationEnd(animation: Animation?) {
                            mBinding.iBtnToggle.rotation = 180.0F
                        }

                        override fun onAnimationStart(animation: Animation?) {

                        }

                    })

                } else {

                    val swipeRightToLeft =
                        AnimationUtils.loadAnimation(mActivity, R.anim.swipe_right_to_left)
                    mBinding.llCategory.startAnimation(swipeRightToLeft)
                    mBinding.llSubCategory.startAnimation(swipeRightToLeft)

                    swipeRightToLeft.setAnimationListener(object : Animation.AnimationListener {
                        override fun onAnimationRepeat(animation: Animation?) {

                        }

                        override fun onAnimationEnd(animation: Animation?) {
                            mBinding.llCategory.visibility = View.GONE
                            mBinding.llSubCategory.visibility = View.GONE
                            mBinding.iBtnToggle.rotation = 0.0F
                        }

                        override fun onAnimationStart(animation: Animation?) {
                        }

                    })
                }
            } else {
                showToast(
                    mActivity,
                    getString(R.string.kindly_close_modifire_popup),
                    R.drawable.icon,
                    custom_toast_container
                )
            }
        }

        mBinding.btnRefund.setOnClickListener {
            /*DisplayRefundDataToCDS(
                AppConstant.CDS_REFUND_SCREEN,
                orderTransList,
                ""
            )*/
            refundItemList.clear()
            orderTransRefundList.clear()
            if (!selected_history?.orderRefund.isNullOrEmpty()) {
                var list = selected_history?.orderRefund!!
                for (refundItemModel in list) {
                    var refundItem = refundItemModel.refundItems!!
                    for (refund in refundItem) {
                        refundItemList.add(refund)
                    }
                }
            }
            dialogRefund = Dialog(mActivity)
            dialogRefund?.let { dialogRefund ->
                //Log.e(TAG, "initView: ${Gson().toJson(selected_history)}")
                isRefundDialogOpen = true
                dialogRefund.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialogRefund.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                dialogRefund.setContentView(R.layout.dialog_refund)
                dialogRefund.window!!.setCallback(
                        UserInteractionAwareCallback(
                                dialogRefund.window!!.getCallback(),
                                mActivity
                        )
                )

                dialogRefund.setCancelable(false)
                dialogRefund.show()
                var isSplitRefund = false
                var isDiscountedOrder = false
                var isModifireOrder = false
                var totalRefundItemQtyCount = 0
                val ivclose = dialogRefund.findViewById<ImageView>(R.id.ivclose)
                val rvAllOrder = dialogRefund.findViewById<RecyclerView>(R.id.rvAllOrder)
                val btnSubmit = dialogRefund.findViewById<Button>(R.id.btnSubmit)
                val btnSelectAll = dialogRefund.findViewById<Button>(R.id.btnSelectAll)
                val tvTotalLeft = dialogRefund.findViewById<TextView>(R.id.tvTotalLeft)
                tvTotalRight = dialogRefund.findViewById<TextView>(R.id.tvTotalRight)
                val edtReason = dialogRefund.findViewById<EditText>(R.id.edtReason)

                val containerDiscount = dialogRefund.findViewById<LinearLayout>(R.id.parentDiscount)
                val containerDiscountRemark =
                    dialogRefund.findViewById<LinearLayout>(R.id.parentDiscountRemark)
                val tvCartDiscount = dialogRefund.findViewById<TextView>(R.id.tvCartDiscount)
                val tvCartDiscountRemark =
                    dialogRefund.findViewById<TextView>(R.id.tvCartDiscountRemark)

                containerRefundSelectedDiscount =
                    dialogRefund.findViewById<LinearLayout>(R.id.parentselectedDiscount)
                containerRefundSelectedDiscountRemark =
                    dialogRefund.findViewById<LinearLayout>(R.id.parentselectedDiscountRemark)
                tvRefundselectedCartDiscount =
                    dialogRefund.findViewById<TextView>(R.id.tvselectedCartDiscount)
                tvRefundselectedCartDiscountRemark =
                    dialogRefund.findViewById<TextView>(R.id.tvselectedCartDiscountRemark)


                if (selected_history!!.orderDiscounts?.isNullOrEmpty()!!) {
                    containerDiscount.visibility = View.GONE
                    containerDiscountRemark.visibility = View.GONE
                    containerRefundSelectedDiscount.visibility = View.GONE
                    containerRefundSelectedDiscountRemark.visibility = View.GONE
                } else {
                    isDiscountedOrder = true
                    containerDiscount.visibility = View.VISIBLE
                    containerDiscountRemark.visibility = View.VISIBLE

                    containerRefundSelectedDiscount.visibility = View.VISIBLE
                    containerRefundSelectedDiscountRemark.visibility = View.VISIBLE

                    tvCartDiscount.text = currency_symbol + " " + String.format(
                        "%.2f",
                        selected_history!!.orderDiscounts?.get(0)?.dblDiscountAmt
                    )
                    tvCartDiscountRemark.text =
                        selected_history!!.orderDiscounts?.get(0)?.strRemark + CV.WAS_APPLIED

                    tvRefundselectedCartDiscount.text = currency_symbol + " " + String.format(
                        "%.2f",
                        selected_history!!.orderDiscounts?.get(0)?.dblDiscountAmt
                    )
                    tvRefundselectedCartDiscountRemark.text =
                        selected_history!!.orderDiscounts?.get(0)?.strRemark + CV.WAS_APPLIED
                }

                //orderTransList.clear()
                orderTransRefundList.clear()
                orderTransListSelected.clear()
                if (selected_history!!.tCheckTaxExempt == "1") {
                    for (i in selected_history!!.orderProducts!!.indices) {
                        val temp = OrderTransModel(
                            selected_history!!.iCheckDetailID,
                            selected_history!!.strInvoiceNumber,
                            selected_history!!.orderProducts!![i].iCheckItemDetailId.toInt(),
                            selected_history!!.orderProducts!![i].strProductName,
                            selected_history!!.orderProducts!![i].dblPrice.toDouble(),
                            selected_history!!.orderProducts!![i].dblItemQty.toInt(),
                            selected_history!!.orderProducts!![i].dblAmount.toDouble(),
                            "",
                            "",
                            "",
                            selected_history!!.orderProducts!![i].dblItemTaxPercentage,
                            selected_history!!.orderProducts!![i].dblItemTaxAmount,
                            "",
                            NewSaleFragmentCommon.itemRefundedRemaingQty(
                                selected_history!!.orderProducts!![i],
                                refundItemList
                            ),
                            0.0,
                            NewSaleFragmentCommon.itemIsRefunded(
                                selected_history!!.orderProducts!![i],
                                refundItemList
                            ),
                            selected_history!!.orderProducts!![i].strProductType,
                            selected_history!!.orderProducts!![i].iDiscountId.toInt(),
                            selected_history!!.orderProducts!![i].strRemark,
                            selected_history!!.orderProducts!![i].dblItemDiscount.toFloat(),
                            "",
                            GetModifierIngredient(selected_history!!.orderProducts!!.get(i).productModifier),
                            selected_history!!.orderProducts!![i].strInstruction,
                            selected_history!!.orderProducts!!.get(i).kitchenServedquantity,
                            isKitchenPrintServed,
                            tIsOpenDiscount = selected_history!!.orderProducts!![i].tIsOpenDiscount,
                            totalItemQty = selected_history!!.orderProducts!![i].dblItemQty.toInt(),
                            refundProductId = selected_history!!.orderProducts!![i].iProductId
                        )
                        orderTransRefundList.add(temp)
                        if (!TextUtils.isEmpty(selected_history!!.orderProducts!![i].strRemark)) {
                            isDiscountedOrder = true
                        }
                        if (!TextUtils.isEmpty(temp.ingredientName)) {
                            isModifireOrder = true
                        }
                        totalRefundItemQtyCount = totalRefundItemQtyCount + temp.totalItemQty
                        // orderTransList.add(temp)
                    }
                } else {
                    for (i in selected_history!!.orderProducts!!.indices) {
                        val temp = OrderTransModel(
                            selected_history!!.iCheckDetailID,
                            selected_history!!.strInvoiceNumber,
                            selected_history!!.orderProducts!![i].iCheckItemDetailId.toInt(),
                            selected_history!!.orderProducts!![i].strProductName,
                            selected_history!!.orderProducts!![i].dblPrice.toDouble(),
                            selected_history!!.orderProducts!![i].dblItemQty.toInt(),
                            selected_history!!.orderProducts!![i].dblAmount.toDouble(),
                            "",
                            "",
                            "",
                            "0.0",
                            "0.00",
                            "",
                            NewSaleFragmentCommon.itemRefundedRemaingQty(
                                selected_history!!.orderProducts!![i],
                                refundItemList
                            ),
                            0.0,
                            NewSaleFragmentCommon.itemIsRefunded(
                                selected_history!!.orderProducts!![i],
                                refundItemList
                            ),
                            selected_history!!.orderProducts!![i].strProductType,
                            selected_history!!.orderProducts!![i].iDiscountId.toInt(),
                            selected_history!!.orderProducts!![i].strRemark,
                            selected_history!!.orderProducts!![i].dblItemDiscount.toFloat(),
                            "",
                            GetModifierIngredient(selected_history!!.orderProducts!!.get(i).productModifier),
                            selected_history!!.orderProducts!![i].strInstruction,
                            selected_history!!.orderProducts!!.get(i).kitchenServedquantity,
                            isKitchenPrintServed,
                            tIsOpenDiscount = selected_history!!.orderProducts!![i].tIsOpenDiscount,
                            totalItemQty = selected_history!!.orderProducts!![i].dblItemQty.toInt(),
                            refundProductId = selected_history!!.orderProducts!![i].iProductId
                        )
                        orderTransRefundList.add(temp)
                        if (!TextUtils.isEmpty(selected_history!!.orderProducts!![i].strRemark)) {
                            isDiscountedOrder = true
                        }
                        if (!TextUtils.isEmpty(temp.ingredientName)) {
                            isModifireOrder = true
                        }
                        totalRefundItemQtyCount = totalRefundItemQtyCount + temp.totalItemQty
                        //orderTransList.add(temp)
                    }
                }


                mRefundAdapter =
                    RefundAllItemListAdapter(mActivity, orderTransRefundList, this, this)
                rvAllOrder.adapter = mRefundAdapter
                var paymentMode = AppConstant.CASH
                this.selected_history?.orderPayments?.let { list ->
                    if (list.size > 0) {
                        if (list.size > 1) {
                            isSplitRefund = true
                            payment_type = paymentMode
                        } else {
                            paymentMode = list[0].strPaymentMethod
                            payment_type = paymentMode
                        }
                    }
                }
                var orderWithoutRefundList = orderTransRefundList.map { it.copy() }
                    .filter { item -> !item.isRefunded } as ArrayList<OrderTransModel>
                orderWithoutRefundList.forEach { it.itemQty = it.itemQtyLeft }
                calculateCartSelectedRefundProcedure(orderWithoutRefundList) { cart: CartRefundSummaryData ->

                    tvCartDiscount.text =
                        currency_symbol + " " + String.format("%.2f", (cart.totalDiscount))
                    tvTotalLeft.text = currency_symbol + " " + String.format(
                        "%.2f",
                        ((cart.totalAmount + cart.taxAmount) + (cart.saleTotalAmount + cart.saleTaxAmount)) - cart.totalDiscount
                    )
                }

                if (orderTransListSelected.size > 0) {

                    if (selected_history!!.orderDiscounts?.isNullOrEmpty()!!) {
                        containerDiscount.visibility = View.GONE
                        containerDiscountRemark.visibility = View.GONE
                    } else {
                        containerRefundSelectedDiscount.visibility = View.VISIBLE
                        containerRefundSelectedDiscountRemark.visibility = View.VISIBLE
                    }
                } else {
                    tvTotalRight.text = currency_symbol + " " + "00.00"
                    containerRefundSelectedDiscount.visibility = View.GONE
                    containerRefundSelectedDiscountRemark.visibility = View.GONE
                }
                DisplayRefundDataToCDS(
                    AppConstant.CDS_REFUND_SCREEN,
                    orderTransRefundList,
                    ""
                )

                btnSelectAll.setOnClickListener {
                    orderTransRefundList.clear()
                    orderTransList.clear()
                    if (selected_history!!.tCheckTaxExempt == "1") {
                        for (i in selected_history!!.orderProducts!!.indices) {
                            val temp = OrderTransModel(
                                selected_history!!.iCheckDetailID,
                                selected_history!!.strInvoiceNumber,
                                selected_history!!.orderProducts!![i].iCheckItemDetailId.toInt(),
                                selected_history!!.orderProducts!![i].strProductName,
                                selected_history!!.orderProducts!![i].dblPrice.toDouble(),
                                selected_history!!.orderProducts!![i].dblItemQty.toInt(),
                                selected_history!!.orderProducts!![i].dblAmount.toDouble(),
                                "",
                                "",
                                "",
                                selected_history!!.orderProducts!![i].dblItemTaxPercentage,
                                selected_history!!.orderProducts!![i].dblItemTaxAmount,
                                "",
                                NewSaleFragmentCommon.itemRefundedRemaingQty(
                                    selected_history!!.orderProducts!![i],
                                    refundItemList
                                ),
                                0.0,
                                NewSaleFragmentCommon.itemIsRefunded(
                                    selected_history!!.orderProducts!![i],
                                    refundItemList
                                ),
                                selected_history!!.orderProducts!![i].strProductType,
                                selected_history!!.orderProducts!![i].iDiscountId.toInt(),
                                selected_history!!.orderProducts!![i].strRemark,
                                selected_history!!.orderProducts!![i].dblItemDiscount.toFloat(),
                                "",
                                GetModifierIngredient(selected_history!!.orderProducts!!.get(i).productModifier),
                                selected_history!!.orderProducts!![i].strInstruction,
                                selected_history!!.orderProducts!!.get(i).kitchenServedquantity,
                                isKitchenPrintServed,
                                tIsOpenDiscount = selected_history!!.orderProducts!![i].tIsOpenDiscount,
                                totalItemQty = selected_history!!.orderProducts!![i].dblItemQty.toInt(),
                                refundProductId = selected_history!!.orderProducts!![i].iProductId
                            )
                            orderTransRefundList.add(temp)
                            orderTransList.add(temp)

                        }
                    } else {
                        for (i in selected_history!!.orderProducts!!.indices) {
                            val temp = OrderTransModel(
                                selected_history!!.iCheckDetailID,
                                selected_history!!.strInvoiceNumber,
                                selected_history!!.orderProducts!![i].iCheckItemDetailId.toInt(),
                                selected_history!!.orderProducts!![i].strProductName,
                                selected_history!!.orderProducts!![i].dblPrice.toDouble(),
                                selected_history!!.orderProducts!![i].dblItemQty.toInt(),
                                selected_history!!.orderProducts!![i].dblAmount.toDouble(),
                                "",
                                "",
                                "",
                                "0.0",
                                "0.00",
                                "",
                                NewSaleFragmentCommon.itemRefundedRemaingQty(
                                    selected_history!!.orderProducts!![i],
                                    refundItemList
                                ),
                                0.0,
                                NewSaleFragmentCommon.itemIsRefunded(
                                    selected_history!!.orderProducts!![i],
                                    refundItemList
                                ),
                                selected_history!!.orderProducts!![i].strProductType,
                                selected_history!!.orderProducts!![i].iDiscountId.toInt(),
                                selected_history!!.orderProducts!![i].strRemark,
                                selected_history!!.orderProducts!![i].dblItemDiscount.toFloat(),
                                "",
                                GetModifierIngredient(selected_history!!.orderProducts!!.get(i).productModifier),
                                selected_history!!.orderProducts!![i].strInstruction,
                                selected_history!!.orderProducts!!.get(i).kitchenServedquantity,
                                isKitchenPrintServed,
                                tIsOpenDiscount = selected_history!!.orderProducts!![i].tIsOpenDiscount,
                                totalItemQty = selected_history!!.orderProducts!![i].dblItemQty.toInt(),
                                refundProductId = selected_history!!.orderProducts!![i].iProductId
                            )
                            orderTransRefundList.add(temp)
                            orderTransList.add(temp)
                        }
                    }

                    orderTransListSelected.clear()
                    for (i in orderTransRefundList.indices) {
                        val itemOrderTrans = orderTransRefundList[i]
                        if (!itemOrderTrans.isRefunded) {
                            var actualQty = itemOrderTrans.itemQty
                            val temp = OrderTransModel(
                                itemOrderTrans.id,
                                itemOrderTrans.orderId,
                                itemOrderTrans.itemId,
                                itemOrderTrans.itemName,
                                itemOrderTrans.itemRate,
                                itemOrderTrans.itemQtyLeft,
                                itemOrderTrans.itemAmount,
                                itemOrderTrans.strPricingType,
                                itemOrderTrans.paymentType,
                                itemOrderTrans.dateTime,
                                itemOrderTrans.taxPercentage,
                                itemOrderTrans.taxAmount,
                                itemOrderTrans.reason,
                                itemOrderTrans.itemQtyLeft,
                                itemOrderTrans.grandTotal,
                                false,
                                selected_history!!.orderProducts!![i].strProductType,
                                selected_history!!.orderProducts!![i].iDiscountId.toInt(),
                                selected_history!!.orderProducts!![i].strRemark,
                                selected_history!!.orderProducts!![i].dblItemDiscount.toFloat(),
                                itemOrderTrans.premodifierType,
                                itemOrderTrans.ingredientName,
                                itemOrderTrans.strInstruction,
                                selected_history!!.orderProducts!![i].kitchenServedquantity,
                                isKitchenPrintServed,
                                tIsOpenDiscount = itemOrderTrans.tIsOpenDiscount,
                                totalItemQty = actualQty,
                                refundProductId = selected_history!!.orderProducts!![i].iProductId
                            )
                            orderTransListSelected.add(temp)
                        }
                    }

                    ShowCartLevelDiscount(orderTransListSelected)

                    val rvselectedOrder =
                        dialogRefund.findViewById<RecyclerView>(R.id.rvSelectedOrder)
                    mRefundSelectedAdapter =
                        RefundSelectedItemListAdapter(
                            this@NewSalesFragment,
                            mActivity,
                            orderTransListSelected,
                            tvTotalRight,
                            containerRefundSelectedDiscount,
                            containerRefundSelectedDiscountRemark,
                            tvRefundselectedCartDiscount,
                            tvRefundselectedCartDiscountRemark,
                            this
                        )
                    rvselectedOrder.adapter = mRefundSelectedAdapter

                    if (orderTransListSelected.size > 0) {

                        if (selected_history!!.orderDiscounts?.isNullOrEmpty()!!) {
                            containerRefundSelectedDiscount.visibility = View.GONE
                            containerRefundSelectedDiscountRemark.visibility = View.GONE
                        } else {
                            containerRefundSelectedDiscount.visibility = View.VISIBLE
                            containerRefundSelectedDiscountRemark.visibility = View.VISIBLE
                        }
                    } else {
                        tvTotalRight.text = currency_symbol + " " + "00.00"
                        containerRefundSelectedDiscount.visibility = View.GONE
                        containerRefundSelectedDiscountRemark.visibility = View.GONE
                    }

                    //Hardware Code For CDS Display -- Select All Refund case invoke
                    DisplayRefundDataToCDS(
                        AppConstant.CDS_REFUND_SCREEN,
                        orderTransListSelected,
                        ""
                    )

                    mRefundAdapter.run {
                        updateData(orderTransRefundList)
                    }
                }
                ivclose.setOnClickListener {
                    orderTransListSelected.clear()
                    orderTransRefundList.clear()
                    dialogRefund.dismiss()
                    isRefundDialogOpen = false
                    DisplayNewSaleDataToCDS(orderTransList, "", false)
                }

                btnSubmit.setOnClickListener {
                    CM.onKeyboardClose(edtReason, mActivity)
                    isRefundDialogOpen = false
                    var qtyCount = 0
                    var selectedTotalRefundItemQtyCount = 0
                    for (i in orderTransListSelected.indices) {
                        if (orderTransListSelected[i].itemQty == 0) {
                            qtyCount++
                        }
                        selectedTotalRefundItemQtyCount =
                            selectedTotalRefundItemQtyCount + orderTransListSelected[i].itemQty
                    }
                    if (payment_type.equals(AppConstant.CASH) || NewSaleFragmentCommon.isOtherCashTrasaction(
                            paymentMethodList, payment_type
                        )
                    ) {
                        if (!TextUtils.isEmpty(
                                CM.getSp(mActivity, CV.CASH_IN_DRAWER, "").toString()
                            )
                        ) {
                            if (CM.getSp(mActivity, CV.CASH_IN_DRAWER, "").toString()
                                    .toDouble() < tvTotalRight.text.toString().split(" ").get(1)
                                    .toDouble()
                            ) {
                                showToast(
                                    mActivity,
                                    getString(R.string.msg_not_enough_cash_in_drawer),
                                    R.drawable.icon,
                                    custom_toast_container
                                )
                                return@setOnClickListener
                            }

                        }
                    }
                        if (orderTransListSelected.size == 0) {
                            showToast(
                                mActivity,
                                getString(R.string.msg_please_select_product_to_refund),
                                R.drawable.icon, custom_toast_container
                            )
                        } else if (qtyCount > 0) {
                            showToast(
                                mActivity,
                                getString(R.string.msg_enter_quantity),
                                R.drawable.icon, custom_toast_container
                            )
                        } else {
                            if (edtReason.text.toString().equals("")) {
                                showToast(
                                    mActivity,
                                    getString(R.string.msg_enter_reason),
                                    R.drawable.icon, custom_toast_container
                                )
                            } else {
                                if (!CM.isInternetAvailable(mActivity)) {
                                    CM.showMessageOK(
                                        mActivity,
                                        "",
                                        resources.getString(R.string.msg_network_error),
                                        null
                                    )
                                } else {
                                    if ((isSplitRefund || isDiscountedOrder || isModifireOrder) && totalRefundItemQtyCount != selectedTotalRefundItemQtyCount) {
                                        if(isSplitRefund){
                                            CM.showMessageOK(
                                                mActivity,
                                                "",
                                                getString(R.string.restrict_split_parttialy),
                                                null
                                            )
                                        }else{
                                            CM.showMessageOK(
                                                mActivity,
                                                "",
                                                getString(R.string.restrict_parttialy),
                                                null
                                            )
                                        }
                                        return@setOnClickListener
                                    }
                                    paymentViewModel.reasonForRefund = edtReason.text.toString()
                                    var total_refund = 0.0
                                    var mTotalTax = 0.0
                                    calculateCartSelectedRefundProcedure(
                                        orderTransListSelected
                                    ) { cart: CartRefundSummaryData ->
                                        mTotalTax = cart.taxAmount + cart.saleTaxAmount
                                        total_refund =
                                            ((cart.totalAmount + cart.taxAmount) + (cart.saleTotalAmount + cart.saleTaxAmount)) - cart.totalDiscount
                                    }
                                    var strCardLastDigits = ""
                                    var strCardHolderName = ""
                                    selected_history?.let { it ->
                                        it.orderPayments?.get(0)?.let { orderPaymentsModel ->
                                            if (paymentMode != AppConstant.CASH) {
                                                strCardLastDigits =
                                                    orderPaymentsModel.strPaymentCardLast4
                                                strCardHolderName =
                                                    orderPaymentsModel.strcardHolderName
                                            }
                                        }
                                    }

                                    //Hardware Code For CDS Display -- Refund Payment Process case invoke
                                    DisplayRefundDataToCDS(
                                        AppConstant.CDS_PAYMENTPROCESS_SCREEN,
                                        orderTransRefundList,
                                        ""
                                    )

                                    DialogUtility.openRefundCheckOutDialog(
                                        mActivity,
                                        paymentMode,
                                        strCardLastDigits,
                                        strCardHolderName,
                                        total_refund,
                                        mTotalTax,
                                        paymentMethodList
                                    ) { paymentType ->

                                        paymentViewModel.selectedPaymentMode = paymentType
                                        showPaymentProgressDialog()
                                        paymentViewModel.payableAmount =
                                            String.format("%.2f", (total_refund))
                                        paymentViewModel.getPaymentStatusObserver.value =
                                            AppConstant.PaymentProcess.LOADING.value
                                        updateProgressDialogTitle(getRefundMsg(total_refund))


                                        selected_history?.orderPayments?.get(0)
                                            ?.let { orderPaymentModel ->
                                                webCallRefund(
                                                    paymentViewModel.getMerchantId(),
                                                    orderPaymentModel.strRetRef,
                                                    paymentType
                                                ) { isSuccess, strInvoiceNumber, totalQty, errorMsg ->
                                                    if (isSuccess) {
                                                        if (paymentType == AppConstant.CASH || NewSaleFragmentCommon.isOtherCashTrasaction(
                                                                paymentMethodList,
                                                                paymentType
                                                            )
                                                        ) {
                                                            var activityTemp =
                                                                (activity as ParentActivity)
                                                            activityTemp.openCashBox()
                                                        }

                                                        payment_mode = paymentType

                                                        PrintRefundReceipt(
                                                            paymentViewModel.reasonForRefund,
                                                            strInvoiceNumber
                                                        )
                                                        //Hardware Code For CDS Display -- Refund Payment Done case invoke
                                                        DisplayRefundDataToCDS(
                                                            AppConstant.CDS_PAYMENTDONE_SCREEN,
                                                            orderTransRefundList,
                                                            "This order has been refunded successfully."
                                                        )

                                                        onRefundCancelProcedure()

                                                        paymentViewModel.selectedPaymentMode =
                                                            payment_type
                                                        paymentViewModel.payableAmount = ""

                                                        dialogRefund?.let {
                                                            if (it.isShowing) {
                                                                it.dismiss()
                                                            }
                                                        }
                                                        paymentViewModel.getPaymentStatusObserver.value =
                                                            AppConstant.PaymentProcess.SUCCESS_REFUND.value

                                                    } else {
                                                        paymentViewModel.msgCancel = errorMsg
                                                        paymentViewModel.getPaymentStatusObserver.value =
                                                            AppConstant.PaymentProcess.FAILED_REFUND.value
                                                    }
                                                }
                                            }
                                    }
                                }
                            }
                        }
                    }
                }
        }

        mBinding.btnCheckout.setOnClickListener {
            trasaction_emp_id = 0
            trasaction_emp_name = ""
            trasaction_emp_role = ""
            if (!CM.isInternetAvailable(mActivity)) {
                DialogUtility.openSingleActionDialog(
                    resources.getString(R.string.msg_network_error),
                    "OK",
                    mActivity
                ) {

                }
            } else {
                if (mBinding.tvTotalValue.text.toString().split(" ")[1].toDouble()>=10000000)
                {
                    CM.showMessageOK(
                        mActivity,
                        "",
                        String.format(getString(R.string.higher_amount_msg),currency_symbol,currency_symbol),
                        null
                    )
                }
                else if (orderTransList.size > 0) {
                    var isItemWithZeroPrice = false
                    for (i in orderTransList.indices) {
                        if (orderTransList.get(i).iDiscountId != -1 && orderTransList.get(i).itemAmount <= 0.0) {
                            isItemWithZeroPrice = true
                            break
                        }
                    }
                    if (isItemWithZeroPrice) {
                        showToast(
                            mActivity,
                            getString(R.string.msg_item_value_should_not_be_zero),
                            R.drawable.icon, custom_toast_container
                        )
                        return@setOnClickListener
                    }

                    if (mBinding.tvTotalValue.text.toString().split(" ")[1].toDouble() <= 0.0) {
                        if (orderTransList.any { it.iDiscountId != -1 }) {
                            showToast(
                                mActivity,
                                getString(R.string.msg_cart_value_should_not_be_zero),
                                R.drawable.icon, custom_toast_container
                            )
                            return@setOnClickListener
                        }
                    }
                    if (checkIsRequiredModifireSelected()) {
                        if (mBinding.layoutModifier.visibility == View.VISIBLE) {
                            mBinding.layoutModifier.visibility = View.GONE
                            mBinding.gridItem.visibility = View.VISIBLE
                            modifierList.clear()
                        }

                        //  var tt = total + total_tax_amount
                        var isDifferentCashier = false
                        discountListViewModel?.let { discountListViewModel ->
                            val isSameUserOrAdmin =
                                discountListViewModel.isSameUserOrStoreAdmin()
                            val count =
                                discountListViewModel.appliedSimpleDiscountItem(order_id)

                            if (!isSameUserOrAdmin && count > 0) {

                                isDifferentCashier = true
                                Log.d("Data", "different user")
                                CM.showMessageOK(
                                    mActivity,
                                    "",
                                    resources.getString(R.string.msg_different_Cashier),
                                    DialogInterface.OnClickListener { p0, p1 ->
                                        p0?.dismiss()
                                        doResetCart()
                                    }
                                )

                            }
                        }


                        if (!isDifferentCashier) {

                            var tt =
                                mBinding.tvTotalValue.text.toString().split(" ")[1].toDouble()
                            var firstValue = round(tt.toInt())

                            strAmount.clear()
                            firstValue += 5
                            strAmount.add(firstValue)
                            strAmount.add(firstValue + 5)
                            strAmount.add(firstValue + 10)
                            //openNewCheckOutDialog()
                            val date = Calendar.getInstance().time
                            val sdf = SimpleDateFormat(CV.WS_DATE_FORMATTE)
                            itemRequestList.clear()
                            for (i in orderTransList.indices) {
                                var cartLevelDiscount = false
                                var dicountId = 0;
                                val item = orderTransList[i]

                                val turnsType =
                                    object : TypeToken<HashMap<String, List<IngredientMasterModel>>>() {}.type
                                val modifierMap = Gson().fromJson<HashMap<String, List<IngredientMasterModel>>>(
                                    item.premodifierType,
                                    turnsType
                                )
                                var gson=GsonBuilder().create()
                                Log.d("modifierdatas",gson.toJson(modifierMap))
                                var tempValueList = ArrayList<IngredientMasterModel>()
                                var modifierReqList = ArrayList<ModifierRequestModel>()
                                if (modifierMap != null) {
                                    for ((k, v) in modifierMap) {
                                        println("$k = $v")
                                        tempValueList = v as ArrayList<IngredientMasterModel>

                                        for (i in tempValueList.indices) {
                                            var temp: ModifierRequestModel?
                                            if (k.equals(CV.SUB)) {
                                                temp = ModifierRequestModel(
                                                    tempValueList.get(i).iModifierGroupId.toString(),
                                                    tempValueList.get(i).strOptionName + " Substitute With " + mDatabase.daoIngredientList()
                                                        .getdefaultIngredient(
                                                            tempValueList.get(i).iModifierGroupId.toString(),
                                                            item.itemId
                                                        ),
                                                    tempValueList.get(i).dblOptionPrice.toDouble(),
                                                    k
                                                )
                                            } else {
                                                temp = ModifierRequestModel(
                                                    tempValueList.get(i).iModifierGroupId.toString(),
                                                    tempValueList.get(i).strOptionName,
                                                    tempValueList.get(i).dblOptionPrice.toDouble(),
                                                    k
                                                )
                                            }
                                            modifierReqList.add(temp!!)
                                        }
                                    }
                                }
                                var txtRateInPercentage = 0.0
                                if (!TextUtils.isEmpty(item.taxPercentage)) {
                                    txtRateInPercentage = item.taxPercentage.toDouble()
                                }

                                var quantity = item.itemQty
                                val rate = item.itemRate
                                var discountAmount = 0.0f

                                item.discountAmount?.let {
                                    discountAmount = it
                                }

                                if (discountAmount == 0.0f) {
                                    discountListViewModel?.selectedCartDiscount?.let { selectedDiscount ->
                                        if (selectedDiscount.discountType.equals(AppConstant.COUPON_DISCOUNT)) {
                                            var isValidProduct =
                                                discountListViewModel?.checkDiscountMappingItemIsAvailable(
                                                    item.itemId.toLong(),
                                                    discountListViewModel?.selectedCartDiscount?.discountId!!.toLong()
                                                )

                                            if (isValidProduct!!) {
                                                if (item.discountAmount == 0.0f) {
                                                    val amountCopoun = (quantity * rate)
                                                    val itemWeightAge =
                                                        (amountCopoun * 100) / totalApplicableItemDiscount
                                                    discountAmount =
                                                        ((coupnDiscountAmount * itemWeightAge) / 100).toFloat()
                                                    cartLevelDiscount = true
                                                    dicountId =
                                                        discountListViewModel?.selectedCartDiscount?.discountId!!
                                                }
                                            }

                                        } else {
                                            val amountCopoun = (quantity * rate)
                                            val itemWeightAge =
                                                (amountCopoun * 100) / totalApplicableItemDiscount
                                            discountAmount =
                                                ((coupnDiscountAmount * itemWeightAge) / 100).toFloat()
                                            cartLevelDiscount = true
                                            dicountId =
                                                discountListViewModel?.selectedCartDiscount?.discountId!!
                                        }
                                    }
                                } else {
                                    cartLevelDiscount = false
                                    dicountId = item.iDiscountId
                                }

                                val amount = (quantity * rate) - discountAmount


                                var tax_amount = 0.0
                                var taxrate = 0.0
                                customerListViewModel?.isTaxIncluded?.let {
                                    if (it.value!!)
                                        taxrate = (txtRateInPercentage / 100)
                                }

                                tax_amount = amount * taxrate // (amount * (txtRateInPercentage / 100))
                                tax_amount = String.format("%.2f", (tax_amount)).toDouble()

                                val temp = OrderTransRequestModel(
                                    item.itemId.toString(),
                                    item.itemQty.toString(),
                                    item.itemRate.toString(),
                                    getFormatedAmount(item.itemAmount),
                                    item.taxPercentage,
                                    tax_amount.toString(),
                                    String.format("%.2f", (discountAmount)),
                                    dicountId,
                                    item.discountRemark,
                                    "",
                                    "",
                                    sdf.format(date),
                                    "",
                                    "",
                                    "",
                                    item.premodifierType,
                                    modifierReqList,
                                    cartLevelDiscount,
                                    item.tIsOpenDiscount,
                                    item.strInstruction,
                                    item.kitchenServedquantity
                                )
                                itemRequestList.add(temp)
                            }
                            webCall_Order_generate()
                            //CDS PaymentProcess Screen Invoke
                            DisplayNewSaleDataProcessToCDS(
                                (mBinding.rvCart.adapter as OrderItemListAdapter).getDataset(),
                                "",
                                false,currency_symbol+"0.00")
                            /*var baseActivityPrint = (activity as ParentActivity)
                            baseActivityPrint.checkDrawOverlayPermission(
                                AppConstant.CDS_PAYMENTPROCESS_SCREEN,
                                "",
                                false,
                                "",
                                CDSSalesModel(),
                                "",
                                false
                            )*/
                        }
                    } else {
                        showToast(
                            mActivity,
                            getString(R.string.kindly_close_modifire_popup),
                            R.drawable.icon, custom_toast_container
                        )
                    }
                } else {
                    showToast(
                        mActivity,
                        getString(R.string.msg_Please_select_atleast_one_item),
                        R.drawable.icon, custom_toast_container
                    )
                }
            }
        }

        mBinding.btnVoid.setOnClickListener {
            if (BuildConfig.FLAVOR == "normal") {
                mBinding.btnCheckout.performClick()
            } else {
                if (mBinding.tvTotalValue.text.toString().split(" ")[1].toDouble()
                        .equals(0.0)
                ) {
                    showToast(
                        mActivity,
                        getString(R.string.msg_Please_select_atleast_one_item_to_void),
                        R.drawable.icon, custom_toast_container
                    )
                } else {

                    /*for (i in orderTransList.indices) {
                        if (!orderTransList.get(i).isKitchenPrintServed) {
                            isKitchenPrintServed = false
                            break
                        }
                    }*/
                    for (i in orderTransList.indices) {
                        if (orderTransList.get(i).kitchenServedquantity>0)
                        {
                            showToast(
                                mActivity,
                                "You can not make this order VOID",
                                R.drawable.icon, custom_toast_container
                            )
                            return@setOnClickListener
                        }
                    }
                    openPassCodeDialog("VoidOrder", 0.0, "0.0") {
                        Log.i("Role", "Role$it")
                        when (it) {
                            1 -> {

                                doVoidCallProcedure()
                            }
                            else -> {
                                PromptMPinFaildForTaxExclude(getString(R.string.msg_void_move))
                            }
                        }
                    }

                    /*if (isKitchenPrintServed) {
                        openPassCodeDialog("VoidOrder", 0.0, "0.0") {
                            Log.i("Role", "Role$it")
                            when (it) {
                                1 -> {

                                    doVoidCallProcedure()
                                }
                                else -> {
                                    PromptMPinFaildForTaxExclude(getString(R.string.msg_void_move))
                                }
                            }
                        }
                    } else {
                        showToast(
                            mActivity,
                            "You can not make this order VOID",
                            R.drawable.icon, custom_toast_container
                        )
                    }*/

                }
            }
        }

        mBinding.btnSave.setOnClickListener {
            btnSave.isEnabled=false
            if (orderTransList.size > 0) {
                var isItemWithZeroPrice = false
                for (i in orderTransList.indices) {
                    if (orderTransList.get(i).iDiscountId != -1 && orderTransList.get(i).itemAmount <= 0.0) {
                        isItemWithZeroPrice = true
                        break
                    }
                }
                if (isItemWithZeroPrice) {
                    showToast(
                        mActivity,
                        getString(R.string.msg_item_value_should_not_be_zero),
                        R.drawable.icon, custom_toast_container
                    )
                    btnSave.isEnabled=true
                    return@setOnClickListener
                }
            }

            if (mBinding.layoutModifier.visibility == View.VISIBLE) {
                showToast(
                    mActivity,
                    getString(R.string.kindly_close_modifire_popup),
                    R.drawable.icon,
                    custom_toast_container
                )
                btnSave.isEnabled=true
            } else if (orderTransList.size == 0) {
                showToast(
                    mActivity,
                    getString(R.string.msg_Please_select_atleast_one_item_to_hold),
                    R.drawable.icon, custom_toast_container
                )
                btnSave.isEnabled=true
            } else if (customerListViewModel?.isSelectedCustomerEmpty()!!) {
                showToast(
                    mActivity,
                    getString(R.string.msg_please_select_customer_first),
                    R.drawable.icon, custom_toast_container
                )
                btnSave.isEnabled=true
            } else if (customerListViewModel?.isSelectedCustomerIsWalkInCustomer()!!) {
                showToast(
                    mActivity,
                    getString(R.string.msg_walk_in_customer_not_allowed),
                    R.drawable.icon, custom_toast_container
                )
                btnSave.isEnabled=true
            } else {
                showKcsDialog()
                if (selected_history == null) {
                    /*val totalRecord = mDatabase.daoOrerHistoryList()
                        .getNumberOfRecordsByStatus(AppConstant.ON_HOLD)
                    if (totalRecord >= AppConstant.MAX_ON_HOLD_RECORD) {
                        showToast(
                            mActivity,
                            getString(R.string.msg_max_allowed_on_hold),
                            R.drawable.icon, custom_toast_container
                        )
                        return@setOnClickListener
                    }

                    val noofRecord = mDatabase.daoOrerHistoryList().isCustomerRecordExist(
                        customerListViewModel?.selectedCustomer!!.iCustomerId,
                        AppConstant.ON_HOLD
                    )

                    if (noofRecord > 0) {
                        showToast(
                            mActivity, String.format(
                                getString(R.string.msg_on_hold_customer_exist),
                                getCustomerName()
                            ), R.drawable.icon, custom_toast_container
                        )
                        return@setOnClickListener
                    }*/
                    placeOrderViewModel!!.hold_order_customer_exist(
                        customerListViewModel?.selectedCustomer!!.iCustomerId.toString()
                    )?.observe(this, androidx.lifecycle.Observer { mHoldModel ->
                        LogUtil.d("Existornot", mHoldModel.toString())
                        if (mHoldModel.data == null) {
                            showToast(
                                mActivity,
                                mHoldModel.message.toString(),
                                R.drawable.icon,
                                custom_toast_container
                            )
                            dismissKcsDialog()
                            btnSave.isEnabled=true
                        } else {
                            if (selected_history != null) {
                                order_id = selected_history?.iCheckDetailID.toString()
                            }

                            isbringBackKitchenOrder = false
                            isOrderContainKitchenProduct = false
                            isHistoryOrderContainKitchenProduct = false

                            for (i in orderTransList.indices) {
                                if (orderTransList.get(i).strProductType.equals(CV.PRODUCTS_TYPE_KITCHEN)) {
                                    isOrderContainKitchenProduct = true

                                    break
                                }
                            }

                            if (isOrderContainKitchenProduct) {

                                for (i in orderTransList.indices) {
                                    if (orderTransList.get(i).itemQty > orderTransList.get(i).kitchenServedquantity && TextUtils.equals(
                                            orderTransList.get(i).strProductType,
                                            CV.PRODUCTS_TYPE_KITCHEN
                                        )
                                    ) {
                                        isbringBackKitchenOrder = true
                                        break
                                    }
                                }
                                if (isbringBackKitchenOrder) {
                                    ShowPopUpforKitchenPrint()
                                } else {
                                    addOrderToOnHold(true)
                                }

                            } else {
                                addOrderToOnHold(true)
                                if (mBinding.layoutModifier.visibility == View.VISIBLE) {
                                    mBinding.layoutModifier.visibility = View.GONE
                                    mBinding.gridItem.visibility = View.VISIBLE
                                    modifierList.clear()
                                }
                            }
                        }
                    })
                }else {
                    if (selected_history != null) {
                        order_id = selected_history?.iCheckDetailID.toString()
                    }


                    isbringBackKitchenOrder = false
                    isOrderContainKitchenProduct = false
                    isHistoryOrderContainKitchenProduct = false

                    for (i in orderTransList.indices) {
                        if (orderTransList.get(i).strProductType.equals(CV.PRODUCTS_TYPE_KITCHEN)) {
                            isOrderContainKitchenProduct = true

                            break
                        }
                    }

                    /*if (selected_history != null) {
                    for (i in selected_history!!.orderProducts!!.indices) {
                        if (!selected_history!!.orderProducts!!.get(i).isKitchenPrintServed) {
                            isOrderContainKitchenProduct = true
                            isbringBackKitchenOrder = true
                            isHistoryOrderContainKitchenProduct = true
                            break
                        } else {
                            isbringBackKitchenOrder = true
                        }

                        if (selected_history!!.orderProducts!!.get(i).strProductType.equals(CV.PRODUCTS_TYPE_KITCHEN)) {
                            isHistoryOrderContainKitchenProduct = true
                            break
                        } else {
                            isHistoryOrderContainKitchenProduct = false
                        }
                    }
                }*/

                    if (isOrderContainKitchenProduct) {

                        /*if (isbringBackKitchenOrder) {
                        if (isHistoryOrderContainKitchenProduct) {
                            showToast(
                                mActivity,
                                "You can't put this order on Hold Again, You have to Proceed this order",
                                R.drawable.icon, custom_toast_container
                            )
                        } else {
                            ShowPopUpforKitchenPrint()
                        }

                    } else {
                        ShowPopUpforKitchenPrint()
                    }*/
                        for (i in orderTransList.indices) {
                            if (orderTransList.get(i).itemQty > orderTransList.get(i).kitchenServedquantity && TextUtils.equals(
                                    orderTransList.get(i).strProductType, CV.PRODUCTS_TYPE_KITCHEN
                                )
                            ) {
                                isbringBackKitchenOrder = true
                                break
                            }
                        }
                        if (isbringBackKitchenOrder) {
                            ShowPopUpforKitchenPrint()
                        } else {
                            addOrderToOnHold(true)
                        }

                    } else {
                        addOrderToOnHold(true)
                        if (mBinding.layoutModifier.visibility == View.VISIBLE) {
                            mBinding.layoutModifier.visibility = View.GONE
                            mBinding.gridItem.visibility = View.VISIBLE
                            modifierList.clear()
                        }
                    }
                }


            }
        }

        mBinding.btnPrint.setOnClickListener {
            if (selected_history!!.status == AppConstant.ON_HOLD) {


            } else if (selected_history!!.status.equals(
                    AppConstant.HistoryType.VOID.toString().toUpperCase()
                )
            ) {
                PrintVoidReceipt(selected_history)

            } else if (selected_history!!.orderRefund != null && selected_history!!.orderRefund!!.size > 0) {

                RePrintRefundReceipt(selected_history)

            } else {
                var activity = (activity as ParentActivity)
                activity.openRePrintDialog { isPrintKitchenReceipt, isPrintSaleReceipt ->

                    if (isPrintKitchenReceipt) {
                        PrintKitchenReceipt(
                            selected_history!!.employeeData!!.get(0).strFirstName,
                            selected_history!!.employeeData!!.get(0).strEmployeeRoleName,
                            AppConstant.REFUND
                        )
                    }
                    if (isPrintSaleReceipt) {
                        RePrintSalesReceipt(selected_history)
                    }
                }
            }
        }

        val viewModelFactory = Injection.provideViewModelFactory(mActivity)
        searchViewModel =
            ViewModelProvider(this, viewModelFactory).get(SearchViewModel::class.java)
        searchViewModel?.getBestsellerProduct()
        setTextChangedListener()
        addLiveObserver()
        getCashierDiscountList(CM.getSp(activity, CV.EMP_ID, 0) as Int)
    }

    private fun ShowPopUpforKitchenPrint() {
        DialogUtility.openDoubleActionDialog(
            mActivity,
            "Your order contains kitchen product would you like to send it to kitchen"
        ) {
            if (it) {
                addOrderToOnHold(true)
            } else {
                Log.e("DEBUG", "Starting Kitchen Printer for Hold")
                PrintKitchenReceipt(
                    CM.getSp(mActivity, CV.EMP_NAME, "") as String,
                    CM.getSp(mActivity, CV.EMP_ROLE, "") as String,
                    AppConstant.ON_HOLD
                )
                addOrderToOnHold(false)
            }
        }
    }

    /**
     * When roduct select then item closed
     */
    private fun menuClose() {
        if (mBinding.llCategory.visibility == View.VISIBLE ||
            mBinding.llSubCategory.visibility == View.VISIBLE
        ) {
            val swipeRightToLeft =
                AnimationUtils.loadAnimation(mActivity, R.anim.swipe_right_to_left)
            mBinding.llCategory.startAnimation(swipeRightToLeft)
            mBinding.llSubCategory.startAnimation(swipeRightToLeft)

            swipeRightToLeft.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationRepeat(animation: Animation?) {

                }

                override fun onAnimationEnd(animation: Animation?) {
                    mBinding.llCategory.visibility = View.GONE
                    mBinding.llSubCategory.visibility = View.GONE
                    mBinding.iBtnToggle.rotation = 0.0F
                }

                override fun onAnimationStart(animation: Animation?) {
                }

            })
        }
    }


    private fun webCallGetLastAction() {
        webCallGetLastStatus()
    }

    private fun webCallGetLastStatus() {
        lastStatusViewModel!!.getLastActionDayStartClose()
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                    isCheckIn_status = true
                } else {
                    isCheckIn_status =
                        !loginResponseModel.data!!.data.strTnxtype.equals(AppConstant.SHIFT_START)
                }

                if (isCheckIn_status) {
                    last_action_status = AppConstant.SHIFT_CLOSE
                    NewSaleFragmentCommon.resetShiftDetails(requireActivity())
                } else {
                    NewSaleFragmentCommon.setShiftDetails(requireActivity(), loginResponseModel)
                    last_action_status = AppConstant.SHIFT_START
                }
                if (last_action_status.equals(AppConstant.SHIFT_START)) {
                    webCallGetPayOutData()
                } else {
                    webCallGetPayInData()
                }
            })
    }

    private fun webCallGetPayOutData() {
        val checkOutDataViewModel: CheckOutDataViewModel? =
            ViewModelProviders.of(mActivity).get(CheckOutDataViewModel::class.java)

        checkOutDataViewModel!!.getCheckOutData(emp_id)
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                if (loginResponseModel.data != null) {
                    val cashInDrawer = loginResponseModel.data!!.data.balanceCashInDrawer
                    CM.setSp(mActivity, CV.CASH_IN_DRAWER, cashInDrawer)
                }
            })
    }

    private fun webCallGetPayInData() {
        val checkInDataViewModel: CheckInDataViewModel? =
            ViewModelProviders.of(mActivity).get(CheckInDataViewModel::class.java)

        checkInDataViewModel!!.getCheckInData(emp_id)
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                if (loginResponseModel.data != null) {
                    CM.setSp(
                        mActivity,
                        CV.CASH_IN_DRAWER,
                        loginResponseModel.data!!.data.dblCashInDrawer
                    )
                }
            })

    }


    private fun getSalesBogoDiscountList() {
        /*  discountListViewModel!!.getSalesBogoDiscountList(CM.getSp(activity, CV.EMP_ID, 0) as Int)
              ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer { loginResponseModel ->
                  if (loginResponseModel.data == null) {
                      Toast.makeText(activity, loginResponseModel.message, Toast.LENGTH_LONG).show()
                  } else {
                      loginResponseModel.data?.run {

                          for (i in loginResponseModel.data!!.data.bogo_discount.indices) {
                              buyProductList =
                                  loginResponseModel.data!!.data.bogo_discount.get(i).discountBuyProducts
                              getProductList =
                                  loginResponseModel.data!!.data.bogo_discount.get(i).discountGetProducts
                          }
                          Toast.makeText(activity, loginResponseModel.data!!.message, Toast.LENGTH_LONG)
                              .show()
                      }

                  }
              })*/
    }

    fun checkTaxIcluded(): String {
        var tCheckTaxExempt = "1"
        if (mBinding.cbTax.isChecked) {
            tCheckTaxExempt = "1"
        } else {
            tCheckTaxExempt = "0"
        }
        return tCheckTaxExempt
    }

    private fun setModifierData(item_id: Int) {
        modifierList.clear()
        for (i in 0..9) {
            val test_obj = ModifierMasterModel(
                0,
                0, "", "", "", "",
                "", 0, 0, 0, 0, 0, 0, "", "", "", 0, 0, "", 0
            )
            modifierList.add(test_obj)
        }
        for (i in mDatabase.daoModifierGroupList().getModifierGroupList(item_id).indices) {
            modifierList.removeAt(i)
            modifierList.add(
                i,
                mDatabase.daoModifierGroupList().getModifierGroupList(item_id).get(i)
            )
        }

        mBinding.layoutModifier.rvModifierGroup.adapter =
            ModifierListAdapter(context!!, modifierList, this)
        if (modifierList != null && modifierList.size > 0) {
            selected_modifier = modifierList.get(0)
            getPreModifierData(modifierList.get(0).iModifierGroupId.toString())
            getIngredientData(modifierList.get(0).iModifierGroupId.toString())

        }
    }

    private fun getPreModifierData(modifierIdGroupId: String) {
        preModifierList.clear()

        for (i in 0..5) {
            val test_obj = PreModifierMasterModel(0, "0", "", "", "", "", "", 0, 0)
            preModifierList.add(test_obj)
        }
        for (i in mDatabase.daoPreModifierList().getPreModifierList(
            modifierIdGroupId.toInt(),
            item_id
        ).indices) {
            preModifierList.removeAt(i)
            preModifierList.add(
                i,
                mDatabase.daoPreModifierList().getPreModifierList(
                    modifierIdGroupId.toInt(),
                    item_id
                ).get(i)
            )
        }

        mBinding.layoutModifier.rvPremodifier.adapter =
            PreModifierListAdapter(context!!, preModifierList, this)
        if (preModifierList != null && preModifierList.size > 0) {
            selected_pre_modifier = preModifierList.get(0)
            preModifierList.get(0).isPreModifireSelected = true
        }

    }

    private fun getIngredientData(modifierIdGroupId: String) {
        ingredientList.clear()

        for (i in 0..17) {
            val test_obj = IngredientMasterModel(0, "0", 0, "", "", 0, false, 0)
            ingredientList.add(test_obj)
        }
        for (i in mDatabase.daoIngredientList().getIngredientList(
            modifierIdGroupId,
            item_id
        ).indices) {
            ingredientList.removeAt(i)
            ingredientList.add(
                i,
                mDatabase.daoIngredientList().getIngredientList(modifierIdGroupId, item_id)
                    .get(i)
            )
        }

        // =============================

        var modifierMap = HashMap<String, List<IngredientMasterModel>>()

        //val stringHashMap = mDatabase.daoOrderTrans().getPreModifierTypeTemp(order_id, item_id)

        val stringHashMap = mDatabase.daoOrderTrans().getPreModifierTypeTemp_(order_id, item_id,order_product_id)

        if (!TextUtils.isEmpty(stringHashMap)) {
            // method to convert string to hasMap
            val turnsType =
                object : TypeToken<HashMap<String, List<IngredientMasterModel>>>() {}.type
            modifierMap = Gson().fromJson<HashMap<String, List<IngredientMasterModel>>>(
                stringHashMap,
                turnsType
            )
        }

        for ((k, v) in modifierMap) {

            println("$k = $v")
            val tempValueList = v as ArrayList<IngredientMasterModel>
            for (item in tempValueList) {
                for (j in ingredientList.indices) {
                    if (item.iModifierGroupOptionId.equals(ingredientList.get(j).iModifierGroupOptionId)) {
                        //  ingredientList.get(j).isSelected = true
                    }
                }
            }

        }


        // ===========================
        adapter_ingredient = IngredientListAdapter(context!!, ingredientList, this)
        mBinding.layoutModifier.rvIngrediant.adapter = adapter_ingredient
        if (ingredientList != null && ingredientList.size > 0) {
            selected_ingredient = ingredientList.get(0)
        }
        ingredientHighlitedOpreation(modifierMap)
    }

    fun ingredientHighlitedOpreation(modifierMap: HashMap<String, List<IngredientMasterModel>>?) {
        if (modifierMap != null && modifierMap.isNotEmpty()) {
            var tempValueList = ArrayList<IngredientMasterModel>()
            for ((k, v) in modifierMap) {
                tempValueList.addAll(v)
            }
            for (item in ingredientList) {
                var filteredSelectedList =
                    tempValueList.filter { tempItem -> tempItem.iModifierGroupOptionId == item.iModifierGroupOptionId }
                if (tempValueList.isNotEmpty() && item.iProductId != 0 && filteredSelectedList.isNotEmpty()) {
                    item.isIngredientHighlited = true
                } else {
                    item.isIngredientHighlited = false
                }
            }
        } else {
            for (item in ingredientList) {
                item.isIngredientHighlited = false
            }
        }
        if (adapter_ingredient != null) {
            adapter_ingredient.notifyDataSetChanged()
        }
    }


    override fun listItemClickedModifier(position: Int, modifierId: String) {
        if (position >= modifierList.size) {
            return
        }
        selected_modifier = modifierList.get(position)

        getPreModifierData(modifierId)
        getIngredientData(modifierId)
    }

    private fun addOrderToOnHold(isKitchenPrintServed: Boolean) {

        //add data into order master
        this.calculateCartProcedure(orderTransList, true) { cart: CartSummaryData ->

            var isKitchenPrint = isKitchenPrintServed

            var hold_product_list = ArrayList<OrderTransRequestModel>()
            for (orderTranObj in orderTransList) {
                if (orderTranObj.strProductType.equals(CV.PRODUCTS_TYPE_RETAILS)) {
                    isKitchenPrint = true
                } else {
                    isKitchenPrint = isKitchenPrintServed
                }
                orderTranObj?.let {
                    if (TextUtils.isEmpty(it.id)) {
                        orderTranObj.id = UUID.randomUUID().toString()
                    }
                    /*var hold_product = OrderProductsModel(
                        "",
                        orderTranObj.itemName,
                        orderTranObj.itemQty.toString(),
                        orderTranObj.itemRate.toString(),
                        orderTranObj.itemAmount.toString(),
                        orderTranObj.strPricingType,
                        orderTranObj.taxPercentage,
                        orderTranObj.taxAmount,
                        orderTranObj.discountAmount.toString(),
                        orderTranObj.iDiscountId.toString(),
                        orderTranObj.itemId,
                        "",
                        orderTranObj.id,
                        ArrayList(),
                        orderTranObj.discountRemark,
                        orderTranObj.strProductType,
                        orderTranObj.premodifierType,
                        orderTranObj.ingredientName,
                        orderTranObj.strInstruction,
                        orderTranObj.kitchenServedquantity,
                        isKitchenPrint,
                        tIsOpenDiscount = orderTranObj.tIsOpenDiscount
                    )*/
                    val date = Calendar.getInstance().time
                    val sdf = SimpleDateFormat(CV.WS_DATE_FORMATTE)
                    val turnsType =
                        object : TypeToken<HashMap<String, List<IngredientMasterModel>>>() {}.type
                    LogUtil.d("modifierdata",orderTranObj.premodifierType)
                    val modifierMap = Gson().fromJson<HashMap<String, List<IngredientMasterModel>>>(
                        orderTranObj.premodifierType,
                        turnsType
                    )
                    var tempValueList = ArrayList<IngredientMasterModel>()
                    var modifierReqList = ArrayList<ModifierRequestModel>()

                    if (modifierMap != null) {
                        for ((k, v) in modifierMap) {
                            println("$k = $v")
                            tempValueList = v as ArrayList<IngredientMasterModel>

                            for (i in tempValueList.indices) {
                                var temp: ModifierRequestModel?
                                if (k.equals(CV.SUB)) {
                                    temp = ModifierRequestModel(
                                        tempValueList.get(i).iModifierGroupId.toString(),
                                        tempValueList.get(i).strOptionName + " Substitute With " + mDatabase.daoIngredientList()
                                            .getdefaultIngredient(
                                                tempValueList.get(i).iModifierGroupId.toString(),
                                                orderTranObj.itemId
                                            ),
                                        tempValueList.get(i).dblOptionPrice.toDouble(),
                                        k
                                    )
                                } else {
                                    temp = ModifierRequestModel(
                                        tempValueList.get(i).iModifierGroupId.toString(),
                                        tempValueList.get(i).strOptionName,
                                        tempValueList.get(i).dblOptionPrice.toDouble(),
                                        k
                                    )
                                }
                                modifierReqList.add(temp!!)
                            }
                        }
                    }
                    var cartLevelDiscount = false
                    var dicountId = 0
                    var txtRateInPercentage = 0.0
                    if (!TextUtils.isEmpty(orderTranObj.taxPercentage)) {
                        txtRateInPercentage = orderTranObj.taxPercentage.toDouble()
                    }

                    var quantity = orderTranObj.itemQty
                    val rate = orderTranObj.itemRate
                    var discountAmount = 0.0f

                    orderTranObj.discountAmount?.let {
                        discountAmount = it
                    }

                    if (discountAmount == 0.0f) {
                        discountListViewModel?.selectedCartDiscount?.let { selectedDiscount ->
                            if (selectedDiscount.discountType.equals(AppConstant.COUPON_DISCOUNT)) {
                                var isValidProduct =
                                    discountListViewModel?.checkDiscountMappingItemIsAvailable(
                                        orderTranObj.itemId.toLong(),
                                        discountListViewModel?.selectedCartDiscount?.discountId!!.toLong()
                                    )

                                if (isValidProduct!!) {
                                    if (orderTranObj.discountAmount == 0.0f) {
                                        val amountCopoun = (quantity * rate)
                                        val itemWeightAge =
                                            (amountCopoun * 100) / totalApplicableItemDiscount
                                        discountAmount =
                                            ((coupnDiscountAmount * itemWeightAge) / 100).toFloat()
                                        cartLevelDiscount = true
                                        dicountId =
                                            discountListViewModel?.selectedCartDiscount?.discountId!!
                                    }
                                }

                            } else {
                                val amountCopoun = (quantity * rate)
                                val itemWeightAge =
                                    (amountCopoun * 100) / totalApplicableItemDiscount
                                discountAmount =
                                    ((coupnDiscountAmount * itemWeightAge) / 100).toFloat()
                                cartLevelDiscount = true
                                dicountId =
                                    discountListViewModel?.selectedCartDiscount?.discountId!!
                            }
                        }
                    } else {
                        cartLevelDiscount = false
                        dicountId = orderTranObj.iDiscountId
                    }

                    val amount = (quantity * rate) - discountAmount


                    var tax_amount = 0.0
                    var taxrate = 0.0

                    customerListViewModel?.isTaxIncluded?.let {
                        if (it.value!!)
                            taxrate = (txtRateInPercentage / 100)
                    }

                    tax_amount = amount * taxrate // (amount * (txtRateInPercentage / 100))
                    tax_amount = String.format("%.2f", (tax_amount)).toDouble()

                    val temp = OrderTransRequestModel(
                        orderTranObj.itemId.toString(),
                        orderTranObj.itemQty.toString(),
                        orderTranObj.itemRate.toString(),
                        getFormatedAmount(orderTranObj.itemAmount),
                        orderTranObj.taxPercentage,
                        tax_amount.toString(),
                        orderTranObj.discountAmount.toString(),
                        orderTranObj.iDiscountId,
                        orderTranObj.discountRemark,
                        "",
                        "",
                        "",
                        "",
                        sdf.format(date),
                        "",
                        orderTranObj.premodifierType,
                        modifierReqList,
                        false,
                        orderTranObj.tIsOpenDiscount,
                        orderTranObj.strInstruction,
                        orderTranObj.kitchenServedquantity
                    )
                    hold_product_list.add(temp)
                }

            }

            var holdOrderDiscountList = ArrayList<OrderDiscountsModel>()

            discountListViewModel?.getAppliedDiscountOnCart(order_id).let {

                if (!it.isNullOrEmpty()) {
                    val holdOrderDiscount = OrderDiscountsModel(
                        convertDateFormate(
                            CV.DISPLAY_DATE_FORMATTE_AMPM,
                            System.currentTimeMillis()
                        ),
                        "",
                        it.get(0).strDiscountName,
                        "",
                        it.get(0).discount_amount,
                        it.get(0).iDiscountId,
                        it.get(0).strDiscountName,
                        it.get(0).type,
                        it.get(0).strType,
                        it.get(0).dblValue,
                        it.get(0).strAppliedTo,
                        it.get(0).minCartAmount
                    )

                    holdOrderDiscountList.add(holdOrderDiscount)
                }

            }


            orderHistoryList.clear()
            val timezone =
                TimeZone.getTimeZone(CM.getSp(activity, CV.STRTIMEZONE, "") as String)
            val holdorderFormat = SimpleDateFormat(CV.DISPLAY_DATE_FORMATTE_AMPM)
            holdorderFormat.timeZone = timezone
            LogUtil.e("Holdorderdate", holdorderFormat.format(Date()))


            /*val holdOrder = OrderHistoryModel(
                order_id,
                AppConstant.ON_HOLD,
                0,
                convertDatetospecifictimezone(activity!!),
                customerListViewModel?.selectedCustomer!!.iCustomerId.toString(),
                customerListViewModel?.selectedCustomer!!.strFirstName,
                customerListViewModel?.selectedCustomer!!.strLastName,
                getFormatedAmount(cart.totalAmount - cart.taxAmount),
                getFormatedAmountWithCurrencySymbol(
                    cart.totalAmount.toString().toDouble()
                ),
                getFormatedAmount(cart.taxAmount),
                "",
                hold_product_list,
                holdOrderDiscountList,
                null,
                null,
                checkTaxIcluded(),
                isKitchenPrintServed
            )
            orderHistoryList.add(holdOrder)*/


            /*var dao: OrderHistoryListDAO? = null
            dao = mDatabase?.daoOrerHistoryList()
            dao!!.insertOrderHistoryData(orderHistoryList)*/
            val strPaymentType = if (paymentRequestList.size > 1) {
                AppConstant.SPLIT
            } else {
                AppConstant.NORMAL
            }
            placeOrderViewModel!!.placeOrder(
                CV.server_order_id.toString(),
                order_id,
                customerListViewModel?.selectedCustomer!!.iCustomerId,
                if (trasaction_emp_id == 0) emp_id else trasaction_emp_id,
                mBinding.tvTotalValue.text.toString().split(" ")[1].toDouble(),
                mBinding.tvTax.text.toString().split(" ")[1].toDouble(),
                mBinding.tvDiscountValue.text.toString().split(" ")[1].toDouble(),
                mBinding.tvTotal.text.toString().split(" ")[1].toDouble(),
                Gson().toJson(hold_product_list),
                Gson().toJson(paymentRequestList),
                Gson().toJson(holdOrderDiscountList),
                "",
                "0",
                checkTaxIcluded(),
                strPaymentType,
                AppConstant.HistoryStatus.HOLDSTATUS.value
            )?.observe(this, androidx.lifecycle.Observer { NewSaleandRefundModel ->
                dismissKcsDialog()
                btnSave.isEnabled=true

                if (NewSaleandRefundModel.data == null) {
                    if (NewSaleandRefundModel.message.equals(CV.HOLD_ORDER_GENERATED_MSG))
                    {
                        HoldOrderErrorDialog(NewSaleandRefundModel.message!!)
                        return@Observer
                    }
                    showToast(
                        mActivity,
                        NewSaleandRefundModel.message.toString(),
                        R.drawable.icon, custom_toast_container
                    )
                } else {
                    doResetCart()
                    //Hardware Code For CDS Display -- Newsale put Order on Hold case invoke
                    DisplayNewSaleDataToCDS(ArrayList(), "This order has been placed on hold.", false)

                    total = 0.0
                    total_tax_amount = 0.0
                    CM.setSp(mActivity, CV.ORDER_ID, "")
                    displayCalculation(00.00, 00.00, 0, 0.0f)
                    setEvent(AppConstant.ORDER)
                    customerListViewModel?.selectedCustomer = customerListViewModel?.walkInCustomer
                    setCustomerName()
                    adapter(orderTransList, eventName)
                    order_id = ""
                    temp_order_id = ""
                    selectedAge = null
                    mBinding.cbTax.isChecked = true
                    CM.setSp(mActivity, CV.ORDER_ID, order_id)
                    selected_history = null
                    discountListViewModel?.selectedCartDiscount = null
                    clearDiscountData()
                    mBinding.ivAddDiscount.visibility = View.VISIBLE

                    mBinding.ivCancel.isEnabled = true
                    mBinding.gridItem.visibility = View.VISIBLE
                    mBinding.layoutModifier.visibility = View.GONE

                    mBinding.ivAddCustomer.isEnabled = true
                    mBinding.ivAdd.visibility = View.VISIBLE
                }
            })

        }
    }

    private fun addLiveObserver() {
        customerListViewModel?.run {
            this.getCustomerObserver()
                ?.observe(this@NewSalesFragment, androidx.lifecycle.Observer { list ->
                    customerList?.let {

                        customerList = list as ArrayList<CustomerModel>
                        customerListFilter = list

                        for (i in customerList.indices) {
                            if (customerList[i].iIsWalkIn.equals(1)) {
                                customerListViewModel?.walkInCustomer = customerList.get(i)
                                setCustomerName()
                                break
                            }
                        }
                    }

                    if (customerListViewModel?.selectedCustomer == null) {
                        customerListViewModel?.selectedCustomer =
                            customerListViewModel?.walkInCustomer
                    }
                })
        }
        searchViewModel?.bestSelllerProducts?.observe(
            this,
            androidx.lifecycle.Observer { productList ->
                if (productList != null) {

                    itemList = productList as ArrayList<ItemModel>
                    itemListFilter = itemList
                    Log.e("====", "=== itemList =====" + itemList.size)
                    mBinding.gridItem.visibility = View.VISIBLE
                    mBinding.tvNoProductFound.visibility = View.GONE
                    mBinding.gridItem.adapter = ItemListAdapter(mActivity, itemList)

                }
            })
        setPaymentStatusObserver()
    }

    private fun getCashierDiscountList(iEmployeeId: Int) {
        discountListViewModel?.getDiscountList(iEmployeeId)
    }

    private fun getCustomerName() = if (customerListViewModel?.selectedCustomer == null) {
        customerListViewModel?.walkInCustomer?.strFirstName + " " + customerListViewModel?.walkInCustomer?.strLastName
    } else {
        customerListViewModel?.selectedCustomer?.strFirstName + " " + customerListViewModel?.selectedCustomer?.strLastName
    }

    private fun setCustomerName() {
        mBinding.tvNewOrder.text = getCustomerName()
    }

    private fun setTextChangedListener() {
        val watcher = object : TextWatcher {
            private var searchFor = ""

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                /* val searchText = s.toString().trim()
                *//* if (searchText == searchFor)
                    return*//*

                searchFor = searchText

                launch {
                    delay(100)  //debounce timeOut
                   *//* if (searchText != searchFor)
                        return@launch*//*
                    if (!TextUtils.isEmpty(searchText)) {
                        if (isModifirePopupClose()) {
                            menuClose()
                            Log.d("BARCODE_SCAN","barcode :: "+searchText)
                            var product = searchViewModel?.searchProductBarcode(searchText)
                            if (product != null) {
                                Log.d("BARCODE_SCAN","Product :: "+product.strBarCode)
                                isCustomerEligible(product)
                                edtSearchBarcode.setText("")
                                edtSearchBarcode.requestFocus()
                                edtSearchBarcode.setRawInputType(InputType.TYPE_CLASS_TEXT)
                                edtSearchBarcode.isFocusable = true
                                searchViewModel?.resetProduct()
                                searchViewModel?.isSearching = false
                            } else {
                                searchViewModel?.isSearching = false
                                productNotFoundDialog {
                                    edtSearchBarcode.setText("")
                                    edtSearchBarcode.requestFocus()
                                    edtSearchBarcode.setRawInputType(InputType.TYPE_CLASS_TEXT)
                                    edtSearchBarcode.isFocusable = true
                                    searchViewModel?.resetProduct()
                                }

                            }
                        } else {
                            mBinding.edtSearchBarcode.text!!.clear()
                        }
                    }
                    // do our magic here
                }*/
            }

            override fun afterTextChanged(s: Editable?) = Unit
            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) =
                Unit
        }
        edtSearchBarcode.addTextChangedListener(watcher)

        searchViewModel?.product?.observe(this, androidx.lifecycle.Observer { product ->
            if (product != null) {
                Log.d("BARCODE_SCAN", "PRODUCT ::" + product.strBarCode)
                isCustomerEligible(product)
                edtSearchBarcode.setText("")
                edtSearchBarcode.requestFocus()
                edtSearchBarcode.setRawInputType(InputType.TYPE_CLASS_TEXT)
                edtSearchBarcode.isFocusable = true
                searchViewModel?.resetProduct()
                searchViewModel?.isSearching = false
            } else {
                searchViewModel?.isSearching = false
                productNotFoundDialog {
                    edtSearchBarcode.setText("")
                    edtSearchBarcode.requestFocus()
                    edtSearchBarcode.setRawInputType(InputType.TYPE_CLASS_TEXT)
                    edtSearchBarcode.isFocusable = true
                    searchViewModel?.resetProduct()
                }

            }
        })

        mBinding.cbDiscountApplied.setOnCheckedChangeListener { buttonView, isChecked ->

            if (!isChecked) {
                clearDiscountData()
                //Hardware Code For CDS Display -- NewSale While remove discount
                DisplayNewSaleDataToCDS(cartAdapter?.getDataset()!!, "", false)
            }
        }

        mBinding.cbTax.setOnClickListener {
            if (orderTransList.size > 0) {
                if (!CM.isInternetAvailable(mActivity)) {
                    CM.showMessageOK(
                        mActivity,
                        "",
                        resources.getString(R.string.msg_network_error),
                        null
                    )

                    mBinding.cbTax.isChecked = true
                } else if (mBinding.cbTax.isChecked) {
                    setTaxIncludedStatus(mBinding.cbTax.isChecked)
                    showToast(
                        mActivity,
                        getString(R.string.msg_tax_added),
                        R.drawable.icon, custom_toast_container
                    )
                    this.calculateCartProcedure(orderTransList, true) {}

                    //Hardware Code For CDS Display -- NewSale While Tax Checkbox state change case invoke
                    DisplayNewSaleDataToCDS(orderTransList, "", false)
                } else {
                    openPassCodeDialog("TaxCheckBox", 0.0, "0.0") {
                        Log.i("Role", "Role$it")
                        when (it) {
                            1 -> {
                                setTaxIncludedStatus(mBinding.cbTax.isChecked)
                                showToast(
                                    mActivity,
                                    getString(R.string.msg_tax_removed),
                                    R.drawable.icon, custom_toast_container
                                )

                                this.calculateCartProcedure(orderTransList, true) {}

                                //Hardware Code For CDS Display -- NewSale While Tax Checkbox state change case invoke
                                DisplayNewSaleDataToCDS(orderTransList, "", false)
                            }
                            else -> {
                                if (!mBinding.cbTax.isChecked) {
                                    PromptMPinFaildForTaxExclude()
                                }
                                setTaxIncludedStatus(true)
                                this.calculateCartProcedure(orderTransList, true) {}
                            }
                        }
                    }
                }
            } else {
                showToast(
                    mActivity,
                    getString(R.string.msg_tax_can_not_apply_on_empty_cart),
                    R.drawable.icon, custom_toast_container
                )
                mBinding.cbTax.isChecked = true
            }
        }

        customerListViewModel?.isTaxIncluded?.observe(this, androidx.lifecycle.Observer {
            it?.let {
                mBinding.cbTax.isChecked = it
            }
        })

    }

    private fun PromptMPinFaildForTaxExclude(msg: String = resources.getString(R.string.msg_tax_exclude)) {
        CM.showMessageOK(
            mActivity,
            "",
            msg,
            null
        )

    }

    override fun onResume() {
        super.onResume()
        AppConstant.EMP_ID = emp_id
        if (Build.VERSION.SDK_INT >= 11) {
            edtSearchBarcode.setRawInputType(InputType.TYPE_CLASS_TEXT)
            edtSearchBarcode.setTextIsSelectable(true)
        } else {
            edtSearchBarcode.setRawInputType(InputType.TYPE_NULL)
            edtSearchBarcode.isFocusable = true
        }
        CM.hideSoftKeyboard(mActivity)
    }

    override fun onDestroy() {
        super.onDestroy()
        (activity as ParentActivity).setFragmentKeyeventListener(null)
        if (selected_history == null) {
            mDatabase.daoOrderTrans().deleteOrder(order_id)
            mDatabase.daoProductValidation().deleteProductValidation(order_id)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            val customer = data!!.getStringExtra("customer")
            customerListViewModel?.selectedCustomer =
                Gson().fromJson(customer, CustomerModel::class.java)
            setCustomerName()
            val isUpdated = data!!.getBooleanExtra("isUpdated", false)
            //   if (isUpdated) {
            customerListViewModel?.getCustomer("")
            //    }
        }
    }
//    private fun openModifierLayout(itemId: Int, itemName: String) {
//        mBinding.layoutModifier.visibility = View.VISIBLE
//        val itamModify = getString(R.string.modifying) + " " + itemName
//        mBinding.layoutModifier.tvTitle.text = itamModify
//        mBinding.gridItem.visibility = View.GONE
//        setModifierData(itemId)
//    }

    /*private fun setModifierData(item_id: Int) {
        modifierList.clear()
        for (i in 0..9) {
            val test_obj = ModifierMasterModel(
                0,
                0, "", "", "", "",
                "", 0, 0, 0, 0, 0, 0, "", "", "", 0, 0, "", 0
            )
            modifierList.add(test_obj)
        }
        for (i in mDatabase.daoModifierGroupList().getModifierGroupList(item_id).indices) {
            modifierList.removeAt(i)
            modifierList.add(
                i,
                mDatabase.daoModifierGroupList().getModifierGroupList(item_id).get(i)
            )
        }

        mBinding.layoutModifier.rvModifierGroup.adapter =
            ModifierListAdapter(context!!, modifierList, this)
        if (modifierList != null && modifierList.size > 0) {
            selected_modifier = modifierList.get(0)
            getPreModifierData(modifierList.get(0).iModifierGroupId.toString())
            getIngredientData(modifierList.get(0).iModifierGroupId.toString())

        }
    }*/

    /*private fun getPreModifierData(modifierIdGroupId: String) {
        preModifierList.clear()

        for (i in 0..5) {
            val test_obj = PreModifierMasterModel(0, "0", "", "", "", "", "", 0, 0)
            preModifierList.add(test_obj)
        }
        for (i in mDatabase.daoPreModifierList().getPreModifierList(
            modifierIdGroupId.toInt(),
            item_id
        ).indices) {
            preModifierList.removeAt(i)
            preModifierList.add(
                i,
                mDatabase.daoPreModifierList().getPreModifierList(
                    modifierIdGroupId.toInt(),
                    item_id
                ).get(i)
            )
        }

        mBinding.layoutModifier.rvPremodifier.adapter =
            PreModifierListAdapter(context!!, preModifierList, this)
        if (preModifierList != null && preModifierList.size > 0) {
            selected_pre_modifier = preModifierList.get(0)
        }

    }

    private fun getIngredientData(modifierIdGroupId: String) {
        ingredientList.clear()

        for (i in 0..17) {
            val test_obj = IngredientMasterModel(0, "0", 0, "", "", 0, false, 0)
            ingredientList.add(test_obj)
        }
        for (i in mDatabase.daoIngredientList().getIngredientList(
            modifierIdGroupId,
            item_id
        ).indices) {
            ingredientList.removeAt(i)
            ingredientList.add(
                i,
                mDatabase.daoIngredientList().getIngredientList(modifierIdGroupId, item_id).get(i)
            )
        }

        // =============================

        var modifierMap = HashMap<String, List<IngredientMasterModel>>()

        val stringHashMap = mDatabase.daoOrderTrans().getPreModifierTypeTemp(order_id, item_id)

        if (!TextUtils.isEmpty(stringHashMap)) {
            // method to convert string to hasMap
            val turnsType =
                object : TypeToken<HashMap<String, List<IngredientMasterModel>>>() {}.type
            modifierMap = Gson().fromJson<HashMap<String, List<IngredientMasterModel>>>(
                stringHashMap,
                turnsType
            )
        }

        for ((k, v) in modifierMap) {

            println("$k = $v")
            val tempValueList = v as ArrayList<IngredientMasterModel>
            for (item in tempValueList) {
                for (j in ingredientList.indices) {
                    if (item.iModifierGroupOptionId.equals(ingredientList.get(j).iModifierGroupOptionId)) {
                        //  ingredientList.get(j).isSelected = true
                    }
                }
            }

        }


        // ===========================
        adapter_ingredient = IngredientListAdapter(context!!, ingredientList, this)
        mBinding.layoutModifier.rvIngrediant.adapter = adapter_ingredient
        if (ingredientList != null && ingredientList.size > 0) {
            selected_ingredient = ingredientList.get(0)
        }
    }*/

    /*override fun listItemClickedModifier(position: Int, modifierId: String) {
        if (position >= modifierList.size) {
            return
        }
        selected_modifier = modifierList.get(position)

        getPreModifierData(modifierId)
        getIngredientData(modifierId)
    }*/

    override fun listItemClickedPreModifier(position: Int, pre_modifierId: String) {
        if (preModifierList.get(position).iPreModifierGroupId.isNotEmpty()) {
            selected_pre_modifier = preModifierList.get(position)
        }
    }

    override fun listItemClickedIngredient(position: Int, ingredientId: String) {

        selected_ingredient = ingredientList.get(position)


        if (selected_ingredient!!.strOptionName.equals("")) {
            return
        }/*else {
            if (selected_ingredient!!.tIsDefault == 1 && selected_ingredient!!.isIngredientHighlited==true ) {
                showToast(
                        mActivity,
                        getString(R.string.default_remove_msg),
                        R.drawable.icon,
                        custom_toast_container
                )
                return
            }
        }*/
        var modifierMap = HashMap<String, List<IngredientMasterModel>>()

        //val stringHashMap = mDatabase.daoOrderTrans().getPreModifierTypeTemp(order_id, item_id)
        val stringHashMap = mDatabase.daoOrderTrans().getPreModifierTypeTemp_(order_id, item_id,order_product_id)

        if (!TextUtils.isEmpty(stringHashMap)) {
            // method to convert string to hasMap
            val turnsType =
                object : TypeToken<HashMap<String, List<IngredientMasterModel>>>() {}.type
            modifierMap = Gson().fromJson<HashMap<String, List<IngredientMasterModel>>>(
                stringHashMap,
                turnsType
            )
        }
        var ingredientListTemp = ArrayList<IngredientMasterModel>()
        var tt = ArrayList<IngredientMasterModel>()
        val updatedItem = orderTransList.reversed()[pos]
        if (modifierMap.containsKey(CV.DEFAULT))
        {
            var ingredientdefaultlist= ArrayList<IngredientMasterModel>()
            ingredientdefaultlist=modifierMap.get(CV.DEFAULT) as ArrayList<IngredientMasterModel>
            if (ingredientdefaultlist.indexOf(selected_ingredient!!)!=-1)
            {
                showToast(
                        mActivity,
                        getString(R.string.default_remove_msg),
                        R.drawable.icon,
                        custom_toast_container
                )
                return
            }
        }

        selected_pre_modifier?.let {

            val sectedPreModiferNameTemp = it.strPreModifierName

            if (modifierMap.containsKey(it.strPreModifierName)) {
                ingredientListTemp =
                    modifierMap.get(it.strPreModifierName) as ArrayList<IngredientMasterModel>


                selected_ingredient?.let {
                    if (ingredientListTemp.indexOf(it) == -1) {
                        //  ingredientList.add(it)
                        var tempValueList = ArrayList<IngredientMasterModel>()
                        for ((k, v) in modifierMap) {
                            tempValueList.addAll(v)
                        }
                        if (tempValueList.contains(selected_ingredient!!)) {
                            showToast(
                                mActivity,
                                getString(R.string.not_allowed),
                                R.drawable.icon,
                                custom_toast_container
                            )
                            return
                        }

                        if (ingredientListTemp.isNotEmpty()) {
                            for (i in ingredientListTemp.indices) {
                                if (selected_modifier!!.iModifierGroupId.equals(
                                        ingredientListTemp.get(
                                            i
                                        ).iModifierGroupId
                                    )
                                ) {
                                    tt.add(ingredientListTemp.get(i))
                                }
                            }
                        }
                        LogUtil.e("Selected_modifier", tt.toString())

                        var tempValueAllList = ArrayList<IngredientMasterModel>()
                        for ((k, v) in modifierMap) {
                            if (k != CV.NO && k != CV.SUB && k!=CV.DEFAULT)
                                tempValueAllList.addAll(v)
                        }
                        var tempFiltredGroupList =
                            tempValueAllList.filter { item -> item.iModifierGroupId == selected_modifier!!.iModifierGroupId }
                        if (sectedPreModiferNameTemp.equals(
                                CV.SUB
                            )
                        ) {
                            if (mDatabase.daoIngredientList()
                                    .getdefaultIngredient(selected_modifier!!.iModifierGroupId.toString(),orderTransList.reversed()[pos].itemId).isNullOrEmpty()
                            ) {
                                ErrorDialog(getString(R.string.no_default_msg))
                                return
                            }

                            LogUtil.e("SUBSTITUDE", sectedPreModiferNameTemp)
                            if (tt.size > 0) {
                                showToast(
                                    mActivity,
                                    getString(R.string.second_substitute),
                                    R.drawable.icon,
                                    custom_toast_container
                                )
                                return
                            }
                            val tempFilteredList =
                                modifierMap[CV.DEFAULT]!!.filter { item ->
                                    item.strOptionName != mDatabase.daoIngredientList()
                                        .getdefaultIngredient(
                                            selected_modifier!!.iModifierGroupId.toString(),
                                            item_id
                                        )
                                }
                            modifierMap.remove(CV.DEFAULT)
                            if (tempFilteredList.isNotEmpty() && tempFilteredList.size > 0) {
                                modifierMap.set(CV.DEFAULT, tempFilteredList)
                            }
                            ingredientListTemp.add(it)
                        } else if (tempFiltredGroupList.size < selected_modifier!!.iOptionMax || sectedPreModiferNameTemp == CV.NO) {
                            ingredientListTemp.add(it)
                        } else {
                            /*val stringHashMap =
                                mDatabase.daoOrderTrans()
                                    .getPreModifierTypeTemp(order_id, item_id)*/
                            val stringHashMap =
                                mDatabase.daoOrderTrans()
                                    .getPreModifierTypeTemp_(order_id, item_id,order_product_id)

                            if (!TextUtils.isEmpty(stringHashMap)) {
                                // method to convert string to hasMap
                                val turnsType =
                                    object :
                                        TypeToken<HashMap<String, List<IngredientMasterModel>>>() {}.type
                                modifierMap =
                                    Gson().fromJson<HashMap<String, List<IngredientMasterModel>>>(
                                        stringHashMap,
                                        turnsType
                                    )
                            }
                            ingredientListTemp.clear()
                            showToast(
                                mActivity,
                                getString(R.string.maximum_limit_is_reached),
                                R.drawable.icon, custom_toast_container
                            )
                            ingredientList.get(position).isSelected = false
                            ingredientHighlitedOpreation(modifierMap)
                            return
                        }
                    } else {
                        if (sectedPreModiferNameTemp.equals(CV.SUB)) {
                            //LogUtil.e("Default_dfd1",modifierMap[CV.DEFAULT]!!.size.toString())
                            var tempValueList = ArrayList<IngredientMasterModel>()
                            if (modifierMap.containsKey(CV.DEFAULT)) {

                                for (i in modifierMap[CV.DEFAULT]!!.indices) {
                                    tempValueList.add(modifierMap[CV.DEFAULT]!!.get(i))
                                }
                            }
                            LogUtil.e(
                                "ingredientmodel",
                                mDatabase.daoIngredientList().getdefaultIngredientmodel(
                                    selected_modifier!!.iModifierGroupId.toString(),
                                    item_id
                                ).toString()
                            )
                            tempValueList.add(
                                mDatabase.daoIngredientList().getdefaultIngredientmodel(
                                    selected_modifier!!.iModifierGroupId.toString(),
                                    item_id
                                )
                            )
                            modifierMap.set(CV.DEFAULT, tempValueList)
                            val gson = GsonBuilder().create()
                            val ingredientDataInStringFormate = gson.toJson(modifierMap)
                            /*mDatabase.daoOrderTrans()
                                .updateModifier(
                                    ingredientDataInStringFormate,
                                    GetModifierIngredients(modifierMap),
                                    order_id,
                                    item_id
                                )*/
                            mDatabase.daoOrderTrans()
                                .updateModifier_(
                                    ingredientDataInStringFormate,
                                    GetModifierIngredients(modifierMap),
                                    order_id,
                                    item_id,
                                    order_product_id
                                )
                        }
                        deleteModifier(position)
                        return
                    }
                }

            } else {
                selected_ingredient?.let {
                    if (modifierMap.size > 0) {
                        var tempValueList = ArrayList<IngredientMasterModel>()
                        for ((k, v) in modifierMap) {
                            tempValueList.addAll(v)
                        }
                        if (tempValueList.contains(selected_ingredient!!)) {
                            showToast(
                                mActivity,
                                getString(R.string.not_allowed),
                                R.drawable.icon,
                                custom_toast_container
                            )
                            return
                        }
                    }

                    if (ingredientListTemp.indexOf(it) == -1) {
                        if (ingredientListTemp != null && ingredientListTemp.size > 0) {
                            for (i in ingredientListTemp.indices) {
                                if (selected_modifier!!.iModifierGroupId.equals(
                                        ingredientListTemp.get(
                                            i
                                        ).iModifierGroupId
                                    )
                                ) {
                                    tt.add(ingredientListTemp.get(i))
                                }
                            }
                        }
                        var tempValueAllList = ArrayList<IngredientMasterModel>()
                        for ((k, v) in modifierMap) {
                            if (k != CV.NO && k != CV.SUB && k!=CV.DEFAULT)
                                tempValueAllList.addAll(v)
                        }
                        var tempFiltredGroupList =
                            tempValueAllList.filter { item -> item.iModifierGroupId == selected_modifier!!.iModifierGroupId }
                        if (sectedPreModiferNameTemp.equals(
                                CV.SUB
                            )
                        ) {
                            if (mDatabase.daoIngredientList()
                                    .getdefaultIngredient(selected_modifier!!.iModifierGroupId.toString(),orderTransList.reversed()[pos].itemId).isNullOrEmpty()
                            ) {
                                ErrorDialog(getString(R.string.no_default_msg))
                                return
                            }
                            if (tt.size > 0) {
                                showToast(
                                    mActivity,
                                    getString(R.string.second_substitute),
                                    R.drawable.icon,
                                    custom_toast_container
                                )
                                return
                            }
                            LogUtil.e("SUBSTITUDE", sectedPreModiferNameTemp)

                            val tempFilteredList =
                                modifierMap[CV.DEFAULT]!!.filter { item ->
                                    item.strOptionName != mDatabase.daoIngredientList()
                                        .getdefaultIngredient(
                                            selected_modifier!!.iModifierGroupId.toString(),
                                            item_id
                                        )
                                }
                            modifierMap.remove(CV.DEFAULT)
                            if (tempFilteredList.isNotEmpty() && tempFilteredList.size > 0) {
                                modifierMap.set(CV.DEFAULT, tempFilteredList)
                            }
                            ingredientListTemp.add(it)
                        } else if (tempFiltredGroupList.size < selected_modifier!!.iOptionMax || sectedPreModiferNameTemp == CV.NO) {
                            ingredientListTemp.add(it)
                        } else {
                            ingredientListTemp.clear()
                            //Below code used for get latest modifire map data
                            /*val stringHashMap =
                                mDatabase.daoOrderTrans()
                                    .getPreModifierTypeTemp(order_id, item_id)*/
                            val stringHashMap =
                                mDatabase.daoOrderTrans()
                                    .getPreModifierTypeTemp_(order_id, item_id,order_product_id)
                            if (!TextUtils.isEmpty(stringHashMap)) {
                                // method to convert string to hasMap
                                val turnsType =
                                    object :
                                        TypeToken<HashMap<String, List<IngredientMasterModel>>>() {}.type
                                modifierMap =
                                    Gson().fromJson<HashMap<String, List<IngredientMasterModel>>>(
                                        stringHashMap,
                                        turnsType
                                    )
                            }
                            showToast(
                                mActivity,
                                getString(R.string.maximum_limit_is_reached),
                                R.drawable.icon, custom_toast_container
                            )
                            ingredientList.get(position).isSelected = false
                            ingredientHighlitedOpreation(modifierMap)
                            return
                        }
                    } else {
                        if (sectedPreModiferNameTemp.equals(CV.SUB)) {
                            //LogUtil.e("Default_dfd",modifierMap[CV.DEFAULT]!!.size.toString())
                            var tempValueList = ArrayList<IngredientMasterModel>()
                            if (modifierMap.containsKey(CV.DEFAULT)) {

                                for (i in modifierMap[CV.DEFAULT]!!.indices) {
                                    tempValueList.add(modifierMap[CV.DEFAULT]!!.get(i))
                                }
                            }
                            tempValueList.add(
                                mDatabase.daoIngredientList().getdefaultIngredientmodel(
                                    selected_modifier!!.iModifierGroupId.toString(),
                                    item_id
                                )
                            )
                            modifierMap.set(CV.DEFAULT, tempValueList)
                            val gson = GsonBuilder().create()
                            val ingredientDataInStringFormate = gson.toJson(modifierMap)
                            /*mDatabase.daoOrderTrans()
                                .updateModifier(
                                    ingredientDataInStringFormate,
                                    GetModifierIngredients(modifierMap),
                                    order_id,
                                    item_id
                                )*/
                            mDatabase.daoOrderTrans()
                                .updateModifier_(
                                    ingredientDataInStringFormate,
                                    GetModifierIngredients(modifierMap),
                                    order_id,
                                    item_id,
                                    order_product_id
                                )

                        }
                        deleteModifier(position)
                        return
                    }
                }
            }
            modifierMap.set(it.strPreModifierName, ingredientListTemp)
            ingredientHighlitedOpreation(modifierMap)

            val gson = GsonBuilder().create()
            val ingredientDataInStringFormate = gson.toJson(modifierMap)

            updatedItem.premodifierType = ingredientDataInStringFormate
            updatedItem.ingredientName = GetModifierIngredients(modifierMap)

            /*mDatabase.daoOrderTrans()
                .updateModifier(
                    ingredientDataInStringFormate,
                    GetModifierIngredients(modifierMap),
                    order_id,
                    item_id
                )*/
            mDatabase.daoOrderTrans()
                .updateModifier_(
                    ingredientDataInStringFormate,
                    GetModifierIngredients(modifierMap),
                    order_id,
                    item_id,
                    order_product_id
                )
        }
        selected_pre_modifier?.let { preModiifer ->
            if (preModiifer.strPreModifierName.equals(CV.ADD) ||
                preModiifer.strPreModifierName.equals(CV.EXTRA)
            ) {
                //    preModiifer.strPreModifierName.equals(CV.ON_SIDE)
                selected_ingredient?.let {
                    if (!TextUtils.isEmpty(it.dblOptionPrice)) {
                        updatedItem.itemRate =
                            updatedItem.itemRate + it.dblOptionPrice.toDouble()
                    }
                }

                updatedItem.itemAmount =
                    updatedItem.itemRate * updatedItem.itemQty
                updatedItem.taxAmount =
                    (updatedItem.itemAmount * updatedItem.taxPercentage.toDouble() / 100).toString()

                /*mDatabase.daoOrderTrans().updatePriceAmountTax(
                    updatedItem.itemRate,
                    updatedItem.itemAmount,
                    updatedItem.taxAmount,
                    order_id,
                    item_id
                )*/
                mDatabase.daoOrderTrans().updatePriceAmountTax_(
                    updatedItem.itemRate,
                    updatedItem.itemAmount,
                    updatedItem.taxAmount,
                    order_id,
                    item_id,
                    order_product_id
                )
                openDiscountCalculation(updatedItem)
                discountMixMatchCalculation() {
                    cartAdapter?.updateItem(pos, updatedItem)
                    val tempReverseList = orderTransList.reversed()
                    tempReverseList.get(pos).id = updatedItem.id
                    tempReverseList.get(pos).orderId = updatedItem.orderId
                    tempReverseList.get(pos).itemId = updatedItem.itemId
                    tempReverseList.get(pos).itemName = updatedItem.itemName
                    tempReverseList.get(pos).itemRate = updatedItem.itemRate
                    tempReverseList.get(pos).itemQty = updatedItem.itemQty
                    tempReverseList.get(pos).itemAmount = updatedItem.itemAmount
                    tempReverseList.get(pos).discountAmount = updatedItem.discountAmount
                    tempReverseList.get(pos).discountRemark = updatedItem.discountRemark
                    tempReverseList.get(pos).iDiscountId = updatedItem.iDiscountId
                    tempReverseList.get(pos).strPricingType = updatedItem.strPricingType
                    tempReverseList.get(pos).paymentType = updatedItem.paymentType
                    tempReverseList.get(pos).dateTime = updatedItem.dateTime
                    tempReverseList.get(pos).taxPercentage = updatedItem.taxPercentage
                    tempReverseList.get(pos).taxAmount = updatedItem.taxAmount
                    tempReverseList.get(pos).reason = updatedItem.reason
                    tempReverseList.get(pos).itemQtyLeft = updatedItem.itemQtyLeft
                    tempReverseList.get(pos).grandTotal = updatedItem.grandTotal
                    tempReverseList.get(pos).premodifierType = updatedItem.premodifierType
                    tempReverseList.get(pos).ingredientName = updatedItem.ingredientName
                    mDatabase.daoOrderTrans().updateOrderTrans(orderTransList.get(pos))
                    this.calculateCartProcedure(tempReverseList, true) {}
                    //Hardware Code For CDS Display -- NewSale While select any ingredient
                    DisplayNewSaleDataToCDS(orderTransList, "", false)
                }

            } else {
                openDiscountCalculation(updatedItem)
                discountMixMatchCalculation() {
                    cartAdapter?.updateItem(pos, updatedItem)
                    val tempReverseList = orderTransList.reversed()
                    tempReverseList.get(pos).id = updatedItem.id
                    tempReverseList.get(pos).orderId = updatedItem.orderId
                    tempReverseList.get(pos).itemId = updatedItem.itemId
                    tempReverseList.get(pos).itemName = updatedItem.itemName
                    tempReverseList.get(pos).itemRate = updatedItem.itemRate
                    tempReverseList.get(pos).itemQty = updatedItem.itemQty
                    tempReverseList.get(pos).itemAmount = updatedItem.itemAmount
                    tempReverseList.get(pos).strPricingType = updatedItem.strPricingType
                    tempReverseList.get(pos).paymentType = updatedItem.paymentType
                    tempReverseList.get(pos).dateTime = updatedItem.dateTime
                    tempReverseList.get(pos).taxPercentage = updatedItem.taxPercentage
                    tempReverseList.get(pos).taxAmount = updatedItem.taxAmount
                    tempReverseList.get(pos).reason = updatedItem.reason
                    tempReverseList.get(pos).itemQtyLeft = updatedItem.itemQtyLeft
                    tempReverseList.get(pos).grandTotal = updatedItem.grandTotal
                    tempReverseList.get(pos).premodifierType = updatedItem.premodifierType
                    tempReverseList.get(pos).ingredientName = updatedItem.ingredientName

                    mDatabase.daoOrderTrans().updateOrderTrans(orderTransList.get(pos))
                    this.calculateCartProcedure(tempReverseList, true) {}
                    //Hardware Code For CDS Display -- NewSale While select any ingredient
                    DisplayNewSaleDataToCDS(orderTransList, "", false)
                }
            }
        }
    }

    private fun openInstructionDialog() {
        val dialog = Dialog(mActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.setCallback(
            UserInteractionAwareCallback(
                dialog.window!!.getCallback(),
                mActivity
            )
        )
        dialog.setContentView(R.layout.dialog_modifier_instruction)

        dialog.setCancelable(false)
        dialog.show()

        val ivCancel = dialog.findViewById<ImageView>(R.id.ivCancel)
        val edtinstruction = dialog.findViewById<TextView>(R.id.edtinstruction)
        val btnSubmit = dialog.findViewById<Button>(R.id.btnSubmit)
        btnSubmit.setText(getString(R.string.confirm))
        edtinstruction.text= orderTransList.reversed().get(pos).strInstruction
        btnSubmit.setOnClickListener {
            LogUtil.d("POS_number",pos.toString())
            orderTransList.reversed().get(pos).strInstruction=edtinstruction.text.toString()
            /*mDatabase.daoOrderTrans().updateOrderTransInstruction(edtinstruction.text.toString(),
                orderTransList.reversed().get(pos).orderId,orderTransList.reversed().get(pos).itemId)*/
            mDatabase.daoOrderTrans().updateOrderTransInstruction_(edtinstruction.text.toString(),
                orderTransList.reversed().get(pos).orderId,orderTransList.reversed().get(pos).itemId,orderTransList.reversed().get(pos).id)
            cartAdapter!!.notifyDataSetChanged()
            DisplayNewSaleDataToCDS(cartAdapter!!.getDataset(),"",false)
            dialog.dismiss()
        }
        ivCancel.setOnClickListener {
            dialog.dismiss()
        }
    }

    private fun deleteModifier(i: Int) {
        val turnsType =
            object : TypeToken<HashMap<String, List<IngredientMasterModel>>>() {}.type
        /*var modifierMap = Gson().fromJson<HashMap<String, List<IngredientMasterModel>>>(
            mDatabase.daoOrderTrans().getAppliedModifierPerItemOnOrder(order_id, item_id),
            turnsType
        )*/
        var modifierMap = Gson().fromJson<HashMap<String, List<IngredientMasterModel>>>(
            mDatabase.daoOrderTrans().getAppliedModifierPerItemOnOrder_(order_id, item_id,order_product_id),
            turnsType
        )
        var ingredientListTemp = ArrayList<IngredientMasterModel>()
        var keyListTemp = ArrayList<String>()

        for ((k, v) in modifierMap) {
            println("$k = $v")
            //ingredientListTemp = v as ArrayList<IngredientMasterModel>
            keyListTemp.add(k)
            ingredientListTemp.addAll(v)
        }
        if (ingredientListTemp.size == 1) {
            modifierMap.remove(selected_pre_modifier!!.strPreModifierName)
            ingredientHighlitedOpreation(modifierMap)
            orderTransList.reversed().get(pos).premodifierType = ""
            orderTransList.reversed().get(pos).ingredientName = ""
            if (selected_pre_modifier!!.strPreModifierName.equals(CV.ADD) ||
                selected_pre_modifier!!.strPreModifierName.equals(CV.EXTRA)
            ) {
                orderTransList.reversed().get(pos).itemRate = orderTransList.reversed()
                    .get(pos).itemRate - selected_ingredient!!.dblOptionPrice.toDouble()
                orderTransList.reversed().get(pos).itemAmount =
                    orderTransList.reversed().get(pos).itemRate * orderTransList.reversed()
                        .get(pos).itemQty
                val gson = GsonBuilder().create()
                val ingredientDataInStringFormate = gson.toJson(modifierMap)
                val ingredientStringName = GetModifierIngredients(modifierMap)
                mDatabase.daoOrderTrans().updateOrderTrans(orderTransList.reversed().get(pos))
                openDiscountCalculation(orderTransList.reversed().get(pos))
                discountMixMatchCalculation() {
                    cartAdapter!!.notifyItemChanged(pos)
                    /*mDatabase.daoOrderTrans().updateModifier(
                        ingredientDataInStringFormate,
                        ingredientStringName,
                        order_id,
                        item_id
                    )*/
                    mDatabase.daoOrderTrans().updateModifier_(
                        ingredientDataInStringFormate,
                        ingredientStringName,
                        order_id,
                        item_id,
                        order_product_id
                    )
                    this.calculateCartProcedure(orderTransList.reversed(), true) {}
                    //Hardware Code For CDS Display -- NewSale While select any ingredient
                    DisplayNewSaleDataToCDS(orderTransList, "", false)
                }
            } else {
                mDatabase.daoOrderTrans().updateOrderTrans(orderTransList.reversed().get(pos))
                val gson = GsonBuilder().create()
                val ingredientDataInStringFormate = gson.toJson(modifierMap)
                val ingredientStringName = GetModifierIngredients(modifierMap)
                openDiscountCalculation(orderTransList.reversed().get(pos))
                discountMixMatchCalculation() {
                    cartAdapter!!.notifyItemChanged(pos)
                    /*mDatabase.daoOrderTrans().updateModifier(
                        ingredientDataInStringFormate,
                        ingredientStringName,
                        order_id,
                        item_id
                    )*/
                    mDatabase.daoOrderTrans().updateModifier_(
                        ingredientDataInStringFormate,
                        ingredientStringName,
                        order_id,
                        item_id,
                        order_product_id
                    )
                    this.calculateCartProcedure(orderTransList.reversed(), true) {}
                    //Hardware Code For CDS Display -- NewSale While select any ingredient
                    DisplayNewSaleDataToCDS(orderTransList, "", false)
                }
            }
            return
        }
        ingredientListTemp.remove(selected_ingredient)
        val tt = ArrayList<IngredientMasterModel>()
        tt.add(selected_ingredient!!)
        var isRemove = true
        if (keyListTemp.size == 1 && ingredientListTemp.size >= 1) {
            isRemove = false
        }
        if (isRemove) {
            //modifierMap.remove(selected_pre_modifier!!.strPreModifierName, tt)
            //above function not working in some case so we used below code
            val tempFilteredList =
                modifierMap[selected_pre_modifier!!.strPreModifierName]!!.filter { item -> item.strOptionName != tt[0].strOptionName }
            modifierMap.remove(selected_pre_modifier!!.strPreModifierName)
            if (tempFilteredList.isNotEmpty() && tempFilteredList.size > 0) {
                modifierMap.set(selected_pre_modifier!!.strPreModifierName, tempFilteredList)
            }
            ingredientHighlitedOpreation(modifierMap)
        } else {
            modifierMap.set(selected_pre_modifier!!.strPreModifierName, ingredientListTemp)
            ingredientHighlitedOpreation(modifierMap)
        }
        val gson = GsonBuilder().create()
        val ingredientDataInStringFormate = gson.toJson(modifierMap)
        val ingredientStringName = GetModifierIngredients(modifierMap)
        /*mDatabase.daoOrderTrans()
            .updateModifier(
                ingredientDataInStringFormate,
                ingredientStringName,
                order_id,
                item_id
            )*/
        mDatabase.daoOrderTrans()
            .updateModifier_(
                ingredientDataInStringFormate,
                ingredientStringName,
                order_id,
                item_id,
                order_product_id
            )
        orderTransList.reversed()

        orderTransList.reversed().get(pos).premodifierType = ingredientDataInStringFormate
        orderTransList.reversed().get(pos).ingredientName = ingredientStringName
        if (selected_pre_modifier!!.strPreModifierName.equals(CV.ADD) ||
            selected_pre_modifier!!.strPreModifierName.equals(CV.EXTRA)
        ) {
            orderTransList.reversed().get(pos).itemRate = orderTransList.reversed()
                .get(pos).itemRate - selected_ingredient!!.dblOptionPrice.toDouble()
            orderTransList.reversed().get(pos).itemAmount =
                orderTransList.reversed().get(pos).itemRate * orderTransList.reversed()
                    .get(pos).itemQty
            mDatabase.daoOrderTrans().updateOrderTrans(orderTransList.reversed().get(pos))
            openDiscountCalculation(orderTransList.reversed().get(pos))
            discountMixMatchCalculation() {
                cartAdapter!!.notifyItemChanged(pos)
                this.calculateCartProcedure(orderTransList.reversed(), true) {}
                //Hardware Code For CDS Display -- NewSale While select any ingredient
                DisplayNewSaleDataToCDS(orderTransList, "", false)
            }
        } else {
            mDatabase.daoOrderTrans().updateOrderTrans(orderTransList.reversed().get(pos))
            openDiscountCalculation(orderTransList.reversed().get(pos))
            discountMixMatchCalculation() {
                cartAdapter!!.notifyItemChanged(pos)
                this.calculateCartProcedure(orderTransList.reversed(), true) {}
                //Hardware Code For CDS Display -- NewSale While select any ingredient
                DisplayNewSaleDataToCDS(orderTransList, "", false)
            }
        }
    }

    private fun openSearchItemDialog() {
        allDataModel.clear()
        edtSearchBarcode.setText("")
        dialogSearchItem = Dialog(mActivity)
        dialogSearchItem.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogSearchItem.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialogSearchItem.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogSearchItem.window?.setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        )
        dialogSearchItem.window!!.setCallback(
                UserInteractionAwareCallback(
                        dialogSearchItem.window!!.getCallback(),
                        mActivity
                )
        )
        dialogSearchItem.setContentView(R.layout.dialog_search_item)
        dialogSearchItem.setCancelable(true)

        dialogSearchItem.setOnCancelListener {
            searchViewModel?.resetProductsList()
            allDataModel.clear()
            itemSearchAdapter = null
        }
        dialogSearchItem.setOnDismissListener {
            searchViewModel?.resetProductsList()
            allDataModel.clear()
            itemSearchAdapter = null
        }

        val edtSearchCustomer =
            dialogSearchItem.findViewById<AppCompatEditText>(R.id.edtSearchCustomer)

        val rvCustomer = dialogSearchItem.findViewById<RecyclerView>(R.id.rvCustomer)
        rvCustomer.adapter = itemSearchAdapter
        rvCustomer.setHasFixedSize(true)

        val mLayoutManager = GridLayoutManager(dialogSearchItem.context, 1)
        //set up layout manager
        mLayoutManager.orientation = LinearLayoutManager.VERTICAL
        mLayoutManager.isSmoothScrollbarEnabled = true
        mLayoutManager.isAutoMeasureEnabled = false
        rvCustomer.layoutManager = mLayoutManager

        allDataModel = ArrayList()
        itemSearchAdapter = ItemListSearchAdapter(mActivity, allDataModel, this)
        rvCustomer.adapter = itemSearchAdapter

        var tempList: ArrayList<ItemModel> = ArrayList()
        searchViewModel!!.products.observe(this, androidx.lifecycle.Observer { productList ->
            if (productList != null) {

                itemSearchAdapter?.let {
                    it.notifyDataSetChanged()
                    allDataModel.addAll(productList as ArrayList<ItemModel>)
                    it.addData(allDataModel)
                    tempList.addAll(productList as ArrayList<ItemModel>)
                }
                searchViewModel!!.OFFSET += productList.size
                Log.d("observe", "off set ${searchViewModel!!.OFFSET} ")
            }
        })

        searchViewModel?.OFFSET = 0
        searchViewModel!!.searchText = ""
        searchViewModel?.searchProducts()

        var mBtnSearch = dialogSearchItem.findViewById<AppCompatButton>(R.id.btn_search)

        mBtnSearch.setOnClickListener {
            allDataModel.clear()
            itemSearchAdapter =
                ItemListSearchAdapter(mActivity, allDataModel, this@NewSalesFragment)
            rvCustomer.adapter = itemSearchAdapter

            val imm =
                context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(it.windowToken, 0)

            searchViewModel!!.OFFSET = 0
            searchViewModel!!.resetProductsList()
            searchViewModel!!.searchProducts()
        }

        edtSearchCustomer.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                searchViewModel!!.searchText = s.toString()

                if (s.isEmpty()) {
                    tempList.clear()
                    if (tempList.size > 0) {
                        itemSearchAdapter = ItemListSearchAdapter(
                            mActivity, tempList,
                            this@NewSalesFragment
                        )
                        rvCustomer.adapter = itemSearchAdapter
                    } else {
                        allDataModel.clear()
                        itemSearchAdapter =
                            ItemListSearchAdapter(
                                mActivity,
                                allDataModel,
                                this@NewSalesFragment
                            )
                        rvCustomer.adapter = itemSearchAdapter

                        searchViewModel!!.OFFSET = 0
                        searchViewModel!!.resetProductsList()
                        Log.d("afterTextChanged", "off set ${searchViewModel!!.OFFSET} ")
                        searchViewModel!!.searchProducts()
                    }
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })

        dialogSearchItem.show()
    }


    private fun webCallRefund(
        merchantId: String,
        retref: String,
        paymentType: String,
        callBack: (isSuccess: Boolean, strInvoiceNumber: String, totalQty: Int, errorMsg: String) -> Unit
    ) {
        resetExistingEmployee()
//        showKcsDialog()
        var totalItemTax = 0.0
        var totalDiscount = 0.0
        var totalItemQty = 0
        val req_payment_refund: ArrayList<RefundRequestModel> = ArrayList()
        payment_mode = paymentType
        var tt = 0
        for (i in orderTransListSelected.indices) {
            tt = orderTransListSelected[i].itemQty
            var tax = orderTransListSelected[i].taxAmount
            var newTax = tax.replace(",", "")
            var tax_amount =
                ((orderTransListSelected[i].itemAmount) * (newTax.toDouble())) / 100
            orderTransListSelected.get(i).taxAmount = String.format(
                "%.2f", newTax.toDouble()
            )
            var taxAmoutForItem = getTaxAmountAppliedDiscount(orderTransListSelected.get(i))
            val temp = RefundRequestModel(
                orderTransListSelected[i].itemId.toString(),
                tt.toString(),
                orderTransListSelected.get(i).itemRate.toString(),
                orderTransListSelected.get(i).reason,
                taxAmoutForItem

            )
            totalItemTax += taxAmoutForItem
            //totalItemTax += orderTransListSelected.get(i).taxAmount.toDouble() * tt
            totalItemQty += tt
            req_payment_refund.add(temp)
        }

        calculateCartSelectedRefundProcedure(orderTransListSelected) { cart: CartRefundSummaryData ->
            if (isRefundDiscountApplied) {
                totalDiscount = cart.totalDiscount.toDouble()
            } else {
                totalDiscount = cart.saleDiscountAmount.toDouble()
            }
        }
        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.WS_DATE_FORMATTE)
        paymentRefundViewModel!!.paymentRefund(
            paymentViewModel.payableAmount.toDouble(),
            sdf.format(date),
            id,
            paymentType,
            merchantId,
            retref,
            paymentViewModel.reasonForRefund,
            "Done",
            emp_id,
            Gson().toJson(req_payment_refund),
            totalItemTax,
            totalDiscount

        )?.observe(this, androidx.lifecycle.Observer { NewSaleandRefundModel ->

            if (NewSaleandRefundModel.data == null) {
                showToast(
                    mActivity,
                    NewSaleandRefundModel.message!!,
                    R.drawable.icon,
                    custom_toast_container
                )
                NewSaleandRefundModel.message?.let { callBack(false, "", 0, it) }
            } else {
                NewSaleandRefundModel.data?.let {
                    callBack(true, it.data.strInvoiceNumber, totalItemQty, "")

                }
            }
        })
    }

    private fun PrintRefundReceipt(
        mReason: String,
        mInvoiceNumber: String
    ) {
        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.DISPLAY_DATE_FORMATTE_AMPM)

        val mPrintRefundReceipt = PrinterRefundModel()

        val POSName = CM.getSp(mActivity, CV.POS_NAME, "") as String

        mPrintRefundReceipt.strSocialMedia =
            CM.getSp(mActivity, CV.STRSOCIALMEDIA, "").toString()
        mPrintRefundReceipt.facebook = CM.getSp(mActivity, CV.STRFACEBOOK, "").toString()
        mPrintRefundReceipt.instagram = CM.getSp(mActivity, CV.STRINSTAGRAM, "").toString()
        mPrintRefundReceipt.twitter = CM.getSp(mActivity, CV.STRTWITTER, "").toString()
        mPrintRefundReceipt.youtube = CM.getSp(mActivity, CV.STRYOUTUBE, "").toString()

        mPrintRefundReceipt.setStoreName(StoreName)
        mPrintRefundReceipt.setAddressLine1(StoreAddress1)
        mPrintRefundReceipt.setAddressLine2(StoreAddress2)
        mPrintRefundReceipt.setTelNumber(StorePhoneNum)

        mPrintRefundReceipt.setStoreLogoUrl(
            CM.getSp(mActivity, CV.STORE_LOGO_URL, "").toString()
        )
        mPrintRefundReceipt.setStoreLogoLocalPath(CV.STORE_LOGO_LOCAL_PATH)

        mPrintRefundReceipt.setBilledPersonName(getCustomerName())
        mPrintRefundReceipt.setInvoiceNo(selected_history!!.strInvoiceNumber)
        mPrintRefundReceipt.setRefundInvoiceNo(mInvoiceNumber)

        mPrintRefundReceipt.setReceiptDateTime(selected_history!!.datetimeCheckBusinessDate)
        mPrintRefundReceipt.setPOSStationNo(POSName)

        mPrintRefundReceipt.setCashierName(emp_name)
        mPrintRefundReceipt.setEmpRole(emp_role)

        mPrinterRefundProducts.clear()
        for (i in orderTransListSelected.indices) {

            val mPrinterRefundProductsModel = PrinterRefundProductsModel()
            orderTransListSelected.get(i).itemAmount - orderTransListSelected.get(i).discountAmount

            val itemQty = orderTransListSelected.get(i).itemQty
            val itemRate = orderTransListSelected.get(i).itemRate
            var itemPrice = itemQty * itemRate
            itemPrice -= NewSaleFragmentCommon.floatToDouble(orderTransListSelected.get(i).discountAmount)
            mPrinterRefundProductsModel.setProductName(orderTransListSelected.get(i).itemName)
            mPrinterRefundProductsModel.setQuanity(orderTransListSelected.get(i).itemQty.toString())
            mPrinterRefundProductsModel.setModifierIngredient(
                GetModifierIngredient(
                    selected_history!!.orderProducts!!.get(
                        i
                    ).productModifier
                )
            )
            mPrinterRefundProductsModel.setPaidAmount(currency_symbol + " " + itemPrice.toString())
            mPrinterRefundProductsModel.setDiscount(
                currency_symbol + " " + orderTransListSelected.get(
                    i
                ).discountAmount.toString()
            )
            mPrinterRefundProductsModel.setDiscountRemark(orderTransListSelected.get(i).discountRemark)

            mPrinterRefundProducts.add(mPrinterRefundProductsModel)
        }

        calculateCartSelectedRefundProcedure(orderTransListSelected) { cart: CartRefundSummaryData ->

            var isTaxIncluded = true
            if (selected_history != null) {
                isTaxIncluded = !selected_history!!.tCheckTaxExempt.equals("0")
            }
            var final_tax_anount = 00.00
            if (isTaxIncluded) {
                final_tax_anount = cart.taxAmount + cart.saleTaxAmount
            } else {
                final_tax_anount = 00.00
            }
            var totalPayableAmount = 00.00
            if (customerListViewModel?.isTaxIncluded?.value!!) {
                mPrintRefundReceipt.setTaxApplied(
                    getFormatedAmountWithCurrencySymbol(
                        final_tax_anount
                    )
                )
            } else {
                mPrintRefundReceipt.setTaxApplied(getFormatedAmountWithCurrencySymbol(00.00))
            }
            totalPayableAmount =
                (cart.totalAmount + cart.saleTotalAmount + final_tax_anount) - cart.totalDiscount
            mPrintRefundReceipt.setTotalQty(cart.totalQty.toString())

            mPrintRefundReceipt.setSubtotal(
                currency_symbol + " " + String.format(
                    "%.2f",
                    (cart.totalAmount + cart.saleTotalAmount)
                )
            )

            mPrintRefundReceipt.setGrandTotal(
                currency_symbol + " " + String.format(
                    "%.2f",
                    (totalPayableAmount)
                )
            )
            mPrintRefundReceipt.setRefundAmount(
                currency_symbol + " " + String.format(
                    "%.2f",
                    (totalPayableAmount)
                )
            )

            if (isRefundDiscountApplied) {
                mPrintRefundReceipt.setDiscount(getFormatedAmountWithCurrencySymbol(cart.totalDiscount))
                if (!selected_history!!.orderDiscounts?.isNullOrEmpty()!! && cart.totalDiscount > 0) {
                    mPrintRefundReceipt.setDiscountRemark(
                        selected_history!!.orderDiscounts?.get(
                            0
                        )?.strRemark + CV.WAS_APPLIED
                    )
                } else {
                    mPrintRefundReceipt.setDiscountRemark("")
                }
            } else {
                mPrintRefundReceipt.setDiscount(currency_symbol + " 0.0")
                mPrintRefundReceipt.setDiscountRemark("")
            }
        }

        mPrintRefundReceipt.setRefundReason(mReason)

        mPrintRefundReceipt.setRefundProducts(mPrinterRefundProducts)
        mPrintRefundReceipt.setPaymentMode(payment_mode)
        if (NewSaleFragmentCommon.isOtherCashTrasaction(paymentMethodList, payment_mode)) {
            mPrintRefundReceipt.isOtherPaymentMode = true
        }

        var baseActivityPrint = (activity as ParentActivity)

        if (baseActivityPrint.getBitmapFromLocal(
                mPrintRefundReceipt.getStoreLogoLocalPath(),
                200
            ) != null
        ) {
            mImagesCache.addImageToWarehouse(
                SQUARE_IMAGE_NAME,
                baseActivityPrint.getBitmapFromLocal(
                    mPrintRefundReceipt.getStoreLogoLocalPath(),
                    200
                )!!
            )
        }

        mImagesCache.addImageToWarehouse(
            BARCODE_IMAGE_NAME,
            mBarcodeGenerator!!.GenerateBarcode(selected_history!!.strInvoiceNumber)
        )

        baseActivityPrint.mPrinterController.printRefundTicket(
            baseActivityPrint,
            mPrintRefundReceipt
        )
    }

    private fun RePrintRefundReceipt(selected_history: OrderHistoryModel?) {
        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.DISPLAY_DATE_FORMATTE_AMPM)

        val mPrintRefundReceipt = PrinterRefundModel()

        val POSName = CM.getSp(mActivity, CV.POS_NAME, "") as String
        mPrintRefundReceipt.strSocialMedia =
            CM.getSp(mActivity, CV.STRSOCIALMEDIA, "").toString()
        mPrintRefundReceipt.facebook = CM.getSp(mActivity, CV.STRFACEBOOK, "").toString()
        mPrintRefundReceipt.instagram = CM.getSp(mActivity, CV.STRINSTAGRAM, "").toString()
        mPrintRefundReceipt.twitter = CM.getSp(mActivity, CV.STRTWITTER, "").toString()
        mPrintRefundReceipt.youtube = CM.getSp(mActivity, CV.STRYOUTUBE, "").toString()

        mPrintRefundReceipt.setStoreName(StoreName)
        mPrintRefundReceipt.setAddressLine1(StoreAddress1)
        mPrintRefundReceipt.setAddressLine2(StoreAddress2)
        mPrintRefundReceipt.setTelNumber(StorePhoneNum)
        mPrintRefundReceipt.setStoreLogoUrl(
            CM.getSp(mActivity, CV.STORE_LOGO_URL, "").toString()
        )
        mPrintRefundReceipt.setStoreLogoLocalPath(CV.STORE_LOGO_LOCAL_PATH)

        mPrintRefundReceipt.setBilledPersonName(selected_history!!.strFirstName + " " + selected_history!!.strLastName)
        mPrintRefundReceipt.setInvoiceNo(selected_history!!.strInvoiceNumber)
        mPrintRefundReceipt.setRefundInvoiceNo(selected_history!!.strInvoiceNumber)

        mPrintRefundReceipt.setReceiptDateTime(selected_history!!.datetimeCheckBusinessDate)
        mPrintRefundReceipt.setPOSStationNo(POSName)

        emp_name =
            selected_history.orderRefund!!.get(0).refundEmployeeData!!.get(0).strFirstName
        emp_role =
            selected_history.orderRefund!!.get(0).refundEmployeeData!!.get(0).strEmployeeRoleName

        mPrintRefundReceipt.setCashierName(emp_name)
        mPrintRefundReceipt.setEmpRole(emp_role)

        mPrintRefundReceipt.setRefundReason(selected_history!!.orderRefund!!.get(0).strRefundReason)

        mPrinterRefundProducts.clear()
        orderTransListSelected.clear()
        for (i in selected_history.orderRefund!!.indices) {

            for (j in selected_history.orderRefund!!.get(i).refundItems!!.indices) {
                var iCheckProductId =
                    selected_history.orderRefund!!.get(i).refundItems?.get(j)?.iCheckItemDetailId
                if (selected_history.orderProducts!!.any { it.iCheckItemDetailId.toInt() == iCheckProductId }) {
                    var productpos =
                        selected_history.orderProducts!!.indexOfFirst { it.iCheckItemDetailId.toInt() == iCheckProductId }
                    var itemqty =
                        selected_history!!.orderRefund!![i].refundItems?.get(j)
                            ?.dblItemQty?.toDouble()
                    var itemprice =
                        selected_history!!.orderRefund!![i].refundItems?.get(j)
                            ?.dblRefundAmount?.toDouble()
                    var itemtotal =
                        (itemprice!! * itemqty!!) - NewSaleFragmentCommon.floatToDouble(
                            orderTransList.get(productpos).discountAmount
                        )
                    val mPrinterRefundProductsModel = PrinterRefundProductsModel()

                    mPrinterRefundProductsModel.setProductName(
                        selected_history!!.orderProducts!!.get(
                            productpos
                        ).strProductName
                    )

                    mPrinterRefundProductsModel.setQuanity(
                        selected_history.orderRefund!![i].refundItems!![j].dblItemQty.toString()
                    )

                    mPrinterRefundProductsModel.setPaidAmount(currency_symbol + " " + itemtotal)

                    mPrinterRefundProductsModel.setDiscount(
                        currency_symbol + " " + orderTransList.get(
                            productpos
                        ).discountAmount.toString()
                    )

                    mPrinterRefundProductsModel.setDiscountRemark(orderTransList.get(productpos).discountRemark)

//                    mPrinterRefundProductsModel.setModifierIngredient(orderTransList.get(i).ingredientName)
                    mPrinterRefundProductsModel.setModifierIngredient(
                        GetModifierIngredient(
                            selected_history!!.orderProducts!!.get(productpos).productModifier
                        )
                    )
                    mPrinterRefundProducts.add(mPrinterRefundProductsModel)
                    val temp = OrderTransModel(
                        orderTransList[productpos].id,
                        orderTransList[productpos].orderId,
                        orderTransList[productpos].itemId,
                        orderTransList[productpos].itemName,
                        orderTransList[productpos].itemRate,
                        selected_history!!.orderRefund!![i].refundItems?.get(j)?.dblItemQty!!,
                        orderTransList[productpos].itemAmount,
                        orderTransList[productpos].strPricingType,
                        orderTransList[productpos].paymentType,
                        orderTransList[productpos].dateTime,
                        orderTransList[productpos].taxPercentage,
                        orderTransList[productpos].taxAmount,
                        orderTransList[productpos].reason,
                        0,
                        orderTransList[productpos].grandTotal,
                        false,
                        selected_history!!.orderProducts!![productpos].strProductType,
                        selected_history!!.orderProducts!![productpos].iDiscountId.toInt(),
                        selected_history!!.orderProducts!![productpos].strRemark,
                        selected_history!!.orderProducts!![productpos].dblItemDiscount.toFloat(),
                        orderTransList[productpos].premodifierType,
                        orderTransList[productpos].ingredientName,
                        orderTransList[productpos].strInstruction,
                        selected_history!!.orderProducts!![productpos].kitchenServedquantity,
                        isKitchenPrintServed,
                        tIsOpenDiscount = orderTransList[productpos].tIsOpenDiscount,
                        totalItemQty = selected_history!!.orderRefund!![i].refundItems?.get(
                            j
                        )?.dblItemQty!!,
                        refundProductId = selected_history!!.orderProducts!![productpos].iProductId
                    )
                    orderTransListSelected.add(temp)

                }

            }
        }
        /*for (i in selected_history!!.orderProducts!!.indices) {

            for (j in selected_history.orderRefund!!.get(0).refundItems!!.indices) {
                var iProductId =
                    selected_history.orderRefund!!.get(0).refundItems?.get(j)?.iProductId

//                var itemprice = selected_history!!.orderProducts!!.get(i).dblAmount.toDouble() - orderTransList.get(i).discountAmount

                if (iProductId?.equals(selected_history.orderProducts!!.get(i).iProductId)!!) {

                    var itemqty =
                        selected_history!!.orderRefund!![0].refundItems?.get(j)
                            ?.dblItemQty?.toDouble()
                    var itemprice =
                        selected_history!!.orderRefund!![0].refundItems?.get(j)
                            ?.dblRefundAmount?.toDouble()
                    var itemtotal =
                        (itemprice!! * itemqty!!) - NewSaleFragmentCommon.floatToDouble(
                            orderTransList.get(i).discountAmount
                        )

                    val mPrinterRefundProductsModel = PrinterRefundProductsModel()

                    mPrinterRefundProductsModel.setProductName(
                        selected_history!!.orderProducts!!.get(
                            i
                        ).strProductName
                    )

                    mPrinterRefundProductsModel.setQuanity(
                        selected_history.orderRefund!![0].refundItems!![j].dblItemQty.toString()
                    )

                    mPrinterRefundProductsModel.setPaidAmount(currency_symbol + " " + itemtotal)

                    mPrinterRefundProductsModel.setDiscount(
                        currency_symbol + " " + orderTransList.get(
                            i
                        ).discountAmount.toString()
                    )

                    mPrinterRefundProductsModel.setDiscountRemark(orderTransList.get(i).discountRemark)

//                    mPrinterRefundProductsModel.setModifierIngredient(orderTransList.get(i).ingredientName)
                    mPrinterRefundProductsModel.setModifierIngredient(
                        GetModifierIngredient(
                            selected_history!!.orderProducts!!.get(i).productModifier
                        )
                    )
                    mPrinterRefundProducts.add(mPrinterRefundProductsModel)

                    val temp = OrderTransModel(
                        orderTransList[i].id,
                        orderTransList[i].orderId,
                        orderTransList[i].itemId,
                        orderTransList[i].itemName,
                        orderTransList[i].itemRate,
                        selected_history!!.orderRefund!![0].refundItems?.get(j)?.dblItemQty!!,
                        orderTransList[i].itemAmount,
                        orderTransList[i].strPricingType,
                        orderTransList[i].paymentType,
                        orderTransList[i].dateTime,
                        orderTransList[i].taxPercentage,
                        orderTransList[i].taxAmount,
                        orderTransList[i].reason,
                        0,
                        orderTransList[i].grandTotal,
                        false,
                        selected_history!!.orderProducts!![i].strProductType,
                        selected_history!!.orderProducts!![i].iDiscountId.toInt(),
                        selected_history!!.orderProducts!![i].strRemark,
                        selected_history!!.orderProducts!![i].dblItemDiscount.toFloat(),
                        orderTransList[i].premodifierType,
                        orderTransList[i].ingredientName,
                        orderTransList[i].strInstruction,
                        selected_history!!.orderProducts!![i].kitchenServedquantity,
                        isKitchenPrintServed,
                        tIsOpenDiscount = orderTransList[i].tIsOpenDiscount,
                        totalItemQty = selected_history!!.orderRefund!![0].refundItems?.get(j)?.dblItemQty!!,
                        refundProductId = selected_history!!.orderProducts!![i].iProductId
                    )
                    orderTransListSelected.add(temp)
                }
            }

        }*/
        payment_mode = selected_history.orderPayments?.get(0)?.strPaymentMethod.toString()



        calculateCartSelectedRefundProcedure(orderTransListSelected,isFromRefundRePrint = true) { cart: CartRefundSummaryData ->
            var isTaxIncluded = true
            if (selected_history != null) {
                isTaxIncluded = !selected_history!!.tCheckTaxExempt.equals("0")
            }
            var final_tax_anount = 00.00
            if (isTaxIncluded) {
                final_tax_anount = cart.taxAmount + cart.saleTaxAmount
            } else {
                final_tax_anount = 00.00
            }
            var totalPayableAmount = 00.00
            if (customerListViewModel?.isTaxIncluded?.value!!) {
                mPrintRefundReceipt.setTaxApplied(
                    getFormatedAmountWithCurrencySymbol(
                        final_tax_anount
                    )
                )
            } else {
                mPrintRefundReceipt.setTaxApplied(getFormatedAmountWithCurrencySymbol(00.00))
            }
            totalPayableAmount =
                (cart.totalAmount + cart.saleTotalAmount + final_tax_anount) - cart.totalDiscount
            mPrintRefundReceipt.setTotalQty(cart.totalQty.toString())

            mPrintRefundReceipt.setSubtotal(
                currency_symbol + " " + String.format(
                    "%.2f",
                    (cart.totalAmount + cart.saleTotalAmount)
                )
            )

            mPrintRefundReceipt.setGrandTotal(
                currency_symbol + " " + String.format(
                    "%.2f",
                    (totalPayableAmount)
                )
            )
            mPrintRefundReceipt.setRefundAmount(
                currency_symbol + " " + String.format(
                    "%.2f",
                    (totalPayableAmount)
                )
            )
            mPrintRefundReceipt.setDiscount(getFormatedAmountWithCurrencySymbol(cart.totalDiscount))
            if (selected_history!!.orderDiscounts?.isNullOrEmpty()!!) {
                mPrintRefundReceipt.setDiscountRemark("")
            } else {
                mPrintRefundReceipt.setDiscountRemark(selected_history!!.orderDiscounts?.get(0)?.strRemark + CV.WAS_APPLIED)
            }
        }

        mPrintRefundReceipt.setRefundProducts(mPrinterRefundProducts)
        mPrintRefundReceipt.setPaymentMode(payment_mode)
        if (NewSaleFragmentCommon.isOtherCashTrasaction(
                paymentMethodList,
                payment_mode
            )
        ) {
            mPrintRefundReceipt.isOtherPaymentMode = true
        }

        var baseActivityPrint = (activity as ParentActivity)

        if (baseActivityPrint.getBitmapFromLocal(
                mPrintRefundReceipt.getStoreLogoLocalPath(),
                200
            ) != null
        ) {
            mImagesCache.addImageToWarehouse(
                SQUARE_IMAGE_NAME,
                baseActivityPrint.getBitmapFromLocal(
                    mPrintRefundReceipt.getStoreLogoLocalPath(),
                    200
                )!!
            )
        }

        mImagesCache.addImageToWarehouse(
            BARCODE_IMAGE_NAME,
            mBarcodeGenerator!!.GenerateBarcode(selected_history!!.strInvoiceNumber)
        )

        baseActivityPrint.mPrinterController.printRefundTicket(
            baseActivityPrint,
            mPrintRefundReceipt
        )
    }


    private fun PrintVoidReceipt(selected_history: OrderHistoryModel?) {

        var POSName = CM.getSp(mActivity, CV.POS_NAME, "") as String
        val mPrinterSalesModel = PrinterSalesModel()

        mPrinterSalesModel.strSocialMedia =
            CM.getSp(mActivity, CV.STRSOCIALMEDIA, "").toString()
        mPrinterSalesModel.facebook = CM.getSp(mActivity, CV.STRFACEBOOK, "").toString()
        mPrinterSalesModel.instagram = CM.getSp(mActivity, CV.STRINSTAGRAM, "").toString()
        mPrinterSalesModel.twitter = CM.getSp(mActivity, CV.STRTWITTER, "").toString()
        mPrinterSalesModel.youtube = CM.getSp(mActivity, CV.STRYOUTUBE, "").toString()

        mPrinterSalesModel.setStoreName(StoreName)
        mPrinterSalesModel.setAddressLine1(StoreAddress1)
        mPrinterSalesModel.setAddressLine2(StoreAddress2)
        mPrinterSalesModel.setTelNumber(StorePhoneNum)
        mPrinterSalesModel.setStoreLogoUrl(
            CM.getSp(mActivity, CV.STORE_LOGO_URL, "").toString()
        )
        mPrinterSalesModel.setStoreLogoLocalPath(CV.STORE_LOGO_LOCAL_PATH)

        mPrinterSalesModel.setBilledPersonName(selected_history!!.strFirstName + " " + selected_history!!.strLastName)
        mPrinterSalesModel.setInvoiceNo(selected_history!!.strInvoiceNumber)

        mPrinterSalesModel.setReceiptDateTime(selected_history!!.datetimeCheckBusinessDate)
        mPrinterSalesModel.setCashierName(selected_history?.employeeData?.get(0)?.strFirstName)
        mPrinterSalesModel.setPOSStationNo(POSName)
        mPrinterSalesModel.setEmpRole(selected_history?.employeeData?.get(0)?.strEmployeeRoleName)


        mPrinterSalesModel.setTotalQty(mBinding.tvTotalQty.text.toString())

        mPrinterSalesProducts.clear()
        for (i in selected_history!!.orderProducts!!.indices) {
            val mPrinterSalesProductsModel = PrinterSalesProductsModel()

            mPrinterSalesProductsModel.setProductName(selected_history!!.orderProducts!!.get(i).strProductName)
            mPrinterSalesProductsModel.setTotal(
                currency_symbol + " " + selected_history!!.orderProducts!!.get(
                    i
                ).dblAmount
            )
            mPrinterSalesProductsModel.setQuanity(selected_history!!.orderProducts!!.get(i).dblItemQty)
            mPrinterSalesProducts.add(mPrinterSalesProductsModel)
        }

        mPrinterSalesModel.setSalesProducts(mPrinterSalesProducts)

        var baseActivityPrint = (activity as ParentActivity)

        if (baseActivityPrint.getBitmapFromLocal(
                mPrinterSalesModel.getStoreLogoLocalPath(),
                200
            ) != null
        ) {
            mImagesCache.addImageToWarehouse(
                SQUARE_IMAGE_NAME,
                baseActivityPrint.getBitmapFromLocal(
                    mPrinterSalesModel.getStoreLogoLocalPath(),
                    200
                )!!
            )
        }

        mImagesCache.addImageToWarehouse(
            BARCODE_IMAGE_NAME,
            mBarcodeGenerator!!.GenerateBarcode(selected_history!!.strInvoiceNumber)
        )


        baseActivityPrint.mPrinterController.printVoidOrderTicket(
            baseActivityPrint,
            mPrinterSalesModel
        )
    }

    fun historyHoldOrderFilter(
        inputList: List<OrderHistoryModel>,
        search: String
    ): ArrayList<OrderHistoryModel> {
        val filteredList = ArrayList<OrderHistoryModel>()
        inputList.forEach { orderHistoryModel ->
            var tempDate = orderHistoryModel.datetimeCheckBusinessDate.split(" ")
            var date = tempDate[0]

            var customerName =
                orderHistoryModel.strFirstName + " " + orderHistoryModel.strLastName

            if (customerName.contains(search, ignoreCase = true) ||
                date.contains(search, ignoreCase = true)
            ) {
                filteredList.add(orderHistoryModel)
            }
        }

        Log.e(TAG, "filteredList: " + Gson().toJson(filteredList))

        return filteredList
    }

    private fun openSearchOrderHistoryDialog() {
        search = ""
        dialogorder = Dialog(mActivity)
        dialogorder.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogorder.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogorder.getWindow()!!.setCallback(
            UserInteractionAwareCallback(
                dialogorder.getWindow()!!.getCallback(),
                mActivity
            )
        )
        dialogorder.setContentView(R.layout.dialog_search_order)

        dialogorder.setCancelable(true)
        dialogorder.show()

        val btnOpen = dialogorder.findViewById<Button>(R.id.btnOpen)
        val btnToday = dialogorder.findViewById<Button>(R.id.btnToday)
        val btnAll = dialogorder.findViewById<Button>(R.id.btnAll)
        val btnVoid = dialogorder.findViewById<Button>(R.id.btnVoid)
        val btnInprocess = dialogorder.findViewById<Button>(R.id.btnInprocess)
        val btnSearch = dialogorder.findViewById<AppCompatButton>(R.id.btn_search_history)
        //  val rvOrder = dialogorder.findViewById<RecyclerView>(R.id.rvOrder)
        // val tvNodataFound = dialogorder.findViewById<TextView>(R.id.tvNodataFound)
        val edtSearchOrder = dialogorder.findViewById<EditText>(R.id.edtSearchOrder)
        rvOrder = dialogorder.findViewById<RecyclerView>(R.id.rvOrder)
        tvNodataFound = dialogorder.findViewById<TextView>(R.id.tvNodataFound)
        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.WS_DATE_FORMATTE)

        var tempOrderHistoryList = ArrayList<OrderHistoryModel>()

        btnSearch.setOnClickListener {
            search = edtSearchOrder.text.toString()
            last_text_edit = System.currentTimeMillis().toInt()

            val imm =
                context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(it.windowToken, 0)
            Log.e(TAG, "DATE: " + sdf.format(date))
            currentPageNo = 0
            if (history_type.equals(AppConstant.HistoryType.TODAY.value)) {
                loadFirstPage(
                    sdf.format(date),
                    sdf.format(date),
                    history_type,
                    rvOrder,
                    tvNodataFound,
                    search,
                    history_status
                )
            } else {
                loadFirstPage(
                    "",
                    "",
                    history_type,
                    rvOrder,
                    tvNodataFound,
                    search,
                    history_status
                )
            }
            /*if (btnOpen.isSelected) {
                orderHistoryList = mDatabase.daoOrerHistoryList()
                    .getHoldOrderHistory(AppConstant.ON_HOLD) as ArrayList<OrderHistoryModel>

                if (search.isEmpty()) {
                    tempOrderHistoryList = orderHistoryList
                } else {
                    tempOrderHistoryList = historyHoldOrderFilter(orderHistoryList, search)
                }

                if (tempOrderHistoryList.size > 0) {
                    rvOrder.adapter =
                        OrderHistoryListAdapter(
                            mActivity,
                            tempOrderHistoryList.reversed(),
                            this
                        )
                    rvOrder.visibility = View.VISIBLE
                    tvNodataFound.visibility = View.GONE
                } else {
                    rvOrder.visibility = View.GONE
                    tvNodataFound.visibility = View.VISIBLE
                }
            } else {
                Log.e(TAG, "DATE: " + sdf.format(date))
                currentPageNo = 0
                if (history_type.equals(AppConstant.HistoryType.TODAY.value)) {
                    loadFirstPage(
                        sdf.format(date),
                        sdf.format(date),
                        history_type,
                        rvOrder,
                        tvNodataFound,
                        search
                    )
                } else {
                    loadFirstPage(
                        "",
                        "",
                        history_type,
                        rvOrder,
                        tvNodataFound,
                        search
                    )
                }
            }*/
        }
        edtSearchOrder.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                Handler().postDelayed(Runnable {
                    if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_ENTER) {
                        edtSearchOrder.requestFocus()
                    }
                }, 10)

                true
            }
            false
        })
        edtSearchOrder.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (s.toString().isEmpty()) {
                    Log.e(TAG, "List: " + Gson().toJson(orderListFilter))

                    search = ""
                    currentPageNo = 0

                    val imm =
                        context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(edtSearchOrder.windowToken, 0)
                    if (history_type.equals(AppConstant.HistoryType.TODAY.value)) {
                        loadFirstPage(
                            sdf.format(date),
                            sdf.format(date),
                            history_type,
                            rvOrder,
                            tvNodataFound,
                            search,
                            history_status
                        )
                    } else {
                        loadFirstPage(
                            "",
                            "",
                            history_type,
                            rvOrder,
                            tvNodataFound,
                            search,
                            history_status
                        )
                    }
                    /*if (!btnOpen.isSelected) {
                        rvOrder.visibility = View.VISIBLE
                        tvNodataFound.visibility = View.GONE
                        tempHistoryList.clear()

                        if (history_type.equals(AppConstant.HistoryType.TODAY.value)) {
                            loadFirstPage(
                                sdf.format(date),
                                sdf.format(date),
                                history_type,
                                rvOrder,
                                tvNodataFound,
                                search
                            )
                        } else {
                            loadFirstPage(
                                "",
                                "",
                                history_type,
                                rvOrder,
                                tvNodataFound,
                                search
                            )
                        }
                    } else {
                        orderHistoryList = mDatabase.daoOrerHistoryList()
                            .getHoldOrderHistory(AppConstant.ON_HOLD) as ArrayList<OrderHistoryModel>

                        rvOrder.adapter =
                            OrderHistoryListAdapter(
                                mActivity,
                                orderHistoryList.reversed(),
                                this@NewSalesFragment
                            )
                        rvOrder.visibility = View.VISIBLE
                        tvNodataFound.visibility = View.GONE
                    }*/
                }
            }

            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            }
        })
        history_status=AppConstant.HistoryStatus.COMPLETEDSTATUS.value
        history_type = AppConstant.HistoryType.TODAY.value
        currentPageNo = 0
        loadFirstPage(
            sdf.format(date),
            sdf.format(date),
            AppConstant.HistoryType.TODAY.value,
            rvOrder,
            tvNodataFound,
            search,
            AppConstant.HistoryStatus.COMPLETEDSTATUS.value
        )

        fun changeButtonStatus(id: Int) {
            btnOpen.isSelected = false
            btnToday.isSelected = false
            btnAll.isSelected = false
            btnVoid.isSelected = false
            btnInprocess.isSelected = false
            when (id) {
                R.id.btnOpen -> {
                    btnOpen.isSelected = true
                }
                R.id.btnToday -> {
                    btnToday.isSelected = true
                }
                R.id.btnAll -> {
                    btnAll.isSelected = true
                }
                R.id.btnVoid -> {
                    btnVoid.isSelected = true
                }
                R.id.btnInprocess-> {
                    btnInprocess.isSelected = true
                }
            }
        }
        changeButtonStatus(R.id.btnToday)
        btnOpen.setOnClickListener {
            //showKcsDialog()
            changeButtonStatus(R.id.btnOpen)
            if (CM.isInternetAvailable(mActivity)) {

                currentPageNo = 0
                history_type = AppConstant.HistoryType.HOLD.value
                history_status=AppConstant.HistoryStatus.HOLDSTATUS.value
                loadFirstPage(
                    "",
                    "",
                    AppConstant.HistoryType.HOLD.value,
                    rvOrder,
                    tvNodataFound,
                    search,
                    AppConstant.HistoryStatus.HOLDSTATUS.value
                )
            }
            //Get Hold Order History from Local Database
            /*orderHistoryList = mDatabase.daoOrerHistoryList()
                .getHoldOrderHistory(AppConstant.ON_HOLD) as ArrayList<OrderHistoryModel>

            if (orderHistoryList != null && orderHistoryList.size > 0) {
                rvOrder.adapter =
                    OrderHistoryListAdapter(mActivity, orderHistoryList.reversed(), this)
                rvOrder.visibility = View.VISIBLE
                tvNodataFound.visibility = View.GONE
            } else {
                rvOrder.visibility = View.GONE
                tvNodataFound.visibility = View.VISIBLE
            }*/

            //dismissKcsDialog()
        }

        btnToday.setOnClickListener {
            changeButtonStatus(R.id.btnToday)
            if (CM.isInternetAvailable(mActivity)) {
                val date = Calendar.getInstance().time
                val sdf = SimpleDateFormat(CV.WS_DATE_FORMATTE)
                currentPageNo = 0
                history_type = AppConstant.HistoryType.TODAY.value
                history_status= AppConstant.HistoryStatus.COMPLETEDSTATUS.value
                loadFirstPage(
                    sdf.format(date),
                    sdf.format(date),
                    AppConstant.HistoryType.TODAY.value,
                    rvOrder,
                    tvNodataFound,
                    search,
                    AppConstant.HistoryStatus.COMPLETEDSTATUS.value
                )
            }
        }

        btnAll.setOnClickListener {
            changeButtonStatus(R.id.btnAll)


            //get order list
            if (!CM.isInternetAvailable(mActivity)) {
                //get order list offline

                val orderListLiveData = mDatabase.daoOrerHistoryList().getAllOrderList()
                orderListLiveData.observe(this, androidx.lifecycle.Observer { orderlist ->
                    if (orderlist != null) {
                        orderHistoryList = orderlist as ArrayList<OrderHistoryModel>
                        orderListFilter = orderlist
                        rvOrder.visibility = View.VISIBLE
                        tvNodataFound.visibility = View.GONE
                        rvOrder.adapter =
                            OrderHistoryListAdapter(mActivity, orderHistoryList, this)

                    } else {
                        rvOrder.visibility = View.GONE
                        tvNodataFound.visibility = View.VISIBLE
                    }
                })
            } else {
                currentPageNo = 0
                history_type = AppConstant.HistoryType.ALL.value
                history_status = AppConstant.HistoryStatus.COMPLETEDSTATUS.value
                loadFirstPage(
                    "",
                    "",
                    AppConstant.HistoryType.ALL.value,
                    rvOrder,
                    tvNodataFound,
                    search,
                    AppConstant.HistoryStatus.COMPLETEDSTATUS.value
                )
            }
        }
        btnVoid.setOnClickListener {
            changeButtonStatus(R.id.btnVoid)
            currentPageNo = 0
            history_type = AppConstant.HistoryType.VOID.value
            history_status= AppConstant.HistoryStatus.VOIDSTATUS.value
            loadFirstPage(
                "",
                "",
                AppConstant.HistoryType.VOID.value,
                rvOrder,
                tvNodataFound,
                search,
                AppConstant.HistoryStatus.VOIDSTATUS.value
            )
        }
        btnInprocess.setOnClickListener {
            changeButtonStatus(R.id.btnInprocess)
            currentPageNo = 0
            history_type = AppConstant.HistoryType.INPROCESS.value
            history_status= AppConstant.HistoryStatus.INPROCESSSTATUS.value
            loadFirstPage(
                "",
                "",
                AppConstant.HistoryType.INPROCESS.value,
                rvOrder,
                tvNodataFound,
                search,
                AppConstant.HistoryStatus.INPROCESSSTATUS.value
            )
        }
    }

    private fun round(n: Int): Int { // Smaller multiple
        return (n + 4) / 5 * 5
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_new_sales,
            container, false
        )

        return mBinding.root
    }

    private val input_finish_checker = Runnable {
        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.WS_DATE_FORMATTE)
        if (System.currentTimeMillis() > last_text_edit + delay - 500) {
            currentPageNo = 0
            if (history_type.equals(AppConstant.HistoryType.TODAY.value)) {
                loadFirstPage(
                    sdf.format(date),
                    sdf.format(date),
                    history_type,
                    rvOrder,
                    tvNodataFound,
                    search,
                    history_status
                )
            } else {
                loadFirstPage(
                    "",
                    "",
                    history_type,
                    rvOrder,
                    tvNodataFound,
                    search,
                    history_status
                )
            }
        }
    }

    private fun openChangePasscodeDialog() {
        val dialogchangempin = Dialog(mActivity)
        dialogchangempin.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogchangempin.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogchangempin.window!!.setCallback(
                UserInteractionAwareCallback(
                        dialogchangempin.window!!.getCallback(),
                        mActivity
                )
        )
        dialogchangempin.setContentView(R.layout.dialog_change_mpin)

        dialogchangempin.setCancelable(false)
        dialogchangempin.show()

        val tvUserNameLogin = dialogchangempin.findViewById<TextView>(R.id.tvUserNameLogin)
        tvUserNameLogin.text = "User: " + emp_name
        val mKeyBoardLayout =
            dialogchangempin.findViewById<KeyBoardLayout>(R.id.keyboard_layout)


        val edtCurrentMPin =
            dialogchangempin.findViewById<NumberDecimalEditText>(R.id.edtCurrentMPin)
        val edtNewMPin = dialogchangempin.findViewById<NumberDecimalEditText>(R.id.edtNewMPin)
        val edtReEnterMpin =
            dialogchangempin.findViewById<NumberDecimalEditText>(R.id.edtReEnterMpin)
        mKeyBoardLayout.addKeyBoardCallback(edtCurrentMPin)
        mKeyBoardLayout.addKeyBoardCallback(edtNewMPin)
        mKeyBoardLayout.addKeyBoardCallback(edtReEnterMpin)

        edtCurrentMPin.setTypeface(Typeface.DEFAULT)
        edtNewMPin.setTypeface(Typeface.DEFAULT)
        edtReEnterMpin.setTypeface(Typeface.DEFAULT)

        edtCurrentMPin.requestFocus()
        edtCurrentMPin.setFocusable(true)
        dialogchangempin.getWindow()
            ?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        mKeyBoardLayout.setDrawLine(true)
        mKeyBoardLayout.showKeyBoard()
        val btnSubmit = dialogchangempin.findViewById<Button>(R.id.btnSubmit)
        btnSubmit.setText("Confirm")
        // val btnFinish = dialogchangempin.findViewById<Button>(R.id.btnFinish)
        btnSubmit.setOnClickListener {
            if (TextUtils.isEmpty(edtCurrentMPin.text.toString())) {
                showToast(
                    mActivity,
                    mActivity.getString(R.string.enter_current_mpin),
                    R.drawable.icon,
                    custom_toast_container
                )
            } else if (TextUtils.isEmpty(edtNewMPin.text.toString())) {
                showToast(
                    mActivity,
                    mActivity.getString(R.string.enter_new_mpin),
                    R.drawable.icon,
                    custom_toast_container
                )
            } else if (TextUtils.isEmpty(edtReEnterMpin.text.toString())) {
                showToast(
                    mActivity,
                    mActivity.getString(R.string.enter_confirm_mpin),
                    R.drawable.icon,
                    custom_toast_container
                )
            } else if (!edtCurrentMPin.text.toString().trim().length.equals(4) ||
                !edtNewMPin.text.toString().trim().length.equals(4) ||
                !edtReEnterMpin.text.toString().trim().length.equals(4)
            ) {
                showToast(
                    mActivity,
                    mActivity.getString(R.string.invalid_mpin),
                    R.drawable.icon,
                    custom_toast_container
                )
            } else if (!edtNewMPin.text.toString().equals(edtReEnterMpin.text.toString())) {
                showToast(
                    mActivity,
                    mActivity.getString(R.string.mpin_not_match),
                    R.drawable.icon,
                    custom_toast_container
                )
            } else {
                if (!CM.isInternetAvailable(mActivity)) {
                    CM.showMessageOK(
                        mActivity,
                        "",
                        resources.getString(R.string.msg_network_error),
                        null
                    )

                } else {
                    webCallChangeMpin(
                        edtReEnterMpin.text.toString(),
                        edtCurrentMPin.text.toString(),
                        dialogchangempin
                    )
                }
            }
        }

        val ivCancel = dialogchangempin.findViewById<ImageView>(R.id.ivCancel)
        ivCancel.setOnClickListener {
            dialogchangempin.dismiss()
        }
    }

    private fun webCallChangeMpin(mpin: String, iMpin_old: String, dialogchangempin: Dialog) {
        showKcsDialog()

        viewModelChangeMPin!!.changeMpin(emp_id, mpin.toInt(), iMpin_old.toInt())
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                } else {
                    showToast(
                        mActivity,
                        loginResponseModel.data!!.message,
                        R.drawable.icon,
                        custom_toast_container
                    )
                    dialogchangempin.dismiss()
                }
            })
    }

    private fun openSearchCustomerDialog() {
        dialogSearchCustomer = Dialog(mActivity)
        dialogSearchCustomer.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogSearchCustomer.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogSearchCustomer.window!!.setCallback(
                UserInteractionAwareCallback(
                        dialogSearchCustomer.window!!.getCallback(),
                        mActivity
                )
        )

        dialogSearchCustomer.setContentView(R.layout.dialog_search_customer)

        dialogSearchCustomer.setCancelable(true)
        dialogSearchCustomer.show()
        filterProduct.clear()

        val rvCustomer = dialogSearchCustomer.findViewById<RecyclerView>(R.id.rvCustomer)
        rvCustomer.adapter = CustomerListAdapter(mActivity, customerList, this)

        val edtSearchCustomer =
            dialogSearchCustomer.findViewById<AppCompatEditText>(R.id.edtSearchCustomer)
        edtSearchCustomer.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                if (s.isEmpty()) {
                    filterProduct.clear()
                    filterCustomer(s.toString(), rvCustomer)

                } else {
                    filterCustomer(s.toString(), rvCustomer)
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })
    }

    fun filterCustomer(text: String, rvCustomer: RecyclerView) {
        filterProduct.clear()
        //looping through existing elements
        for (s in customerListFilter) {
            //if the existing elements contains the search input
            var merge_first_name_last_name = "${s.strLastName} , ${s.strFirstName}"
            if (s.strFirstName.toLowerCase().contains(text.toLowerCase())
                || s.strLastName.toLowerCase().contains(text.toLowerCase())
                || s.strPhone.toLowerCase()
                    .contains(text.toLowerCase()) || merge_first_name_last_name.toLowerCase()
                    .contains(text.toLowerCase())
            ) {
                //adding the element to filtered list
                filterProduct.add(s)
            }
        }
        rvCustomer.adapter = CustomerListAdapter(mActivity, filterProduct, this)
    }

    fun filterCatAndSubCat(text: String, rvCustomer: RecyclerView) {
        val filterProduct: java.util.ArrayList<CategoryModel> =
            java.util.ArrayList<CategoryModel>()
        filterProduct.clear()
        //looping through existing elements
        for (s in categoryListFilter) {
            //if the existing elements contains the search input
            if (s.strProductCategoryName.toLowerCase().contains(text.toLowerCase())
            ) {
                //adding the element to filtered list
                filterProduct.add(s)
            }
        }
        rvCustomer.adapter = CategoryListAdapter(mActivity, filterProduct, null)
    }

    private fun doVoidCallProcedure() {

        showKcsDialog()
        resetExistingEmployee()

        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.WS_DATE_FORMATTE)
        if (itemRequestList.size > 0) {
            itemRequestList.clear()
        }
        for (i in orderTransList.indices) {
            val item = orderTransList[i]

            val turnsType =
                object : TypeToken<HashMap<String, List<IngredientMasterModel>>>() {}.type
            val modifierMap = Gson().fromJson<HashMap<String, List<IngredientMasterModel>>>(
                item.premodifierType,
                turnsType
            )
            var tempValueList = ArrayList<IngredientMasterModel>()
            var modifierReqList = ArrayList<ModifierRequestModel>()

            if (modifierMap != null) {
                for ((k, v) in modifierMap) {
                    println("$k = $v")
                    tempValueList = v as ArrayList<IngredientMasterModel>

                    for (i in tempValueList.indices) {
                        var temp: ModifierRequestModel?
                        if (k.equals(CV.SUB)) {
                            temp = ModifierRequestModel(
                                tempValueList.get(i).iModifierGroupId.toString(),
                                tempValueList.get(i).strOptionName + " Substitute With " + mDatabase.daoIngredientList()
                                    .getdefaultIngredient(
                                        tempValueList.get(i).iModifierGroupId.toString(),
                                        item.itemId
                                    ),
                                tempValueList.get(i).dblOptionPrice.toDouble(),
                                k
                            )
                        } else {
                            temp = ModifierRequestModel(
                                tempValueList.get(i).iModifierGroupId.toString(),
                                tempValueList.get(i).strOptionName,
                                tempValueList.get(i).dblOptionPrice.toDouble(),
                                k
                            )
                        }
                        modifierReqList.add(temp!!)
                    }
                }
            }

            val temp = OrderTransRequestModel(
                item.itemId.toString(),
                item.itemQty.toString(),
                item.itemRate.toString(),
                getFormatedAmount(item.itemAmount),
                item.taxPercentage,
                getFormatedAmount(item.taxAmount.toDouble()),
                item.discountAmount.toString(),
                item.iDiscountId,
                item.discountRemark,
                "",
                "",
                "",
                "",
                sdf.format(date),
                "",
                item.premodifierType,
                modifierReqList,
                false,
                item.tIsOpenDiscount,
                item.strInstruction,
                item.kitchenServedquantity
            )
            itemRequestList.add(temp)
        }
        var customerId = 0
        if (customerListViewModel?.selectedCustomer != null) {
            customerId = customerListViewModel?.selectedCustomer!!.iCustomerId
        } else {
            customerId = selected_history?.iCustomerId?.toInt() ?: 0
        }

        val discountRequestList: ArrayList<DiscountRequestModel> = ArrayList()

        discountListViewModel?.getAppliedDiscountOnCart(order_id)?.run {
            for (discountApplyModel in this) {
                val discount = DiscountRequestModel(
                    discountApplyModel.discount_amount.toString(),
                    discountApplyModel.iDiscountId.toString(),
                    discount_name,
                    discountApplyModel.type,
                    discountApplyModel.strType,
                    discountApplyModel.dblValue,
                    discountApplyModel.strAppliedTo,
                    discountApplyModel.minCartAmount
                )
                discountRequestList.add(discount)
            }
        }

        val strPaymentType = if (paymentRequestList.size > 1) {
            AppConstant.SPLIT
        } else {
            AppConstant.NORMAL
        }
        placeOrderViewModel!!.placeOrder(
            CV.server_order_id.toString(),
            order_id,
            customerId,
            emp_id,
            mBinding.tvTotalValue.text.toString().split(" ")[1].toDouble(),
            mBinding.tvTax.text.toString().split(" ")[1].toDouble(),
            mBinding.tvDiscountValue.text.toString().split(" ")[1].toDouble(),
            mBinding.tvTotal.text.toString().split(" ").get(1).toDouble(),
            Gson().toJson(itemRequestList),
            Gson().toJson(paymentRequestList),
            Gson().toJson(discountRequestList),
            "",
            "1",
            checkTaxIcluded(),
            strPaymentType,
            AppConstant.HistoryStatus.VOIDSTATUS.value
        )
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                } else {
                    showToast(
                        mActivity,
                        loginResponseModel.data!!.message,
                        R.drawable.icon,
                        custom_toast_container
                    )

                    doResetCart()
                    //Hardware Code For CDS Display -- NewSale While Order voided case invoke
                    DisplayNewSaleDataToCDS(
                        ArrayList<OrderTransModel>(),
                        "This order has been voided.",
                        false
                    )
                }
            })
    }

    private fun webCall_Order_generate() {
        mBinding.gridItem.isEnabled=false
        mBinding.btnCheckout.isEnabled=false
        showKcsDialog()
        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.DISPLAY_DATE_FORMATTE)

        val discountRequestList: ArrayList<DiscountRequestModel> = ArrayList()


        discountListViewModel?.getAppliedDiscountOnCart(order_id)?.run {
            for (discountApplyModel in this) {
                val discount = DiscountRequestModel(
                    mBinding.tvDiscountValue.text.toString().split(" ")[1],
                    discountApplyModel.iDiscountId.toString(),
                    discount_name,
                    discountApplyModel.type,
                    discountApplyModel.strType,
                    discountApplyModel.dblValue,
                    discountApplyModel.strAppliedTo,
                    discountApplyModel.minCartAmount
                )
                discountRequestList.add(discount)
            }
        }


        var customerId = 0
        if (customerListViewModel?.selectedCustomer != null) {
            customerId = customerListViewModel?.selectedCustomer!!.iCustomerId
        } else {
            customerId = selected_history?.iCustomerId?.toInt() ?: 0
        }

        val strPaymentType = if (paymentRequestList.size > 1) {
            AppConstant.SPLIT
        } else {
            AppConstant.NORMAL
        }
        placeOrderViewModel!!.placeOrder(
            CV.server_order_id.toString(),
            order_id,
            customerId,
            if (trasaction_emp_id == 0) emp_id else trasaction_emp_id,
            mBinding.tvTotalValue.text.toString().split(" ")[1].toDouble(),
            mBinding.tvTax.text.toString().split(" ")[1].toDouble(),
            mBinding.tvDiscountValue.text.toString().split(" ")[1].toDouble(),
            mBinding.tvTotal.text.toString().split(" ")[1].toDouble(),
            Gson().toJson(itemRequestList),
            Gson().toJson(paymentRequestList),
            Gson().toJson(discountRequestList),
            "",
            "0",
            checkTaxIcluded(),
            strPaymentType,
            AppConstant.HistoryStatus.INPROCESSSTATUS.value
        )?.observe(this, androidx.lifecycle.Observer { NewSaleandRefundModel ->
            //mBinding.gridItem.isEnabled=true
            dismissKcsDialog()

            if (NewSaleandRefundModel.data == null) {
                LogUtil.d("already_done",NewSaleandRefundModel.message)
                if(TextUtils.equals(NewSaleandRefundModel.message,getString(R.string.order_already_generated_msg)))
                {
                    DialogUtility.openSingleActionDialog(
                        getString(R.string.order_already_generated_msg),
                        "Close",
                        mActivity
                    ) {
                        if (it) {
                            doResetCart()
                        }
                    }
                }else {
                    DialogUtility.openActionDialogwithMessage_Retry_Cancel_Btn(
                        getString(R.string.internet_or_server_msg),
                        mActivity
                    ) {
                        if (it) {
                            mBinding.btnCheckout.isEnabled = true
                            mBinding.btnCheckout.performClick()
                        } else {
                            mBinding.gridItem.isEnabled = true
                            mBinding.btnCheckout.isEnabled = true
                        }
                    }
                }
            } else {
                InvoiceNumber = NewSaleandRefundModel.data!!.data.strInvoiceNumber
                CV.server_order_id = NewSaleandRefundModel.data!!.data.order_id
                CV.ORDER_DATE = NewSaleandRefundModel.data!!.data.datetimeCheckBusinessDate
                openNewCheckOutDialog()
                /*NewSaleandRefundModel.data?.let {
                    InvoiceNumber = it.data.strInvoiceNumber
                    payment_mode = it.data.orderPayments?.get(0)?.strPaymentMethod.toString()
                }

                PrintKitchenReceipt(emp_name, emp_role, AppConstant.ORDER)


                paymentViewModel.resetAuthCard()
                payment_type = CM.getSp(mActivity, CV.PAYMENT_TYPE, AppConstant.CASH) as String
                paymentViewModel.selectedPaymentMode = payment_type*/
                //CDS Payment Process Done Screen Invoke

                //isPrint = false
            }
        })
    }

    private fun webCall_update_payment_type() {
        //showKcsDialog()

        val strPaymentType = if (paymentRequestList.size > 0) {
            paymentRequestList.joinToString { it.strPaymentMethod }+","+payment_type
        } else {
            payment_type
        }
        placeOrderViewModel!!.Update_Payment_type(
            CV.server_order_id.toString(),
            strPaymentType
        )?.observe(this, androidx.lifecycle.Observer {
            //dismissKcsDialog()
            if (it.data == null) {
                if (TextUtils.equals(
                        it.message,
                        getString(R.string.order_already_generated_msg)
                    )
                ) {
                    DialogUtility.openSingleActionDialog(
                        getString(R.string.order_already_generated_msg),
                        "Close",
                        mActivity
                    ) {
                        if (it) {
                            dismissOpenNewCheckOutDialog()
                            dismissPaymentProgressDialog()
                            doResetCart()
                        }
                    }
                }
            }
        })
    }

    /*private fun webCallPlaceOrder() {
        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.DISPLAY_DATE_FORMATTE)

        val discountRequestList: ArrayList<DiscountRequestModel> = ArrayList()


        discountListViewModel?.getAppliedDiscountOnCart(order_id)?.run {
            for (discountApplyModel in this) {
                val discount = DiscountRequestModel(
                    mBinding.tvDiscountValue.text.toString().split(" ")[1],
                    discountApplyModel.iDiscountId.toString(),
                    discount_name,
                    discountApplyModel.type,
                    discountApplyModel.strType,
                    discountApplyModel.dblValue,
                    discountApplyModel.strAppliedTo,
                    discountApplyModel.minCartAmount
                )
                discountRequestList.add(discount)
            }
        }

        showPaymentProgressDialog()
        paymentViewModel.getPaymentStatusObserver.value =
            AppConstant.PaymentProcess.LOADING.value
        updateProgressDialogTitle(getTotalCartMsg())

        var customerId = 0
        if (customerListViewModel?.selectedCustomer != null) {
            customerId = customerListViewModel?.selectedCustomer!!.iCustomerId
        } else {
            customerId = selected_history?.iCustomerId?.toInt() ?: 0
        }

        val strPaymentType = if (paymentRequestList.size > 1) {
            AppConstant.SPLIT
        } else {
            AppConstant.NORMAL
        }
        placeOrderViewModel!!.placeOrder(
            order_id,
            customerId,
            if (trasaction_emp_id == 0) emp_id else trasaction_emp_id,
            mBinding.tvTotalValue.text.toString().split(" ")[1].toDouble(),
            mBinding.tvTax.text.toString().split(" ")[1].toDouble(),
            mBinding.tvDiscountValue.text.toString().split(" ")[1].toDouble(),
            mBinding.tvTotal.text.toString().split(" ")[1].toDouble(),
            Gson().toJson(itemRequestList),
            Gson().toJson(paymentRequestList),
            Gson().toJson(discountRequestList),
            "",
            "0",
            checkTaxIcluded(),
            strPaymentType
        )?.observe(this, androidx.lifecycle.Observer { NewSaleandRefundModel ->

            if (NewSaleandRefundModel.data == null) {
                paymentViewModel.getPaymentStatusObserver.value =
                    AppConstant.PaymentProcess.FAILED.value
            } else {
                paymentViewModel.getPaymentStatusObserver.value =
                    AppConstant.PaymentProcess.SUCCESS.value


                InvoiceNumber = NewSaleandRefundModel.data!!.data.strInvoiceNumber
                CV.server_order_id = NewSaleandRefundModel.data!!.data.order_id
                CV.ORDER_DATE = NewSaleandRefundModel.data!!.data.datetimeCheckBusinessDate

                NewSaleandRefundModel.data?.let {
                    InvoiceNumber = it.data.strInvoiceNumber
                    payment_mode = it.data.orderPayments?.get(0)?.strPaymentMethod.toString()
                }

                PrintKitchenReceipt(emp_name, emp_role, AppConstant.ORDER)
                // CV.STR_RET_REF = temp.strRetRef

                paymentViewModel.resetAuthCard()
                payment_type = CM.getSp(mActivity, CV.PAYMENT_TYPE, AppConstant.CASH) as String
                paymentViewModel.selectedPaymentMode = payment_type
                //CDS Payment Process Done Screen Invoke

                //isPrint = false
            }
        })
    }*/

    private fun webCallUpdateorderpayment() {


        showPaymentProgressDialog()
        paymentViewModel.getPaymentStatusObserver.value =
            AppConstant.PaymentProcess.LOADING.value
        updateProgressDialogTitle(getTotalCartMsg())
        val strPaymentType = if (paymentRequestList.size > 1) {
            AppConstant.SPLIT
        } else {
            AppConstant.NORMAL
        }
        placeOrderViewModel!!.Update_order_payment(
            CV.server_order_id.toString(),
            Gson().toJson(paymentRequestList),
            strPaymentType
        )?.observe(this, androidx.lifecycle.Observer { NewSaleandRefundModel ->

            if (NewSaleandRefundModel.data == null) {
                if(TextUtils.equals(NewSaleandRefundModel.message,getString(R.string.order_already_generated_msg))) {
                    DialogUtility.openSingleActionDialog(
                        getString(R.string.order_already_generated_msg),
                        "Close",
                        mActivity
                    ) {
                        if (it) {
                            dismissOpenNewCheckOutDialog()
                            dismissPaymentProgressDialog()
                            doResetCart()
                        }
                    }
                }else {
                    paymentViewModel.getPaymentStatusObserver.value =
                        AppConstant.PaymentProcess.FAILED.value
                }
            } else {
                paymentViewModel.getPaymentStatusObserver.value =
                    AppConstant.PaymentProcess.SUCCESS.value


                InvoiceNumber = NewSaleandRefundModel.data!!.data.strInvoiceNumber
                CV.server_order_id = NewSaleandRefundModel.data!!.data.order_id
                CV.ORDER_DATE = NewSaleandRefundModel.data!!.data.datetimeCheckBusinessDate

                NewSaleandRefundModel.data?.let {
                    InvoiceNumber = it.data.strInvoiceNumber
                    payment_mode = it.data.orderPayments?.get(0)?.strPaymentMethod.toString()
                }

                PrintKitchenReceipt(emp_name, emp_role, AppConstant.ORDER)
                // CV.STR_RET_REF = temp.strRetRef

                paymentViewModel.resetAuthCard()
                payment_type = CM.getSp(mActivity, CV.PAYMENT_TYPE, AppConstant.CASH) as String
                paymentViewModel.selectedPaymentMode = payment_type
                //CDS Payment Process Done Screen Invoke

                //isPrint = false
            }
        })
    }

    private fun webCallCheckLastPayment() {
        var orderId = temp_order_id
        showPaymentProgressDialog()
        paymentViewModel.getPaymentStatusObserver.value =
            AppConstant.PaymentProcess.LOADING.value
        updateProgressDialogTitle("Payment Status...")

        paymentViewModel.checkPaymentApi(
            orderId
        )?.observe(this, androidx.lifecycle.Observer { cardResponse ->

            if (cardResponse != null && cardResponse.data != null && cardResponse.data!!.data != null) {
                var btnLastPayment =
                    dialogcheckout!!.findViewById<Button>(R.id.btnCheckLastPayment)
                btnLastPayment.visibility = View.GONE
                Btncalculate_change_flag=false
                paymentViewModel.cardResponseModel = cardResponse.data!!.data
                paymentViewModel.cardResponseModel!!.orderid = orderId

                paymentViewModel.cardResponseModel?.let { cardResponse ->
                    if (cardResponse.respcode == "000") {
                        dismissPaymentProgressDialog()
                        val paidAmt = paymentViewModel.payableAmount.toDouble() / 100
                        //orderGenerate(0.0, paidAmt.toString())
                        orderPaymentRequestWithOrderGenerate(
                            payment_type,
                            0.0,
                            paidAmt.toString()
                        )

                    } else {
                        handleError(cardResponse.resptext)
                    }
                }
            } else {
                if (cardResponse != null) {
                    cardResponse.message?.let {
                        if (!it.contains("connection abort"))
                        {
                            var btnLastPayment =
                                dialogcheckout!!.findViewById<Button>(R.id.btnCheckLastPayment)
                            btnLastPayment.visibility = View.GONE
                            Btncalculate_change_flag=false
                        }
                        handleError(it)
                    }
                } else {
                    paymentViewModel.getPaymentStatusObserver.value =
                        AppConstant.PaymentProcess.FAILED.value
                }
            }
        })

    }

    private fun isCashTransaction() =
        payment_type.toLowerCase().equals(AppConstant.CASH, ignoreCase = true)


    private fun openNewCheckOutDialog() {
        val CARD_NUMBER_TOTAL_SYMBOLS = 19 // size of pattern 0000-0000-0000-0000
        val CARD_NUMBER_TOTAL_DIGITS = 16 // max numbers of digits in pattern: 0000 x 4
        val CARD_NUMBER_DIVIDER_MODULO =
            5 // means divider position is every 5th symbol beginning with 1
        val CARD_NUMBER_DIVIDER_POSITION =
            CARD_NUMBER_DIVIDER_MODULO - 1 // means divider position is every 4th symbol beginning with 0
        val CARD_NUMBER_DIVIDER = '-'

        val CARD_DATE_TOTAL_SYMBOLS = 5 // size of pattern MM/YY
        val CARD_DATE_TOTAL_DIGITS = 4 // max numbers of digits in pattern: MM + YY
        val CARD_DATE_DIVIDER_MODULO =
            3 // means divider position is every 3rd symbol beginning with 1
        val CARD_DATE_DIVIDER_POSITION =
            CARD_DATE_DIVIDER_MODULO - 1 // means divider position is every 2nd symbol beginning with 0
        val CARD_DATE_DIVIDER = '/'

        val CARD_CVC_TOTAL_SYMBOLS = 3
        dismissOpenNewCheckOutDialog()
        dialogcheckout = Dialog(mActivity)
        dialogcheckout!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogcheckout!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogcheckout!!.window!!.setCallback(
            UserInteractionAwareCallback(
                dialogcheckout!!.window!!.getCallback(),
                mActivity
            )
        )
        dialogcheckout!!.setContentView(R.layout.dialog_payment_process)

        dialogcheckout!!.setCancelable(false)
        dialogcheckout!!.show()
        payment_type = CM.getSp(mActivity, CV.PAYMENT_TYPE, AppConstant.CASH) as String
        webCall_update_payment_type()

        val ivClearAmonut = dialogcheckout!!.findViewById<ImageView>(R.id.ivClearAmount)
        val btnCash = dialogcheckout!!.findViewById<Button>(R.id.btnCash)
        val llCash = dialogcheckout!!.findViewById<LinearLayout>(R.id.llCash)

        val edtAmount = dialogcheckout!!.findViewById<EditText>(R.id.edtAmount)
        val btnCredit = dialogcheckout!!.findViewById<Button>(R.id.btnCredit)

        val btnDebit = dialogcheckout!!.findViewById<Button>(R.id.btnDebit)
        val btnApplePay = dialogcheckout!!.findViewById<Button>(R.id.btnApplePay)
        val btnManualCard = dialogcheckout!!.findViewById<Button>(R.id.btnManualCard)
        val tvInfoMsgManualCard =
            dialogcheckout!!.findViewById<TextView>(R.id.tvInfoMsgManualCard)
        val btnSplit = dialogcheckout!!.findViewById<Button>(R.id.btnSplit)

        val btnCash1 = dialogcheckout!!.findViewById<Button>(R.id.btnCash1)
        val btnCash2 = dialogcheckout!!.findViewById<Button>(R.id.btnCash2)
        val btnCash3 = dialogcheckout!!.findViewById<Button>(R.id.btnCash3)
        val llPaymentCompleted =
            dialogcheckout!!.findViewById<LinearLayout>(R.id.llPaymentCompleted)
        llPaymentCompleted.visibility = View.GONE

        val keyboard: DateKeyboard =
            dialogcheckout!!.findViewById(R.id.keyboard) as DateKeyboard
        edtAmount.setRawInputType(InputType.TYPE_CLASS_TEXT)
        edtAmount.setTextIsSelectable(true)

        val ic = edtAmount.onCreateInputConnection(EditorInfo())
        keyboard.setInputConnection(ic)

        val tvTotalQtyValue = dialogcheckout!!.findViewById<TextView>(R.id.tvTotalQtyValue)
        val tvAmount = dialogcheckout!!.findViewById<TextView>(R.id.tvCartTotalValue)
        tvBalanceDueValue = dialogcheckout!!.findViewById<TextView>(R.id.tvBalanceDueValue)
        val btnPaymentMethod = dialogcheckout!!.findViewById<Button>(R.id.btnPaymentMethod)
        spnSplit = dialogcheckout!!.findViewById<AppCompatSpinner>(R.id.spnSplit)
        spnPaymentMethod =
            dialogcheckout!!.findViewById<AppCompatSpinner>(R.id.spnPaymentMethod)
        rvPayment = dialogcheckout!!.findViewById<RecyclerView>(R.id.rvPayment)
        if (paymentMethodList != null && paymentMethodList!!.size > 0) {
            btnPaymentMethod.visibility = View.VISIBLE
        } else {
            btnPaymentMethod.visibility = View.GONE
        }
        paymentRequestList.clear()
        paymentList.clear()
        btnSplit.isEnabled = true
        spnSplit.isEnabled = true
        val arrayAdapter = ArrayAdapter(
            mActivity,
            android.R.layout.simple_list_item_1,
            resources.getStringArray(R.array.split_type)
        )
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnSplit.adapter = arrayAdapter


        val arrayPaymentMethodAdapter = paymentMethodList?.let {
            ArrayAdapter(
                mActivity,
                android.R.layout.simple_list_item_1,
                it
            )
        }
        arrayPaymentMethodAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnPaymentMethod.adapter = arrayPaymentMethodAdapter

        rvPayment.adapter = PaymentListAdapter(mActivity, paymentList, currency_symbol)

        val btnCalculateChange = dialogcheckout!!.findViewById<Button>(R.id.btnCalculateChange)
        val btnCheckLastPayment =
            dialogcheckout!!.findViewById<Button>(R.id.btnCheckLastPayment)
        val btnPlaceOrder = dialogcheckout!!.findViewById<Button>(R.id.btnPlaceOrder)

        val ivCancel = dialogcheckout!!.findViewById<ImageView>(R.id.ivCancel)
        ivCancel.setOnClickListener {
            if (paymentRequestList.isNotEmpty()) {
                DialogUtility.openSingleActionDialog(
                    getString(R.string.payment_cancel_alert_msg),
                    "Ok",
                    mActivity,
                    "Alert!"
                ) {
                }
            }
            else if(Btncalculate_change_flag==true){
                DialogUtility.openSingleActionDialog(
                    resources.getString(R.string.calculate_change_flag_msg),
                    "OK",
                    mActivity
                ) {

                }
                return@setOnClickListener
            }
            else {
                DialogUtility.openPaymentCancellationDialog(mActivity) {
                    if (it) {
                        DisplayNewSaleDataToCDS(
                            (mBinding.rvCart.adapter as OrderItemListAdapter).getDataset(),
                            "",
                            false
                        )
                        dismissOpenNewCheckOutDialog()
                        mBinding.gridItem.isEnabled=true
                        mBinding.btnCheckout.isEnabled=true
                    }
                }
            }
        }

        val tt = mBinding.tvTotalValue.text.toString().split(" ")[1].toDouble()
        val totalQty = mBinding.tvTotalQty.text.toString()
        tvTotalQtyValue.text = totalQty
        tvAmount.formatAmount(currency_symbol, tt)
        tvBalanceDueValue.formatAmount(currency_symbol, tt)

        if (tvAmount.text.toString().split(" ")[1].toDouble() <= 0) {
            val isNotOnlyOpenDiscountedProduct = orderTransList.any { it.iDiscountId != -1 }
            if (!isNotOnlyOpenDiscountedProduct) {
                payment_type = AppConstant.CASH
                btnPaymentMethod.visibility = View.GONE
                btnManualCard.visibility = View.GONE
                tvInfoMsgManualCard.visibility = View.GONE
                btnCredit.visibility = View.GONE
                btnDebit.visibility = View.GONE
                btnApplePay.visibility = View.GONE
            }
        }

        fun manageViews() {

            if (!payment_type.equals(
                    AppConstant.MANUAL_CARD,
                    true
                ) || !NewSaleFragmentCommon.isOtherCashTrasaction(
                    paymentMethodList,
                    payment_type
                )
            ) {
                val splitByTotalCount = paymentList.filter { item -> item.isSplit }.count()
                if (spnSplit.visibility == View.VISIBLE && splitByTotalCount > 0) {
                    /*  val amount =
                          tvAmount.text.toString().split(" ")[1].toDouble() / spnSplit.selectedItem.toString().toInt()
                      edtAmount.setText(String.format("%.2f", (amount)))*/

                    keyboard.visibility = View.INVISIBLE
                    edtAmount.isEnabled = false
                    ivClearAmonut.visibility = View.GONE
                    spnSplit.isEnabled = false
                    btnSplit.isEnabled = false
                } else if (spnSplit.visibility == View.VISIBLE) {
                    keyboard.visibility = View.INVISIBLE
                    edtAmount.isEnabled = false
                    ivClearAmonut.visibility = View.GONE
                } else {
                    ivClearAmonut.visibility = View.VISIBLE
                    keyboard.visibility = View.VISIBLE
                    edtAmount.isEnabled = true
                    spnSplit.isEnabled = true
                    btnSplit.isEnabled = true
                }
            }

            if (!NewSaleFragmentCommon.isOtherCashTrasaction(paymentMethodList, payment_type)) {
                spnPaymentMethod.setSelection(0)
            }

            tvInfoMsgManualCard.visibility = View.GONE
            if (payment_type.equals(AppConstant.CASH, true)) {
                btnCash.background = mActivity.resources.getDrawable(R.drawable.btn_bg)
                btnCash.setTextColor(resources.getColor(R.color.colorWhite))

                btnPaymentMethod.setTextColor(resources.getColor(R.color.colorPrimary))
                btnPaymentMethod.background = resources.getDrawable(R.drawable.edt_bg)

                btnManualCard.setTextColor(resources.getColor(R.color.colorPrimary))
                btnManualCard.background = resources.getDrawable(R.drawable.edt_bg)

                btnCredit.setTextColor(resources.getColor(R.color.colorPrimary))
                btnCredit.background = resources.getDrawable(R.drawable.edt_bg)

                btnDebit.setTextColor(resources.getColor(R.color.colorPrimary))
                btnDebit.background = resources.getDrawable(R.drawable.edt_bg)

                btnApplePay.setTextColor(resources.getColor(R.color.colorPrimary))
                btnApplePay.background = resources.getDrawable(R.drawable.edt_bg)

                if (tvTotal.text.toString().split(" ")[1].toDouble() <= 0) {
                    val isNotOnlyOpenDiscountedProduct =
                        orderTransList.any { it.iDiscountId != -1 }
                    if (!isNotOnlyOpenDiscountedProduct) {
                        btnCredit.visibility = View.GONE
                        btnDebit.visibility = View.GONE
                        btnApplePay.visibility = View.GONE
                        btnPaymentMethod.visibility = View.GONE
                        btnManualCard.visibility = View.GONE
                    }
                }
                if (spnSplit.visibility == View.VISIBLE) {
                    llCash.visibility = View.INVISIBLE
                } else {
                    llCash.visibility = View.VISIBLE
                }
            } else if (NewSaleFragmentCommon.isOtherCashTrasaction(
                    paymentMethodList,
                    payment_type
                )
            ) {
                btnPaymentMethod.background = mActivity.resources.getDrawable(R.drawable.btn_bg)
                btnPaymentMethod.setTextColor(resources.getColor(R.color.colorWhite))

                btnCash.setTextColor(resources.getColor(R.color.colorPrimary))
                btnCash.background = resources.getDrawable(R.drawable.edt_bg)

                btnManualCard.setTextColor(resources.getColor(R.color.colorPrimary))
                btnManualCard.background = resources.getDrawable(R.drawable.edt_bg)

                btnCredit.setTextColor(resources.getColor(R.color.colorPrimary))
                btnCredit.background = resources.getDrawable(R.drawable.edt_bg)

                btnDebit.setTextColor(resources.getColor(R.color.colorPrimary))
                btnDebit.background = resources.getDrawable(R.drawable.edt_bg)

                btnApplePay.setTextColor(resources.getColor(R.color.colorPrimary))
                btnApplePay.background = resources.getDrawable(R.drawable.edt_bg)

                llCash.visibility = View.INVISIBLE

                edtAmount.isEnabled = false
                keyboard.visibility = View.INVISIBLE
                ivClearAmonut.visibility = View.GONE
                spnSplit.isEnabled = false
                btnSplit.isEnabled = false
                val amount =
                    tvBalanceDueValue.text.toString()
                        .split(" ")[1].toDouble()
                edtAmount.setText(String.format("%.2f", (amount)))
                spnPaymentMethod.performClick()
            } else if (payment_type.equals(AppConstant.CREDIT, true)) {
                btnCredit.background = mActivity.resources.getDrawable(R.drawable.btn_bg)
                btnCredit.setTextColor(resources.getColor(R.color.colorWhite))

                btnCash.setTextColor(resources.getColor(R.color.colorPrimary))
                btnCash.background = resources.getDrawable(R.drawable.edt_bg)

                btnPaymentMethod.setTextColor(resources.getColor(R.color.colorPrimary))
                btnPaymentMethod.background = resources.getDrawable(R.drawable.edt_bg)

                btnManualCard.setTextColor(resources.getColor(R.color.colorPrimary))
                btnManualCard.background = resources.getDrawable(R.drawable.edt_bg)

                btnDebit.setTextColor(resources.getColor(R.color.colorPrimary))
                btnDebit.background = resources.getDrawable(R.drawable.edt_bg)

                btnApplePay.setTextColor(resources.getColor(R.color.colorPrimary))
                btnApplePay.background = resources.getDrawable(R.drawable.edt_bg)

                llCash.visibility = View.INVISIBLE
            } else if (payment_type.equals(AppConstant.DEBIT, true)) {
                btnDebit.background = mActivity.resources.getDrawable(R.drawable.btn_bg)
                btnDebit.setTextColor(resources.getColor(R.color.colorWhite))

                btnCash.setTextColor(resources.getColor(R.color.colorPrimary))
                btnCash.background = resources.getDrawable(R.drawable.edt_bg)

                btnCredit.setTextColor(resources.getColor(R.color.colorPrimary))
                btnCredit.background = resources.getDrawable(R.drawable.edt_bg)

                btnManualCard.setTextColor(resources.getColor(R.color.colorPrimary))
                btnManualCard.background = resources.getDrawable(R.drawable.edt_bg)

                btnApplePay.setTextColor(resources.getColor(R.color.colorPrimary))
                btnApplePay.background = resources.getDrawable(R.drawable.edt_bg)

                btnPaymentMethod.setTextColor(resources.getColor(R.color.colorPrimary))
                btnPaymentMethod.background = resources.getDrawable(R.drawable.edt_bg)

                llCash.visibility = View.INVISIBLE
            } else if (payment_type.equals(AppConstant.APPLE_PAY, true)) {
                btnApplePay.background = mActivity.resources.getDrawable(R.drawable.btn_bg)
                btnApplePay.setTextColor(resources.getColor(R.color.colorWhite))

                btnCash.setTextColor(resources.getColor(R.color.colorPrimary))
                btnCash.background = resources.getDrawable(R.drawable.edt_bg)

                btnCredit.setTextColor(resources.getColor(R.color.colorPrimary))
                btnCredit.background = resources.getDrawable(R.drawable.edt_bg)

                btnDebit.setTextColor(resources.getColor(R.color.colorPrimary))
                btnDebit.background = resources.getDrawable(R.drawable.edt_bg)

                btnManualCard.setTextColor(resources.getColor(R.color.colorPrimary))
                btnManualCard.background = resources.getDrawable(R.drawable.edt_bg)

                btnPaymentMethod.setTextColor(resources.getColor(R.color.colorPrimary))
                btnPaymentMethod.background = resources.getDrawable(R.drawable.edt_bg)

                llCash.visibility = View.INVISIBLE
            } else if (payment_type.equals(AppConstant.MANUAL_CARD, true)) {
                btnManualCard.background = mActivity.resources.getDrawable(R.drawable.btn_bg)
                btnManualCard.setTextColor(resources.getColor(R.color.colorWhite))

                btnCash.setTextColor(resources.getColor(R.color.colorPrimary))
                btnCash.background = resources.getDrawable(R.drawable.edt_bg)

                btnPaymentMethod.setTextColor(resources.getColor(R.color.colorPrimary))
                btnPaymentMethod.background = resources.getDrawable(R.drawable.edt_bg)

                btnCredit.setTextColor(resources.getColor(R.color.colorPrimary))
                btnCredit.background = resources.getDrawable(R.drawable.edt_bg)

                btnDebit.setTextColor(resources.getColor(R.color.colorPrimary))
                btnDebit.background = resources.getDrawable(R.drawable.edt_bg)

                btnApplePay.setTextColor(resources.getColor(R.color.colorPrimary))
                btnApplePay.background = resources.getDrawable(R.drawable.edt_bg)

                tvInfoMsgManualCard.visibility = View.VISIBLE
                llCash.visibility = View.INVISIBLE

                edtAmount.isEnabled = false
                keyboard.visibility = View.INVISIBLE
                ivClearAmonut.visibility = View.GONE
                //  spnSplit.visibility = View.INVISIBLE
                //btnSplit.setTextColor(resources.getColor(R.color.colorPrimary))
                // btnSplit.background = resources.getDrawable(R.drawable.edt_bg)
                spnSplit.isEnabled = false
                btnSplit.isEnabled = false
                val amount =
                    tvBalanceDueValue.text.toString()
                        .split(" ")[1].toDouble()
                edtAmount.setText(String.format("%.2f", (amount)))
            } else {
                btnCash.background = mActivity.resources.getDrawable(R.drawable.btn_bg)
                btnCash.setTextColor(resources.getColor(R.color.colorWhite))

                btnCredit.setTextColor(resources.getColor(R.color.colorPrimary))
                btnCredit.background = resources.getDrawable(R.drawable.edt_bg)

                btnDebit.setTextColor(resources.getColor(R.color.colorPrimary))
                btnDebit.background = resources.getDrawable(R.drawable.edt_bg)

                btnApplePay.setTextColor(resources.getColor(R.color.colorPrimary))
                btnApplePay.background = resources.getDrawable(R.drawable.edt_bg)

                btnManualCard.setTextColor(resources.getColor(R.color.colorPrimary))
                btnManualCard.background = resources.getDrawable(R.drawable.edt_bg)

                btnPaymentMethod.setTextColor(resources.getColor(R.color.colorPrimary))
                btnPaymentMethod.background = resources.getDrawable(R.drawable.edt_bg)

                if (tvAmount.text.toString().split(" ")[1].toDouble() <= 0) {
                    val isNotOnlyOpenDiscountedProduct =
                        orderTransList.any { it.iDiscountId != -1 }
                    if (!isNotOnlyOpenDiscountedProduct) {
                        btnCredit.visibility = View.GONE
                        btnDebit.visibility = View.GONE
                        btnApplePay.visibility = View.GONE
                        btnPaymentMethod.visibility = View.GONE
                        btnSplit.visibility = View.GONE
                    }

                }
                llCash.visibility = View.VISIBLE
            }
        }
        edtAmount.addTextChangedListener(GenericTextWatcher(edtAmount))

        ivClearAmonut.setOnClickListener {
            edtAmount.setText("")
            edtAmount.requestFocus()
            edtAmount.isFocusable = true
        }

        edtAmount.setText("${String.format("%.2f", (tt))}")
        manageViews()

        btnCash1.text = currency_symbol + " " + format("%.2f", strAmount[0].toDouble())
        btnCash2.text = currency_symbol + " " + format("%.2f", strAmount[1].toDouble())
        btnCash3.text = currency_symbol + " " + format("%.2f", strAmount[2].toDouble())

        btnCash1.setOnClickListener {
            edtAmount.setText(btnCash1.text.toString().split(" ")[1])
        }

        btnCash2.setOnClickListener {
            edtAmount.setText(btnCash2.text.toString().split(" ").get(1))
        }

        btnCash3.setOnClickListener {
            edtAmount.setText(btnCash3.text.toString().split(" ").get(1))
        }

        btnCash.setOnClickListener {
            payment_type = AppConstant.CASH
            manageViews()
            if (spnSplit.visibility == View.VISIBLE && spnSplit.isEnabled==true) {
                var strPaymentType_ar=ArrayList<String>()
                for (i in 0..spnSplit.selectedItem.toString().toInt()-1)
                {
                    strPaymentType_ar.add(payment_type)
                }
                placeOrderViewModel!!.Update_Payment_type(
                    CV.server_order_id.toString(),
                    strPaymentType_ar.joinToString()
                )?.observe(mActivity, androidx.lifecycle.Observer {
                    //dismissKcsDialog()

                })

            }else {
                webCall_update_payment_type()
            }
        }
        btnPaymentMethod.setOnClickListener {
            var paymenData = spnPaymentMethod.selectedItem as PaymentMethodModelData
            payment_type = paymenData.strPaymentMethod
            manageViews()
            if (spnSplit.visibility == View.VISIBLE && spnSplit.isEnabled==true) {
                var strPaymentType_ar=ArrayList<String>()
                for (i in 0..spnSplit.selectedItem.toString().toInt()-1)
                {
                    strPaymentType_ar.add(payment_type)
                }
                placeOrderViewModel!!.Update_Payment_type(
                    CV.server_order_id.toString(),
                    strPaymentType_ar.joinToString()
                )?.observe(mActivity, androidx.lifecycle.Observer {
                    //dismissKcsDialog()

                })

            }else {
                webCall_update_payment_type()
            }
        }
        btnCredit.setOnClickListener {
            payment_type = AppConstant.CREDIT
            manageViews()
            if (spnSplit.visibility == View.VISIBLE && spnSplit.isEnabled==true) {
                var strPaymentType_ar=ArrayList<String>()
                for (i in 0..spnSplit.selectedItem.toString().toInt()-1)
                {
                    strPaymentType_ar.add(payment_type)
                }
                placeOrderViewModel!!.Update_Payment_type(
                    CV.server_order_id.toString(),
                    strPaymentType_ar.joinToString()
                )?.observe(mActivity, androidx.lifecycle.Observer {
                    //dismissKcsDialog()

                })

            }else {
                webCall_update_payment_type()
            }
        }
        btnManualCard.setOnClickListener {
            payment_type = AppConstant.MANUAL_CARD
            manageViews()
            if (spnSplit.visibility == View.VISIBLE && spnSplit.isEnabled==true) {
                var strPaymentType_ar=ArrayList<String>()
                for (i in 0..spnSplit.selectedItem.toString().toInt()-1)
                {
                    strPaymentType_ar.add(payment_type)
                }
                placeOrderViewModel!!.Update_Payment_type(
                    CV.server_order_id.toString(),
                    strPaymentType_ar.joinToString()
                )?.observe(mActivity, androidx.lifecycle.Observer {
                    //dismissKcsDialog()

                })

            }else {
                webCall_update_payment_type()
            }
        }

        btnApplePay.setOnClickListener {
            payment_type = AppConstant.APPLE_PAY
            manageViews()
            if (spnSplit.visibility == View.VISIBLE && spnSplit.isEnabled==true) {
                var strPaymentType_ar=ArrayList<String>()
                for (i in 0..spnSplit.selectedItem.toString().toInt()-1)
                {
                    strPaymentType_ar.add(payment_type)
                }
                placeOrderViewModel!!.Update_Payment_type(
                    CV.server_order_id.toString(),
                    strPaymentType_ar.joinToString()
                )?.observe(mActivity, androidx.lifecycle.Observer {
                    //dismissKcsDialog()

                })

            }else {
                webCall_update_payment_type()
            }
        }
        btnDebit.setOnClickListener {
            payment_type = AppConstant.DEBIT
            manageViews()
            if (spnSplit.visibility == View.VISIBLE && spnSplit.isEnabled==true) {
                var strPaymentType_ar=ArrayList<String>()
                for (i in 0..spnSplit.selectedItem.toString().toInt()-1)
                {
                    strPaymentType_ar.add(payment_type)
                }
                placeOrderViewModel!!.Update_Payment_type(
                    CV.server_order_id.toString(),
                    strPaymentType_ar.joinToString()
                )?.observe(mActivity, androidx.lifecycle.Observer {
                    //dismissKcsDialog()

                })

            }else {
                webCall_update_payment_type()
            }
        }
        btnSplit.setOnClickListener {
            if (spnSplit.visibility == View.VISIBLE) {
                spnSplit.visibility = View.INVISIBLE
                btnSplit.setTextColor(resources.getColor(R.color.colorPrimary))
                btnSplit.background = resources.getDrawable(R.drawable.edt_bg)
                llCash.visibility = View.INVISIBLE
                if (payment_type.equals(AppConstant.CASH) || NewSaleFragmentCommon.isOtherCashTrasaction(
                        paymentMethodList,
                        payment_type
                    )
                ) {
                    llCash.visibility = View.VISIBLE
                }
                edtAmount.isEnabled = true
                ivClearAmonut.visibility = View.VISIBLE
                keyboard.visibility = View.VISIBLE
                val amount =
                    tvBalanceDueValue.text.toString()
                        .split(" ")[1].toDouble()
                edtAmount.setText(String.format("%.2f", (amount)))
                btnManualCard.visibility = View.VISIBLE
                if (paymentMethodList != null && paymentMethodList!!.size > 0) {
                    btnPaymentMethod.visibility = View.VISIBLE
                } else {
                    btnPaymentMethod.visibility = View.GONE
                }
            } else {
                edtAmount.isEnabled = false
                keyboard.visibility = View.INVISIBLE
                ivClearAmonut.visibility = View.GONE
                spnSplit.visibility = View.VISIBLE
                btnSplit.background = mActivity.resources.getDrawable(R.drawable.btn_bg)
                btnSplit.setTextColor(resources.getColor(R.color.colorWhite))
                llCash.visibility = View.INVISIBLE
                spnSplit.setSelection(0)

                val amount =
                    tvBalanceDueValue.text.toString()
                        .split(" ")[1].toDouble() / spnSplit.selectedItem.toString().toInt()
                edtAmount.setText(String.format("%.2f", (amount)))
                btnManualCard.visibility = View.GONE
                btnPaymentMethod.visibility = View.GONE
                var strPaymentType_ar=ArrayList<String>()
                for (i in 0..spnSplit.selectedItem.toString().toInt()-1)
                {
                    strPaymentType_ar.add(payment_type)
                }
                placeOrderViewModel!!.Update_Payment_type(
                    CV.server_order_id.toString(),
                    strPaymentType_ar.joinToString()
                )?.observe(this, androidx.lifecycle.Observer {
                    //dismissKcsDialog()

                })
            }
        }
        spnSplit.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                if (spnSplit.visibility == View.VISIBLE) {
                    val amount =
                        tvBalanceDueValue.text.toString()
                            .split(" ")[1].toDouble() / (parent.getItemAtPosition(
                            position
                        )).toString().toInt()
                    edtAmount.setText(String.format("%.2f", (amount)))
                    var strPaymentType_ar=ArrayList<String>()
                    for (i in 0..spnSplit.selectedItem.toString().toInt()-1)
                    {
                        strPaymentType_ar.add(payment_type)
                    }
                    placeOrderViewModel!!.Update_Payment_type(
                        CV.server_order_id.toString(),
                        strPaymentType_ar.joinToString()
                    )?.observe(mActivity, androidx.lifecycle.Observer {
                        //dismissKcsDialog()

                    })
                } else {
                    edtAmount.setText(
                        String.format(
                            "%.2f",
                            (tvBalanceDueValue.text.toString().split(" ")[1].toDouble())
                        )
                    )
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // another interface callback
            }
        }

        spnPaymentMethod.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val paymentData = paymentMethodList!![position]
                btnPaymentMethod.text = paymentData.strPaymentMethod
                if (spnSplit.visibility == View.VISIBLE && spnSplit.isEnabled==true) {
                    var strPaymentType_ar=ArrayList<String>()
                    for (i in 0..spnSplit.selectedItem.toString().toInt()-1)
                    {
                        strPaymentType_ar.add(payment_type)
                    }
                    placeOrderViewModel!!.Update_Payment_type(
                        CV.server_order_id.toString(),
                        strPaymentType_ar.joinToString()
                    )?.observe(mActivity, androidx.lifecycle.Observer {
                        //dismissKcsDialog()

                    })

                }else {
                    if (position != 0)
                        payment_type = paymentData.strPaymentMethod
                    webCall_update_payment_type()
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        btnPlaceOrder.setOnClickListener {
            orderGenerate()
        }
        btnCheckLastPayment.setOnClickListener {
            if (CM.isInternetAvailable(mActivity)) {
                webCallCheckLastPayment()
            }else{
                DialogUtility.openSingleActionDialog(
                    resources.getString(R.string.msg_network_error),
                    "OK",
                    mActivity
                ) {

                }
            }
        }
        btnCalculateChange.setOnClickListener {
            if (SystemClock.elapsedRealtime() - lastTimeClicked < 1000) {
                return@setOnClickListener
            }
            lastTimeClicked = SystemClock.elapsedRealtime()
            if (NewSaleFragmentCommon.isOtherCashTrasaction(
                    paymentMethodList,
                    payment_type
                ) && spnPaymentMethod.selectedItemPosition == 0
            ) {
                showToast(
                    mActivity,
                    getString(R.string.select_valid_payment),
                    R.drawable.icon,
                    custom_toast_container
                )
                return@setOnClickListener
            }else if(Btncalculate_change_flag==true){
                DialogUtility.openSingleActionDialog(
                    resources.getString(R.string.calculate_change_flag_msg),
                    "OK",
                    mActivity
                ) {

                }
                return@setOnClickListener
            }
            else if (edtAmount.text.toString().isEmpty())
            {
                showToast(
                    mActivity,
                    getString(R.string.enter_valid_amount),
                    R.drawable.icon,
                    custom_toast_container
                )
                return@setOnClickListener
            }
            else if (edtAmount.text.toString().toDouble()>=10000000)
            {
                CM.showMessageOK(
                    mActivity,
                    "",
                    String.format(getString(R.string.higher_amount_msg),currency_symbol,currency_symbol),
                    null
                )
                return@setOnClickListener
            }
            if (paymentRequestList != null && paymentRequestList.size > 0) {
                temp_order_id = "$order_id-${paymentRequestList.size}"
            } else {
                temp_order_id = order_id
            }
            val amount = edtAmount.text.toString()
            if (payment_type == AppConstant.CASH || NewSaleFragmentCommon.isOtherCashTrasaction(
                    paymentMethodList,
                    payment_type
                )
            ) {
                try {
                    if (paymentRequestList != null && paymentRequestList.size > 0 && amount.toDouble() > tvBalanceDueValue.text.toString()
                                    .split(" ")[1].toDouble()
                    ) {
                        showToast(
                                mActivity,
                                getString(R.string.cash_higher_payment),
                                R.drawable.icon,
                                custom_toast_container
                        )
                    } else {
                        try {
                            btnCheckLastPayment.visibility = View.GONE
                            val isNotOnlyOpenDiscontedProduct =
                                    orderTransList.any { it.iDiscountId != -1 }
                            if (amount == "0." && isNotOnlyOpenDiscontedProduct) {
                                showToast(
                                        mActivity,
                                        getString(R.string.enter_valid_amount),
                                        R.drawable.icon,
                                        custom_toast_container
                                )
                                return@setOnClickListener

                            } else if (amount.toDouble() <= 0 && isNotOnlyOpenDiscontedProduct) {
                                showToast(
                                        mActivity,
                                        getString(R.string.enter_valid_amount),
                                        R.drawable.icon,
                                        custom_toast_container
                                )
                                return@setOnClickListener
                            }
                            var remaining_amount =
                                    amount.toDouble() - mBinding.tvTotalValue.text.toString().split(" ")
                                            .get(1)
                                            .toDouble()
                            if (remaining_amount < 0) {
                                remaining_amount = 0.0
                            }
                            paymentViewModel.payableAmount =
                                    String.format("%.0f", (amount.toDouble() * 100))
                            if (CM.getSp(
                                            context!!,
                                            CV.IS_ADMIN_APPROVAL_REQUIRED_FOR_TRANSACTION,
                                            1
                                    ) == 1
                            ) {
                                if (emp_role.equals(AppConstant.STAFF)) {
                                    showRestrictDialog(getString(R.string.not_authorized))
                                } else {
                                    if (CM.isInternetAvailable(mActivity)) {
                                        if (isCashTransaction() || NewSaleFragmentCommon.isOtherCashTrasaction(
                                                        paymentMethodList, payment_type
                                                )
                                        ) {
                                            var activityTemp = (activity as ParentActivity)
                                            activityTemp.openCashBox()
                                            payment_mode = payment_type
                                            webCall_update_payment_type()
                                            orderPaymentRequestWithOrderGenerate(
                                                    payment_type,
                                                    remaining_amount,
                                                    amount
                                            )
                                        }
                                    } else {
                                        CM.showMessageOK(
                                                mActivity,
                                                "",
                                                resources.getString(R.string.msg_network_error),
                                                DialogInterface.OnClickListener { p0, p1 ->
                                                    p0?.dismiss()
                                                    dismissKcsDialog()
                                                }
                                        )
                                    }
                                }
                            } else {
                                openPassCodeDialog(AppConstant.NEW_SALE, remaining_amount, amount) {
                                    var activityTemp = (activity as ParentActivity)
                                    activityTemp.openCashBox()
                                    payment_mode = payment_type
                                    webCall_update_payment_type()
                                    orderPaymentRequestWithOrderGenerate(
                                            payment_mode,
                                            remaining_amount,
                                            amount
                                    )
                                }
                            }

                        } catch (e: NumberFormatException) {
                            e.printStackTrace()
                            showToast(
                                    mActivity,
                                    getString(R.string.msg_enter_proper_amount),
                                    R.drawable.icon,
                                    custom_toast_container
                            )
                        }
                    }
                } catch (e: NumberFormatException) {
                    e.printStackTrace()
                    showToast(
                            mActivity,
                            getString(R.string.msg_enter_proper_amount),
                            R.drawable.icon,
                            custom_toast_container
                    )
                }

            } else {
                try {
                    if (amount.toDouble() > tvBalanceDueValue.text.toString()
                            .split(" ")[1].toDouble()
                    ) {
                        showToast(
                            mActivity,
                            getString(R.string.card_higher_payment),
                            R.drawable.icon,
                            custom_toast_container
                        )
                    } else {
                        paymentViewModel.payableAmount =
                            String.format("%.0f", (amount.toDouble() * 100))
                        if (CM.getSp(
                                context!!,
                                CV.IS_ADMIN_APPROVAL_REQUIRED_FOR_TRANSACTION,
                                1
                            ) == 1
                        ) {
                            btnCheckLastPayment.visibility = View.GONE
                            connectPaymentGateway(payment_type)
                        } else {
                            openPassCodeDialog(
                                AppConstant.NEW_SALE,
                                edtAmount.text.toString().toDouble(),
                                amount
                            ) {
                                btnCheckLastPayment.visibility = View.GONE
                                connectPaymentGateway(payment_type)
                            }
                        }
                    }
                } catch (e: NumberFormatException) {
                    e.printStackTrace()
                    showToast(
                        mActivity,
                        getString(R.string.msg_enter_proper_amount),
                        R.drawable.icon,
                        custom_toast_container
                    )
                }
            }
        }
    }


    fun orderPaymentRequestWithOrderGenerate(
        payment_type: String,
        remain: Double,
        givenAmount: String
    ) {
        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.DISPLAY_DATE_FORMATTE)
        val isSplit = spnSplit.visibility == View.VISIBLE
        var temp = PaymentRequestModel(
            "1",
            mBinding.tvTotal.text.toString().split(" ")[1],
            mBinding.tvTotalValue.text.toString().split(" ")[1],
            payment_type,
            "",
            sdf.format(date),
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            String.format("%.2f", (givenAmount.toDouble())),
            remain.toBigDecimal().toPlainString(),
            "",
            payment_type,
            false
        )

        if (!isCashTransaction() && !NewSaleFragmentCommon.isOtherCashTrasaction(
                paymentMethodList,
                payment_type
            )
        ) {
            paymentViewModel.cardResponseModel?.let {
                var cardType = "Card"
                var signature = ""
                var applepaystatus: Boolean = false
                if (payment_mode.toString().equals(AppConstant.APPLE_PAY)) {
                    applepaystatus = true
                }
                if (it.emvTagData != null && it.emvTagData.isNotEmpty()) {
                    try {
                        cardType =
                            Gson().fromJson(
                                it.emvTagData,
                                EmvTagData::class.java
                            ).applicationLabel
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }/*else{
                    cardType = payment_type
                }*/
                it.signature?.let {
                    signature = it
                }
                lastFourDigit = it.token.takeLast(4)
                it.name?.let {
                    cardHolderName = it
                }
                var avsresp =if (it.avsresp==null) { "" }else{ it.avsresp}
                temp = PaymentRequestModel(
                    "1",
                    mBinding.tvTotal.text.toString().split(" ").get(1),
                    mBinding.tvTotalValue.text.toString().split(" ").get(1),
                    payment_type,
                    lastFourDigit,
                    sdf.format(date),
                    paymentViewModel.hsn,
                    it.merchid,
                    it.orderid,
                    cardHolderName,
                    it.batchid,
                    it.retref,
                    avsresp.toString(),
                    String.format("%.2f", (givenAmount.toDouble())),
                    "0.0",
                    signature,
                    cardType,
                    applepaystatus
                )
                paymentList.add(
                    PaymentModel(
                        it.merchid,
                        payment_type,
                        givenAmount,
                        temp.strRetRef,
                        currency_symbol,
                        cardType,
                        isSplit
                    )
                )
            }

        } else {
            paymentList.add(
                PaymentModel(
                    "1",
                    payment_type,
                    givenAmount,
                    temp.strRetRef,
                    currency_symbol,
                    payment_type,
                    isSplit
                )
            )
        }
        rvPayment.adapter!!.notifyDataSetChanged()
        paymentRequestList.add(temp)
        if (isSplit) {
            val btnSplit = dialogcheckout!!.findViewById<Button>(R.id.btnSplit)
            btnSplit.isEnabled = false
            spnSplit.isEnabled = false
        }

        var totalPayableAmount = 0.0
        if (paymentList.isNotEmpty()) {
            totalPayableAmount = paymentList.map { it.strPaymentAmount.toDouble() }.sum()
        }
        var balanceDue = mBinding.tvTotalValue.text.toString().split(" ")[1].toDouble()
            .minus(String.format("%.2f", (totalPayableAmount.toDouble())).toDouble())
        if (balanceDue < 0.0 || mBinding.tvTotalValue.text.toString()
                .split(" ")[1].toDouble() == String.format(
                "%.2f",
                (totalPayableAmount.toDouble())
            )
                .toDouble()
        ) {
            balanceDue = 0.0
        }
        tvBalanceDueValue.text = "$currency_symbol ${String.format("%.2f", (balanceDue))}"
        val edtAmount = dialogcheckout!!.findViewById<EditText>(R.id.edtAmount)
        if (!isSplit) {
            edtAmount.setText(String.format("%.2f", (balanceDue)))
        } else {
            val slectedItemValue = spnSplit.selectedItem.toString().toInt()
            val splitByTotalCount = paymentList.filter { item -> item.isSplit }.count()
            if (splitByTotalCount == slectedItemValue - 1) {
                edtAmount.setText(String.format("%.2f", (balanceDue)))
            }
        }

        if (balanceDue == 0.0) {
            val llPaymentCompleted =
                dialogcheckout!!.findViewById<LinearLayout>(R.id.llPaymentCompleted)
            llPaymentCompleted.visibility = View.VISIBLE
            val btnPlaceOrder = dialogcheckout!!.findViewById<Button>(R.id.btnPlaceOrder)
            btnPlaceOrder.visibility = View.VISIBLE
            val llLeftView = dialogcheckout!!.findViewById<LinearLayout>(R.id.llLeftView)
            llLeftView.visibility = View.GONE
            orderGenerate()
        }
    }

    fun dismissOpenNewCheckOutDialog() {
        if (mActivity.isFinishing) {
            return
        }
        if (dialogcheckout != null && dialogcheckout!!.isShowing) {
            dialogcheckout!!.dismiss()
            dialogcheckout = null
        }
    }

    public fun openPassCodeDialog(
        mFrom: String, remain: Double, givenAmount: String, callBack: (rollId: Int) -> Unit
    ) {
        dialog = Dialog(mActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.setCallback(
                UserInteractionAwareCallback(
                        dialog.window!!.getCallback(),
                        mActivity
                )
        )
        dialog.setContentView(R.layout.dialog_passcode)

        dialog.setCancelable(false)
        dialog.show()

        val btnOneCredential = dialog.findViewById<Button>(R.id.btnOneCredential)
        val btnTwoCredential = dialog.findViewById<Button>(R.id.btnTwoCredential)
        val btnThreeCredential = dialog.findViewById<Button>(R.id.btnThreeCredential)
        val btnFourCredential = dialog.findViewById<Button>(R.id.btnFourCredential)
        val btnFiveCredential = dialog.findViewById<Button>(R.id.btnFiveCredential)
        val btnSixCredential = dialog.findViewById<Button>(R.id.btnSixCredential)
        val btnSevenCredential = dialog.findViewById<Button>(R.id.btnSevenCredential)
        val btnEightCredential = dialog.findViewById<Button>(R.id.btnEightCredential)
        val btnNineCredential = dialog.findViewById<Button>(R.id.btnNineCredential)
        val btnZeroCredential = dialog.findViewById<Button>(R.id.btnZeroCredential)

        val btnBackSpaceCredential = dialog.findViewById<Button>(R.id.btnBackSpaceCredential)
        val btnResetMpin = dialog.findViewById<Button>(R.id.btnResetMpin)

        ivRoundOneCredential = dialog.findViewById(R.id.ivRoundOneCredential)
        ivRoundTwoCredential = dialog.findViewById(R.id.ivRoundTwoCredential)
        ivRoundThreeCredential = dialog.findViewById(R.id.ivRoundThreeCredential)
        ivRoundFourCredential = dialog.findViewById(R.id.ivRoundFourCredential)

        llPinRound = dialog.findViewById(R.id.llPinRound)

        val tvClose = dialog.findViewById<TextView>(R.id.tvClose)
        tvClose.setOnClickListener {
            dialog.dismiss()
        }
        btnResetMpin.setOnClickListener {
            count = 0
            minusCount = false
            pin = ""
            dialog.dismiss()
            customerListViewModel?.let {
                setTaxIncludedStatus(it.isTaxIncluded.value!!)
            }
        }
        btnOneCredential.setOnClickListener {
            countCall(count)
            fillPasscode("1", mFrom, remain, givenAmount, callBack)
        }

        btnTwoCredential.setOnClickListener(View.OnClickListener {
            countCall(count)
            fillPasscode("2", mFrom, remain, givenAmount, callBack)
        })
        btnThreeCredential.setOnClickListener {
            countCall(count)
            fillPasscode("3", mFrom, remain, givenAmount, callBack)
        }
        btnFourCredential.setOnClickListener {
            countCall(count)
            fillPasscode("4", mFrom, remain, givenAmount, callBack)
        }
        btnFiveCredential.setOnClickListener {
            countCall(count)
            fillPasscode("5", mFrom, remain, givenAmount, callBack)
        }
        btnSixCredential.setOnClickListener {
            countCall(count)
            fillPasscode("6", mFrom, remain, givenAmount, callBack)
        }
        btnSevenCredential.setOnClickListener {
            countCall(count)
            fillPasscode("7", mFrom, remain, givenAmount, callBack)
        }
        btnEightCredential.setOnClickListener {
            countCall(count)
            fillPasscode("8", mFrom, remain, givenAmount, callBack)
        }
        btnNineCredential.setOnClickListener {
            countCall(count)
            fillPasscode("9", mFrom, remain, givenAmount, callBack)
        }
        btnZeroCredential.setOnClickListener {
            countCall(count)
            fillPasscode("0", mFrom, remain, givenAmount, callBack)
        }
        btnBackSpaceCredential.setOnClickListener {
            dropPasscode()
            if (count > 0) {
                count -= 1
            }
            minusCount = true
            countCall(count)
        }
    }

    fun dropPasscode() {
        if (count <= 4 && count > 0) {
            if (pin.length > 0) {
                pin = pin.substring(0, pin.length - 1)
            }
        }
    }

    fun fillPasscode(
        code: String, mFrom: String, remain: Double,
        givenAmount: String,
        callBack: (rollId: Int) -> Unit
    ) {
        if (count <= 4 && count > 0) {
            pin = pin + code
        }

        if (count == 4) {
            try {
                if (CM.isInternetAvailable(mActivity)) {
                    webcallVerifyMPin(mFrom, remain, givenAmount, callBack)
                } else {
                    pin = ""
                    count = 0
                    CM.showMessageOK(
                        mActivity,
                        "",
                        resources.getString(R.string.msg_network_error),
                        DialogInterface.OnClickListener { p0, p1 ->
                            p0?.dismiss()
                            dialog.dismiss()
                            dismissKcsDialog()
                        }
                    )
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun webcallVerifyMPin(
        mFrom: String, remain: Double, givenAmount: String, callBack: (rollId: Int) -> Unit
    ) {
        showKcsDialog()
        viewModelVerifyMPin!!.verifyMPin(pin)
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    loginResponseModel.message?.let {
                        DialogUtility.showPinInvalidateDialog(
                            mActivity,
                            it,
                            loginResponseModel.action
                        )
                    }

                    val shake =
                        AnimationUtils.loadAnimation(mActivity, R.anim.shake)
                    llPinRound.startAnimation(shake)
                    count = 0
                    ivRoundOneCredential.setImageResource(R.drawable.roundblankbutton)
                    ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                    ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                    ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)
                    minusCount = false
                    pin = ""

                } else {
                    if (loginResponseModel.data!!.data.strEmployeeRoleName == AppConstant.STAFF) {
                        count = 0
                        minusCount = false
                        pin = ""
                        dialog.dismiss()
                        var msg = getString(R.string.you_do_not_have_the_rights)
                        when (mFrom) {
                            "VoidOrder" -> {
                                msg = getString(R.string.auth_void_order)
                            }
                            "TaxCheckBox" -> {
                                mBinding.cbTax.isChecked = true
                                msg =
                                    getString(R.string.auth_remove_tax)
                            }
                            "deleteOrder" -> {
                                msg =
                                    getString(R.string.auth_delee_item)
                            }
                            CV.APPLY_DISCOUNT -> {
                                discountListViewModel?.selectedCartDiscount = null
                                msg = getString(R.string.auth_apply_discount)
                            }
                            AppConstant.NEW_SALE -> {
                                msg = getString(R.string.auth_trasaction)
                            }

                        }
                        showRestrictDialog(msg)
                        return@Observer
                    }
                    if (loginResponseModel.data!!.data.lastEmployeeStatus.equals(AppConstant.CLOCK_OUT)) {
                        count = 0
                        minusCount = false
                        pin = ""
                        showToast(
                            mActivity,
                            getString(R.string.msg_user_already_clock_out),
                            R.drawable.icon,
                            custom_toast_container
                        )
                        dialog.dismiss()
                        //reset
                        cbTax.isChecked = true
                        setTaxIncludedStatus(cbTax.isChecked)
                        if (!mFrom.equals(CV.APPLY_DISCOUNT)) {
                            this.calculateCartProcedure(orderTransList, true) {}
                        } else {
                            discountListViewModel?.selectedCartDiscount = null
                        }
                        return@Observer
                    }

                    dialog.dismiss()

                    /*emp_id = loginResponseModel.data!!.data.iCheckEmployeeId
                    emp_role = loginResponseModel.data!!.data.strEmployeeRoleName
                    emp_name =
                        loginResponseModel.data!!.data.strFirstName + " " + loginResponseModel.data!!.data.strLastName;*/


                    if (mFrom.equals(AppConstant.NEW_SALE)) {
                        var activityTemp = (activity as ParentActivity)
                        activityTemp.ePOSPrinterController.getKitchenPrinterList()
                        if (loginResponseModel.data!!.data.strEmployeeRoleName.equals(
                                AppConstant.STAFF
                            )
                        ) {
                            showRestrictDialog(getString(R.string.you_do_not_have_the_rights))
                        } else {
                            trasaction_emp_id = loginResponseModel.data!!.data.iCheckEmployeeId
                            trasaction_emp_name =
                                loginResponseModel.data!!.data.strFirstName + " " + loginResponseModel.data!!.data.strLastName
                            trasaction_emp_role =
                                loginResponseModel.data!!.data.strEmployeeRoleName
                            //Cashbox Open for NewSale Flow
                            if (isCashTransaction()) {
                                callBack(-1)
                            } else {
                                callBack(-1)
                            }
                        }
                    } else if (mFrom.equals("VoidOrder")) {
                        callBack(loginResponseModel.data!!.data.iEmployeeRoleId)
                    } else if (mFrom.equals("deleteOrder")) {
                        callBack(loginResponseModel.data!!.data.iEmployeeRoleId)
                    } else if (mFrom.equals(CV.APPLY_DISCOUNT)) {
                        if (loginResponseModel.data!!.data.lastEmployeeStatus.equals(AppConstant.CLOCK_OUT)) {
                            callBack(0)
                        } else {
                            callBack(loginResponseModel.data!!.data.iEmployeeRoleId)
                        }
                    } else if (mFrom.equals("TaxCheckBox")) {
                        callBack(loginResponseModel.data!!.data.iEmployeeRoleId)
                    } else if (mFrom.equals("Refund")) {
                        onRefundCancelProcedure()
                    } else {
                        callBack(loginResponseModel.data!!.data.iEmployeeRoleId)
                    }
                    count = 0
                    minusCount = false
                    pin = ""
                }

            })
    }

    fun countCall(c: Int) {
        when (c) {
            0 -> if (minusCount) {
                count = 0
                ivRoundOneCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                minusCount = false
            } else {
                count += 1
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

            }
            1 -> if (minusCount) {
                count = 1
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                minusCount = false
            } else {
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                count += 1
            }
            2 -> if (minusCount) {
                count = 2
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                minusCount = false
            } else {
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                count += 1
            }
            3 -> if (minusCount) {
                count = 3
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                minusCount = false
            } else {
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                count += 1
            }
            4 -> if (minusCount) {
                count = 4
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                minusCount = false
            } else {
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                count += 1
            }
            5 -> if (minusCount) {
                count = 5
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                minusCount = false
            } else {
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)
                count += 1
            }
        }
    }

    fun orderGenerate() {

        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.WS_DATE_FORMATTE)

        if (itemRequestList.size > 0) {
            itemRequestList.clear()
        }

        for (i in orderTransList.indices) {
            var cartLevelDiscount = false
            var dicountId = 0;
            val item = orderTransList[i]

            val turnsType =
                object : TypeToken<HashMap<String, List<IngredientMasterModel>>>() {}.type
            val modifierMap = Gson().fromJson<HashMap<String, List<IngredientMasterModel>>>(
                item.premodifierType,
                turnsType
            )
            var tempValueList = ArrayList<IngredientMasterModel>()
            var modifierReqList = ArrayList<ModifierRequestModel>()
            if (modifierMap != null) {
                for ((k, v) in modifierMap) {
                    println("$k = $v")
                    tempValueList = v as ArrayList<IngredientMasterModel>

                    for (i in tempValueList.indices) {
                        var temp: ModifierRequestModel?
                        if (k.equals(CV.SUB)) {
                            temp = ModifierRequestModel(
                                tempValueList.get(i).iModifierGroupId.toString(),
                                tempValueList.get(i).strOptionName + " Substitute With " + mDatabase.daoIngredientList()
                                    .getdefaultIngredient(
                                        tempValueList.get(i).iModifierGroupId.toString(),
                                        item.itemId
                                    ),
                                tempValueList.get(i).dblOptionPrice.toDouble(),
                                k
                            )
                        } else {
                            temp = ModifierRequestModel(
                                tempValueList.get(i).iModifierGroupId.toString(),
                                tempValueList.get(i).strOptionName,
                                tempValueList.get(i).dblOptionPrice.toDouble(),
                                k
                            )
                        }
                        modifierReqList.add(temp!!)
                    }
                }
            }
            var txtRateInPercentage = 0.0
            if (!TextUtils.isEmpty(item.taxPercentage)) {
                txtRateInPercentage = item.taxPercentage.toDouble()
            }

            var quantity = item.itemQty
            val rate = item.itemRate
            var discountAmount = 0.0f

            item.discountAmount?.let {
                discountAmount = it
            }

            if (discountAmount == 0.0f) {
                discountListViewModel?.selectedCartDiscount?.let { selectedDiscount ->
                    if (selectedDiscount.discountType.equals(AppConstant.COUPON_DISCOUNT)) {
                        var isValidProduct =
                            discountListViewModel?.checkDiscountMappingItemIsAvailable(
                                item.itemId.toLong(),
                                discountListViewModel?.selectedCartDiscount?.discountId!!.toLong()
                            )

                        if (isValidProduct!!) {
                            if (item.discountAmount == 0.0f) {
                                val amountCopoun = (quantity * rate)
                                val itemWeightAge =
                                    (amountCopoun * 100) / totalApplicableItemDiscount
                                discountAmount =
                                    ((coupnDiscountAmount * itemWeightAge) / 100).toFloat()
                                cartLevelDiscount = true
                                dicountId =
                                    discountListViewModel?.selectedCartDiscount?.discountId!!
                            }
                        }

                    } else {
                        val amountCopoun = (quantity * rate)
                        val itemWeightAge =
                            (amountCopoun * 100) / totalApplicableItemDiscount
                        discountAmount =
                            ((coupnDiscountAmount * itemWeightAge) / 100).toFloat()
                        cartLevelDiscount = true
                        dicountId =
                            discountListViewModel?.selectedCartDiscount?.discountId!!
                    }
                }
            } else {
                cartLevelDiscount = false
                dicountId = item.iDiscountId
            }

            val amount = (quantity * rate) - discountAmount


            var tax_amount = 0.0
            var taxrate = 0.0
            customerListViewModel?.isTaxIncluded?.let {
                if (it.value!!)
                    taxrate = (txtRateInPercentage / 100)
            }

            tax_amount = amount * taxrate // (amount * (txtRateInPercentage / 100))
            tax_amount = String.format("%.2f", (tax_amount)).toDouble()

            val temp = OrderTransRequestModel(
                item.itemId.toString(),
                item.itemQty.toString(),
                item.itemRate.toString(),
                getFormatedAmount(item.itemAmount),
                item.taxPercentage,
                tax_amount.toString(),
                discountAmount.toString(),
                dicountId,
                item.discountRemark,
                "",
                "",
                sdf.format(date),
                "",
                "",
                "",
                item.premodifierType,
                modifierReqList,
                cartLevelDiscount,
                item.tIsOpenDiscount,
                item.strInstruction,
                item.kitchenServedquantity
            )
            itemRequestList.add(temp)
        }

        //webCallPlaceOrder()
        webCallUpdateorderpayment()
    }


    private fun PrintKitchenReceipt(emp_name: String, emp_role: String, mOrderStatus: String) {

        val mKitchenPrinterData = KitchenPrinterData()

        mKitchenPrinterData.setInvoiceNo(InvoiceNumber)
        mKitchenPrinterData.setBilledPersonName(emp_name)
        mKitchenPrinterData.rollName = emp_role
        mKitchenPrinterData.customerName = getCustomerName()

        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.DISPLAY_DATE_FORMATTE_AMPM)

        mKitchenPrinterData.setOrderDate(sdf.format(date).toString().split("\\s".toRegex())[0])
        mKitchenPrinterData.setOrderTime(
            sdf.format(date).toString().split("\\s\\s".toRegex())[1]
        )

        var POSName = CM.getSp(mActivity, CV.POS_NAME, "") as String
        mKitchenPrinterData.setPOSName(POSName)

        var mPrinterKitchenProductsList: ArrayList<PrinterKitchenProductsModel> = ArrayList()
        var isOrderModifiedAfterHold = false
        var isKitchenPrintServedforHold = false
        var isKitchenPrintServedforSameQty = false
        var isOrderSameQty = false

        var itemqty = 0
        if (selected_history != null) {//is Pull-Back from History
            if (!mOrderStatus.equals(AppConstant.REFUND)) {//Order and Hold case
                mKitchenPrinterData.setOrderDate(selected_history!!.datetimeCheckBusinessDate);
                isKitchenPrintServedforSameQty =
                    isKitchenPrintServedforHoldOrder(selected_history)
                for (i in orderTransList.indices) {
                    if (orderTransList.get(i).strProductType.equals(CV.PRODUCTS_TYPE_KITCHEN)) {
                        val mPrinterKitchenProductsModel = PrinterKitchenProductsModel()
                        var pos = FindSamePreviousItemQtybyItsName(
                            selected_history,
                            orderTransList.get(i).itemId
                        )
                        if (pos != -1) {//is item exists in History
                            if (isKitchenPrintServedforSameQty) {
                                if (orderTransList.get(i).itemQty.equals(
                                        selected_history!!.orderProducts?.get(
                                            pos
                                        )!!.dblItemQty.toInt()
                                    )
                                ) {// Check individual item qty - is same
                                    itemqty = orderTransList.get(i).itemQty
                                    isOrderSameQty = true
                                } else {
                                    isOrderSameQty = false
                                    itemqty = (orderTransList.get(i).itemQty).minus(
                                        selected_history!!.orderProducts?.get(pos)!!.dblItemQty.toInt()
                                    )
                                }
                            } else {
                                isOrderSameQty = false
                                itemqty = orderTransList.get(i).itemQty
                            }
                        } else {
                            isOrderSameQty = false
                            itemqty = orderTransList.get(i).itemQty
                        }
                        /*if (!isOrderSameQty) {

                            isOrderModifiedAfterHold = true
                            isKitchenPrintServedforHold = false

                            mPrinterKitchenProductsModel.setProductName(orderTransList.get(i).itemName)
                            mPrinterKitchenProductsModel.setQuantity(itemqty.toString())
                            mPrinterKitchenProductsModel.setModifierIngredient(
                                orderTransList.get(
                                    i
                                ).ingredientName.trim()
                            )

                            mPrinterKitchenProductsList.add(mPrinterKitchenProductsModel)
                        }*/

                        if (orderTransList.get(i).itemQty > orderTransList.get(i).kitchenServedquantity)
                        {
                            LogUtil.d("itemQty",orderTransList.get(i).itemQty.toString())
                            LogUtil.d("itemserverQty",orderTransList.get(i).kitchenServedquantity.toString())
                            mPrinterKitchenProductsModel.setProductName(orderTransList.get(i).itemName)
                            mPrinterKitchenProductsModel.setQuantity((orderTransList.get(i).itemQty- orderTransList.get(i).kitchenServedquantity).toString())
                            orderTransList.get(i).kitchenServedquantity=(orderTransList.get(i).itemQty- orderTransList.get(i).kitchenServedquantity)+ orderTransList.get(i).kitchenServedquantity
                            mPrinterKitchenProductsModel.setModifierIngredient(
                                orderTransList.get(
                                    i
                                ).ingredientName.trim()
                            )
                            mPrinterKitchenProductsModel.setInstruction(orderTransList.get(i).strInstruction)

                            mPrinterKitchenProductsList.add(mPrinterKitchenProductsModel)
                        }
                    }
                }
            } else {//Reprint Case
                mKitchenPrinterData.setOrderDate(selected_history!!.datetimeCheckBusinessDate);
                isOrderModifiedAfterHold = true
                isKitchenPrintServedforHold = false
                for (i in orderTransList.indices) {
                    itemqty = orderTransList.get(i).itemQty
                    if (orderTransList.get(i).strProductType.equals(CV.PRODUCTS_TYPE_KITCHEN)) {
                        val mPrinterKitchenProductsModel = PrinterKitchenProductsModel()
                        mPrinterKitchenProductsModel.setProductName(orderTransList.get(i).itemName)
                        mPrinterKitchenProductsModel.setQuantity(itemqty.toString())
                        mPrinterKitchenProductsModel.setModifierIngredient(orderTransList.get(i).ingredientName.trim())
                        mPrinterKitchenProductsModel.setInstruction(orderTransList.get(i).strInstruction)
                        mPrinterKitchenProductsList.add(mPrinterKitchenProductsModel)
                    }
                }
            }
        } else {//Normal Flow
            if (mOrderStatus.equals(AppConstant.ON_HOLD)) {
                mKitchenPrinterData.setOrderDate(convertDatetospecifictimezone(activity!!));
            } else {
                mKitchenPrinterData.setOrderDate(CV.ORDER_DATE);
            }
            isOrderModifiedAfterHold = true
            isKitchenPrintServedforHold = false
            for (i in orderTransList.indices) {
                itemqty = orderTransList.get(i).itemQty
                if (orderTransList.get(i).strProductType.equals(CV.PRODUCTS_TYPE_KITCHEN)) {
                    orderTransList.get(i).kitchenServedquantity=itemqty
                    val mPrinterKitchenProductsModel = PrinterKitchenProductsModel()
                    mPrinterKitchenProductsModel.setProductName(orderTransList.get(i).itemName)
                    mPrinterKitchenProductsModel.setQuantity(itemqty.toString())
                    mPrinterKitchenProductsModel.setModifierIngredient(orderTransList.get(i).ingredientName.trim())
                    mPrinterKitchenProductsModel.setInstruction(orderTransList.get(i).strInstruction)
                    mPrinterKitchenProductsList.add(mPrinterKitchenProductsModel)
                }
            }
        }

        mKitchenPrinterData.setOrderStatus(mOrderStatus)
        if (!mPrinterKitchenProductsList.isNullOrEmpty()) {
                    mKitchenPrinterData.setmPrinterKitchenProductsModel(
                        mPrinterKitchenProductsList
                    )
                    var baseActivity = (activity as ParentActivity)
                    if (baseActivity.ePOSPrinterController.PrintKitchenReceipt(
                            mKitchenPrinterData
                        )
                    ) {

                        Log.e("KITCHEN_PRINTER", "Kitchen Printer Working Fine")

                    } else {
                        KitchenPrinterNotFoundDialog() {
                            Log.e(
                                "KITCHEN_PRINTER",
                                "Kitchen Printer not Working and stared discover new list"
                            )
                        }
                    }
        }
    }

    private fun isKitchenPrintServedforHoldOrder(selectedHistory: OrderHistoryModel?): Boolean {
        var isKitchenPrintServedforHoldOrder = false
        for (i in selectedHistory!!.orderProducts!!.indices) {
            if (!selectedHistory!!.orderProducts!!.get(i).isKitchenPrintServed) {
                isKitchenPrintServedforHoldOrder = true
                break
            }
        }
        return isKitchenPrintServedforHoldOrder

    }

    private fun FindSamePreviousItemQtybyItsName(
        selectedHistory: OrderHistoryModel?,
        itemId: Int
    ): Int {
        var pos: Int = -1
        for (i in selectedHistory!!.orderProducts!!.indices) {
            if (selectedHistory.orderProducts!![i].iProductId.equals(itemId)) {
                pos = i
                break
            }
        }
        return pos
    }

    private fun KitchenPrinterNotFoundDialog(callBack: (isPrintAllow: Boolean) -> Unit) {
        if (mActivity != null) {
            val dialog = Dialog(mActivity)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.window!!.setCallback(
                    UserInteractionAwareCallback(
                            dialog.window!!.getCallback(),
                            mActivity
                    )
            )

            dialog.setContentView(R.layout.dialog_kitchen_printer_notfound)

            dialog.setCancelable(false)
            val btnYes = dialog.findViewById<Button>(R.id.btnYes)
            val btnNo = dialog.findViewById<Button>(R.id.btnNo)
            btnYes.setOnClickListener {
                dialog.dismiss()
                callBack(true)

            }

            dialog.show()
        }
    }

    private fun PrintSalesReceipt(
        mDateTime: String,
        mInvoiceNumber: String,
        mTenderAmount: String
    ) {

        var baseActivityPrint = (activity as ParentActivity)

        var POSName = CM.getSp(mActivity, CV.POS_NAME, "") as String

        val mPrinterSalesModel = PrinterSalesModel()

        mPrinterSalesModel.strSocialMedia =
            CM.getSp(mActivity, CV.STRSOCIALMEDIA, "").toString()
        mPrinterSalesModel.facebook = CM.getSp(mActivity, CV.STRFACEBOOK, "").toString()
        mPrinterSalesModel.instagram = CM.getSp(mActivity, CV.STRINSTAGRAM, "").toString()
        mPrinterSalesModel.twitter = CM.getSp(mActivity, CV.STRTWITTER, "").toString()
        mPrinterSalesModel.youtube = CM.getSp(mActivity, CV.STRYOUTUBE, "").toString()

        mPrinterSalesModel.setStoreName(StoreName)
        mPrinterSalesModel.setAddressLine1(StoreAddress1)
        mPrinterSalesModel.setAddressLine2(StoreAddress2)
        mPrinterSalesModel.setTelNumber(StorePhoneNum)
        mPrinterSalesModel.setStoreLogoUrl(
            CM.getSp(mActivity, CV.STORE_LOGO_URL, "").toString()
        )
        mPrinterSalesModel.setStoreLogoLocalPath(CV.STORE_LOGO_LOCAL_PATH)

        mPrinterSalesModel.setBilledPersonName(customerListViewModel?.selectedCustomer?.strFirstName + " " + customerListViewModel?.selectedCustomer?.strLastName)
        mPrinterSalesModel.setInvoiceNo(mInvoiceNumber)

        mPrinterSalesModel.setReceiptDateTime(mDateTime)
        mPrinterSalesModel.setPOSStationNo(POSName)

        mPrinterSalesModel.setCashierName(if (TextUtils.isEmpty(trasaction_emp_name)) emp_name else trasaction_emp_name)
        mPrinterSalesModel.setEmpRole(if (TextUtils.isEmpty(trasaction_emp_role)) emp_role else trasaction_emp_role)

        for (paymentRequest in paymentRequestList) {
            var printPaymentMode = PrinterPaymentSalesModel()
            printPaymentMode.setmPaymentMode(paymentRequest.strPaymentMethod)
            printPaymentMode.setmCustomerPaid(currency_symbol + " " + paymentRequest.dblGIvenAmount)
            printPaymentMode.setmTransactionIdDate(paymentRequest.datePaymentDate)
            printPaymentMode.setmTransactionNumber(paymentRequest.strRetRef)
            printPaymentMode.setmCardType(paymentRequest.strCardType)
            if (NewSaleFragmentCommon.isOtherCashTrasaction(
                    paymentMethodList,
                    paymentRequest.strPaymentMethod
                )
            ) {
                printPaymentMode.isOtherCashPayment = true
            }
            if (paymentRequest.strPaymentMethod.equals(
                    AppConstant.CASH,
                    ignoreCase = true
                ) || NewSaleFragmentCommon.isOtherCashTrasaction(
                    paymentMethodList,
                    paymentRequest.strPaymentMethod
                )
            ) {
                printPaymentMode.setmChangeDue(currency_symbol + " " + paymentRequest.dblGIvenChange)
                printPaymentMode.setmTenderedAmount(currency_symbol + " " + (paymentRequest.dblGIvenAmount.toDouble() + paymentRequest.dblGIvenChange.toDouble()))
            } else {
                printPaymentMode.setmCardHolderName(paymentRequest.strcardHolderName)
                printPaymentMode.setmCardNumber(paymentRequest.strPaymentCardLast4)
                printPaymentMode.setmTransactionNumber(paymentRequest.strRetRef)
                printPaymentMode.setmTenderedAmount(currency_symbol + " " + (paymentRequest.dblGIvenAmount.toDouble()))

                if (!TextUtils.isEmpty(paymentRequest.strSignature)) {
                    printPaymentMode.setmStrSignature(paymentRequest.strSignature)
                    mImagesCache.addImageToWarehouse(
                        printPaymentMode.getmTransactionNumber(),
                        baseActivityPrint.getBitmapFromBase64(paymentRequest.strSignature)
                    )
                }
            }
            mPrinterSalesModel.paymentModes.add(printPaymentMode)
        }

        val mTaxApplied =
            if (mBinding.cbTax.isChecked) (mBinding.tvTax.text.toString()) else currency_symbol + " " + "0.0"

        mPrinterSalesModel.setTotalQty(mBinding.tvTotalQty.text.toString())
        mPrinterSalesModel.setSubtotal(mBinding.tvTotal.text.toString())
        mPrinterSalesModel.setTaxApplied(mTaxApplied)
        mPrinterSalesModel.setDiscount(mBinding.tvDiscountValue.text.toString())
        mPrinterSalesModel.setDiscountRemark(mBinding.cbDiscountApplied.text.toString())
        mPrinterSalesModel.setGrandTotal(mBinding.tvTotalValue.text.toString())
        mPrinterSalesModel.setCustomerPaid(currency_symbol + " " + mTenderAmount)
        mPrinterSalesModel.setTotalSavings(
            getFormatedAmountWithCurrencySymbol(
                getTotalSavingAmount(
                    mBinding.tvDiscountValue.text.toString().split(" ")[1].toFloat()
                )
            )
        )
        mPrinterSalesProducts.clear()
        for (i in orderTransList.indices) {
            val mPrinterSalesProductsModel = PrinterSalesProductsModel()

            val itemQty = orderTransList.get(i).itemQty
            val itemRate = orderTransList.get(i).itemRate
            var itemPrice = itemQty * itemRate
            itemPrice -= NewSaleFragmentCommon.floatToDouble(orderTransList.get(i).discountAmount)

            mPrinterSalesProductsModel.setProductName(orderTransList.get(i).itemName)
            mPrinterSalesProductsModel.setQuanity(orderTransList.get(i).itemQty.toString())
            mPrinterSalesProductsModel.setRate(currency_symbol + " " + orderTransList.get(i).itemRate.toString())
            mPrinterSalesProductsModel.setDiscount(currency_symbol + " " + orderTransList.get(i).discountAmount.toString())
            mPrinterSalesProductsModel.setDiscountRemark(orderTransList.get(i).discountRemark)
            mPrinterSalesProductsModel.setTotal(currency_symbol + " " + itemPrice.toString())
            mPrinterSalesProductsModel.setModifierIngredient(orderTransList.get(i).ingredientName.trim())
            mPrinterSalesProductsModel.setInstruction(orderTransList.get(i).strInstruction);
            mPrinterSalesProducts.add(mPrinterSalesProductsModel)
        }

        mPrinterSalesModel.setPaymentMode(payment_mode)
        mPrinterSalesModel.setSalesProducts(mPrinterSalesProducts)

        if (baseActivityPrint.getBitmapFromLocal(
                mPrinterSalesModel.getStoreLogoLocalPath(),
                200
            ) != null
        ) {
            mImagesCache.addImageToWarehouse(
                SQUARE_IMAGE_NAME,
                baseActivityPrint.getBitmapFromLocal(
                    mPrinterSalesModel.getStoreLogoLocalPath(),
                    200
                )!!
            )
        }

        mImagesCache.addImageToWarehouse(
            BARCODE_IMAGE_NAME,
            mBarcodeGenerator!!.GenerateBarcode(mInvoiceNumber)
        )

        baseActivityPrint.mPrinterController.printNewSalesTicket(
            baseActivityPrint,
            mPrinterSalesModel,
            CV.PRINT
        )
    }

    private fun RePrintSalesReceipt(selected_history: OrderHistoryModel?) {
        resetExistingEmployee()
        var baseActivityPrint = (activity as ParentActivity)
        var POSName = CM.getSp(mActivity, CV.POS_NAME, "") as String

        val mPrinterSalesModel = PrinterSalesModel()

        mPrinterSalesModel.strSocialMedia =
            CM.getSp(mActivity, CV.STRSOCIALMEDIA, "").toString()
        mPrinterSalesModel.facebook = CM.getSp(mActivity, CV.STRFACEBOOK, "").toString()
        mPrinterSalesModel.instagram = CM.getSp(mActivity, CV.STRINSTAGRAM, "").toString()
        mPrinterSalesModel.twitter = CM.getSp(mActivity, CV.STRTWITTER, "").toString()
        mPrinterSalesModel.youtube = CM.getSp(mActivity, CV.STRYOUTUBE, "").toString()


        mPrinterSalesModel.setStoreName(StoreName)
        mPrinterSalesModel.setAddressLine1(StoreAddress1)
        mPrinterSalesModel.setAddressLine2(StoreAddress2)
        mPrinterSalesModel.setTelNumber(StorePhoneNum)
        mPrinterSalesModel.setStoreLogoUrl(
            CM.getSp(mActivity, CV.STORE_LOGO_URL, "").toString()
        )
        mPrinterSalesModel.setStoreLogoLocalPath(CV.STORE_LOGO_LOCAL_PATH)

        mPrinterSalesModel.setBilledPersonName(selected_history!!.strFirstName + " " + selected_history!!.strLastName)
        mPrinterSalesModel.setInvoiceNo(selected_history!!.strInvoiceNumber)

        mPrinterSalesModel.setReceiptDateTime(selected_history!!.datetimeCheckBusinessDate)
        mPrinterSalesModel.setPOSStationNo(POSName)


        mPrinterSalesModel.setCashierName(selected_history!!.employeeData!!.get(0).strFirstName)
        mPrinterSalesModel.setEmpRole(selected_history!!.employeeData!!.get(0).strEmployeeRoleName)

        for (paymentRequest in selected_history!!.orderPayments!!) {
            var printPaymentMode = PrinterPaymentSalesModel()
            printPaymentMode.setmPaymentMode(paymentRequest.strPaymentMethod)
            printPaymentMode.setmCustomerPaid(currency_symbol + " " + paymentRequest.dblGIvenAmount)
            printPaymentMode.setmTransactionIdDate(paymentRequest.datePaymentDate)
            printPaymentMode.setmTransactionNumber(paymentRequest.strRetRef)
            printPaymentMode.setmCardType(paymentRequest.strCardType)
            if (NewSaleFragmentCommon.isOtherCashTrasaction(
                    paymentMethodList,
                    paymentRequest.strPaymentMethod
                )
            ) {
                printPaymentMode.isOtherCashPayment = true
            }
            if (paymentRequest.strPaymentMethod.equals(
                    AppConstant.CASH,
                    ignoreCase = true
                ) || NewSaleFragmentCommon.isOtherCashTrasaction(
                    paymentMethodList,
                    paymentRequest.strPaymentMethod
                )
            ) {
                printPaymentMode.setmChangeDue(currency_symbol + " " + paymentRequest.dblGIvenChange)
                printPaymentMode.setmTenderedAmount(currency_symbol + " " + (paymentRequest.dblGIvenAmount.toDouble() + paymentRequest.dblGIvenChange.toDouble()))
            } else {
                printPaymentMode.setmCardHolderName(paymentRequest.strcardHolderName)
                printPaymentMode.setmCardNumber(paymentRequest.strPaymentCardLast4)
                printPaymentMode.setmTransactionNumber(paymentRequest.strRetRef)
                printPaymentMode.setmTenderedAmount(currency_symbol + " " + (paymentRequest.dblGIvenAmount.toDouble()))

                if (!TextUtils.isEmpty(paymentRequest.strSignature)) {
                    printPaymentMode.setmStrSignature(paymentRequest.strSignature)
                    mImagesCache.addImageToWarehouse(
                        printPaymentMode.getmTransactionNumber(),
                        baseActivityPrint.getBitmapFromBase64(paymentRequest.strSignature)
                    )
                }
            }
            mPrinterSalesModel.paymentModes.add(printPaymentMode)
        }

        val mTaxApplied =
            if (mBinding.cbTax.isChecked) (mBinding.tvTax.text.toString()) else currency_symbol + " " + "0.0"

        mPrinterSalesModel.setTotalQty(mBinding.tvTotalQty.text.toString())
        mPrinterSalesModel.setSubtotal(mBinding.tvTotal.text.toString())
        mPrinterSalesModel.setTaxApplied(mTaxApplied)
        mPrinterSalesModel.setDiscount(mBinding.tvDiscountValue.text.toString())
        if (selected_history!!.orderDiscounts?.isNullOrEmpty()!!) {
            mPrinterSalesModel.setDiscountRemark("")
        } else {
            mPrinterSalesModel.setDiscountRemark(selected_history!!.orderDiscounts?.get(0)?.strRemark + CV.WAS_APPLIED)
        }

        mPrinterSalesModel.setGrandTotal(mBinding.tvTotalValue.text.toString())
        mPrinterSalesModel.setTotalSavings(
            getFormatedAmountWithCurrencySymbol(
                getTotalSavingAmount(
                    mBinding.tvDiscountValue.text.toString().split(" ")[1].toFloat()
                )
            )
        )
        mPrinterSalesProducts.clear()
        for (i in selected_history!!.orderProducts!!.indices) {
            val mPrinterSalesProductsModel = PrinterSalesProductsModel()

            selected_history!!.orderProducts!!.get(i).dblAmount.toDouble() - orderTransList.get(
                i
            ).discountAmount

            val itemQty = orderTransList.get(i).itemQty
            val itemRate = orderTransList.get(i).itemRate
            var itemPrice = itemQty * itemRate
            itemPrice -= NewSaleFragmentCommon.floatToDouble(orderTransList.get(i).discountAmount)


            mPrinterSalesProductsModel.setProductName(selected_history!!.orderProducts!!.get(i).strProductName)
            mPrinterSalesProductsModel.setQuanity(selected_history!!.orderProducts!!.get(i).dblItemQty)
            mPrinterSalesProductsModel.setRate(
                currency_symbol + " " + selected_history!!.orderProducts!!.get(
                    i
                ).dblPrice
            )
            mPrinterSalesProductsModel.setTotal(
                currency_symbol + " " + itemPrice
            )
            mPrinterSalesProductsModel.setModifierIngredient(orderTransList.get(i).ingredientName.trim())
            mPrinterSalesProductsModel.setDiscount(
                currency_symbol + " " + selected_history!!.orderProducts!!.get(
                    i
                ).dblItemDiscount
            )
            mPrinterSalesProductsModel.setDiscountRemark(orderTransList.get(i).discountRemark)
            mPrinterSalesProductsModel.setInstruction(orderTransList.get(i).strInstruction);


            mPrinterSalesProducts.add(mPrinterSalesProductsModel)
        }

        mPrinterSalesModel.setPaymentMode(selected_history!!.orderPayments!!.get(0).strPaymentMethod)
        mPrinterSalesModel.setSalesProducts(mPrinterSalesProducts)

        if (baseActivityPrint.getBitmapFromLocal(
                mPrinterSalesModel.getStoreLogoLocalPath(),
                200
            ) != null
        ) {
            mImagesCache.addImageToWarehouse(
                SQUARE_IMAGE_NAME,
                baseActivityPrint.getBitmapFromLocal(
                    mPrinterSalesModel.getStoreLogoLocalPath(),
                    200
                )!!
            )
        }

        mImagesCache.addImageToWarehouse(
            BARCODE_IMAGE_NAME,
            mBarcodeGenerator!!.GenerateBarcode(selected_history!!.strInvoiceNumber)
        )

        baseActivityPrint.mPrinterController.printNewSalesTicket(
            baseActivityPrint,
            mPrinterSalesModel, CV.REPRINT
        )
    }

    private fun itemAmountCalculation(item_model: ItemModel) {
        mBinding.btnCheckout.isEnabled=false
        if (CM.getSp(mActivity, CV.ORDER_ID, "") == "") {
            order_id = UUID.randomUUID().toString()
            CM.setSp(mActivity, CV.ORDER_ID, order_id)
        } else {
            order_id = CM.getSp(mActivity, CV.ORDER_ID, "") as String
        }
        setEvent(AppConstant.ORDER)
        var total_tax = 0.0

        total_tax = item_model.totalTax

        var quantity = 1

        val rate = item_model.dblRetailPrice.toDouble()
        val amount = quantity * rate

        var tax_amount = 0.0

        if (!cbTax.isChecked) {
            setTaxIncludedStatus(false)
        }
        tax_amount = (amount * total_tax) / 100

        val orderTransModel = OrderTransModel(
            UUID.randomUUID().toString(),
            order_id,
            item_model.iProductID,
            item_model.strProductName,
            rate,
            quantity,
            amount,
            item_model.strPricingType,
            "",
            "",
            item_model.totalTax.toString(),
            tax_amount.toString(),
            "",
            0,
            0.0,
            false,
            item_model.strProductType,
            0,
            "",
            0.0f,
            "",
            "",
            "",
            0,
            true,
            tIsOpenDiscount = item_model.tIsOpenDiscount
        )
        CV.productRequireHashMap.clear()
        if (mDatabase.daoModifierGroupList()
                .getModifierGroupList(item_model.iProductID) != null && mDatabase.daoModifierGroupList()
                .getModifierGroupList(
                    item_model.iProductID
                )!!.isNotEmpty()
        ) {
            for (i in mDatabase.daoModifierGroupList()
                .getModifierGroupList(item_model.iProductID)!!.indices) {
                if (mDatabase.daoModifierGroupList().getModifierGroupList(item_model.iProductID)
                        .get(
                            i
                        ).strIsRequired.equals(CV.REQUIRED)
                ) {
                    if (CV.productRequireHashMap.containsKey(item_model.iProductID)) {
                        CV.productRequireHashMap.put(
                            item_model.iProductID,
                            (CV.productRequireHashMap.get(item_model.iProductID)!! + 1)
                        )
                    } else {
                        CV.productRequireHashMap.put(item_model.iProductID, 1)
                    }
                }
            }
        }

        /*if (selected_history != null) {
            pos = FindSamePreviousItemQtybyItsName(selected_history, item_model.iProductID)
            if (pos != -1) {
                if (selected_history!!.orderProducts?.get(pos)!!.isKitchenPrintServed) {
                    if (mDatabase.daoModifierGroupList()
                            .getModifierGroupList(item_model.iProductID) != null && mDatabase.daoModifierGroupList()
                            .getModifierGroupList(item_model.iProductID)!!.isNotEmpty()
                    ) {
                        for (i in mDatabase.daoModifierGroupList()
                            .getModifierGroupList(item_model.iProductID)!!.indices) {
                            if (mDatabase.daoModifierGroupList()
                                    .getModifierGroupList(item_model.iProductID)
                                    .get(i).strIsRequired.equals(CV.REQUIRED) || mDatabase.daoModifierGroupList()
                                    .getModifierGroupList(item_model.iProductID)
                                    .get(i).strIsRequired.equals(CV.FORCE_SHOW_OPTIONAL)
                            ) {
                                var tempList = orderTransList
                                pos = tempList.reversed().indexOfFirst {
                                    it.itemId == item_model.iProductID
                                }
                                if (pos == -1) {
                                    pos = 0
                                }
                                item_id = item_model.iProductID
                                mBinding.tvNoProductFound.visibility = View.GONE
                                openModifierLayout(
                                    item_model.iProductID,
                                    item_model.strProductName
                                )
                                break
                            }
                        }
                    }
                }
            } else {
                if (mDatabase.daoModifierGroupList()
                        .getModifierGroupList(item_model.iProductID) != null && mDatabase.daoModifierGroupList()
                        .getModifierGroupList(item_model.iProductID)!!.isNotEmpty()
                ) {
                    for (i in mDatabase.daoModifierGroupList()
                        .getModifierGroupList(item_model.iProductID)!!.indices) {
                        if (mDatabase.daoModifierGroupList()
                                .getModifierGroupList(item_model.iProductID)
                                .get(i).strIsRequired.equals(CV.REQUIRED) || mDatabase.daoModifierGroupList()
                                .getModifierGroupList(item_model.iProductID)
                                .get(i).strIsRequired.equals(CV.FORCE_SHOW_OPTIONAL)
                        ) {
                            var tempList = orderTransList
                            pos = tempList.reversed().indexOfFirst {
                                it.itemId == item_model.iProductID
                            }
                            if (pos == -1) {
                                pos = 0
                            }
                            item_id = item_model.iProductID
                            mBinding.tvNoProductFound.visibility = View.GONE
                            openModifierLayout(item_model.iProductID, item_model.strProductName)
                            break
                        }
                    }
                }
            }
        } else {
            if (mDatabase.daoModifierGroupList()
                    .getModifierGroupList(item_model.iProductID) != null && mDatabase.daoModifierGroupList()
                    .getModifierGroupList(item_model.iProductID)!!.isNotEmpty()
            ) {
                for (i in mDatabase.daoModifierGroupList()
                    .getModifierGroupList(item_model.iProductID)!!.indices) {
                    if (mDatabase.daoModifierGroupList()
                            .getModifierGroupList(item_model.iProductID)
                            .get(i).strIsRequired.equals(CV.REQUIRED) || mDatabase.daoModifierGroupList()
                            .getModifierGroupList(item_model.iProductID)
                            .get(i).strIsRequired.equals(CV.FORCE_SHOW_OPTIONAL)
                    ) {
                        var tempList = orderTransList
                        pos = tempList.reversed().indexOfFirst {
                            it.itemId == item_model.iProductID
                        }
                        if (pos == -1) {
                            pos = 0
                        }

                        item_id = item_model.iProductID
                        mBinding.tvNoProductFound.visibility = View.GONE
                        openModifierLayout(item_model.iProductID, item_model.strProductName)
                        break
                    }
                }
            }
        }*/
        checkIfAlreadyExist(orderTransModel, orderTransModel.itemRate)
    }

    private fun itemAmountCalculation_return(item_model: ItemModel) : OrderTransModel {
        if (CM.getSp(mActivity, CV.ORDER_ID, "") == "") {
            order_id = UUID.randomUUID().toString()
            CM.setSp(mActivity, CV.ORDER_ID, order_id)
        } else {
            order_id = CM.getSp(mActivity, CV.ORDER_ID, "") as String
        }
        setEvent(AppConstant.ORDER)
        var total_tax = 0.0
        total_tax = item_model.totalTax
        var quantity = 1
        val rate = item_model.dblRetailPrice.toDouble()
        val amount = quantity * rate
        var tax_amount = 0.0
        if (!cbTax.isChecked) {
            setTaxIncludedStatus(false)
        }
        tax_amount = (amount * total_tax) / 100
        val orderTransModel = OrderTransModel(
            UUID.randomUUID().toString(),
            order_id,
            item_model.iProductID,
            item_model.strProductName,
            rate,
            quantity,
            amount,
            item_model.strPricingType,
            "",
            "",
            item_model.totalTax.toString(),
            tax_amount.toString(),
            "",
            0,
            0.0,
            false,
            item_model.strProductType,
            0,
            "",
            0.0f,
            "",
            "",
            "",
            0,
            true,
            tIsOpenDiscount = item_model.tIsOpenDiscount
        )
        CV.productRequireHashMap.clear()
        if (mDatabase.daoModifierGroupList()
                .getModifierGroupList(item_model.iProductID) != null && mDatabase.daoModifierGroupList()
                .getModifierGroupList(
                    item_model.iProductID
                )!!.isNotEmpty()
        ) {
            for (i in mDatabase.daoModifierGroupList()
                .getModifierGroupList(item_model.iProductID)!!.indices) {
                if (mDatabase.daoModifierGroupList().getModifierGroupList(item_model.iProductID)
                        .get(
                            i
                        ).strIsRequired.equals(CV.REQUIRED)
                ) {
                    if (CV.productRequireHashMap.containsKey(item_model.iProductID)) {
                        CV.productRequireHashMap.put(
                            item_model.iProductID,
                            (CV.productRequireHashMap.get(item_model.iProductID)!! + 1)
                        )
                    } else {
                        CV.productRequireHashMap.put(item_model.iProductID, 1)
                    }
                }
            }
        }
        return orderTransModel
    }

    private fun getFreeProduct(itemId: Int): ItemModel {
        lateinit var itemModel: ItemModel
        for (i in itemList.indices) {
            if (itemList[i].iProductID == itemId) {
                itemModel = itemList[i]
                break
            }
        }
        return itemModel

    }

    private fun isCustomerEligible(item_model: ItemModel) {
        Log.e("==== eventName ===", "===" + eventName)
        if (orderTransList.size == 0) {
            selectedAge = null
        }
        if (eventName.equals(AppConstant.REFUND)) {
            return
        }
        if (item_model.iAgeLimit == 0) {
            itemAmountCalculation(item_model)
            return
        } else if (selectedAge != null) {
            if (selectedAge!! >= item_model.iAgeLimit) {
                itemAmountCalculation(item_model)
            } else {
                showAgeLimitMessage(selectedAge!!, item_model.iAgeLimit)
            }
            return
        }

        if (customerListViewModel?.selectedCustomer != null && !TextUtils.isEmpty(
                customerListViewModel?.selectedCustomer?.dateDob
            ) && !customerListViewModel?.isSelectedCustomerIsWalkInCustomer()!!
        ) {
            val birhthdate = CM.convertStringToDate(
                CV.WS_DATE_FORMATTE,
                customerListViewModel?.selectedCustomer?.dateDob!!
            )
            selectedAge = CM.calculateAge(birhthdate)
            if (selectedAge!! >= item_model.iAgeLimit) {
                itemAmountCalculation(item_model)
            } else {
                showAgeLimitMessage(selectedAge!!, item_model.iAgeLimit)
            }
        } else {
            if (selected_history != null) {
                if (FindSamePreviousItemQtybyItsName(
                        selected_history,
                        item_model.iProductID
                    ).equals(-1)
                ) {
                    ageVerificationConfirmationDialog(item_model) {
                        itemAmountCalculation(item_model)
                    }
                } else {
                    itemAmountCalculation(item_model)
                }
            } else {
                ageVerificationConfirmationDialog(item_model) {
                    itemAmountCalculation(item_model)
                }
            }
        }
    }

    private fun ageVerificationConfirmationDialog(
        item_model: ItemModel,
        callBack: (item_model: ItemModel) -> Unit
    ) {
        if (ageRestrictionFragment != null && ageRestrictionFragment!!.isVisible) {
            return
        }
        ageRestrictionFragment = BirthDateDialog(item_model) { calendar ->
            val formatter = SimpleDateFormat("yyyy-MM-dd")
            val formattedDate = formatter.format(calendar.time)
            mDatabase.daoCustomerList().updateCustomerDateofBirth(
                formattedDate.toString(),
                customerListViewModel?.selectedCustomer?.iCustomerId!!
            )
            selectedAge = CM.calculateAge(calendar.time);
            if (selectedAge!! >= item_model.iAgeLimit) {
                callBack(item_model)
            } else {
                showAgeLimitMessage(selectedAge!!, item_model.iAgeLimit)
            }
        }

        ageRestrictionFragment!!.isCancelable = false
        ageRestrictionFragment!!.show(mActivity.supportFragmentManager, "dialog")
    }


    private fun showAgeLimitMessage(yourage: Int, requireAge: Int) {
        if (yourage < 0) {
            CM.showMessageOK(
                mActivity,
                "",
                "You are not eligible to buy this product as this product is available for ${requireAge} year(s) and above",
                null
            )
        } else {
            CM.showMessageOK(
                mActivity,
                "",
                "You are ${yourage} year(s) old. This product is available for ${requireAge} year(s) and above",
                null
            )
        }
    }

    /**
     * Calculation of to total saving
     */
    private fun getTotalSavingAmount(cartDiscount: Float): Double {
        val totalSaleDiscount = orderTransList.map { it.discountAmount }.sum()
        return (totalSaleDiscount + cartDiscount).toDouble()
    }

    private fun getFormatedAmountWithCurrencySymbol(amount: Double) =
        "$currency_symbol ${String.format("%.2f", (amount))}"


    private fun <T> getFormatedAmountWithCurrencySymbol(amount: T) =
        "$currency_symbol ${String.format("%.2f", (amount))}"

    private fun getFormatedAmount(amount: Double) = String.format("%.2f", (amount))

    /*private fun checkIfAlreadyExist(orderTransModel: OrderTransModel, rate: Double) {
        val orderTransLiveData =
            mDatabase.daoOrderTrans().getOrderTransItem(order_id, orderTransModel.itemId)
        for (orderitem in mDatabase.daoOrderTrans().getOrderTrans(order_id))
        {
            LogUtil.d("instruction1",orderitem.strInstruction)
        }
        if (orderTransLiveData.isNotEmpty()) {
            val qty = orderTransLiveData[0].itemQty + 1
            orderTransModel.itemQty = qty
            orderTransModel.itemRate = orderTransLiveData[0].itemRate
            val total = orderTransModel.itemQty * orderTransLiveData[0].itemRate
            orderTransModel.itemAmount = total
            orderTransModel.iDiscountId = orderTransLiveData[0].iDiscountId
            orderTransModel.discountRemark = orderTransLiveData[0].discountRemark
            mDatabase.daoOrderTrans().updateOrderTrans2(
                orderTransModel.itemQty,
                orderTransModel.itemAmount,
                orderTransModel.taxAmount,
                orderTransModel.orderId,
                orderTransModel.itemId
            )
            if (orderTransModel.iDiscountId == -1 && orderTransModel.discountRemark.isNotEmpty()) {
                val orderTransLiveDataTemp =
                    mDatabase.daoOrderTrans()
                        .getOrderTransItem(orderTransModel.orderId, orderTransModel.itemId)
                openDiscountCalculation(orderTransLiveDataTemp[0])
            }
            getProductsFromDatabase(order_id)
        } else {
            mDatabase.daoOrderTrans().insertOrderTrans(orderTransModel)
            getProductsFromDatabase(order_id)
            pos=0
            if (!mDatabase.daoModifierGroupList()
                    .getModifierGroupList(orderTransList.reversed()[pos].itemId).isNullOrEmpty()
            ) {
                add_default_ingredient(orderTransList.reversed()[pos].itemId)
            }
        }
    }*/

    private fun checkIfAlreadyExist(orderTransModel: OrderTransModel, rate: Double) {
        val orderTransLiveData =
            mDatabase.daoOrderTrans().getOrderTransItem(order_id, orderTransModel.itemId)
        for (orderitem in mDatabase.daoOrderTrans().getOrderTrans(order_id)) {
            LogUtil.d("instruction1", orderitem.strInstruction)
        }
        if (orderTransLiveData.isNotEmpty()) {
            if (TextUtils.equals(CV.PRODUCTS_TYPE_KITCHEN, orderTransModel.strProductType) && mDatabase.daoModifierGroupList()
                    .getModifierGroupList(orderTransModel.itemId).size > 0) {
                DialogUtility.openDoubleActionModifierDialog(
                    mActivity,
                    getString(R.string.modifier_two_btn_dialog_message),
                    orderTransModel.itemName,
                    orderTransLiveData.last().ingredientName
                ) {
                    if (it) {
                        order_product_id = orderTransModel.id
                        mDatabase.daoOrderTrans().insertOrderTrans(orderTransModel)
                        getProductsFromDatabase(order_id)
                        pos = 0
                        if (!mDatabase.daoModifierGroupList()
                                .getModifierGroupList(orderTransModel.itemId).isNullOrEmpty()
                        ) {
                            add_default_ingredient(orderTransModel.itemId)
                        }
                        if (mDatabase.daoModifierGroupList()
                                .getModifierGroupList(orderTransModel.itemId) != null && mDatabase.daoModifierGroupList()
                                .getModifierGroupList(orderTransModel.itemId) !!.isNotEmpty()
                        ) {
                            for (i in mDatabase.daoModifierGroupList()
                                .getModifierGroupList(orderTransModel.itemId) !!.indices) {
                                if (mDatabase.daoModifierGroupList()
                                        .getModifierGroupList(orderTransModel.itemId)
                                        .get(i).strIsRequired.equals(CV.REQUIRED) || mDatabase.daoModifierGroupList()
                                        .getModifierGroupList(orderTransModel.itemId)
                                        .get(i).strIsRequired.equals(CV.FORCE_SHOW_OPTIONAL)
                                ) {
                                    /*var tempList = orderTransList
                                    pos = tempList.reversed().indexOfFirst {
                                        it.itemId == orderTransModel.itemId
                                    }
                                    if (pos == -1) {
                                        pos = 0
                                    }*/
                                    item_id = orderTransModel.itemId
                                    //order_product_id=orderTransModel.id
                                    mBinding.tvNoProductFound.visibility = View.GONE
                                    openModifierLayout(orderTransModel.itemId, orderTransModel.itemName)
                                    break
                                }
                            }
                        }
                        /*if (selected_history != null) {
                            pos = FindSamePreviousItemQtybyItsName(selected_history, orderTransModel.itemId)
                            if (pos != -1) {
                                if (selected_history!!.orderProducts?.get(pos)!!.isKitchenPrintServed) {
                                    if (mDatabase.daoModifierGroupList()
                                            .getModifierGroupList(orderTransModel.itemId) != null && mDatabase.daoModifierGroupList()
                                            .getModifierGroupList(orderTransModel.itemId)!!.isNotEmpty()
                                    ) {
                                        for (i in mDatabase.daoModifierGroupList()
                                            .getModifierGroupList(orderTransModel.itemId)!!.indices) {
                                            if (mDatabase.daoModifierGroupList()
                                                    .getModifierGroupList(orderTransModel.itemId)
                                                    .get(i).strIsRequired.equals(CV.REQUIRED) || mDatabase.daoModifierGroupList()
                                                    .getModifierGroupList(orderTransModel.itemId)
                                                    .get(i).strIsRequired.equals(CV.FORCE_SHOW_OPTIONAL)
                                            ) {
                                                var tempList = orderTransList
                                                pos = tempList.reversed().indexOfFirst {
                                                    it.itemId == orderTransModel.itemId
                                                }
                                                if (pos == -1) {
                                                    pos = 0
                                                }
                                                item_id = orderTransModel.itemId
                                                //order_product_id=orderTransModel.id
                                                mBinding.tvNoProductFound.visibility = View.GONE
                                                openModifierLayout(
                                                    orderTransModel.itemId,
                                                    orderTransModel.itemName
                                                )
                                                break
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (mDatabase.daoModifierGroupList()
                                        .getModifierGroupList(orderTransModel.itemId) != null && mDatabase.daoModifierGroupList()
                                        .getModifierGroupList(orderTransModel.itemId)!!.isNotEmpty()
                                ) {
                                    for (i in mDatabase.daoModifierGroupList()
                                        .getModifierGroupList(orderTransModel.itemId)!!.indices) {
                                        if (mDatabase.daoModifierGroupList()
                                                .getModifierGroupList(orderTransModel.itemId)
                                                .get(i).strIsRequired.equals(CV.REQUIRED) || mDatabase.daoModifierGroupList()
                                                .getModifierGroupList(orderTransModel.itemId)
                                                .get(i).strIsRequired.equals(CV.FORCE_SHOW_OPTIONAL)
                                        ) {
                                            var tempList = orderTransList
                                            pos = tempList.reversed().indexOfFirst {
                                                it.itemId == orderTransModel.itemId
                                            }
                                            if (pos == -1) {
                                                pos = 0
                                            }
                                            item_id = orderTransModel.itemId
                                            //order_product_id=orderTransModel.id
                                            mBinding.tvNoProductFound.visibility = View.GONE
                                            openModifierLayout(orderTransModel.itemId, orderTransModel.itemName)
                                            break
                                        }
                                    }
                                }
                            }
                        } else {
                            if (mDatabase.daoModifierGroupList()
                                    .getModifierGroupList(orderTransModel.itemId) != null && mDatabase.daoModifierGroupList()
                                    .getModifierGroupList(orderTransModel.itemId)!!.isNotEmpty()
                            ) {
                                for (i in mDatabase.daoModifierGroupList()
                                    .getModifierGroupList(orderTransModel.itemId)!!.indices) {
                                    if (mDatabase.daoModifierGroupList()
                                            .getModifierGroupList(orderTransModel.itemId)
                                            .get(i).strIsRequired.equals(CV.REQUIRED) || mDatabase.daoModifierGroupList()
                                            .getModifierGroupList(orderTransModel.itemId)
                                            .get(i).strIsRequired.equals(CV.FORCE_SHOW_OPTIONAL)
                                    ) {
                                        var tempList = orderTransList
                                        pos = tempList.reversed().indexOfFirst {
                                            it.itemId == orderTransModel.itemId
                                        }
                                        if (pos == -1) {
                                            pos = 0
                                        }

                                        item_id = orderTransModel.itemId
                                        //order_product_id=orderTransModel.id
                                        mBinding.tvNoProductFound.visibility = View.GONE
                                        openModifierLayout(orderTransModel.itemId, orderTransModel.itemName)
                                        break
                                    }
                                }
                            }
                        }*/
                    } else {
                        order_product_id = orderTransLiveData.reversed()[0].id
                        val qty = orderTransLiveData.reversed()[0].itemQty + 1
                        orderTransModel.itemQty = qty
                        orderTransModel.itemRate = orderTransLiveData.reversed()[0].itemRate
                        val total = orderTransModel.itemQty * orderTransLiveData.reversed()[0].itemRate
                        orderTransModel.itemAmount = total
                        orderTransModel.iDiscountId = orderTransLiveData.reversed()[0].iDiscountId
                        orderTransModel.discountRemark = orderTransLiveData.reversed()[0].discountRemark
                        mDatabase.daoOrderTrans().updateOrderTrans2_(
                            orderTransModel.itemQty,
                            orderTransModel.itemAmount,
                            orderTransModel.taxAmount,
                            orderTransModel.orderId,
                            orderTransModel.itemId,
                            orderTransLiveData.reversed()[0].id
                        )
                        if (orderTransModel.iDiscountId == -1 && orderTransModel.discountRemark.isNotEmpty()) {
                            val orderTransLiveDataTemp =
                                mDatabase.daoOrderTrans()
                                    .getOrderTransItem(orderTransModel.orderId, orderTransModel.itemId)
                            openDiscountCalculation(orderTransLiveDataTemp.reversed()[0])
                        }

                        getProductsFromDatabase(order_id)
                    }
                }
            } else {
                order_product_id = orderTransLiveData[0].id
                val qty = orderTransLiveData[0].itemQty + 1
                orderTransModel.itemQty = qty
                orderTransModel.itemRate = orderTransLiveData[0].itemRate
                val total = orderTransModel.itemQty * orderTransLiveData[0].itemRate
                orderTransModel.itemAmount = total
                orderTransModel.iDiscountId = orderTransLiveData[0].iDiscountId
                orderTransModel.discountRemark = orderTransLiveData[0].discountRemark
                mDatabase.daoOrderTrans().updateOrderTrans2_(
                    orderTransModel.itemQty,
                    orderTransModel.itemAmount,
                    orderTransModel.taxAmount,
                    orderTransModel.orderId,
                    orderTransModel.itemId,
                    orderTransLiveData[0].id
                )
                if (orderTransModel.iDiscountId == -1 && orderTransModel.discountRemark.isNotEmpty()) {
                    val orderTransLiveDataTemp =
                        mDatabase.daoOrderTrans()
                            .getOrderTransItem(orderTransModel.orderId, orderTransModel.itemId)
                    openDiscountCalculation(orderTransLiveDataTemp[0])
                }

                getProductsFromDatabase(order_id)
            }
        } else {
            order_product_id = orderTransModel.id
            mDatabase.daoOrderTrans().insertOrderTrans(orderTransModel)
            getProductsFromDatabase(order_id)
            pos = 0
            if (!mDatabase.daoModifierGroupList()
                    .getModifierGroupList(orderTransModel.itemId).isNullOrEmpty()
            ) {
                add_default_ingredient(orderTransModel.itemId)
            }
            if (mDatabase.daoModifierGroupList()
                    .getModifierGroupList(orderTransModel.itemId) != null && mDatabase.daoModifierGroupList()
                    .getModifierGroupList(orderTransModel.itemId) !!.isNotEmpty()
            ) {
                for (i in mDatabase.daoModifierGroupList()
                    .getModifierGroupList(orderTransModel.itemId) !!.indices) {
                    if (mDatabase.daoModifierGroupList()
                            .getModifierGroupList(orderTransModel.itemId)
                            .get(i).strIsRequired.equals(CV.REQUIRED) || mDatabase.daoModifierGroupList()
                            .getModifierGroupList(orderTransModel.itemId)
                            .get(i).strIsRequired.equals(CV.FORCE_SHOW_OPTIONAL)
                    ) {
                        /*var tempList = orderTransList
                        pos = tempList.reversed().indexOfFirst {
                            it.itemId == orderTransModel.itemId
                        }
                        if (pos == -1) {
                            pos = 0
                        }*/
                        item_id = orderTransModel.itemId
                        //order_product_id=orderTransModel.id
                        mBinding.tvNoProductFound.visibility = View.GONE
                        openModifierLayout(orderTransModel.itemId, orderTransModel.itemName)
                        break
                    }
                }
            }
            /*if (selected_history != null) {
                pos = FindSamePreviousItemQtybyItsName(selected_history, orderTransModel.itemId)
                if (pos != -1) {
                    if (selected_history!!.orderProducts?.get(pos)!!.isKitchenPrintServed) {
                        if (mDatabase.daoModifierGroupList()
                                .getModifierGroupList(orderTransModel.itemId) != null && mDatabase.daoModifierGroupList()
                                .getModifierGroupList(orderTransModel.itemId)!!.isNotEmpty()
                        ) {
                            for (i in mDatabase.daoModifierGroupList()
                                .getModifierGroupList(orderTransModel.itemId)!!.indices) {
                                if (mDatabase.daoModifierGroupList()
                                        .getModifierGroupList(orderTransModel.itemId)
                                        .get(i).strIsRequired.equals(CV.REQUIRED) || mDatabase.daoModifierGroupList()
                                        .getModifierGroupList(orderTransModel.itemId)
                                        .get(i).strIsRequired.equals(CV.FORCE_SHOW_OPTIONAL)
                                ) {
                                    var tempList = orderTransList
                                    pos = tempList.reversed().indexOfFirst {
                                        it.itemId == orderTransModel.itemId
                                    }
                                    if (pos == -1) {
                                        pos = 0
                                    }
                                    item_id = orderTransModel.itemId
                                    //order_product_id=orderTransModel.id
                                    mBinding.tvNoProductFound.visibility = View.GONE
                                    openModifierLayout(
                                        orderTransModel.itemId,
                                        orderTransModel.itemName
                                    )
                                    break
                                }
                            }
                        }
                    }
                } else {
                    if (mDatabase.daoModifierGroupList()
                            .getModifierGroupList(orderTransModel.itemId) != null && mDatabase.daoModifierGroupList()
                            .getModifierGroupList(orderTransModel.itemId)!!.isNotEmpty()
                    ) {
                        for (i in mDatabase.daoModifierGroupList()
                            .getModifierGroupList(orderTransModel.itemId)!!.indices) {
                            if (mDatabase.daoModifierGroupList()
                                    .getModifierGroupList(orderTransModel.itemId)
                                    .get(i).strIsRequired.equals(CV.REQUIRED) || mDatabase.daoModifierGroupList()
                                    .getModifierGroupList(orderTransModel.itemId)
                                    .get(i).strIsRequired.equals(CV.FORCE_SHOW_OPTIONAL)
                            ) {
                                var tempList = orderTransList
                                pos = tempList.reversed().indexOfFirst {
                                    it.itemId == orderTransModel.itemId
                                }
                                if (pos == -1) {
                                    pos = 0
                                }
                                item_id = orderTransModel.itemId
                                //order_product_id=orderTransModel.id
                                mBinding.tvNoProductFound.visibility = View.GONE
                                openModifierLayout(orderTransModel.itemId, orderTransModel.itemName)
                                break
                            }
                        }
                    }
                }
            } else {
                if (mDatabase.daoModifierGroupList()
                        .getModifierGroupList(orderTransModel.itemId) != null && mDatabase.daoModifierGroupList()
                        .getModifierGroupList(orderTransModel.itemId)!!.isNotEmpty()
                ) {
                    for (i in mDatabase.daoModifierGroupList()
                        .getModifierGroupList(orderTransModel.itemId)!!.indices) {
                        if (mDatabase.daoModifierGroupList()
                                .getModifierGroupList(orderTransModel.itemId)
                                .get(i).strIsRequired.equals(CV.REQUIRED) || mDatabase.daoModifierGroupList()
                                .getModifierGroupList(orderTransModel.itemId)
                                .get(i).strIsRequired.equals(CV.FORCE_SHOW_OPTIONAL)
                        ) {
                            var tempList = orderTransList
                            pos = tempList.reversed().indexOfFirst {
                                it.itemId == orderTransModel.itemId
                            }
                            if (pos == -1) {
                                pos = 0
                            }

                            item_id = orderTransModel.itemId
                            //order_product_id=orderTransModel.id
                            mBinding.tvNoProductFound.visibility = View.GONE
                            openModifierLayout(orderTransModel.itemId, orderTransModel.itemName)
                            break
                        }
                    }
                }
            }*/
        }

    }

    private fun addItemToCart(orderTransModel: OrderTransModel): OrderTransModel {
        var qty = orderTransModel.itemQty
        ++qty
        val item = mDatabase.daoItemList().getItem(orderTransModel.itemId)
        orderTransModel.itemQty = qty
        orderTransModel.itemAmount = qty * orderTransModel.itemRate
        val tax_amount = (orderTransModel.itemRate * item.totalTax) / 100

        return orderTransModel
    }
    private fun addManuallyItemToCart(orderTransModel: OrderTransModel,mqty:Int): OrderTransModel {
        //var qty = orderTransModel.itemQty + mqty
        var mpriviousitemqty = 1
        if (TextUtils.equals(orderTransModel.strProductType,CV.PRODUCTS_TYPE_KITCHEN)) {
            if (mqty >= orderTransModel.kitchenServedquantity) {
                orderTransModel.itemQty = mqty
                orderTransModel.itemAmount = mqty * orderTransModel.itemRate
            } else {
                showToast(
                    mActivity,
                    "You can't decrease qty of kitchen products which is served",
                    R.drawable.icon,
                    custom_toast_container
                )
            }
        }else{
            orderTransModel.itemQty = mqty
            orderTransModel.itemAmount = mqty * orderTransModel.itemRate
        }
        /*if (!orderTransModel.isKitchenPrintServed) {
            if (selected_history != null) {
                var pos = FindSamePreviousItemQtybyItsName(
                    selected_history,
                    orderTransModel.itemId
                )
                mpriviousitemqty =
                    selected_history!!.orderProducts?.get(pos)!!.dblItemQty.toInt()
            }
            if (mqty > mpriviousitemqty) {
                orderTransModel.itemQty = mqty
                orderTransModel.itemAmount = mqty * orderTransModel.itemRate
            }
            else {
                showToast(
                    mActivity,
                    "You can't decrease qty of kitchen products which is served",
                    R.drawable.icon,
                    custom_toast_container
                )
            }
        }else {
            orderTransModel.itemQty = mqty
            orderTransModel.itemAmount = mqty * orderTransModel.itemRate
        }*/

        return orderTransModel
    }

    private fun removeItemFromCart(orderTransModel: OrderTransModel): OrderTransModel {
        var qty = orderTransModel.itemQty
        --qty
        val item = mDatabase.daoItemList().getItem(orderTransModel.itemId)
        val tax_amount = (orderTransModel.itemRate * item.totalTax) / 100

        orderTransModel.itemQty = qty
        orderTransModel.itemAmount = qty * orderTransModel.itemRate
        return orderTransModel
    }

    private fun productNotFoundDialog(callBack: () -> Unit) {
        if (mActivity != null) {
            val dialog = Dialog(mActivity)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.window!!.setCallback(
                    UserInteractionAwareCallback(
                            dialog.window!!.getCallback(),
                            mActivity
                    )
            )
            dialog.setContentView(R.layout.dialog_product_not_found)

            dialog.setCancelable(false)
            val btnOk = dialog.findViewById<Button>(R.id.btnOk)

            btnOk.setOnClickListener {
                dialog.dismiss()
                callBack()

            }
            dialog.show()
        }
    }
    private fun ErrorDialog(error_msg:String) {
        if (mActivity != null) {
            val dialog = Dialog(mActivity)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.window!!.setCallback(
                UserInteractionAwareCallback(
                    dialog.window!!.getCallback(),
                    mActivity
                )
            )
            dialog.setContentView(R.layout.error_dialog)

            dialog.setCancelable(false)
            val btnOk = dialog.findViewById<Button>(R.id.btnOk)
            val errortxt=dialog.findViewById<TextView>(R.id.error_txt);
            errortxt.text=error_msg

            btnOk.setOnClickListener {
                dialog.dismiss()
            }
            dialog.show()
        }
    }

    private fun HoldOrderErrorDialog(error_msg:String) {
        if (mActivity != null) {
            val dialog = Dialog(mActivity)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.window!!.setCallback(
                UserInteractionAwareCallback(
                    dialog.window!!.getCallback(),
                    mActivity
                )
            )
            dialog.setContentView(R.layout.error_dialog)

            dialog.setCancelable(false)
            val btnOk = dialog.findViewById<Button>(R.id.btnOk)
            val errortxt=dialog.findViewById<TextView>(R.id.error_txt);
            errortxt.text=error_msg

            btnOk.setOnClickListener {
                if (dialogcheckout!=null) {
                    if (dialogcheckout!!.isShowing) {
                        dialogcheckout!!.dismiss()
                    }
                }
                dialog.dismiss()
                doResetCart()
                //Hardware Code For CDS Display -- Newsale put Order on Hold case invoke
                DisplayNewSaleDataToCDS(ArrayList(), "", false)

                total = 0.0
                total_tax_amount = 0.0
                CM.setSp(mActivity, CV.ORDER_ID, "")
                displayCalculation(00.00, 00.00, 0, 0.0f)
                setEvent(AppConstant.ORDER)
                customerListViewModel?.selectedCustomer = customerListViewModel?.walkInCustomer
                setCustomerName()
                adapter(orderTransList, eventName)
                order_id = ""
                temp_order_id = ""
                selectedAge = null
                mBinding.cbTax.isChecked = true
                CM.setSp(mActivity, CV.ORDER_ID, order_id)
                selected_history = null
                discountListViewModel?.selectedCartDiscount = null
                clearDiscountData()
                mBinding.ivAddDiscount.visibility = View.VISIBLE

                mBinding.ivCancel.isEnabled = true
                mBinding.gridItem.visibility = View.VISIBLE
                mBinding.layoutModifier.visibility = View.GONE

                mBinding.ivAddCustomer.isEnabled = true
                mBinding.ivAdd.visibility = View.VISIBLE

            }
            dialog.show()
        }
    }

    private fun getProductsFromDatabase(orderId: String) {

        val orderTransLiveData = mDatabase.daoOrderTrans().getOrderTrans(orderId)
        if (orderTransLiveData != null) {
            if (orderTransList.size > 0) {
                orderTransList.clear()
            }
            orderTransList = orderTransLiveData as ArrayList<OrderTransModel>
            discountMixMatchCalculation() {
                val test = orderTransList.asReversed()
                setEvent(AppConstant.ORDER)
                adapter(test, eventName)
                if (!searchViewModel?.isSearching!!)
                    (mBinding.rvCart.adapter as OrderItemListAdapter).notifyDataSetChanged()
                getTotalAmount(test)
                //Hardware Code For CDS Display -- NewSale While select any item to create new order case invoke
                DisplayNewSaleDataToCDS(
                    (mBinding.rvCart.adapter as OrderItemListAdapter).getDataset(),
                    "",
                    false
                )
                LogUtil.d("ProductEnd_time",System.currentTimeMillis().toString())
                mBinding.btnCheckout.isEnabled=true
            }
        }
    }

    fun add_default_ingredient(item_id: Int) {
        for (selected_modifier in mDatabase.daoModifierGroupList()
            .getModifierGroupList(item_id)) {
            val selected_pre_modifier = CV.DEFAULT
            for (selected_ingredient in mDatabase.daoIngredientList()
                .getIngredientList(selected_modifier.iModifierGroupId.toString(), item_id)) {
                if (selected_ingredient.tIsDefault == 1) {
                    LogUtil.e("selected_pre_modifier", selected_ingredient.strOptionName)
                    if (selected_ingredient!!.strOptionName.equals("")) {
                        return
                    }
                    var modifierMap = HashMap<String,
                            List<IngredientMasterModel>>()
                    /*val stringHashMap =
                        mDatabase.daoOrderTrans().getPreModifierTypeTemp(order_id, item_id)*/
                    val stringHashMap =
                        mDatabase.daoOrderTrans().getPreModifierTypeTemp_(order_id, item_id,order_product_id)
                    LogUtil.e("StringHashmap", stringHashMap);
                    if (!TextUtils.isEmpty(stringHashMap)) {
                        // method to convert string to hasMap
                        val turnsType = object :
                            TypeToken<HashMap<String, List<IngredientMasterModel>>>() {}.type
                        modifierMap =
                            Gson().fromJson<HashMap<String, List<IngredientMasterModel>>>(
                                stringHashMap,
                                turnsType
                            )
                    }

                    var ingredientListTemp = ArrayList<IngredientMasterModel>()
                    var tt = ArrayList<IngredientMasterModel>()
                    val updatedItem = orderTransList.reversed()[pos]
                    LogUtil.e("UpdateItem", updatedItem.itemName)
                    selected_pre_modifier?.let {
                        val sectedPreModiferNameTemp = it
                        if (modifierMap.containsKey(it)) {
                            ingredientListTemp =
                                modifierMap.get(it) as ArrayList<IngredientMasterModel>

                            selected_ingredient?.let {
                                if (ingredientListTemp.indexOf(it) == -1) {
                                    //  ingredientList.add(it)
                                    var tempValueList = ArrayList<IngredientMasterModel>()
                                    for ((k, v) in modifierMap) {
                                        tempValueList.addAll(v)
                                    }

                                    if (ingredientListTemp.isNotEmpty()) {
                                        for (i in ingredientListTemp.indices) {
                                            if (selected_modifier!!.iModifierGroupId.equals(
                                                    ingredientListTemp.get(
                                                        i
                                                    ).iModifierGroupId
                                                )
                                            ) {
                                                tt.add(ingredientListTemp.get(i))
                                            }
                                        }
                                    }

                                    var tempValueAllList = ArrayList<IngredientMasterModel>()
                                    for ((k, v) in modifierMap) {
                                        if (k != CV.DEFAULT)
                                            tempValueAllList.addAll(v)
                                    }
                                    var tempFiltredGroupList =
                                        tempValueAllList.filter { item ->
                                            item.iModifierGroupId == selected_modifier!!.iModifierGroupId
                                        }
                                    if (tempFiltredGroupList.size < selected_modifier!!.iOptionMax || sectedPreModiferNameTemp == CV.DEFAULT) {
                                        ingredientListTemp.add(it)
                                    }
                                }
                            }

                        } else {
                            selected_ingredient?.let {
                                if (modifierMap.size > 0) {
                                    var tempValueList = ArrayList<IngredientMasterModel>()
                                    for ((k, v) in modifierMap) {
                                        tempValueList.addAll(v)
                                    }

                                }
                                if (ingredientListTemp.indexOf(it) == -1) {
                                    if (ingredientListTemp != null && ingredientListTemp.size > 0) {
                                        for (i in ingredientListTemp.indices) {
                                            if (selected_modifier!!.iModifierGroupId.equals(
                                                    ingredientListTemp.get(
                                                        i
                                                    ).iModifierGroupId
                                                )
                                            ) {
                                                tt.add(ingredientListTemp.get(i))
                                            }
                                        }
                                    }
                                    var tempValueAllList = ArrayList<IngredientMasterModel>()
                                    for ((k, v) in modifierMap) {
                                        if (k != CV.DEFAULT)
                                            tempValueAllList.addAll(v)
                                    }
                                    var tempFiltredGroupList =
                                        tempValueAllList.filter { item ->
                                            item.iModifierGroupId == selected_modifier!!.iModifierGroupId
                                        }
                                    if (tempFiltredGroupList.size < selected_modifier!!.iOptionMax || sectedPreModiferNameTemp == CV.DEFAULT) {
                                        ingredientListTemp.add(it)
                                    }
                                }
                            }
                        }
                        modifierMap.set(it, ingredientListTemp)
                        for (i in mDatabase.daoModifierGroupList().getModifierGroupList(item_id)!!.indices) {
                            if (mDatabase.daoModifierGroupList()
                                            .getModifierGroupList(item_id)
                                            .get(i).strIsRequired.equals(CV.REQUIRED) || mDatabase.daoModifierGroupList()
                                            .getModifierGroupList(item_id)
                                            .get(i).strIsRequired.equals(CV.FORCE_SHOW_OPTIONAL)) {
                                //ingredientHighlitedOpreation(modifierMap)
                                break
                            }
                        }

                        val gson = GsonBuilder().create()
                        val ingredientDataInStringFormate = gson.toJson(modifierMap)

                        updatedItem.premodifierType = ingredientDataInStringFormate
                        updatedItem.ingredientName = GetModifierIngredients(modifierMap)

                        /*mDatabase.daoOrderTrans()
                            .updateModifier(
                                ingredientDataInStringFormate,
                                GetModifierIngredients(modifierMap),
                                order_id,
                                item_id
                            )*/
                        mDatabase.daoOrderTrans()
                            .updateModifier_(
                                ingredientDataInStringFormate,
                                GetModifierIngredients(modifierMap),
                                order_id,
                                item_id,
                                order_product_id
                            )
                    }
                    selected_pre_modifier?.let { preModiifer ->
                        if (preModiifer.equals(CV.DEFAULT)) {
                            openDiscountCalculation(updatedItem)
                            discountMixMatchCalculation() {
                                cartAdapter?.updateItem(pos, updatedItem)
                                val tempReverseList = orderTransList.reversed()
                                tempReverseList.get(pos).id = updatedItem.id
                                tempReverseList.get(pos).orderId = updatedItem.orderId
                                tempReverseList.get(pos).itemId = updatedItem.itemId
                                tempReverseList.get(pos).itemName = updatedItem.itemName
                                tempReverseList.get(pos).itemRate = updatedItem.itemRate
                                tempReverseList.get(pos).itemQty = updatedItem.itemQty
                                tempReverseList.get(pos).itemAmount = updatedItem.itemAmount
                                tempReverseList.get(pos).strPricingType =
                                    updatedItem.strPricingType
                                tempReverseList.get(pos).paymentType = updatedItem.paymentType
                                tempReverseList.get(pos).dateTime = updatedItem.dateTime
                                tempReverseList.get(pos).taxPercentage =
                                    updatedItem.taxPercentage
                                tempReverseList.get(pos).taxAmount = updatedItem.taxAmount
                                tempReverseList.get(pos).reason = updatedItem.reason
                                tempReverseList.get(pos).itemQtyLeft = updatedItem.itemQtyLeft
                                tempReverseList.get(pos).grandTotal = updatedItem.grandTotal
                                tempReverseList.get(pos).premodifierType =
                                    updatedItem.premodifierType
                                tempReverseList.get(pos).ingredientName =
                                    updatedItem.ingredientName

                                mDatabase.daoOrderTrans()
                                    .updateOrderTrans(orderTransList.get(pos))
                                this.calculateCartProcedure(tempReverseList, true) {}
                                //Hardware Code For CDS Display -- NewSale While select any ingredient
                                DisplayNewSaleDataToCDS(orderTransList, "", false)
                            }
                        }

                    }

                }

            }

        }
    }

    private fun DisplayNewSaleDataToCDS(
        mOrderTransModel: List<OrderTransModel>,
        mDisplayMsg: String,
        isCancelDialogShow: Boolean
    ) {

        val date = Calendar.getInstance().time
        val sdtf = SimpleDateFormat(CV.DISPLAY_DATE_FORMATTE_AMPM)

        var mCDSSalesProducts: ArrayList<PrinterSalesProductsModel> = ArrayList()

        val mCDSSalesModel = CDSSalesModel()

        if (mOrderTransModel.isNotEmpty()) {
            for (i in mOrderTransModel.indices) {
                val orderTransModel = mOrderTransModel[i]

                val itemQty = orderTransModel.itemQty
                val itemRate = orderTransModel.itemRate
                var itemPrice = itemQty * itemRate

                itemPrice -= NewSaleFragmentCommon.floatToDouble(orderTransModel.discountAmount)
                val mPrinterSalesProductsModel = PrinterSalesProductsModel()

                mPrinterSalesProductsModel.setProductName(orderTransModel.itemName)
                mPrinterSalesProductsModel.setQuanity(itemQty.toString())
                mPrinterSalesProductsModel.setRate(String.format("%.2f", (itemRate)))
                mPrinterSalesProductsModel.setDiscount(
                    currency_symbol + " " + String.format(
                        "%.2f",
                        orderTransModel.discountAmount
                    )
                )
                mPrinterSalesProductsModel.setDiscountRemark(orderTransModel.discountRemark)

                mPrinterSalesProductsModel.setModifierIngredient(orderTransModel.ingredientName)

                mPrinterSalesProductsModel.setTotal(String.format("%.2f", (itemPrice)))
                mPrinterSalesProductsModel.setInstruction(orderTransModel.strInstruction);
                mCDSSalesProducts.add(mPrinterSalesProductsModel)
            }

            mCDSSalesModel.setOrderId(mOrderTransModel.get(0).orderId)

            mCDSSalesModel.setCashierName(emp_role + " - " + emp_name)
            mCDSSalesModel.setSaleDateTime(sdtf.format(date).toString())
            mCDSSalesModel.setBilledPersonName(getCustomerName())

            mCDSSalesModel.setDiscountRemark(cbDiscountApplied.text.toString())

            this.calculateCartProcedure(orderTransList, true) { cart: CartSummaryData ->

                var isTaxIncluded = true
                if (selected_history != null) {
                    isTaxIncluded = !selected_history!!.tCheckTaxExempt.equals("0")
                }
                var final_tax_anount = 00.00
                if (isTaxIncluded) {
                    final_tax_anount = cart.taxAmount
                } else {
                    final_tax_anount = 00.00
                }
                var totalPayableAmount = 00.00
                var cartDiscount = NewSaleFragmentCommon.floatToDouble(cart.totalDiscount)

                Log.e("DEBUG", "cartDiscount -- " + cartDiscount)

                if (customerListViewModel?.isTaxIncluded?.value!!) {
                    mCDSSalesModel.setTaxApplied(
                        getFormatedAmountWithCurrencySymbol(
                            final_tax_anount
                        )
                    )
                    totalPayableAmount = cart.totalAmount + final_tax_anount/* - cartDiscount*/
                } else {
                    mCDSSalesModel.setTaxApplied(getFormatedAmountWithCurrencySymbol(00.00))
                    totalPayableAmount = cart.totalAmount /*- cartDiscount*/
                }

                mCDSSalesModel.setSubtotal(
                    currency_symbol + " " + String.format(
                        "%.2f",
                        (cart.totalAmount + cart.totalDiscount)
                    )
                )

                mCDSSalesModel.setGrandTotal(
                    currency_symbol + " " + String.format(
                        "%.2f",
                        (totalPayableAmount)
                    )
                )
                mCDSSalesModel.setDiscount(mBinding.tvDiscountValue.text.toString())

                mCDSSalesModel.setTotalSavings(
                    getFormatedAmountWithCurrencySymbol(
                        getTotalSavingAmount(
                            cart.totalDiscount
                        )
                    )
                )

            }
        }
        mCDSSalesModel.setSalesProducts(mCDSSalesProducts)


        var baseActivityPrint = (activity as ParentActivity)
        baseActivityPrint.checkDrawOverlayPermission(
            AppConstant.CDS_NEWSALE_SCREEN, mDisplayMsg,
            false,
            "",
            mCDSSalesModel,
            "", isCancelDialogShow
        )
    }
    private fun DisplayNewSaleDataProcessToCDS(
        mOrderTransModel: List<OrderTransModel>,
        mDisplayMsg: String,
        isCancelDialogShow: Boolean,
        change_due_str : String
    ) {

        val date = Calendar.getInstance().time
        val sdtf = SimpleDateFormat(CV.DISPLAY_DATE_FORMATTE_AMPM)

        var mCDSSalesProducts: ArrayList<PrinterSalesProductsModel> = ArrayList()

        val mCDSSalesModel = CDSSalesModel()

        if (mOrderTransModel.isNotEmpty()) {
            for (i in mOrderTransModel.indices) {
                val orderTransModel = mOrderTransModel[i]

                val itemQty = orderTransModel.itemQty
                val itemRate = orderTransModel.itemRate
                var itemPrice = itemQty * itemRate

                itemPrice -= NewSaleFragmentCommon.floatToDouble(orderTransModel.discountAmount)
                val mPrinterSalesProductsModel = PrinterSalesProductsModel()

                mPrinterSalesProductsModel.setProductName(orderTransModel.itemName)
                mPrinterSalesProductsModel.setQuanity(itemQty.toString())
                mPrinterSalesProductsModel.setRate(String.format("%.2f", (itemRate)))
                mPrinterSalesProductsModel.setDiscount(
                    currency_symbol + " " + String.format(
                        "%.2f",
                        orderTransModel.discountAmount
                    )
                )
                mPrinterSalesProductsModel.setDiscountRemark(orderTransModel.discountRemark)

                mPrinterSalesProductsModel.setModifierIngredient(orderTransModel.ingredientName)

                mPrinterSalesProductsModel.setTotal(String.format("%.2f", (itemPrice)))
                mCDSSalesProducts.add(mPrinterSalesProductsModel)
            }

            mCDSSalesModel.setOrderId(mOrderTransModel.get(0).orderId)

            mCDSSalesModel.setCashierName(emp_role + " - " + emp_name)
            mCDSSalesModel.setSaleDateTime(sdtf.format(date).toString())
            mCDSSalesModel.setBilledPersonName(getCustomerName())

            mCDSSalesModel.setDiscountRemark(cbDiscountApplied.text.toString())

            this.calculateCartProcedure(orderTransList, true) { cart: CartSummaryData ->

                var isTaxIncluded = true
                if (selected_history != null) {
                    isTaxIncluded = !selected_history!!.tCheckTaxExempt.equals("0")
                }
                var final_tax_anount = 00.00
                if (isTaxIncluded) {
                    final_tax_anount = cart.taxAmount
                } else {
                    final_tax_anount = 00.00
                }
                var totalPayableAmount = 00.00
                var cartDiscount = NewSaleFragmentCommon.floatToDouble(cart.totalDiscount)

                Log.e("DEBUG", "cartDiscount -- " + cartDiscount)

                if (customerListViewModel?.isTaxIncluded?.value!!) {
                    mCDSSalesModel.setTaxApplied(
                        getFormatedAmountWithCurrencySymbol(
                            final_tax_anount
                        )
                    )
                    totalPayableAmount = cart.totalAmount + final_tax_anount/* - cartDiscount*/
                } else {
                    mCDSSalesModel.setTaxApplied(getFormatedAmountWithCurrencySymbol(00.00))
                    totalPayableAmount = cart.totalAmount /*- cartDiscount*/
                }

                mCDSSalesModel.setSubtotal(
                    currency_symbol + " " + String.format(
                        "%.2f",
                        (cart.totalAmount + cart.totalDiscount)
                    )
                )

                mCDSSalesModel.setGrandTotal(
                    currency_symbol + " " + String.format(
                        "%.2f",
                        (totalPayableAmount)
                    )
                )
                mCDSSalesModel.setDiscount(mBinding.tvDiscountValue.text.toString())

                mCDSSalesModel.setTotalSavings(
                    getFormatedAmountWithCurrencySymbol(
                        getTotalSavingAmount(
                            cart.totalDiscount
                        )
                    )
                )

            }
        }
        mCDSSalesModel.setSalesProducts(mCDSSalesProducts)
        mCDSSalesModel.setChange_due(change_due_str)


        var baseActivityPrint = (activity as ParentActivity)
        baseActivityPrint.checkDrawOverlayPermission(
            AppConstant.CDS_PAYMENTPROCESS_SCREEN, mDisplayMsg,
            false,
            "",
            mCDSSalesModel,
            "", isCancelDialogShow
        )
    }

    private fun populateSubcategory(iProductCategoryID: Int) {
        val subcategoryListLiveData =
            mDatabase.daoSubCategoryList().getSubCategories(iProductCategoryID)
        subcategoryListLiveData.observe(this, androidx.lifecycle.Observer { subcategorylist ->
            if (subcategorylist != null && subcategorylist.size > 0) {
                subcategoryList = subcategorylist as ArrayList<SubCategoryModel>
                mBinding.rvsubCategory.visibility = View.VISIBLE
                mBinding.tvNoSubcategoryFound.visibility = View.GONE
                mBinding.rvsubCategory.adapter =
                    SubCategoryListAdapter(mActivity, subcategoryList, this)
                mBinding.tvsubCategoryValue.text = subcategoryList.get(0).strProductCategoryName
                searchViewModel?.let {
                    if (!it.isBestSellerProduct) {
                        populateItem(subcategoryList[0].iProductCategoryID)
                    }
                }
            } else {
                mBinding.rvsubCategory.visibility = View.GONE
                mBinding.gridItem.visibility = View.GONE
                val itemListFilter = ArrayList<ItemModel>()
                mBinding.gridItem.adapter = ItemListAdapter(mActivity, itemListFilter)

                mBinding.tvNoSubcategoryFound.visibility = View.VISIBLE
                mBinding.tvNoProductFound.visibility = View.VISIBLE
                mBinding.tvsubCategoryValue.text = ""
            }
        })
    }

    private fun DisplayRefundDataToCDS(
        mScreenName: String,
        mOrderTransModel: List<OrderTransModel>,
        mDisplayMsg: String
    ) {

        val date = Calendar.getInstance().time
        val sdtf = SimpleDateFormat(CV.DISPLAY_DATE_FORMATTE_AMPM)

        var mCDSSalesProducts: ArrayList<PrinterSalesProductsModel> = ArrayList()

        val mCDSSalesModel = CDSSalesModel()

        if (mOrderTransModel.isNotEmpty()) {
            for (i in mOrderTransModel.indices) {
                val orderTransModel = mOrderTransModel[i]

                val itemQty = orderTransModel.itemQty
                val itemRate = orderTransModel.itemRate
                var itemPrice = itemQty * itemRate
                itemPrice -= NewSaleFragmentCommon.floatToDouble(orderTransModel.discountAmount)

                val mPrinterSalesProductsModel = PrinterSalesProductsModel()

                mPrinterSalesProductsModel.setProductName(orderTransModel.itemName)
                mPrinterSalesProductsModel.setQuanity(itemQty.toString())
                mPrinterSalesProductsModel.setRate(String.format("%.2f", (itemRate)))
                mPrinterSalesProductsModel.setDiscount(
                    currency_symbol + " " + String.format(
                        "%.2f",
                        orderTransModel.discountAmount
                    )
                )
                mPrinterSalesProductsModel.setDiscountRemark(orderTransModel.discountRemark)
                mPrinterSalesProductsModel.setModifierIngredient(orderTransModel.ingredientName)

                val taxRate = orderTransModel.taxPercentage.toDouble()
                //remove tax calculation single product in CDS side
                val taxAmount: Double = 0.0//itemPrice / 100.0f * taxRate

                val mtax_amount: String =
                    if (mBinding.cbTax.isChecked) taxAmount.toString() else "0.0"

                mPrinterSalesProductsModel.setTax(mtax_amount)

                var itemPricetotal = itemPrice + (mtax_amount.toDouble())
                mPrinterSalesProductsModel.setTotal(
                    String.format(
                        "%.2f",
                        (itemPricetotal)
                    )
                )
                mPrinterSalesProductsModel.setInstruction(orderTransModel.strInstruction)

                mCDSSalesProducts.add(mPrinterSalesProductsModel)
            }
            mCDSSalesModel.setOrderId(mOrderTransModel.get(0).orderId)
        }

        mCDSSalesModel.setCashierName(emp_role + " - " + emp_name)
        mCDSSalesModel.setSaleDateTime(sdtf.format(date).toString())
        mCDSSalesModel.setBilledPersonName(getCustomerName())

        if (mScreenName == AppConstant.CDS_HOLD_SCREEN) {
            this.calculateCartProcedure(mOrderTransModel, true) { cart: CartSummaryData ->

                var isTaxIncluded = true
                if (selected_history != null) {
                    isTaxIncluded = !selected_history!!.tCheckTaxExempt.equals("0")
                }
                var final_tax_anount = 00.00
                if (isTaxIncluded) {
                    final_tax_anount = cart.taxAmount
                } else {
                    final_tax_anount = 00.00
                }
                var totalPayableAmount = 00.00

                var cartDiscount = 0.0


                if (isRefundDialogOpen) {
                    Log.e("DEBUG", "isRefundDialogOpen -- " + isRefundDialogOpen)
                    Log.e("DEBUG", "isRefundDiscountApplied -- " + isRefundDiscountApplied)

                    dialogRefund?.let {
                        Log.e(
                            "DEBUG",
                            "isRefundDiscountApplied vv-- " + it.findViewById<LinearLayout>(R.id.parentselectedDiscount).visibility
                        )
                        if (it.findViewById<LinearLayout>(R.id.parentselectedDiscount).visibility == View.VISIBLE) {
                            cartDiscount = cart.totalDiscount.toDouble()

                            Log.e("DEBUG", "VISIBLE -- ")
                        } else {
                            Log.e("DEBUG", "ELSE VISIBLE -- ")
                            cartDiscount = 0.0
                        }
                    }
                    if (cartDiscount > 0) {
                        //  mCDSSalesModel.setDiscountRemark(mBinding.cbDiscountApplied.text.toString())
                        mCDSSalesModel.setDiscountRemark(mBinding.cbDiscountApplied.text.toString())

                    }
                } else {
                    cartDiscount = cart.totalDiscount.toDouble()
                    if (!mBinding.cbDiscountApplied.text.toString().isNullOrEmpty()) {
                        // mCDSSalesModel.setDiscountRemark(mBinding.cbDiscountApplied.text.toString())
                        mCDSSalesModel.setDiscountRemark(mBinding.cbDiscountApplied.text.toString())
                    }
                }

                if (customerListViewModel?.isTaxIncluded?.value!!) {
                    mCDSSalesModel.setTaxApplied(
                        getFormatedAmountWithCurrencySymbol(
                            final_tax_anount
                        )
                    )
                    totalPayableAmount = cart.totalAmount + final_tax_anount
                } else {
                    mCDSSalesModel.setTaxApplied(getFormatedAmountWithCurrencySymbol(00.00))
                    totalPayableAmount = cart.totalAmount
                }

                mCDSSalesModel.setSubtotal(
                    currency_symbol + " " + String.format(
                        "%.2f",
                        (cart.totalAmount + cartDiscount)
                    )
                )

                mCDSSalesModel.setGrandTotal(
                    currency_symbol + " " + String.format(
                        "%.2f",
                        (totalPayableAmount)
                    )
                )

                mCDSSalesModel.setDiscount(
                    currency_symbol + " " + String.format(
                        "%.2f",
                        (cartDiscount)
                    )
                )

                mCDSSalesModel.setTotalSavings(
                    getFormatedAmountWithCurrencySymbol(
                        getTotalSavingAmount(
                            cart.totalDiscount
                        )
                    )
                )
            }
        } else {
            calculateCartSelectedRefundProcedure(mOrderTransModel) { cart: CartRefundSummaryData ->

                var isTaxIncluded = true
                if (selected_history != null) {
                    isTaxIncluded = !selected_history!!.tCheckTaxExempt.equals("0")
                }
                var final_tax_anount = 00.00
                if (isTaxIncluded) {
                    final_tax_anount = cart.taxAmount + cart.saleTaxAmount
                } else {
                    final_tax_anount = 00.00
                }
                var totalPayableAmount = 00.00

                if (isRefundDialogOpen && cart.totalDiscount > 0) {
                    mCDSSalesModel.setDiscountRemark(mBinding.cbDiscountApplied.text.toString())
                } else {
                    if (!mBinding.cbDiscountApplied.text.toString().isNullOrEmpty()) {
                        mCDSSalesModel.setDiscountRemark(mBinding.cbDiscountApplied.text.toString())
                    }
                }

                if (customerListViewModel?.isTaxIncluded?.value!!) {
                    mCDSSalesModel.setTaxApplied(
                        getFormatedAmountWithCurrencySymbol(
                            final_tax_anount
                        )
                    )
                } else {
                    mCDSSalesModel.setTaxApplied(getFormatedAmountWithCurrencySymbol(00.00))
                }
                totalPayableAmount =
                    (cart.totalAmount + cart.saleTotalAmount + final_tax_anount) - cart.totalDiscount

                mCDSSalesModel.setSubtotal(
                    currency_symbol + " " + String.format(
                        "%.2f",
                        (cart.totalAmount + cart.saleTotalAmount)
                    )
                )
                mCDSSalesModel.setGrandTotal(
                    currency_symbol + " " + String.format(
                        "%.2f",
                        (totalPayableAmount)
                    )
                )

                mCDSSalesModel.setDiscount(
                    currency_symbol + " " + String.format(
                        "%.2f",
                        (cart.totalDiscount)
                    )
                )

                mCDSSalesModel.setTotalSavings(
                    getFormatedAmountWithCurrencySymbol(
                        cart.totalDiscount + cart.saleDiscountAmount
                    )
                )
            }
        }

        mCDSSalesModel.setSalesProducts(mCDSSalesProducts)

        var baseActivityPrint = (activity as ParentActivity)
        baseActivityPrint.checkDrawOverlayPermission(
            mScreenName,
            mDisplayMsg,
            false,
            "",
            mCDSSalesModel,
            InvoiceNumber,
            false
        )
    }

    private fun populateItem(suCategoryId: Int) {
        if (suCategoryId == 0) {
            itemList = mDatabase.daoItemList().getBestSellerProduct() as ArrayList<ItemModel>
            if (itemList != null && itemList.size > 0) {
                itemListFilter = itemList
                mBinding.gridItem.visibility = View.VISIBLE
                mBinding.tvNoProductFound.visibility = View.GONE
                mBinding.gridItem.adapter = ItemListAdapter(mActivity, itemList)
            } else {
                mBinding.gridItem.visibility = View.GONE
                mBinding.tvNoProductFound.visibility = View.VISIBLE
            }
        } else {
            val itemListLiveData = mDatabase.daoItemList().getItems(suCategoryId)
            itemListLiveData.observe(this, androidx.lifecycle.Observer { itemlist ->
                itemList = itemlist as ArrayList<ItemModel>

                if (itemList != null && itemList.size > 0) {
                    itemListFilter = itemList
                    mBinding.gridItem.visibility = View.VISIBLE
                    mBinding.tvNoProductFound.visibility = View.GONE
                    mBinding.gridItem.adapter = ItemListAdapter(mActivity, itemList)

                    itemListLiveData.removeObserver { this }
                } else {
                    itemListFilter = itemList
                    mBinding.gridItem.adapter = ItemListAdapter(mActivity, itemListFilter)

                    mBinding.gridItem.visibility = View.GONE
                    mBinding.tvNoProductFound.visibility = View.VISIBLE
                }
            })
        }

    }

    override fun listItemClickedCategory(position: Int, category_id: Int) {
        mBinding.layoutModifier.visibility = View.GONE
        mBinding.gridItem.visibility = View.VISIBLE
        searchViewModel?.resetBestSellerProductsList()
        mBinding.tvCategoryValue.setText(categoryList.get(position).strProductCategoryName)
        populateSubcategory(category_id)
    }

    override fun listItemClickedSubCategory(position: Int, subcategory_id: Int) {
        mBinding.layoutModifier.visibility = View.GONE
        mBinding.gridItem.visibility = View.VISIBLE
        searchViewModel?.resetBestSellerProductsList()
        mBinding.tvsubCategoryValue.setText(subcategoryList.get(position).strProductCategoryName)
        populateItem(subcategory_id)
    }

    override fun listItemClickedCustomer(position: Int, category_id: Int) {
        CM.hideSoftKeyboard(mActivity)
        if (filterProduct.size > 0) {
            customerListViewModel?.selectedCustomer = filterProduct[position]
        } else {
            customerListViewModel?.selectedCustomer = customerListFilter[position]
        }
        dialogSearchCustomer.dismiss()
        setCustomerName()

        if (orderTransList.size > 0) {
            DisplayNewSaleDataToCDS(
                (mBinding.rvCart.adapter as OrderItemListAdapter).getDataset(),
                "",
                false
            )
        } else {
            DisplayNewSaleDataToCDS(ArrayList<OrderTransModel>(), "", false)
        }

    }

    override fun listItemClickedOrder(position: Int, orderHistoryModel: OrderHistoryModel) {
        var partialRefundorderTotalList: ArrayList<OrderTransModel> = ArrayList()

        InvoiceNumber = orderHistoryModel.strInvoiceNumber
        order_id = orderHistoryModel.iCheckDetailID
        CV.server_order_id=orderHistoryModel.iCheckDetailID.toInt()
        selected_history = orderHistoryModel

        customerListViewModel?.selectedCustomer =
            mDatabase.daoCustomerList().getCustomer(orderHistoryModel.iCustomerId.toInt())

        customerListViewModel?.selectedCustomer = CustomerModel(
            orderHistoryModel.iCustomerId.toInt(),
            orderHistoryModel.strFirstName,
            orderHistoryModel.strLastName,
            "", "", "", "", "", "", "", 0, 0, "", "", 0
        )
        mBinding.layoutModifier.visibility = View.GONE
        mBinding.gridItem.visibility = View.VISIBLE

        mBinding.tvNewOrder.text =
            orderHistoryModel.strFirstName + " " + orderHistoryModel.strLastName
        orderTransList.clear()
        id = orderHistoryModel.iCheckDetailID
        val isOnHold = orderHistoryModel.status
        if (isOnHold == AppConstant.ON_HOLD) {
            mDatabase.daoOrerHistoryList().deleteHoldOrder(id)
            isKitchenPrintServed = orderHistoryModel.isKitchenPrintServed
        }

        for (i in orderHistoryModel.orderProducts!!.indices) {
            var id =
                if (isOnHold == AppConstant.ON_HOLD) orderHistoryModel.orderProducts!![i].iCheckItemDetailId else orderHistoryModel.strInvoiceNumber
            var orderId =
                if (isOnHold == AppConstant.ON_HOLD) orderHistoryModel.iCheckDetailID else orderHistoryModel.strInvoiceNumber

            var sbFullString =
                GetModifierIngredient(selected_history!!.orderProducts!!.get(i).productModifier)

            if (isOnHold == AppConstant.ON_HOLD) {
                //sbFullString = selected_history!!.orderProducts!!.get(i).ingredientName
                val orderProduct = orderHistoryModel.orderProducts!![i]
                orderProduct?.let {
                    val temp = OrderTransModel(
                        id,
                        orderId,
                        orderProduct.iProductId,
                        orderProduct.strProductName,
                        orderProduct.dblPrice.toDouble(),
                        orderProduct.dblItemQty.toInt(),
                        orderProduct.dblAmount.toDouble(),
                        orderProduct.strPricingType,
                        "",
                        "",
                        orderProduct.dblItemTaxPercentage,
                        orderProduct.dblItemTaxAmount,
                        "",
                        orderProduct.dblItemQty.toInt(),
                        0.0,
                        false,
                        orderProduct.strProductType,
                        orderProduct.iDiscountId.toInt(),
                        orderProduct.strRemark,
                        orderProduct.dblItemDiscount.toFloat(),
                        orderProduct.premodifierType,
                        sbFullString,
                        orderProduct.strInstruction,
                        orderProduct.kitchenServedquantity,
                        orderProduct.isKitchenPrintServed,
                        tIsOpenDiscount = orderProduct.tIsOpenDiscount

                    )
                    orderTransList.add(temp)
                    mDatabase.daoOrderTrans().insertOrderTrans(temp)
                }
            } else {
                if (orderHistoryModel.orderRefund!!.isEmpty()) {
                    val orderProduct = orderHistoryModel.orderProducts!![i]
                    orderProduct?.let {
                        val temp = OrderTransModel(
                            id,
                            orderId,
                            orderProduct.iProductId,
                            orderProduct.strProductName,
                            orderProduct.dblPrice.toDouble(),
                            orderProduct.dblItemQty.toInt(),
                            orderProduct.dblAmount.toDouble(),
                            "",
                            "",
                            "",
                            orderProduct.dblItemTaxPercentage,
                            orderProduct.dblItemTaxAmount,
                            "",
                            orderProduct.dblItemQty.toInt(),
                            0.0,
                            false,
                            orderProduct.strProductType,
                            orderProduct.iDiscountId.toInt(),
                            orderProduct.strRemark,
                            orderProduct.dblItemDiscount.toFloat(),
                            "",
                            sbFullString.toString(),
                            orderProduct.strInstruction,
                            orderProduct.kitchenServedquantity,
                            isKitchenPrintServed,
                            tIsOpenDiscount = 0,
                            totalItemQty = orderProduct.dblItemQty.toInt(),
                            refundProductId = orderProduct.iProductId
                        )
                        orderTransList.add(temp)
                    }
                } else {

                    var isRefunded = false
                    for (j in refundItemList.indices) {
                        if (refundItemList[j].iProductId == orderHistoryModel.orderProducts!![i].iProductId &&
                            refundItemList[j].dblItemQty == orderHistoryModel.orderProducts!![i].dblItemQty.toInt()
                        ) {
                            isRefunded = true

                            val refundedorderProducts = orderHistoryModel.orderProducts!![i]
                            //orderHistoryModel.orderRefund!!.get(0).refundItems!![j].dblItemQty
                            refundedorderProducts?.let {
                                val temp = OrderTransModel(
                                    id,
                                    orderId,
                                    refundedorderProducts.iProductId,
                                    refundedorderProducts.strProductName,
                                    refundedorderProducts.dblPrice.toDouble(),
                                    refundItemList[j].dblItemQty,
                                    refundedorderProducts.dblAmount.toDouble(),
                                    "",
                                    "",
                                    "",
                                    refundedorderProducts.dblItemTaxPercentage,
                                    refundedorderProducts.dblItemTaxAmount,
                                    "",
                                    refundItemList[j].dblItemQty,
                                    0.0,
                                    isRefunded,
                                    refundedorderProducts.strProductType,
                                    refundedorderProducts.iDiscountId.toInt(),
                                    refundedorderProducts.strRemark,
                                    refundedorderProducts.dblItemDiscount.toFloat(),
                                    "",
                                    sbFullString.toString(),
                                    refundedorderProducts.strInstruction,
                                    refundedorderProducts.kitchenServedquantity,
                                    isKitchenPrintServed,
                                    tIsOpenDiscount = 0
                                )

                                partialRefundorderTotalList.add(temp)
                                // orderTransList.add(temp)
                            }
                        }
                    }

                    val orderProduct = orderHistoryModel.orderProducts!![i]

                    orderProduct?.let {
                        val temp = OrderTransModel(
                            id,
                            orderId,
                            orderProduct.iProductId,
                            orderProduct.strProductName,
                            orderProduct.dblPrice.toDouble(),
                            orderProduct.dblItemQty.toInt(),
                            orderProduct.dblAmount.toDouble(),
                            "",
                            "",
                            "",
                            orderProduct.dblItemTaxPercentage,
                            orderProduct.dblItemTaxAmount,
                            "",
                            orderProduct.dblItemQty.toInt(),
                            0.0,
                            isRefunded,
                            orderProduct.strProductType,
                            orderProduct.iDiscountId.toInt(),
                            orderProduct.strRemark,
                            orderProduct.dblItemDiscount.toFloat(),
                            "",
                            sbFullString.toString(),
                            orderProduct.strInstruction,
                            orderProduct.kitchenServedquantity,
                            isKitchenPrintServed,
                            tIsOpenDiscount = 0,
                            totalItemQty = orderProduct.dblItemQty.toInt(),
                            refundProductId = orderProduct.iProductId
                        )
                        orderTransList.add(temp)
                    }
                }
            }
        }
        if (!orderHistoryModel.orderDiscounts.isNullOrEmpty()) {
            val disMaster = discountListViewModel?.getDiscountMasterData(
                orderHistoryModel.orderDiscounts?.get(0)?.iDiscountId!!
            )
            if (disMaster != null) {
                discountListViewModel?.selectedCartDiscount = disMaster
                getDiscountAmountOnCart(false)
            }
            calculateDiscountOnCart()
        }

        val status =
            if (isOnHold == AppConstant.ON_HOLD) AppConstant.ON_HOLD else AppConstant.REFUND
        setEvent(status)

        selected_history.let {
            if (it != null) {
                if (it.tCheckTaxExempt.equals("0")) {
                    mBinding.cbTax.isChecked = false
                } else {
                    mBinding.cbTax.isChecked = true
                }

            }
        }
        val test = orderTransList.asReversed()
        adapter(test, eventName)
        mBinding.rvCart.adapter?.notifyDataSetChanged()

        this.calculateCartProcedure(orderTransList, true) {}

        if (orderHistoryModel.status == AppConstant.ON_HOLD) {
            mBinding.ivAddCustomer.isEnabled = false
            mBinding.ivAdd.visibility = View.GONE
            CM.setSp(mActivity, CV.ORDER_ID, orderHistoryModel.iCheckDetailID)
            mBinding.btnSave.visibility = View.VISIBLE
            mBinding.btnCancel.visibility = View.GONE
            mBinding.btnCheckout.visibility = View.VISIBLE
            mBinding.btnVoid.visibility = View.VISIBLE
            mBinding.cbTax.visibility = View.VISIBLE
            mBinding.btnExchange.visibility = View.GONE
            mBinding.btnRefund.visibility = View.GONE
            mBinding.btnPrint.visibility = View.GONE

            mBinding.ivStamp.visibility = View.GONE

            DisplayRefundDataToCDS(AppConstant.CDS_HOLD_SCREEN, orderTransList, "")

        } else if (orderHistoryModel.status.equals(
                AppConstant.HistoryType.VOID.toString().toUpperCase()
            )
        ) {
            mBinding.ivAddCustomer.isEnabled = false
            mBinding.ivAdd.visibility = View.GONE
            mBinding.btnSave.visibility = View.GONE
            mBinding.btnCancel.visibility = View.GONE
            mBinding.btnCheckout.visibility = View.GONE
            mBinding.btnVoid.visibility = View.GONE
            mBinding.cbTax.visibility = View.GONE
            mBinding.btnExchange.visibility = View.VISIBLE
            mBinding.btnRefund.visibility = View.GONE
            mBinding.btnPrint.visibility = View.VISIBLE

            mBinding.ivStamp.setImageDrawable(resources.getDrawable(R.drawable.ic_stamp_void))
            mBinding.ivStamp.visibility = View.VISIBLE
            mBinding.ivAddDiscount.hide()

            var baseActivityPrint = (activity as ParentActivity)
            //Hardware Code For CDS Display -- Normal Image Display Case Invoke
            baseActivityPrint.checkDrawOverlayPermission(
                AppConstant.CDS_NORMAL_SCREEN,
                "",
                false,
                "",
                CDSSalesModel(),
                "",
                false
            )

        } else if (orderHistoryModel.orderRefund != null && orderHistoryModel.dblCheckGrossTotal.toDouble() <= orderHistoryModel.orderRefund!!.map { it.dblRefundAmount.toDouble() }
                .sum()) {
            mBinding.ivAddCustomer.isEnabled = false
            mBinding.ivAdd.visibility = View.GONE
            mBinding.btnSave.visibility = View.GONE
            mBinding.btnCancel.visibility = View.GONE
            mBinding.btnCheckout.visibility = View.GONE
            mBinding.btnVoid.visibility = View.GONE
            mBinding.cbTax.visibility = View.GONE
            mBinding.btnExchange.visibility = View.VISIBLE
            mBinding.btnRefund.visibility = View.GONE
            mBinding.btnPrint.visibility = View.VISIBLE

            mBinding.ivStamp.setImageDrawable(resources.getDrawable(R.drawable.ic_stamp_refund))
            mBinding.ivStamp.visibility = View.VISIBLE
            mBinding.ivAddDiscount.hide()

            //Invoke CDS Display to display Hold and Refund orders
            DisplayRefundDataToCDS(
                AppConstant.CDS_REFUND_SCREEN,
                orderTransList,
                ""
            )

        } else {
            mBinding.ivAddCustomer.isEnabled = false
            mBinding.ivAdd.visibility = View.GONE
            mBinding.btnSave.visibility = View.GONE
            mBinding.btnCancel.visibility = View.GONE
            mBinding.btnCheckout.visibility = View.GONE
            mBinding.btnVoid.visibility = View.GONE
            mBinding.cbTax.visibility = View.GONE
            mBinding.btnExchange.visibility = View.VISIBLE
            mBinding.btnRefund.visibility = View.VISIBLE
            mBinding.btnPrint.visibility = View.VISIBLE
            if (orderHistoryModel.orderRefund!!.isEmpty()) {
                mBinding.ivStamp.setImageDrawable(resources.getDrawable(R.drawable.ic_stamp_completed))
            }else{
                mBinding.ivStamp.setImageDrawable(resources.getDrawable(R.drawable.ic_stamp_parially_refunded))
            }
            mBinding.ivStamp.visibility = View.VISIBLE
            mBinding.ivAddDiscount.hide()

            //Invoke CDS Display to display Hold and Refund orders
            DisplayRefundDataToCDS(AppConstant.CDS_COMPLETE_SCREEN, orderTransList, "")
        }
        dialogorder.dismiss()
    }

    private fun setEvent(eventName: String) {
        this.eventName = eventName
        customerListViewModel?.setEventName(eventName)
    }

    override fun listItemClicked(position: Int, orderTransModel: OrderTransModel) {
        orderTransList.clear()

        if (selected_history!!.tCheckTaxExempt == "1") {
            for (i in selected_history!!.orderProducts!!.indices) {
                val temp = OrderTransModel(
                    selected_history!!.iCheckDetailID,
                    selected_history!!.strInvoiceNumber,
                    selected_history!!.orderProducts!![i].iCheckItemDetailId.toInt(),
                    selected_history!!.orderProducts!![i].strProductName,
                    selected_history!!.orderProducts!![i].dblPrice.toDouble(),
                    selected_history!!.orderProducts!![i].dblItemQty.toInt(),
                    selected_history!!.orderProducts!![i].dblAmount.toDouble(),
                    "",
                    "",
                    "",
                    selected_history!!.orderProducts!![i].dblItemTaxPercentage,
                    selected_history!!.orderProducts!![i].dblItemTaxAmount,
                    "",
                    NewSaleFragmentCommon.itemRefundedRemaingQty(
                        selected_history!!.orderProducts!![i],
                        refundItemList
                    ),
                    0.0,
                    NewSaleFragmentCommon.itemIsRefunded(
                        selected_history!!.orderProducts!![i],
                        refundItemList
                    ),
                    selected_history!!.orderProducts!![i].strProductType,
                    selected_history!!.orderProducts!![i].iDiscountId.toInt(),
                    selected_history!!.orderProducts!![i].strRemark,
                    selected_history!!.orderProducts!![i].dblItemDiscount.toFloat(),
                    "",
                    GetModifierIngredient(selected_history!!.orderProducts!!.get(i).productModifier),
                    selected_history!!.orderProducts!![i].strInstruction,
                    selected_history!!.orderProducts!!.get(i).kitchenServedquantity,
                    isKitchenPrintServed,
                    tIsOpenDiscount = selected_history!!.orderProducts!![i].tIsOpenDiscount,
                    totalItemQty = selected_history!!.orderProducts!![i].dblItemQty.toInt(),
                    refundProductId = selected_history!!.orderProducts!![i].iProductId
                )
                orderTransList.add(temp)
            }
        } else {
            for (i in selected_history!!.orderProducts!!.indices) {
                val temp = OrderTransModel(
                    selected_history!!.iCheckDetailID,
                    selected_history!!.strInvoiceNumber,
                    selected_history!!.orderProducts!![i].iCheckItemDetailId.toInt(),
                    selected_history!!.orderProducts!![i].strProductName,
                    selected_history!!.orderProducts!![i].dblPrice.toDouble(),
                    selected_history!!.orderProducts!![i].dblItemQty.toInt(),
                    selected_history!!.orderProducts!![i].dblAmount.toDouble(),
                    "",
                    "",
                    "",
                    "0.0",
                    "0.00",
                    "",
                    NewSaleFragmentCommon.itemRefundedRemaingQty(
                        selected_history!!.orderProducts!![i],
                        refundItemList
                    ),
                    0.0,
                    NewSaleFragmentCommon.itemIsRefunded(
                        selected_history!!.orderProducts!![i],
                        refundItemList
                    ),
                    selected_history!!.orderProducts!![i].strProductType,
                    selected_history!!.orderProducts!![i].iDiscountId.toInt(),
                    selected_history!!.orderProducts!![i].strRemark,
                    selected_history!!.orderProducts!![i].dblItemDiscount.toFloat(),
                    "",
                    GetModifierIngredient(selected_history!!.orderProducts!!.get(i).productModifier),
                    selected_history!!.orderProducts!![i].strInstruction,
                    selected_history!!.orderProducts!!.get(i).kitchenServedquantity,
                    isKitchenPrintServed,
                    tIsOpenDiscount = selected_history!!.orderProducts!![i].tIsOpenDiscount,
                    totalItemQty = selected_history!!.orderProducts!![i].dblItemQty.toInt(),
                    refundProductId = selected_history!!.orderProducts!![i].iProductId
                )
                orderTransList.add(temp)
            }
        }
        if (mBinding.tvDiscountValue.text.toString().split(" ")[1].toDouble() > 0) {
            if (orderTransModel.discountAmount > 0) {
                if (!checkIfExist(orderTransModel)) {
                    orderTransModel.itemQty = orderTransModel.itemQtyLeft
                    orderTransListSelected.add(orderTransModel)
                }

                dialogRefund?.let {
                    val rvselectedOrder = it.findViewById<RecyclerView>(R.id.rvSelectedOrder)
                    mRefundSelectedAdapter =
                        RefundSelectedItemListAdapter(
                            this@NewSalesFragment,
                            mActivity,
                            orderTransListSelected,
                            tvTotalRight,
                            containerRefundSelectedDiscount,
                            containerRefundSelectedDiscountRemark,
                            tvRefundselectedCartDiscount,
                            tvRefundselectedCartDiscountRemark,
                            this
                        )
                    rvselectedOrder.adapter = mRefundSelectedAdapter

                    ShowCartLevelDiscount(orderTransListSelected)
                    DisplayRefundDataToCDS(
                        AppConstant.CDS_REFUND_SCREEN,
                        orderTransListSelected,
                        ""
                    )
                }
            } else {
                orderAllCartDiscountItems.clear()
                if (!orderTransListSelected.contains(orderTransModel)) {
                    if (orderTransModel.discountAmount > 0) {
                    } else {
                        if (!checkIfExist(orderTransModel)) {
                            orderTransModel.itemQty = orderTransModel.itemQtyLeft
                            orderAllCartDiscountItems.add(orderTransModel)
                        }
                    }

                    orderTransListSelected.addAll(orderAllCartDiscountItems)
                    dialogRefund?.let {
                        val rvselectedOrder =
                            it.findViewById<RecyclerView>(R.id.rvSelectedOrder)
                        mRefundSelectedAdapter =
                            RefundSelectedItemListAdapter(
                                this@NewSalesFragment,
                                mActivity,
                                orderTransListSelected,
                                tvTotalRight,
                                containerRefundSelectedDiscount,
                                containerRefundSelectedDiscountRemark,
                                tvRefundselectedCartDiscount,
                                tvRefundselectedCartDiscountRemark,
                                this
                            )
                        rvselectedOrder.adapter = mRefundSelectedAdapter


                        ShowCartLevelDiscount(orderTransListSelected)
                        //Hardware Code For CDS Display -- Refund while select single item from all order case invoke
                        DisplayRefundDataToCDS(
                            AppConstant.CDS_REFUND_SCREEN,
                            orderTransListSelected,
                            ""
                        )
                    }
                }
            }
        } else {
            if (!checkIfExist(orderTransModel)) {
                orderTransModel.itemQty = orderTransModel.itemQtyLeft
                orderTransListSelected.add(orderTransModel)
            }

            dialogRefund?.let {
                val rvselectedOrder = it.findViewById<RecyclerView>(R.id.rvSelectedOrder)
                mRefundSelectedAdapter =
                    RefundSelectedItemListAdapter(
                        this@NewSalesFragment,
                        mActivity,
                        orderTransListSelected,
                        tvTotalRight,
                        containerRefundSelectedDiscount,
                        containerRefundSelectedDiscountRemark,
                        tvRefundselectedCartDiscount,
                        tvRefundselectedCartDiscountRemark,
                        this
                    )
                rvselectedOrder.adapter = mRefundSelectedAdapter

                ShowCartLevelDiscount(orderTransListSelected)
                //Hardware Code For CDS Display -- Refund while select single item from all order case invoke
                DisplayRefundDataToCDS(
                    AppConstant.CDS_REFUND_SCREEN,
                    orderTransListSelected,
                    ""
                )
            }
        }
    }

    private fun ShowCartLevelDiscount(orderListSelected: ArrayList<OrderTransModel>) {
        orderAllCartDiscountItems.clear()
        orderAllSelectCartDiscountItems.clear()
        for (i in orderListSelected.indices) {
            if (orderListSelected[i].discountAmount > 0) {
            } else {
                orderAllSelectCartDiscountItems.add(orderListSelected.get(i))
            }
        }

        dialogRefund?.let {
            if (orderListSelected.size > 0) {
                if (selected_history!!.orderDiscounts?.isNullOrEmpty()!!) {
                    it.findViewById<LinearLayout>(R.id.parentselectedDiscount).visibility =
                        View.GONE
                    it.findViewById<LinearLayout>(R.id.parentselectedDiscountRemark).visibility =
                        View.GONE
                } else {
                    if (orderAllSelectCartDiscountItems.size > 0) {
                        var isValid = true
                        if (selected_history!!.orderDiscounts!![0].strDiscountType.equals(
                                AppConstant.COUPON_DISCOUNT
                            )
                        ) {
                            for (item in orderAllSelectCartDiscountItems) {
                                isValid = isValidCopunProduct(item)
                                if (isValid) {
                                    it.findViewById<LinearLayout>(R.id.parentselectedDiscount).visibility =
                                        View.VISIBLE
                                    it.findViewById<LinearLayout>(R.id.parentselectedDiscountRemark)
                                        .visibility =
                                        View.VISIBLE
                                    isRefundDiscountApplied = true
                                    break
                                }
                            }
                            if (!isValid) {
                                it.findViewById<LinearLayout>(R.id.parentselectedDiscount).visibility =
                                    View.GONE
                                it.findViewById<LinearLayout>(R.id.parentselectedDiscountRemark)
                                    .visibility =
                                    View.GONE
                                isRefundDiscountApplied = false
                            }
                        } else {
                            it.findViewById<LinearLayout>(R.id.parentselectedDiscount).visibility =
                                View.VISIBLE
                            it.findViewById<LinearLayout>(R.id.parentselectedDiscountRemark)
                                .visibility =
                                View.VISIBLE
                            isRefundDiscountApplied = true
                        }
                    } else {
                        it.findViewById<LinearLayout>(R.id.parentselectedDiscount).visibility =
                            View.GONE
                        it.findViewById<LinearLayout>(R.id.parentselectedDiscountRemark)
                            .visibility =
                            View.GONE
                        isRefundDiscountApplied = false
                    }
                }
            } else {
                tvTotalRight.text = currency_symbol + " " + "00.00"
                it.findViewById<LinearLayout>(R.id.parentselectedDiscount).visibility =
                    View.GONE
                it.findViewById<LinearLayout>(R.id.parentselectedDiscountRemark).visibility =
                    View.GONE
                isRefundDiscountApplied = false
            }
        }
    }

    fun getRemainingRefundDiscount(
        orderDiscountsModel: OrderDiscountsModel,
        refundOrderList: List<OrderTransModel>,
        isFromRefundRePrint:Boolean = false
    ): Float {
        var remainingRefundDiscount = 0.0f
        if (selected_history!!.orderRefund!!.isNotEmpty()) {
            var totalRefundedDiscount = 0.0f
            for (refundItem in selected_history!!.orderRefund!!) {
                totalRefundedDiscount += refundItem.dblDiscountAmount.toFloat()
            }
            if(isFromRefundRePrint) {
                remainingRefundDiscount =
                    selected_history!!.orderDiscounts!![0].dblDiscountAmt
            }else{remainingRefundDiscount =
                selected_history!!.orderDiscounts!![0].dblDiscountAmt - totalRefundedDiscount}
        } else {
            remainingRefundDiscount = selected_history!!.orderDiscounts!![0].dblDiscountAmt
        }

        var discountAmount = 0.0f
        var filteredNonDiscountOrderList =
            refundOrderList.filter { item -> item.iDiscountId == 0 }
        var remainigProductAmount = 0.0
        var totalApplicableItemDiscount = 0.0
        for (order in filteredNonDiscountOrderList) {
            var isValidProduct = false
            if (orderDiscountsModel.strDiscountType!!.equals(AppConstant.COUPON_DISCOUNT)) {
                isValidProduct =
                    discountListViewModel?.checkDiscountMappingItemIsAvailable(
                        order.refundProductId.toLong(),
                        discountListViewModel?.selectedCartDiscount?.discountId!!.toLong()
                    )!!
            } else {
                isValidProduct = true
            }

            if (isValidProduct!!) {
                totalApplicableItemDiscount += order.itemAmount
                if (!order.isRefunded) {
                    if (checkIfExist(order)) {
                        val orderSelectedItem =
                            orderTransListSelected.single { item -> item.itemId == order.itemId }
                        remainigProductAmount += (order.itemQtyLeft - orderSelectedItem.itemQty) * order.itemRate
                    } else {
                        remainigProductAmount += order.itemAmount
                    }

                }
            }
        }
        if (orderDiscountsModel.strDiscountType!!.equals(AppConstant.COUPON_DISCOUNT)) {
            if (orderDiscountsModel.dblMinCartAmount < remainigProductAmount) {
                for (refundItem in refundOrderList) {
                    val isValidProduct = NewSaleFragmentCommon.isValidateCartDiscountedProduct(
                        discountListViewModel!!,
                        refundItem.refundProductId,
                        discountListViewModel?.selectedCartDiscount?.discountId!!
                    )
                    if (isValidProduct) {
                        val amountCopoun = (refundItem.itemQty * refundItem.itemRate)
                        val itemWeightAge =
                            (amountCopoun * 100) / totalApplicableItemDiscount
                        discountAmount +=
                            ((coupnDiscountAmount * itemWeightAge) / 100).toFloat()
                    }
                }
                return discountAmount
            }
            return remainingRefundDiscount
        } else {
            for (refundItem in refundOrderList) {
                val isValidProduct = true
                if (isValidProduct) {
                    val amountCopoun = (refundItem.itemQty * refundItem.itemRate)
                    val itemWeightAge =
                        (amountCopoun * 100) / totalApplicableItemDiscount
                    discountAmount +=
                        ((coupnDiscountAmount * itemWeightAge) / 100).toFloat()
                }
            }

            return discountAmount
        }
        return remainingRefundDiscount
    }

    public fun checkIfExist(orderTransModel: OrderTransModel): Boolean {
        var isExist = false
        for (i in orderTransListSelected.indices) {
            if (orderTransListSelected.get(i).itemId == orderTransModel.itemId) {
                isExist = true
                break
            }
        }
        return isExist
    }

    override fun listItemClickedItem(position: Int, category_id: Int) {
    }

    override fun listItemClickedItem(itemModel: ItemModel) {
        CM.onKeyboardClose(dialogSearchItem.edtSearchCustomer, mActivity)
        isCustomerEligible(itemModel)
        if (dialogSearchItem != null)
            dialogSearchItem.dismiss()
    }

    fun resetOrder() {
        doResetCart()
    }

    override fun onItemClick(action: String, position: Int, orderTransModel: OrderTransModel) {
        when (action) {
            "add" -> {
                /*var orderTranAddsModel = addItemToCart(orderTransModel)
                mDatabase.daoOrderTrans().updateOrderTrans(orderTranAddsModel)
                openDiscountCalculation(orderTranAddsModel)
                cartAdapter?.run {
                    discountMixMatchCalculation() {
                        updateItem(orderTransList.asReversed())
                        getTotalAmount(this.getDataset())
                        calculateDiscountOnCart()

                        DisplayNewSaleDataToCDS(cartAdapter?.getDataset()!!, "", false)
                    }

                }*/
                if (isModifirePopupClose()) {
                    if (TextUtils.equals(
                            CV.PRODUCTS_TYPE_KITCHEN,
                            orderTransModel.strProductType
                        ) && mDatabase.daoModifierGroupList()
                            .getModifierGroupList(orderTransModel.itemId).size > 0
                    ) {
                        DialogUtility.openDoubleActionModifierDialog(
                            mActivity,
                            getString(R.string.modifier_two_btn_dialog_message),
                            orderTransModel.itemName,
                            orderTransModel.ingredientName
                        ) {
                            if (it) {
                                val item = mDatabase.daoItemList().getItem(orderTransModel.itemId)
                                val ordertransmodel_ = itemAmountCalculation_return(item)
                                order_product_id = ordertransmodel_.id
                                mDatabase.daoOrderTrans().insertOrderTrans(ordertransmodel_)
                                getProductsFromDatabase(order_id)
                                pos = 0
                                if (!mDatabase.daoModifierGroupList()
                                        .getModifierGroupList(ordertransmodel_.itemId)
                                        .isNullOrEmpty()
                                ) {
                                    add_default_ingredient(ordertransmodel_.itemId)
                                }
                                if (mDatabase.daoModifierGroupList()
                                        .getModifierGroupList(orderTransModel.itemId) != null && mDatabase.daoModifierGroupList()
                                        .getModifierGroupList(orderTransModel.itemId)!!.isNotEmpty()
                                ) {
                                    for (i in mDatabase.daoModifierGroupList()
                                        .getModifierGroupList(orderTransModel.itemId)!!.indices) {
                                        if (mDatabase.daoModifierGroupList()
                                                .getModifierGroupList(orderTransModel.itemId)
                                                .get(i).strIsRequired.equals(CV.REQUIRED) || mDatabase.daoModifierGroupList()
                                                .getModifierGroupList(orderTransModel.itemId)
                                                .get(i).strIsRequired.equals(CV.FORCE_SHOW_OPTIONAL)
                                        ) {
                                            /*var tempList = orderTransList
                                        pos = tempList.reversed().indexOfFirst {
                                            it.itemId == orderTransModel.itemId
                                        }
                                        if (pos == -1) {
                                            pos = 0
                                        }*/
                                            item_id = orderTransModel.itemId
                                            //order_product_id=orderTransModel.id
                                            mBinding.tvNoProductFound.visibility = View.GONE
                                            openModifierLayout(
                                                orderTransModel.itemId,
                                                orderTransModel.itemName
                                            )
                                            break
                                        }
                                    }
                                }
                                /*if (selected_history != null) {
                                pos = FindSamePreviousItemQtybyItsName(selected_history, ordertransmodel_.itemId)
                                if (pos != -1) {
                                    if (selected_history!!.orderProducts?.get(pos)!!.isKitchenPrintServed) {
                                        if (mDatabase.daoModifierGroupList()
                                                .getModifierGroupList(ordertransmodel_.itemId) != null && mDatabase.daoModifierGroupList()
                                                .getModifierGroupList(ordertransmodel_.itemId)!!.isNotEmpty()
                                        ) {
                                            for (i in mDatabase.daoModifierGroupList()
                                                .getModifierGroupList(ordertransmodel_.itemId)!!.indices) {
                                                if (mDatabase.daoModifierGroupList()
                                                        .getModifierGroupList(ordertransmodel_.itemId)
                                                        .get(i).strIsRequired.equals(CV.REQUIRED) || mDatabase.daoModifierGroupList()
                                                        .getModifierGroupList(ordertransmodel_.itemId)
                                                        .get(i).strIsRequired.equals(CV.FORCE_SHOW_OPTIONAL)
                                                ) {
                                                    var tempList = orderTransList
                                                    pos = tempList.reversed().indexOfFirst {
                                                        it.itemId == ordertransmodel_.itemId
                                                    }
                                                    if (pos == -1) {
                                                        pos = 0
                                                    }
                                                    item_id = ordertransmodel_.itemId
                                                    //order_product_id=orderTransModel.id
                                                    mBinding.tvNoProductFound.visibility = View.GONE
                                                    openModifierLayout(
                                                        ordertransmodel_.itemId,
                                                        ordertransmodel_.itemName
                                                    )
                                                    break
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (mDatabase.daoModifierGroupList()
                                            .getModifierGroupList(ordertransmodel_.itemId) != null && mDatabase.daoModifierGroupList()
                                            .getModifierGroupList(ordertransmodel_.itemId)!!.isNotEmpty()
                                    ) {
                                        for (i in mDatabase.daoModifierGroupList()
                                            .getModifierGroupList(ordertransmodel_.itemId)!!.indices) {
                                            if (mDatabase.daoModifierGroupList()
                                                    .getModifierGroupList(ordertransmodel_.itemId)
                                                    .get(i).strIsRequired.equals(CV.REQUIRED) || mDatabase.daoModifierGroupList()
                                                    .getModifierGroupList(ordertransmodel_.itemId)
                                                    .get(i).strIsRequired.equals(CV.FORCE_SHOW_OPTIONAL)
                                            ) {
                                                var tempList = orderTransList
                                                pos = tempList.reversed().indexOfFirst {
                                                    it.itemId == ordertransmodel_.itemId
                                                }
                                                if (pos == -1) {
                                                    pos = 0
                                                }
                                                item_id = ordertransmodel_.itemId
                                                //order_product_id=ordertransmodel_.id
                                                mBinding.tvNoProductFound.visibility = View.GONE
                                                openModifierLayout(ordertransmodel_.itemId, ordertransmodel_.itemName)
                                                break
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (mDatabase.daoModifierGroupList()
                                        .getModifierGroupList(ordertransmodel_.itemId) != null && mDatabase.daoModifierGroupList()
                                        .getModifierGroupList(ordertransmodel_.itemId)!!.isNotEmpty()
                                ) {
                                    for (i in mDatabase.daoModifierGroupList()
                                        .getModifierGroupList(ordertransmodel_.itemId)!!.indices) {
                                        if (mDatabase.daoModifierGroupList()
                                                .getModifierGroupList(ordertransmodel_.itemId)
                                                .get(i).strIsRequired.equals(CV.REQUIRED) || mDatabase.daoModifierGroupList()
                                                .getModifierGroupList(ordertransmodel_.itemId)
                                                .get(i).strIsRequired.equals(CV.FORCE_SHOW_OPTIONAL)
                                        ) {
                                            var tempList = orderTransList
                                            pos = tempList.reversed().indexOfFirst {
                                                it.itemId == ordertransmodel_.itemId
                                            }
                                            if (pos == -1) {
                                                pos = 0
                                            }

                                            item_id = ordertransmodel_.itemId
                                            //order_product_id=orderTransModel.id
                                            mBinding.tvNoProductFound.visibility = View.GONE
                                            openModifierLayout(ordertransmodel_.itemId, ordertransmodel_.itemName)
                                            break
                                        }
                                    }
                                }
                            }*/
                            } else {
                                var orderTranAddsModel = addItemToCart(orderTransModel)
                                mDatabase.daoOrderTrans().updateOrderTrans(orderTranAddsModel)
                                openDiscountCalculation(orderTranAddsModel)
                                cartAdapter?.run {
                                    discountMixMatchCalculation() {
                                        updateItem(orderTransList.asReversed())
                                        getTotalAmount(this.getDataset())
                                        calculateDiscountOnCart()

                                        DisplayNewSaleDataToCDS(
                                            cartAdapter?.getDataset()!!,
                                            "",
                                            false
                                        )
                                    }

                                }
                            }
                        }
                    } else {
                        var orderTranAddsModel = addItemToCart(orderTransModel)
                        mDatabase.daoOrderTrans().updateOrderTrans(orderTranAddsModel)
                        openDiscountCalculation(orderTranAddsModel)
                        cartAdapter?.run {
                            discountMixMatchCalculation() {
                                updateItem(orderTransList.asReversed())
                                getTotalAmount(this.getDataset())
                                calculateDiscountOnCart()

                                DisplayNewSaleDataToCDS(cartAdapter?.getDataset()!!, "", false)
                            }

                        }
                    }
                }
            }
            "sub" -> {
                var mpriviousitemqty = 1
                if (TextUtils.equals(orderTransModel.strProductType,CV.PRODUCTS_TYPE_KITCHEN)) {
                    if (orderTransModel.itemQty > orderTransModel.kitchenServedquantity) {
                        if (orderTransModel.itemQty > 1) {
                            var orderTranAddsModel = removeItemFromCart(orderTransModel)
                            mDatabase.daoOrderTrans().updateOrderTrans(orderTranAddsModel)
                            openDiscountCalculation(orderTranAddsModel)
                            cartAdapter?.run {
                                //updateItem(position, orderTranAddsModel)
                                discountMixMatchCalculation() {
                                    updateItem(orderTransList.asReversed())
                                    getTotalAmount(this.getDataset())

                                    DisplayNewSaleDataToCDS(cartAdapter?.getDataset()!!, "", false)
                                }
                            }
                            calculateDiscountOnCart()
                        }
                    } else {
                        showToast(
                            mActivity,
                            "You can't decrease qty of kitchen products which is served",
                            R.drawable.icon,
                            custom_toast_container
                        )
                    }
                }else{
                    if (orderTransModel.itemQty > 1) {
                        var orderTranAddsModel = removeItemFromCart(orderTransModel)
                        mDatabase.daoOrderTrans().updateOrderTrans(orderTranAddsModel)
                        openDiscountCalculation(orderTranAddsModel)
                        cartAdapter?.run {
                            //updateItem(position, orderTranAddsModel)
                            discountMixMatchCalculation() {
                                updateItem(orderTransList.asReversed())
                                getTotalAmount(this.getDataset())

                                DisplayNewSaleDataToCDS(cartAdapter?.getDataset()!!, "", false)
                            }
                        }
                        calculateDiscountOnCart()
                    }
                }
                /*if (orderTransModel.isKitchenPrintServed) {

                    if (orderTransModel.itemQty > 1) {
                        var orderTranAddsModel = removeItemFromCart(orderTransModel)
                        mDatabase.daoOrderTrans().updateOrderTrans(orderTranAddsModel)
                        openDiscountCalculation(orderTranAddsModel)
                        cartAdapter?.run {
                            //updateItem(position, orderTranAddsModel)
                            discountMixMatchCalculation() {
                                updateItem(orderTransList.asReversed())
                                getTotalAmount(this.getDataset())

                                DisplayNewSaleDataToCDS(cartAdapter?.getDataset()!!, "", false)
                            }
                        }
                        calculateDiscountOnCart()
                    }
                } else {
                    if (selected_history != null) {
                        var pos = FindSamePreviousItemQtybyItsName(
                            selected_history,
                            orderTransModel.itemId
                        )
                        mpriviousitemqty =
                            selected_history!!.orderProducts?.get(pos)!!.dblItemQty.toInt()
                    }
                    if (orderTransModel.itemQty > mpriviousitemqty) {
                        var orderTranAddsModel = removeItemFromCart(orderTransModel)
                        mDatabase.daoOrderTrans().updateOrderTrans(orderTranAddsModel)
                        openDiscountCalculation(orderTranAddsModel)
                        cartAdapter?.run {
                            //updateItem(position, orderTranAddsModel)
                            discountMixMatchCalculation() {
                                updateItem(orderTransList.asReversed())
                                getTotalAmount(this.getDataset())

                                DisplayNewSaleDataToCDS(cartAdapter?.getDataset()!!, "", false)
                            }
                        }
                        calculateDiscountOnCart()
                    } else {
                        showToast(
                            mActivity,
                            "You can't decrease qty of kitchen products which is served",
                            R.drawable.icon,
                            custom_toast_container
                        )
                    }
                }*/

            }
            "delete" -> {
                if (TextUtils.equals(orderTransModel.strProductType,CV.PRODUCTS_TYPE_KITCHEN)) {
                    if (orderTransModel.kitchenServedquantity==0) {
                        openPassCodeDialog("deleteOrder", 0.0, "0.0") {
                            Log.i("Role", "Role$it")
                            when (it) {
                                1 -> {
                                    //For isKitchenPrintServed false don't allow to Delete
                                    if (orderTransList.size == 1) {
                                        // mDatabase.daoProductValidation().deleteProductValidation(order_id)
                                        if (mBinding.layoutModifier.visibility == View.VISIBLE) {
                                            mBinding.layoutModifier.visibility = View.GONE
                                            mBinding.gridItem.visibility = View.VISIBLE
                                            modifierList.clear()
                                        }
                                    } else if (selected_modifier != null && selected_modifier!!.iProductId == orderTransModel.itemId) {
                                        if (mBinding.layoutModifier.visibility == View.VISIBLE) {
                                            mBinding.layoutModifier.visibility = View.GONE
                                            mBinding.gridItem.visibility = View.VISIBLE
                                            modifierList.clear()
                                        }
                                    }
                                    //mDatabase.daoOrderTrans().deleteByItemId(orderTransModel.itemId)
                                    mDatabase.daoOrderTrans().deleteByItemIdAndId(orderTransModel.itemId,order_id,orderTransModel.id)
                                    cartAdapter?.run {
                                        discountMixMatchCalculation() {
                                            removeItemFromSet(position)
                                            getTotalAmount(this.getDataset())
                                            DisplayNewSaleDataToCDS(
                                                cartAdapter?.getDataset()!!,
                                                "",
                                                false
                                            )

                                            orderTransList.let {
                                                if (it.size == 0) {
                                                    var baseActivityPrint =
                                                        (activity as ParentActivity)
                                                    //Hardware Code For CDS Display -- Normal Image Display Case Invoke
                                                    baseActivityPrint.checkDrawOverlayPermission(
                                                        AppConstant.CDS_NORMAL_SCREEN,
                                                        "",
                                                        false,
                                                        "",
                                                        CDSSalesModel(),
                                                        "",
                                                        false
                                                    )
                                                    doResetCart()
                                                }
                                            }
                                        }
                                    }

                                    //calculateDiscountOnCart()
                                }
                                else -> {
                                    PromptMPinFaildForTaxExclude(getString(R.string.msg_delete_item))
                                }
                            }
                        }

                    }else{
                        showToast(
                            mActivity,
                            "You can't delete the item for kitchen products which is served",
                            R.drawable.icon,
                            custom_toast_container
                        )
                    }
                }else{
                    openPassCodeDialog("deleteOrder", 0.0, "0.0") {
                        Log.i("Role", "Role$it")
                        when (it) {
                            1 -> {
                                //For isKitchenPrintServed false don't allow to Delete
                                if (orderTransList.size == 1) {
                                    // mDatabase.daoProductValidation().deleteProductValidation(order_id)
                                    if (mBinding.layoutModifier.visibility == View.VISIBLE) {
                                        mBinding.layoutModifier.visibility = View.GONE
                                        mBinding.gridItem.visibility = View.VISIBLE
                                        modifierList.clear()
                                    }
                                } else if (selected_modifier != null && selected_modifier!!.iProductId == orderTransModel.itemId) {
                                    if (mBinding.layoutModifier.visibility == View.VISIBLE) {
                                        mBinding.layoutModifier.visibility = View.GONE
                                        mBinding.gridItem.visibility = View.VISIBLE
                                        modifierList.clear()
                                    }
                                }
                                //mDatabase.daoOrderTrans().deleteByItemId(orderTransModel.itemId)
                                mDatabase.daoOrderTrans().deleteByItemIdAndId(orderTransModel.itemId,order_id,orderTransModel.id)
                                cartAdapter?.run {
                                    discountMixMatchCalculation() {
                                        removeItemFromSet(position)
                                        getTotalAmount(this.getDataset())
                                        DisplayNewSaleDataToCDS(
                                            cartAdapter?.getDataset()!!,
                                            "",
                                            false
                                        )

                                        orderTransList.let {
                                            if (it.size == 0) {
                                                var baseActivityPrint =
                                                    (activity as ParentActivity)
                                                //Hardware Code For CDS Display -- Normal Image Display Case Invoke
                                                baseActivityPrint.checkDrawOverlayPermission(
                                                    AppConstant.CDS_NORMAL_SCREEN,
                                                    "",
                                                    false,
                                                    "",
                                                    CDSSalesModel(),
                                                    "",
                                                    false
                                                )
                                                doResetCart()
                                            }
                                        }
                                    }
                                }

                                //calculateDiscountOnCart()
                            }
                            else -> {
                                PromptMPinFaildForTaxExclude(getString(R.string.msg_delete_item))
                            }
                        }
                    }
                }
                /*if (orderTransModel.isKitchenPrintServed) {
                    openPassCodeDialog("deleteOrder", 0.0, "0.0") {
                        Log.i("Role", "Role$it")
                        when (it) {
                            1 -> {
                                //For isKitchenPrintServed false don't allow to Delete
                                if (orderTransList.size == 1) {
                                    // mDatabase.daoProductValidation().deleteProductValidation(order_id)
                                    if (mBinding.layoutModifier.visibility == View.VISIBLE) {
                                        mBinding.layoutModifier.visibility = View.GONE
                                        mBinding.gridItem.visibility = View.VISIBLE
                                        modifierList.clear()
                                    }
                                } else if (selected_modifier != null && selected_modifier!!.iProductId == orderTransModel.itemId) {
                                    if (mBinding.layoutModifier.visibility == View.VISIBLE) {
                                        mBinding.layoutModifier.visibility = View.GONE
                                        mBinding.gridItem.visibility = View.VISIBLE
                                        modifierList.clear()
                                    }
                                }
                                mDatabase.daoOrderTrans().deleteByItemId(orderTransModel.itemId)
                                cartAdapter?.run {
                                    discountMixMatchCalculation() {
                                        removeItemFromSet(position)
                                        getTotalAmount(this.getDataset())
                                        DisplayNewSaleDataToCDS(
                                            cartAdapter?.getDataset()!!,
                                            "",
                                            false
                                        )

                                        orderTransList.let {
                                            if (it.size == 0) {
                                                var baseActivityPrint =
                                                    (activity as ParentActivity)
                                                //Hardware Code For CDS Display -- Normal Image Display Case Invoke
                                                baseActivityPrint.checkDrawOverlayPermission(
                                                    AppConstant.CDS_NORMAL_SCREEN,
                                                    "",
                                                    false,
                                                    "",
                                                    CDSSalesModel(),
                                                    "",
                                                    false
                                                )
                                                doResetCart()
                                            }
                                        }
                                    }
                                }

                                //calculateDiscountOnCart()
                            }
                            else -> {
                                PromptMPinFaildForTaxExclude(getString(R.string.msg_delete_item))
                            }
                        }
                    }
                } else {
                    showToast(
                        mActivity,
                        "You can't delete the item for kitchen products which is served",
                        R.drawable.icon,
                        custom_toast_container
                    )
                }*/

            }
            "modifier" -> {
                if (orderTransModel.kitchenServedquantity==0) {
                    if (mBinding.layoutModifier.visibility == View.GONE) {
                        menuClose()
                        pos = position
                        item_id = orderTransModel.itemId
                        order_product_id=orderTransModel.id
                        mBinding.tvNoProductFound.visibility = View.GONE
                        openModifierLayout(orderTransModel.itemId, orderTransModel.itemName)
                    } else {
                        showToast(
                            mActivity,
                            getString(R.string.kindly_close_modifire_popup),
                            R.drawable.icon,
                            custom_toast_container
                        )
                    }
                } else {
                    showToast(
                        mActivity,
                        "You can't edit modifier item for Kitchen Products which is served",
                        R.drawable.icon,
                        custom_toast_container
                    )
                }
            }
            "price" -> {

                if (orderTransModel.kitchenServedquantity==0) {
                    /*val stringHashMap = mDatabase.daoOrderTrans()
                        .getPreModifierTypeTemp(orderTransModel.orderId, orderTransModel.itemId)*/
                    val stringHashMap = mDatabase.daoOrderTrans()
                        .getPreModifierTypeTemp_(orderTransModel.orderId, orderTransModel.itemId,order_product_id)

                    if (TextUtils.isEmpty(stringHashMap)) {
                        val dialogFragment = PriceChangeDialogFragment(
                            orderTransModel
                        ) { updatedRate ->

                            orderTransModel.itemRate = updatedRate
                            orderTransModel.itemAmount =
                                orderTransModel.itemQty * orderTransModel.itemRate
                            orderTransModel.taxAmount =
                                ((orderTransModel.itemAmount * orderTransModel.taxPercentage.toDouble()) / 100).toString()
                            mDatabase.daoOrderTrans().updateOrderTrans(orderTransModel)
                            openDiscountCalculation(orderTransModel)
                            cartAdapter?.run {
                                discountMixMatchCalculation() {
                                    notifyItemChanged(position)
                                    getTotalAmount(this.getDataset())
                                    DisplayNewSaleDataToCDS(
                                        cartAdapter?.getDataset()!!,
                                        "",
                                        false
                                    )
                                }
                            }
                            calculateDiscountOnCart()
                        }
                        dialogFragment.isCancelable = false
                        dialogFragment.setListener(object : ItemCallBack<OrderTransModel> {
                            override fun onItemClick(
                                action: String,
                                position: Int,
                                orderTransModel: OrderTransModel
                            ) {
                            }
                        })
                        dialogFragment.show(mActivity.supportFragmentManager, "dialog")
                    } else {
                        showToast(
                            mActivity,
                            getString(R.string.msg_modified_item_price_retriction),
                            R.drawable.icon, custom_toast_container
                        )
                    }
                } else {
                    showToast(
                        mActivity,
                        "You can't edit in-store price for kitchen products which is served",
                        R.drawable.icon,
                        custom_toast_container
                    )
                }
            }
            "manually" -> {
                val dialogFragment = QuantityChangeDialogFragment(
                    orderTransModel
                ) { quantity ->
                    var orderTranAddsModel = addManuallyItemToCart(orderTransModel,quantity.toInt())
                    mDatabase.daoOrderTrans().updateOrderTrans(orderTranAddsModel)
                    openDiscountCalculation(orderTranAddsModel)
                    cartAdapter?.run {
                        discountMixMatchCalculation() {
                            updateItem(orderTransList.asReversed())
                            getTotalAmount(this.getDataset())
                            calculateDiscountOnCart()

                            DisplayNewSaleDataToCDS(cartAdapter?.getDataset()!!, "", false)
                        }

                    }
                }
                dialogFragment.show(childFragmentManager, "dialog")


            }
            "openDiscount" -> {
                val dialogFragment = OpenDiscountDialogFragment(
                    mActivity,
                    orderTransModel
                ) { discountType: String, amount: String ->
                    if (amount.toFloat() > 0) {
                        if (discountType.equals(AppConstant.AMOUNT, true)) {
                            orderTransModel.discountAmount = amount.toFloat()
                            orderTransModel.discountRemark =
                                "Flat $currency_symbol${amount.toFloat()}"
                        } else {
                            val discountAmount =
                                ((orderTransModel.itemQty * orderTransModel.itemRate) * (amount.toFloat() / 100)).toFloat()
                            orderTransModel.discountAmount = discountAmount
                            orderTransModel.discountRemark = "$amount%"
                        }
                        orderTransModel.itemAmount =
                            ((orderTransModel.itemQty * orderTransModel.itemRate).toFloat() - orderTransModel.discountAmount).toDouble()
                        orderTransModel.iDiscountId = -1
                        mDatabase.daoOrderTrans()
                            .updateOrderTransForDiscount(orderTransModel)
                        cartAdapter?.run {
                            updateItem(orderTransList.asReversed())
                            getTotalAmount(this.getDataset())

                            DisplayNewSaleDataToCDS(cartAdapter?.getDataset()!!, "", false)
                        }
                        calculateDiscountOnCart()
                    } else {
                        orderTransModel.discountAmount = 0.0f
                        orderTransModel.iDiscountId = 0
                        orderTransModel.discountRemark = ""

                        orderTransModel.itemAmount =
                            orderTransModel.itemQty * orderTransModel.itemRate
                        mDatabase.daoOrderTrans()
                            .updateOrderTransForDiscount(orderTransModel)
                        discountMixMatchCalculation() {
                            cartAdapter?.run {
                                updateItem(orderTransList.asReversed())
                                getTotalAmount(this.getDataset())

                                DisplayNewSaleDataToCDS(cartAdapter?.getDataset()!!, "", false)
                            }
                            calculateDiscountOnCart()
                        }
                    }


                }

                dialogFragment.show(childFragmentManager, "dialog")
            }
        }
    }

    private fun openDiscountCalculation(orderTransModel: OrderTransModel) {
        if (orderTransModel.iDiscountId == -1 && orderTransModel.discountRemark.isNotEmpty()) {
            if (orderTransModel.discountRemark.contains("%")) {
                val discount = orderTransModel.discountRemark.replace("%", "").toInt()
                val discountAmount =
                    ((orderTransModel.itemQty * orderTransModel.itemRate) * (discount.toFloat() / 100)).toFloat()
                orderTransModel.discountAmount = discountAmount
                orderTransModel.itemAmount =
                    ((orderTransModel.itemQty * orderTransModel.itemRate).toFloat() - discountAmount).toDouble()
                mDatabase.daoOrderTrans()
                    .updateOrderTransForDiscount(orderTransModel)
                cartAdapter?.run {
                    updateItem(orderTransList.asReversed())
                    getTotalAmount(this.getDataset())

                    DisplayNewSaleDataToCDS(cartAdapter?.getDataset()!!, "", false)
                }
                calculateDiscountOnCart()
            } else {
                orderTransModel.itemAmount =
                    ((orderTransModel.itemQty * orderTransModel.itemRate).toFloat() - orderTransModel.discountAmount).toDouble()
                if (orderTransModel.discountAmount > orderTransModel.itemAmount) {
                    orderTransModel.discountAmount = 0.0f
                    orderTransModel.iDiscountId = 0
                    orderTransModel.discountRemark = ""

                    mDatabase.daoOrderTrans()
                        .updateOrderTransForDiscount(orderTransModel)
                    discountMixMatchCalculation() {
                        cartAdapter?.run {
                            updateItem(orderTransList.asReversed())
                            getTotalAmount(this.getDataset())

                            DisplayNewSaleDataToCDS(cartAdapter?.getDataset()!!, "", false)
                        }
                        calculateDiscountOnCart()
                    }
                }
            }
        }
    }

    private fun discountMixMatchCalculation(
        callback: (mixMachedFinished: Boolean) -> Unit
    ) {
        GlobalScope.launch(Dispatchers.Main) {
            val discountIds =
                discountListViewModel?.getDiscountIds() as ArrayList<Long>
            if (discountIds.isNotEmpty()) {
                for (itemDiscountId in discountIds) {
                    val buyDiscountListGroup =
                        discountListViewModel?.validateAutoApplyDiscountOnItemGroup(
                            itemDiscountId.toInt()
                        ) as ArrayList<DiscountAndProduct>
                    val discountByItemList = buyDiscountListGroup.filter {
                        orderTransList.any { item -> it.productId.toInt() == item.itemId && item.iDiscountId != -1 }
                    }
                    if (discountByItemList.isNotEmpty()) {
                        for (item in discountByItemList) {
                            if (item.discountMaster.discountType.equals(AppConstant.SALES_DISCOUNT)) {
                                var discountAmount = 0.0f
                                if (item.discountMaster.strType == CV.PERCENT) {
                                    for (orderItem in orderTransList) {
                                        if (item.productId.toInt() == orderItem.itemId) {
                                            discountAmount =
                                                ((orderItem.itemQty * orderItem.itemRate) * (item.discountMaster.dblValue / 100)).toFloat()
                                            orderItem.discountAmount = discountAmount
                                            orderItem.iDiscountId =
                                                item.discountMaster.discountId
                                            orderItem.discountRemark = item.discountMaster.name
                                            mDatabase.daoOrderTrans()
                                                .updateOrderTransForDiscount(orderItem)
                                        }
                                    }
                                }
                            }
                        }
                        //callback(true)
                    } else {
                        for (orderItem in orderTransList) {
                            if (itemDiscountId.toInt() != 0 && itemDiscountId.toInt() == orderItem.iDiscountId && orderItem.iDiscountId != -1) {
                                orderItem.discountAmount = 0.0f
                                orderItem.iDiscountId = 0
                                orderItem.discountRemark = ""
                                mDatabase.daoOrderTrans().updateOrderTransForDiscount(orderItem)
                            }
                        }
                        //callback(true)
                    }
                }

            }
            callback(true)
        }
    }

    private fun openModifierLayout(itemId: Int, itemName: String) {
        mBinding.layoutModifier.visibility = View.VISIBLE
        val itamModify = getString(R.string.modifying) + " " + itemName
        mBinding.layoutModifier.tvTitle.text = itamModify
        mBinding.gridItem.visibility = View.GONE
        setModifierData(itemId)
        mBinding.layoutModifier.ivInstruction.setOnClickListener{
            try{
                openInstructionDialog()
            }catch (e: java.lang.Exception)
            {
                e.printStackTrace()
            }
        }
    }

    private fun checkIsRequiredModifireSelected(): Boolean {
        var modifierMap = HashMap<String, List<IngredientMasterModel>>()
        //val stringHashMap = mDatabase.daoOrderTrans().getPreModifierTypeTemp(order_id, item_id)
        val stringHashMap = mDatabase.daoOrderTrans().getPreModifierTypeTemp_(order_id, item_id,order_product_id)

        if (!TextUtils.isEmpty(stringHashMap)) {
            // method to convert string to hasMap
            val turnsType =
                object : TypeToken<HashMap<String, List<IngredientMasterModel>>>() {}.type
            modifierMap = Gson().fromJson<HashMap<String, List<IngredientMasterModel>>>(
                stringHashMap,
                turnsType
            )
        }
        var ingredientMasterList = ArrayList<IngredientMasterModel>()
        tempHashMap.clear()
        var filteredModifierList =
            modifierList.filter { item -> item.strIsRequired == CV.REQUIRED }
        if (filteredModifierList.isNotEmpty()) {
            var counter = 0
            for ((k, v) in modifierMap) {
                ingredientMasterList.addAll(v as ArrayList<IngredientMasterModel>)
            }
            for (item in filteredModifierList) {
                val filtredList =
                    ingredientMasterList.filter { tItem -> tItem.iModifierGroupId == item.iModifierGroupId && tItem.tIsDefault != 1 }
                if (filtredList.isNotEmpty()) {
                    counter = counter + 1
                }
            }
            return filteredModifierList.size == counter
        }
        return true
    }

    private fun adapter(mList: MutableList<OrderTransModel>, type: String) {
        cartAdapter = OrderItemListAdapter(
            mActivity,
            mDatabase,
            mList,
            this@NewSalesFragment,
            type,
            order_id
        )
        mBinding.rvCart.adapter = cartAdapter
    }

    fun calculateTotalProcedure(
        orderList: List<OrderTransModel>,
        istaxConsider: Boolean
    ): Double {
        var finalTotal = 0.0

        for (i in orderList.indices) {
            val orderTransModel = orderList[i]

            val itemQty = orderTransModel.itemQty
            val itemRate = orderTransModel.itemRate
            val itemPrice = itemQty * itemRate
            val total_tax_amount: Double =
                if (istaxConsider) orderTransModel.taxAmount.toDouble() * orderTransModel.itemQty else 0.0

            val totalAmount =
                itemPrice + total_tax_amount - orderTransModel.discountAmount.toDouble()
            finalTotal += totalAmount
        }

        return finalTotal
    }

    fun calculateTotalTaxProcedure(orderList: List<OrderTransModel>): Double {
        var totalTaxAmount = 0.0

        for (i in orderList.indices) {
            val orderTransModel = orderList[i]

            val itemQty = orderTransModel.itemQty
            val itemRate = orderTransModel.itemRate
            val itemPrice = itemQty * itemRate
            val taxRate = orderTransModel.taxPercentage.toDouble() / 100
            val taxAmount = itemPrice * taxRate
            totalTaxAmount += taxAmount
        }

        return totalTaxAmount
    }

    fun isValidCopunProduct(orderTransModel: OrderTransModel): Boolean {
        var isValidProduct: Boolean? = false
        if (selected_history != null && selected_history!!.orderProducts != null && selected_history!!.status != AppConstant.ON_HOLD) {

            var orderProducts: List<OrderProductsModel>?
            if (isRefundDialogOpen) {
                orderProducts = selected_history!!.orderProducts?.filter {
                    it.iProductId == orderTransModel.refundProductId
                }
            } else {
                orderProducts = selected_history!!.orderProducts?.filter {
                    it.iProductId.toString() == orderTransModel.itemId.toString()
                }
                if (orderProducts.isNullOrEmpty()) {
                    orderProducts = selected_history!!.orderProducts?.filter {
                        it.iCheckItemDetailId == orderTransModel.itemId.toString()
                    }
                }

            }

            val discountAndProduct =
                discountListViewModel?.checkProductCouponDetail(
                    orderProducts?.get(0)?.iProductId!!.toLong(),
                    selected_history!!.orderDiscounts?.get(0)?.iDiscountId!!.toLong()
                )
            if (discountAndProduct != null) {
                isValidProduct =
                    discountListViewModel?.checkDiscountMappingItemIsAvailable(
                        orderProducts?.get(0)?.iProductId!!.toLong(),
                        selected_history!!.orderDiscounts?.get(0)?.iDiscountId!!.toLong()
                    )
            } else {
                isValidProduct = false
            }
        } else {
            isValidProduct =
                discountListViewModel?.checkDiscountMappingItemIsAvailable(
                    orderTransModel.itemId.toLong(),
                    discountListViewModel?.selectedCartDiscount?.discountId!!.toLong()
                )
        }
        return isValidProduct!!
    }

    fun calculateCartProcedure(
        orderList: List<OrderTransModel>,
        appliedDiscount: Boolean,
        callBack: (cart: CartSummaryData) -> Unit
    ) {
        var totalAmount = 0.0
        var taxAmount = 0.0
        var totalQty = 0
        var totalDiscount = 0.0f
        for (i in orderList.indices) {
            val orderTransModel = orderList[i]
            var txtRateInPercentage = 0.0
            if (!TextUtils.isEmpty(orderTransModel.taxPercentage)) {
                txtRateInPercentage = orderTransModel.taxPercentage.toDouble()
            }

            var quantity = orderTransModel.itemQty
            totalQty += quantity
            val rate = orderTransModel.itemRate
            var discountAmount = 0.0

            orderTransModel.discountAmount?.let {
                discountAmount = it.toDouble()
            }

            if (discountAmount == 0.0 && appliedDiscount) {
                /*
                   item level discount implement when copun discount is applied on particular item using weightage wise
                */
                discountListViewModel?.selectedCartDiscount?.let { selectedDiscount ->
                    if (selectedDiscount.discountType.equals(AppConstant.COUPON_DISCOUNT)) {
                        var isValidProduct = isValidCopunProduct(orderTransModel)
                        if (isValidProduct!!) {
                            if (orderTransModel.discountAmount == 0.0f) {
                                val amountCopoun = (quantity * rate)
                                val itemWeightAge =
                                    (amountCopoun * 100) / totalApplicableItemDiscount
                                discountAmount =
                                    ((coupnDiscountAmount * itemWeightAge) / 100).toDouble()
                            } else {
                                discountAmount = 0.0
                            }
                        } else {
                            discountAmount = 0.0
                        }
                    } else {
                        val amountCopoun = (quantity * rate)
                        val itemWeightAge = (amountCopoun * 100) / totalApplicableItemDiscount
                        discountAmount =
                            ((coupnDiscountAmount * itemWeightAge) / 100).toDouble()
                    }
                }
            }


            val amount = (quantity * rate) - discountAmount

            var tax_amount = 0.0
            var taxrate = 0.0
            customerListViewModel?.isTaxIncluded?.let {
                if (it.value!!)
                    taxrate = (txtRateInPercentage / 100)
            }




            if (discountAmount == 0.0 && !appliedDiscount) {
                tax_amount = getTaxAmountAppliedDiscount(orderTransModel)
                tax_amount = String.format("%.2f", tax_amount).toDouble()
                if (tax_amount == 0.0) {
                    tax_amount = amount * taxrate
                }
            } else {
                tax_amount = amount * taxrate // (amount * (txtRateInPercentage / 100))
            }

            tax_amount = String.format("%.2f", (tax_amount)).toDouble()
            totalAmount += String.format("%.2f", amount).toDouble()
            taxAmount += tax_amount
        }


        if (selected_history != null) {


            if (selected_history!!.orderDiscounts.isNullOrEmpty()) {

                discountListViewModel?.selectedCartDiscount?.let {
                    totalDiscount += getDiscountAmountOnCart(true)
                } ?: discountListViewModel?.getAppliedDiscountOnCart(order_id)?.run {
                    for (discountApplyModel in this) {
                        totalDiscount += discountApplyModel.discount_amount
                    }
                }

            } else {
                if (selected_history!!.status.equals(AppConstant.ON_HOLD)) {
                    discountListViewModel?.selectedCartDiscount?.let {
                        totalDiscount += getDiscountAmountOnCart(true)
                    } ?: discountListViewModel?.getAppliedDiscountOnCart(order_id)?.run {
                        for (discountApplyModel in this) {
                            totalDiscount += discountApplyModel.discount_amount
                        }
                    }
                } else {
                    selected_history?.run {
                        this.orderDiscounts?.let {
                            if (it.size > 0) {
                                val orderDiscountsModel = it[0]
                                setCartDiscountReadableView(orderDiscountsModel)
                                totalDiscount += orderDiscountsModel.dblDiscountAmt

                                if (selected_history!!.status.equals(AppConstant.ON_HOLD)) {
                                    mBinding.cbDiscountApplied.isEnabled = true
                                }
                            }
                        }

                    }
                }
            }
        } else {
            discountListViewModel?.selectedCartDiscount?.let {
                totalDiscount += getDiscountAmountOnCart(true)
            } ?: discountListViewModel?.getAppliedDiscountOnCart(order_id)?.run {
                for (discountApplyModel in this) {
                    totalDiscount += discountApplyModel.discount_amount
                }
            }
        }

        displayCalculation(taxAmount, totalAmount, totalQty, totalDiscount)
        callBack(
            CartSummaryData(
                totalQty,
                totalAmount,
                taxAmount,
                totalDiscount
            )
        )
    }

    private fun setCartDiscountReadableView(orderDiscountsModel: OrderDiscountsModel) {
        orderDiscountsModel?.let {
//            Log.e("DEBUG", "Discount strRemark set on Cart Screen --- "+it.strRemark)
            mBinding.cbDiscountApplied.text =
                it.strRemark + CV.WAS_APPLIED
            mBinding.cbDiscountApplied.show()
            mBinding.ivAddDiscount.hide()
            mBinding.cbDiscountApplied.isChecked = true
            mBinding.cbDiscountApplied.isEnabled = false
        }
    }

    fun calculateCartSelectedRefundProcedure(
        orderList: List<OrderTransModel>,
        isFromRefundRePrint:Boolean = false,
        callBack: (cart: CartRefundSummaryData) -> Unit
    ) {
        var orderAllSaleDiscountItems: ArrayList<OrderTransModel> = ArrayList()
        var orderAvailableAllCartDiscountItems: ArrayList<OrderTransModel> = ArrayList()

        for (orderItem in orderList) {
            if (orderItem.iDiscountId != 0) {
                orderAllSaleDiscountItems.add(orderItem)
            } else {
                orderAvailableAllCartDiscountItems.add(orderItem)
            }
        }
        var totalAmount = 0.0
        var taxAmount = 0.0
        var totalQty = 0
        var totalDiscount = 0.0f
        var saleTaxAmount = 0.0
        var saleDiscountAmount = 0.0f
        var saleTotalAmount = 0.0

        for (orderTransModel in orderAllSaleDiscountItems) {
            var quantity = orderTransModel.itemQty
            totalQty += quantity
            val rate = orderTransModel.itemRate
            var tax_amount = 0.0
            val discountAmount =
                (orderTransModel.discountAmount.toDouble() * orderTransModel.itemQty) / orderTransModel.totalItemQty
            tax_amount =
                NewSaleFragmentCommon.taxAmountCalculation(orderTransModel, discountAmount)
            saleDiscountAmount += discountAmount.toFloat()
            saleTotalAmount += (quantity * rate) - discountAmount
            saleTaxAmount += tax_amount
        }

        if (selected_history != null) {
            selected_history?.run {
                this.orderDiscounts?.let {
                    var isValidProduct = false
                    for (refundOrderItem in orderAvailableAllCartDiscountItems) {
                        if (it.isNotEmpty()) {
                            if (it[0].strDiscountType.equals(AppConstant.COUPON_DISCOUNT)) {
                                val isValid =
                                    NewSaleFragmentCommon.isValidateCartDiscountedProduct(
                                        discountListViewModel!!,
                                        refundOrderItem.refundProductId,
                                        it[0].iDiscountId
                                    )
                                if (isValid) {
                                    isValidProduct = true
                                }
                            } else {
                                isValidProduct = true
                            }
                        }
                        var tax_amount = getTaxAmountAppliedDiscount(refundOrderItem)
                        taxAmount += tax_amount
                        totalQty += refundOrderItem.itemQty
                        totalAmount += refundOrderItem.itemQty * refundOrderItem.itemRate
                    }
                    if (it.isNotEmpty() && isValidProduct) {
                        val orderDiscountsModel = it[0]
                        setCartDiscountReadableView(orderDiscountsModel)
                        totalDiscount += getRemainingRefundDiscount(
                            orderDiscountsModel,
                            orderAvailableAllCartDiscountItems,
                            isFromRefundRePrint = isFromRefundRePrint
                        )

                    }
                }
            }
        }
        var mTotalTax = String.format("%.2f", (taxAmount)).toDouble()
        callBack(
            CartRefundSummaryData(
                totalQty,
                totalAmount,
                mTotalTax,
                saleTaxAmount,
                saleDiscountAmount,
                saleTotalAmount,
                totalDiscount
            )
        )
    }

    private fun getTotalAmount(orderList: List<OrderTransModel>) =
        if (cartAdapter != null || orderList.isEmpty()) {
            this.calculateCartProcedure(orderList, true) { cart: CartSummaryData ->
                if ((cart.totalAmount + cart.totalDiscount) >= cart.totalDiscount) {
                    displayCalculation(
                        cart.taxAmount,
                        cart.totalAmount,
                        cart.totalQty,
                        cart.totalDiscount
                    )
                    if (cart.totalDiscount <= 0) {
                        if (mBinding.cbDiscountApplied.visibility == View.VISIBLE) {
                            clearDiscountData()
                            //Hardware Code For CDS Display -- NewSale While remove discount
                            DisplayNewSaleDataToCDS(cartAdapter?.getDataset()!!, "", false)
                        }
                    }
                } else if (mBinding.cbDiscountApplied.visibility == View.VISIBLE) {
                    clearDiscountData()
                    //Hardware Code For CDS Display -- NewSale While remove discount
                    DisplayNewSaleDataToCDS(cartAdapter?.getDataset()!!, "", false)
                }
            }
        } else {
            displayCalculation(00.00, 00.00, 0, 0.0f)
        }

    private fun displayCalculation(
        taxAmount: Double,
        total: Double,
        totalQty: Int,
        totalDiscount: Float
    ) {

        mBinding.tvTotal.text = getFormatedAmountWithCurrencySymbol(total + totalDiscount)
        var isTaxIncluded = true
        if (selected_history != null) {
            isTaxIncluded = !selected_history!!.tCheckTaxExempt.equals("0")
        }
        var final_tax_anount = 00.00
        if (isTaxIncluded) {
            final_tax_anount = taxAmount
        } else {
            final_tax_anount = 00.00
        }

        var totalPayableAmount = 00.00
        if (customerListViewModel?.isTaxIncluded?.value!!) {
            mBinding.tvTax.text = getFormatedAmountWithCurrencySymbol(final_tax_anount)
            totalPayableAmount = total + final_tax_anount/* - totalDiscount*/
        } else {
            mBinding.tvTax.text = getFormatedAmountWithCurrencySymbol(00.00)
            totalPayableAmount = total /*- totalDiscount*/
        }


        mBinding.tvTotalValue.text = getFormatedAmountWithCurrencySymbol(totalPayableAmount)
        mBinding.tvTotalSavingsValue.text =
            getFormatedAmountWithCurrencySymbol(getTotalSavingAmount(totalDiscount))

        if (selected_history != null) {
            val isOnHold = selected_history!!.status
            if (isOnHold == AppConstant.ON_HOLD) {

            } else {
                if (selected_history?.orderRefund?.isEmpty()!!) {
                    mBinding.tvTotalSavingsValue.visibility = View.VISIBLE
                    mBinding.tvTotalSavingsLable.visibility = View.VISIBLE
                } else {
                    mBinding.tvTotalSavingsValue.visibility = View.GONE
                    mBinding.tvTotalSavingsLable.visibility = View.GONE
                }
            }

        } else {
            mBinding.tvTotalSavingsValue.visibility = View.VISIBLE
            mBinding.tvTotalSavingsLable.visibility = View.VISIBLE
        }

        mBinding.tvTotalQty.text = "$totalQty"
        mBinding.tvDiscountValue.text = getFormatedAmountWithCurrencySymbol(totalDiscount)

    }


    private fun resetCartTotalCalculation() {
        displayCalculation(00.00, 00.00, 0, 0.0f)

        mBinding.tvTotalSavingsValue.visibility = View.VISIBLE
        mBinding.tvTotalSavingsLable.visibility = View.VISIBLE
    }

    private fun doResetCart() {
        resetExistingEmployee()
        webCallGetLastAction()
        mDatabase.daoOrderTrans().deleteOrder(order_id)
        mDatabase.daoProductValidation().deleteProductValidation(order_id)
        retry_count=0
        total_tax_amount = 0.0
        total = 0.0
        CV.server_order_id = 0
        paymentList.clear()
        paymentRequestList.clear()
        CV.ORDER_DATE = ""
        CV.REMAIN_AMOUNT = 0.0
        CV.GIVEN_AMOUNT = 0.0
        mBinding.tvScan.text = ""
        clearDiscountData()
        removeOnHoldOrder()
        searchViewModel?.getBestsellerProduct()
        tempHashMap.clear()
        productRequireHashMap.clear()
        customerListViewModel?.selectedCustomer = customerListViewModel?.walkInCustomer
        setCustomerName()
        order_id = ""
        temp_order_id = ""
        selectedAge = null
        CM.setSp(mActivity, CV.ORDER_ID, "")
        orderTransList.clear()
        setTaxIncludedStatus(true)
        adapter(orderTransList, eventName)
        (mBinding.rvCart.adapter as OrderItemListAdapter).notifyDataSetChanged()
        resetCartTotalCalculation()
        mBinding.rvCategory.adapter = CategoryListAdapter(mActivity, categoryList, this)
        populateSubcategory(categoryList[0].iProductCategoryID)
        mBinding.tvCategoryValue.text = categoryList[0].strProductCategoryName
        setEvent(AppConstant.ORDER)
        mBinding.layoutModifier.visibility = View.GONE
        mBinding.gridItem.visibility = View.VISIBLE
        mBinding.ivAddCustomer.isEnabled = true
        mBinding.ivAdd.visibility = View.VISIBLE
        selected_history = null
        mBinding.cbDiscountApplied.isEnabled = true
        isKitchenPrintServed = true
        InvoiceNumber = ""
        placeOrderViewModel?.tIsApplePay = false
        refundItemList.clear()
        mBinding.gridItem.isEnabled=true
        mBinding.btnCheckout.isEnabled=true
    }

    private fun clearDiscountData() {

        discountListViewModel?.deleteAppliedDiscountOnCart(order_id)

        mBinding.ivAddDiscount.visibility = View.VISIBLE
        mBinding.cbDiscountApplied.visibility = View.GONE
        mBinding.cbDiscountApplied.text = " "
        discountListViewModel?.selectedCartDiscount = null
        mBinding.tvDiscountValue.text = getFormatedAmountWithCurrencySymbol(0.0)
        if (selected_history != null) {
            selected_history!!.orderDiscounts = emptyList()
        }
        getTotalAmount(orderTransList)
    }

    fun onCancelProcedure() {
        setEvent(AppConstant.ORDER)
        doResetCart()
        //Hardware Code For CDS Display -- NewSale Cancel case invoke
        DisplayNewSaleDataToCDS(
            ArrayList<OrderTransModel>(),
            "This order has been canceled.",
            false
        )
    }

    fun onRefundCancelProcedure() {
        setEvent(AppConstant.ORDER)
        doResetCart()
        mBinding.btnRefund.visibility = View.GONE
        mBinding.btnExchange.visibility = View.GONE
        mBinding.btnPrint.visibility = View.GONE
        mBinding.btnCancel.visibility = View.GONE
        mBinding.btnSave.visibility = View.VISIBLE
        mBinding.btnCheckout.visibility = View.VISIBLE
        mBinding.btnVoid.visibility = View.VISIBLE
        mBinding.cbTax.visibility = View.VISIBLE

        mBinding.ivStamp.visibility = View.GONE
        setTaxIncludedStatus(true)
        customerListViewModel?.selectedCustomer = customerListViewModel?.walkInCustomer
        customerListViewModel?.getCustomer("")
    }

    private fun removeOnHoldOrder() {
        selected_history?.run {
            if (this.status == AppConstant.ON_HOLD) {
                mDatabase.daoOrderTrans().deleteOrderFromMaster(this.iCheckDetailID)
                mDatabase.daoOrderTrans().deleteById(this.iCheckDetailID)
                selected_history = null
            }
        }
    }

    private fun setTaxIncludedStatus(status: Boolean) {
        customerListViewModel?.let {
            it.isTaxIncluded.value = status
            if (selected_history != null) {
                if (status) {
                    selected_history!!.tCheckTaxExempt = "1"
                } else {
                    selected_history!!.tCheckTaxExempt = "0"

                }

            }
        }
    }


    private fun connectPaymentGateway(
        cardType: String
    ) {
        paymentViewModel.server_order_id=CV.server_order_id.toString()
        paymentViewModel.getConnect(true).observe(this, androidx.lifecycle.Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        paymentViewModel?.sendBoltrequest(paymentViewModel.bolt_order_id,CV.server_order_id.toString(),"","","",Gson().toJson(resource))?.observe(this,androidx.lifecycle.Observer {
                            LogUtil.d("Boltresponsedata",it.toString())
                        })
                        Log.d("Data", "getConnect ${resource.data}")
//                        readConfirmation()
                        readInput(
                            cardType
                        )
                        displayStoreName()
                    }
                    Status.ERROR -> {
                        paymentViewModel?.sendBoltrequest(paymentViewModel.bolt_order_id,CV.server_order_id.toString(),"","","",Gson().toJson(resource))?.observe(this,androidx.lifecycle.Observer {
                            LogUtil.d("Boltresponsedata",it.toString())
                        })
                        Log.d("Data", "getConnect ${resource.message}")
                        resource.message?.let { it1 -> handleError(it1) }
                    }
                    Status.LOADING -> {
                        Log.d("Data", "getConnect ${resource.status}")

                        showPaymentProgressDialog()
                        paymentViewModel.getPaymentStatusObserver.value =
                            AppConstant.PaymentProcess.LOADING.value
                        updateProgressDialogTitle(getPaymentProgressMsg())

                    }
                }
            }
        })
    }

    private fun displayStoreName() {
        paymentViewModel.display(String.format(getString(R.string.welcome_msg), StoreName))
    }

    private fun readInput(
        cardType: String
    ) {
        if (cardType.equals(AppConstant.CREDIT, true)) {
            payment_mode = AppConstant.CREDIT
            paymentViewModel.selectedPaymentMode = AppConstant.CREDIT
            paymentViewModel.cardRequestModel.includePin = false
            if ((CM.getSp(
                    context!!,
                    CV.SIGNATURE_AMOUNT,
                    0.0f
                ) as Float) < (paymentViewModel.payableAmount.toFloat() / 100)
            ) {
                paymentViewModel.cardRequestModel.includeSignature = true
            } else {
                paymentViewModel.cardRequestModel.includeSignature = false
            }
            placeOrderViewModel?.tIsApplePay = false
        } else if (cardType.equals(AppConstant.MANUAL_CARD, true)) {
            payment_mode = AppConstant.MANUAL_CARD
            payment_type = AppConstant.MANUAL_CARD
            paymentViewModel.selectedPaymentMode = AppConstant.MANUAL_CARD
            paymentViewModel.manualCardReadRequestModel.includeSignature = false
            paymentViewModel.manualCardReadRequestModel.includeExpirationDate = true
            paymentViewModel.manualCardReadRequestModel.includePIN = true
            placeOrderViewModel?.tIsApplePay = false
        } else if (cardType.equals(AppConstant.APPLE_PAY, true)) {
            payment_mode = AppConstant.APPLE_PAY
            paymentViewModel.selectedPaymentMode = AppConstant.APPLE_PAY
            paymentViewModel.cardRequestModel.includePin = false
            paymentViewModel.cardRequestModel.includeSignature = false
            placeOrderViewModel?.tIsApplePay = true
        } else {
            payment_mode = AppConstant.DEBIT
            paymentViewModel.selectedPaymentMode = AppConstant.DEBIT
            paymentViewModel.cardRequestModel.includePin = true
            paymentViewModel.cardRequestModel.includeSignature = false
            placeOrderViewModel?.tIsApplePay = false
        }
        if (cardType.equals(AppConstant.MANUAL_CARD, true)) {
            //readManualCard()
            authManualCard(temp_order_id)
        } else {
            authCard(
                cardType
            )
        }
    }

    private fun readConfirmation() {

        paymentViewModel.readConfirmation().observe(this, androidx.lifecycle.Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        Log.d("Data", "readConfirmation ${resource.data}")
                        val readConfirmationResponse = resource.data as ReadConfirmationResponse

                        if (readConfirmationResponse.confirmed) {
                            paymentViewModel.selectedPaymentMode = AppConstant.CREDIT
                            paymentViewModel.cardRequestModel.includePin = false
                            paymentViewModel.cardRequestModel.includeSignature = true

                        } else {
                            paymentViewModel.selectedPaymentMode = AppConstant.DEBIT
                            paymentViewModel.cardRequestModel.includePin = true
                            paymentViewModel.cardRequestModel.includeSignature = false
                        }

                        //authCard()
                    }
                    Status.ERROR -> {
                        Log.d("Data", "readConfirmation ${resource.message}")
                        resource.message?.let { it1 -> handleError(it1) }

                    }
                    Status.LOADING -> {
                        Log.d("Data", "readConfirmation ${resource.status}")
                        paymentViewModel.getPaymentStatusObserver.value =
                            AppConstant.PaymentProcess.SWIPE.value


                    }
                }
            }
        })
    }


    /*private fun readManualCard() {
       paymentViewModel.readManualCard().observe(this, androidx.lifecycle.Observer {
           it?.let { resource ->
               when (resource.status) {
                   Status.SUCCESS -> {
                       LogUtil.d("Manual Card", "${resource.data}")
                       // call card read data
                       paymentViewModel.manualCardResponseModel = resource.data

                       paymentViewModel.manualCardResponseModel?.let { manualCardResponseModel ->
                           if (manualCardResponseModel.token != "") {
                               authManualCard(manualCardResponseModel.token,order_id)
                           } else {
                               handleError(manualCardResponseModel.resptext)
                           }
                       }

                   }
                   Status.ERROR -> {
                       Log.d("Data", "Read card ${resource.message}")
                       resource.message?.let { it1 -> handleError(it1) }
                   }
                   Status.LOADING -> {
                       Log.d("Data", "Read card ${resource.status}")
                       paymentViewModel.getPaymentStatusObserver.value =
                           AppConstant.PaymentProcess.AUTHENTICATION.value
                   }
               }
           }
       })
   }*/
    private fun authManualCard(orderId: String) {
        paymentViewModel.server_order_id=CV.server_order_id.toString()
        paymentViewModel.authManual(orderId).observe(this, androidx.lifecycle.Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        paymentViewModel?.sendBoltrequest(paymentViewModel.bolt_order_id,CV.server_order_id.toString(),"",Gson().toJson(resource),"","")?.observe(this,androidx.lifecycle.Observer {
                            LogUtil.d("Boltresponsedata",it.toString())
                            paymentViewModel.bolt_order_id="0"
                        })
                        LogUtil.d("DEBIT DEBUG", "authCard ${resource.data}")
                        // call card read data
                        paymentViewModel.cardResponseModel = resource.data

                        paymentViewModel.cardResponseModel?.let { cardResponse ->
                            if (cardResponse.respcode == "000") {
                                dismissPaymentProgressDialog()
                                val paidAmt = paymentViewModel.payableAmount.toDouble() / 100
                                //orderGenerate(0.0, paidAmt.toString())
                                webCall_update_payment_type()
                                orderPaymentRequestWithOrderGenerate(
                                    payment_type,
                                    0.0,
                                    paidAmt.toString()
                                )

                            } else {
                                Btncalculate_change_flag=true
                                var btnLastPayment =
                                    dialogcheckout!!.findViewById<Button>(R.id.btnCheckLastPayment)
                                btnLastPayment.visibility = View.VISIBLE
                                handleError(cardResponse.resptext)
                            }
                        }

                    }
                    Status.ERROR -> {
                        paymentViewModel?.sendBoltrequest(paymentViewModel.bolt_order_id,CV.server_order_id.toString(),"",Gson().toJson(resource),"","")?.observe(this,androidx.lifecycle.Observer {
                            LogUtil.d("Boltresponsedata",it.toString())
                            paymentViewModel.bolt_order_id="0"
                        })
                        Btncalculate_change_flag=true
                        var btnLastPayment =
                            dialogcheckout!!.findViewById<Button>(R.id.btnCheckLastPayment)
                        btnLastPayment.visibility = View.VISIBLE
                        Log.d("Data", "authCard ${resource.message}")
                        resource.message?.let { it1 -> handleError(it1) }
                    }
                    Status.LOADING -> {
                        Log.d("Data", "authCard ${resource.status}")
                        paymentViewModel.getPaymentStatusObserver.value =
                            AppConstant.PaymentProcess.AUTHENTICATION.value
                    }
                }
            }
        })
    }

    private fun authCard(
        cardType: String
    ) {
        paymentViewModel.cardRequestModel.amount = paymentViewModel.payableAmount
        paymentViewModel.server_order_id=CV.server_order_id.toString()

        paymentViewModel.authCard(temp_order_id).observe(this, androidx.lifecycle.Observer {
            it?.let { resource ->


                when (resource.status) {
                    Status.SUCCESS -> {
                        LogUtil.d("DEBIT DEBUG", "authCard ${resource.data}")
                        // call card read data
                        paymentViewModel?.sendBoltrequest(paymentViewModel.bolt_order_id,CV.server_order_id.toString(),"",Gson().toJson(resource),"","")?.observe(this,androidx.lifecycle.Observer {
                            LogUtil.d("Boltresponsedata",it.toString())
                            paymentViewModel.bolt_order_id="0"
                        })
                        paymentViewModel.cardResponseModel = resource.data

                        paymentViewModel.cardResponseModel?.let { cardResponse ->
                            if (cardResponse.respcode == "000") {
                                dismissPaymentProgressDialog()
                                val paidAmt = paymentViewModel.payableAmount.toDouble() / 100
                                //orderGenerate(0.0, paidAmt.toString())
                                webCall_update_payment_type()
                                orderPaymentRequestWithOrderGenerate(
                                    cardType,
                                    0.0,
                                    paidAmt.toString()
                                )

                            } else {
                                Btncalculate_change_flag=true
                                var btnLastPayment =
                                    dialogcheckout!!.findViewById<Button>(R.id.btnCheckLastPayment)
                                btnLastPayment.visibility = View.VISIBLE
                                handleError(cardResponse.resptext)
                            }
                        }

                    }
                    Status.ERROR -> {
                        paymentViewModel?.sendBoltrequest(paymentViewModel.bolt_order_id,CV.server_order_id.toString(),"",Gson().toJson(resource),"","")?.observe(this,androidx.lifecycle.Observer {
                            paymentViewModel.bolt_order_id="0"
                            LogUtil.d("Boltresponsedata",it.toString())
                        })
                        Btncalculate_change_flag=true
                        var btnLastPayment =
                            dialogcheckout!!.findViewById<Button>(R.id.btnCheckLastPayment)
                        btnLastPayment.visibility = View.VISIBLE
                        Log.d("Data", "authCard ${resource.message}")
                        resource.message?.let { it1 -> handleError(it1) }
                    }
                    Status.LOADING -> {
                        Log.d("Data", "authCard ${resource.status}")
                        paymentViewModel.getPaymentStatusObserver.value =
                            AppConstant.PaymentProcess.AUTHENTICATION.value
                    }
                }
            }
        })
    }


    private fun disConnectPaymentGateway() {

        paymentViewModel.disConnect().observe(this, androidx.lifecycle.Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        Log.d("Data", "disConnectPaymentGateway ${resource.data}")
                        dismissKcsDialog()
                    }
                    Status.ERROR -> {
                        Log.d("Data", "disConnectPaymentGateway ${resource.message}")
                        resource.message?.let { it1 -> handleError(it1) }
                    }
                    Status.LOADING -> {
                        Log.d("Data", "disConnectPaymentGateway ${resource.status}")

                    }
                }
            }
        })
    }

    private fun handleError(errorMessage: String) {
        if (errorMessage.contains("connection abort")) {
            paymentViewModel.msgCancel = getString(R.string.msg_network_error)
        } else if (errorMessage.contains("timeout")) {
            paymentViewModel.msgCancel = getString(R.string.payment_error)
        } else if (errorMessage.contains("HTTP 401")) {
            paymentViewModel.msgCancel = getString(R.string.error_message_401)
        } else if (errorMessage.contains("HTTP 400")) {
            paymentViewModel.msgCancel = getString(R.string.error_message_400)
        } else if (errorMessage.contains("HTTP 500")) {
            paymentViewModel.msgCancel = getString(R.string.error_message_500)
        } else {
            paymentViewModel.msgCancel = errorMessage
        }

        paymentViewModel.getPaymentStatusObserver.value =
            AppConstant.PaymentProcess.CANCEL.value
        paymentViewModel.cancel().observe(this, androidx.lifecycle.Observer {
            it?.let { resource ->
                Log.d("CancelRequest",resource.status.toString());
            }
        })
    }

    fun showRestrictDialog(displayMsg: String) {
        val dialog = Dialog(mActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.setCallback(
                UserInteractionAwareCallback(
                        dialog.window!!.getCallback(),
                        mActivity
                )
        )
        dialog.setContentView(R.layout.dialog_restrict)
        val tvLabel = dialog.findViewById<TextView>(R.id.tvLabel)
        tvLabel.setText(displayMsg)
        dialog.setCancelable(false)
        dialog.show()

        val btnok = dialog.findViewById<Button>(R.id.btnok)
        btnok.setOnClickListener {
            dialog.dismiss()
        }
    }

    private fun showDiscountDialogFragment() {
        val dialogFragment = DiscountDialogFragment()
        dialogFragment.isCancelable = false
        dialogFragment.setListener(object : ItemCallBack<DiscountMaster> {
            override fun onItemClick(
                action: String,
                position: Int,
                orderTransModel: DiscountMaster
            ) {
                discountListViewModel?.selectedCartDiscount = orderTransModel
                if (orderTransList.size > 0) {
                    //Apply discount
                    if (discountListViewModel?.selectedCartDiscount?.strAppliedTo == CV.CHECK) {
                        discountListViewModel?.selectedCartDiscount?.let {
                            var emp_role = CM.getSp(mActivity, CV.EMP_ROLE, "") as String
                            if (it.discountType == AppConstant.SIMPLE_DISCOUNT && it.isAdminApprovalRequired == 1 && emp_role != AppConstant.STORE_ADMIN) {
                                openPassCodeDialog(CV.APPLY_DISCOUNT, 0.0, "0.0") {
                                    Log.i("Role", "Role$it")
                                    when (it) {
                                        1 -> {
                                            calculateDiscountOnCart()
                                        }
                                        0 -> {
                                            discountListViewModel?.selectedCartDiscount = null
                                            // if admin is clock out cashier can't be apply discount.
                                        }
                                        else -> {
                                            discountListViewModel?.selectedCartDiscount = null
                                            PromptMPinFaildForTaxExclude(getString(R.string.msg_admin_pin_require_discount))
                                        }
                                    }
                                }
                            } else {
                                //cart
                                calculateDiscountOnCart()
                            }
                        }

                    } /*else if (discountListViewModel?.selectedCartDiscount?.strAppliedTo.equals(CV.ITEM)) {
                        //on item
                        calculateDiscountOnItem()
                    }*/
                    dialogFragment.dismiss()
                } else {
                    discountListViewModel?.selectedCartDiscount = null
                    showToast(
                        mActivity,
                        getString(R.string.msg_discount_can_not_apply_on_empty_cart),
                        R.drawable.icon, custom_toast_container
                    )
                }
            }
        })
        dialogFragment.show(childFragmentManager, "dialog")
    }

    private fun insertDiscountDataIntoDB(discount_amount: Float) {
        discountListViewModel?.insertAppliedDiscountOnCart(order_id, discount_amount)
    }


    fun getDiscountAmountOnCart(displayToast: Boolean): Float {
        coupnDiscountAmount = 0.0f
        totalApplicableItemDiscount = 0.0
        var discount_amount = 0.0f
        var totalNonDiscountedProductAmount = 0.0
        var filteredNonDiscountOrderList =
            orderTransList.filter { item -> item.iDiscountId == 0 }

        if (filteredNonDiscountOrderList.isNotEmpty()) {
            discountListViewModel?.selectedCartDiscount?.let { selectedDiscount ->
                if (selectedDiscount.discountType.equals(AppConstant.COUPON_DISCOUNT)) {
                    var discountApplicableTotal = 0.0f
                    for (i in orderTransList.indices) {
                        val orderTransModel = orderTransList[i]
                        var isValidProduct: Boolean? = false
                        if (selected_history != null && selected_history!!.orderProducts != null && selected_history!!.status != AppConstant.ON_HOLD) {
                            var orderProducts = selected_history!!.orderProducts?.filter {
                                it.iProductId.toString() == orderTransModel.itemId.toString()
                            }
                            val discountAndProduct =
                                discountListViewModel?.checkProductCouponDetail(
                                    orderProducts?.get(0)?.iProductId!!.toLong(),
                                    selected_history!!.orderDiscounts?.get(0)?.iDiscountId!!.toLong()
                                )
                            if (discountAndProduct != null) {
                                isValidProduct =
                                    discountListViewModel?.checkDiscountMappingItemIsAvailable(
                                        orderProducts?.get(0)?.iProductId!!.toLong(),
                                        selected_history!!.orderDiscounts?.get(0)?.iDiscountId!!.toLong()
                                    )

                            } else {
                                isValidProduct = false
                            }
                        } else {
                            isValidProduct =
                                discountListViewModel?.checkDiscountMappingItemIsAvailable(
                                    orderTransModel.itemId.toLong(),
                                    discountListViewModel?.selectedCartDiscount?.discountId!!.toLong()
                                )
                        }
                        if (isValidProduct!!) {
                            if (orderTransModel.discountAmount == 0.0f) {
                                val amount =
                                    (orderTransModel.itemQty * orderTransModel.itemRate.toFloat())
                                discountApplicableTotal += amount
                                totalNonDiscountedProductAmount += orderTransModel.itemAmount
                            }
                        }
                    }
                    if (selectedDiscount.strType == CV.AMOUNT) {
                        //amount
                        discount_amount = selectedDiscount.dblValue
                    } else {
                        discount_amount =
                            (discountApplicableTotal * selectedDiscount.dblValue / 100)
                    }
                } else {
                    for (item in filteredNonDiscountOrderList) {
                        totalNonDiscountedProductAmount += item.itemAmount
                    }

                    if (selectedDiscount.strType == CV.AMOUNT) {
                        //amount
                        discount_amount = selectedDiscount.dblValue
                    } else {
                        var discountApplicableTotal = 0.0f
                        for (i in orderTransList.indices) {
                            val orderTransModel = orderTransList[i]
                            if (orderTransModel.iDiscountId == 0) {
                                val amount =
                                    (orderTransModel.itemQty * orderTransModel.itemRate.toFloat())
                                discountApplicableTotal += amount
                            }
                        }
                        discount_amount =
                            (discountApplicableTotal * selectedDiscount.dblValue / 100)
                    }
                }

            }
        }
        discountListViewModel?.selectedCartDiscount?.let { selectedDiscount ->
            if (selectedDiscount.discountType.equals(AppConstant.COUPON_DISCOUNT)) {
                if (totalNonDiscountedProductAmount == 0.0) {
                    if (displayToast) {
                        CM.showToast(
                            mActivity,
                            mActivity.getString(R.string.msg_non_discounted),
                            R.drawable.icon,
                            custom_toast_container
                        )
                    }
                    clearDiscountData()
                    discount_amount = 0.0f
                    coupnDiscountAmount = 0.0f
                    totalApplicableItemDiscount = 0.0
                    return discount_amount
                } else if (selectedDiscount.minCartAmount.toDouble() > totalNonDiscountedProductAmount) {
                    if (displayToast) {
                        CM.showToast(
                            mActivity, String.format(
                                mActivity.getString(R.string.msg_min_cart_value),
                                currency_symbol,
                                String.format(
                                    "%.2f",
                                    (selectedDiscount.minCartAmount.toDouble())
                                )
                            ), R.drawable.icon, custom_toast_container
                        )
                    }
                    clearDiscountData()
                    discount_amount = 0.0f
                    coupnDiscountAmount = 0.0f
                    totalApplicableItemDiscount = 0.0
                    return discount_amount
                }

            }else if (selectedDiscount.discountType.equals(AppConstant.SIMPLE_DISCOUNT))
            {
                if (totalNonDiscountedProductAmount == 0.0) {
                    if (displayToast) {
                        CM.showToast(
                                mActivity,
                                mActivity.getString(R.string.multiple_discount_restriction),
                                R.drawable.icon,
                                custom_toast_container
                        )
                    }
                    clearDiscountData()
                    discount_amount = 0.0f
                    coupnDiscountAmount = 0.0f
                    totalApplicableItemDiscount = 0.0
                    return discount_amount
                }
            }
        }
        if (discount_amount > totalNonDiscountedProductAmount) {
            if (displayToast) {
                CM.showToast(
                    mActivity,
                    mActivity.getString(R.string.msg_discount_can_not_higher_than_total),
                    R.drawable.icon, custom_toast_container
                )
            }
            clearDiscountData()
            discount_amount = 0.0f
            coupnDiscountAmount = 0.0f
            totalApplicableItemDiscount = 0.0
            return discount_amount
        } else {
            totalApplicableItemDiscount = totalNonDiscountedProductAmount
            coupnDiscountAmount = discount_amount
        }


        return discount_amount
    }

    private fun calculateDiscountOnCart() {
        calculateTotalAfterDiscountAppliedOnCart(getDiscountAmountOnCart(true))
    }

    private fun calculateTotalAfterDiscountAppliedOnCart(discount_amount: Float) {
        var total: Double = 0.0
        var final_tax_anount: Double = 0.0

        this.calculateCartProcedure(orderTransList, true) { cart: CartSummaryData ->
            total = cart.totalAmount + cart.taxAmount
            final_tax_anount = cart.taxAmount
        }
        if (discount_amount <= 0) {
            clearDiscountData()
        } else {


            mBinding.tvTax.text = getFormatedAmountWithCurrencySymbol(final_tax_anount)
            mBinding.tvDiscountValue.text =
                getFormatedAmountWithCurrencySymbol(discount_amount.toDouble())
            mBinding.tvTotalValue.text =
                getFormatedAmountWithCurrencySymbol(total)
            mBinding.tvTotalSavingsValue.text =
                getFormatedAmountWithCurrencySymbol(getTotalSavingAmount(discount_amount))
            cbDiscountApplied.visibility = View.VISIBLE
            cbDiscountApplied.isChecked = true
            mBinding.ivAddDiscount.visibility = View.GONE
            if (discountListViewModel?.selectedCartDiscount?.discountType == AppConstant.COUPON_DISCOUNT) {
                discount_name = discountListViewModel?.selectedCartDiscount?.couponCode!!
                cbDiscountApplied.text =
                    discountListViewModel?.selectedCartDiscount?.couponCode + CV.APPLIED_SUCCESSFULLY
            } else {
                discount_name = discountListViewModel?.selectedCartDiscount?.name!!
                cbDiscountApplied.text =
                    discountListViewModel?.selectedCartDiscount?.name + CV.APPLIED_SUCCESSFULLY
            }
            insertDiscountDataIntoDB(discount_amount)
        }
        //Hardware Code For CDS Display -- NewSale While any discount applied
        DisplayNewSaleDataToCDS(orderTransList, "", false)

    }


    private fun setPaymentStatusObserver() {
        paymentViewModel.getPaymentStatusObserver.observe(this, androidx.lifecycle.Observer {

            when (it) {
                AppConstant.PaymentProcess.NONE.value -> {

                }
                AppConstant.PaymentProcess.LOADING.value -> {
                    paymentViewModel.getPaymentStatusObserver.value?.toInt()?.let { it1 ->
                        updateDialogMsg(
                            getString(R.string.Please_wait),
                            it1
                        ) {}
                    }
                }
                AppConstant.PaymentProcess.SWIPE.value -> {
                    paymentViewModel.getPaymentStatusObserver.value?.toInt()?.let { it1 ->
                        updateDialogMsg(
                            getString(R.string.prompt_msg_waiting_for_card_selection),
                            it1
                        ) {}
                    }


                }
                AppConstant.PaymentProcess.AUTHENTICATION.value -> {
                    paymentViewModel.getPaymentStatusObserver.value?.toInt()?.let { it1 ->
                        updateDialogMsg(
                            getString(R.string.prompt_msg_insert_card),
                            it1
                        ) {}
                    }
                }
                AppConstant.PaymentProcess.SUCCESS.value -> {
                    paymentViewModel.getPaymentStatusObserver.value?.toInt()?.let { it1 ->
                        updateDialogMsg(
                            getString(R.string.title_transaction),
                            it1
                        ) {
                            dismissOpenNewCheckOutDialog()
                            printReceipt()
                        }
                    }
                }
                AppConstant.PaymentProcess.FAILED.value -> {
                    paymentViewModel.getPaymentStatusObserver.value?.toInt()?.let { it1 ->
                        /*updateDialogMsg(
                            getString(R.string.title_transaction),
                            it1
                        ) {}*/
                        dismissKcsDialog()
                        if (retry_count==2)
                        {
                            DialogUtility.openSingleActionDialog(
                                getString(R.string.server_busy_message),
                                "Close",
                                mActivity
                            ) {
                                if (it) {
                                    dismissOpenNewCheckOutDialog()
                                    doResetCart()
                                }
                            }
                        }else {
                            DialogUtility.openSingleActionDialog(
                                getString(R.string.internet_or_server_msg),
                                "Retry",
                                mActivity
                            ) {
                                if (it) {
                                    retry_count=retry_count+1
                                    webCallUpdateorderpayment()
                                }
                            }
                            if (retry_count==1)
                            {
                                showKcsDialog()
                                val timer = object: CountDownTimer(2000, 1000) {
                                    override fun onTick(millisUntilFinished: Long) {
                                        Log.d("timedisplay",millisUntilFinished.toString())

                                    }
                                    override fun onFinish() {
                                        dismissKcsDialog()
                                    }
                                }
                                timer.start()

                            }
                        }

                    }
                }
                AppConstant.PaymentProcess.SUCCESS_REFUND.value -> {
                    paymentViewModel.getPaymentStatusObserver.value?.toInt()?.let { it1 ->
                        updateDialogMsg(
                            getString(R.string.title_refudn_transaction),
                            it1
                        ) {}
                    }
                }
                AppConstant.PaymentProcess.FAILED_REFUND.value -> {
                    paymentViewModel.getPaymentStatusObserver.value?.toInt()?.let { it1 ->
                        paymentViewModel.msgCancel?.let { it2 ->
                            updateDialogMsg(
                                getString(R.string.title_refudn_transaction),
                                it1, it2
                            ) {}
                        }
                    }
                }
                AppConstant.PaymentProcess.CANCEL.value -> {
                    paymentViewModel.getPaymentStatusObserver.value?.toInt()?.let { it1 ->
                        paymentViewModel.msgCancel?.let { errorMSg ->
                            updateDialogMsg(
                                errorMSg,
                                it1
                            ) {
                                //CDS PaymentProcess Screen Invoke
                                DisplayNewSaleDataProcessToCDS(
                                    (mBinding.rvCart.adapter as OrderItemListAdapter).getDataset(),
                                    "",
                                    false,
                                    currency_symbol+" 0.00")
                                /*var baseActivityPrint = (activity as ParentActivity)
                                baseActivityPrint.checkDrawOverlayPermission(
                                    AppConstant.CDS_PAYMENTPROCESS_SCREEN,
                                    "",
                                    false,
                                    "",
                                    CDSSalesModel(),
                                    "",
                                    false
                                )*/
                            }
                            DisplayNewSaleDataToCDS(orderTransList, errorMSg, true)
                        }
                    }
                }


                AppConstant.PaymentProcess.INVALID_CHOICE.value -> {
                    paymentViewModel.getPaymentStatusObserver.value?.toInt()?.let { it1 ->
                        updateDialogMsg(
                            getString(R.string.err_invalid_choice),
                            it1
                        ) {
                            readInput("")
                        }

                    }
                }
            }
        })
    }

    private fun getPaymentProgressMsg(): String {
        val paidAmt = paymentViewModel.payableAmount.toDouble() / 100
        val paidAmount = String.format(
            getString(R.string.title_split_payment),
            currency_symbol,
            String.format("%.2f", (paidAmt))
        )
        var splitText: String = paidAmount
        if (paidAmt >= mBinding.tvTotalValue.text.toString().split(" ")[1].toDouble()) {
            splitText = paidAmount
        } else {
            splitText = "Split - ${paymentList.size + 1} $paidAmount"
        }
        return splitText
    }

    private fun getTotalCartMsg(): String {
        var totalPayableAmount = 0.0
        if (paymentList.isNotEmpty()) {
            totalPayableAmount = paymentList.map { it.strPaymentAmount.toDouble() }.sum()
        }
        return String.format(
            getString(R.string.title_payment),
            currency_symbol,
            String.format("%.2f", (totalPayableAmount))
        )
    }

    private fun getRefundMsg(mTotalRefundAmount: Double): String {
//        val paidAmt = paymentViewModel.payableAmount.toDouble()
        return String.format(
            getString(R.string.title_payment_refund),
            currency_symbol,
            String.format("%.2f", (mTotalRefundAmount))
        )
    }

    private fun updateProgressDialogTitle(titleMsg: String) {
        updatePaymentDialogTitle(titleMsg)
    }


    private fun GetModifierIngredient(mProductModifier: List<ProductModifier>): String {
        val sbFullString = StringBuilder()

        val addModifierArr = ArrayList<ProductModifier>()
        val subModifierArr = ArrayList<ProductModifier>()
        val noModifierArr = ArrayList<ProductModifier>()
        val liteModifierArr = ArrayList<ProductModifier>()
        val extraModifierArr = ArrayList<ProductModifier>()
        val onsideModifierArr = ArrayList<ProductModifier>()
        val defaultModifierArr = ArrayList<ProductModifier>()

        for (j in mProductModifier.indices) {

            if (mProductModifier.get(j).strPreModifierName.equals(CV.ADD)) {

                addModifierArr.add(mProductModifier.get(j))

            } else if (mProductModifier.get(j).strPreModifierName.equals(CV.SUB)) {

                subModifierArr.add(mProductModifier.get(j))

            } else if (mProductModifier.get(j).strPreModifierName.equals(CV.NO)) {

                noModifierArr.add(mProductModifier.get(j))

            } else if (mProductModifier.get(j).strPreModifierName.equals(CV.LITE)) {

                liteModifierArr.add(mProductModifier.get(j))

            } else if (mProductModifier.get(j).strPreModifierName.equals(CV.EXTRA)) {

                extraModifierArr.add(mProductModifier.get(j))

            } else if (mProductModifier.get(j).strPreModifierName.equals(CV.ON_SIDE)) {

                onsideModifierArr.add(mProductModifier.get(j))

            } else if (mProductModifier.get(j).strPreModifierName.equals(CV.DEFAULT)) {

                defaultModifierArr.add(mProductModifier.get(j))

            }
        }



        if (liteModifierArr.size > 0) {
            var delimeter = ""
            val sbIngredient = StringBuilder()
            sbFullString.append("${CV.LITE} : ")

            for (item in liteModifierArr) {
                sbIngredient.append(delimeter)
                sbIngredient.append(item.strOptionName)
                delimeter = ","
            }
            sbFullString.append(sbIngredient.trim())
            sbFullString.append(System.lineSeparator())
        }

        if (noModifierArr.size > 0) {
            var delimeter = ""
            val sbIngredient = StringBuilder()
            sbFullString.append("${CV.NO} : ")

            for (item in noModifierArr) {
                sbIngredient.append(delimeter)
                sbIngredient.append(item.strOptionName)
                delimeter = ","
            }
            sbFullString.append(sbIngredient.trim())
            sbFullString.append(System.lineSeparator())
        }

        if (addModifierArr.size > 0) {
            var delimeter = ""
            val sbIngredient = StringBuilder()
            sbFullString.append("${CV.ADD} : ")

            for (item in addModifierArr) {
                sbIngredient.append(delimeter)
                sbIngredient.append(item.strOptionName)
                delimeter = ","
            }
            sbFullString.append(sbIngredient.trim())
            sbFullString.append(System.lineSeparator())

        }
        if (subModifierArr.size > 0) {
            var delimeter = ""
            val sbIngredient = StringBuilder()
            sbFullString.append("${CV.SUB} : ")

            for (item in subModifierArr) {
                sbIngredient.append(delimeter)
                sbIngredient.append(item.strOptionName)
                delimeter = ","
            }
            sbFullString.append(sbIngredient.trim())
            sbFullString.append(System.lineSeparator())
        }


        if (extraModifierArr.size > 0) {
            var delimeter = ""
            val sbIngredient = StringBuilder()
            sbFullString.append("${CV.EXTRA} : ")

            for (item in extraModifierArr) {
                sbIngredient.append(delimeter)
                sbIngredient.append(item.strOptionName)
                delimeter = ","
            }
            sbFullString.append(sbIngredient.trim())
            sbFullString.append(System.lineSeparator())
        }
        if (onsideModifierArr.size > 0) {
            var delimeter = ""
            val sbIngredient = StringBuilder()
            sbFullString.append("${CV.ON_SIDE} : ")
            for (item in onsideModifierArr) {
                sbIngredient.append(delimeter)
                sbIngredient.append(item.strOptionName)
                delimeter = ","
            }
            sbFullString.append(sbIngredient.trim())
            sbFullString.append(System.lineSeparator())
        }
        if (defaultModifierArr.size > 0) {
            var delimeter = ""
            val sbIngredient = StringBuilder()
            sbFullString.append("${CV.DEFAULT} : ")
            for (item in defaultModifierArr) {
                sbIngredient.append(delimeter)
                sbIngredient.append(item.strOptionName)
                delimeter = ","
            }
            sbFullString.append(sbIngredient.trim())
            sbFullString.append(System.lineSeparator())
        }

        return sbFullString.toString().trim()
    }

    private fun GetModifierIngredients(mModifierMap: HashMap<String, List<IngredientMasterModel>>): String {

        var sbFullString = StringBuilder()

        val addModifierArr = HashMap<String, List<IngredientMasterModel>>()
        val subModifierArr = HashMap<String, List<IngredientMasterModel>>()
        val noModifierArr = HashMap<String, List<IngredientMasterModel>>()
        val liteModifierArr = HashMap<String, List<IngredientMasterModel>>()
        val extraModifierArr = HashMap<String, List<IngredientMasterModel>>()
        val onsideModifierArr = HashMap<String, List<IngredientMasterModel>>()
        val defaultModifierArr = HashMap<String, List<IngredientMasterModel>>()

        var tempValueList: List<IngredientMasterModel>

        for ((k, v) in mModifierMap) {

            if (k.equals(CV.LITE)) {
                liteModifierArr.put(k, v)
                if (liteModifierArr.size > 0) {
                    var delimeter = ""
                    val sbIngredient = StringBuilder()
                    sbFullString.append("${CV.LITE} : ")

                    tempValueList = liteModifierArr.getValue(k)

                    for (i in tempValueList.indices) {

                        sbIngredient.append(delimeter)
                        sbIngredient.append(tempValueList.get(i).strOptionName)
                        delimeter = ","

                    }
                    sbFullString.append(sbIngredient.trim())
                    sbFullString.append(System.lineSeparator())

                }
            } else if (k.equals(CV.NO)) {
                noModifierArr.put(k, v)
                if (noModifierArr.size > 0) {
                    var delimeter = ""
                    val sbIngredient = StringBuilder()
                    sbFullString.append("${CV.NO} : ")

                    tempValueList = noModifierArr.getValue(k)

                    for (i in tempValueList.indices) {

                        sbIngredient.append(delimeter)
                        sbIngredient.append(tempValueList.get(i).strOptionName)
                        delimeter = ","

                    }
                    sbFullString.append(sbIngredient.trim())
                    sbFullString.append(System.lineSeparator())

                }
            } else if (k.equals(CV.ADD)) {
                addModifierArr.put(k, v)
                if (addModifierArr.size > 0) {
                    var delimeter = ""
                    val sbIngredient = StringBuilder()
                    sbFullString.append("${CV.ADD} : ")

                    tempValueList = addModifierArr.getValue(k)

                    for (i in tempValueList.indices) {

                        sbIngredient.append(delimeter)
                        sbIngredient.append(tempValueList.get(i).strOptionName)
                        delimeter = ","

                    }
                    sbFullString.append(sbIngredient.trim())
                    sbFullString.append(System.lineSeparator())

                }

            } else if (k.equals(CV.SUB)) {
                subModifierArr.put(k, v)

                if (subModifierArr.size > 0) {
                    var delimeter = ""
                    val sbIngredient = StringBuilder()
                    sbFullString.append("${CV.SUB} : ")

                    tempValueList = subModifierArr.getValue(k)

                    for (i in tempValueList.indices) {

                        sbIngredient.append(delimeter)
                        sbIngredient.append(
                            tempValueList.get(i).strOptionName + " Substitute With " + mDatabase.daoIngredientList()
                                .getdefaultIngredient(
                                    tempValueList.get(i).iModifierGroupId.toString(),
                                    tempValueList.get(i).iProductId
                                )
                        )
                        delimeter = ","

                    }
                    sbFullString.append(sbIngredient.trim())
                    sbFullString.append(System.lineSeparator())

                }
            } else if (k.equals(CV.EXTRA)) {
                extraModifierArr.put(k, v)
                if (extraModifierArr.size > 0) {
                    var delimeter = ""
                    val sbIngredient = StringBuilder()
                    sbFullString.append("${CV.EXTRA} : ")

                    tempValueList = extraModifierArr.getValue(k)

                    for (i in tempValueList.indices) {

                        sbIngredient.append(delimeter)
                        sbIngredient.append(tempValueList.get(i).strOptionName)
                        delimeter = ","

                    }
                    sbFullString.append(sbIngredient.trim())
                    sbFullString.append(System.lineSeparator())

                }
            } else if (k.equals(CV.ON_SIDE)) {
                onsideModifierArr.put(k, v)
                if (onsideModifierArr.size > 0) {
                    var delimeter = ""
                    val sbIngredient = StringBuilder()
                    sbFullString.append("${CV.ON_SIDE} : ")
                    tempValueList = onsideModifierArr.getValue(k)

                    for (i in tempValueList.indices) {

                        sbIngredient.append(delimeter)
                        sbIngredient.append(tempValueList.get(i).strOptionName)
                        delimeter = ","

                    }
                    sbFullString.append(sbIngredient.trim())

                    sbFullString.append(System.lineSeparator())

                }
            } else if (k.equals(CV.DEFAULT)) {
                defaultModifierArr.put(k, v)
                if (defaultModifierArr.size > 0) {
                    var delimeter = ""
                    val sbIngredient = StringBuilder()
                    sbFullString.append("${CV.DEFAULT} : ")
                    tempValueList = defaultModifierArr.getValue(k)

                    for (i in tempValueList.indices) {

                        sbIngredient.append(delimeter)
                        sbIngredient.append(tempValueList.get(i).strOptionName)
                        delimeter = ","

                    }
                    sbFullString.append(sbIngredient.trim())

                    sbFullString.append(System.lineSeparator())

                }
            }

        }
        return sbFullString.toString().trim()
    }


    override fun RefundItemClicked(
        action: String,
        position: Int,
        refundorderTranslist: MutableList<OrderTransModel>
    ) {
        if (action.equals("delete")) {

            if (mBinding.tvDiscountValue.text.toString().split(" ")[1].toDouble() > 0) {

                if (refundorderTranslist.get(position).discountAmount > 0) {
                    mRefundSelectedAdapter?.run {
                        dialogRefund?.let { dialogRefund ->
                            removeItemFromSet(position)
                            mRefundAdapter.run {
                                updateData(orderTransList)
                            }
                        }
                    }
                    //Hardware Code For CDS Display -- Refund Selected item Deleted from refund order list case invoke
                    DisplayRefundDataToCDS(
                        AppConstant.CDS_REFUND_SCREEN,
                        refundorderTranslist,
                        ""
                    )
                    ShowCartLevelDiscount(refundorderTranslist as ArrayList<OrderTransModel>)
                } else {

                    orderAllCartDiscountItems.clear()
                    try {
                        if (refundorderTranslist.get(position).discountAmount > 0) {
//                                orderAllSaleDiscountItems.add(orderTransListSelected.get(i))
                        } else {
                            orderAllCartDiscountItems.add(refundorderTranslist.get(position))
                        }

                        orderTransListSelected.removeAll(orderAllCartDiscountItems)
                        mRefundAdapter.run {
                            updateData(orderTransList)
                        }
                    } catch (e: Exception) {
                        Log.e("DEBUG", "Exception -- " + e.toString())
                    }

                    mRefundSelectedAdapter.notifyDataSetChanged()

                    ShowCartLevelDiscount(refundorderTranslist as ArrayList<OrderTransModel>)

                    //Hardware Code For CDS Display -- Refund Selected item Deleted from refund order list case invoke
                    DisplayRefundDataToCDS(
                        AppConstant.CDS_REFUND_SCREEN,
                        refundorderTranslist,
                        ""
                    )


                }

            } else {
                mRefundSelectedAdapter?.run {
                    removeItemFromSet(position)
                    mRefundAdapter.run {
                        updateData(orderTransList)
                    }
                }
                //Hardware Code For CDS Display -- Refund Selected item Deleted from refund order list case invoke
                DisplayRefundDataToCDS(AppConstant.CDS_REFUND_SCREEN, refundorderTranslist, "")
                ShowCartLevelDiscount(refundorderTranslist as ArrayList<OrderTransModel>)
            }

        } else {
            //Hardware Code For CDS Display -- Refund Selected item Deleted from refund order list case invoke
            DisplayRefundDataToCDS(AppConstant.CDS_REFUND_SCREEN, refundorderTranslist, "")
            ShowCartLevelDiscount(refundorderTranslist as ArrayList<OrderTransModel>)
        }


    }

    //When Modifire popup open then restrict to another product selection
    private fun isModifirePopupClose(): Boolean {
        if (mBinding.layoutModifier.visibility == View.VISIBLE) {
            showToast(
                mActivity,
                getString(R.string.kindly_close_modifire_popup),
                R.drawable.icon, custom_toast_container
            )
            return false
        }
        return true
    }

    override fun onLoadMore() {
        loading = true
        if (CM.isInternetAvailable(mActivity)) {
            loadMorePage()
        } else {
            CM.showMessageOK(
                mActivity,
                "",
                resources.getString(R.string.msg_network_error),
                null
            )
        }
    }

    fun loadFirstPage(
        start_date: String,
        end_date: String,
        type: String,
        rvOrder: RecyclerView,
        tvNodataFound: TextView,
        search: String,
        strCheckStatus: String
    ) {
        loading = true
        currentPageNo++
        start_date_order_history = start_date
        end_date_order_history = end_date
        showKcsDialog()
        orderHistoryListViewModel!!.getOrderHistory(
            mDatabase,
            start_date,
            end_date,
            type,
            currentPageNo,
            search,
            strCheckStatus
        )
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                    rvOrder.visibility = View.GONE
                    tvNodataFound.visibility = View.VISIBLE
                } else {
                    totalPages = loginResponseModel.data!!.total_pages
                    if (currentPageNo == totalPages) {
                        hasNextPage = false
                    } else {
                        hasNextPage = true
                    }
                    if (hasNextPage) {
                        setupPagination(rvOrder)
                    } else {
                        setupWithoutPagination(rvOrder)
                    }

                    orderHistoryList.clear()
                    rvOrder.visibility = View.VISIBLE
                    tvNodataFound.visibility = View.GONE
                    orderHistoryList.addAll(loginResponseModel.data!!.data)
                    orderListFilter = orderHistoryList
                    rvOrder.visibility = View.VISIBLE
                    tvNodataFound.visibility = View.GONE
                    orderHistoryListAdapter =
                        OrderHistoryListAdapter(mActivity, orderHistoryList, this)
                    rvOrder.adapter = orderHistoryListAdapter

                }
            })
    }

    private fun setupPagination(rvOrder: RecyclerView) {
        orderHistoryListAdapter =
            OrderHistoryListAdapter(mActivity, orderHistoryList, this)
        loading = false

        orderHistoryListAdapter.removeAllChildViews(rvOrder)
        orderHistoryListAdapter.removeAllChildViews(rvOrder)
        rvOrder.setAdapter(orderHistoryListAdapter)
        paginate = Paginate.with(rvOrder, this)
            .setLoadingTriggerThreshold(2)
            .addLoadingListItem(true)
            .build()

    }


    private fun setupWithoutPagination(rvOrder: RecyclerView) {
        orderHistoryListAdapter =
            OrderHistoryListAdapter(mActivity, orderHistoryList, this)
        loading = false

        orderHistoryListAdapter.removeAllChildViews(rvOrder)
        rvOrder.setAdapter(orderHistoryListAdapter)
        paginate = Paginate.with(rvOrder, this)
            .setLoadingTriggerThreshold(2)
            .addLoadingListItem(false)
            .build()

    }

    fun loadMorePage() {
        loading = true
        currentPageNo++

        showKcsDialog()
        orderHistoryListViewModel!!.getOrderHistory(
            mDatabase,
            start_date_order_history,
            end_date_order_history,
            history_type,
            currentPageNo,
            search,
            history_status
        )
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                loading = false
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                } else {
                    totalPages = loginResponseModel.data!!.total_pages
                    Log.e(
                        "==== currentPageNo ==" + currentPageNo,
                        "=== loadMorePage totalPages ===" + totalPages
                    )
                    if (currentPageNo == totalPages) {
                        hasNextPage = false
                    } else {
                        hasNextPage = true
                    }
                    orderHistoryList.addAll(loginResponseModel.data!!.data)
                    if (orderHistoryListAdapter != null) {
                        orderHistoryListAdapter.notifyDataSetChanged()
                    }
                }
            })
    }

    override fun isLoading(): Boolean {
        return loading
    }

    override fun hasLoadedAllItems(): Boolean {
        return currentPageNo >= totalPages
    }


    private fun resetExistingEmployee() {
        emp_id = CM.getSp(mActivity, CV.EMP_ID, 0) as Int
        AppConstant.EMP_ID = emp_id
        emp_name = CM.getSp(mActivity, CV.EMP_NAME, "") as String
        emp_role = CM.getSp(mActivity, CV.EMP_ROLE, "") as String
    }


    fun getTaxAmountAppliedDiscount(orderTransModel: OrderTransModel): Double {

        var taxAmount = 0.0
        var discountAmount = 0.0
        var txtRateInPercentage = 0.0
        if (!TextUtils.isEmpty(orderTransModel.taxPercentage)) {
            txtRateInPercentage = orderTransModel.taxPercentage.toDouble()
        }
        discountListViewModel?.selectedCartDiscount?.let { selectedDiscount ->
            if (selectedDiscount.discountType.equals(AppConstant.COUPON_DISCOUNT)) {
                var isValidProduct: Boolean? = false
                if (selected_history != null && selected_history!!.orderProducts != null) {

                    var orderProducts: List<OrderProductsModel>?
                    if (isRefundDialogOpen) {
                        orderProducts = selected_history!!.orderProducts?.filter {
                            it.iCheckItemDetailId == orderTransModel.itemId.toString()
                        }
                    } else {
                        orderProducts = selected_history!!.orderProducts?.filter {
                            it.iProductId.toString() == orderTransModel.itemId.toString()
                        }
                        if (orderProducts.isNullOrEmpty()) {
                            orderProducts = selected_history!!.orderProducts?.filter {
                                it.iCheckItemDetailId == orderTransModel.itemId.toString()
                            }
                        }

                    }

                    val discountAndProduct =
                        discountListViewModel?.checkProductCouponDetail(
                            orderProducts?.get(0)?.iProductId!!.toLong(),
                            selected_history!!.orderDiscounts?.get(0)?.iDiscountId!!.toLong()
                        )
                    if (discountAndProduct != null) {
                        isValidProduct =
                            discountListViewModel?.checkDiscountMappingItemIsAvailable(
                                orderProducts?.get(0)?.iProductId!!.toLong(),
                                selected_history!!.orderDiscounts?.get(0)?.iDiscountId!!.toLong()
                            )
                    } else {
                        isValidProduct = false
                    }
                } else {
                    isValidProduct =
                        discountListViewModel?.checkDiscountMappingItemIsAvailable(
                            orderTransModel.itemId.toLong(),
                            discountListViewModel?.selectedCartDiscount?.discountId!!.toLong()
                        )
                }

                if (isValidProduct!!) {
                    if (orderTransModel.discountAmount == 0.0f) {
                        val amountCopoun = (orderTransModel.itemQty * orderTransModel.itemRate)
                        val itemWeightAge = (amountCopoun * 100) / totalApplicableItemDiscount
                        discountAmount =
                            ((coupnDiscountAmount * itemWeightAge) / 100).toDouble()
                    } else {
                        discountAmount = 0.0
                    }
                } else {
                    discountAmount = 0.0
                }
            } else {
                val amountCopoun = (orderTransModel.itemQty * orderTransModel.itemRate)
                val itemWeightAge = (amountCopoun * 100) / totalApplicableItemDiscount
                discountAmount = ((coupnDiscountAmount * itemWeightAge) / 100).toDouble()
            }
        }


        val amount = (orderTransModel.itemQty * orderTransModel.itemRate) - discountAmount
        var taxrate = (txtRateInPercentage / 100)
        taxAmount = amount * taxrate
        taxAmount = String.format("%.2f", taxAmount).toDouble()
        return taxAmount
    }

    fun printReceipt() {
        var dilaogTitle = getString(R.string.print_receipt)
        val cdsSalesModel= CDSSalesModel()
        if (paymentRequestList.size == 1) {
            if (paymentRequestList[0].strPaymentMethod == AppConstant.CASH || NewSaleFragmentCommon.isOtherCashTrasaction(
                    paymentMethodList,
                    paymentRequestList[0].strPaymentMethod
                )
            ) {
                dilaogTitle = "Change Due $currency_symbol${
                    String.format(
                        "%.2f",
                        (paymentRequestList[0].dblGIvenChange.toDouble())
                    )
                } out of $currency_symbol${
                    String.format(
                        "%.2f",
                        (paymentRequestList[0].dblGIvenAmount.toDouble())
                    )
                }"
                cdsSalesModel.change_due=currency_symbol+String.format(
                    "%.2f",
                    (paymentRequestList[0].dblGIvenChange.toDouble())
                )
                cdsSalesModel.total_paid_amount=currency_symbol+String.format(
                    "%.2f",
                    (paymentRequestList[0].dblGIvenAmount.toDouble())
                )
            } else {
                dilaogTitle = "Charged to customer's card: $currency_symbol${
                    String.format(
                        "%.2f",
                        (paymentRequestList[0].dblGIvenAmount.toDouble())
                    )
                }"
                cdsSalesModel.change_due=currency_symbol+"0.0"
                cdsSalesModel.total_paid_amount=currency_symbol+String.format(
                    "%.2f",
                    (paymentRequestList[0].dblGIvenAmount.toDouble())
                )
            }
            DisplayNewSaleDataProcessToCDS(
                (mBinding.rvCart.adapter as OrderItemListAdapter).getDataset(),
                "",
                false,
                currency_symbol+String.format(
                    "%.2f",
                    (paymentRequestList[0].dblGIvenChange.toDouble())
                ))
        }

        DialogUtility.openReceiptDialog(
            dilaogTitle,
            customerListViewModel?.selectedCustomer?.strEmail!!,
            mActivity
        ) { isPrintSuccess, email ->
            if (!email.equals("") && CV.server_order_id != 0) {
                placeOrderViewModel?.sendOrderReceipt(email, CV.server_order_id)?.observe(
                    this,
                    androidx.lifecycle.Observer {
                    })
            }
            if (isPrintSuccess) {
                PrintSalesReceipt(
                    CV.ORDER_DATE,
                    InvoiceNumber,
                    String.format("%.2f", (CV.GIVEN_AMOUNT))
                )
                paymentViewModel.resetCardResponse()
            }

            (activity as ParentActivity).checkDrawOverlayPermission(
                AppConstant.CDS_PAYMENTDONE_SCREEN, "Payment Successful",
                isPrintSuccess,
                email,
                cdsSalesModel,
                InvoiceNumber,
                false
            )
            doResetCart()
        }
    }

    override fun onFragmentBarcodeResult(barcode: String) {
        if (!TextUtils.isEmpty(barcode)) {
            LogUtil.d("ProductStart_time",System.currentTimeMillis().toString())
            /* if (searchText == searchFor)
                 return*/

            // searchFor = searchText
            launch {
                delay(100)  //debounce timeOut
                /* if (searchText != searchFor)
                     return@launch*/
                if (!TextUtils.isEmpty(barcode)) {
                    if (isModifirePopupClose()) {
                        menuClose()
                        Log.d("BARCODE_SCAN", "barcode :: " + barcode)
                        var product = searchViewModel?.searchProductBarcode(barcode)
                        if (product != null) {
                            Log.d("BARCODE_SCAN", "Product :: " + product.strBarCode)
                            isCustomerEligible(product)
                            edtSearchBarcode.setText("")
                            edtSearchBarcode.requestFocus()
                            edtSearchBarcode.setRawInputType(InputType.TYPE_CLASS_TEXT)
                            edtSearchBarcode.isFocusable = true
                            searchViewModel?.resetProduct()
                            searchViewModel?.isSearching = false
                        } else {
                            searchViewModel?.isSearching = false
                            productNotFoundDialog {
                                edtSearchBarcode.setText("")
                                edtSearchBarcode.requestFocus()
                                edtSearchBarcode.setRawInputType(InputType.TYPE_CLASS_TEXT)
                                edtSearchBarcode.isFocusable = true
                                searchViewModel?.resetProduct()
                            }
                        }
                    } else {
                        mBinding.edtSearchBarcode.text!!.clear()
                    }
                }
                // do our magic here
            }
            edtSearchBarcode.setText(barcode)
        }
    }

    companion object {
        private const val TAG = "NewSalesFragment"
    }

}