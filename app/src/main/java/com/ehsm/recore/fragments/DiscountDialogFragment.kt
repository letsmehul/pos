package com.ehsm.recore.fragments


import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ehsm.recore.R
import com.ehsm.recore.adapter.DiscountAdapter
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.interfaces.ItemCallBack
import com.ehsm.recore.model.DiscountMaster
import com.ehsm.recore.repository.DiscountListRepository
import com.ehsm.recore.utils.*
import com.ehsm.recore.viewmodel.DiscountListViewModel
import com.ehsm.recore.viewmodelfactory.viewModelFactory

class DiscountDialogFragment() : DialogFragment() {

    private var discountListViewModel: DiscountListViewModel? = null

    lateinit var adapter: DiscountAdapter
    lateinit var rvCart: RecyclerView
    lateinit var ivCancel: ImageView
    lateinit var tvMessage: AppCompatTextView
    var callBackListener: ItemCallBack<DiscountMaster>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        dialog?.window?.setCallback(
                UserInteractionAwareCallback(
                        dialog?.window!!.getCallback(),
                        activity
                )
        )
        this.activity?.let {
            discountListViewModel = ViewModelProviders.of(requireParentFragment(),
                viewModelFactory {
                    DiscountListViewModel(DiscountListRepository(RecoreDB.getInstance(it)))
                }
            ).get(DiscountListViewModel::class.java)
        }

        discountListViewModel?.getDiscountList(CM.getSp(activity, CV.EMP_ID, 0) as Int)
        val view = inflater.inflate(R.layout.dialog_fragment_discount, container, false)
        rvCart = view.findViewById(R.id.rvCart)
        tvMessage = view.findViewById(R.id.tvMessage)
        ivCancel = view.findViewById(R.id.ivCancel)
        ivCancel.setOnClickListener {
            dismiss()
        }
        setObservers()
        return view
    }

    private fun setObservers() {
        var hashMap: HashMap<String, List<DiscountMaster>> = HashMap()
        discountListViewModel?.run {
            this.discountsList.observe(viewLifecycleOwner, Observer {

                val currentEmpId = (CM.getSp(activity, CV.EMP_ID, 0) as Int).toString()
                var simpleDiscount = it.filter { item ->
                    item.discountType == AppConstant.SIMPLE_DISCOUNT && item.employees!!.split(',').map { it.trim() }.contains(currentEmpId)
                }
                /*var simpleDiscount =
                    it.filter { item -> item.discountType == AppConstant.SIMPLE_DISCOUNT }*/

                if (simpleDiscount != null && simpleDiscount.isNotEmpty()) {
                    hashMap[AppConstant.SIMPLE_DISCOUNT] = simpleDiscount
                }

                val coupanDiscount = it.filter { item ->
                    item.discountType == AppConstant.COUPON_DISCOUNT
                }

                if (coupanDiscount != null && coupanDiscount.isNotEmpty()) {
                    hashMap[AppConstant.COUPON_DISCOUNT] = coupanDiscount
                }
                if (hashMap.size > 0) {
                    val hashMap = hashMap.toSortedMap()
                    adapter = DiscountAdapter(hashMap, callBackListener)
                    val llm = LinearLayoutManager(activity)

                    rvCart.setHasFixedSize(true)
                    rvCart.layoutManager = llm
                    rvCart.adapter = adapter
                } else {
                    showEmptyView()
                }
            })
        }
    }

    private fun showEmptyView() {
        tvMessage.visibility = View.VISIBLE
        rvCart.visibility = View.GONE
        tvMessage.text = "No Discount Found"
    }

    fun setListener(callBackListener: ItemCallBack<DiscountMaster>) {
        this.callBackListener = callBackListener
    }

}