//package com.ehsm.recore.fragments
//
//import android.app.Dialog
//import android.content.Intent
//import android.graphics.Color
//import android.graphics.drawable.ColorDrawable
//import android.os.Bundle
//import android.text.Editable
//import android.text.InputFilter
//import android.text.TextUtils
//import android.text.TextWatcher
//import android.util.Log
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import android.view.Window
//import android.view.animation.AnimationUtils
//import android.widget.*
//import androidx.appcompat.app.AppCompatActivity
//import androidx.appcompat.widget.AppCompatEditText
//import androidx.core.view.isVisible
//import androidx.databinding.DataBindingUtil
//import androidx.lifecycle.ViewModelProvider
//import androidx.lifecycle.ViewModelProviders
//import androidx.recyclerview.widget.LinearLayoutManager
//import androidx.recyclerview.widget.RecyclerView
//import com.bumptech.glide.Glide
//import com.ehsm.recore.R
//import com.ehsm.recore.activities.AddCustomerActivity
//import com.ehsm.recore.activities.CheckInOutActivity
//import com.ehsm.recore.activities.RefundActivity
//import com.ehsm.recore.adapter.*
//import com.ehsm.recore.database.RecoreDB
//import com.ehsm.recore.databinding.FragmentNewSalesBinding
//import com.ehsm.recore.interfaces.OnLoadMoreListener
//import com.ehsm.recore.model.*
//import com.ehsm.recore.utils.*
//import com.ehsm.recore.viewmodel.SearchViewModel
//import com.google.gson.Gson
//import com.kcspl.divyangapp.viewmodel.*
//import java.text.SimpleDateFormat
//import java.util.*
//import kotlin.collections.ArrayList
//
//
//class NewSalesFragment2 : BaseFragment(),
//    CategoryListAdapter.ItemClickCategory,
//    SubCategoryListAdapter.ItemClickSubCategory,
//    ItemListSearchAdapter.ListItemClickedItem,
//    OrderItemListAdapter.ItemClick,
//    OrderItemListAdapter.ItemClick1,
//    OrderItemListAdapter.ItemClick2,
//    OrderHistoryListAdapter.ListItemClickedOrder,
//    CustomerListAdapter.ListItemClickedCustomer {
//
//    private lateinit var mBinding: FragmentNewSalesBinding
//    private var mActivity = AppCompatActivity()
//    private lateinit var mDatabase: RecoreDB
//    lateinit var dialogSearchCustomer: Dialog
//    private lateinit var dialogorder: Dialog
//    private lateinit var selectedHistory: OrderHistoryModel
//    private var categoryList = ArrayList<CategoryModel>()
//    private var subcategoryList = ArrayList<SubCategoryModel>()
//    private var itemList = ArrayList<ItemModel>()
//    private var itemListFilter = ArrayList<ItemModel>()
//    private var itemRequestList: ArrayList<OrderTransRequestModel> = ArrayList()
//    private var customerList = ArrayList<CustomerModel>()
//    private var orderListFilter = ArrayList<OrderHistoryModel>()
//
//    var filterProduct: ArrayList<CustomerModel> = ArrayList()
//
//    var allDataModel: ArrayList<ItemModel> = ArrayList()
//
//    private var placeOrderViewModel: PlaceOrderViewModel? = null
//    private var viewModelChangeMPin: ChangeMPinViewModel? = null
//    private var customerListViewModel: CustomerListViewModel? = null
//    private var orderHistoryListViewModel: OrderHistoryViewModel? = null
//
//    private var customerListFilter = ArrayList<CustomerModel>()
//
//    private var selectedCustomerModel: CustomerModel? = null
//
//    private var order_id = ""
//    private var currency_symbol = ""
//    var strAmount: ArrayList<Int> = ArrayList()
//
//    private lateinit var dialogSearchItem: Dialog
//    private lateinit var ivRoundOneCredential: ImageView
//    private lateinit var ivRoundTwoCredential: ImageView
//    private lateinit var ivRoundThreeCredential: ImageView
//    private lateinit var ivRoundFourCredential: ImageView
//    private lateinit var llPinRound: LinearLayout
//    private lateinit var dialog: Dialog
//    private var count = 0
//    private var minusCount = false
//    private var pin = ""
//
//    private var viewModelVerifyMPin: VerifyMPinViewModel? = null
//    private var searchViewModel: SearchViewModel?=null
//
//    override fun initView(view: View, savedInstances: Bundle?) {
//        mActivity = (activity as AppCompatActivity?)!!
//        mDatabase = RecoreDB.getInstance(mActivity)!!
//        currency_symbol = CM.getSp(mActivity, CV.CURRENCY_CODE, "") as String
//        viewModelVerifyMPin = ViewModelProviders.of(this).get(VerifyMPinViewModel::class.java)
//        placeOrderViewModel = ViewModelProviders.of(this).get(PlaceOrderViewModel::class.java)
//        viewModelChangeMPin = ViewModelProviders.of(this).get(ChangeMPinViewModel::class.java)
//        customerListViewModel = ViewModelProviders.of(this).get(CustomerListViewModel::class.java)
//        orderHistoryListViewModel =
//            ViewModelProviders.of(this).get(OrderHistoryViewModel::class.java)
//
//        CM.removeStatusBar(mActivity)
//        getAllcategories()
//        getAllItem()
//        setUserData()
//        mBinding.reluser.setOnClickListener {
//            openChangePasscodeDialog()
//        }
//        mBinding.layoutHistory.setOnClickListener {
//            openSearchOrderHistoryDialog()
//        }
//        mBinding.btnRefund.setOnClickListener {
//            val i = Intent(mActivity, RefundActivity::class.java)
//            i.putExtra("selectedHistory", Gson().toJson(selectedHistory))
//            CM.startActivityWithIntent(mActivity, i)
//        }
//        mBinding.btnSave.setOnClickListener {
//            if (mBinding.tvTotalValue.text.toString().split(" ").get(1).toDouble().equals(0.0)) {
//                Toast.makeText(
//                    mActivity,
//                    "Please select atleast one item to hold the order.",
//                    Toast.LENGTH_SHORT
//                ).show()
//            } else {
//                // val holdOrderList = mDatabase.daoOrderTrans().getHoldOrderItem("On Hold")
//                val isHoldOrder = CM.getSp(mActivity, CV.IS_HOLD_ORDER, false) as Boolean
////holdOrderList != null && holdOrderList.size > 0
//                if (isHoldOrder) {
//                    Toast.makeText(mActivity, "Hold order already exists.", Toast.LENGTH_SHORT)
//                        .show()
//                    mDatabase.daoOrderTrans().deleteOrder(order_id)
//
//                    resetOrder()
//                } else {
//                    val orderTransLiveData = mDatabase.daoOrderTrans().getOrderTrans(order_id)
//                    for (i in orderTransLiveData.indices) {
//                        mDatabase.daoOrderTrans().updateHoldOrder(
//                            "On Hold",
//                            orderTransLiveData.get(i).orderId,
//                            orderTransLiveData.get(i).itemId
//                        )
//                    }
//                    CM.setSp(mActivity, CV.IS_HOLD_ORDER, true)
//
//                    order_id = ""
//                    resetOrder()
//
//                }
//            }
//
//        }
//        mBinding.btnCancel.setOnClickListener {
//            mDatabase.daoOrderTrans().deleteOrder(order_id)
//            resetOrder()
//        }
//        mBinding.btnExchange.setOnClickListener {
//            order_id = ""
//            resetOrder()
//            mBinding.btnRefund.visibility = View.GONE
//            mBinding.btnExchange.visibility = View.GONE
//            mBinding.btnCancel.visibility = View.VISIBLE
//            mBinding.btnSave.visibility = View.VISIBLE
//            mBinding.btnCheckout.visibility = View.VISIBLE
//        }
//        mBinding.gridItem.setOnItemClickListener({ parent, view, position, id ->
//            if (!mBinding.btnRefund.isVisible) {
//                if (selectedCustomerModel == null) {
//                    Toast.makeText(mActivity, "Please select customer first", Toast.LENGTH_SHORT)
//                        .show()
//                } else {
//                    itemAmountCalculation(itemList.get(position))
//                }
//            }
//        })
//        mBinding.tvScan.setOnClickListener {
//            if (!mBinding.btnRefund.isVisible) {
//                openSearchItemDialog()
//            }
//        }
//
//        mBinding.ivAddCustomer.setOnClickListener {
//            openSearchCustomerDialog()
//        }
//
//        mBinding.ivAdd.setOnClickListener {
//            startActivityForResult(Intent(mActivity, AddCustomerActivity::class.java), 1)
//        }
//
//        mBinding.btnCheckout.setOnClickListener {
//            // val orderTransList = mDatabase.daoOrderTrans().getOrderTrans(order_id)
//            val adapter: OrderItemListAdapter = mBinding.rvCart.adapter as OrderItemListAdapter
//            val orderTransList = adapter.getDataset()
//            Log.e("=== ### size ===##", "=====" + orderTransList.size)
//            if (orderTransList.size > 0) {
//                val total = mBinding.tvTotalValue.text.toString().split(" ").get(1).toDouble()
//                var firstValue = round(total.toInt())
//
//                strAmount.clear()
//                firstValue = firstValue + 5
//                strAmount.add(firstValue)
//                strAmount.add(firstValue + 5)
//                strAmount.add(firstValue + 10)
//
//                openCheckOutDialog()
//            } else {
//                Toast.makeText(
//                    mActivity,
//                    "Please select at least one item to create the order.",
//                    Toast.LENGTH_SHORT
//                ).show()
//            }
//        }
//        webCallCustomerList("")
//    }
//    private fun getAllItem() {
//        allDataModel = mDatabase.daoItemList().getAllItems() as ArrayList<ItemModel>
//    }
//    private fun openSearchOrderHistoryDialog() {
//        dialogorder = Dialog(mActivity)
//        dialogorder.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialogorder.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//
//        dialogorder.setContentView(R.layout.dialog_search_order)
//
//        dialogorder.setCancelable(true)
//        dialogorder.show()
//
//        val btnOpen = dialogorder.findViewById<Button>(R.id.btnOpen)
//        val btnToday = dialogorder.findViewById<Button>(R.id.btnToday)
//        val btnAll = dialogorder.findViewById<Button>(R.id.btnAll)
//        val rvOrder = dialogorder.findViewById<RecyclerView>(R.id.rvOrder)
//        val tvNodataFound = dialogorder.findViewById<TextView>(R.id.tvNodataFound)
//        val edtSearchOrder = dialogorder.findViewById<EditText>(R.id.edtSearchOrder)
//
//        val date = Calendar.getInstance().time
//        val sdf = SimpleDateFormat(CV.WS_DATE_FORMATTE)
//        webCallOderList(sdf.format(date), sdf.format(date), rvOrder, tvNodataFound)
//
//        edtSearchOrder.addTextChangedListener(object : TextWatcher {
//
//            override fun afterTextChanged(s: Editable) {
//                filterOrderHistory(s.toString(), rvOrder)
//            }
//
//            override fun beforeTextChanged(
//                s: CharSequence, start: Int,
//                count: Int, after: Int
//            ) {
//            }
//
//            override fun onTextChanged(
//                s: CharSequence, start: Int,
//                before: Int, count: Int
//            ) {
//            }
//        })
//
//        btnAll.setOnClickListener {
//            btnAll.background = resources.getDrawable(R.drawable.btn_solid)
//            btnAll.setTextColor(resources.getColor(R.color.colorWhite))
//
//            btnOpen.background = resources.getDrawable(R.drawable.btn_stroke)
//            btnOpen.setTextColor(resources.getColor(R.color.colorPrimary))
//
//            btnToday.background = resources.getDrawable(R.drawable.btn_stroke)
//            btnToday.setTextColor(resources.getColor(R.color.colorPrimary))
//
//            webCallOderList("", "", rvOrder, tvNodataFound)
//
//        }
//
//        btnOpen.setOnClickListener {
//
//            btnOpen.background = resources.getDrawable(R.drawable.btn_solid)
//            btnOpen.setTextColor(resources.getColor(R.color.colorWhite))
//
//            btnToday.background = resources.getDrawable(R.drawable.btn_stroke)
//            btnToday.setTextColor(resources.getColor(R.color.colorPrimary))
//
//            btnAll.background = resources.getDrawable(R.drawable.btn_stroke)
//            btnAll.setTextColor(resources.getColor(R.color.colorPrimary))
//
//            if (getHoldOrderData().size > 0) {
//                rvOrder.visibility = View.VISIBLE
//                tvNodataFound.visibility = View.GONE
//                orderListFilter = getHoldOrderData()
//                rvOrder.adapter = OrderHistoryListAdapter(mActivity, getHoldOrderData(), this)
//            } else {
//                rvOrder.visibility = View.GONE
//                tvNodataFound.visibility = View.VISIBLE
//            }
//
//        }
//
//        btnToday.setOnClickListener {
//
//            btnToday.background = resources.getDrawable(R.drawable.btn_solid)
//            btnToday.setTextColor(resources.getColor(R.color.colorWhite))
//
//            btnOpen.background = resources.getDrawable(R.drawable.btn_stroke)
//            btnOpen.setTextColor(resources.getColor(R.color.colorPrimary))
//
//            btnAll.background = resources.getDrawable(R.drawable.btn_stroke)
//            btnAll.setTextColor(resources.getColor(R.color.colorPrimary))
//
//            if (!CM.isInternetAvailable(mActivity)) {
//            } else {
//                webCallOderList(sdf.format(date), sdf.format(date), rvOrder, tvNodataFound)
//            }
//        }
//    }
//
//    fun filterOrderHistory(text: String, rvOrder: RecyclerView) {
//        val filterProduct: java.util.ArrayList<OrderHistoryModel> =
//            java.util.ArrayList<OrderHistoryModel>()
//        filterProduct.clear()
//        for (s in orderListFilter) {
//            if (s.strFirstName.toLowerCase().contains(text.toLowerCase())
//                || s.strLastName.toLowerCase().contains(text.toLowerCase())
//                || s.strInvoiceNumber.toLowerCase().contains(text.toLowerCase())
//            ) {
//                filterProduct.add(s)
//            }
//        }
//        rvOrder.adapter = OrderHistoryListAdapter(mActivity, filterProduct, this)
//
//    }
//
//
//    private fun webCallOderList(
//        start_date: String,
//        end_date: String,
//        rvOrder: RecyclerView,
//        tvNodataFound: TextView
//    ) {
//
//        orderHistoryListViewModel!!.getOrderHistory(mDatabase, start_date, end_date)
//            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
//                if (loginResponseModel.data == null) {
//                    Toast.makeText(mActivity, loginResponseModel.message, Toast.LENGTH_LONG).show()
//                    if (getHoldOrderData().size > 0) {
//                        orderListFilter = getHoldOrderData()
//                        rvOrder.visibility = View.VISIBLE
//                        tvNodataFound.visibility = View.GONE
//                        rvOrder.adapter =
//                            OrderHistoryListAdapter(mActivity, getHoldOrderData(), this)
//                    } else {
//                        rvOrder.visibility = View.GONE
//                        tvNodataFound.visibility = View.VISIBLE
//                    }
//                } else {
//                    val all_hostory = ArrayList<OrderHistoryModel>()
//                    all_hostory.addAll(getHoldOrderData())
//                    all_hostory.addAll(loginResponseModel.data!!.data)
//                    orderListFilter = all_hostory
//                    rvOrder.visibility = View.VISIBLE
//                    tvNodataFound.visibility = View.GONE
//                    rvOrder.adapter = OrderHistoryListAdapter(mActivity, all_hostory, this)
//                }
//
//            })
//    }
//
//    private fun getHoldOrderData(): ArrayList<OrderHistoryModel> {
//        var orderHistoryList = ArrayList<OrderHistoryModel>()
//
//        val date = Calendar.getInstance().time
//        val sdf = SimpleDateFormat(CV.DISPLAY_DATE_FORMATTE)
//        val holdOrderList = mDatabase.daoOrderTrans().getHoldOrderItem("On Hold")
//        if (holdOrderList.size > 0) {
//            var total_tax = 0.0
//            var total = 0.0
//            var product_list = ArrayList<OrderProductsModel>()
//
//            for (i in holdOrderList.indices) {
//                total_tax = total_tax + holdOrderList.get(i).taxAmount.toDouble()
//                total = total + holdOrderList.get(i).itemAmount
//
//                val test = OrderProductsModel(
//                    "",
//                    holdOrderList.get(i).itemName,
//                    holdOrderList.get(i).itemQty.toString(),
//                    holdOrderList.get(i).itemRate.toString(),
//                    holdOrderList.get(i).itemAmount.toString(),
//                    holdOrderList.get(i).taxAmount,
//                    "",
//                    "",
//                    holdOrderList.get(i).itemId,"")
//                product_list.add(test)
//            }
//            val temp = OrderHistoryModel(
//                "",
//                "On Hold",
//                0,
//                sdf.format(date),
//                selectedCustomerModel!!.iCustomerId.toString(),
//                selectedCustomerModel!!.strFirstName,
//                selectedCustomerModel!!.strLastName,
//                total.toString(),
//                (total_tax + total).toString(),
//                total_tax.toString(),
//                holdOrderList.get(0).orderId,
//                product_list,
//                null,
//                null,
//                null
//            )
//            orderHistoryList.clear()
//            orderHistoryList.add(temp)
//        }
//
//
//        return orderHistoryList
//    }
//
//
//    private fun webCallCustomerList(search: String) {
//        customerListViewModel!!.getCustomer(mDatabase, search)
//            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
//                if (loginResponseModel.data == null) {
//                    Toast.makeText(mActivity, loginResponseModel.message, Toast.LENGTH_LONG).show()
//                } else {
//                    customerList = loginResponseModel.data!!.data
//                    customerListFilter = loginResponseModel.data!!.data
//
//                    for (i in customerList.indices) {
//                        if (customerList.get(i).iIsWalkIn.equals(1)) {
//                            selectedCustomerModel = customerList.get(i)
//                            mBinding.tvNewOrder.setText(selectedCustomerModel!!.strFirstName + " " + selectedCustomerModel!!.strLastName)
//                            break
//                        }
//                    }
//                }
//
//            })
//
//    }
//
//    fun filterCustomer(text: String, rvCustomer: RecyclerView) {
//        filterProduct.clear()
//        for (s in customerListFilter) {
//            if (s.strFirstName.toLowerCase().contains(text.toLowerCase())
//                || s.strLastName.toLowerCase().contains(text.toLowerCase())
//                || s.strPhone.toLowerCase().contains(text.toLowerCase())
//            ) {
//                filterProduct.add(s)
//            }
//        }
//        rvCustomer.adapter = CustomerListAdapter(mActivity, filterProduct, this)
//    }
//
//    private fun openSearchCustomerDialog() {
//        dialogSearchCustomer = Dialog(mActivity)
//        dialogSearchCustomer.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialogSearchCustomer.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//
//        dialogSearchCustomer.setContentView(R.layout.dialog_search_customer)
//
//        dialogSearchCustomer.setCancelable(true)
//        dialogSearchCustomer.show()
//
//        val rvCustomer = dialogSearchCustomer.findViewById<RecyclerView>(R.id.rvCustomer)
//        rvCustomer.adapter = CustomerListAdapter(mActivity, customerList, this)
//
//        val edtSearchCustomer =
//            dialogSearchCustomer.findViewById<AppCompatEditText>(R.id.edtSearchCustomer)
//        edtSearchCustomer.addTextChangedListener(object : TextWatcher {
//
//            override fun afterTextChanged(s: Editable) {
//                if (s.isEmpty()) {
//                    filterProduct.clear()
//                    filterCustomer(s.toString(), rvCustomer)
//                } else {
//                    filterCustomer(s.toString(), rvCustomer)
//                }
//            }
//
//            override fun beforeTextChanged(
//                s: CharSequence, start: Int,
//                count: Int, after: Int
//            ) {
//            }
//
//            override fun onTextChanged(
//                s: CharSequence, start: Int,
//                before: Int, count: Int
//            ) {
//            }
//        })
//
//    }
//
//    private fun openChangePasscodeDialog() {
//        val dialogchangempin = Dialog(mActivity)
//        dialogchangempin.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialogchangempin.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//
//        dialogchangempin.setContentView(R.layout.dialog_change_mpin)
//
//        dialogchangempin.setCancelable(false)
//        dialogchangempin.show()
//
//        val tvUserNameLogin = dialogchangempin.findViewById<TextView>(R.id.tvUserNameLogin)
//        tvUserNameLogin.setText("Logged in as " + CM.getSp(mActivity, CV.EMP_NAME, "") as String)
//
//        val edtCurrentMPin = dialogchangempin.findViewById<EditText>(R.id.edtCurrentMPin)
//        val edtNewMPin = dialogchangempin.findViewById<EditText>(R.id.edtNewMPin)
//        val edtReEnterMpin = dialogchangempin.findViewById<EditText>(R.id.edtReEnterMpin)
//
//        val btnFinish = dialogchangempin.findViewById<Button>(R.id.btnFinish)
//        btnFinish.setOnClickListener {
//            if (edtCurrentMPin.text.toString().equals("")) {
//                Toast.makeText(
//                    mActivity,
//                    getString(R.string.enter_current_mpin),
//                    Toast.LENGTH_SHORT
//                ).show()
//            } else if (edtNewMPin.text.toString().equals("")) {
//                Toast.makeText(mActivity, getString(R.string.enter_new_mpin), Toast.LENGTH_SHORT)
//                    .show()
//            } else if (edtReEnterMpin.text.toString().equals("")) {
//                Toast.makeText(
//                    mActivity,
//                    getString(R.string.enter_confirm_mpin),
//                    Toast.LENGTH_SHORT
//                ).show()
//            } else if (!edtNewMPin.text.toString().equals(edtReEnterMpin.text.toString())) {
//                Toast.makeText(mActivity, getString(R.string.mpin_not_match), Toast.LENGTH_SHORT)
//                    .show()
//            } else {
//                if (!CM.isInternetAvailable(mActivity)) {
//                    CM.showMessageOK(
//                        mActivity,
//                        "",
//                        resources.getString(R.string.msg_network_error),
//                        null
//                    )
//
//                } else {
//                    webCallChangeMpin(
//                        edtReEnterMpin.text.toString(),
//                        edtCurrentMPin.text.toString(),
//                        dialogchangempin
//                    )
//                }
//            }
//        }
//
//        val ivCancel = dialogchangempin.findViewById<ImageView>(R.id.ivCancel)
//        ivCancel.setOnClickListener {
//            dialogchangempin.dismiss()
//        }
//    }
//
//
//    private fun setUserData() {
//        mBinding.tvEmpName.setText(CM.getSp(mActivity, CV.EMP_NAME, "") as String)
//        Glide.with(mActivity)
//            .load(CM.getSp(mActivity, CV.EMP_IMAGE, "") as String)
//            .placeholder(R.drawable.icon)
//            .error(R.drawable.user)
//            .into(mBinding.ivUser)
//    }
//
//    private fun webCallChangeMpin(mpin: String, iMpin_old: String, dialogchangempin: Dialog) {
//        showKcsDialog()
//
//        viewModelChangeMPin!!.changeMpin(
//            CM.getSp(mActivity, CV.EMP_ID, 0) as Int,
//            mpin.toInt(),
//            iMpin_old.toInt()
//        )
//            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
//                dismissKcsDialog()
//                if (loginResponseModel.data == null) {
//                    Toast.makeText(mActivity, loginResponseModel.message, Toast.LENGTH_LONG).show()
//                } else {
//                    Toast.makeText(mActivity, loginResponseModel.data!!.message, Toast.LENGTH_LONG)
//                        .show()
//                    dialogchangempin.dismiss()
//                }
//
//            })
//
//
//    }
//
//    private fun openCheckOutDialog() {
//        val dialogcheckout = Dialog(mActivity)
//        dialogcheckout.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialogcheckout.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//
//        dialogcheckout.setContentView(R.layout.dialog_checkout)
//
//        dialogcheckout.setCancelable(false)
//        dialogcheckout.show()
//
//        val ivCancel = dialogcheckout.findViewById<ImageView>(R.id.ivCancel)
//        val btnTender = dialogcheckout.findViewById<Button>(R.id.btnTender)
//
//        val btnCash = dialogcheckout.findViewById<Button>(R.id.btnCash)
//        val llCash = dialogcheckout.findViewById<LinearLayout>(R.id.llCash)
//        val relCash = dialogcheckout.findViewById<RelativeLayout>(R.id.relCash)
//
//        val btnExternal = dialogcheckout.findViewById<Button>(R.id.btnExternal)
//        val llExternal = dialogcheckout.findViewById<LinearLayout>(R.id.llExternal)
//        val llExternal2 = dialogcheckout.findViewById<LinearLayout>(R.id.llExternal2)
//        val btnPay = dialogcheckout.findViewById<Button>(R.id.btnPay)
//
//        val btnOther = dialogcheckout.findViewById<Button>(R.id.btnOther)
//
//        val btnCash1 = dialogcheckout.findViewById<Button>(R.id.btnCash1)
//        val btnCash2 = dialogcheckout.findViewById<Button>(R.id.btnCash2)
//        val btnCash3 = dialogcheckout.findViewById<Button>(R.id.btnCash3)
//
//        if (CM.getSp(mActivity, CV.PAYMENT_TYPE, "").equals("Cash")) {
//            btnCash.background = mActivity.resources.getDrawable(R.drawable.btn_bg)
//            btnCash.setTextColor(resources.getColor(R.color.colorWhite))
//
//            btnExternal.setTextColor(resources.getColor(R.color.colorPrimary))
//            btnExternal.background = resources.getDrawable(R.drawable.edt_bg)
//
//            btnOther.setTextColor(resources.getColor(R.color.colorPrimary))
//            btnOther.background = resources.getDrawable(R.drawable.edt_bg)
//
//            llCash.visibility = View.VISIBLE
//            relCash.visibility = View.VISIBLE
//            llExternal.visibility = View.GONE
//            llExternal2.visibility = View.GONE
//            btnPay.visibility = View.GONE
//        } else {
//            btnExternal.background = mActivity.resources.getDrawable(R.drawable.btn_bg)
//            btnExternal.setTextColor(resources.getColor(R.color.colorWhite))
//
//            btnCash.setTextColor(resources.getColor(R.color.colorPrimary))
//            btnCash.background = resources.getDrawable(R.drawable.edt_bg)
//
//            btnOther.setTextColor(resources.getColor(R.color.colorPrimary))
//            btnOther.background = resources.getDrawable(R.drawable.edt_bg)
//
//
//            llCash.visibility = View.GONE
//            relCash.visibility = View.GONE
//            llExternal.visibility = View.VISIBLE
//            llExternal2.visibility = View.VISIBLE
//            btnPay.visibility = View.VISIBLE
//        }
//
//        val edtAmount = dialogcheckout.findViewById<EditText>(R.id.edtAmount)
//        edtAmount.setFilters(arrayOf<InputFilter>(DecimalDigitsInputFilter(10, 2)))
//        val tvAmountEdit = dialogcheckout.findViewById<TextView>(R.id.tvAmountEdit)
//
//        edtAmount.addTextChangedListener(object : TextWatcher {
//
//            override fun afterTextChanged(s: Editable) {
//                if (s.isNotEmpty()) {
//                    tvAmountEdit.setText(s.toString())
//                }
//            }
//
//            override fun beforeTextChanged(
//                s: CharSequence, start: Int,
//                count: Int, after: Int
//            ) {
//            }
//
//            override fun onTextChanged(
//                s: CharSequence, start: Int,
//                before: Int, count: Int
//            ) {
//            }
//        })
//
//        val tvAmount = dialogcheckout.findViewById<TextView>(R.id.tvAmount)
//        val tt = mBinding.tvTotalValue.text.toString().split(" ").get(1).toDouble()
//
//        tvAmount.setText(String.format("%.2f", (tt)))
//        tvAmountEdit.setText(String.format("%.2f", (tt)))
//
//        btnCash1.setText(strAmount.get(0).toString())
//        btnCash2.setText(strAmount.get(1).toString())
//        btnCash3.setText(strAmount.get(2).toString())
//
//        btnCash1.setOnClickListener {
//            tvAmountEdit.setText(btnCash1.text.toString())
//        }
//        btnCash2.setOnClickListener {
//            tvAmountEdit.setText(btnCash2.text.toString())
//        }
//        btnCash3.setOnClickListener {
//            tvAmountEdit.setText(btnCash3.text.toString())
//        }
//
//
//        btnCash.setOnClickListener {
//            btnCash.setTextColor(resources.getColor(R.color.colorWhite))
//            btnCash.background = resources.getDrawable(R.drawable.btn_bg)
//
//            btnExternal.setTextColor(resources.getColor(R.color.colorPrimary))
//            btnExternal.background = resources.getDrawable(R.drawable.edt_bg)
//
//            btnOther.setTextColor(resources.getColor(R.color.colorPrimary))
//            btnOther.background = resources.getDrawable(R.drawable.edt_bg)
//
//            llCash.visibility = View.VISIBLE
//            relCash.visibility = View.VISIBLE
//            llExternal.visibility = View.GONE
//            llExternal2.visibility = View.GONE
//            btnPay.visibility = View.GONE
//        }
//
//        btnExternal.setOnClickListener {
//            btnExternal.setTextColor(resources.getColor(R.color.colorWhite))
//            btnExternal.background = resources.getDrawable(R.drawable.btn_bg)
//
//            btnCash.setTextColor(resources.getColor(R.color.colorPrimary))
//            btnCash.background = resources.getDrawable(R.drawable.edt_bg)
//
//            btnOther.setTextColor(resources.getColor(R.color.colorPrimary))
//            btnOther.background = resources.getDrawable(R.drawable.edt_bg)
//
//
//            llCash.visibility = View.GONE
//            relCash.visibility = View.GONE
//            llExternal.visibility = View.VISIBLE
//            llExternal2.visibility = View.VISIBLE
//            btnPay.visibility = View.VISIBLE
//        }
//
//        ivCancel.setOnClickListener {
//            openPaymentCancellationDialog(dialogcheckout)
//        }
//        btnTender.setOnClickListener {
//            val remaining_amount =
//                tvAmountEdit.text.toString().toDouble() - mBinding.tvTotalValue.text.toString().split(
//                    " "
//                ).get(1).toDouble()
//            if (remaining_amount < 0) {
//                Toast.makeText(
//                    mActivity,
//                    "Please pay remaining amount." + String.format("%.2f", (remaining_amount * -1)),
//                    Toast.LENGTH_SHORT
//                ).show()
//            } else {
//                dialogcheckout.dismiss()
//                openPassCodeDialog(remaining_amount, tvAmountEdit.text.toString())
//            }
//        }
//    }
//
//    private fun openPaymentCancellationDialog(dialog_previous: Dialog) {
//        val dialog = Dialog(mActivity)
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//
//        dialog.setContentView(R.layout.dialog_cancel_payment)
//
//        dialog.setCancelable(false)
//        dialog.show()
//
//        val btnNo = dialog.findViewById<Button>(R.id.btnNo)
//        val btnYes = dialog.findViewById<Button>(R.id.btnYes)
//        btnNo.setOnClickListener {
//            dialog.dismiss()
//        }
//
//        btnYes.setOnClickListener {
//            dialog.dismiss()
//            dialog_previous.dismiss()
//        }
//
//    }
//
//    private fun openPassCodeDialog(remain: Double, givenAmount: String) {
//        dialog = Dialog(mActivity)
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//
//        dialog.setContentView(R.layout.dialog_passcode)
//
//        dialog.setCancelable(false)
//        dialog.show()
//
//        val btnOneCredential = dialog.findViewById<Button>(R.id.btnOneCredential)
//        val btnTwoCredential = dialog.findViewById<Button>(R.id.btnTwoCredential)
//        val btnThreeCredential = dialog.findViewById<Button>(R.id.btnThreeCredential)
//        val btnFourCredential = dialog.findViewById<Button>(R.id.btnFourCredential)
//        val btnFiveCredential = dialog.findViewById<Button>(R.id.btnFiveCredential)
//        val btnSixCredential = dialog.findViewById<Button>(R.id.btnSixCredential)
//        val btnSevenCredential = dialog.findViewById<Button>(R.id.btnSevenCredential)
//        val btnEightCredential = dialog.findViewById<Button>(R.id.btnEightCredential)
//        val btnNineCredential = dialog.findViewById<Button>(R.id.btnNineCredential)
//        val btnZeroCredential = dialog.findViewById<Button>(R.id.btnZeroCredential)
//
//        val btnBackSpaceCredential = dialog.findViewById<Button>(R.id.btnBackSpaceCredential)
//        val btnResetMpin = dialog.findViewById<Button>(R.id.btnResetMpin)
//
//        ivRoundOneCredential = dialog.findViewById(R.id.ivRoundOneCredential)
//        ivRoundTwoCredential = dialog.findViewById(R.id.ivRoundTwoCredential)
//        ivRoundThreeCredential = dialog.findViewById(R.id.ivRoundThreeCredential)
//        ivRoundFourCredential = dialog.findViewById(R.id.ivRoundFourCredential)
//
//        llPinRound = dialog.findViewById(R.id.llPinRound)
//
//        val tvClose = dialog.findViewById<TextView>(R.id.tvClose)
//        tvClose.setOnClickListener {
//            dialog.dismiss()
//        }
//        btnResetMpin.setOnClickListener {
//            dialog.dismiss()
//            //  openChangePasscodeDialog()
//        }
//        btnOneCredential.setOnClickListener({
//            countCall(count)
//            fillPasscode("1", remain, givenAmount)
//        })
//
//        btnTwoCredential.setOnClickListener(View.OnClickListener {
//            countCall(count)
//            fillPasscode("2", remain, givenAmount)
//        })
//        btnThreeCredential.setOnClickListener({
//            countCall(count)
//            fillPasscode("3", remain, givenAmount)
//        })
//        btnFourCredential.setOnClickListener({
//            countCall(count)
//            fillPasscode("4", remain, givenAmount)
//        })
//        btnFiveCredential.setOnClickListener({
//            countCall(count)
//            fillPasscode("5", remain, givenAmount)
//        })
//        btnSixCredential.setOnClickListener({
//            countCall(count)
//            fillPasscode("6", remain, givenAmount)
//        })
//        btnSevenCredential.setOnClickListener({
//            countCall(count)
//            fillPasscode("7", remain, givenAmount)
//        })
//        btnEightCredential.setOnClickListener({
//            countCall(count)
//            fillPasscode("8", remain, givenAmount)
//        })
//        btnNineCredential.setOnClickListener({
//            countCall(count)
//            fillPasscode("9", remain, givenAmount)
//        })
//        btnZeroCredential.setOnClickListener({
//            countCall(count)
//            fillPasscode("0", remain, givenAmount)
//        })
//        btnBackSpaceCredential.setOnClickListener({
//            dropPasscode()
//            if (count > 0) {
//                count -= 1
//            }
//            minusCount = true
//            countCall(count)
//        })
//    }
//
//    fun countCall(c: Int) {
//        when (c) {
//            0 -> if (minusCount) {
//                count = 0
//                ivRoundOneCredential.setImageResource(R.drawable.roundblankbutton)
//                ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
//                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
//                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)
//
//                minusCount = false
//            } else {
//                count += 1
//                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
//                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
//                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)
//
//            }
//            1 -> if (minusCount) {
//                count = 1
//                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
//                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
//                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)
//
//                minusCount = false
//            } else {
//                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
//                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)
//
//                count += 1
//            }
//            2 -> if (minusCount) {
//                count = 2
//                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
//                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)
//
//                minusCount = false
//            } else {
//                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)
//
//                count += 1
//            }
//            3 -> if (minusCount) {
//                count = 3
//                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)
//
//                minusCount = false
//            } else {
//                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)
//
//                count += 1
//            }
//            4 -> if (minusCount) {
//                count = 4
//                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)
//
//                minusCount = false
//            } else {
//                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)
//
//                count += 1
//            }
//            5 -> if (minusCount) {
//                count = 5
//                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)
//
//                minusCount = false
//            } else {
//                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
//                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)
//
//                count += 1
//            }
//        }
//    }
//
//
//    fun dropPasscode() {
//        if (count <= 4 && count > 0) {
//            if (pin.length > 0) {
//                pin = pin.substring(0, pin.length - 1)
//            }
//        }
//    }
//
//    private fun showIncorrectPasscodeDialog() {
//        val dialog = Dialog(mActivity)
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//
//        dialog.setContentView(R.layout.dialog_wrong_mpin)
//
//        dialog.setCancelable(false)
//        dialog.show()
//
//        val btnReEnterMpin = dialog.findViewById<Button>(R.id.btnReEnterMpin)
//        btnReEnterMpin.setOnClickListener {
//            dialog.dismiss()
//        }
//
//    }
//
//    fun fillPasscode(code: String, remain: Double, givenAmount: String) {
//        if (count <= 4 && count > 0) {
//            pin = pin + code
//        }
//        if (count == 4) {
//            try {
//                if (!CM.isInternetAvailable(mActivity)) {
//                    val itemListLiveData = mDatabase.daoEmployeeList().getAllEmployee()
//                    var matchFound = false
//                    for (i in itemListLiveData.indices) {
//                        Log.e("=== emp name ==", "===" + itemListLiveData.get(i).strFirstName)
//                        if (pin.equals(itemListLiveData.get(i).iMpin)) {
//                            matchFound = true
//
//
//                            CM.setSp(
//                                mActivity,
//                                CV.EMP_NAME,
//                                itemListLiveData.get(i).strFirstName + " " + itemListLiveData.get(i).strLastName
//                            )
//                            CM.setSp(mActivity, CV.EMP_IMAGE, "")
//                            CM.setSp(mActivity, CV.EMP_ID, itemListLiveData.get(i).iCheckEmployeeId)
//                            CM.setSp(
//                                mActivity,
//                                CV.EMP_ROLE,
//                                itemListLiveData.get(i).strEmployeeRoleName
//                            )
//
//                            break
//
//                        }
//                    }
//                    if (matchFound) {
//                        dialog.dismiss()
//                        count = 0
//                        ivRoundOneCredential.setImageResource(R.drawable.roundblankbutton)
//                        ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
//                        ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
//                        ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)
//
//                        minusCount = false
//                        pin = ""
//                        CM.startActivity(mActivity, CheckInOutActivity::class.java)
//                    } else {
//                        showIncorrectPasscodeDialog()
//                        val shake =
//                            AnimationUtils.loadAnimation(mActivity, R.anim.shake)
//                        llPinRound.startAnimation(shake)
//                        count = 0
//                        ivRoundOneCredential.setImageResource(R.drawable.roundblankbutton)
//                        ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
//                        ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
//                        ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)
//
//                        minusCount = false
//                        pin = ""
//
//                    }
//                } else {
//                    webcallVerifyMPin(remain, givenAmount)
//                }
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//        }
//    }
//
//
//    private fun round(n: Int): Int {
//        val a = n / 10 * 10
//        val b = a + 10
//        return if (n - a > b - n) b else a
//    }
//
//    private fun webcallVerifyMPin(remain: Double, givenAmount: String) {
//        showKcsDialog()
//        viewModelVerifyMPin!!.verifyMPin(pin)
//            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
//                dismissKcsDialog()
//                if (loginResponseModel.data == null) {
//                    showIncorrectPasscodeDialog()
//                    val shake =
//                        AnimationUtils.loadAnimation(mActivity, R.anim.shake)
//                    llPinRound.startAnimation(shake)
//                    count = 0
//                    ivRoundOneCredential.setImageResource(R.drawable.roundblankbutton)
//                    ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
//                    ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
//                    ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)
//
//                    minusCount = false
//                    pin = ""
//
//                } else {
//                    Toast.makeText(mActivity, loginResponseModel.data!!.message, Toast.LENGTH_LONG)
//                        .show()
//                    dialog.dismiss()
//
//                    CM.setSp(
//                        mActivity,
//                        CV.EMP_NAME,
//                        loginResponseModel.data!!.data.strFirstName + " " + loginResponseModel.data!!.data.strLastName
//                    )
//                    CM.setSp(
//                        mActivity,
//                        CV.EMP_IMAGE,
//                        loginResponseModel.data!!.data.strProfilePhoto
//                    )
//                    CM.setSp(mActivity, CV.EMP_ID, loginResponseModel.data!!.data.iCheckEmployeeId)
//                    CM.setSp(
//                        mActivity,
//                        CV.EMP_ROLE,
//                        loginResponseModel.data!!.data.strEmployeeRoleName
//                    )
//                    openReceiptDialog(remain, givenAmount)
//
//                    count = 0
//                    minusCount = false
//                    pin = ""
//                }
//
//            })
//    }
//
//    private fun openReceiptDialog(remain: Double, givenAmount: String) {
//        val dialog = Dialog(mActivity)
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//
//        dialog.setContentView(R.layout.dialog_reciept)
//
//        dialog.setCancelable(false)
//        dialog.show()
//
//        val ivCancel = dialog.findViewById<ImageView>(R.id.ivCancel)
//        val btnFinish = dialog.findViewById<Button>(R.id.btnFinish)
//        val tvRemainingAmount = dialog.findViewById<TextView>(R.id.tvRemainingAmount)
//
//        val cbYes = dialog.findViewById<CheckBox>(R.id.cbYes)
//        val cbNo = dialog.findViewById<CheckBox>(R.id.cbNo)
//        val edtEmail = dialog.findViewById<EditText>(R.id.edtEmail)
//        edtEmail.setText(selectedCustomerModel!!.strEmail)
//
//        tvRemainingAmount.setText(
//            "Change Due " + String.format(
//                "%.2f",
//                (remain)
//            ) + " out of " + String.format(
//                "%.2f",
//                (givenAmount.toDouble())
//            )
//        )
//        edtEmail.isEnabled = false
//        cbYes.setOnCheckedChangeListener({ buttonView, isChecked ->
//            if (isChecked) {
//                edtEmail.isEnabled = true
//                cbNo.isChecked = false
//            }
//        })
//
//        cbNo.setOnCheckedChangeListener({ buttonView, isChecked ->
//            if (isChecked) {
//                edtEmail.isEnabled = false
//                cbYes.isChecked = false
//            }
//        })
//        ivCancel.setOnClickListener {
//            openPaymentCancellationDialog(dialog)
//        }
//        btnFinish.setOnClickListener {
//            if (cbYes.isChecked) {
//                if (edtEmail.text.toString().equals("")) {
//                    Toast.makeText(mActivity, "Please Enter Email", Toast.LENGTH_SHORT).show()
//                } else if (!edtEmail.text.toString().isEmailValid()) {
//                    Toast.makeText(mActivity, "Please Enter Valid Email", Toast.LENGTH_SHORT).show()
//                } else {
//                    placeOrder(dialog)
//                }
//            } else {
//                placeOrder(dialog)
//            }
//
//        }
//    }
//
//    fun String.isEmailValid(): Boolean {
//        return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
//    }
//
//    private fun placeOrder(dialog_receipet: Dialog) {
//        if (!CM.isInternetAvailable(mActivity)) {
//            CM.showMessageOK(
//                mActivity,
//                "",
//                resources.getString(R.string.msg_network_error),
//                null
//            )
//        } else {
//            val date = Calendar.getInstance().time
//            val sdf = SimpleDateFormat(CV.WS_DATE_FORMATTE)
//            // val orderTransList = mDatabase.daoOrderTrans().getOrderTrans(order_id)
//            val adapter: OrderItemListAdapter = mBinding.rvCart.adapter as OrderItemListAdapter
//            val orderTransList = adapter.getDataset()
//            for (i in orderTransList.indices) {
//                val temp = OrderTransRequestModel(
//                    orderTransList.get(i).itemId.toString(),
//                    orderTransList.get(i).itemQty.toString(),
//                    orderTransList.get(i).itemRate.toString(),
//                    orderTransList.get(i).itemAmount.toString(),
//                    orderTransList.get(i).taxAmount,
//                    "0",
//                    "",
//                    "",
//                    "",
//                    "",
//                    sdf.format(date),
//                    ""
//                )
//                itemRequestList.add(temp)
//            }
//
//            webCallPlaceOrder(dialog, dialog_receipet, orderTransList)
//        }
//
//    }
//
//    private fun webCallPlaceOrder(
//        dialog: Dialog,
//        dialog_receipet: Dialog,
//        orderTransList: List<OrderTransModel>
//    ) {
//        val date = Calendar.getInstance().time
//        val sdf = SimpleDateFormat(CV.WS_DATE_FORMATTE)
//        val paymentRequestList: ArrayList<PaymentRequestModel> = ArrayList()
//
//        val temp = PaymentRequestModel(
//            "1",
//            mBinding.tvTotal.text.toString().split(" ").get(1),
//            mBinding.tvTotalValue.text.toString().split(" ").get(1),
//            "Cash",
//            "",
//            sdf.format(date)
//        )
//        paymentRequestList.add(temp)
//        showKcsDialog()
//
//        placeOrderViewModel!!.placeOrder(
//            selectedCustomerModel!!.iCustomerId,
//            CM.getSp(mActivity, CV.EMP_ID, 0) as Int,
//            mBinding.tvTotal.text.toString().split(" ").get(1).toDouble(),
//            mBinding.tvTax.text.toString().split(" ").get(1).toDouble(),
//            0.0,
//            mBinding.tvTotalValue.text.toString().split(" ").get(1).toDouble(),
//            Gson().toJson(itemRequestList),
//            Gson().toJson(paymentRequestList)
//
//        )
//            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
//                dismissKcsDialog()
//                if (loginResponseModel.data == null) {
//                    Toast.makeText(mActivity, loginResponseModel.message, Toast.LENGTH_LONG).show()
//                } else {
//                    dialog.dismiss()
//                    dialog_receipet.dismiss()
//                    Toast.makeText(mActivity, loginResponseModel.data!!.message, Toast.LENGTH_LONG)
//                        .show()
//                    mDatabase.daoOrderTrans().deleteOrder(order_id)
//                    if (orderTransList.get(0).reason.equals("On Hold")) {
//                        mDatabase.daoOrderTrans().deleteHoldOrder("On Hold")
//                        CM.setSp(mActivity, CV.IS_HOLD_ORDER, false)
//
//                    }
//                    resetOrder()
//
//                }
//            })
//    }
//
//    public fun resetOrder() {
//        //   mDatabase.daoOrderTrans().deleteOrder(order_id)
//        getProductsFromDatabase(order_id)
//        order_id = ""
//        CM.setSp(mActivity, CV.ORDER_ID, order_id)
//
//        mBinding.rvCategory.adapter = CategoryListAdapter(mActivity, categoryList, this)
//        populateSubcategory(categoryList.get(0).iProductCategoryID)
//        mBinding.tvCategoryValue.setText(categoryList.get(0).strProductCategoryName)
//        webCallCustomerList("")
//    }
//
//
//    private fun openSearchItemDialog() {
//        allDataModel.clear()
//        dialogSearchItem = Dialog(mActivity)
//        dialogSearchItem.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialogSearchItem.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        dialogSearchItem.setContentView(R.layout.dialog_search_item)
//        dialogSearchItem.setCancelable(true)
//        dialogSearchItem.show()
//        dialogSearchItem.setOnCancelListener {
//            searchViewModel?.resetProductsList()
//        }
//        val rvCustomer = dialogSearchItem.findViewById<RecyclerView>(R.id.rvCustomer)
//
//        val adapter = ItemListSearchAdapter(mActivity,  allDataModel,this)
//        adapter.notifyDataSetChanged()
//        rvCustomer.adapter = adapter
//
//        var scrollListener: RecyclerViewLoadMoreScroll
//        val viewModelFactory = Injection.provideViewModelFactory(dialogSearchItem.context)
//        searchViewModel = ViewModelProvider(this,viewModelFactory).get(SearchViewModel::class.java)
//
//
//        val mLayoutManager = LinearLayoutManager(activity)
//        mLayoutManager.orientation = LinearLayoutManager.VERTICAL
//        rvCustomer.layoutManager = mLayoutManager
//        rvCustomer.setHasFixedSize(true)
//        scrollListener = RecyclerViewLoadMoreScroll(mLayoutManager)
//        scrollListener.setOnLoadMoreListener(object : OnLoadMoreListener {
//            override fun onLoadMore() {
//                searchViewModel?.searchProducts()
//
//            }
//        })
//        rvCustomer.addOnScrollListener(scrollListener)
//
//        searchViewModel!!.products.observe(this, androidx.lifecycle.Observer { productList ->
//            if (productList != null) {
//                allDataModel.addAll(productList)
//                adapter.notifyDataSetChanged()
////                scrollListener.setLoaded()
//                searchViewModel!!.OFFSET +=productList.size;
//            }
//        })
//
//        searchViewModel?.OFFSET=0
//        searchViewModel!!.searchText="";
//        searchViewModel?.searchProducts()
//
//
//        val edtSearchCustomer =
//            dialogSearchItem.findViewById<AppCompatEditText>(R.id.edtSearchCustomer)
//
//        edtSearchCustomer.addTextChangedListener(object : TextWatcher {
//
//            override fun afterTextChanged(s: Editable) {
//                if (s.isEmpty()) {
//                    searchViewModel!!.searchText = s.toString()
//                } else {
//                    searchViewModel!!.searchText = s.toString()
//                }
//                allDataModel.clear()
//                adapter.notifyDataSetChanged()
//                searchViewModel!!.OFFSET =0
//                searchViewModel!!.searchProducts()
//
//            }
//
//            override fun beforeTextChanged(
//                s: CharSequence, start: Int,
//                count: Int, after: Int
//            ) {
//            }
//
//            override fun onTextChanged(
//                s: CharSequence, start: Int,
//                before: Int, count: Int
//            ) {
//            }
//        })
//    }
//
//
//    private fun itemAmountCalculation(item_model: ItemModel) {
//        if (CM.getSp(mActivity, CV.ORDER_ID, "").equals("")) {
//            order_id = UUID.randomUUID().toString()
//            CM.setSp(mActivity, CV.ORDER_ID, order_id)
//        } else {
//            order_id = CM.getSp(mActivity, CV.ORDER_ID, "") as String
//        }
//
//        val orderTransData =
//            mDatabase.daoOrderTrans().getOrderTransItem(order_id, item_model.iProductID)
//
//        if (orderTransData.size > 0) {
//            mDatabase.daoOrderTrans().updateOrderTrans2(
//                orderTransData.get(0).itemQty + 1,
//                (orderTransData.get(0).itemQty + 1) * orderTransData.get(0).itemRate,
//                String.format(
//                    "%.2f",
//                    getTaxAmount(
//                        (orderTransData.get(0).itemQty + 1) * orderTransData.get(0).itemRate,
//                        item_model.totalTax
//                    )
//                ),
//                order_id,
//                item_model.iProductID
//            )
//        } else {
//            val orderTransModel = OrderTransModel(
//                UUID.randomUUID().toString(),
//                order_id,
//                item_model.iProductID,
//                item_model.strProductName,
//                item_model.dblRetailPrice.toDouble(),
//                1,
//                item_model.dblRetailPrice.toDouble(),
//                item_model.strPricingType,
//                "",
//                "",
//                String.format(
//                    "%.2f",
//                    getTaxAmount(item_model.dblRetailPrice.toDouble(), item_model.totalTax)
//                ),
//                "",
//                0,
//                0.0
//            )
//            mDatabase.daoOrderTrans().insertOrderTrans(orderTransModel)
//        }
//        getProductsFromDatabase(order_id)
//
//    }
//
//    private fun getTaxAmount(amount: Double, total_tax: Double): Double {
//        return (amount * total_tax) / 100
//    }
//
//    private fun getProductsFromDatabase(orderId: String) {
//        val orderTransLiveData = mDatabase.daoOrderTrans().getOrderTrans(orderId)
//        val orderTransList: ArrayList<OrderTransModel>
//
//        if (orderTransLiveData != null) {
//            orderTransList = orderTransLiveData as ArrayList<OrderTransModel>
//            val test = orderTransList.asReversed()
//            mBinding.rvCart.adapter = OrderItemListAdapter(
//                mActivity,
//                mDatabase,
//                test,
//                this,
//                this,
//                this,
//                "order"
//            )
//            (mBinding.rvCart.adapter as OrderItemListAdapter).notifyDataSetChanged()
//            getTotalAmount(orderId)
//        }
//    }
//
//    private fun getTotalAmount(orderId: String) {
//        val orderTransLiveData = mDatabase.daoOrderTrans().getOrderTrans(orderId)
//        if (orderTransLiveData != null && orderTransLiveData.size > 0) {
//            var total = 0.0
//            var total_tax = 0.0
//            for (i in orderTransLiveData.indices) {
//                total = total + orderTransLiveData.get(i).itemAmount
//                total_tax = total_tax + orderTransLiveData.get(i).taxAmount.toDouble()
//            }
//            mBinding.tvTax.setText(currency_symbol + " " + String.format("%.2f", total_tax))
//            mBinding.tvTotal.setText(currency_symbol + " " + String.format("%.2f", total))
//            mBinding.tvTotalValue.setText(
//                currency_symbol + " " + String.format(
//                    "%.2f",
//                    (total + total_tax)
//                )
//            )
//        } else {
//            mBinding.tvTax.setText(currency_symbol + " " + String.format("%.2f", 00.00))
//            mBinding.tvTotal.setText(currency_symbol + " " + String.format("%.2f", 00.00))
//            mBinding.tvTotalValue.setText(currency_symbol + " " + String.format("%.2f", 00.00))
//        }
//    }
//
//    private fun getAllcategories() {
//        val categoryListLiveData = mDatabase.daoCategoryList().getCategories()
//        categoryListLiveData.observe(this, androidx.lifecycle.Observer { categorylist ->
//            if (categorylist != null) {
//                categoryList = categorylist as ArrayList<CategoryModel>
//                mBinding.rvCategory.adapter = CategoryListAdapter(mActivity, categoryList, this)
//                populateSubcategory(categoryList.get(0).iProductCategoryID)
//                mBinding.tvCategoryValue.setText(categoryList.get(0).strProductCategoryName)
//
//            }
//        })
//    }
//
//    private fun populateSubcategory(iProductCategoryID: Int) {
//        val subcategoryListLiveData =
//            mDatabase.daoSubCategoryList().getSubCategories(iProductCategoryID)
//        subcategoryListLiveData.observe(this, androidx.lifecycle.Observer { subcategorylist ->
//            if (subcategorylist != null && subcategorylist.size > 0) {
//                subcategoryList = subcategorylist as ArrayList<SubCategoryModel>
//                mBinding.rvsubCategory.adapter =
//                    SubCategoryListAdapter(mActivity, subcategoryList, this)
//                mBinding.tvsubCategoryValue.setText(subcategoryList.get(0).strProductCategoryName)
//                populateItem(subcategoryList.get(0).iProductCategoryID)
//            }
//        })
//    }
//
//    private fun populateItem(suCategoryId: Int) {
//        val itemListLiveData = mDatabase.daoItemList().getItems(suCategoryId)
//        itemListLiveData.observe(this, androidx.lifecycle.Observer { itemlist ->
//            if (itemlist != null) {
//                itemList = itemlist as ArrayList<ItemModel>
//                itemListFilter = itemList
//                mBinding.gridItem.adapter = ItemListAdapter(mActivity, itemList)
//            }
//        })
//    }
//
//    override fun onCreateView(
//        inflater: LayoutInflater,
//        container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        mBinding = DataBindingUtil.inflate(
//            inflater, R.layout.fragment_new_sales,
//            container, false
//        )
//        return mBinding.root
//    }
//
//    override fun listItemClickedCategory(position: Int, category_id: Int) {
//        mBinding.tvCategoryValue.setText(categoryList.get(position).strProductCategoryName)
//        populateSubcategory(category_id)
//    }
//
//    override fun listItemClickedSubCategory(position: Int, subcategory_id: Int) {
//        mBinding.tvsubCategoryValue.setText(subcategoryList.get(position).strProductCategoryName)
//        populateItem(subcategory_id)
//    }
//
//    override fun listItemClickedAdd(
//        position: Int,
//        orderTransModel: OrderTransModel,
//        total_tax: Double
//    ) {
//        getTotalAmount(order_id)
//
//    }
//
//    override fun listItemClickedSub(
//        position: Int,
//        orderTransModel: OrderTransModel,
//        total_tax: Double
//    ) {
//        getTotalAmount(order_id)
//
//    }
//
//    override fun listItemClickedDel(total_tax: Double) {
//        getTotalAmount(order_id)
//    }
//
//    override fun listItemClickedItem(position: Int, category_id: Int) {
//
//    }
//
//    override fun listItemClickedItem(itemModel: ItemModel) {
//        CM.hideSoftKeyboard(mActivity)
//        itemAmountCalculation(itemModel)
//        if(dialogSearchItem!=null)
//            dialogSearchItem.dismiss()
//    }
//
//    override fun listItemClickedCustomer(position: Int, category_id: Int) {
//        CM.hideSoftKeyboard(mActivity)
//        if (filterProduct.size > 0) {
//            selectedCustomerModel = filterProduct.get(position)
//        } else {
//            selectedCustomerModel = customerListFilter.get(position)
//        }
//        dialogSearchCustomer.dismiss()
//        mBinding.tvNewOrder.setText(selectedCustomerModel!!.strFirstName + " " + selectedCustomerModel!!.strLastName)
//    }
//
//    override fun listItemClickedOrder(position: Int, orderHistoryModel: OrderHistoryModel) {
//        selectedHistory = orderHistoryModel
//        if (orderHistoryModel.orderRefund != null && orderHistoryModel.orderRefund!!.size > 0) {
//            Toast.makeText(
//                mActivity,
//                "Refund is already done against this order.",
//                Toast.LENGTH_SHORT
//            ).show()
//            dialogorder.dismiss()
//        } else {
//            if (orderHistoryModel.status.equals("On Hold")) {
//                order_id = orderHistoryModel.strInvoiceNumber
//                CM.setSp(mActivity, CV.ORDER_ID, orderHistoryModel.strInvoiceNumber)
//                CM.setSp(mActivity, CV.IS_HOLD_ORDER, false)
//
//                mBinding.btnSave.visibility = View.VISIBLE
//                mBinding.btnCancel.visibility = View.VISIBLE
//                mBinding.btnCheckout.visibility = View.VISIBLE
//                mBinding.btnExchange.visibility = View.GONE
//                mBinding.btnRefund.visibility = View.GONE
//                mBinding.rvCart.adapter =
//                    OrderItemListAdapter(
//                        mActivity,
//                        mDatabase,
//                        mDatabase.daoOrderTrans().getHoldOrderItem("On Hold") as MutableList<OrderTransModel>,
//                        this,
//                        this,
//                        this,
//                        "order"
//                    )
//                if (mDatabase.daoOrderTrans().getHoldOrderItem("On Hold") != null &&
//                    mDatabase.daoOrderTrans().getHoldOrderItem("On Hold").size > 0
//                ) {
//                    var total_tax = 0.0
//                    var total = 0.0
//                    for (i in mDatabase.daoOrderTrans().getHoldOrderItem("On Hold").indices) {
//                        total_tax =
//                            total_tax + mDatabase.daoOrderTrans().getHoldOrderItem("On Hold").get(i).taxAmount.toDouble()
//                        total =
//                            total + mDatabase.daoOrderTrans().getHoldOrderItem("On Hold").get(i).itemAmount
//                    }
//                    mBinding.tvTax.setText(currency_symbol + " " + String.format("%.2f", total_tax))
//                    mBinding.tvTotal.setText(currency_symbol + " " + String.format("%.2f", total))
//                    mBinding.tvTotalValue.setText(
//                        currency_symbol + " " + String.format(
//                            "%.2f",
//                            (total + total_tax)
//                        )
//                    )
//
//                }
//
//            } else {
//                mBinding.btnSave.visibility = View.GONE
//                mBinding.btnCancel.visibility = View.GONE
//                mBinding.btnCheckout.visibility = View.GONE
//                mBinding.btnExchange.visibility = View.VISIBLE
//                mBinding.btnRefund.visibility = View.VISIBLE
//                var orderHistory = mDatabase.daoOrerHistoryList().getOrderHistory(orderHistoryModel.strInvoiceNumber)
//
//                Log.e("====", "===== size ===" + orderHistory.get(0).orderProducts!!.size)
//
//                var orderTransList: ArrayList<OrderTransModel> = ArrayList()
//
//                for (i in orderHistory.get(0).orderProducts!!.indices) {
//                    val temp = OrderTransModel(
//                        orderHistoryModel.iCheckDetailID,
//                        orderHistoryModel.strInvoiceNumber,
//                        orderHistoryModel.orderProducts!!.get(i).iProductId,
//                        orderHistoryModel.orderProducts!!.get(i).strProductName,
//                        orderHistoryModel.orderProducts!!.get(i).dblPrice.toDouble(),
//                        orderHistoryModel.orderProducts!!.get(i).dblItemQty.toInt(),
//                        orderHistoryModel.orderProducts!!.get(i).dblAmount.toDouble(),
//                        "",
//                        "",
//                        "",
//                        orderHistoryModel.orderProducts!!.get(i).dblItemTaxAmount,
//                        "",
//                        0,
//                        0.0
//                    )
//                    orderTransList.add(temp)
//                }
//                mBinding.rvCart.adapter =
//                    OrderItemListAdapter(
//                        mActivity,
//                        mDatabase,
//                        orderTransList,
//                        this,
//                        this,
//                        this,
//                        "refund"
//                    )
//
//                mBinding.tvTotal.setText(currency_symbol + " " + orderHistoryModel.dblCheckGrossTotal)
//                mBinding.tvTax.setText(currency_symbol + " " + orderHistoryModel.dblCheckTaxTotal)
//                mBinding.tvTotalValue.setText(currency_symbol + " " + orderHistoryModel.dblCheckNetTotal)
//            }
//            dialogorder.dismiss()
//        }
//
//    }
//}
//
//
