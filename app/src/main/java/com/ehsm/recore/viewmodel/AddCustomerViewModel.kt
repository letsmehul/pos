package com.ehsm.recore.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.model.AddCustomerResponseModel
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.model.LoginResponseModel
import com.ehsm.recore.model.ResponseModel
import com.kcspl.divyangapp.repository.AddCustomerRepository
import com.kcspl.divyangapp.repository.ForgotPasswordRepository
import com.kcspl.divyangapp.repository.LoginRepository


class AddCustomerViewModel : ViewModel() {

    private var loginData: MutableLiveData<DataWrapper<AddCustomerResponseModel>>? = null
    private val addCustomerViewModel = AddCustomerRepository()


    fun addCustomer(
        first_name: String,
        last_name: String,
        email: String,
        phone: String,
        remarks: String,
        gender: String,
        status: String,
        strAddress1: String,
        strAddress2: String,
        city_id: Int,
        state_id: Int,
        zipcode: String,
        dob: String,
        iCustomerId: String
    ): MutableLiveData<DataWrapper<AddCustomerResponseModel>>? {
        loginData = addCustomerViewModel.addCustomer(
            first_name, last_name, email, phone, remarks, gender, status, strAddress1,
            strAddress2,
            city_id,
            state_id,
            zipcode,
            dob,
            iCustomerId
        )
        return loginData
    }
}