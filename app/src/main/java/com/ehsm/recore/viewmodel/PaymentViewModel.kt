package com.ehsm.recore.viewmodel

import android.app.Application
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.ehsm.recore.R
import com.ehsm.recore.Recore
import com.ehsm.recore.model.*
import com.ehsm.recore.repository.CardConnectRepository
import com.ehsm.recore.repository.PaymentRepository
import com.ehsm.recore.repository.TerminalRepository
import com.ehsm.recore.utils.AppConstant
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CV
import com.ehsm.recore.utils.Resource
import com.ehsm.recore.utils.logger.LogUtil
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import retrofit2.HttpException
import java.io.IOException
import java.util.*


class PaymentViewModel(
    val applicationContext: Application,
    private val paymentRepository: PaymentRepository,
val mactivity:LifecycleOwner) : AndroidViewModel(applicationContext) {
    private var merchantId = ""
    private var merchantValue: String? = ""
    var hsn = ""
    private val promptMsg = applicationContext.getString(R.string.prompt_payment)
    var selectedPaymentMode = AppConstant.CREDIT
    var payableAmount = ""
    var server_order_id = ""
    var bolt_order_id = "0"
    var cardRequestModel = CardRequestModel()
    var manualCardReadRequestModel = ManualCardReadRequestModel()
    var requestReadCard = RequestReadCard()
    var cardResponseModel: CardResponseModel? = null
    var manualCardResponseModel: ManualCardResponseModel? = null
    private var terminalListData: MutableLiveData<DataWrapper<TerminalListResponseModel>>? = null
    private val terminalRepository = TerminalRepository()
    var msgCancel: String? = null

    var reasonForRefund = ""

    private var checkPaymentData: MutableLiveData<DataWrapper<CheckPaymentResponse>>? = null
    private val cardConnectRepository = CardConnectRepository()

    private var paymentStatus = MutableLiveData<Int>()
    val getPaymentStatusObserver: MutableLiveData<Int>
        get() = paymentStatus

    private var Boltresponse: MutableLiveData<DataWrapper<BoltModel>>? = null

    init {
        CM.getEncryptedSharedPreferences(context = applicationContext).let {
            Recore.getApp()?.applicationContext?.let {
                val sharedPref = CM.getEncryptedSharedPreferences(it)
                merchantValue = sharedPref.getString(AppConstant.MERCHANTID, "")
            }
            merchantId = it.getString(AppConstant.MERCHANTID, merchantValue).toString()
            hsn = it.getString(AppConstant.HSN, AppConstant.HSN_DEFAULT_VALUE).toString()

            if (!TextUtils.isEmpty(merchantId) && !TextUtils.isEmpty(hsn)) {
                val StoreName = CM.getSp(applicationContext, CV.STORE_NAME, "")
                display(
                    String.format(
                        applicationContext.getString(R.string.welcome_msg),
                        StoreName
                    )
                )
            }
        }
    }

    fun getMerchantId() = merchantId

    private fun updateCommon() {
        CM.getEncryptedSharedPreferences(context = applicationContext).let {
            merchantId = it.getString(AppConstant.MERCHANTID, merchantValue).toString()
            hsn = it.getString(AppConstant.HSN, AppConstant.HSN_DEFAULT_VALUE).toString()
        }
    }


    fun getTerminalsFromBoltServer() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = terminalRepository.apiCallTerminal()))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = handleError(exception)))
        }
    }


    fun getConnect(force: Boolean) = liveData(Dispatchers.IO) {
        updateCommon()
        val handler = Handler(Looper.getMainLooper())
        handler.post {
            sendBoltrequest(
                bolt_order_id,
                server_order_id,
                "",
                "",
                Gson().toJson(RequestPingOrConnect(merchantId,hsn,force)),
                ""
            )?.observe(mactivity, androidx.lifecycle.Observer {
                LogUtil.d("Boltresponsedata", it.toString())
                if (it.data!=null)
                {
                    bolt_order_id=it.data!!.data.bolt_id
                }else{

                }
            })
        }
        emit(Resource.loading(data = null))
        try {
            val response = paymentRepository.connect(merchantId, hsn, force)

            emit(Resource.success(data = response))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = handleError(exception)))
        }
    }

    fun disConnect() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            val response = paymentRepository.disconnect(merchantId, hsn)

            emit(Resource.success(data = response))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = handleError(exception)))
        }
    }

    fun cancel() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            val response = paymentRepository.cancel(merchantId, hsn)
            emit(Resource.success(data = response))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = handleError(exception)))
        }
    }

    fun readConfirmation() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success(
                    data = paymentRepository.readConfirmation(
                        merchantId,
                        hsn,
                        promptMsg
                    )
                )
            )
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = handleError(exception)))
        }
    }

    fun readInput() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success(
                    data = paymentRepository.readInput(
                        ReadInput(
                            merchantId,
                            hsn,
                            promptMsg,
                            false,
                            "N1,1"
                        )
                    )
                )
            )
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = handleError(exception)))
        }
    }

    fun display(displayMessage: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success(
                    data = paymentRepository.display(
                        merchantId,
                        hsn,
                        displayMessage
                    )
                )
            )
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = handleError(exception)))
        }
    }

    fun authCard(orderId: String) = liveData(Dispatchers.IO) {
        cardRequestModel.merchantId = merchantId
        cardRequestModel.hsn = hsn
        cardRequestModel.aid = selectedPaymentMode.toLowerCase(Locale.getDefault())
        cardRequestModel.amount = payableAmount
        cardRequestModel.beep = false
        cardRequestModel.orderId = orderId
        /*if(cardRequestModel.aid ==AppConstant.DEBIT){
            cardRequestModel.includeSignature =false
            cardRequestModel.includePin =true
        }else{

            cardRequestModel.includeSignature =true
            cardRequestModel.includePin =false
        }*/
        val handler = Handler(Looper.getMainLooper())
        handler.post {
            sendBoltrequest(
                bolt_order_id,
                server_order_id,
                Gson().toJson(cardRequestModel),
                "",
                "",
                ""
            )?.observe(mactivity, androidx.lifecycle.Observer {
                LogUtil.d("Boltresponsedata", it.toString())
                if (it.data!=null)
                {
                    bolt_order_id=it.data!!.data.bolt_id

                }else{

                }
            })
        }
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = paymentRepository.authCard(cardRequestModel)))
        } catch (exception: Exception) {

            emit(Resource.error(data = null, message = handleError(exception)))
        }
    }

    fun authManual(orderId: String) = liveData(Dispatchers.IO) {
        requestReadCard.merchantId = merchantId
        requestReadCard.hsn = hsn
        requestReadCard.amount = payableAmount
        requestReadCard.includePIN = true
        requestReadCard.orderId = orderId
        cardRequestModel.beep = false
        emit(Resource.loading(data = null))
        val handler = Handler(Looper.getMainLooper())
        handler.post {
            sendBoltrequest(
                bolt_order_id,
                server_order_id,
                Gson().toJson(requestReadCard),
                "",
                "",
                ""
            )?.observe(mactivity, androidx.lifecycle.Observer {
                LogUtil.d("Boltresponsedata", it.toString())
                if (it.data!=null)
                {
                    bolt_order_id=it.data!!.data.bolt_id

                }else{

                }
            })
        }
        try {
            emit(Resource.success(data = paymentRepository.authManual(requestReadCard)))
        } catch (exception: Exception) {

            emit(Resource.error(data = null, message = handleError(exception)))
        }
    }

    fun readManualCard() = liveData(Dispatchers.IO) {
        manualCardReadRequestModel.merchantId = merchantId
        manualCardReadRequestModel.hsn = hsn
        manualCardReadRequestModel.includeExpirationDate = true
        manualCardReadRequestModel.beep = false
        manualCardReadRequestModel.includeSignature = false
        manualCardReadRequestModel.includePIN = true
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = paymentRepository.readManual(manualCardReadRequestModel)))
        } catch (exception: Exception) {

            emit(Resource.error(data = null, message = handleError(exception)))
        }
    }

    private fun getErrorMessageFromGenericResponse(httpException: HttpException): String? {
        var errorMessage: String? = null
        try {
            val body = httpException.response()?.errorBody()

            val adapter = Gson().getAdapter(BaseBoltAPIResponse::class.java)
            val errorParser = adapter.fromJson(body?.string())
            val code = httpException.response()?.code()
            Log.d("Error code ", "$code")
            errorMessage = errorParser.errorMessage
            when (code) {
                500 -> {
                    when (errorParser.errorCode) {
                        6 -> {
                            errorMessage = String.format(
                                applicationContext.getString(R.string.err_bolt_no_terminal_connected),
                                hsn
                            )
                        }
                        7 -> {
                            errorMessage =
                                applicationContext.getString(R.string.err_bolt_busy_terminal_connected)
                        }
                        8 -> {
                            errorMessage =
                                applicationContext.getString(R.string.err_bolt_busy_terminal_connected)
                        }
                    }
                }
                400 -> {
                    errorMessage =
                        applicationContext.getString(R.string.err_bolt_pin_debit_not_supported)
                }
                643 -> {
                    errorMessage = applicationContext.getString(R.string.err_bolt_server_failed)
                }
                700 -> {
                    errorMessage =
                        applicationContext.getString(R.string.err_bolt_signature_not_supported)
                }
                403 -> {
                    errorMessage =
                        applicationContext.getString(R.string.err_bolt_signature_not_supported)
                }
            }
        } catch (e: IOException) {
            e.printStackTrace()

        }
        return errorMessage
    }

    fun resetAuthCard() {
        cardRequestModel = CardRequestModel()
        cardRequestModel.includeSignature = false
        cardRequestModel.includePin = false
        selectedPaymentMode = AppConstant.CREDIT
    }

    fun resetCardResponse() {
        cardResponseModel = null
    }

    private fun handleError(exception: Exception): String {
        return if (exception is HttpException) {
            val errorMessage = getErrorMessageFromGenericResponse(exception)
            if (errorMessage.isNullOrBlank()) {
                exception.message ?: applicationContext.getString(R.string.error_sg_bolt_server)
            } else {
                errorMessage
            }
        } else {
            exception.message ?: applicationContext.getString(R.string.error_sg_bolt_server)
        }
    }


    fun checkPaymentApi(
        orderId: String
    ): MutableLiveData<DataWrapper<CheckPaymentResponse>>? {
        checkPaymentData = cardConnectRepository.apiCallPaymentCheck(orderId, merchantId)
        return checkPaymentData
    }

    fun sendBoltrequest(
        BoltReqLogsId: String,
        iCheckDetailID: String,
        strRequestBody: String,
        strResponseBody: String,
        strConnectRequestBody: String,
        strConnectResponseBody: String
    ) : MutableLiveData<DataWrapper<BoltModel>>? {
        Boltresponse = paymentRepository.sendBoltrequest(BoltReqLogsId,iCheckDetailID,strRequestBody,strResponseBody,strConnectRequestBody,strConnectResponseBody)
        return Boltresponse
    }
}