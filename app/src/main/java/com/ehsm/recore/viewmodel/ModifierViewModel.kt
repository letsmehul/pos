package com.ehsm.recore.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.model.ModifierMasterModel
import com.ehsm.recore.repository.ModifierRepository
import com.ehsm.recore.utils.NonNullMediatorLiveData

class ModifierViewModel constructor(private val repository: ModifierRepository) : ViewModel() {

    private var _hasDiscountLoaded = MutableLiveData<Boolean>()
    var selectedCartDiscount: ModifierMasterModel? = null

    var iCheckEmployeeId :Int =0

    val hasDiscountLoaded: LiveData<Boolean>
        get() = _hasDiscountLoaded

    private val _mDiscountForCashier = NonNullMediatorLiveData<List<ModifierMasterModel>>()
    var discountsList: LiveData<List<ModifierMasterModel>> = _mDiscountForCashier

    fun getDiscountList(iCheckEmployeeId : Int) {
        this.iCheckEmployeeId = iCheckEmployeeId
        //getDiscountAppliedByCashier()
    }
}
