package com.ehsm.recore.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.model.TerminalListResponseModel
import com.ehsm.recore.model.TerminalModel
import com.ehsm.recore.repository.TerminalRepository

class TerminalViewModel: ViewModel() {
    private var terminalListData: MutableLiveData<DataWrapper<TerminalListResponseModel>>? = null
    private val terminalRepository = TerminalRepository()

    var selectedTerminal : TerminalModel?=null

    // get terminal list from our server
    fun getTerminalList(
    ): MutableLiveData<DataWrapper<TerminalListResponseModel>>? {
        terminalListData = terminalRepository.apiCallTerminal()
        return terminalListData
    }
}