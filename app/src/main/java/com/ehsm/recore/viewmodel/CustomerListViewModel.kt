package com.kcspl.divyangapp.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.model.*
import com.ehsm.recore.utils.AppConstant
import com.ehsm.recore.utils.NonNullMediatorLiveData
import com.kcspl.divyangapp.repository.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext


class CustomerListViewModel(application:Application) : AndroidViewModel(application) {

    private var job = Job()
    private val coroutineContext: CoroutineContext get() = job + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    val isTaxIncluded: MutableLiveData<Boolean> = NonNullMediatorLiveData()


//    var isTaxIncluded :Boolean =true
    private var customerData: MutableLiveData<DataWrapper<CustomerResponseModel>>? = null
    private val customerListRepository = CustomerListRepository()
    private val customers : LiveData<List<CustomerModel>> =
        RecoreDB.getInstance(application)?.daoCustomerList()?.getCustomers() ?: NonNullMediatorLiveData()

    var selectedCustomer : CustomerModel? =null

    var walkInCustomer : CustomerModel ? =null

    private val _eventName = MutableLiveData<String>()
    var eventName :LiveData<String> = _eventName

    init {
         getCustomer("")
        _eventName.value = AppConstant.ORDER
        isTaxIncluded.value =true
    }



    fun getEventNameObserver() = eventName
    fun setEventName(eventType:String){
        _eventName.value = eventType;
    }

    fun resetEventName(){
        _eventName.value = AppConstant.ORDER
    }
    fun isSelectedCustomerEmpty() = selectedCustomer==null

    fun isSelectedCustomerIsWalkInCustomer() = selectedCustomer?.iIsWalkIn ==1

    fun getCustomer(search : String): MutableLiveData<DataWrapper<CustomerResponseModel>>? {
        customerData = customerListRepository.getCustomer(RecoreDB.getInstance(getApplication()),search)
        return customerData
    }

    fun getCustomerObserver(): LiveData<List<CustomerModel>> {
        return customers
    }

    /*fun resetSelectedCustomer(){
        selectedCustomer  =walkInCustomer
    }
*/


   /* fun insert(todo: Todo) = scope.launch(Dispatchers.IO) {
        todoRepository.insert(todo)
    }
    fun toggleDone(todo : Todo, checked : Boolean) = scope.launch(Dispatchers.IO){
        todo.done = checked
        todoRepository.update(todo)
    }*/
    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}