package com.kcspl.divyangapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.model.*
import com.google.gson.internal.bind.DateTypeAdapter
import com.kcspl.divyangapp.repository.*


class PlaceOrderViewModel : ViewModel() {

    private var itemData: MutableLiveData<DataWrapper<NewSaleandRefundModel>>? = null
    private var HolditemData: MutableLiveData<DataWrapper<ResponseModel>>? = null
    private val placeOrderRepository = PlaceOrderRepository()
    var tIsApplePay = false

    private var orderReceipt: MutableLiveData<DataWrapper<ResponseModel>>? = null
    private var updatepayment: MutableLiveData<DataWrapper<ResponseModel>>? = null


    fun placeOrder(
        iCheckDetailID: String,
        order_id: String,
        customer_id: Int,
        emp_id: Int,
        gross_total: Double,
        tax_total: Double,
        discount: Double,
        net_total: Double,
        json_item_data: String,
        json_payment_data: String,
        json_discount_data: String,
        customerEmail: String,
        tIsVoided: String = "0",
        tCheckTaxExempt:String,
        strPaymentType:String,
        strCheckStatus : String
    ): MutableLiveData<DataWrapper<NewSaleandRefundModel>>? {
        itemData = placeOrderRepository.placeOrder(
            iCheckDetailID,
            order_id,
            customer_id,
            emp_id,
            gross_total,
            tax_total,
            discount,
            net_total,
            json_item_data,
            json_payment_data,
            json_discount_data,
            customerEmail,
            tIsVoided,
            tCheckTaxExempt,
            tIsApplePay,
            strPaymentType,
            strCheckStatus
        )
        return itemData
    }

    fun Update_order_payment(
        iCheckDetailID: String,
        json_payment_data: String,
        strPaymentType: String
    ): MutableLiveData<DataWrapper<NewSaleandRefundModel>>? {
        itemData = placeOrderRepository.Update_order_payment(
            iCheckDetailID,
            json_payment_data,
            strPaymentType
        )
        return itemData
    }

    fun Update_Payment_type(
        iCheckDetailID: String,
        strPaymentType: String
    ): MutableLiveData<DataWrapper<ResponseModel>>? {
        updatepayment = placeOrderRepository.Update_Payment_type(
            iCheckDetailID,
            strPaymentType
        )
        return updatepayment
    }

    fun hold_order_customer_exist(
        iCustomerId: String
    ): MutableLiveData<DataWrapper<ResponseModel>>? {
        HolditemData = placeOrderRepository.hold_order_customer_exist(
            iCustomerId
        )
        return HolditemData
    }


    fun sendOrderReceipt(customerEmail : String,order_id:Int) : MutableLiveData<DataWrapper<ResponseModel>>? {
        orderReceipt = placeOrderRepository.sendOrderReceipt(customerEmail,order_id)
        return orderReceipt
    }
}