package com.kcspl.divyangapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.model.*
import com.kcspl.divyangapp.repository.*


class ForceCheckOutListViewModel : ViewModel() {

    private var orderData: MutableLiveData<DataWrapper<ForceCheckOutListResponseModel>>? = null
    private val orderHistoryRepository = ForceCheckOutListRepository()


    fun getForceCheckOutList(dbDatabase: RecoreDB?,search_date : String): MutableLiveData<DataWrapper<ForceCheckOutListResponseModel>>? {
        orderData = orderHistoryRepository.getForceCheckOutList(dbDatabase,search_date)
        return orderData
    }
}