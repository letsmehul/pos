package com.kcspl.divyangapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.model.*
import com.kcspl.divyangapp.repository.*


class OrderHistoryViewModel : ViewModel() {

    private var orderData: MutableLiveData<DataWrapper<OrderHistoryResponseModel>>? = null
    private val orderHistoryRepository = OrderHistoryRepository()


    fun getOrderHistory(dbDatabase: RecoreDB?,start_date : String,end_date : String,type:String, page_no : Int,search : String,strCheckStatus : String): MutableLiveData<DataWrapper<OrderHistoryResponseModel>>? {
        orderData = orderHistoryRepository.getOrderHistory(dbDatabase,start_date,end_date,type, page_no,search,strCheckStatus)
        return orderData
    }
}