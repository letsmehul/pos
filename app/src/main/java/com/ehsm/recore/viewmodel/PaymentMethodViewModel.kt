package com.kcspl.divyangapp.viewmodel

import android.os.AsyncTask
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.database.dao.EmployeeListDAO
import com.ehsm.recore.database.dao.PaymentListDAO
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.model.EmployeeList
import com.ehsm.recore.model.PaymentMethodModel
import com.ehsm.recore.model.PaymentMethodModelData
import com.kcspl.divyangapp.repository.PaymentMethodRepository


class PaymentMethodViewModel : ViewModel() {

    private var paymentData: MutableLiveData<DataWrapper<PaymentMethodModel>>? = null
    private val paymentMethodRepository = PaymentMethodRepository()


    fun getPayments(): MutableLiveData<DataWrapper<PaymentMethodModel>>? {
        paymentData = paymentMethodRepository.getPayments()
        return paymentData
    }

    /**
     * This class d for database operation
     */
    internal class PaymentDatabaseOperationTask(
        private val mList: List<PaymentMethodModelData>?,
        private val mDatabase: RecoreDB?,
        private val isFullDataSync: Boolean = false,
        private val callBackDataBase: (isOperationFinished: Boolean) -> Unit
    ) :
        AsyncTask<Void, Void, Void>() {
        private var paymentListDAO: PaymentListDAO? = null
        override fun onPreExecute() {
            super.onPreExecute()
            paymentListDAO = mDatabase!!.daoPaymentList()
        }

        override fun doInBackground(vararg voids: Void): Void? {
            paymentListDAO!!.deletePayments()
            if (mList != null && mList.isNotEmpty()) {
                paymentListDAO!!.insertPayment(mList)
            }
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            callBackDataBase(true)
        }
    }
}