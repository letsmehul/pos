package com.kcspl.divyangapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.model.*
import com.kcspl.divyangapp.repository.*


class CashDrawerManualReportViewModel : ViewModel() {

    private var cityData: MutableLiveData<DataWrapper<CashDrawerManualReportResponseModel>>? = null
    private val cashDrawerManualReportRepository = CashDrawerManualReportRepository()


    fun getCashDrawerManualReportData(dbDatabase: RecoreDB?,date : String): MutableLiveData<DataWrapper<CashDrawerManualReportResponseModel>>? {
        cityData = cashDrawerManualReportRepository.getCashDrawerManualReportData(dbDatabase,date)
        return cityData
    }
}