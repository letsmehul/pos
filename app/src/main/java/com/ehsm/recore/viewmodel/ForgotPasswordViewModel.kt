package com.kcspl.divyangapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.model.LoginResponseModel
import com.ehsm.recore.model.ResponseModel
import com.kcspl.divyangapp.repository.ForgotPasswordRepository
import com.kcspl.divyangapp.repository.LoginRepository


class ForgotPasswordViewModel : ViewModel() {

    private var loginData: MutableLiveData<DataWrapper<ResponseModel>>? = null
    private val forgotPasswordRepository = ForgotPasswordRepository()


    fun forgotPassword(email: String): MutableLiveData<DataWrapper<ResponseModel>>? {
        loginData = forgotPasswordRepository.forgotPassword(email)
        return loginData
    }
}