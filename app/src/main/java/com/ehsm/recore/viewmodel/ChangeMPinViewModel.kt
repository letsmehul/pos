package com.kcspl.divyangapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.model.LoginResponseModel
import com.ehsm.recore.model.ResponseModel
import com.kcspl.divyangapp.repository.ChangeMPinRepository
import com.kcspl.divyangapp.repository.ForgotPasswordRepository
import com.kcspl.divyangapp.repository.LoginRepository


class ChangeMPinViewModel : ViewModel() {

    private var loginData: MutableLiveData<DataWrapper<ResponseModel>>? = null
    private val changeMPinRepository = ChangeMPinRepository()


    fun changeMpin(emp_id: Int,mpin : Int, iMpin_old : Int): MutableLiveData<DataWrapper<ResponseModel>>? {
        loginData = changeMPinRepository.changeMpin(emp_id, mpin, iMpin_old)
        return loginData
    }
}