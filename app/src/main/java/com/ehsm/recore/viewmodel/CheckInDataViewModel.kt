package com.kcspl.divyangapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.model.CheckInDataResponseModel
import com.ehsm.recore.model.DataWrapper
import com.kcspl.divyangapp.repository.CheckInDataRepository

class CheckInDataViewModel : ViewModel() {

    private var checkInData: MutableLiveData<DataWrapper<CheckInDataResponseModel>>? = null
    private val checkInDataRepository = CheckInDataRepository()


    fun getCheckInData(emp_id: Int): MutableLiveData<DataWrapper<CheckInDataResponseModel>>? {
        checkInData = checkInDataRepository.getCheckInData(emp_id)
        return checkInData
    }
}