package com.kcspl.divyangapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.model.LoginResponseModel
import com.ehsm.recore.model.VerifyMPin
import com.ehsm.recore.model.VerifyMPinResponseModel
import com.kcspl.divyangapp.repository.LoginRepository
import com.kcspl.divyangapp.repository.VerifyMPinRepository


class VerifyMPinViewModel : ViewModel() {

    private var mpinData: MutableLiveData<DataWrapper<VerifyMPinResponseModel>>? = null
    private val verifyMPinRepository = VerifyMPinRepository()


    fun verifyMPin(mpin: String): MutableLiveData<DataWrapper<VerifyMPinResponseModel>>? {
        mpinData = verifyMPinRepository.verifyMPin(mpin)
        return mpinData
    }
}