package com.kcspl.divyangapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.model.*
import com.kcspl.divyangapp.repository.CategoryListRepository
import com.kcspl.divyangapp.repository.LoginRepository
import com.kcspl.divyangapp.repository.StateListRepository
import com.kcspl.divyangapp.repository.VerifyMPinRepository


class StateListViewModel : ViewModel() {

    private var stateData: MutableLiveData<DataWrapper<StateListResponseModel>>? = null
    private val stateListRepository = StateListRepository()


    fun getStates(dbDatabase: RecoreDB?): MutableLiveData<DataWrapper<StateListResponseModel>>? {
        stateData = stateListRepository.getStates(dbDatabase)
        return stateData
    }
}