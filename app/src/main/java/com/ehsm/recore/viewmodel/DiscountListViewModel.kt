package com.ehsm.recore.viewmodel

import android.text.TextUtils
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ehsm.recore.Recore
import com.ehsm.recore.model.*
import com.ehsm.recore.repository.DiscountListRepository
import com.ehsm.recore.utils.AppConstant
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CV
import com.ehsm.recore.utils.NonNullMediatorLiveData
import kotlinx.coroutines.*

class DiscountListViewModel constructor(private val repository: DiscountListRepository) :
    ViewModel() {

    private var _hasDiscountLoaded = MutableLiveData<Boolean>()
    var selectedCartDiscount: DiscountMaster? = null

    var iCheckEmployeeId: Int = 0

    val hasDiscountLoaded: LiveData<Boolean>
        get() = _hasDiscountLoaded

    init {
        iCheckEmployeeId = CM.getSp(Recore.getApp()?.baseContext, CV.EMP_ID, 0) as Int
    }

    private val _mDiscountForCashier = NonNullMediatorLiveData<List<DiscountMaster>>()
    var discountsList: LiveData<List<DiscountMaster>> = _mDiscountForCashier

    fun getDiscountList(iCheckEmployeeId: Int) {
        this.iCheckEmployeeId = iCheckEmployeeId
        getDiscountAppliedByCashier()
    }

    fun deleteAppliedDiscountOnCart(orderId: String) =
        viewModelScope.launch { repository.deleteAppliedDiscountOnCart(orderId) }

    fun isCartLevelDiscountApplied(orderId: String) = repository.isCartLevelDiscountApplied(orderId)

    fun appliedSimpleDiscountItem(orderId: String) = repository.appliedSimpleDiscountItem(orderId)

    fun getAppliedDiscountOnCart(orderId: String) = repository.getAppliedDiscountOnCart(orderId)

    fun insertAppliedDiscountOnCart(orderId: String, appliedDiscountAmount: Float) {
        selectedCartDiscount?.let { selectedDiscount ->

            val model = DiscountApplyModel(
                orderId,
                selectedDiscount.discountId,
                "",
                selectedDiscount.name,
                selectedDiscount.discountType ?: "",
                selectedDiscount.strType,
                selectedDiscount.dblValue,
                appliedDiscountAmount,
                selectedDiscount.strAppliedTo ?: "",
                selectedDiscount.minCartAmount
            )
            repository.insertAppliedCartDiscount(model)
        }
    }

    fun isSameUserOrStoreAdmin(): Boolean {
        val lastLoginRole = CM.getSp(Recore.getApp()?.baseContext, CV.EMP_ROLE, "")
        return if (lastLoginRole == AppConstant.ROLE_ID_ADMIN) {
            true
        } else {
            val lastLoginId = CM.getSp(Recore.getApp()?.baseContext, CV.EMP_ID, 0)
            lastLoginId == iCheckEmployeeId
        }
    }

    fun sychWithServer(
        iCheckEmployeeId: Int,
        discountListcallBack: (isDiscountSuccess: Boolean, message: String?) -> Unit
    ) {
        this.iCheckEmployeeId = iCheckEmployeeId
        repository.deleteDiscount()
        repository.getDiscountsFromServer(iCheckEmployeeId) { discountModel, error ->
            if (TextUtils.isEmpty(error)) {
                viewModelScope.launch {
                    discountModel?.simple_discount?.let {
                        repository.saveSimpleDiscount(it)
                    }
                    discountModel?.coupon_discount?.let {
                        repository.saveCouponDiscount(it)
                        for (item: DiscountMaster in it) {
                            item.discountProducts?.let { it1 ->
                                repository.saveSalesDiscountProducts(
                                    it1
                                )
                            }
                        }
                    }
                    discountModel?.sale_discount?.let {
                        if (it.isNotEmpty()) {
                            repository.saveSalesDiscount(it)
                            for (item: DiscountMaster in it) {
                                item.discountProducts?.let { it1 ->
                                    repository.saveSalesDiscountProducts(
                                        it1
                                    )
                                }
                            }
                        }
                    }
                    discountModel?.bogo_discount?.let {
                        if (it.isNotEmpty()) {
                            repository.saveBogoDiscount(it)
                            for (item: DiscountMaster in it) {
                                item.discountBuyProducts?.let { it1 ->
                                    repository.saveBogoDiscountBuyItems(
                                        it1
                                    )
                                }
                                item.discountGetProducts?.let { it1 ->
                                    repository.saveBogoDiscountGetItems(
                                        it1
                                    )
                                }
                            }
                        }
                    }

                    _mDiscountForCashier.value = repository.getCashierDiscount()
                    _hasDiscountLoaded.value = true
                    discountListcallBack(true, "Success")
                }
            } else {
                discountListcallBack(true, error)
            }
        }
    }

    private fun getDiscountAppliedByCashier() {

        viewModelScope.launch {
            val list = repository.getCashierDiscount()
            if (list.isNotEmpty()) {
                _mDiscountForCashier.value = list
                _hasDiscountLoaded.value = true
            }
        }
        if (hasDiscountLoaded.value != true) {
            repository.getDiscountsFromServer(iCheckEmployeeId) { discountModel, error ->
                if (TextUtils.isEmpty(error)) {
                    viewModelScope.launch {
                        discountModel?.simple_discount?.let {
                            repository.saveSimpleDiscount(it)
                        }
                        discountModel?.coupon_discount?.let {
                            repository.saveCouponDiscount(it)
                        }
                        discountModel?.sale_discount?.let {
                            if (it.isNotEmpty()) {
                                repository.saveSalesDiscount(it)
                                for (item: DiscountMaster in it) {
                                    item.discountProducts?.let { it1 ->
                                        repository.saveSalesDiscountProducts(
                                            it1
                                        )
                                    }
                                }
                            }
                        }
                        discountModel?.bogo_discount?.let {
                            if (it.isNotEmpty()) {
                                repository.saveBogoDiscount(it)
                                for (item: DiscountMaster in it) {
                                    item.discountBuyProducts?.let { it1 ->
                                        repository.saveBogoDiscountBuyItems(
                                            it1
                                        )
                                    }
                                    item.discountGetProducts?.let { it1 ->
                                        repository.saveBogoDiscountGetItems(
                                            it1
                                        )
                                    }
                                }
                            }
                        }

                        _mDiscountForCashier.value = repository.getCashierDiscount()
                        _hasDiscountLoaded.value = true
                    }
                }
            }
        }

    }

    suspend fun validateAutoApplyDiscount(productId: Int, itemQty: Int): List<DiscountAndProduct> {
        val list = getApplicableDiscountOnItem(productId, itemQty).await()
        Log.d("List", "Size os $list.size")
        return list as ArrayList
    }

    suspend fun validateAutoApplyDiscountOnItemGroup(discountId: Int): List<DiscountAndProduct> {
        val list = getApplicableDiscountOnItemGroup(discountId).await()
        Log.d("List", "Size os $list.size")
        return list as ArrayList
    }

    suspend fun getDiscountIds(): List<Long> {
        val list = getAllDiscountIds().await()
        Log.d("List", "Size os $list.size")
        return list as ArrayList
    }

     fun getMinQtyFromDiscountId(iDiscountId:Int): Int{
         return repository.getMinQtyFromDiscountId(iDiscountId)
    }


    fun getApplicableDiscountOnItem(
        productId: Int,
        itemQty: Int
    ): Deferred<List<DiscountAndProduct>> = viewModelScope.async {
        repository.getApplicableDiscountOnItem(productId, itemQty)
    }

    fun getApplicableDiscountOnItemGroup(
        discountId: Int
    ): Deferred<List<DiscountAndProduct>> = viewModelScope.async {
        repository.getApplicableDiscountOnItemGroup(discountId)
    }

    fun getAllDiscountIds(): Deferred<List<Long>> = viewModelScope.async {
        repository.getAllDiscountIds()
    }


    suspend fun getFreeDiscountProducts(productId: Int): List<DiscountAndProduct> {
        val list = getFreeDiscountProductsWithoutNull(productId).await()
        Log.d("List", "Size os $list.size")
        return list as ArrayList
    }

    fun getFreeDiscountProductsWithoutNull(iDiscountId: Int): Deferred<List<DiscountAndProduct>> =
        viewModelScope.async {
            repository.getFreeDiscountProducts(iDiscountId)
        }


    fun checkDiscountMappingItemIsAvailable(pid: Long,dis_id : Long) : Boolean{
        return repository.checkDiscountMappingItemIsAvailable(pid, dis_id)
    }


    fun getDiscountMasterData(iDiscountId: Int) : DiscountMaster{
        return repository.getDiscountMasterData(iDiscountId)!!
    }

    fun checkProductCouponDetail(pid: Long,dis_id : Long) : DiscountAndProduct? {
        return repository.checkProductCouponDetail(pid, dis_id)
    }


    /* fun getSalesBogoDiscountList(iCheckEmployeeId : Int): MutableLiveData<DataWrapper<DiscountResponse>>? {
         discountData = discountListRepository.getSalesBogoDiscountList(iCheckEmployeeId)
         return discountData
     }*/

}
