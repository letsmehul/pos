package com.kcspl.divyangapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.model.*
import com.kcspl.divyangapp.repository.*


class BreakViewModel : ViewModel() {

    private val breakActionRepository = BreakRepository()
    private var breakActionData: MutableLiveData<DataWrapper<BreakInOutResponse>>? = null
    private var breakInOutData: MutableLiveData<DataWrapper<ResponseModel>>? = null

    fun getBreakInBreakOut(
        emp_id: Int
    ): MutableLiveData<DataWrapper<BreakInOutResponse>>? {
        breakActionData = breakActionRepository.getBreak(emp_id)
        return breakActionData
    }
    fun breakInOutData(date : String, type :String,emp_id :Int): MutableLiveData<DataWrapper<ResponseModel>>? {
        breakInOutData = breakActionRepository.breakInOutData(date,type,emp_id)
        return breakInOutData
    }
}