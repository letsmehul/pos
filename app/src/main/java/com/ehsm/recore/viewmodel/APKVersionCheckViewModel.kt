package com.ehsm.recore.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.model.ResponseModel
import com.ehsm.recore.repository.APKVersionRepository

class APKVersionCheckViewModel: ViewModel() {
    private var mAPKVersionCheckData: MutableLiveData<DataWrapper<ResponseModel>>? = null
    private val mAPKVersionRepository = APKVersionRepository()

    fun getAPKVersion(current_version_code: String): MutableLiveData<DataWrapper<ResponseModel>>? {
        mAPKVersionCheckData = mAPKVersionRepository.apiCallAPKVersionCheck(current_version_code)
        return mAPKVersionCheckData
    }
}