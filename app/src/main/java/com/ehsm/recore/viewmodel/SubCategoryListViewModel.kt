package com.kcspl.divyangapp.viewmodel

import android.os.AsyncTask
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.R
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.database.dao.CategoryListDAO
import com.ehsm.recore.database.dao.SubCategoryListDAO
import com.ehsm.recore.model.*
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CV
import com.ehsm.recore.utils.SyncOperation
import com.kcspl.divyangapp.repository.SubCategoryListRepository


class SubCategoryListViewModel : ViewModel() {

    private var mpinData: MutableLiveData<DataWrapper<SubCategoryListResponseModel>>? = null
    private val subCategoryListRepository = SubCategoryListRepository()


    fun getsubCategories(
        dbDatabase: RecoreDB?,
        category_id: String,
        subCatSyncDateTime: String
    ): MutableLiveData<DataWrapper<SubCategoryListResponseModel>>? {
        mpinData =
            subCategoryListRepository.getSubCategories(dbDatabase, category_id, subCatSyncDateTime)
        return mpinData
    }

    /**
     * This class for database operation
     */
    internal class SubCategoryDataBaseOperationTask(
        private val mList: List<SubCategoryModel>?,
        private val mDatabase: RecoreDB?,
        private val isFullDataSync: Boolean,
        private val callBackDataBase: (isOperationFinished: Boolean) -> Unit
    ) :
        AsyncTask<Void, Void, Void>() {
        private var subCategoryListDAO: SubCategoryListDAO? = null
        override fun onPreExecute() {
            super.onPreExecute()
            subCategoryListDAO = mDatabase!!.daoSubCategoryList()
        }
        override fun doInBackground(vararg voids: Void): Void? {
            if (mList != null && mList.isNotEmpty()) {
                if (isFullDataSync) {
                    subCategoryListDAO!!.deleteSubCategories()
                }
                subCategoryListDAO!!.insertSubCategory(mList)
            }
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            callBackDataBase(true)
        }
    }
}