package com.ehsm.recore.viewmodel

import android.os.AsyncTask
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.R
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.database.dao.CategoryListDAO
import com.ehsm.recore.model.CategoryListResponseModel
import com.ehsm.recore.model.CategoryModel
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CV
import com.ehsm.recore.utils.SyncOperation
import com.kcspl.divyangapp.repository.CategoryListRepository


class CategoryListViewModel : ViewModel() {

    private var mpinData: MutableLiveData<DataWrapper<CategoryListResponseModel>>? = null
    private val categoryListRepository = CategoryListRepository()

    fun getCategories(dbDatabase: RecoreDB?, catSyncDateTime: String): MutableLiveData<DataWrapper<CategoryListResponseModel>>? {
        mpinData = categoryListRepository.getCategories(dbDatabase,catSyncDateTime)
        return mpinData
    }

    /**
     * This class for database operation
     */
    internal class CategoryDataBaseOperationTask(
        private val mList: List<CategoryModel>?,
        private val mDatabase: RecoreDB?,
        private val isFullDataSync: Boolean,
        private val callBackDataBase: (isOperationFinished: Boolean) -> Unit
    ) :
        AsyncTask<Void, Void, Void>() {
        private var categoryListDAO: CategoryListDAO? = null
        override fun onPreExecute() {
            super.onPreExecute()
            categoryListDAO = mDatabase!!.daoCategoryList()
        }
        override fun doInBackground(vararg voids: Void): Void? {
            if (mList != null && mList.isNotEmpty()) {
                if (isFullDataSync) {
                    categoryListDAO!!.deleteCategories()
                }
                categoryListDAO!!.insertCategory(mList)
            }
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            callBackDataBase(true)
        }
    }
}