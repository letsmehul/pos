package com.kcspl.divyangapp.viewmodel

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.os.AsyncTask
import android.text.TextUtils
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.Recore
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.model.LoginResponseModel
import com.ehsm.recore.utils.AppConstant
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CV
import com.ehsm.recore.utils.SyncOperation
import com.kcspl.divyangapp.repository.LoginRepository
import java.io.*
import java.net.URL
import java.net.URLConnection


class LoginViewModel : ViewModel() {

    private var loginData: MutableLiveData<DataWrapper<LoginResponseModel>>? = null
    private val loginRepository = LoginRepository()


    fun logIn(
        username: String,
        password: String,
        fcm: String,
        mac_address: String
    ): MutableLiveData<DataWrapper<LoginResponseModel>>? {
        loginData = loginRepository.logIn(username, password, fcm, mac_address)
        return loginData
    }

    fun logInData(): MutableLiveData<DataWrapper<LoginResponseModel>>? {
        loginData = loginRepository.logInData()
        return loginData
    }

    fun loginDataOperation(
        mActivity: Activity,
        loginResponseModel: LoginResponseModel,
        callBackOperation: (isOperationFinished: Boolean) -> Unit
    ) {

        CM.setSp(mActivity, CV.API_KEY, loginResponseModel.data.strApiKey)
        CM.setSp(mActivity, CV.POS_NAME, loginResponseModel.data.posName)
        CM.setSp(mActivity, CV.STORE_ID, loginResponseModel.data.store_id)
        CM.setSp(mActivity, CV.POS_ID, loginResponseModel.data.iUserLogins)
        //   CM.setSp(this, CV.CURRENCY_CODE, loginResponseModel.data.strCurrencyCode)
        CM.setSp(mActivity, CV.PAYMENT_TYPE, loginResponseModel.data.strPaymentMethod)
        CM.setSp(mActivity, CV.LOCK_TIMER, loginResponseModel.data.strScreenLockTimer)
        CM.setSp(
            mActivity,
            CV.IS_ADMIN_APPROVAL_REQUIRED_FOR_TRANSACTION,
            loginResponseModel.data.tIsTransactionalRights
        )
        CM.setSp(mActivity, CV.SIGNATURE_AMOUNT, loginResponseModel.data.dblSignatureAmount)

        CM.setSp(mActivity, CV.STRSOCIALMEDIA, loginResponseModel.data.strSocialMedia)
        loginResponseModel.data.strFacebook?.let { CM.setSp(mActivity, CV.STRFACEBOOK, it) }
        loginResponseModel.data.strInstagram?.let { CM.setSp(mActivity, CV.STRINSTAGRAM, it) }
        loginResponseModel.data.strTwitter?.let { CM.setSp(mActivity, CV.STRTWITTER, it) }
        loginResponseModel.data.strYoutube?.let { CM.setSp(mActivity, CV.STRYOUTUBE, it) }

        CM.setSp(
            mActivity,
            CV.CURRENCY_CODE,
            loginResponseModel.data.strCurrencysymbol
        )

        CM.setSp(mActivity, CV.STORE_NAME, loginResponseModel.data.strStoreName)
        CM.setSp(
            mActivity,
            CV.STORE_ADD_LINE1,
            loginResponseModel.data.strAddressLine1
        )
        CM.setSp(
            mActivity,
            CV.STORE_ADD_LINE2,
            loginResponseModel.data.strAddressLine2
        )
        CM.setSp(mActivity, CV.STORE_TEL_NUM, loginResponseModel.data.strPhone)
        CM.setSp(mActivity, CV.STORE_LOGO_URL, loginResponseModel.data.strLogo)
        CM.setSp(
            mActivity,
            CV.SQUARE_LOGO_PATH,
            loginResponseModel.data.strSquareImageLogo
        )

        if (!TextUtils.isEmpty(loginResponseModel.data.strBoltMerchantId)) {
            Recore.getApp()?.applicationContext?.let {
                val sharedPref = CM.getEncryptedSharedPreferences(
                    it
                )
                sharedPref.edit().putString(
                    AppConstant.MERCHANTID,
                    loginResponseModel.data.strBoltMerchantId
                ).commit()
            }

        }
        if (!TextUtils.isEmpty(loginResponseModel.data.strBoltAuthKey)) {
            Recore.getApp()?.applicationContext?.let {
                val sharedPref = CM.getEncryptedSharedPreferences(
                    it
                )
                sharedPref.edit().putString(
                    AppConstant.HEADER_Authorization,
                    loginResponseModel.data.strBoltAuthKey
                ).commit()
            }
        }
        CM.setSp(
            mActivity,
            CV.PROMO_CONTENT_TYPE,
            loginResponseModel.data.strPromotionalAdsType
        )
        CM.setSp(
            mActivity,
            CV.PROMO_CONTENT_PATH,
            loginResponseModel.data.strPromotionalAds
        )
        CM.loadBitmaptoStore(
            loginResponseModel.data.strLogo,
            CV.STORE_LOGO_IMAGE_NAME,
            CV.STORE_LOGO_LOCAL_PATH,
            mActivity
        )
        CM.loadBitmaptoStore(
            loginResponseModel.data.strFullScreen,
            CV.FULL_SCREEN_IMAGE_NAME,
            CV.FULL_SCREEN_LOCAL_PATH,
            mActivity
        )
        CM.loadBitmaptoStore(
            loginResponseModel.data.strSquareImageLogo,
            CV.SQUARE_LOGO_IMAGE_NAME,
            CV.SQUARE_LOGO_LOCAL_PATH,
            mActivity
        )

        CM.setSp(
            mActivity,
            CV.IS_PAY_IN_OUT_ENABLE,
            1 == loginResponseModel.data.iPayInOut
        )
        if (loginResponseModel.data.strPromotionalAdsType.equals("Video")) {
            DownloadFromUrlTask(
                loginResponseModel.data.strPromotionalAds,
                CV.PROMO_VIDEO_PATH,
                mActivity.applicationContext
            ).execute()
        }
        callBackOperation(true)
    }

    class DownloadFromUrlTask(
        private val VideonURL: String?,
        private val fileName: String,
        private val mActivity: Context
    ) :
        AsyncTask<Int, Int, String>() {

        override fun doInBackground(vararg params: Int?): String {
            CM.DownloadFromUrl(mActivity, VideonURL, fileName)

            return "Task Completed"
        }

        override fun onPostExecute(result: String) {
            super.onPostExecute(result)
        }
    }

}