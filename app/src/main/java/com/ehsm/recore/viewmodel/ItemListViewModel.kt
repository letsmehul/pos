package com.kcspl.divyangapp.viewmodel

import android.os.AsyncTask
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.R
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.database.dao.IngredientListDAO
import com.ehsm.recore.database.dao.ItemListDAO
import com.ehsm.recore.database.dao.ModifierGroupListDAO
import com.ehsm.recore.database.dao.PreModifierListDAO
import com.ehsm.recore.model.*
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CV
import com.ehsm.recore.utils.SyncOperation
import com.kcspl.divyangapp.repository.*


class ItemListViewModel : ViewModel() {

    private var itemData: MutableLiveData<DataWrapper<ItemListResponseModel>>? = null
    private val itemListRepository = ItemListRepository()


    fun getItems(
        dbDatabase: RecoreDB?,
        category_id: String,
        prodSyncDateTime: String
    ): MutableLiveData<DataWrapper<ItemListResponseModel>>? {
        itemData = itemListRepository.getItemList(dbDatabase, category_id, prodSyncDateTime)
        return itemData
    }

    /**
     * This class for database operation
     */
     class ProductItemDataBaseOperationTask(
        private val mList: List<ItemModel>?,
        private val mDatabase: RecoreDB?,
        private val isFullDataSync: Boolean,
        private val callBackDataBase: (isOperationFinished: Boolean,progressUpdateValue: Int) -> Unit
    ) :
        AsyncTask<Int, Int, String>() {
        private var daoItemList: ItemListDAO? = null
        private var daoModifierList: ModifierGroupListDAO? = null
        private var daoPreModifierList: PreModifierListDAO? = null
        private var daoIngredientList: IngredientListDAO? = null
        var percentage = 0
        override fun onPreExecute() {
            super.onPreExecute()
            daoItemList = mDatabase!!.daoItemList()
            daoModifierList = mDatabase.daoModifierGroupList()
            daoPreModifierList = mDatabase.daoPreModifierList()
            daoIngredientList = mDatabase.daoIngredientList()
        }

        override fun doInBackground(vararg params: Int?): String {
            if (mList != null && mList.isNotEmpty()) {
                if (isFullDataSync) {
                    daoItemList!!.deleteProducts()
                    daoItemList!!.insertItem(mList)
                    daoModifierList!!.deleteModifier()
                    daoPreModifierList!!.deletePreModifier()
                    daoIngredientList!!.deleteIngredient()
                } else {
                    daoItemList!!.insertItem(mList)
                    for (i in mList.indices) {
                        val item = mList.get(i)
                        daoModifierList!!.deleteModifierGroup(item.iProductID)
                        daoPreModifierList!!.deletePreModifierByProductId(item.iProductID)
                        daoIngredientList!!.deleteIngredientByProductId(item.iProductID)
                        daoIngredientList!!.deleteModifierMapperByProductId(item.iProductID)
                    }
                }

                for (i in mList.indices) {
                    val item = mList.get(i)
                    daoModifierList!!.insertModifierGroup(item.modifierArr!!)
                    for (j in item.modifierArr!!.indices) {
                        val modifierGroup = item.modifierArr!![j]
                        daoPreModifierList!!.insertPreModifier(modifierGroup.PreModifier!!)
                        val preModifierGroup = item.modifierArr!!.get(j).PreModifier!!

                        val inGradientList = item.modifierArr!!.get(j).ModifierGroupOption!!
                        daoIngredientList!!.insertIngredient(inGradientList)
                        for (l in preModifierGroup.indices) {
                            val premodifier = preModifierGroup[l]
                            for (k in inGradientList.indices) {
                                val inGradient = inGradientList[k]
                                daoIngredientList?.insertModiferMapper(
                                    ModifierMapper(
                                        modifierGroup.iModifierGroupId,
                                        inGradient.iModifierGroupOptionId,
                                        item.iProductID,
                                        premodifier.iPreModifierId
                                    )
                                )
                            }
                        }

                    }
                    try {
                        if (percentage != (i * 100) / mList.size) {
                            percentage = (i * 100) / mList.size
                            publishProgress(percentage)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
            return "Task Completed"
        }

        override fun onProgressUpdate(vararg values: Int?) {
            super.onProgressUpdate(*values)
            var display = values[0]!!
            callBackDataBase(false,display)
        }

        override fun onPostExecute(result: String) {
            super.onPostExecute(result)
            callBackDataBase(true,100)
        }
    }
}