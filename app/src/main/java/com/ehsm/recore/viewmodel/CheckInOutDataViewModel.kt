package com.kcspl.divyangapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.model.ClockOutDataResponseModel
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.model.LoginResponseModel
import com.ehsm.recore.model.ResponseModel
import com.kcspl.divyangapp.repository.ChangeMPinRepository
import com.kcspl.divyangapp.repository.CheckInOutDataRepository
import com.kcspl.divyangapp.repository.ForgotPasswordRepository
import com.kcspl.divyangapp.repository.LoginRepository


class CheckInOutDataViewModel : ViewModel() {

    private var loginData: MutableLiveData<DataWrapper<ClockOutDataResponseModel>>? = null
    private val checkInOutDataRepository = CheckInOutDataRepository()


    fun checkInOutData(date : String, amount : Double,highcash : Double,shortcash : Double,type :String,emp_id :Int, remarks : String,cash_in_drawer : Double,user_id :Int, isForcecheckOut :Int,bitCashCollectByAdmin : Int): MutableLiveData<DataWrapper<ClockOutDataResponseModel>>? {
        loginData = checkInOutDataRepository.checkInOutData(date, amount,highcash,shortcash,type,emp_id, remarks,cash_in_drawer,user_id,isForcecheckOut,bitCashCollectByAdmin)
        return loginData
    }
}