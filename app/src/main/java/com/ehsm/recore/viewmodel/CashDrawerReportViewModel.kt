package com.kcspl.divyangapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.model.*
import com.kcspl.divyangapp.repository.*


class CashDrawerReportViewModel : ViewModel() {

    private var cityData: MutableLiveData<DataWrapper<CashDrawerReportResponseModel>>? = null
    private val cashDrawerReportRepository = CashDrawerReportRepository()


    fun getCashDrawerReportData(dbDatabase: RecoreDB?,date : String): MutableLiveData<DataWrapper<CashDrawerReportResponseModel>>? {
        cityData = cashDrawerReportRepository.getCashDrawerReportData(dbDatabase,date)
        return cityData
    }
}