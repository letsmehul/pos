package com.ehsm.recore.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.ehsm.recore.database.dao.ItemListDAO
import com.ehsm.recore.model.DiscountAndProduct
import com.ehsm.recore.model.ItemModel
import com.ehsm.recore.utils.BaseViewModel
import com.ehsm.recore.utils.NonNullMediatorLiveData
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class SearchViewModel constructor(private val itemListDAO: ItemListDAO) : BaseViewModel() {

    private val _mProducts = NonNullMediatorLiveData<List<ItemModel>>()
    var products: LiveData<List<ItemModel>> = _mProducts

    private val _mBestSellerProducts = NonNullMediatorLiveData<List<ItemModel>>()
    var bestSelllerProducts: LiveData<List<ItemModel>> = _mBestSellerProducts

    private val _mProduct = NonNullMediatorLiveData<ItemModel>()
    var product: LiveData<ItemModel> = _mProduct

    private val LIMIT = 100
    var OFFSET = 0
    var searchText = ""
    //    init {
//        getBestsellerProduct()
//    }
    var isBestSellerProduct = true
    var isSearching = false;
    fun searchProducts() = launch(coroutineContext) {
        _mProducts.postValue(itemListDAO.getSearchItems(searchText, LIMIT, OFFSET))
    }

    fun getBestsellerProduct() = launch(coroutineContext) {
        isBestSellerProduct = true
        _mBestSellerProducts.postValue(itemListDAO.getBestSellerProduct())
    }

    fun resetProductsList() {
        products = NonNullMediatorLiveData()
    }

    fun resetBestSellerProductsList() {
        isBestSellerProduct = false
        bestSelllerProducts = NonNullMediatorLiveData()
    }

    fun resetProduct() {
        isSearching = false
        product = NonNullMediatorLiveData()
    }

    fun searchProductBarcode(barcodeString: String): ItemModel {
        Log.d("BARCODE_SCAN","searchProductBarcode :: "+barcodeString)
        isSearching = true
        return itemListDAO.getSearchProductByBarcode(barcodeString)
        //_mProduct.postValue(itemListDAO.getSearchProductByBarcode(barcodeString))
    }


    suspend fun getProduct(productId: Long) : ItemModel {
        return getFreeDiscountProductsWithoutNull(productId.toInt()).await()
    }

    fun getFreeDiscountProductsWithoutNull(productId:Int) : Deferred<ItemModel> =viewModelScope.async {
        itemListDAO.getItem(productId)
    }
}