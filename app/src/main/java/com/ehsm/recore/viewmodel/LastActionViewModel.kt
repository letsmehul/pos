package com.kcspl.divyangapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.model.*
import com.kcspl.divyangapp.repository.*


class LastActionViewModel : ViewModel() {

    private var itemData: MutableLiveData<DataWrapper<LastActionResponseModel>>? = null
    private val lastActionRepository = LastActionRepository()


    fun getLastAction(emp_id : Int
    ): MutableLiveData<DataWrapper<LastActionResponseModel>>? {
        itemData = lastActionRepository.getLastAction(emp_id)
        return itemData
    }
}