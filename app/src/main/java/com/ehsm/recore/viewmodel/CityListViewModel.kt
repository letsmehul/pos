package com.kcspl.divyangapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.model.*
import com.kcspl.divyangapp.repository.*


class CityListViewModel : ViewModel() {

    private var cityData: MutableLiveData<DataWrapper<CityListResponseModel>>? = null
    private val stateListRepository = CityListRepository()


    fun getCity(dbDatabase: RecoreDB?,state_id : Int): MutableLiveData<DataWrapper<CityListResponseModel>>? {
        cityData = stateListRepository.getCityList(dbDatabase,state_id)
        return cityData
    }
}