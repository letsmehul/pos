package com.kcspl.divyangapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.model.CheckInDataResponseModel
import com.ehsm.recore.model.CheckOutDataResponseModel
import com.ehsm.recore.model.DataWrapper
import com.kcspl.divyangapp.repository.CheckInDataRepository
import com.kcspl.divyangapp.repository.CheckOutDataRepository

class CheckOutDataViewModel : ViewModel() {

    private var checkOutData: MutableLiveData<DataWrapper<CheckOutDataResponseModel>>? = null
    private val checkOutDataRepository = CheckOutDataRepository()


    fun getCheckOutData(emp_id: Int): MutableLiveData<DataWrapper<CheckOutDataResponseModel>>? {
        checkOutData = checkOutDataRepository.getCheckOutData(emp_id)
        return checkOutData
    }
}