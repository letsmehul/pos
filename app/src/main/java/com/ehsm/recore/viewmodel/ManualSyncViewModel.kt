package com.kcspl.divyangapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.model.*
import com.kcspl.divyangapp.repository.*


class ManualSyncViewModel : ViewModel() {

    private var itemData: MutableLiveData<DataWrapper<ManualSyncResponseModel>>? = null
    private val itemListRepository = ManualSyncRepository()


    fun manualsync(dbDatabase: RecoreDB?,category_id : Int): MutableLiveData<DataWrapper<ManualSyncResponseModel>>? {
        itemData = itemListRepository.manualsync(dbDatabase,category_id)
        return itemData
    }
}