package com.kcspl.divyangapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.model.*
import com.kcspl.divyangapp.repository.*


class PaymentRefundViewModel : ViewModel() {

    private var itemData: MutableLiveData<DataWrapper<NewSaleandRefundModel>>? = null
    private val paymentRefundRepository = PaymentRefundRepository()


    fun paymentRefund(
        amount: Double,
        date: String,
        id: String,
        refund_type: String,
        merchantId:String,
        retref:String,
        reason : String,
        status : String,
        emp_id : Int,
        item_data : String,
        taxAmount: Double,
        dblDiscountAmount: Double
    ): MutableLiveData<DataWrapper<NewSaleandRefundModel>>? {
        itemData = paymentRefundRepository.paymentRefund(
            amount, date, id, refund_type,merchantId,retref, reason, status, emp_id, item_data,taxAmount, dblDiscountAmount
        )
        return itemData
    }
}