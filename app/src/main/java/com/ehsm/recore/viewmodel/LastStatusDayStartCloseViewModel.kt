package com.kcspl.divyangapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.model.*
import com.kcspl.divyangapp.repository.*


class LastStatusDayStartCloseViewModel : ViewModel() {

    private var itemData: MutableLiveData<DataWrapper<POSLastStatusResponseModel>>? = null
    private val lastActionRepository = LastStatusDayStartCloseRepository()


    fun getLastActionDayStartClose(): MutableLiveData<DataWrapper<POSLastStatusResponseModel>>? {
        itemData = lastActionRepository.getLastActionDayStartClose()
        return itemData
    }
}