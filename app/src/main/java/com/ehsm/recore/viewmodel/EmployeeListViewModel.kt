package com.ehsm.recore.viewmodel

import android.os.AsyncTask
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ehsm.recore.R
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.database.dao.EmployeeListDAO
import com.ehsm.recore.model.*
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.SyncOperation
import com.kcspl.divyangapp.repository.CategoryListRepository
import com.kcspl.divyangapp.repository.EmployeeListRepository
import com.kcspl.divyangapp.repository.LoginRepository
import com.kcspl.divyangapp.repository.VerifyMPinRepository


class EmployeeListViewModel : ViewModel() {

    private var empData: MutableLiveData<DataWrapper<EmployeeListResponseModel>>? = null
    private val employeeListRepository = EmployeeListRepository()


    fun getEmployeeList(dbDatabase: RecoreDB?): MutableLiveData<DataWrapper<EmployeeListResponseModel>>? {
        empData = employeeListRepository.getEmployeeList(dbDatabase)
        return empData
    }

    /**
     * This class d for database operation
     */
    internal class EmployeeDatabaseOperationTask(
        private val mList: List<EmployeeList>?,
        private val mDatabase: RecoreDB?,
        private val callBackDataBase: (isOperationFinished: Boolean) -> Unit
    ) :
        AsyncTask<Void, Void, Void>() {
        private var employeeListDAO: EmployeeListDAO? = null
        override fun onPreExecute() {
            super.onPreExecute()
            employeeListDAO = mDatabase!!.daoEmployeeList()
        }
        override fun doInBackground(vararg voids: Void): Void? {
            if (mList != null && mList.isNotEmpty()) {
                employeeListDAO!!.insertEmployee(mList)
            }
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            callBackDataBase(true)
        }
    }
}