package com.kcspl.divyangapp.repository

import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.model.*
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class LastStatusDayStartCloseRepository {

    fun getLastActionDayStartClose(): MutableLiveData<DataWrapper<POSLastStatusResponseModel>> {
        val dataWrapper = DataWrapper<POSLastStatusResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<POSLastStatusResponseModel>>()

        RestClient.getService()
            ?.getLastActionDayStartClose()
            ?.enqueue(object : CustomApiCallback<POSLastStatusResponseModel>() {

                override fun handleResponseData(data: POSLastStatusResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }
}