package com.kcspl.divyangapp.repository

import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.model.LoginResponseModel
import com.ehsm.recore.model.VerifyMPinResponseModel
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class VerifyMPinRepository {
    fun verifyMPin(mpin: String): MutableLiveData<DataWrapper<VerifyMPinResponseModel>> {
        val dataWrapper = DataWrapper<VerifyMPinResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<VerifyMPinResponseModel>>()

        RestClient.getService()?.verifyMPin(mpin)
            ?.enqueue(object : CustomApiCallback<VerifyMPinResponseModel>() {

                override fun handleResponseData(data: VerifyMPinResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?, action: String) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    dataWrapper.action =action;
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }


}