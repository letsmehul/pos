package com.kcspl.divyangapp.repository

import android.os.AsyncTask
import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.database.dao.CategoryListDAO
import com.ehsm.recore.database.dao.EmployeeListDAO
import com.ehsm.recore.model.*
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class EmployeeListRepository {
    private var dao: EmployeeListDAO? = null

    fun getEmployeeList(dbDatabase: RecoreDB?): MutableLiveData<DataWrapper<EmployeeListResponseModel>> {
        dao = dbDatabase?.daoEmployeeList()

        val dataWrapper = DataWrapper<EmployeeListResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<EmployeeListResponseModel>>()

        RestClient.getService()?.getEmployeeList("")
            ?.enqueue(object : CustomApiCallback<EmployeeListResponseModel>() {

                override fun handleResponseData(data: EmployeeListResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }
}