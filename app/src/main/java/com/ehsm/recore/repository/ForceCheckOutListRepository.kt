package com.kcspl.divyangapp.repository

import android.os.AsyncTask
import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.database.dao.*
import com.ehsm.recore.model.*
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class ForceCheckOutListRepository {
    private var dao: ForceCheckOutListDAO? = null

    fun getForceCheckOutList(dbDatabase: RecoreDB?,search_date : String): MutableLiveData<DataWrapper<ForceCheckOutListResponseModel>> {
        dao = dbDatabase?.daoForceCheckOutList()

        val dataWrapper = DataWrapper<ForceCheckOutListResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<ForceCheckOutListResponseModel>>()

        RestClient.getService()?.getForceCheckOutList(search_date)
            ?.enqueue(object : CustomApiCallback<ForceCheckOutListResponseModel>() {

                override fun handleResponseData(data: ForceCheckOutListResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper

                    AddForceCheckOutListTask(dataWrapper.data!!.data).execute()
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }

    internal inner class AddForceCheckOutListTask(private val mList: List<ForceCheckOutModel>?) :
        AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg voids: Void): Void? {
            if (mList != null) {
                dao!!.insertForceCheckOutData(mList)
            }
            return null
        }
    }
}