package com.kcspl.divyangapp.repository

import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.model.AddCustomerResponseModel
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.model.LoginResponseModel
import com.ehsm.recore.model.ResponseModel
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class AddCustomerRepository {
    fun addCustomer(
        first_name: String,
        last_name: String,
        email: String,
        phone: String,
        remarks: String,
        gender: String,
        status: String,
        strAddress1: String,
        strAddress2: String,
        city_id: Int,
        state_id: Int,
        zipcode: String,
        dob: String,
        iCustomerId : String

    ): MutableLiveData<DataWrapper<AddCustomerResponseModel>> {
        val dataWrapper = DataWrapper<AddCustomerResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<AddCustomerResponseModel>>()

        RestClient.getService()
            ?.addCustomer(
                first_name,
                last_name,
                email,
                phone,
                remarks,
                gender,
                status,
                strAddress1,
                strAddress2,
                city_id,
                state_id,
                zipcode,
                dob,
                iCustomerId
            )
            ?.enqueue(object : CustomApiCallback<AddCustomerResponseModel>() {

                override fun handleResponseData(data: AddCustomerResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }


}