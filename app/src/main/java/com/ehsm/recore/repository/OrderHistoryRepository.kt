package com.kcspl.divyangapp.repository

import android.os.AsyncTask
import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.database.dao.*
import com.ehsm.recore.model.*
import com.ehsm.recore.network.RestClient
import com.ehsm.recore.utils.AppConstant
import com.kcspl.divyangapp.network.CustomApiCallback

class OrderHistoryRepository {
    private var dao: OrderHistoryListDAO? = null

    fun getOrderHistory(dbDatabase: RecoreDB?,start_date : String,end_date : String,type:String ="",page_no : Int,search : String,strCheckStatus : String): MutableLiveData<DataWrapper<OrderHistoryResponseModel>> {
        dao = dbDatabase?.daoOrerHistoryList()

        val dataWrapper = DataWrapper<OrderHistoryResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<OrderHistoryResponseModel>>()

        RestClient.getService()?.getOrderHistoryPagination(start_date,end_date,type,page_no,search,strCheckStatus)
            ?.enqueue(object : CustomApiCallback<OrderHistoryResponseModel>() {

                override fun handleResponseData(data: OrderHistoryResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper
                    AddOrderHistoryTask(dataWrapper.data!!.data).execute()
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }

    internal inner class AddOrderHistoryTask(private val mList: List<OrderHistoryModel>?) :
        AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg voids: Void): Void? {
            if (mList != null) {
                dao!!.insertOrderHistoryData(mList)
            }
            return null
        }
    }
}