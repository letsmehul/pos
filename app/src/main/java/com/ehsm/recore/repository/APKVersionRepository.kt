package com.ehsm.recore.repository

import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.model.ResponseModel
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class APKVersionRepository {
    fun apiCallAPKVersionCheck(current_version_code: String): MutableLiveData<DataWrapper<ResponseModel>> {
        val dataWrapper = DataWrapper<ResponseModel>()
        val mAPKVersionModel = MutableLiveData<DataWrapper<ResponseModel>>()

        RestClient.getService()?.getAPKVersionCheck(current_version_code)
            ?.enqueue(object : CustomApiCallback<ResponseModel>() {

                override fun handleResponseData(data: ResponseModel?) {
                    dataWrapper.data = data
                    mAPKVersionModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    mAPKVersionModel.value = dataWrapper
                }
                override fun showErrorMessage(errormessage: String?, latest_apk_url: String) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    dataWrapper.latest_apk_url =latest_apk_url;
                    mAPKVersionModel.value = dataWrapper
                }
            })
        return mAPKVersionModel
    }

}