package com.kcspl.divyangapp.repository

import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.model.LoginResponseModel
import com.ehsm.recore.model.ResponseModel
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class ChangeMPinRepository {
    fun changeMpin(
        emp_id: Int,
        mpin: Int,
        iMpin_old: Int
    ): MutableLiveData<DataWrapper<ResponseModel>> {
        val dataWrapper = DataWrapper<ResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<ResponseModel>>()

        RestClient.getService()?.changeMpin(emp_id, mpin, iMpin_old)
            ?.enqueue(object : CustomApiCallback<ResponseModel>() {

                override fun handleResponseData(data: ResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }


}