package com.kcspl.divyangapp.repository

import android.os.AsyncTask
import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.database.dao.CategoryListDAO
import com.ehsm.recore.database.dao.StateListDAO
import com.ehsm.recore.model.*
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class StateListRepository {
    private var dao: StateListDAO? = null

    fun getStates(dbDatabase: RecoreDB?): MutableLiveData<DataWrapper<StateListResponseModel>> {
        dao = dbDatabase?.daoStateList()

        val dataWrapper = DataWrapper<StateListResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<StateListResponseModel>>()

        RestClient.getService()?.getStateList()
            ?.enqueue(object : CustomApiCallback<StateListResponseModel>() {

                override fun handleResponseData(data: StateListResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper

                    AddStateListTask(dataWrapper.data!!.data).execute()
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }

    internal inner class AddStateListTask(private val mList: List<StateModel>?) :
        AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg voids: Void): Void? {
            if (mList != null) {
                dao!!.insertState(mList)
            }
            return null
        }
    }
}