package com.ehsm.recore.repository

import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.model.TerminalListResponseModel
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class TerminalRepository {
    fun apiCallTerminal(
    ): MutableLiveData<DataWrapper<TerminalListResponseModel>> {
        val dataWrapper = DataWrapper<TerminalListResponseModel>()
        val terminalResponseModel = MutableLiveData<DataWrapper<TerminalListResponseModel>>()

        RestClient.getService()?.getTerminalList()
            ?.enqueue(object : CustomApiCallback<TerminalListResponseModel>() {

                override fun handleResponseData(data: TerminalListResponseModel?) {
                    dataWrapper.data = data
                    terminalResponseModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    terminalResponseModel.value = dataWrapper
                }
            })
        return terminalResponseModel
    }

}