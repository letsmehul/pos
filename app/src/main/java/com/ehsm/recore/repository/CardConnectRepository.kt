package com.ehsm.recore.repository

import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.model.CardResponseModel
import com.ehsm.recore.model.CheckPaymentResponse
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.model.ResponseModel
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class CardConnectRepository {
    fun apiCallPaymentCheck(
        orderId: String,
        merchantId: String
    ): MutableLiveData<DataWrapper<CheckPaymentResponse>> {
        val dataWrapper = DataWrapper<CheckPaymentResponse>()
        val checkPaymentResponse = MutableLiveData<DataWrapper<CheckPaymentResponse>>()

        RestClient.getService()?.checkPayment(orderId, merchantId)
            ?.enqueue(object : CustomApiCallback<CheckPaymentResponse>() {

                override fun handleResponseData(data: CheckPaymentResponse?) {
                    dataWrapper.data = data
                    checkPaymentResponse.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    checkPaymentResponse.value = dataWrapper
                }
            })
        return checkPaymentResponse
    }

}