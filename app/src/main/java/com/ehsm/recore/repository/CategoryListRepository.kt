package com.kcspl.divyangapp.repository

import android.os.AsyncTask
import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.database.dao.CategoryListDAO
import com.ehsm.recore.model.*
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class CategoryListRepository {
    private var dao: CategoryListDAO? = null

    fun getCategories(dbDatabase: RecoreDB?, catSyncDateTime: String): MutableLiveData<DataWrapper<CategoryListResponseModel>> {
        dao = dbDatabase?.daoCategoryList()

        val dataWrapper = DataWrapper<CategoryListResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<CategoryListResponseModel>>()

        RestClient.getService()?.getCategoryList(catSyncDateTime)
            ?.enqueue(object : CustomApiCallback<CategoryListResponseModel>() {

                override fun handleResponseData(data: CategoryListResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }
}