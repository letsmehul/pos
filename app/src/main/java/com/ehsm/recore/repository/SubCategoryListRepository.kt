package com.kcspl.divyangapp.repository

import android.os.AsyncTask
import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.database.dao.SubCategoryListDAO
import com.ehsm.recore.model.*
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class SubCategoryListRepository {
    private var dao: SubCategoryListDAO? = null

    fun getSubCategories(
        dbDatabase: RecoreDB?,
        category_id: String,
        subCatSyncDateTime: String
    ): MutableLiveData<DataWrapper<SubCategoryListResponseModel>> {
        dao = dbDatabase?.daoSubCategoryList()

        val dataWrapper = DataWrapper<SubCategoryListResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<SubCategoryListResponseModel>>()

        RestClient.getService()?.getSubCategoryList(category_id, subCatSyncDateTime)
            ?.enqueue(object : CustomApiCallback<SubCategoryListResponseModel>() {

                override fun handleResponseData(data: SubCategoryListResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }
}