package com.kcspl.divyangapp.repository

import android.os.AsyncTask
import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.database.dao.CategoryListDAO
import com.ehsm.recore.database.dao.CustomerListDAO
import com.ehsm.recore.database.dao.ItemListDAO
import com.ehsm.recore.database.dao.SubCategoryListDAO
import com.ehsm.recore.model.*
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class CustomerListRepository {
    private var dao: CustomerListDAO? = null

    fun getCustomer(dbDatabase: RecoreDB?, search : String): MutableLiveData<DataWrapper<CustomerResponseModel>> {
        dao = dbDatabase?.daoCustomerList()

        val dataWrapper = DataWrapper<CustomerResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<CustomerResponseModel>>()

        RestClient.getService()?.getCustomerList(search)
            ?.enqueue(object : CustomApiCallback<CustomerResponseModel>() {

                override fun handleResponseData(data: CustomerResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper
                    if (dataWrapper.data != null && dataWrapper.data!!.data != null) {
                        dao!!.insertCustomer(dataWrapper.data!!.data)
                    }
                    //AddCustomerListTask(dataWrapper.data!!.data).execute()
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }

    internal inner class AddCustomerListTask(private val mList: List<CustomerModel>?) :
        AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg voids: Void): Void? {
            if (mList != null) {
                dao!!.insertCustomer(mList)
            }
            return null
        }
    }
}