package com.kcspl.divyangapp.repository

import android.os.AsyncTask
import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.database.dao.*
import com.ehsm.recore.model.*
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class CashDrawerManualReportRepository {
    private var dao: CashDrawerManualReportListDAO? = null

    fun getCashDrawerManualReportData(dbDatabase: RecoreDB?,date : String): MutableLiveData<DataWrapper<CashDrawerManualReportResponseModel>> {
        dao = dbDatabase?.daoCashDrawerManualReportList()

        val dataWrapper = DataWrapper<CashDrawerManualReportResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<CashDrawerManualReportResponseModel>>()

        RestClient.getService()?.getCashDrawerManualReportData(date)
            ?.enqueue(object : CustomApiCallback<CashDrawerManualReportResponseModel>() {

                override fun handleResponseData(data: CashDrawerManualReportResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper

                    AddCashDrawerManualTask(dataWrapper.data!!.data).execute()
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }

    internal inner class AddCashDrawerManualTask(private val mList: List<CashDrawerManualReportModel>?) :
        AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg voids: Void): Void? {
            if (mList != null) {
                dao!!.insertCashDrawerManualData(mList)
            }
            return null
        }
    }
}