package com.kcspl.divyangapp.repository

import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.model.CheckInDataResponseModel
import com.ehsm.recore.model.CheckOutDataResponseModel
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.model.ResponseModel
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class CheckOutDataRepository {
    fun getCheckOutData(emp_id: Int): MutableLiveData<DataWrapper<CheckOutDataResponseModel>> {
        val dataWrapper = DataWrapper<CheckOutDataResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<CheckOutDataResponseModel>>()

        RestClient.getService()?.getCheckOutData(emp_id)
            ?.enqueue(object : CustomApiCallback<CheckOutDataResponseModel>() {

                override fun handleResponseData(data: CheckOutDataResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }


}