package com.kcspl.divyangapp.repository

import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.model.NewSaleandRefundModel
import com.ehsm.recore.model.ResponseModel
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback
import retrofit2.Callback
import retrofit2.Response

class PlaceOrderRepository {

    fun placeOrder(
        iCheckDetailID: String,
        order_id: String,
        customer_id: Int,
        emp_id: Int,
        gross_total: Double,
        tax_total: Double,
        discount: Double,
        net_total: Double,
        json_item_data: String,
        json_payment_data: String,
        json_discount_data: String,
        customerEmail: String,
        tIsVoided: String = "0",
        tCheckTaxExempt: String,
        tIsApplePay: Boolean,
        strPaymentType: String,
        strCheckStatus: String
    ): MutableLiveData<DataWrapper<NewSaleandRefundModel>> {
        val dataWrapper = DataWrapper<NewSaleandRefundModel>()
        val mNewSaleandRefundModel = MutableLiveData<DataWrapper<NewSaleandRefundModel>>()

        RestClient.getService()
            ?.placeOrder(
                iCheckDetailID,
                order_id,
                customer_id,
                emp_id,
                gross_total,
                tax_total,
                discount,
                net_total,
                json_item_data,
                json_payment_data,
                json_discount_data,
                customerEmail,
                tIsVoided,
                tCheckTaxExempt,
                tIsApplePay,
                strPaymentType,
                strCheckStatus
            )
            ?.enqueue(object : CustomApiCallback<NewSaleandRefundModel>() {

                override fun handleResponseData(data: NewSaleandRefundModel?) {
                    dataWrapper.data = data
                    mNewSaleandRefundModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    mNewSaleandRefundModel.value = dataWrapper
                }
            })
        return mNewSaleandRefundModel
    }

    fun Update_order_payment(
        iCheckDetailID: String,
        json_payment_data: String,
        strPaymentType: String
    ): MutableLiveData<DataWrapper<NewSaleandRefundModel>> {
        val dataWrapper = DataWrapper<NewSaleandRefundModel>()
        val mNewSaleandRefundModel = MutableLiveData<DataWrapper<NewSaleandRefundModel>>()

        RestClient.getService()
            ?.Update_order_payment(
                iCheckDetailID,
                json_payment_data,
                strPaymentType
            )
            ?.enqueue(object : CustomApiCallback<NewSaleandRefundModel>() {

                override fun handleResponseData(data: NewSaleandRefundModel?) {
                    dataWrapper.data = data
                    mNewSaleandRefundModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    mNewSaleandRefundModel.value = dataWrapper
                }
            })
        return mNewSaleandRefundModel
    }

    fun Update_Payment_type(
        iCheckDetailID: String,
        strPaymentType: String
    ): MutableLiveData<DataWrapper<ResponseModel>> {
        val dataWrapper = DataWrapper<ResponseModel>()
        val responsemodel = MutableLiveData<DataWrapper<ResponseModel>>()

        RestClient.getService()
            ?.Update_Payment_type(
                iCheckDetailID,
                strPaymentType
            )
            ?.enqueue(object : CustomApiCallback<ResponseModel>() {

                override fun handleResponseData(data: ResponseModel?) {
                    dataWrapper.data = data
                    responsemodel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    responsemodel.value = dataWrapper
                }
            })
        return responsemodel
    }

    fun hold_order_customer_exist(
        iCustomerId: String
    ): MutableLiveData<DataWrapper<ResponseModel>> {
        val dataWrapper = DataWrapper<ResponseModel>()
        val mHoldModel = MutableLiveData<DataWrapper<ResponseModel>>()

        RestClient.getService()
            ?.hold_order_customer_exist(
                iCustomerId
            )
            ?.enqueue(object : CustomApiCallback<ResponseModel>() {

                override fun handleResponseData(data: ResponseModel?) {
                    dataWrapper.data = data
                    mHoldModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    mHoldModel.value = dataWrapper
                }
            })
        return mHoldModel
    }


    fun sendOrderReceipt(
        customerEmail: String,
        order_id: Int
    ): MutableLiveData<DataWrapper<ResponseModel>>? {
        val dataWrapper = DataWrapper<ResponseModel>()
        val responseModel = MutableLiveData<DataWrapper<ResponseModel>>()

        RestClient.getService()
            ?.sendOrderReceipt(customerEmail, order_id)
            ?.enqueue(object : CustomApiCallback<ResponseModel>() {

                override fun handleResponseData(data: ResponseModel?) {
                    dataWrapper.data = data
                    responseModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    responseModel.value = dataWrapper
                }
            })

        return responseModel
    }
}