package com.kcspl.divyangapp.repository

import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.model.*
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class LastActionRepository {

    fun getLastAction(emp_id: Int): MutableLiveData<DataWrapper<LastActionResponseModel>> {
        val dataWrapper = DataWrapper<LastActionResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<LastActionResponseModel>>()

        RestClient.getService()
            ?.getLastAction(emp_id)
            ?.enqueue(object : CustomApiCallback<LastActionResponseModel>() {

                override fun handleResponseData(data: LastActionResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }
}