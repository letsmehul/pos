package com.kcspl.divyangapp.repository

import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.model.LoginResponseModel
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class LoginRepository {
    fun logIn(
        username: String,
        password: String,
        fcm: String,
        mac_address: String
    ): MutableLiveData<DataWrapper<LoginResponseModel>> {
        val dataWrapper = DataWrapper<LoginResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<LoginResponseModel>>()

        RestClient.getService()?.logIn(username,password,fcm,mac_address)
            ?.enqueue(object : CustomApiCallback<LoginResponseModel>() {

                override fun handleResponseData(data: LoginResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }

    fun logInData(): MutableLiveData<DataWrapper<LoginResponseModel>> {
        val dataWrapper = DataWrapper<LoginResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<LoginResponseModel>>()

        RestClient.getService()?.logInData()
            ?.enqueue(object : CustomApiCallback<LoginResponseModel>() {
                override fun handleResponseData(data: LoginResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }


}