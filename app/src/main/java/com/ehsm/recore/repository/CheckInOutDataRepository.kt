package com.kcspl.divyangapp.repository

import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.model.ClockOutDataResponseModel
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.model.LoginResponseModel
import com.ehsm.recore.model.ResponseModel
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class CheckInOutDataRepository {
    fun checkInOutData(date : String, amount : Double,highcash : Double,shortcash : Double,type :String,emp_id :Int, remarks : String,cash_in_drawer : Double,user_id :Int, isForcecheckOut :Int,bitCashCollectByAdmin : Int): MutableLiveData<DataWrapper<ClockOutDataResponseModel>> {
        val dataWrapper = DataWrapper<ClockOutDataResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<ClockOutDataResponseModel>>()

        RestClient.getService()?.checkInOutData(date, amount,highcash,shortcash,type,emp_id, remarks,cash_in_drawer,user_id,isForcecheckOut,bitCashCollectByAdmin)
            ?.enqueue(object : CustomApiCallback<ClockOutDataResponseModel>() {

                override fun handleResponseData(data: ClockOutDataResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }


}