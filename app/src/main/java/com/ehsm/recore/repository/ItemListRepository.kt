package com.kcspl.divyangapp.repository

import android.os.AsyncTask
import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.database.dao.*
import com.ehsm.recore.model.*
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class ItemListRepository {
    private var daoItemList: ItemListDAO? = null
    private var daoModifierList: ModifierGroupListDAO? = null
    private var daoPreModifierList: PreModifierListDAO? = null
    private var daoIngredientList: IngredientListDAO? = null

    init {

    }

    fun getItemList(
        dbDatabase: RecoreDB?,
        subcategory_id: String,
        prodSyncDateTime: String
    ): MutableLiveData<DataWrapper<ItemListResponseModel>> {
        daoItemList = dbDatabase?.daoItemList()
        daoModifierList = dbDatabase?.daoModifierGroupList()
        daoPreModifierList = dbDatabase?.daoPreModifierList()
        daoIngredientList = dbDatabase?.daoIngredientList()

        val dataWrapper = DataWrapper<ItemListResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<ItemListResponseModel>>()

        RestClient.getService()?.getItemList(subcategory_id, prodSyncDateTime)
            ?.enqueue(object : CustomApiCallback<ItemListResponseModel>() {

                override fun handleResponseData(data: ItemListResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }
}