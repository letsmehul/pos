package com.kcspl.divyangapp.repository

import android.os.AsyncTask
import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.database.dao.CashDrawerReportListDAO
import com.ehsm.recore.database.dao.CategoryListDAO
import com.ehsm.recore.database.dao.CityListDAO
import com.ehsm.recore.database.dao.StateListDAO
import com.ehsm.recore.model.*
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class CashDrawerReportRepository {
    private var dao: CashDrawerReportListDAO? = null

    fun getCashDrawerReportData(dbDatabase: RecoreDB?,date : String): MutableLiveData<DataWrapper<CashDrawerReportResponseModel>> {
        dao = dbDatabase?.daoCashDrawerReportList()

        val dataWrapper = DataWrapper<CashDrawerReportResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<CashDrawerReportResponseModel>>()

        RestClient.getService()?.getCashDrawerReportData(date)
            ?.enqueue(object : CustomApiCallback<CashDrawerReportResponseModel>() {

                override fun handleResponseData(data: CashDrawerReportResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper

                    AddCashDrawerTask(dataWrapper.data!!.data).execute()
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }

    internal inner class AddCashDrawerTask(private val mList: List<CashDrawerReportModel>?) :
        AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg voids: Void): Void? {
            if (mList != null) {
                dao!!.insertCashDrawerData(mList)
            }
            return null
        }
    }
}