package com.kcspl.divyangapp.repository

import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.model.CheckInDataResponseModel
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.model.ResponseModel
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class CheckInDataRepository {
    fun getCheckInData(emp_id: Int): MutableLiveData<DataWrapper<CheckInDataResponseModel>> {
        val dataWrapper = DataWrapper<CheckInDataResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<CheckInDataResponseModel>>()

        RestClient.getService()?.getCheckInData(emp_id)
            ?.enqueue(object : CustomApiCallback<CheckInDataResponseModel>() {

                override fun handleResponseData(data: CheckInDataResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }


}