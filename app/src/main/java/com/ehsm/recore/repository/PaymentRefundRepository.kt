package com.kcspl.divyangapp.repository

import android.os.AsyncTask
import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.database.dao.CategoryListDAO
import com.ehsm.recore.database.dao.ItemListDAO
import com.ehsm.recore.database.dao.SubCategoryListDAO
import com.ehsm.recore.model.*
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class PaymentRefundRepository {

    fun paymentRefund(
        amount: Double,
        date: String,
        id: String,
        refund_type: String,
        merchantId:String,
        retref:String,
        reason : String,
        status : String,
        emp_id : Int,
        item_data : String,
        taxAmount : Double,
        dblDiscountAmount : Double
    ): MutableLiveData<DataWrapper<NewSaleandRefundModel>> {
        val dataWrapper = DataWrapper<NewSaleandRefundModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<NewSaleandRefundModel>>()

        RestClient.getService()
            ?.paymentRefund(amount, date, id, refund_type,merchantId,retref, reason, status, emp_id, taxAmount,item_data, dblDiscountAmount)
            ?.enqueue(object : CustomApiCallback<NewSaleandRefundModel>() {

                override fun handleResponseData(data: NewSaleandRefundModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }
}