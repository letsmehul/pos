package com.ehsm.recore.repository

import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.model.*
import com.ehsm.recore.network.BoltApiHelper
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class PaymentRepository(private val apiHelper: BoltApiHelper) {

    suspend fun getTerminals(merchantId :String) = apiHelper.getTerminals(merchantId)


    suspend fun connect(merchantId :String,hsn:String,force :Boolean) =apiHelper.connect(merchantId,hsn,force)

    suspend fun ping(merchantId :String,hsn:String) =apiHelper.ping(merchantId,hsn)

    suspend fun disconnect(merchantId :String,hsn:String) =apiHelper.disconnect(merchantId,hsn)

    suspend fun cancel(merchantId :String,hsn:String) =apiHelper.cancel(merchantId,hsn)

    suspend fun display(merchantId :String,hsn:String,text:String) =apiHelper.display(merchantId,hsn,text)

    suspend fun readConfirmation(merchantId :String,hsn:String,text:String) =apiHelper.readConfirmation(RequestReadSignature(merchantId,hsn,text))

    suspend fun readInput(readInput: ReadInput) =apiHelper.readInput(readInput)

    suspend fun authCard(cardRequestModel: CardRequestModel) =apiHelper.authCard(cardRequestModel)

    suspend fun authManual(requestReadCard: RequestReadCard) =apiHelper.authManual(requestReadCard)

    suspend fun readManual(manualCardReadRequestModel: ManualCardReadRequestModel) =apiHelper.readManual(manualCardReadRequestModel)

    fun sendBoltrequest(
        BoltReqLogsId: String,
        iCheckDetailID: String,
        strRequestBody: String,
        strResponseBody: String,
        strConnectRequestBody: String,
        strConnectResponseBody: String
    ): MutableLiveData<DataWrapper<BoltModel>> {
        val dataWrapper = DataWrapper<BoltModel>()
        val Sendboltmodel = MutableLiveData<DataWrapper<BoltModel>>()

        RestClient.getService()
            ?.sendBoltrequest(
                BoltReqLogsId,
                iCheckDetailID,
                strRequestBody,
                strResponseBody,
                strConnectRequestBody,
                strConnectResponseBody
            )
            ?.enqueue(object : CustomApiCallback<BoltModel>() {

                override fun handleResponseData(data: BoltModel?) {
                    dataWrapper.data = data
                    Sendboltmodel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    Sendboltmodel.value = dataWrapper
                }
            })
        return Sendboltmodel
    }

}