package com.kcspl.divyangapp.repository

import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.model.NewSaleandRefundModel
import com.ehsm.recore.model.PaymentMethodModel
import com.ehsm.recore.model.ResponseModel
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback
import retrofit2.Callback
import retrofit2.Response

class PaymentMethodRepository {

    fun getPayments(): MutableLiveData<DataWrapper<PaymentMethodModel>> {
        val dataWrapper = DataWrapper<PaymentMethodModel>()
        val paymentMethodModel = MutableLiveData<DataWrapper<PaymentMethodModel>>()

        RestClient.getService()
            ?.getPaymentList()
            ?.enqueue(object : CustomApiCallback<PaymentMethodModel>() {

                override fun handleResponseData(data: PaymentMethodModel?) {
                    dataWrapper.data = data
                    paymentMethodModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    paymentMethodModel.value = dataWrapper
                }
            })
        return paymentMethodModel
    }
}