package com.kcspl.divyangapp.repository

import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.model.*
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class BreakRepository {

    fun getBreak(emp_id: Int): MutableLiveData<DataWrapper<BreakInOutResponse>> {
        val dataWrapper = DataWrapper<BreakInOutResponse>()
        val loginResponseModel = MutableLiveData<DataWrapper<BreakInOutResponse>>()

        RestClient.getService()
            ?.getBreakAction(emp_id)
            ?.enqueue(object : CustomApiCallback<BreakInOutResponse>() {

                override fun handleResponseData(data: BreakInOutResponse?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }

    fun breakInOutData(
        date: String,
        type: String,
        emp_id: Int
    ): MutableLiveData<DataWrapper<ResponseModel>> {
        val dataWrapper = DataWrapper<ResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<ResponseModel>>()

        RestClient.getService()?.breakInOutData(date, type, emp_id)
            ?.enqueue(object : CustomApiCallback<ResponseModel>() {

                override fun handleResponseData(data: ResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }
}