package com.kcspl.divyangapp.repository

import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.database.dao.CategoryListDAO
import com.ehsm.recore.database.dao.ItemListDAO
import com.ehsm.recore.database.dao.ManualSyncDAO
import com.ehsm.recore.database.dao.SubCategoryListDAO
import com.ehsm.recore.model.*
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback
import java.util.*

class ManualSyncRepository {
    private var dao: ManualSyncDAO? = null

    fun manualsync(dbDatabase: RecoreDB?, iCheckEmployeeId : Int): MutableLiveData<DataWrapper<ManualSyncResponseModel>> {
        dao = dbDatabase?.daoManualSync()

        val dataWrapper = DataWrapper<ManualSyncResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<ManualSyncResponseModel>>()

        RestClient.getService()?.manualsync(iCheckEmployeeId)
            ?.enqueue(object : CustomApiCallback<ManualSyncResponseModel>() {

                override fun handleResponseData(data: ManualSyncResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper

                    AddManualSyncTask(dataWrapper.data!!.employee_list_with_last_activity).execute()
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }

    internal inner class AddManualSyncTask(private val mList: List<EmployeeListWithLastAction>?) :
        AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg voids: Void): Void? {
            if (mList != null) {
                dao!!.insertItem(mList)
            }
            return null
        }

    }
}