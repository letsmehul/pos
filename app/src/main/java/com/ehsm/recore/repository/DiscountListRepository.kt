package com.ehsm.recore.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.database.dao.DiscountDAO
import com.ehsm.recore.model.*
import com.ehsm.recore.network.RestClient
import com.ehsm.recore.utils.AppConstant
import com.kcspl.divyangapp.network.CustomApiCallback
import kotlin.math.min

class DiscountListRepository(val dbDatabase: RecoreDB?) {

    private var discountDAO: DiscountDAO?=null

    init {

        discountDAO = dbDatabase?.daoDiscount()
    }

    fun getDiscountList(iCheckEmployeeId : Int,callBack:(hasDiscountLoaded :Boolean) -> Unit): MutableLiveData<DataWrapper<DiscountResponse>> {

        val dataWrapper = DataWrapper<DiscountResponse>()
        val discountResponseModel = MutableLiveData<DataWrapper<DiscountResponse>>()
        RestClient.getService()?.getDiscountList(iCheckEmployeeId)
            ?.enqueue(object : CustomApiCallback<DiscountResponse>() {

                override  fun handleResponseData(data: DiscountResponse?) {
                    dataWrapper.data = data
                    discountResponseModel.value = dataWrapper



                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    discountResponseModel.value = dataWrapper
                }
            })
        return discountResponseModel
    }

    fun getDiscountsFromServer(iCheckEmployeeId : Int,callBack:(data: DiscountModel?,error :String?) -> Unit){
        RestClient.getService()?.getDiscountList(iCheckEmployeeId)
            ?.enqueue(object : CustomApiCallback<DiscountResponse>() {

                override  fun handleResponseData(data: DiscountResponse?) {
                    data?.let {
                        discountResponse ->
                        callBack(discountResponse.data,"")
                    }
                }

                override fun showErrorMessage(errormessage: String?) {
                    callBack(null,errormessage)
                }
            })
    }

    suspend fun saveSimpleDiscount(simple_discount: List<DiscountMaster>){
        discountDAO?.run {
            try {
                this.insertItemsIntoDiscountMaster(simple_discount)
            }catch (e:Exception)
            {
                Log.d("error",e.localizedMessage)
            }
        }
    }

    suspend fun saveCouponDiscount(discounts: List<DiscountMaster>){
        discountDAO?.run {

            try {
                this.insertItemsIntoDiscountMaster(discounts)
            }catch (e:Exception)
            {
                Log.d("error",e.localizedMessage)
            }


        }
    }

    suspend fun saveSalesDiscount(discounts: List<DiscountMaster>){
        discountDAO?.run {


            try {
                this.insertItemsIntoDiscountMaster(discounts)
            }catch (e:Exception)
            {
                Log.d("error",e.localizedMessage)
            }
        }
    }

    suspend fun saveSalesDiscountProducts(discounts: List<DiscountMapping>){
        discountDAO?.run {


            try {
                this.insertItemsIntoDiscountMapping(discounts)
            }catch (e:Exception)
            {
                Log.d("error",e.localizedMessage)
            }
        }
    }


    suspend fun saveBogoDiscount(discounts: List<DiscountMaster>){
        discountDAO?.run {
            try {
                this.insertItemsIntoDiscountMaster(discounts)
            }catch (e:Exception)
            {
                Log.d("error",e.localizedMessage)
            }
        }
    }


    suspend fun saveBogoDiscountBuyItems(discounts: List<DiscountMapping>){
        discountDAO?.run {


            try {
                this.insertItemsIntoDiscountMapping(discounts)
            }catch (e:Exception)
            {
                Log.d("error",e.localizedMessage)
            }
        }
    }

    suspend fun saveBogoDiscountGetItems(mappingList: List<DiscountMapping>){
        discountDAO?.run {

            try {
                this.insertItemsIntoDiscountMapping(mappingList)
            }catch (e:Exception)
            {
                Log.d("error",e.localizedMessage)
            }
        }
    }

    suspend fun deleteAppliedDiscountOnCart(orderId:String){
        discountDAO?.deleteByOrderId(orderId)
    }

    fun getCashierDiscount() :List<DiscountMaster>{
        var list = ArrayList<DiscountMaster>()
        discountDAO?.run {
          try {
              list = this.getDiscountByTypes(listOf(AppConstant.SIMPLE_DISCOUNT,AppConstant.COUPON_DISCOUNT)) as ArrayList<DiscountMaster>
          }catch (e:Exception){
              Log.d("error",e.localizedMessage)
          }
        }
        return list
    }

    fun isCartLevelDiscountApplied(orderId:String) :Boolean{
        var isCartLevelDiscountApplied =false
        discountDAO?.getAppliedDiscount(orderId)?.let {
            isCartLevelDiscountApplied =  it.isNotEmpty()
        }
       return isCartLevelDiscountApplied
    }

    fun appliedSimpleDiscountItem(orderId:String) :Int{
        var noOfItems = 0
        discountDAO?.getSimpleDiscountCount(orderId,AppConstant.SIMPLE_DISCOUNT)?.let {
            noOfItems=  it
        }
        return noOfItems
    }

    fun getAppliedDiscountOnCart(orderId:String) :List<DiscountApplyModel>{
        var list = ArrayList<DiscountApplyModel>()
        discountDAO?.run {
            try {
                list = this.getAppliedDiscount(orderId) as ArrayList<DiscountApplyModel>
            }catch (e:Exception){
                Log.d("error",e.localizedMessage)
            }
        }
        return list
    }


    fun insertAppliedCartDiscount(discountApplyModel:DiscountApplyModel) :List<DiscountApplyModel>{
        var list = ArrayList<DiscountApplyModel>()
        discountDAO?.run {
            this.insertAppliedDiscount(discountApplyModel)
        }
        return list
    }

    fun deleteDiscount(){
        discountDAO?.run {
            this.deleteDiscountMaster()
            this.deleteDiscountTransaction()
        }
    }

    fun getApplicableDiscountOnItem(iProductId: Int, itemQty: Int) : ArrayList<DiscountAndProduct> {
        var list =ArrayList<DiscountAndProduct>()
        discountDAO?.run {
           try {
               list = this.getApplicableDiscountOnItem(iProductId,itemQty,"Buy") as ArrayList<DiscountAndProduct>
           }catch (e:Exception){
               e.printStackTrace()
           }

        }
        return list
    }

    fun getApplicableDiscountOnItemGroup(discountId: Int) : ArrayList<DiscountAndProduct> {
        var list =ArrayList<DiscountAndProduct>()
        discountDAO?.run {
            try {
                    list = this.getApplicableDiscountOnItemGroup(discountId,"Buy") as ArrayList<DiscountAndProduct>

            }catch (e:Exception){
                e.printStackTrace()
            }

        }
        return list
    }

    fun getAllDiscountIds() : ArrayList<Long> {
        var list =ArrayList<Long>()
        discountDAO?.run {
            try {
                list = this.getDiscountIds() as ArrayList<Long>

            }catch (e:Exception){
                e.printStackTrace()
            }

        }
        return list
    }




    fun getFreeDiscountProducts(iDiscount:Int) : ArrayList<DiscountAndProduct> {
        var list =ArrayList<DiscountAndProduct>()
        discountDAO?.run {
            try {
                list = this.getFreeDiscountProduct(iDiscount,"Get") as ArrayList<DiscountAndProduct>
            }catch (e:Exception){
                e.printStackTrace()
            }

        }
        return list
    }

    fun getMinQtyFromDiscountId(iDiscountId:Int) : Int {
        var minQty:Int = 0
        discountDAO?.run {
            try {
                minQty = this.getMinQty(iDiscountId)
            }catch (e:Exception){
                e.printStackTrace()
            }

        }
        return minQty
    }

    fun checkDiscountMappingItemIsAvailable(pid: Long,dis_id : Long) : Boolean{
        discountDAO?.run {
            return this.checkDiscountMappingItemIsAvailable(pid, dis_id)
        }
        return false
    }


    fun getDiscountMasterData(iDiscountId: Int) : DiscountMaster?{
        discountDAO?.run {
            return this.getDiscountMasterData(iDiscountId)
        }
        return null
    }

    fun checkProductCouponDetail(pid: Long, dis_id: Long): DiscountAndProduct? {
        discountDAO?.run {
            return this.checkProductCouponDetail(pid, dis_id)
        }
        return null
    }

    /*suspend  fun getCashierDiscountList(iCheckEmployeeId : Int) :List<DiscountMaster> = withContext(Dispatchers.IO){



        return@withContext
    }*/


}