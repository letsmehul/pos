package com.kcspl.divyangapp.repository

import android.os.AsyncTask
import androidx.lifecycle.MutableLiveData
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.database.dao.CategoryListDAO
import com.ehsm.recore.database.dao.CityListDAO
import com.ehsm.recore.database.dao.StateListDAO
import com.ehsm.recore.model.*
import com.ehsm.recore.network.RestClient
import com.kcspl.divyangapp.network.CustomApiCallback

class CityListRepository {
    private var dao: CityListDAO? = null

    fun getCityList(dbDatabase: RecoreDB?,state_id : Int): MutableLiveData<DataWrapper<CityListResponseModel>> {
        dao = dbDatabase?.daoCityList()

        val dataWrapper = DataWrapper<CityListResponseModel>()
        val loginResponseModel = MutableLiveData<DataWrapper<CityListResponseModel>>()

        RestClient.getService()?.getCityList(state_id)
            ?.enqueue(object : CustomApiCallback<CityListResponseModel>() {

                override fun handleResponseData(data: CityListResponseModel?) {
                    dataWrapper.data = data
                    loginResponseModel.value = dataWrapper

                    AddCityListTask(dataWrapper.data!!.data).execute()
                }

                override fun showErrorMessage(errormessage: String?) {
                    dataWrapper.message = errormessage
                    dataWrapper.data = null
                    loginResponseModel.value = dataWrapper
                }
            })
        return loginResponseModel
    }

    internal inner class AddCityListTask(private val mList: List<CityModel>?) :
        AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg voids: Void): Void? {
            if (mList != null) {
                dao!!.insertCity(mList)
            }
            return null
        }
    }
}