package com.ehsm.recore.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.ehsm.recore.model.*

@Dao
interface CashDrawerReportListDAO {

    @Query("Select * From CashDrawerReportModel")
    fun getCashDrawerData(): LiveData<List<CashDrawerReportModel>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertCashDrawerData(stateList : List<CashDrawerReportModel>)

    @Query("Delete from CashDrawerReportModel")
    fun deleteCashDrawerData()
}