package com.ehsm.recore.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.ehsm.recore.model.*

@Dao
interface CashDrawerManualReportListDAO {

    @Query("Select * From CashDrawerManualReportModel")
    fun getCashDrawerManualData(): LiveData<List<CashDrawerManualReportModel>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertCashDrawerManualData(stateList : List<CashDrawerManualReportModel>)

    @Query("Delete from CashDrawerManualReportModel")
    fun deleteCashDrawerManualData()
}