package com.ehsm.recore.database.dao

import androidx.room.*
import com.ehsm.recore.model.ModifierMasterModel
import com.ehsm.recore.model.PreModifierMasterModel


@Dao
interface PreModifierListDAO {

   // @Query("Select * From PreModifierMasterModel where iModifierGroupId =:iModifierGroupId")
   // @Query("SELECT * FROM ModifierMapper LEFT JOIN PreModifierMasterModel ON ModifierMapper.iPreModifierId = PreModifierMasterModel.iPreModifierId where ModifierMapper.iModifierGroupId =:iModifierGroupId GROUP by PreModifierMasterModel.strPreModifierName")
   @Query("Select * From PreModifierMasterModel where iModifierGroupId =:iModifierGroupId AND iProductId =:iProductId")
    fun getPreModifierList(iModifierGroupId : Int, iProductId: Int): List<PreModifierMasterModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPreModifier(ItemList : List<PreModifierMasterModel>)

    @Query("DELETE FROM PreModifierMasterModel where iPreModifierGroupId =:iPreModifierGroupId")
    fun deletePreModifier(iPreModifierGroupId : String)

    @Query("DELETE FROM PreModifierMasterModel where iProductId =:iProductId")
    fun deletePreModifierByProductId(iProductId : Int)

    @Query("DELETE FROM PreModifierMasterModel")
    fun deletePreModifier()
}