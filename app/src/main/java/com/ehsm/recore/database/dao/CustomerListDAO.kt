package com.ehsm.recore.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.ehsm.recore.model.CategoryModel
import com.ehsm.recore.model.CustomerModel
import com.ehsm.recore.model.ItemModel
import com.ehsm.recore.model.SubCategoryModel

@Dao
interface CustomerListDAO {

    @Query("Select * From CustomerModel")
    fun getCustomers(): LiveData<List<CustomerModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCustomer(ItemList : List<CustomerModel>)


    @Query("Select * From CustomerModel where iCustomerId =:iCustomerId")
    fun getCustomer(iCustomerId : Int): CustomerModel

    @Query("UPDATE CustomerModel SET dateDob=:dateDob WHERE iCustomerId = :iCustomerId")
    fun updateCustomerDateofBirth(dateDob : String, iCustomerId : Int)

    @Query("DELETE FROM CustomerModel")
    fun deleteCustomers()
}