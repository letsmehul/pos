package com.ehsm.recore.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.ehsm.recore.model.CategoryModel
import com.ehsm.recore.model.EmployeeList

@Dao
interface EmployeeListDAO {

    @Query("Select * From EmployeeList")
    fun getAllEmployee():List<EmployeeList>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertEmployee(categoryList : List<EmployeeList>)

    @Query("DELETE FROM EmployeeList")
    fun deleteEmployee()
}