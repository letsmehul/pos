package com.ehsm.recore.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.ehsm.recore.model.*

@Dao
interface OrderHistoryListDAO {

    @Query("Select * From OrderHistoryModel")
    fun getAllOrderList(): LiveData<List<OrderHistoryModel>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertOrderHistoryData(orderList : List<OrderHistoryModel>)

    @Query("Select * From OrderHistoryModel where strInvoiceNumber =:strInvoiceNumber")
    //  fun getOrderTransItem(orderId : String, item_id : Int): LiveData<List<OrderTransModel>>
    fun getOrderHistory(strInvoiceNumber : String): List<OrderHistoryModel>

    @Query("Select * From OrderHistoryModel where status =:status")
    //  fun getOrderTransItem(orderId : String, item_id : Int): LiveData<List<OrderTransModel>>
    fun getHoldOrderHistory(status : String): List<OrderHistoryModel>

    @Query("DELETE From OrderHistoryModel WHERE iCheckDetailID = :orderId")
    fun deleteHoldOrder(orderId: String)

    @Query("Select count() From OrderHistoryModel where iCustomerId =:customerId AND status =:status")
    fun isCustomerRecordExist(customerId : Int, status : String): Int

    @Query("Select count() From OrderHistoryModel where status=:status")
    fun getNumberOfRecordsByStatus(status : String): Int

    @Query("DELETE FROM OrderHistoryModel")
    fun deleteOrderHistoryData()
}