package com.ehsm.recore.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ehsm.recore.model.CategoryModel

@Dao
interface CategoryListDAO {

    @Query("Select * From CategoryModel where tIsActive = 1 order by iDisplayOrder")
    fun getCategories(): LiveData<List<CategoryModel>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertCategory(categoryList : List<CategoryModel>)

    @Query("DELETE FROM CategoryModel")
    fun deleteCategories()
}