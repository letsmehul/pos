package com.ehsm.recore.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.ehsm.recore.model.*

@Dao
interface ForceCheckOutListDAO {

    @Query("Select * From ForceCheckOutModel")
    fun getAllForceCheckOutList(): LiveData<List<ForceCheckOutModel>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertForceCheckOutData(orderList : List<ForceCheckOutModel>)

    @Query("DELETE FROM ForceCheckOutModel")
    fun deleteForceCheckOut()
}