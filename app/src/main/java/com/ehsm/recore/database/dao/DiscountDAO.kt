package com.ehsm.recore.database.dao

import androidx.room.*
import com.ehsm.recore.model.*
import kotlinx.coroutines.flow.Flow


@Dao
interface DiscountDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertItemsIntoDiscountMaster(ItemList: List<DiscountMaster>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertItemsIntoDiscountMapping(ItemList: List<DiscountMapping>)

    @Query("Select * From DiscountMaster where discountType IN (:discountTypes)")
    fun getDiscountByTypes(discountTypes: List<String>): List<DiscountMaster>


    @Query("Select * From DiscountApplyModel where orderId =:orderId")
    fun getAppliedDiscount(orderId: String): List<DiscountApplyModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAppliedDiscount(ItemList: DiscountApplyModel)

    @Query("Select COUNT(iDiscountId) From DiscountApplyModel where orderId =:orderId and type =:discountType")
    fun getSimpleDiscountCount(orderId: String, discountType: String): Int


    @Query("Delete From DiscountApplyModel WHERE orderId = :orderId")
    fun deleteByOrderId(orderId: String)

    @Query("DELETE FROM DiscountMaster")
    fun deleteDiscountMaster()

    @Query("DELETE FROM DiscountMapping")
    fun deleteDiscountTransaction()

    @Query("DELETE FROM DiscountApplyModel")
    fun deleteDiscountApply()

    @Query("SELECT * from DiscountMaster left JOIN DiscountMapping WHERE DiscountMapping.discountId == DiscountMaster.discountId and DiscountMapping.productId =:productId and :itemQty >= DiscountMaster.buyQty AND (DiscountMapping.strCondition =:strCondition OR DiscountMapping.strCondition IS NULL) ORDER BY buyQty ASC")
    fun getApplicableDiscountOnItem(
        productId: Int,
        itemQty: Int,
        strCondition: String
    ): List<DiscountAndProduct>

    /*@Query("SELECT * from DiscountMaster left JOIN DiscountMapping WHERE DiscountMapping.discountId == DiscountMaster.discountId and DiscountMapping.discountId = :discountId and (SELECT ifnull(sum(itemQty),0) FROM OrderTransModel LEFT JOIN DiscountMapping WHERE OrderTransModel.itemId = DiscountMapping.productId and DiscountMapping.discountId = :discountId and ((SELECT orderId from OrderMasterModel WHERE holdnumber = 1) != OrderTransModel.orderId OR (SELECT count(*) from OrderMasterModel WHERE holdnumber = 1) = 0)) >= DiscountMaster.buyQty AND (DiscountMapping.strCondition = :strCondition OR DiscountMapping.strCondition IS NULL) ORDER BY buyQty ASC")
    fun getApplicableDiscountOnItemGroup(discountId: Int, strCondition:String) : List<DiscountAndProduct>*/

    @Query("SELECT * from DiscountMaster left JOIN DiscountMapping WHERE DiscountMaster.discountType = 'Sale Discount' and DiscountMapping.discountId == DiscountMaster.discountId and DiscountMapping.discountId = :discountId and (SELECT ifnull(sum(itemQty),0) FROM OrderTransModel LEFT JOIN DiscountMapping WHERE OrderTransModel.itemId = DiscountMapping.productId and DiscountMapping.discountId = :discountId and OrderTransModel.iDiscountId != -1 and NOT EXISTS (SELECT orderId FROM OrderMasterModel WHERE OrderMasterModel.orderId = OrderTransModel.orderId and holdnumber = 1)) >= DiscountMaster.buyQty AND (DiscountMapping.strCondition = :strCondition OR DiscountMapping.strCondition IS NULL) ORDER BY buyQty ASC")
    fun getApplicableDiscountOnItemGroup(discountId: Int, strCondition:String) : List<DiscountAndProduct>


    @Query("SELECT * from DiscountMaster left JOIN DiscountMapping WHERE DiscountMapping.discountId == DiscountMaster.discountId and DiscountMapping.discountId =:iDiscountId AND (DiscountMapping.strCondition =:strCondition AND DiscountMapping.strCondition IS NOT NULL)")
    fun getFreeDiscountProduct(iDiscountId: Int, strCondition: String): List<DiscountAndProduct>

    @Query("SELECT buyQty from DiscountMaster WHERE DiscountMaster.discountId == :iDiscountId")
    fun getMinQty(iDiscountId: Int): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertItemsIntoDiscountTransaction(ItemList: DiscountTrans)

    @Query("Select COUNT(iDiscountId) From DiscountApplyModel where orderId =:orderId and type =:discountType")
    fun getItemDiscountCount(orderId: String, discountType: String): Int

    @Query("SELECT discountId from DiscountMaster ORDER BY buyQty ASC")
    fun getDiscountIds(): List<Long>

    @Query("Select * From DiscountMapping where productId =:pid and discountId=:dis_id")
    fun checkDiscountMappingItemIsAvailable(pid: Long,dis_id : Long): Boolean

    @Query("Select * From DiscountMaster where discountId =:dis_id")
    fun getDiscountMasterData(dis_id : Int): DiscountMaster

    @Query("SELECT * from DiscountMaster left JOIN DiscountMapping WHERE DiscountMapping.discountId == DiscountMaster.discountId and DiscountMapping.discountId =:dis_id AND DiscountMapping.productId =:pid")
    fun checkProductCouponDetail(pid: Long,dis_id : Long): DiscountAndProduct

}