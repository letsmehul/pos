package com.ehsm.recore.database

import android.content.Context
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteDatabase
import com.ehsm.recore.DataTypeConverter
import com.ehsm.recore.database.dao.*
import com.ehsm.recore.model.*
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CV
import com.ehsm.recore.utils.logger.LogUtil


@Database(

    entities = [CategoryModel::class,SubCategoryModel::class,ItemModel::class,OrderMasterModel::class
        ,CustomerModel::class,StateModel::class,CityModel::class,CashDrawerReportModel ::class, CashDrawerManualReportModel :: class,
        OrderHistoryModel :: class, ForceCheckOutModel :: class,EmployeeList :: class, EmployeeListWithLastAction :: class,
        OrderTransModel::class, DiscountApplyModel:: class, DiscountMaster::class,DiscountMapping::class, DiscountTrans::class,
        ProductValidationModel::class,ModifierMasterModel ::class, PreModifierMasterModel::class,IngredientMasterModel::class, ModifierMappingModel::class,ModifierMapper::class, PaymentMethodModelData::class],
    version = 4,
    autoMigrations = [
        AutoMigration (from = 3, to = 4)
    ],
    exportSchema = true
)
@TypeConverters(DataTypeConverter::class)
// change DB version to 4 and make location table
abstract class RecoreDB : RoomDatabase() {
    //list of DAO
    abstract fun daoCategoryList(): CategoryListDAO
    abstract fun daoSubCategoryList(): SubCategoryListDAO
    abstract fun daoItemList(): ItemListDAO
    abstract fun daoCustomerList(): CustomerListDAO
    abstract fun daoStateList(): StateListDAO
    abstract fun daoCityList(): CityListDAO
    abstract fun daoCashDrawerReportList(): CashDrawerReportListDAO
    abstract fun daoCashDrawerManualReportList(): CashDrawerManualReportListDAO
    abstract fun daoOrerHistoryList(): OrderHistoryListDAO
    abstract fun daoForceCheckOutList(): ForceCheckOutListDAO
    abstract fun daoEmployeeList(): EmployeeListDAO
    abstract fun daoPaymentList(): PaymentListDAO
    abstract fun daoManualSync(): ManualSyncDAO
    abstract fun daoProductValidation(): ProductValidationDAO
    abstract fun daoModifierGroupList(): ModifierGroupListDAO
    abstract fun daoPreModifierList(): PreModifierListDAO
    abstract fun daoIngredientList(): IngredientListDAO
    abstract fun daoModifierMapping(): ModifierMappingDAO
    abstract fun daoOrderTrans(): OrderTransDAO
    abstract fun daoDiscount():DiscountDAO

    companion object {
        private var INSTANCE: RecoreDB? = null
        private lateinit var context:Context

        fun getInstance(context: Context): RecoreDB? {
            this.context=context
            if (INSTANCE == null) {
                synchronized(RecoreDB::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context, RecoreDB::class.java, "recore_db")
                            .allowMainThreadQueries()
                            .fallbackToDestructiveMigration()
                            .addCallback(CALLBACK)
                            .build()
                    }

                }
            }
            return INSTANCE
        }
        private val CALLBACK = object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                LogUtil.i("LOG_TAG", "Database is created")
            }

            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                LogUtil.i("LOG_TAG", "Database is Opened")
            }

            override fun onDestructiveMigration(db: SupportSQLiteDatabase) {
                super.onDestructiveMigration(db)
                CM.setSp(context, CV.ISSYNCOPERATIONFINISHED, false)
                LogUtil.i("LOG_TAG", "Database is OnDestructiveMigration")
            }
        }


    }






}