package com.ehsm.recore.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.ehsm.recore.model.EmployeeListWithLastAction
import com.ehsm.recore.model.ItemModel

@Dao
interface ManualSyncDAO {

//    @Query("Select * From ItemModel where iProductSubcategoryId =:iProductSubcategoryId")
//    fun getItems(iProductSubcategoryId : Int): LiveData<List<ItemModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertItem(ItemList : List<EmployeeListWithLastAction>)

    @Query("Select * From EmployeeListWithLastAction where iCheckEmployeeId =:iCheckEmployeeId")
    fun getAllItems(iCheckEmployeeId :Int): EmployeeListWithLastAction

    @Query("DELETE FROM EmployeeListWithLastAction")
    fun deleteItem()
}