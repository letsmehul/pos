package com.ehsm.recore.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.ehsm.recore.model.CategoryModel
import com.ehsm.recore.model.SubCategoryModel

@Dao
interface SubCategoryListDAO {

    @Query("Select * From SubCategoryModel where iParentCategoryId =:iParentCategoryId AND tIsActive = 1 ORDER BY strProductCategoryName")
    fun getSubCategories(iParentCategoryId : Int): LiveData<List<SubCategoryModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertSubCategory(categoryList : List<SubCategoryModel>)

    @Query("DELETE FROM SubCategoryModel")
    fun deleteSubCategories()


}