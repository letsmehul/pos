package com.ehsm.recore.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.ehsm.recore.model.CategoryModel
import com.ehsm.recore.model.EmployeeList
import com.ehsm.recore.model.PaymentMethodModelData

@Dao
interface PaymentListDAO {

    @Query("Select * From PaymentMethod")
    fun getPaymentList():List<PaymentMethodModelData>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertPayment(paymentListList : List<PaymentMethodModelData>)

    @Query("DELETE FROM PaymentMethod")
    fun deletePayments()
}