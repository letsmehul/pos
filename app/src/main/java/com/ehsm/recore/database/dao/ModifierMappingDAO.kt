package com.ehsm.recore.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ehsm.recore.model.ModifierMappingModel

@Dao
interface ModifierMappingDAO {

    @Query("Select * From ModifierMappingModel where itemId =:itemId and orderId =:orderId")
    fun getModifierMapping(itemId : String,orderId : String): List<ModifierMappingModel>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertModifierMapping(modifierMappingModel: ModifierMappingModel)

    @Query("select * from OrderTransModel INNER JOIN ModifierMappingModel on OrderTransModel.orderId = ModifierMappingModel.orderId")
    fun getJoinModifierMapping(): List<ModifierMappingModel>

    @Query("DELETE FROM ModifierMappingModel")
    fun deleteModifierMapping()
}