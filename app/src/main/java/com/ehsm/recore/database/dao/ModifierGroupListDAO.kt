package com.ehsm.recore.database.dao

import androidx.room.*
import com.ehsm.recore.model.ModifierMasterModel


@Dao
interface ModifierGroupListDAO {

   @Query("Select * From ModifierMasterModel where iProductId =:iProductId")
    //@Query("SELECT * FROM ModifierMapper LEFT JOIN ModifierMasterModel ON ModifierMapper.iModifierGroupId = ModifierMasterModel.iModifierGroupId where ModifierMapper.iProductId =:iProductId GROUP by ModifierMasterModel.strPosShowName")
    fun getModifierGroupList(iProductId : Int): List<ModifierMasterModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertModifierGroup(ItemList : List<ModifierMasterModel>)

    @Query("DELETE FROM ModifierMasterModel where iProductId =:iProductId")
    fun deleteModifierGroup(iProductId : Int)

    @Query("DELETE FROM ModifierMasterModel")
    fun deleteModifier()
}