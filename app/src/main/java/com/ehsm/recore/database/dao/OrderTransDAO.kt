package com.ehsm.recore.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.ehsm.recore.model.OrderMasterModel
import com.ehsm.recore.model.OrderTransModel

@Dao
interface OrderTransDAO {

    @Query("Select * From OrderTransModel where orderId =:orderId")
    fun getOrderTrans(orderId : String): List<OrderTransModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrderTrans(ItemList : OrderTransModel)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrderTrans(ItemList : List<OrderTransModel>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrderMaster(ItemList : OrderMasterModel)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateOrderMaster(ItemList : OrderMasterModel)


    @Query("Delete From OrderMasterModel WHERE orderId = :orderId")
    fun deleteOrderFromMaster(orderId: String)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateOrderTrans(ItemList : OrderTransModel)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateOrderTransForDiscount(ItemList : OrderTransModel)

    @Query("UPDATE OrderTransModel SET itemQty=:itemQty WHERE orderId = :orderId AND itemId=:itemId")
    fun updateOrderTransQty(itemQty : Int,orderId : String,itemId : Int)

    @Query("UPDATE OrderTransModel SET strInstruction=:itemInstruction WHERE orderId = :orderId AND itemId=:itemId")
    fun updateOrderTransInstruction(itemInstruction : String,orderId : String,itemId : Int)

    @Query("UPDATE OrderTransModel SET strInstruction=:itemInstruction WHERE orderId = :orderId AND itemId=:itemId AND id=:Id")
    fun updateOrderTransInstruction_(itemInstruction : String,orderId : String,itemId : Int,Id : String)


    @Query("UPDATE OrderTransModel SET itemQty=:itemQty, itemAmount=:itemAmount, taxAmount=:taxAmount WHERE orderId = :orderId AND itemId=:itemId")
    fun updateOrderTrans2(itemQty : Int, itemAmount: Double, taxAmount : String,orderId : String,itemId : Int)

    @Query("UPDATE OrderTransModel SET itemQty=:itemQty, itemAmount=:itemAmount, taxAmount=:taxAmount WHERE orderId = :orderId AND itemId=:itemId AND id=:Id")
    fun updateOrderTrans2_(itemQty : Int, itemAmount: Double, taxAmount : String,orderId : String,itemId : Int,Id : String)


    @Query("UPDATE OrderTransModel SET itemQty=:itemQty, itemAmount=:itemAmount, taxAmount=:taxAmount, iDiscountId=:iDiscountId, discountRemark=:discountRemark ,discountAmount =:discountAmount  WHERE orderId = :orderId AND itemId=:itemId")
    fun updateOrderTrans3(itemQty : Int, itemAmount: Double, taxAmount : String,iDiscountId : Int?=0,discountRemark:String?="",discountAmount:Float?=0.0f, orderId : String,itemId : Int)

    @Query("Select * From OrderTransModel where orderId =:orderId AND itemId =:item_id")
    fun getOrderTransItem(orderId : String, item_id : Int): List<OrderTransModel>

    @Query("Select * From OrderTransModel where reason =:reason")
    fun getHoldOrderItem(reason : String): List<OrderTransModel>

    @Query("UPDATE OrderTransModel SET reason=:reason WHERE orderId = :orderId AND itemId=:itemId")
    fun updateHoldOrder(reason : String,orderId : String,itemId : Int)

    @Query("Delete From OrderTransModel WHERE id = :id")
    fun deleteById(id: String)

    @Query("Delete From OrderTransModel WHERE itemId = :itemId")
    fun deleteByItemId(itemId: Int)

    @Query("Delete From OrderTransModel WHERE itemId = :itemId and id = :Id and orderId= :orderId")
    fun deleteByItemIdAndId(itemId: Int,orderId: String,Id: String)

    @Query("DELETE From OrderTransModel WHERE orderId = :orderId")
    fun deleteOrder(orderId: String)

    @Query("Select ingredientName from OrderTransModel WHERE orderId = :orderId and itemId = :itemId and premodifierType = :premodifier")
    fun getOrderTransModifier(orderId: String, premodifier: String, itemId: Int): String

    @Query("Select premodifierType from OrderTransModel WHERE orderId = :orderId and itemId = :itemId ")
    fun getPreModifierType(orderId: String, itemId: Int): List<String>

    @Query("SELECT COUNT(premodifierType) FROM OrderTransModel WHERE premodifierType = :premodifier")
    fun getPreModifierCount(premodifier: String): Int

   /* @Query("Select premodifierType from OrderTransModel WHERE orderId = :orderId and itemId = :itemId ")
    fun getPreModifierTypeTemp(orderId: String, itemId: Int): String


    @Query("UPDATE OrderTransModel SET premodifierType=:modifierData, ingredientName=:displayString WHERE orderId = :orderId AND itemId=:itemId")
    fun updateModifier(modifierData: String, displayString: String, orderId: String, itemId: Int)

    @Query("UPDATE OrderTransModel SET premodifierType=:modifierData WHERE orderId = :orderId AND itemId=:itemId")
    fun updateModifierpremodifierType(modifierData: String, orderId: String, itemId: Int)

    @Query("UPDATE OrderTransModel SET itemRate=:itemRate, itemAmount=:itemAmount, taxAmount=:taxAmount WHERE orderId = :orderId AND itemId=:itemId")
    fun updatePriceAmountTax(
        itemRate: Double,
        itemAmount: Double,
        taxAmount: String,
        orderId: String,
        itemId: Int
    )*/

    @Query("Select premodifierType From OrderTransModel where orderId =:orderId AND itemId =:item_id")
    fun getAppliedModifierPerItemOnOrder(orderId : String, item_id : Int): String

    @Query("Select premodifierType From OrderTransModel where orderId =:orderId AND itemId =:item_id AND id=:Id")
    fun getAppliedModifierPerItemOnOrder_(orderId : String, item_id : Int,Id : String): String

    @Query("Select * From OrderTransModel where orderId =:orderId AND itemId =:item_id")
    fun getOrderTransItemByProductId(orderId : String, item_id : Int): List<OrderTransModel>

    @Query("Select premodifierType from OrderTransModel WHERE orderId = :orderId and itemId = :itemId ")
    fun getPreModifierTypeTemp(orderId: String, itemId: Int): String

    @Query("Select premodifierType from OrderTransModel WHERE orderId = :orderId and itemId = :itemId AND id=:Id")
    fun getPreModifierTypeTemp_(orderId: String, itemId: Int,Id : String): String

    @Query("UPDATE OrderTransModel SET premodifierType=:modifierData WHERE orderId = :orderId AND itemId=:itemId")
    fun updateModifierpremodifierType(modifierData: String, orderId: String, itemId: Int)

    @Query("UPDATE OrderTransModel SET premodifierType=:premodifierType, ingredientName=:ingredientName WHERE orderId = :orderId AND itemId=:itemId")
    fun updateModifier(premodifierType: String, ingredientName: String, orderId: String, itemId: Int)

    @Query("UPDATE OrderTransModel SET premodifierType=:premodifierType, ingredientName=:ingredientName WHERE orderId = :orderId AND itemId=:itemId AND id=:Id")
    fun updateModifier_(premodifierType: String, ingredientName: String, orderId: String, itemId: Int,Id : String)

    @Query("UPDATE OrderTransModel SET ingredientName=:ingredientName WHERE orderId = :orderId")
    fun updateOrderTransModifier(orderId: String, ingredientName: String)

    @Query("UPDATE OrderTransModel SET itemRate=:itemRate, itemAmount=:itemAmount, taxAmount=:taxAmount WHERE orderId = :orderId AND itemId=:itemId")
    fun updatePriceAmountTax(
        itemRate: Double,
        itemAmount: Double,
        taxAmount: String,
        orderId: String,
        itemId: Int
    )

    @Query("UPDATE OrderTransModel SET itemRate=:itemRate, itemAmount=:itemAmount, taxAmount=:taxAmount WHERE orderId = :orderId AND itemId=:itemId and id=:Id")
    fun updatePriceAmountTax_(
        itemRate: Double,
        itemAmount: Double,
        taxAmount: String,
        orderId: String,
        itemId: Int,
        Id : String
    )

    @Query("DELETE FROM OrderMasterModel")
    fun deleteOrderMasrter()

    @Query("DELETE FROM OrderTransModel")
    fun deleteOrderTrans()

    @Query("DELETE FROM OrderTransModel WHERE NOT EXISTS (SELECT orderId FROM OrderMasterModel WHERE OrderMasterModel.orderId = OrderTransModel.orderId and holdnumber = 1)")
    fun deleteWithoutHoldOrderTrans()
}