package com.ehsm.recore.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ehsm.recore.model.CategoryModel
import com.ehsm.recore.model.ProductValidationModel

@Dao
interface ProductValidationDAO {

    @Query("Select * From ProductValidationModel where orderId =:orderId")
    fun getProductValidation(orderId : String): ProductValidationModel

    @Query("Select * From ProductValidationModel where validationType =:validationType and orderId =:orderId")
    fun getProductValidationForAge(validationType : String,orderId: String): ProductValidationModel

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertProductValidation(categoryList : ProductValidationModel)

    @Query("DELETE FROM ProductValidationModel where orderId =:orderId")
    fun deleteProductValidation(orderId : String)

    @Query("DELETE FROM ProductValidationModel")
    fun deleteAllPreModifier()
}