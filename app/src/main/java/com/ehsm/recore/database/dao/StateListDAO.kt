package com.ehsm.recore.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.ehsm.recore.model.CategoryModel
import com.ehsm.recore.model.StateModel

@Dao
interface StateListDAO {

    @Query("Select * From StateModel")
    fun getStates(): LiveData<List<StateModel>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertState(stateList : List<StateModel>)

    @Query("DELETE FROM StateModel")
    fun deleteState()
}