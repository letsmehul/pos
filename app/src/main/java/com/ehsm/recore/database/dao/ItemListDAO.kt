package com.ehsm.recore.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.ehsm.recore.model.ItemModel


@Dao
interface ItemListDAO {

    @Query("Select * From ItemModel where iProductSubcategoryId =:iProductSubcategoryId AND tIsActive = 1 ORDER BY strProductName COLLATE NOCASE ASC")
    fun getItems(iProductSubcategoryId : Int): LiveData<List<ItemModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertItem(ItemList : List<ItemModel>)

    @Query("Select * From ItemModel where iProductID =:iProductID AND tIsActive = 1")
    fun getItem(iProductID : Int): ItemModel

    @Query("Select * From ItemModel ORDER BY strProductName")
    fun getAllItems(): List<ItemModel>

    @Query("DELETE FROM ItemModel")
    fun deleteProducts()


    @Query("Select * From ItemModel where tIsActive = 1 AND (strBarCode LIKE '%' || :searchText  || '%' OR strProductName LIKE '%' || :searchText  || '%' OR subCategoryName LIKE '%' || :searchText  || '%' OR dblRetailPrice LIKE '%' || :searchText  || '%' OR strProductCode LIKE '%' || :searchText  || '%' OR strProductCategoryName LIKE '%' || :searchText  || '%') order by strProductName COLLATE NOCASE ASC LIMIT:limit OFFSET:offset")
    suspend fun getSearchItems(searchText:String , limit:Int,offset:Int): List<ItemModel>

    @Query("Select * From ItemModel where strBarCode = :barcode AND tIsActive = 1 ORDER BY strProductName COLLATE NOCASE ASC")
    fun getSearchProductByBarcode(barcode:String): ItemModel

    @Query("Select * From ItemModel where tBestSeller = 1 AND tIsActive = 1 ORDER BY strProductName COLLATE NOCASE ASC")
     fun getBestSellerProduct(): List<ItemModel>
}