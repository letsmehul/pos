package com.ehsm.recore.database.dao

import androidx.room.*
import com.ehsm.recore.model.*


@Dao
interface IngredientListDAO {

    //@Query("Select * From IngredientMasterModel where iModifierGroupId =:iModifierGroupId")
    //@Query("SELECT * FROM ModifierMapper LEFT JOIN IngredientMasterModel ON ModifierMapper.iModifierGroupOptionId = IngredientMasterModel.iModifierGroupOptionId where ModifierMapper.iModifierGroupId =:iModifierGroupId AND IngredientMasterModel.iModifierGroupId =:iModifierGroupId GROUP by IngredientMasterModel.strOptionName")
    @Query("Select * From IngredientMasterModel where iModifierGroupId =:iModifierGroupId AND iProductId =:iProductId ")
    fun getIngredientList(iModifierGroupId : String, iProductId: Int): List<IngredientMasterModel>

    @Query("Select strOptionName From IngredientMasterModel where iModifierGroupId =:iModifierGroupId AND iProductId =:iProductId AND tIsDefault=1")
    fun getdefaultIngredient(iModifierGroupId : String, iProductId: Int): String

    @Query("Select * From IngredientMasterModel where iModifierGroupId =:iModifierGroupId AND iProductId =:iProductId AND tIsDefault=1")
    fun getdefaultIngredientmodel(iModifierGroupId : String, iProductId: Int): IngredientMasterModel

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertIngredient(IngredientMasterModel : List<IngredientMasterModel>)

    @Query("DELETE FROM IngredientMasterModel where iModifierGroupId =:iModifierGroupId")
    fun deleteIngredient(iModifierGroupId : String)

    @Query("DELETE FROM IngredientMasterModel where iProductId =:iProductId")
    fun deleteIngredientByProductId(iProductId : Int)

    @Query("DELETE FROM IngredientMasterModel")
    fun deleteIngredient()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertModiferMapper(modifierMapper : ModifierMapper)

    @Query("DELETE FROM ModifierMapper where iProductId =:iProductId")
    fun deleteModifierMapperByProductId(iProductId : Int)

    @Query("DELETE FROM ModifierMapper")
    fun deleteModifierMapper()
}