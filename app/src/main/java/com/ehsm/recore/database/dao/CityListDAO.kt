package com.ehsm.recore.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.ehsm.recore.model.CategoryModel
import com.ehsm.recore.model.CityModel
import com.ehsm.recore.model.StateModel

@Dao
interface CityListDAO {

    @Query("Select * From CityModel  where iStateId =:iStateId")
    fun getCity(iStateId : Int): LiveData<List<CityModel>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertCity(stateList : List<CityModel>)

    @Query("DELETE FROM CityModel")
    fun deleteCity()
}