package com.ehsm.recore.network

import com.ehsm.recore.model.*

class BoltApiHelper(private val  boltApiService: BoltApiService) {

    suspend fun getTerminals(merchantId :String) =boltApiService.getTerminals(BaseRequest(merchantId))

    suspend fun connect(merchantId :String,hsn:String,force :Boolean) =boltApiService.connect(RequestPingOrConnect(merchantId,hsn,force))

    suspend fun ping(merchantId :String,hsn:String) =boltApiService.ping(
        RequestPreConnect(merchantId,hsn)
    )

    suspend fun disconnect(merchantId :String,hsn:String) =boltApiService.disconnect(
        RequestPreConnect(merchantId,hsn)
    )

    suspend fun cancel(merchantId :String,hsn:String) =boltApiService.cancel(
        RequestPreConnect(merchantId,hsn)
    )

    suspend fun display(merchantId :String,hsn:String,text:String) =boltApiService.display(
        RequestDisplay(merchantId,hsn,text)
    )

    suspend fun readConfirmation(requestReadSignature: RequestReadSignature) =boltApiService.readConfirmation(requestReadSignature)

    suspend fun readInput(readInput: ReadInput) =boltApiService.readInput(readInput)

    suspend fun readCard(requestReadCard: RequestReadCard) =boltApiService.readCard(requestReadCard)

    suspend fun authCard(cardRequestModel: CardRequestModel) =boltApiService.authCard(cardRequestModel)

    suspend fun authManual(requestReadCard: RequestReadCard) =boltApiService.authManual(requestReadCard)
    suspend fun readManual(manualCardReadRequestModel: ManualCardReadRequestModel) =boltApiService.readManual(manualCardReadRequestModel)

}