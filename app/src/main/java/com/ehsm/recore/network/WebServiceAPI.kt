package com.kcspl.divyangapp.network

import com.ehsm.recore.model.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*


/**
 **The web API interface**
 *  Please define all your web API extensions here
 */
interface WebServiceAPI {


    @FormUrlEncoded
    @POST("api/apiv1/login")
    fun logIn(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("fcm") fcm: String,
        @Field("mac_address") mac_address: String
    ): Call<LoginResponseModel>

    @GET("api/apiv1/login_data")
    fun logInData(): Call<LoginResponseModel>

    @FormUrlEncoded
    @POST("api/apiv1/verify_pin")
    fun verifyMPin(@Field("iEmployeePin") iEmployeePin: String): Call<VerifyMPinResponseModel>

    @GET("api/apiv1/category_list")
    fun getCategoryList(@Query("sync_date_time") syncDateTime: String): Call<CategoryListResponseModel>


    @POST("api/apiv1/terminal_list")
    fun getTerminalList(): Call<TerminalListResponseModel>

    @FormUrlEncoded
    @POST("api/apiv1/apk_version_check")
    fun getAPKVersionCheck(@Field("version_code") version_code: String): Call<ResponseModel>

    @GET
    fun downloadFileFromUrl(@Url fileUrl: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("api/apiv1/sub_category_list")
    fun getSubCategoryList(@Field("iParentCategoryId") iParentCategoryId: String,@Field("sync_date_time") subCatSyncDateTime: String): Call<SubCategoryListResponseModel>

    @FormUrlEncoded
    @POST("api/apiv1/product_list")
    fun getItemList(@Field("iProductSubcategoryId") iProductSubcategoryId: String,@Field("sync_date_time") prodSyncDateTime: String): Call<ItemListResponseModel>

    @FormUrlEncoded
    @POST("api/apiv1/order_generate")
    fun placeOrder(
        @Field("iCheckDetailID") iCheckDetailID: String,
        @Field("order_id") order_id: String,
        @Field("iCustomerId") iCustomerId: Int,
        @Field("iCheckEmployeId") iCheckEmployeId: Int,
        @Field("dblCheckGrossTotal") dblCheckGrossTotal: Double,
        @Field("dblCheckTaxTotal") dblCheckTaxTotal: Double,
        @Field("dblCheckDiscounts") dblCheckDiscounts: Double,
        @Field("dblCheckNetTotal") dblCheckNetTotal: Double,
        @Field("productJSON") productJSON: String,
        @Field("paymentJSON") paymentJSON: String,
        @Field("discountJSON") discountJSON: String,
        @Field("customer_email") customerEmail: String,
        @Field("tIsVoided") tIsVoided: String ="0",
        @Field("tCheckTaxExempt") tCheckTaxExempt: String,
        @Field("tIsApplePay") tIsApplePay: Boolean,
        @Field("strPaymentType") strPaymentType: String,
        @Field("strCheckStatus") strCheckStatus: String
    ): Call<NewSaleandRefundModel>

    @FormUrlEncoded
    @POST("api/apiv1/update_order_payment")
    fun Update_order_payment(
        @Field("iCheckDetailID") iCheckDetailID: String,
        @Field("paymentJSON") paymentJSON: String,
        @Field("strPaymentType") strPaymentType: String
        ): Call<NewSaleandRefundModel>

    @FormUrlEncoded
    @POST("api/apiv1/update_order_paymentmethod")
    fun Update_Payment_type(
        @Field("iCheckDetailID") iCheckDetailID: String,
        @Field("strPaymentMethodTmp") strPaymentType: String
    ): Call<ResponseModel>

    @FormUrlEncoded
    @POST("api/apiv1/hold_order_customer_exist")
    fun hold_order_customer_exist(
        @Field("iCustomerId") iCustomerId: String
    ): Call<ResponseModel>

    @FormUrlEncoded
    @POST("api/apiv1/send_order_receipt")
    fun sendOrderReceipt(
        @Field("customer_email") customerEmail: String,
        @Field("order_id") order_id: Int
    ): Call<ResponseModel>

    @FormUrlEncoded
    @POST("api/apiv1/add_update_bolt")
    fun sendBoltrequest(
        @Field("BoltReqLogsId") BoltReqLogsId: String,
        @Field("iCheckDetailID") iCheckDetailID: String,
        @Field("strRequestBody") strRequestBody: String,
        @Field("strResponseBody") strResponseBody: String,
        @Field("strConnectRequestBody") strConnectRequestBody: String,
        @Field("strConnectResponseBody") strConnectResponseBody: String

    ): Call<BoltModel>

    @FormUrlEncoded
    @POST("api/apiv1/forgot_password")
    fun forgotPassword(@Field("strUserName") username: String): Call<ResponseModel>

    @FormUrlEncoded
    @POST("api/apiv1/customers_list")
    fun getCustomerList(@Field("search") search: String): Call<CustomerResponseModel>

    @FormUrlEncoded
    @POST("api/apiv1/add_customer")
    fun addCustomer(
        @Field("strFirstName") strFirstName: String,
        @Field("strLastName") strLastName: String,
        @Field("strEmail") strEmail: String,
        @Field("strPhone") strPhone: String,
        @Field("strRemark") strRemark: String,
        @Field("strGender") strGender: String,
        @Field("strStatus") strStatus: String,
        @Field("strAddress1") strAddress1: String,
        @Field("strAddress2") strAddress2: String,
        @Field("iCityId") iCityId: Int,
        @Field("iStateId") iStateId: Int,
        @Field("strZipCode") strZipCode: String,
        @Field("dateDob") dateDob: String,
        @Field("iCustomerId") iCustomerId: String

    ): Call<AddCustomerResponseModel>

    @GET("api/apiv1/state_list")
    fun getStateList(): Call<StateListResponseModel>

    @FormUrlEncoded
    @POST("api/apiv1/city_list")
    fun getCityList(@Field("iStateId") search: Int): Call<CityListResponseModel>

    @FormUrlEncoded
    @POST("api/apiv1/change_pin")
    fun changeMpin(
        @Field("iCheckEmployeeId") iCheckEmployeeId: Int,
        @Field("iMpin") iMpin: Int,
        @Field("iMpin_old") iMpin_old: Int
    ): Call<ResponseModel>

    @FormUrlEncoded
    @POST("api/apiv1/check_in_data")
    fun getCheckInData(
        @Field("iCheckEmployeeId") iCheckEmployeeId: Int
    ): Call<CheckInDataResponseModel>

    @FormUrlEncoded
    @POST("api/apiv1/check_in_out")
    fun checkInOutData(
        @Field("dateTnx") dateTnx: String,
        @Field("dblAmount") dblAmount: Double,
        @Field("dblHighCash") dblHighCash: Double,
        @Field("dblShortCash") dblShortCash: Double,
        @Field("strTnxtype") strTnxtype: String,
        @Field("iCheckEmployeeId") iCheckEmployeeId: Int,
        @Field("strRemark") strRemark: String,
        @Field("dblCashInDrawer") dblCashInDrawer: Double,
        @Field("iCreatedUserId") iCreatedUserId : Int,
        @Field("tIsForceCheckOut") tIsForceCheckOut : Int,
        @Field("tIsCashCollectByAdmin") bitCashCollectByAdmin : Int
    ): Call<ClockOutDataResponseModel>
    @FormUrlEncoded
    @POST("api/apiv1/break_start_end")
    fun breakInOutData(
        @Field("dateTnx") dateTnx: String,
        @Field("strTnxtype") strTnxtype: String,
        @Field("iCheckEmployeeId") iCheckEmployeeId: Int
    ): Call<ResponseModel>


    @FormUrlEncoded
    @POST("api/apiv1/check_out_data")
    fun getCheckOutData(
        @Field("iCheckEmployeeId") iCheckEmployeeId: Int
    ): Call<CheckOutDataResponseModel>

    @FormUrlEncoded
    @POST("api/apiv1/cash_drawer_report")
    fun getCashDrawerReportData(
        @Field("searchDate") searchDate: String
    ): Call<CashDrawerReportResponseModel>

    @FormUrlEncoded
    @POST("api/apiv1/cash_drawer_report_manual")
    fun getCashDrawerManualReportData(
        @Field("searchDate") searchDate: String
    ): Call<CashDrawerManualReportResponseModel>

    @FormUrlEncoded
    @POST("api/apiv1/order_list")
    fun getOrderHistory(
        @Field("date_start") date_start: String,
        @Field("date_end") date_end: String,
        @Field("type") type: String
    ): Call<OrderHistoryResponseModel>

    @FormUrlEncoded
    @POST("api/apiv1/order_list_pagination")
    fun getOrderHistoryPagination(
        @Field("date_start") date_start: String,
        @Field("date_end") date_end: String,
        @Field("type") type: String,
        @Field("currentpage") currentpage : Int,
        @Field("search") search :String,
        @Field("strCheckStatus") strCheckStatus :String
    ): Call<OrderHistoryResponseModel>

    @FormUrlEncoded
    @POST("api/apiv1/force_checkout_list")
    fun getForceCheckOutList(
        @Field("search_date") search_date: String): Call<ForceCheckOutListResponseModel>


    @FormUrlEncoded
    @POST("api/apiv1/payment_refund")
    fun paymentRefund(
        @Field("dblRefundAmount") dblRefundAmount: Double,
        @Field("dateRefundDate") dateRefundDate: String,
        @Field("iCheckDetailID") iCheckDetailID: String,
        @Field("strRefundType") strRefundType: String,
        @Field("merchantId") merchantId : String,
        @Field("retref") retref: String,
        @Field("strRefundReason") strRefundReason: String,
        @Field("strRefundStatus") strRefundStatus: String,
        @Field("iCheckEmployeeId") iCheckEmployeeId: Int,
        @Field("dblRefundTaxAmount") dblRefundTaxAmount: Double,
        @Field("productJSON") productJSON: String,
        @Field("dblDiscountAmount") dblDiscountAmount: Double
    ): Call<NewSaleandRefundModel>

    @FormUrlEncoded
    @POST("api/apiv1/employee_last_action")
    fun getLastAction(
        @Field("iCheckEmployeeId") iCheckEmployeeId: Int): Call<LastActionResponseModel>

    @FormUrlEncoded
    @POST("api/apiv1/employee_break_action")
    fun getBreakAction(
        @Field("iCheckEmployeeId") iCheckEmployeeId: Int): Call<BreakInOutResponse>

  //  @FormUrlEncoded
    @POST("api/apiv1/pos_last_status")
    fun getLastActionDayStartClose(): Call<POSLastStatusResponseModel>


    @FormUrlEncoded
    @POST("api/apiv1/employee_list")
    fun getEmployeeList(@Field("search") search: String): Call<EmployeeListResponseModel>

    @FormUrlEncoded
    @POST("api/apiv1/sync_manual")
    fun manualsync(@Field("iCheckEmployeeId") iCheckEmployeeId: Int): Call<ManualSyncResponseModel>

    @FormUrlEncoded
    @POST("api/apiv1/pos_status_update")
    fun statusUpdate(@Field("date_time") date_time: String): Call<ResponseModel>

    //api for discount
    @FormUrlEncoded
    @POST("api/apiv1/discount_list")
    fun getDiscountList(
        @Field("iCheckEmployeeId") iCheckEmployeeId: Int): Call<DiscountResponse>

    @FormUrlEncoded
    @POST("api/apiv1/discount_sale_bogo_list")
    fun getSalesBogoDiscountList(
        @Field("iCheckEmployeeId") iCheckEmployeeId: Int): Call<DiscountResponse>

    @FormUrlEncoded
    @POST("api/apiv1/check_payment_status")
    fun checkPayment(@Field("order_id") orderId: String,@Field("merchantId") merchantId: String): Call<CheckPaymentResponse>

    @GET("api/apiv1/payment_method_list")
    fun getPaymentList(): Call<PaymentMethodModel>

    @GET("recorepos_check")
    fun getStatus(@Query("id") id : String): Call<ResponseModel>
}
