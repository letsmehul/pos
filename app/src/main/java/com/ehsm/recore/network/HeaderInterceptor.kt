package com.ehsm.recore.network

import android.text.TextUtils
import android.util.Log
import com.ehsm.recore.Recore
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CV.HEADER_X_CARDCONNECT_SESSIONKEY
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class HeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
        val response = chain.proceed(request)
        if(response.request.url.toString().endsWith("v2/connect")){
            var x_key_body = response.headers["X-CardConnect-SessionKey"]?.split(";")?.get(0)
            if(!TextUtils.isEmpty(x_key_body)){
                Recore.getApp()?.applicationContext?.let {
                    val sharedPref = CM.getEncryptedSharedPreferences(
                        it
                    )
                    sharedPref.edit().putString(HEADER_X_CARDCONNECT_SESSIONKEY,x_key_body).commit()
                }
                //Log.d("Session Got ", x_key_body)
            }
        }
        return response;
    }
}