package com.ehsm.recore.network

import android.annotation.SuppressLint
import android.content.Context
import android.text.TextUtils
import com.ehsm.recore.BuildConfig
import com.ehsm.recore.Recore
import com.ehsm.recore.utils.AppConstant
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CV
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit


/**
 *This class is used in API Request and Response time
 * Using RestClient instance api call also configure network timeout and log interceptor
 *
 */
class BoltRestAPIClient {

    private val CONNECT_TIME_OUT_SEC = 60
    private val READ_TIME_OUT_SEC = 60
    /**
     * Private constructor for singleton purpose
     */
    //private val BASE_URL = "https://bolt-uat.cardpointe.com/api/"
    private val BASE_URL = "https://bolt.cardpointe.com/api/"



    /**
     * The API reference
     */
    private var service: BoltApiService? = null


    internal lateinit var retrofit: Retrofit

    /*
    *
    *               Internal helper and initializer
    *****************************************************************************
     */
    private fun initRetrofit() {

        val gson = GsonBuilder()
            .setLenient()
            .create()
        retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addConverterFactory(nullOnEmptyConverterFactory)

            .client(getHttpClient())
            .build()
        service = retrofit.create(BoltApiService::class.java)
    }

    private val nullOnEmptyConverterFactory = object : Converter.Factory() {
        fun converterFactory() = this
        override fun responseBodyConverter(type: Type, annotations: Array<out Annotation>, retrofit: Retrofit) =
            object : Converter<ResponseBody, Any?> {
                val nextResponseBodyConverter =
                    retrofit.nextResponseBodyConverter<Any?>(converterFactory(), type, annotations)

                override fun convert(value: ResponseBody) =
                    if (value.contentLength() != 0L) nextResponseBodyConverter.convert(value) else null
            }
    }
    private fun getHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
            .connectTimeout(CONNECT_TIME_OUT_SEC.toLong(), TimeUnit.SECONDS)
            .readTimeout(READ_TIME_OUT_SEC.toLong(), TimeUnit.SECONDS)
            .addInterceptor(customHeaderInterceptor())
            .addInterceptor(loggingInterceptor())
            .addInterceptor(HeaderInterceptor())
        return  builder.build()
    }
    /**
     * @return Interceptor that provides logging
     */
    private fun loggingInterceptor(): Interceptor {
        val logging = HttpLoggingInterceptor()
        // set your desired log level
        if (BuildConfig.DEBUG) {
            logging.level = HttpLoggingInterceptor.Level.BODY
        } else {
            logging.level = HttpLoggingInterceptor.Level.NONE
        }
        return logging
    }

    companion object {
        @SuppressLint("StaticFieldLeak")
        private var webServiceClient: BoltRestAPIClient? = null

        private var mContext: Context? = null
        /**
         * will init retrofit. needs to be called before using API. preferably from Application class
         *
         * @param context
         */
        fun init(context: Context) {
            if (webServiceClient == null) {
                webServiceClient = BoltRestAPIClient()
                webServiceClient!!.initRetrofit()

            }
            mContext = context
        }

        val retrofitClient: Retrofit
            get() = webServiceClient!!.retrofit

        /**
         * @return Web API
         */
        fun getService(): BoltApiService? {
            if (webServiceClient == null) {
                throw IllegalStateException("Please initialise retrofit first")
            }

            return webServiceClient!!.service
        }
    }

    private fun customHeaderInterceptor(): Interceptor {
        return Interceptor { chain ->
            val original = chain.request()
            var x_key_body:String? =""
            Recore.getApp()?.applicationContext?.let {
                val sharedPref = CM.getEncryptedSharedPreferences(
                    it
                )
                x_key_body = sharedPref.getString(CV.HEADER_X_CARDCONNECT_SESSIONKEY,"")

            }
            var authKeyValue: String? = ""
            Recore.getApp()?.applicationContext?.let {
                val sharedPref = CM.getEncryptedSharedPreferences(it)
                authKeyValue = sharedPref.getString(AppConstant.HEADER_Authorization, "")
            }

            val requestBuilder = original.newBuilder()
                .header(CV.HEADER_CONTENT_KEY, CV.HEADER_CONTENT_TYPE_BOLT)
                .header(CV.HEADER_Authorization, authKeyValue?:"")
                .header(CV.HEADER_X_CARDCONNECT_SESSIONKEY,x_key_body ?: "")
                .method(original.method, original.body)
            val request = requestBuilder.build()
            chain.proceed(request)
        }
    }
}

