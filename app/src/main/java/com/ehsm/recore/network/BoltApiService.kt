package com.ehsm.recore.network

import com.ehsm.recore.model.*
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Url

interface BoltApiService {

    @POST("v2/listTerminals")
    suspend fun getTerminals(@Body baseRequest: BaseRequest): Terminals

    @POST("v2/connect")
    suspend fun connect(@Body requestPingOrConnect: RequestPingOrConnect): Unit

    @POST("v2/ping")
    suspend fun ping(@Body requestPreConnect: RequestPreConnect): ConnectResponse

    @POST("v2/disconnect")
    suspend fun disconnect(@Body requestPreConnect: RequestPreConnect)

    @POST("v2/cancel")
    suspend fun cancel(@Body requestPreConnect: RequestPreConnect)

    @POST("v2/readConfirmation")
    suspend fun readConfirmation(@Body requestReadSignature: RequestReadSignature):ReadConfirmationResponse

    @POST("v2/readInput")
    suspend fun readInput(@Body readInput: ReadInput):ReadInputResponse


    @POST("v3/authCard")
    suspend fun authCard(@Body requestReadCard: CardRequestModel):CardResponseModel

    @POST("v2/display")
    suspend fun display(@Body requestDisplay: RequestDisplay): Terminals

    @POST("v3/authManual")
    suspend fun authManual(@Body requestReadCard: RequestReadCard):CardResponseModel

    @POST("v2/readCard")
    suspend fun readCard(@Body requestReadCard: RequestReadCard):ConnectResponse

    @POST("v2/readSignature")
    suspend fun readSignature(@Body requestReadSignature: RequestReadSignature):ConnectResponse

    @POST("v2/readManual")
    suspend fun readManual(@Body manualCardReadRequestModel: ManualCardReadRequestModel):ManualCardResponseModel




}