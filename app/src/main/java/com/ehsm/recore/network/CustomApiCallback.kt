package com.kcspl.divyangapp.network

import android.text.TextUtils
import android.util.Log
import com.ehsm.recore.model.ResponseModel
import com.ehsm.recore.network.RestClient
import com.ehsm.recore.utils.logger.LogUtil
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

abstract class CustomApiCallback<T : ResponseModel> : Callback<T> {

    override fun onResponse(call: Call<T>, response: Response<T>) {

        val data = getResponse(response, ResponseModel::class.java as Class<T>)
        val isError = isErrorInWebResponse(data!!.status)

        if (isError) {            // if error found display popup with message
            if (data.message.isNotEmpty()) {
                if (data != null && data.action == null) {
                    showErrorMessage(data.message)
                } else {
                    if (!TextUtils.isEmpty(data.action)) {
                        showErrorMessage(data.message, data.action)
                    }else if(!TextUtils.isEmpty(data.latest_apk_url)){
                        showErrorMessage(data.message, data.latest_apk_url)
                    }
                    else {
                        showErrorMessage(data.message)

                    }
                }
            } else {
                showErrorMessage("Something went wrong. Please try again")
            }
        } else {
            handleResponseData(response.body())
        }
    }

    protected abstract fun handleResponseData(data: T?)

    protected abstract fun showErrorMessage(errormessage: String?)

    open fun showErrorMessage(errormessage: String?, action: String) {}

    override fun onFailure(call: Call<T>, t: Throwable) {
        if (isKnownException(t)) {
            showErrorMessage("Unable to connect with internet Check your Network Connectivity")
        } else {
            showErrorMessage("Something went wrong. Please try again")
        }
        LogUtil.d("ffdkfdf",t.message)
    }

    //for showing known exception
    private fun isKnownException(t: Throwable): Boolean {
        return (t is ConnectException
                || t is UnknownHostException
                || t is SocketTimeoutException
                || t is IOException)
    }

    // check whether is there any error in web response
    private fun isErrorInWebResponse(statusCode: Boolean): Boolean {
        // Handler handler = new Handler();
        val isError: Boolean = when (statusCode) {
            true -> false
            else -> true
        }
        return isError
    }

    //Checking response code if not proper or null then showing this message
    private fun <T> getResponse(tResponse: Response<T>, tClass: Class<T>): T? {
        if (tResponse.code() in 200..299) {
            var t = tResponse.body()

            if (t == null) {
                t = GsonBuilder().create().fromJson(createErrorMsgJson(), tClass)
            }
            return t
        } else {
            val errorConverter =
                RestClient.retrofitClient.responseBodyConverter<T>(tClass, arrayOfNulls(0))
            try {
                Log.e("tResponse.errorBody()", "" + Gson().toJson(tResponse.errorBody()))
                return errorConverter.convert(tResponse.errorBody()!!)
            } catch (e: Exception) {
                e.printStackTrace()
                return GsonBuilder().create().fromJson(createErrorMsgJson(), tClass)
            }

        }
    }

    private fun createErrorMsgJson(): String {
        return "\n" +
                "{\n" +
                "  \"Status\": true,\n" +
                "  \"StatusCode\": 0,\n" +
                "  \"Message\": \"Due to network connection error we\\'re having trouble\",\n" +
                "  \"MemberLeaveListResultModel\": {\n" +
                "  \n" +
                "  }\n" +
                "}"
    }
}