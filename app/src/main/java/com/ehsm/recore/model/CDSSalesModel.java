package com.ehsm.recore.model;

import java.util.ArrayList;

public class CDSSalesModel {

    String OrderId;
    String BilledPersonName;

    String SaleDateTime;
    String CashierName;

    String Subtotal;
    String TaxApplied;
    String Discount;
    String TotalSavings;
    String DiscountRemark;
    String GrandTotal;

    String Change_due;
    String Total_paid_amount;


    ArrayList<PrinterSalesProductsModel> mPrinterSalesProductsModel;

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getBilledPersonName() {
        return BilledPersonName;
    }

    public void setBilledPersonName(String billedPersonName) {
        BilledPersonName = billedPersonName;
    }


    public String getSubtotal() {
        return Subtotal;
    }

    public void setSubtotal(String subtotal) {
        Subtotal = subtotal;
    }

    public String getTaxApplied() {
        return TaxApplied;
    }

    public void setTaxApplied(String taxApplied) {
        TaxApplied = taxApplied;
    }

    public String getDiscount() {
        return Discount;
    }

    public void setDiscount(String discount) {
        Discount = discount;
    }

    public String getTotalSavings() {
        return TotalSavings;
    }

    public void setTotalSavings(String totalSavings) {
        TotalSavings = totalSavings;
    }

    public String getDiscountRemark() {
        return DiscountRemark;
    }

    public void setDiscountRemark(String discountRemark) {
        DiscountRemark = discountRemark;
    }
    public String getGrandTotal() {
        return GrandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        GrandTotal = grandTotal;
    }


    public String getSaleDateTime() {
        return SaleDateTime;
    }

    public void setSaleDateTime(String mSaleDateTime) {
        SaleDateTime = mSaleDateTime;
    }

    public String getCashierName() {
        return CashierName;
    }

    public void setCashierName(String cashierName) {
        CashierName = cashierName;
    }


    public ArrayList<PrinterSalesProductsModel> getSalesProducts() {
        return mPrinterSalesProductsModel;
    }

    public void setSalesProducts(ArrayList<PrinterSalesProductsModel> mPrinterSalesProductsModel) {
        this.mPrinterSalesProductsModel = mPrinterSalesProductsModel;
    }

    public String getChange_due() {
        return Change_due;
    }

    public void setChange_due(String change_due) {
        Change_due = change_due;
    }

    public String getTotal_paid_amount() {
        return Total_paid_amount;
    }

    public void setTotal_paid_amount(String total_paid_amount) {
        Total_paid_amount = total_paid_amount;
    }
}
