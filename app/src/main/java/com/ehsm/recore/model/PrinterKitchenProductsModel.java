package com.ehsm.recore.model;

public class PrinterKitchenProductsModel {

    String ProductName;
    String Quantity;
    String ModifierIngredient;
    String Instruction;


    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getModifierIngredient() {
        return ModifierIngredient;
    }

    public void setModifierIngredient(String modifierIngredient) {
        ModifierIngredient = modifierIngredient;
    }

    public String getInstruction() {
        return Instruction;
    }

    public void setInstruction(String instruction) {
        Instruction = instruction;
    }
}
