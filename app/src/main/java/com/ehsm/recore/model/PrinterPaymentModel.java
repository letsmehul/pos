package com.ehsm.recore.model;

public class PrinterPaymentModel {

    String mPaymentMode = null;
    String mCardType = null;
    String mCardNumber = null;
    String mCardHolderName = null;
    String mCustomerPaid = null;
    String mChangeDue = null;
    String mTransactionIdDate = null;
    String mTransactionNumber = null;
    String mStrSignature = null;
    String mTenderedAmount = null;
    String paymentModeTitle = null;
    boolean isOtherCashPayment = false;

    public boolean isOtherCashPayment() {
        return isOtherCashPayment;
    }

    public void setOtherCashPayment(boolean otherCashPayment) {
        isOtherCashPayment = otherCashPayment;
    }

    public String getmPaymentMode() {
        return mPaymentMode;
    }

    public void setmPaymentMode(String mPaymentMode) {
        this.mPaymentMode = mPaymentMode;
    }

    public String getmCardType() {
        return mCardType;
    }

    public void setmCardType(String mCardType) {
        this.mCardType = mCardType;
    }

    public String getmCardNumber() {
        return mCardNumber;
    }

    public void setmCardNumber(String mCardNumber) {
        this.mCardNumber = mCardNumber;
    }

    public String getmCardHolderName() {
        return mCardHolderName;
    }

    public void setmCardHolderName(String mCardHolderName) {
        this.mCardHolderName = mCardHolderName;
    }

    public String getmCustomerPaid() {
        return mCustomerPaid;
    }

    public void setmCustomerPaid(String mCustomerPaid) {
        this.mCustomerPaid = mCustomerPaid;
    }

    public String getmChangeDue() {
        return mChangeDue;
    }

    public void setmChangeDue(String mChangeDue) {
        this.mChangeDue = mChangeDue;
    }

    public String getmTransactionIdDate() {
        return mTransactionIdDate;
    }

    public void setmTransactionIdDate(String mTransactionIdDate) {
        this.mTransactionIdDate = mTransactionIdDate;
    }

    public String getmTransactionNumber() {
        return mTransactionNumber;
    }

    public void setmTransactionNumber(String mTransactionNumber) {
        this.mTransactionNumber = mTransactionNumber;
    }

    public String getmStrSignature() {
        return mStrSignature;
    }

    public void setmStrSignature(String mStrSignature) {
        this.mStrSignature = mStrSignature;
    }

    public String getmTenderedAmount() {
        return mTenderedAmount;
    }

    public void setmTenderedAmount(String mTenderedAmount) {
        this.mTenderedAmount = mTenderedAmount;
    }

    public String getPaymentModeTitle() {
        return paymentModeTitle;
    }

    public void setPaymentModeTitle(String paymentModeTitle) {
        this.paymentModeTitle = paymentModeTitle;
    }
}
