package com.ehsm.recore.model;

import java.util.ArrayList;

public class PrinterSalesModel {

    String StoreName;
    String AddressLine1;
    String AddressLine2;
    String TelNumber;
    String StoreLogoUrl;
    String StoreLogoLocalPath;

    String BilledPersonName;
    String InvoiceNo;

    String TotalQty;
    String Subtotal;
    String TaxApplied;
    String Discount;
    String DiscountRemark;
    String TotalSavings;
    String GrandTotal;

    String CustomerPaid;

    String ReceiptDateTime;
    String CashierName;
    String POSStationNo;
    String EmpRole;

    String PaymentMode;
    ArrayList<PrinterPaymentSalesModel> paymentModes = new ArrayList<>();
    String CardType;
    String CardHolderName;
    String CardLastFoutDigits;
    String CardTransactionNo;

    String RemainingAmount;
    String SignatureBase64;

    String facebook;
    String instagram;
    String twitter;
    String youtube;
    String strSocialMedia = "";

    public String getStrSocialMedia() {
        return strSocialMedia;
    }

    public void setStrSocialMedia(String strSocialMedia) {
        this.strSocialMedia = strSocialMedia;
    }



    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    ArrayList<PrinterSalesProductsModel> mPrinterSalesProductsModel = new ArrayList<>();

    public String getStoreName() {
        return StoreName;
    }

    public void setStoreName(String storeName) {
        StoreName = storeName;
    }

    public String getAddressLine1() {
        return AddressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        AddressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return AddressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        AddressLine2 = addressLine2;
    }

    public String getTelNumber() {
        return TelNumber;
    }

    public void setTelNumber(String telNumber) {
        TelNumber = telNumber;
    }

    public String getStoreLogoLocalPath() {
        return StoreLogoLocalPath;
    }

    public void setStoreLogoLocalPath(String storeLogoLocalPath) {
        StoreLogoLocalPath = storeLogoLocalPath;
    }

    public String getBilledPersonName() {
        return BilledPersonName;
    }

    public void setBilledPersonName(String billedPersonName) {
        BilledPersonName = billedPersonName;
    }

    public String getInvoiceNo() {
        return InvoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        InvoiceNo = invoiceNo;
    }

    public String getSubtotal() {
        return Subtotal;
    }

    public String getTotalQty() {
        return TotalQty;
    }

    public void setTotalQty(String totalQty) {
        TotalQty = totalQty;
    }

    public void setSubtotal(String subtotal) {
        Subtotal = subtotal;
    }

    public String getTaxApplied() {
        return TaxApplied;
    }

    public void setTaxApplied(String taxApplied) {
        TaxApplied = taxApplied;
    }

    public String getDiscount() {
        return Discount;
    }

    public void setDiscount(String discount) {
        Discount = discount;
    }

    public String getDiscountRemark() {
        return DiscountRemark;
    }

    public void setDiscountRemark(String discountRemark) {
        DiscountRemark = discountRemark;
    }

    public String getTotalSavings() {
        return TotalSavings;
    }

    public void setTotalSavings(String totalSavings) {
        TotalSavings = totalSavings;
    }

    public String getGrandTotal() {
        return GrandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        GrandTotal = grandTotal;
    }

    public String getCustomerPaid() {
        return CustomerPaid;
    }

    public void setCustomerPaid(String customerPaid) {
        CustomerPaid = customerPaid;
    }

    public String getReceiptDateTime() {
        return ReceiptDateTime;
    }

    public void setReceiptDateTime(String mReceiptDateTime) {
        ReceiptDateTime = mReceiptDateTime;
    }

    public String getCashierName() {
        return CashierName;
    }

    public void setCashierName(String cashierName) {
        CashierName = cashierName;
    }

    public String getPOSStationNo() {
        return POSStationNo;
    }

    public void setPOSStationNo(String POSStationNo) {
        this.POSStationNo = POSStationNo;
    }

    public ArrayList<PrinterSalesProductsModel> getSalesProducts() {
        return mPrinterSalesProductsModel;
    }

    public void setSalesProducts(ArrayList<PrinterSalesProductsModel> mPrinterSalesProductsModel) {
        this.mPrinterSalesProductsModel = mPrinterSalesProductsModel;
    }

    public String getPaymentMode() {
        return PaymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        PaymentMode = paymentMode;
    }

    public String getCardType() {
        return CardType;
    }

    public void setCardType(String cardType) {
        CardType = cardType;
    }

    public String getCardHolderName() {
        return CardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        CardHolderName = cardHolderName;
    }

    public String getCardLastFoutDigits() {
        return CardLastFoutDigits;
    }

    public void setCardLastFoutDigits(String cardLastFoutDigits) {
        CardLastFoutDigits = cardLastFoutDigits;
    }

    public String getCardTransactionNo() {
        return CardTransactionNo;
    }

    public void setCardTransactionNo(String cardTransactionNo) {
        CardTransactionNo = cardTransactionNo;
    }

    public String getEmpRole() {
        return EmpRole;
    }

    public void setEmpRole(String empRole) {
        EmpRole = empRole;
    }

    public String getStoreLogoUrl() {
        return StoreLogoUrl;
    }

    public void setStoreLogoUrl(String storeLogoUrl) {
        StoreLogoUrl = storeLogoUrl;
    }

    public String getRemainingAmount() {
        return RemainingAmount;
    }

    public void setRemainingAmount(String remainingAmount) {
        RemainingAmount = remainingAmount;
    }

    public String getSignatureBase64() {
        return SignatureBase64;
    }

    public void setSignatureBase64(String signatureBase64) {
        SignatureBase64 = signatureBase64;
    }

    public ArrayList<PrinterPaymentSalesModel> getPaymentModes() {
        return paymentModes;
    }

    public void setPaymentModes(ArrayList<PrinterPaymentSalesModel> paymentModes) {
        this.paymentModes = paymentModes;
    }
}
