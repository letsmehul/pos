package com.ehsm.recore.model

import androidx.annotation.Nullable
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName


/**
 * common models
 */

open class ResponseModel(
    var status: Boolean = false,
    var message: String = "",
    val action: String = "",
    var sync_date_time: String = "",
    var total_pages: Int = 0,
    var latest_apk_url: String = ""

)

data class DataWrapper<T>(
    var data: T? = null,
    var message: String? = null,
    var status: Boolean = false,
    var action: String = "",
    var latest_apk_url: String = ""
)


data class LoginResponseModel(
    val data: LogInModel
) : ResponseModel()

data class AddCustomerResponseModel(
    val data: AddCustomerModel
) : ResponseModel()

data class AddCustomerModel(
    var customer_id: String
)

data class LogInModel(
    var iUserLogins: Int,
    var posName: String,
    var strUserName: String,
    var store_id: Int,
    var strApiKey: String,
    var strGmt: String,
    var strTimeZone: String,
    var strTimeZoneGmt: String,
    var strCurrencyCode: String,
    var strCurrencyName: String,
    var strCountryName: String,
    var iPaymentMethodId: Int,
    var strPaymentMethod: String,
    var strCurrencysymbol: String,
    var strStoreName: String,
    var strAddressLine1: String,
    var strAddressLine2: String,
    var iCityId: Int,
    var iStateId: Int,
    var strZipcode: String,
    var strPhone: String,
    var strLogo: String,
    var iPayInOut: Int,
    var strBoltAuthKey: String,
    var strBoltMerchantId: String,
    var strScreenLockTimer: Long,
    var strFullScreen: String,
    var strSquareImageLogo: String,
    var strPromotionalAds: String,
    var strPromotionalAdsType: String,
    var tIsTransactionalRights: Int = 0,
    var dblSignatureAmount: Float = 0.5f,
    var strFacebook: String? = "",
    var strInstagram: String? = "",
    var strTwitter: String? = "",
    var strYoutube: String? = "",
    var strSocialMedia: String = ""

)

data class VerifyMPinResponseModel(
    val data: VerifyMPin
) : ResponseModel()

data class VerifyMPin(
    var iCheckEmployeeId: Int,
    var strFirstName: String,
    var strLastName: String,
    var strProfilePhoto: String,
    var iEmployeeRoleId: Int,
    var strEmployeeRoleName: String,
    var lastEmployeeStatus: String,
    var tIsOpenDiscount: Int

)


data class NewSaleandRefundModel(
    val data: NewSaleandRefundModelData
) : ResponseModel()

data class BoltModel(
    val data: BoltresponseModel
) : ResponseModel()

data class NewSaleandRefundModelData(
    var strInvoiceNumber: String,
    var datetimeCheckBusinessDate: String,
    var order_id: Int,
    var orderPayments: List<OrderPaymentsModel>?
)

data class BoltresponseModel(
    var bolt_id: String
)

data class CategoryListResponseModel(
    val data: ArrayList<CategoryModel>
) : ResponseModel()


data class TerminalListResponseModel(
    val data: ArrayList<TerminalModel>
) : ResponseModel()

data class PaymentMethodModel(
    val data: ArrayList<PaymentMethodModelData>
) : ResponseModel()

@Entity(indices = [Index(value = arrayOf("data"), unique = true)])
data class TerminalModel(
    var hsn: String,
    var name: String
) {
    override fun toString(): String {
        return "$hsn - $name"
    }
}

@Entity(indices = [Index(value = arrayOf("iProductCategoryID"), unique = true)])
data class CategoryModel(
    @PrimaryKey
    var iProductCategoryID: Int,
    var strProductCategoryName: String,
    var strProductCategoryDescription: String,
    var iDisplayOrder: Int,
    var strProductCategoryImage: String,
    var tIsActive: Int
)

data class SubCategoryListResponseModel(
    val data: ArrayList<SubCategoryModel>
) : ResponseModel()

@Entity(indices = [Index(value = arrayOf("iProductCategoryID"), unique = true)])
data class SubCategoryModel(
    @PrimaryKey
    var iProductCategoryID: Int,
    var iParentCategoryId: Int,
    var strProductCategoryName: String,
    var strProductCategoryDescription: String,
    var strProductParentCategory: String,
    var tIsActive: Int
)

data class ItemListResponseModel(
    val data: ArrayList<ItemModel>
) : ResponseModel()

@Entity(indices = [Index(value = arrayOf("iProductID"), unique = true)])
data class ItemModel(
    @PrimaryKey
    var iProductID: Int,
    var strProductName: String,
    var strProductCategoryName: String,
    var subCategoryName: String,
    var dblRetailPrice: String,
    var strMeasurementUnit: String,
    var strBarCode: String,
    var strProductCode: String,
    var iProductCategoryId: Int,
    var iProductSubcategoryId: Int,
    var strProductImage: String,
    var strProductDescription: String,
    var strPricingType: String,
    var tBestSeller: Int,
    var iAgeLimit: Int,
    var totalTax: Double,
    var tIsActive: Int,
    var strProductType: String,
    var tIsOpenDiscount: Int
) {
    @Ignore
    var modifierArr: List<ModifierMasterModel>? = null
}


@Entity(indices = [Index(value = arrayOf("id"), unique = true)])
data class ModifierMappingModel(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var itemId: String,
    var orderId: String,
    var modifierId: String,
    var preModifierId: String,
    var ingrediantId: String,
    var ingrediantPrice: String,
    var premodifierType: String
)

@Entity
data class ModifierMapper(
    var iModifierGroupId: Int,
    var iModifierGroupOptionId: String,
    var iProductID: Int,
    var iPreModifierId: String

) {
    @PrimaryKey(autoGenerate = true)
    var iModifierMapperId = 0
}

@Entity(indices = [Index(value = arrayOf("id"), unique = true)])
data class ModifierMasterModel(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var iModifierGroupId: Int,
    var strModifierGroupName: String,
    @Nullable
    var strPosShowName: String? = "",
    var strIsRequired: String,
    var strPricingMethod: String,
    var dblGroupLevelAdditionalCharge: String,
    var iOptionMin: Int,
    var iOptionMax: Int,
    var iPreModifierGroupId: Int,
    var tIsActive: Int,
    var iFranchiseStoreId: Int,
    var iFranchiseMasterId: Int,
    var iBusinessUnitid: String,
    var dateTimeCreated: String,
    var dateTimeLastModified: String,
    var iCreatedUserId: Int,
    var iModifiedUserId: Int,
    var strPreModifierGroupName: String,
    var iProductId: Int
) {
    @Ignore
    var PreModifier: List<PreModifierMasterModel>? = null

    @Ignore
    var ModifierGroupOption: List<IngredientMasterModel>? = null
}

@Entity(indices = [Index(value = arrayOf("id"), unique = true)])
data class PreModifierMasterModel(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var iPreModifierId: String,
    var iPreModifierGroupId: String,
    var strPreModifierName: String,
    var strDisplayMode: String,
    @Nullable
    var strPosShowName: String? = "",
    var strKitchenShowName: String,
    var iModifierGroupId: Int,
    var iProductId: Int
) {
    @Ignore
    var isPreModifireSelected: Boolean = false
}

@Entity(indices = [Index(value = arrayOf("id"), unique = true)])
data class IngredientMasterModel(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var iModifierGroupOptionId: String,
    var iModifierGroupId: Int,
    var strOptionName: String,
    var dblOptionPrice: String,
    var tIsDefault: Int,
    var isSelected: Boolean = false,
    var iProductId: Int
) {
    @Ignore
    var isIngredientHighlited: Boolean = false
}

data class TaxModel(
    @PrimaryKey
    var iTaxMasterID: Int,
    var iProductCategoryTaxID: String,
    var strTaxName: String,
    var dblTaxRate: Double,
    var strTaxDescription: String
)

@Entity(indices = [Index(value = arrayOf("orderId"), unique = true)])
data class OrderMasterModel(
    @PrimaryKey
    var orderId: String,
    var holdnumber: Int,
    var customerId: Int,
    var totalAmount: Double,
    var totalTax: Double,
    var totalDiscountLDouble: Float,
    var createdDate: Long,
    var tCheckTaxExempt: String
)

/*@Entity(primaryKeys = ["id","orderId"], indices = [Index(value = arrayOf("id"), unique = true)] )*/
@Entity(indices = [Index(value = arrayOf("id"), unique = true)])
data class OrderTransModel(
    @PrimaryKey
    var id: String,
    var orderId: String,
    var itemId: Int,
    var itemName: String,
    var itemRate: Double,
    var itemQty: Int,
    var itemAmount: Double,
    var strPricingType: String,
    var paymentType: String,
    var dateTime: String,
    var taxPercentage: String = "0.0",
    var taxAmount: String,
    var reason: String,
    var itemQtyLeft: Int,
    var grandTotal: Double,
    var isRefunded: Boolean,
    var strProductType: String,
    var iDiscountId: Int = 0,
    var discountRemark: String = "",
    var discountAmount: Float = 0.0f,
    var premodifierType: String = "",
    var ingredientName: String = "",
    var strInstruction: String = "",
    var kitchenServedquantity: Int=0,
    var isKitchenPrintServed: Boolean,
    var tIsOpenDiscount: Int = 0,
    var totalItemQty: Int = 0,
    var refundProductId: Int = 0
)

data class ModifierRequestModel(
    var iProductModifierGroupId: String,
    var strOptionName: String,
    var dblOptionPrice: Double,
    var strPreModifierName: String
)

@Entity(tableName = "DiscountTransaction")
data class DiscountTrans(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var orderId: String,
    var TransId: String,
    var iDiscountId: Int,
    var discountName: String,
    var discountDescription: String,
    var discountType: String,
    var strType: String,
    var dblValue: Float
)

@Entity(tableName = "PaymentMethod")
data class PaymentMethodModelData(
    @PrimaryKey
    var iPaymentMethodId: Int,
    var strPaymentType: String,
    var strPaymentMethod: String
) {
    @Ignore
    override fun toString(): String = strPaymentMethod
}


@Entity(indices = [Index(value = arrayOf("iDiscountId"), unique = true)])
data class DiscountApplyModel(
    @PrimaryKey
    var orderId: String,
    val iDiscountId: Int,
    val item_id: String,
    val strDiscountName: String,
    val type: String,
    val strType: String,
    val dblValue: Float,
    val discount_amount: Float,
    val strAppliedTo: String,
    val minCartAmount: Int = 0
)

data class OrderTransRequestModel(
    @PrimaryKey
    var iProductID: String,
    var dblItemQty: String,
    var dblPrice: String,
    var dblAmount: String,
    var dblItemTaxPercentage: String,
    var dblItemTaxAmount: String,
    var dblItemDiscount: String?,
    var iDiscountId: Int?,
    var strRemark: String?,
    var tIsVoided: String,
    var dblVoidedQty: String,
    var dblVoidedPrice: String,
    var dblVoidedAmount: String,
    var dateCheckBusinessDay: String,
    var datetimeItemAdded: String,
    var strModifierJson : String,
    var productModifier: ArrayList<ModifierRequestModel>,
    var cartLevelDiscount: Boolean,
    var tIsOpenDiscount: Int,
    var strInstruction:String,
    var kitchenServedquantity: Int=0
)


data class PaymentRequestModel(
    @PrimaryKey
    var iPaymentQty: String,
    var dblGrossAmount: String,
    var dblNetAmount: String,
    var strPaymentMethod: String,
    var strPaymentCardLast4: String,
    var datePaymentDate: String,
    var strHsn: String = "",
    var strMerchantId: String = "",
    var strBoltOrderId: String = "",
    var strcardHolderName: String = "",
    var strBatchId: String = "",
    var strRetRef: String = "",
    var strAvsResp: String = "",
    var dblGIvenAmount: String,
    var dblGIvenChange: String,
    var strSignature: String = "",
    var strCardType: String,
    var tIsApplePay: Boolean
)

data class EmvTagData(
    @SerializedName("Application Label")
    val applicationLabel: String
)


data class DiscountRequestModel(
    @PrimaryKey
    var dblDiscountAmt: String,
    var iDiscountId: String,
    var strRemark: String,
    val strDiscountType: String,
    val strType: String,
    val dblValue: Float,
    val strAppliedTo: String,
    val dblMinCartAmount: Int = 0
)

data class CustomerResponseModel(
    val data: ArrayList<CustomerModel>
) : ResponseModel()

@Entity(indices = [Index(value = arrayOf("iCustomerId"), unique = true)])
data class CustomerModel(
    @PrimaryKey
    var iCustomerId: Int,
    var strFirstName: String,
    var strLastName: String,
    var strPhone: String,
    var strEmail: String,
    var strRemark: String,
    var strGender: String,
    var strStatus: String,
    var strAddress1: String,
    var strAddress2: String,
    var iCityId: Int,
    var iStateId: Int,
    var strZipCode: String,
    var dateDob: String,
    var iIsWalkIn: Int
)

@Entity(indices = [Index(value = arrayOf("id"), unique = true)])
data class ProductValidationModel(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var orderId: String,
    var validationType: String,
    var status: Boolean
)


data class StateListResponseModel(
    val data: ArrayList<StateModel>
) : ResponseModel()

@Entity(indices = [Index(value = arrayOf("iStateID"), unique = true)])
data class StateModel(
    @PrimaryKey
    var iStateID: Int,
    var strStateCode: String,
    var strStateName: String
)

data class CityListResponseModel(
    val data: ArrayList<CityModel>
) : ResponseModel()

@Entity(indices = [Index(value = arrayOf("iCityID"), unique = true)])
data class CityModel(
    @PrimaryKey
    var iCityID: Int,
    var itmp: Int,
    var iStateId: Int,
    var strCityName: String,
    var strCountry: String,
    var strLat: String,
    var strLon: String
)

data class CheckInDataResponseModel(
    val data: CheckInData
) : ResponseModel()

data class CheckInData(
    var dblCashInDrawer: String
)

data class CheckOutDataResponseModel(
    val data: CheckOutData
) : ResponseModel()

data class CheckOutData(
    var startDay: String, // last added key
    var dblCashInDrawer: String, // last checkin amount
    var salesCash: String, // total gross aMONUT
    var manualCashWithdrawal: String, // no sale cash withdarw sum
    var balanceCashInDrawer: String, // last key + cash paymetn - withdrwal
    var totalSalesNumber: String,
    var totalSalesByCard: String,
    var totalSalesByCash: String,
    var totalSalesAmountByCard: String,
    var totalRefundAmount: String,
    var totalPayInAmount: String,
    var totalOutInAmount: String,
    var totalSalesByOther: String,
    var totalSalesByOtherCount: String,
    var totalTxnAmount: String,
    var totalRefundCount: Int,
    var totalSplitAmount: String,
    var otherPaymentMethodDetails :ArrayList<OtherPaymentModel>
)

data class OtherPaymentModel(
    var payment_method: String,
    var total_sale_number: Int,
    var total_sale_amount: String
)

data class ClockOutDataResponseModel(
    val data: ClockOutData
) : ResponseModel()

data class ClockOutData(
    var posStationName: String,
    var employeeName: String,
    var employeeRole: String,
    var clockInTime: String,
    var clockOutTime: String,
    var totalWorkedHours: String
)


data class CashDrawerReportResponseModel(
    val data: ArrayList<CashDrawerReportModel>
) : ResponseModel()

@Entity(indices = [Index(value = arrayOf("id"), unique = true)])
data class CashDrawerReportModel(
    @PrimaryKey
    var id: String,
    var tnxDate: String,
    var amount: String,
    var tnxType: String,
    var iPosLoginId: Int,
    var dateTimeCreated: String,
    var employeeName: String,
    var invoiceNumber: String,
    var customerName: String
)

data class CashDrawerManualReportResponseModel(
    val data: ArrayList<CashDrawerManualReportModel>
) : ResponseModel()

@Entity(indices = [Index(value = arrayOf("id"), unique = true)])
data class CashDrawerManualReportModel(
    @PrimaryKey
    var id: String,
    var dateTnx: String,
    var user: String,
    var dblAmount: String,
    var strTnxtype: String,
    var dateTimeCreated: String,
    var strRemark: String = "",
    var dblCashInDrawer: String
)

data class OrderHistoryResponseModel(
    val data: ArrayList<OrderHistoryModel>
) : ResponseModel()

@Entity(indices = [Index(value = arrayOf("iCheckDetailID"), unique = true)])
data class OrderHistoryModel(
    @PrimaryKey
    var iCheckDetailID: String,
    var status: String,
    var createdByUser:String,
    var modifiedByUser:String,
    var iBusinessUnitID: Int,
    var datetimeCheckBusinessDate: String,
    var iCustomerId: String,
    var strFirstName: String,
    var strLastName: String,
    var dblCheckGrossTotal: String,
    var dblCheckNetTotal: String,
    var dblCheckTaxTotal: String,
    var strInvoiceNumber: String,
    var orderProducts: List<OrderProductsModel>?,
    var orderDiscounts: List<OrderDiscountsModel>?,
    var orderPayments: List<OrderPaymentsModel>?,
    var orderRefund: List<OrderRefundModel>?,
    var tCheckTaxExempt: String,
    var isKitchenPrintServed: Boolean
) {
    @Ignore
    var employeeData: List<EmployeeDataModel>? = null
}

data class EmployeeDataModel(
    var strFirstName: String,
    var strEmployeeRoleName: String
)

data class OrderRefundModel(
    var iCheckPaymentRefundID: Int,
    var iCheckDetailID: String,
    var dateRefundDate: String,
    var dblRefundAmount: String,
    var strRefundType: String,
    var strRefundReason: String,
    var strRefundStatus: String,
    var dblDiscountAmount: String,
    var refundItems: List<OrderRefundItemModel>?
) {
    @Ignore
    var refundEmployeeData: List<EmployeeDataModel>? = null
}

data class OrderRefundItemModel(
    var iPaymentRefundCheckItemDerailID: Int,
    var iCheckPaymentRefundId: Int,
    var iCheckItemDetailId: Int,
    var dblItemQty: Int,
    var dblRefundAmount: String,
    var strRefundReason: String,
    var dblRefundTaxAmount: String,
    var iProductId: Int
)

data class OrderPaymentsModel(
    var dblNetAmount: String,
    var dblGrossAmount: String,
    var iPaymentQty: Int,
    var datetimeBusinessDay: String,
    var datePaymentDate: String,
    var strPaymentMethod: String,
    var iPaymentMethodID: String,
    var strSignature: String,
    var dblGIvenChange: String,
    var dblGIvenAmount: String,
    var strPaymentCardLast4: String,
    var strCardType: String,
    @SerializedName("strHsn")
    val strHsn: String,
    @SerializedName("strMerchantId")
    val strMerchantId: String,
    @SerializedName("strBoltOrderId")
    val strBoltOrderId: String,
    @SerializedName("strcardHolderName")
    val strcardHolderName: String,
    @SerializedName("strBatchId")
    val strBatchId: String,
    @SerializedName("strRetRef")
    val strRetRef: String,
    @SerializedName("strAvsResp")
    val strAvsResp: String
)

data class OrderDiscountsModel(
    var datetimeDiscountApplied: String,
    var iCheckDetailID: String,
    var strDiscountName: String,
    var strDescription: String,
    var dblDiscountAmt: Float,
    var iDiscountId: Int,
    var strRemark: String?,
    val strDiscountType: String?,
    val strType: String?,
    val dblValue: Float,
    val strAppliedTo: String?,
    var dblMinCartAmount: Int = 0
)

data class OrderProductsModel(
    var dateCheckBusinessDay: String,
    var strProductName: String,
    var dblItemQty: String,
    var dblPrice: String,
    var dblAmount: String,
    var strPricingType: String,
    var dblItemTaxPercentage: String,
    var dblItemTaxAmount: String,
    var dblItemDiscount: String,
    var iDiscountId: String,
    var iProductId: Int,
    var iCheckItemDetailId: String,
    var autoId: String,
    var productModifier: List<ProductModifier> = ArrayList(),
    var strRemark: String = "",
    var strProductType: String,
    var premodifierType: String = "",
    var ingredientName: String = "",
    var strInstruction: String="",
    var kitchenServedquantity: Int=0,
    var isKitchenPrintServed: Boolean,
    var tIsOpenDiscount: Int = 0
)

data class ProductModifier(
    var iCheckItemModifierGroupOptionId: String,
    var iCheckItemDetailID: String,
    var iProductModifierGroupId: String,
    var strModifierGroupName: String,
    var strPreModifierName: String,
    var strOptionName: String,
    var dblOptionPrice: String,
    var datetimeCreated: String
)

data class ForceCheckOutListResponseModel(
    val data: ArrayList<ForceCheckOutModel>
) : ResponseModel()

@Entity(indices = [Index(value = arrayOf("iCheckEmployeeId"), unique = true)])
data class ForceCheckOutModel(
    @PrimaryKey
    var iCheckEmployeeId: Int,
    var strFirstName: String,
    var strLastName: String,
    var strEmployeeRoleName: String,
    var iEmployeeRoleId: Int,
    var dateTnx: String,
    var dateTimeCreated: String,
    var posName: String,
    var strUserName: String,
    var strTnxtype: String,
    var pos_status: String,
    var posLastStatusUpdateDate: String
)

data class RefundRequestModel(
    @PrimaryKey
    var iCheckItemDetailId: String,
    var dblItemQty: String,
    var dblRefundAmount: String,
    var strRefundReason: String,
    var dblRefundTaxAmount: Double
)


data class LastActionResponseModel(
    val data: LastAction
) : ResponseModel()

data class LastAction(
    var iDayOpenCloseBalanceID: Int,
    var dateTnx: String,
    var dblAmount: String,
    var strTnxtype: String,
    var iPosLoginId: Int,
    var iFranchiseStoreId: Int,
    var dateTimeCreated: String,
    var dateTimeLastModified: String,
    var iCreatedUserId: Int,
    var iModifiedUserId: Int,
    var strRemark: String,
    var dblCashInDrawer: String,
    var iEmployeeId: Int,
    var tIsForceCheckOut: Int,
    var iBusinessUnitId: Int,
    var iFranchiseMasterId: Int,
    var posName: String,
    var strUserName: String,
    var isBreakStarted : Boolean = false
)

data class BreakInOutResponse(
    var isBreakStart: Boolean,
    var isBreakEnd: Boolean,
    var breakStartData: String,
    var breakEndData: String
):ResponseModel()

data class POSLastStatusResponseModel(
    val data: POSLastStatus
) : ResponseModel()

data class POSLastStatus(
    var iDayOpenCloseBalanceID: Int,
    var dateTnx: String,
    var dblAmount: String,
    var strTnxtype: String,
    var iPosLoginId: Int,
    var iFranchiseStoreId: Int,
    var dateTimeCreated: String,
    var dateTimeLastModified: Int,
    var iCreatedUserId: Int,
    var iModifiedUserId: Int,
    var strRemark: Int,
    var dblCashInDrawer: String,
    var iEmployeeId: Int,
    var iBusinessUnitId: Int,
    var iFranchiseMasterId: Int,
    var posName: String,
    var strUserName: String,
    var emp_name:String
)

data class EmployeeListResponseModel(
    val data: ArrayList<EmployeeList>
) : ResponseModel()

@Entity(indices = [Index(value = arrayOf("iCheckEmployeeId"), unique = true)])
data class EmployeeList(
    @PrimaryKey
    var iCheckEmployeeId: Int,
    var strFirstName: String,
    var strLastName: String,
    var strGender: String,
    var dateTimeDOB: String,
    var strHomePhone: String,
    var strCellPhone: String,
    var strPrimaryEmail: String,
    var strAddressLine1: String,
    var strAddressLine2: String,
    var iCityId: Int,
    var iStateId: Int,
    var strZipCode: String,
    var strCountry: String,
    var tIsActive: String,
    var iMpin: String,
    var iFranchiseStoreId: Int,
    var iBusinessUnitId: Int,
    var iEmployeeRoleId: Int,
    var strEmployeeRoleName: String
)

data class ManualSyncResponseModel(
    val employee_list_with_last_activity: ArrayList<EmployeeListWithLastAction>
) : ResponseModel()

@Entity(indices = [Index(value = arrayOf("iCheckEmployeeId"), unique = true)])
data class EmployeeListWithLastAction(
    @PrimaryKey
    var iCheckEmployeeId: Int,
    var iBusinessUnitId: Int,
    var strSourceEmployeeId: String,
    var iInsightPayEmployeeId: String,
    var strFirstName: String,
    var strLastName: String,
    var strGender: String,
    var dateTimeDOB: String,
    var strHomePhone: String,
    var strCellPhone: String,
    var strPrimaryEmail: String,
    var strAddressLine1: String,
    var strAddressLine2: String,
    var iCityId: Int,
    var iStateId: Int,
    var strZipCode: String,
    var strCountry: String,
    var tIsActive: Int,
    var dateTimeCreated: String,
    var dateTimeLastModified: String,
    var iMpin: String,
    var iFranchiseStoreId: Int,
    var strProfilePhoto: String,
    var iRefId: Int,
    var iEmployeeRoleId: Int,
    var iFranchiseMasterId: Int,
    var strEmployeeRoleName: String,
    var last_activity: List<LastActivityModel>
)

data class LastActivityModel(
    var dateTnx: String,
    var dateTimeCreated: String,
    var posName: String,
    var strUserName: String,
    var strTnxtype: String,
    var iPosLoginId: Int,
    var dblCashInDrawer: String
)

//========================= local model ========

//data class CashDrawerReport(
//    var id: String,
//    var user: String,
//    var cash_in_out: String,
//    var invoice_no: String,
//    var amount: String,
//    var client: String
//)

//data class CashDraweManualAddrReport(
//    var id: String,
//    var user: String,
//    var cash_in_out: String,
//    var amount: String,
//    var date: String
//)

//data class SubCategoryModel(
//    var sub_category_id: String,
//    var category_id: String,
//    var category_image: Int,
//    var category_name: String
//)

//data class ItemModel(
//    var item_id: String,
//    var sub_category_id: String,
//    var item_image: Int,
//    var item_name: String,
//    var item_rate: String
//)

data class OrderModel(
    var order_id: String,
    var order_no: String,
    var price: String,
    var customer: String,
    var status: String
)

data class OrdeItemModel(
    var order_id: String,
    var item_name: String,
    var discription: String,
    var price: String,
    var item_image: String,
    var item_qty: String,
    var item_amount: String
)

//data class CustomerModel(
//    var customer_id: String,
//    var customer_name: String,
//    var customer_image: String,
//    var customer_phone_no: String
//)

data class UserModel(
    var user_id: String,
    var user_name: String,
    var user_image: String,
    var user_phone_no: String,
    var user_email: String
)

data class ReceiptModel(
    var receipt_id: String,
    var receipt_no: String,
    var receipt_amount: String,
    var receipt_time: String
)


data class CartSummaryData(
    val totalQty: Int,
    val totalAmount: Double,
    val taxAmount: Double,
    val totalDiscount: Float
)

data class CartRefundSummaryData(
    val totalQty: Int,
    val totalAmount: Double,
    val taxAmount: Double,
    val saleTaxAmount: Double,
    val saleDiscountAmount: Float,
    val saleTotalAmount: Double,
    val totalDiscount: Float
)

data class PaymentModel(
    @PrimaryKey
    var iPaymentId: String,
    var strPaymentName: String,
    var strPaymentAmount: String,
    var mTransactionNumber: String,
    var currencySymbol: String,
    var cardType: String,
    var isSplit: Boolean
)

data class CheckPaymentResponse(
    val data: CardResponseModel
) : ResponseModel()