package com.ehsm.recore.model;

import java.util.ArrayList;

public class KitchenPrinterData {

    String BilledPersonName;
    String InvoiceNo;
    String OrderDate;
    String OrderTime;
    String POSName;
    String customerName;
    String rollName;
    String OrderStatus;

    ArrayList<PrinterKitchenProductsModel> mPrinterKitchenProductsModel;

    public String getBilledPersonName() {
        return BilledPersonName;
    }

    public void setBilledPersonName(String billedPersonName) {
        BilledPersonName = billedPersonName;
    }

    public String getInvoiceNo() {
        return InvoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        InvoiceNo = invoiceNo;
    }

    public String getOrderDate() {
        return OrderDate;
    }

    public void setOrderDate(String orderDate) {
        OrderDate = orderDate;
    }

    public String getOrderTime() {
        return OrderTime;
    }

    public void setOrderTime(String orderTime) {
        OrderTime = orderTime;
    }

    public ArrayList<PrinterKitchenProductsModel> getmPrinterKitchenProductsModel() {
        return mPrinterKitchenProductsModel;
    }

    public void setmPrinterKitchenProductsModel(ArrayList<PrinterKitchenProductsModel> mPrinterKitchenProductsModel) {
        this.mPrinterKitchenProductsModel = mPrinterKitchenProductsModel;
    }
    public String getPOSName() {
        return POSName;
    }

    public void setPOSName(String POSName) {
        this.POSName = POSName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getRollName() {
        return rollName;
    }

    public void setRollName(String rollName) {
        this.rollName = rollName;
    }

    public String getOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        OrderStatus = orderStatus;
    }
}
