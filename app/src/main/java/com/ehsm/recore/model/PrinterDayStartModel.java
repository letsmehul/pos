package com.ehsm.recore.model;

public class PrinterDayStartModel {

    String CashInDrawer;
    String AddCashAmount;
    String TotalCashIndrawer;
    String DayStartDate;
    String CashierName;
    String POSStationNo;
    String EmpRole;

    public String getCashInDrawer() {
        return CashInDrawer;
    }

    public void setCashInDrawer(String cashInDrawer) {
        CashInDrawer = cashInDrawer;
    }

    public String getAddCashAmount() {
        return AddCashAmount;
    }

    public void setAddCashAmount(String addCashAmount) {
        AddCashAmount = addCashAmount;
    }

    public String getTotalCashIndrawer() {
        return TotalCashIndrawer;
    }

    public void setTotalCashIndrawer(String totalCashIndrawer) {
        TotalCashIndrawer = totalCashIndrawer;
    }

    public String getDayStartDate() {
        return DayStartDate;
    }

    public void setDayStartDate(String dayStartDate) {
        DayStartDate = dayStartDate;
    }

    public String getCashierName() {
        return CashierName;
    }

    public void setCashierName(String cashierName) {
        CashierName = cashierName;
    }

    public String getPOSStationNo() {
        return POSStationNo;
    }

    public void setPOSStationNo(String POSStationNo) {
        this.POSStationNo = POSStationNo;
    }

    public String getEmpRole() {
        return EmpRole;
    }

    public void setEmpRole(String empRole) {
        EmpRole = empRole;
    }
}
