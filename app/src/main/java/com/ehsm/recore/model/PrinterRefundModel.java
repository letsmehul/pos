package com.ehsm.recore.model;

import java.util.ArrayList;

public class PrinterRefundModel {

    String StoreName;
    String AddressLine1;
    String AddressLine2;
    String TelNumber;
    String StoreLogoUrl;
    String StoreLogoLocalPath;

    String BilledPersonName;
    String InvoiceNo;
    String RefundInvoiceNo;

    String TotalQty;
    String Subtotal;
    String TaxApplied;
    String GrandTotal;
    String Discount;
    String DiscountRemark;
    String TotalSavings;

    String RefundAmount;

    String ReceiptDateTime;
    String CashierName;
    String POSStationNo;
    String EmpRole;

    String PaymentMode;
    String RefundReason;

    String facebook = "";
    String instagram= "";
    String twitter= "";
    String youtube= "";

    String strSocialMedia = "";
    boolean isOtherPaymentMode = false;

    public boolean isOtherPaymentMode() {
        return isOtherPaymentMode;
    }

    public void setOtherPaymentMode(boolean otherPaymentMode) {
        isOtherPaymentMode = otherPaymentMode;
    }

    public String getStrSocialMedia() {
        return strSocialMedia;
    }

    public void setStrSocialMedia(String strSocialMedia) {
        this.strSocialMedia = strSocialMedia;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    ArrayList<PrinterRefundProductsModel> mPrinterRefundProducts;

    public String getStoreName() {
        return StoreName;
    }

    public void setStoreName(String storeName) {
        StoreName = storeName;
    }

    public String getAddressLine1() {
        return AddressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        AddressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return AddressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        AddressLine2 = addressLine2;
    }

    public String getTelNumber() {
        return TelNumber;
    }

    public void setTelNumber(String telNumber) {
        TelNumber = telNumber;
    }

    public String getStoreLogoLocalPath() {
        return StoreLogoLocalPath;
    }

    public void setStoreLogoLocalPath(String storeLogoLocalPath) {
        StoreLogoLocalPath = storeLogoLocalPath;
    }

    public String getBilledPersonName() {
        return BilledPersonName;
    }

    public void setBilledPersonName(String billedPersonName) {
        BilledPersonName = billedPersonName;
    }

    public String getInvoiceNo() {
        return InvoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        InvoiceNo = invoiceNo;
    }

    public String getRefundInvoiceNo() {
        return RefundInvoiceNo;
    }

    public void setRefundInvoiceNo(String refundInvoiceNo) {
        RefundInvoiceNo = refundInvoiceNo;
    }

    public String getTotalQty() {
        return TotalQty;
    }

    public void setTotalQty(String totalQty) {
        TotalQty = totalQty;
    }

    public String getSubtotal() {
        return Subtotal;
    }

    public void setSubtotal(String subtotal) {
        Subtotal = subtotal;
    }

    public String getTaxApplied() {
        return TaxApplied;
    }

    public void setTaxApplied(String taxApplied) {
        TaxApplied = taxApplied;
    }

    public String getGrandTotal() {
        return GrandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        GrandTotal = grandTotal;
    }

    public String getRefundAmount() {
        return RefundAmount;
    }

    public void setRefundAmount(String refundAmount) {
        RefundAmount = refundAmount;
    }

    public String getReceiptDateTime() {
        return ReceiptDateTime;
    }

    public void setReceiptDateTime(String receiptDateTime) {
        ReceiptDateTime = receiptDateTime;
    }

    public String getCashierName() {
        return CashierName;
    }

    public void setCashierName(String cashierName) {
        CashierName = cashierName;
    }

    public String getPOSStationNo() {
        return POSStationNo;
    }

    public void setPOSStationNo(String POSStationNo) {
        this.POSStationNo = POSStationNo;
    }

    public ArrayList<PrinterRefundProductsModel> getRefundProducts() {
        return mPrinterRefundProducts;
    }

    public void setRefundProducts(ArrayList<PrinterRefundProductsModel> mPrinterRefundProducts) {
        this.mPrinterRefundProducts = mPrinterRefundProducts;
    }

    public String getPaymentMode() {
        return PaymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        PaymentMode = paymentMode;
    }

    public String getRefundReason() {
        return RefundReason;
    }

    public void setRefundReason(String refundReason) {
        RefundReason = refundReason;
    }

    public String getEmpRole() {
        return EmpRole;
    }

    public void setEmpRole(String empRole) {
        EmpRole = empRole;
    }

    public String getStoreLogoUrl() {
        return StoreLogoUrl;
    }

    public void setStoreLogoUrl(String storeLogoUrl) {
        StoreLogoUrl = storeLogoUrl;
    }

    public String getDiscount() {
        return Discount;
    }

    public void setDiscount(String discount) {
        Discount = discount;
    }

    public String getDiscountRemark() {
        return DiscountRemark;
    }

    public void setDiscountRemark(String discountRemark) {
        DiscountRemark = discountRemark;
    }
    public String getTotalSavings() {
        return TotalSavings;
    }

    public void setTotalSavings(String totalSavings) {
        TotalSavings = totalSavings;
    }
}