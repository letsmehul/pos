package com.ehsm.recore.model;

public class PrinterDayCloseModel {

    String DayOpningCash;

    String TotalNumberofTransactionAmount;
    String TotalNumberofSaleByCash;
    String TotalNumberofSaleByCard;
    String TotalNumberofSaleByOther;
    String TotalNumberofRefund;

    String TotalTransactionAmount;
    String TotalSaleByCash;
    String TotalSaleByCard;
    String TotalSaleBySplit;
    String TotalSaleByOther;
    String TotalRefund;

    String TotalPayIn;
    String TotalPayOut;
    String WithdrawCash;
    String DayCloseCash;

    String DayCloseDateTime;
    String CashierName;
    String POSStationNo;
    String EmpRole;
    String highCash,shortCash;

    public String getTotalSaleBySplit() {
        return TotalSaleBySplit;
    }

    public void setTotalSaleBySplit(String totalSaleBySplit) {
        TotalSaleBySplit = totalSaleBySplit;
    }

    public String getDayOpningCash() {
        return DayOpningCash;
    }

    public void setDayOpningCash(String dayOpningCash) {
        DayOpningCash = dayOpningCash;
    }

    public String getTotalNumberofTransactionAmount() {
        return TotalNumberofTransactionAmount;
    }

    public void setTotalNumberofTransactionAmount(String totalNumberofTransactionAmount) {
        TotalNumberofTransactionAmount = totalNumberofTransactionAmount;
    }

    public String getTotalNumberofSaleByCash() {
        return TotalNumberofSaleByCash;
    }

    public void setTotalNumberofSaleByCash(String totalNumberofSaleByCash) {
        TotalNumberofSaleByCash = totalNumberofSaleByCash;
    }

    public String getTotalNumberofSaleByCard() {
        return TotalNumberofSaleByCard;
    }

    public void setTotalNumberofSaleByCard(String totalNumberofSaleByCard) {
        TotalNumberofSaleByCard = totalNumberofSaleByCard;
    }

    public String getTotalNumberofSaleByOther() {
        return TotalNumberofSaleByOther;
    }

    public void setTotalNumberofSaleByOther(String totalNumberofSaleByOther) {
        TotalNumberofSaleByOther = totalNumberofSaleByOther;
    }

    public String getTotalNumberofRefund() {
        return TotalNumberofRefund;
    }

    public void setTotalNumberofRefund(String totalNumberofRefund) {
        TotalNumberofRefund = totalNumberofRefund;
    }

    public String getTotalTransactionAmount() {
        return TotalTransactionAmount;
    }

    public void setTotalTransactionAmount(String totalTransactionAmount) {
        TotalTransactionAmount = totalTransactionAmount;
    }

    public String getTotalSaleByCash() {
        return TotalSaleByCash;
    }

    public void setTotalSaleByCash(String totalSaleByCash) {
        TotalSaleByCash = totalSaleByCash;
    }

    public String getTotalSaleByCard() {
        return TotalSaleByCard;
    }

    public void setTotalSaleByCard(String totalSaleByCard) {
        TotalSaleByCard = totalSaleByCard;
    }

    public String getTotalSaleByOther() {
        return TotalSaleByOther;
    }

    public void setTotalSaleByOther(String totalSaleByOther) {
        TotalSaleByOther = totalSaleByOther;
    }

    public String getTotalRefund() {
        return TotalRefund;
    }

    public void setTotalRefund(String totalRefund) {
        TotalRefund = totalRefund;
    }

    public String getTotalPayIn() {
        return TotalPayIn;
    }

    public void setTotalPayIn(String totalPayIn) {
        TotalPayIn = totalPayIn;
    }

    public String getTotalPayOut() {
        return TotalPayOut;
    }

    public void setTotalPayOut(String totalPayOut) {
        TotalPayOut = totalPayOut;
    }

    public String getWithdrawCash() {
        return WithdrawCash;
    }

    public void setWithdrawCash(String withdrawCash) {
        WithdrawCash = withdrawCash;
    }

    public String getDayCloseCash() {
        return DayCloseCash;
    }

    public void setDayCloseCash(String dayCloseCash) {
        DayCloseCash = dayCloseCash;
    }

    public String getDayCloseDateTime() {
        return DayCloseDateTime;
    }

    public void setDayCloseDateTime(String dayStartDate) {
        DayCloseDateTime = dayStartDate;
    }

    public String getCashierName() {
        return CashierName;
    }

    public void setCashierName(String cashierName) {
        CashierName = cashierName;
    }

    public String getPOSStationNo() {
        return POSStationNo;
    }

    public void setPOSStationNo(String POSStationNo) {
        this.POSStationNo = POSStationNo;
    }

    public String getEmpRole() {
        return EmpRole;
    }

    public void setEmpRole(String empRole) {
        EmpRole = empRole;
    }

    public String getHighCash() {
        return highCash;
    }

    public void setHighCash(String highCash) {
        this.highCash = highCash;
    }

    public String getShortCash() {
        return shortCash;
    }

    public void setShortCash(String shortCash) {
        this.shortCash = shortCash;
    }
}
