package com.ehsm.recore.model;

public class PrinterClockOutModel {

    String POSStationNo;
    String DayCloseDate;
    String EmpName;
    String EmpRole;
    String ClockInTime;
    String ClockOutTime;
    String TotalHours;

    public String getPOSStationNo() {
        return POSStationNo;
    }

    public void setPOSStationNo(String POSStationNo) {
        this.POSStationNo = POSStationNo;
    }

    public String getDayCloseDate() {
        return DayCloseDate;
    }

    public void setDayCloseDate(String dayCloseDate) {
        DayCloseDate = dayCloseDate;
    }

    public String getEmpName() {
        return EmpName;
    }

    public void setEmpName(String empName) {
        EmpName = empName;
    }

    public String getEmpRole() {
        return EmpRole;
    }

    public void setEmpRole(String empRole) {
        EmpRole = empRole;
    }

    public String getClockInTime() {
        return ClockInTime;
    }

    public void setClockInTime(String clockInTime) {
        ClockInTime = clockInTime;
    }

    public String getClockOutTime() {
        return ClockOutTime;
    }

    public void setClockOutTime(String clockOutTime) {
        ClockOutTime = clockOutTime;
    }

    public String getTotalHours() {
        return TotalHours;
    }

    public void setTotalHours(String totalHours) {
        TotalHours = totalHours;
    }



}
