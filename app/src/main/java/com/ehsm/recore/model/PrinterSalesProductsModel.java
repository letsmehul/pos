package com.ehsm.recore.model;

public class PrinterSalesProductsModel {

    String ProductName;
    String Quanity;
    String Rate;
    String Tax;
    String Discount;
    String ModifierIngredient;
    String DiscountRemark;
    String Total;
    String Instruction;

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getQuanity() {
        return Quanity;
    }

    public void setQuanity(String quanity) {
        Quanity = quanity;
    }

    public String getRate() {
        return Rate;
    }

    public void setRate(String rate) {
        Rate = rate;
    }

    public String getTax() {
        return Tax;
    }

    public void setTax(String tax) {
        Tax = tax;
    }

    public String getDiscount() {
        return Discount;
    }

    public void setDiscount(String discount) {
        Discount = discount;
    }
    public String getModifierIngredient() {
        return ModifierIngredient;
    }

    public void setModifierIngredient(String modifierIngredient) {
        ModifierIngredient = modifierIngredient;
    }

    public String getDiscountRemark() {
        return DiscountRemark;
    }

    public void setDiscountRemark(String discountRemark) {
        DiscountRemark = discountRemark;
    }

    public String getTotal() {
        return Total;
    }

    public void setTotal(String total) {
        Total = total;
    }

    public String getInstruction() {
        return Instruction;
    }

    public void setInstruction(String instruction) {
        Instruction = instruction;
    }
}
