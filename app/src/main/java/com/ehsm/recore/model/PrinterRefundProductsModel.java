package com.ehsm.recore.model;

public class PrinterRefundProductsModel {

    String ProductName;
    String Quanity;
    String PaidAmount;
    String ModifierIngredient;
    String Discount;
    String DiscountRemark;

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getQuanity() {
        return Quanity;
    }

    public void setQuanity(String quanity) {
        Quanity = quanity;
    }

    public String getPaidAmount() {
        return PaidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        PaidAmount = paidAmount;
    }

    public String getModifierIngredient() {
        return ModifierIngredient;
    }

    public void setModifierIngredient(String modifierIngredient) {
        ModifierIngredient = modifierIngredient;
    }
    public String getDiscount() {
        return Discount;
    }

    public void setDiscount(String discount) {
        Discount = discount;
    }

    public String getDiscountRemark() {
        return DiscountRemark;
    }

    public void setDiscountRemark(String discountRemark) {
        DiscountRemark = discountRemark;
    }

}
