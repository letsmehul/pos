package com.ehsm.recore.model

import com.ehsm.recore.utils.AppConstant
import com.google.gson.annotations.SerializedName


//request models
open class BaseRequest(var merchantId: String)

open class RequestPreConnect(merchantId: String, var hsn: String) : BaseRequest(merchantId)

class RequestPingOrConnect(merchantId: String, hsn: String, val force: Boolean) :
    RequestPreConnect(merchantId, hsn)

class RequestDisplay(merchantId: String, hsn: String, val text: String) :
    RequestPreConnect(merchantId, hsn)

class RequestReadCard(
    merchantId: String = "",
    var authMerchantId:String = "",
    hsn: String = "",
    var amount: String="",
    var printReceipt:Boolean = false,
    var includeSignature: Boolean = false,
    var includeAmountDisplay: Boolean = true,
    var includePIN: Boolean = true,
    var beep: Boolean = false,
    var includeAVS: Boolean = false,
    var orderId: String = "",
    var includeCVV: Boolean = false,
    var clearDisplayDelay: String = "500",
    var gzipSignature: Boolean = false
) : RequestPreConnect(merchantId, hsn)

class RequestReadSignature(
    merchantId: String,
    hsn: String,
    val prompt: String,
    val beep: Boolean = false
) : RequestPreConnect(merchantId, hsn)

class ReadInput(
    merchantId: String,
    hsn: String,
    val prompt: String,
    val beep: Boolean = false,
    val format: String
) : RequestPreConnect(merchantId, hsn)

class RequestRefund(merchantId: String, val retref: String, val amount: String) :
    BaseRequest(merchantId)

data class RefundResponseModel(
    @SerializedName("Eoutco")
    val eoutco: List<String>,
    @SerializedName("BHQEW")
    val bHQEW: String,
    @SerializedName("receipt")
    val receipt: String,
    @SerializedName("merchid")
    val merchid: String,
    @SerializedName("respstat")
    val respstat: String,
    @SerializedName("zQxJsT")
    val zQxJsT: Int,
    @SerializedName("currency")
    val currency: String,
    @SerializedName("orderId")
    val orderId: String,
    @SerializedName("resptext")
    val resptext: String,
    @SerializedName("respproc")
    val respproc: String,
    @SerializedName("retref")
    val retref: String,
    @SerializedName("amount")
    val amount: String,
    @SerializedName("respcode")
    val respcode: String
) : BaseBoltAPIResponse()

/*data class RefundResponseModel(
    @SerializedName("respproc")
    val respproc: String,
    @SerializedName("amount")
    val amount: String,
    @SerializedName("resptext")
    val resptext: String,
    @SerializedName("retref")
    val retref: String,
    @SerializedName("respstat")
    val respstat: String,
    @SerializedName("respcode")
    val respcode: String,
    @SerializedName("merchid")
    val merchid: String
) : BaseBoltAPIResponse()*/
class CardRequestModel(
    merchantId: String = "",
    hsn: String = "",
    @SerializedName("aid")
    var aid: String = "credit",
    @SerializedName("amount")
    var amount: String = "",
    @SerializedName("beep")
    var beep: Boolean = true,
    @SerializedName("capture")
    var capture: Boolean = true,
    @SerializedName("clearDisplayDelay")
    var clearDisplayDelay: String = "500",
    @SerializedName("confirmAmount")
    var confirmAmount: Boolean = false,
    @SerializedName("gzipSignature")
    var gzipSignature: Boolean = false,

    @SerializedName("includeAVS")
    var includeAVS: Boolean = false,
    @SerializedName("includeAmountDisplay")
    var includeAmountDisplay: Boolean = true,
    @SerializedName("includePin")
    var includePin: Boolean = true,
    @SerializedName("includeSignature")
    var includeSignature: Boolean = true,

    @SerializedName("orderId")
    var orderId: String = "",
    @SerializedName("printReceipt")
    var printReceipt: Boolean = false,
    @SerializedName("signatureDimensions")
    var signatureDimensions: String = "320,450",
    @SerializedName("signatureFormat")
    var signatureFormat: String = "png",
    @SerializedName("signatureImageType")
    var signatureImageType: String = "rgb"
) : RequestPreConnect(merchantId, hsn)

class ManualCardReadRequestModel(
    merchantId: String = "",
    hsn: String = "",
    @SerializedName("beep")
    var beep: Boolean = true,
    @SerializedName("includeSignature")
    var includeSignature: Boolean = false,
    @SerializedName("includeExpirationDate")
    var includeExpirationDate: Boolean = true,
    var includePIN: Boolean = true
) : RequestPreConnect(merchantId, hsn)

class ManualCardResponseModel(
    @SerializedName("token")
    var token: String = "",
    @SerializedName("expiry")
    var expiry: String = "",
    @SerializedName("signature")
    var signature: String = "",
    @SerializedName("respcode")
    var respcode: String = "",
    @SerializedName("resptext")
    var resptext: String = ""
) : BaseBoltAPIResponse()

//response models
open class BaseBoltAPIResponse {
    var errorCode = 0
    var errorMessage: String = ""
}

class Terminals(val terminals: List<String>) : BaseBoltAPIResponse()

class ConnectResponse(val isConnect: Boolean) : BaseBoltAPIResponse()

class ReadConfirmationResponse(val confirmed: Boolean) : BaseBoltAPIResponse()

class ReadInputResponse(val input: String) : BaseBoltAPIResponse()

class CardResponseModel(
    @SerializedName("token")
    val token: String = "",
    @SerializedName("expiry")
    val expiry: String = "",
    @SerializedName("name", alternate = ["nameOnCard"])
    val name: String = "",
    @SerializedName("signature")
    val signature: String? = "",
    @SerializedName("batchid")
    val batchid: String = "",
    @SerializedName("retref")
    val retref: String = "",
    @SerializedName("avsresp")
    val avsresp: String? = "",
    @SerializedName("respproc")
    val respproc: String = "",
    @SerializedName("amount")
    val amount: String = "",
    @SerializedName("resptext")
    val resptext: String = "",
    @SerializedName("authcode")
    val authcode: String = "",
    @SerializedName("respcode")
    val respcode: String = "",
    @SerializedName("merchid")
    val merchid: String = "",
    @SerializedName("cvvresp")
    val cvvresp: String? = "",
    @SerializedName("respstat")
    val respstat: String = "",
    @SerializedName("emvTagData")
    val emvTagData: String = "",
    @SerializedName("orderid")
    var orderid: String = "",
    @SerializedName("receiptData")
    val receiptData: ReceiptData?
) : BaseBoltAPIResponse() {
    data class ReceiptData(
        @SerializedName("dba")
        val dba: String = "",
        @SerializedName("address1")
        val address1: String = "",
        @SerializedName("address2")
        val address2: String = "",
        @SerializedName("phone")
        val phone: String = "",
        @SerializedName("header")
        val header: String = "",
        @SerializedName("orderNote")
        val orderNote: String = "",
        @SerializedName("dateTime")
        val dateTime: String = "",
        @SerializedName("items")
        val items: String = "",
        @SerializedName("nameOnCard")
        val nameOnCard: String = "",
        @SerializedName("footer")
        val footer: String = ""
    )
}

