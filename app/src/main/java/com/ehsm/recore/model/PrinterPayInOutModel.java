package com.ehsm.recore.model;

public class PrinterPayInOutModel {

    String Reason;
    String PayInOutAmount;
    String DayStartDate;
    String CashierName;
    String POSStationNo;
    String EmpRole;

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }

    public String getPayInOutAmount() {
        return PayInOutAmount;
    }

    public void setPayInOutAmount(String payInOutAmount) {
        PayInOutAmount = payInOutAmount;
    }


    public String getDayStartDate() {
        return DayStartDate;
    }

    public void setDayStartDate(String dayStartDate) {
        DayStartDate = dayStartDate;
    }

    public String getCashierName() {
        return CashierName;
    }

    public void setCashierName(String cashierName) {
        CashierName = cashierName;
    }

    public String getPOSStationNo() {
        return POSStationNo;
    }

    public void setPOSStationNo(String POSStationNo) {
        this.POSStationNo = POSStationNo;
    }

    public String getEmpRole() {
        return EmpRole;
    }

    public void setEmpRole(String empRole) {
        EmpRole = empRole;
    }
}
