package com.ehsm.recore.model

import androidx.room.*
import com.google.gson.annotations.SerializedName


@Entity
class DiscountMaster(
    @PrimaryKey
    @SerializedName("iDiscountId")
    val discountId: Int,
    @SerializedName("strDiscountName")
    val name: String = "",
    @SerializedName("strDescription")
    val description: String = "",
    @SerializedName("type")
    val discountType: String?,
    @SerializedName("strType")
    val strType: String = "",  //either amount or rate
    @SerializedName("dblValue")
    val dblValue: Float,
    @SerializedName("strAppliedTo")
    val strAppliedTo: String?,  // either check or item
    @SerializedName("iBuyQty")
    val buyQty: Int = 0,
    @SerializedName("iGetQty")
    val getQty: Int = 0,
    @SerializedName("dblMinCheckAmount")
    val minCartAmount: Int = 0,
    @SerializedName("strCouponCode")
    var couponCode: String?,
    @SerializedName("tlsAdminnApprovalRequired")
    var isAdminApprovalRequired: Int = 0,
    @SerializedName("employees")
    var employees: String?
) {
    @Ignore
    var discountProducts: List<DiscountMapping>? = null

    @Ignore
    var discountBuyProducts: List<DiscountMapping>? = null

    @Ignore
    var discountGetProducts: List<DiscountMapping>? = null
}

@Entity(indices = arrayOf(Index(value = ["productId", "discountId"], unique = true)))
class DiscountMapping(
    @SerializedName("iProductId")
    val productId: Long,
    @SerializedName("iDiscountId")
    val discountId: Int,
    @SerializedName("strCondition")
    val strCondition: String?,
    @SerializedName("strProductName")
    val strProductName: String?

) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}

class DiscountAndProduct(
    @Embedded
    val discountMaster: DiscountMaster,
    @SerializedName("iProductId")
    val productId: Long,
    @SerializedName("strCondition")
    val strCondition: String?,
    @SerializedName("strProductName")
    val strProductName: String?
)


data class Discount(
    val iDiscountId: Int,
    val strDiscountName: String,
    val strDescription: String,
    val type: String,
    val strType: String,
    val dblValue: Double,
    val strAppliedTo: String,
    val dblMinCheckAmount: String,
    val strCouponCode: String

)


data class DiscountModel(
    val simple_discount: List<DiscountMaster>,
    val coupon_discount: List<DiscountMaster>,
    val sale_discount: List<DiscountMaster>,
    val bogo_discount: List<DiscountMaster>
)

data class DiscountResponse(
    val data: DiscountModel
) : ResponseModel()


data class DiscountGeneric(
    val iDiscountId: Int,
    val strDiscountName: String,
    val strType: String,
    val dblValue: Float,
    val strAppliedTo: String,
    val strCouponCode: String
)

data class DiscountProducts(
    var iDiscountId: String,
    var iProductId: String
)
