package com.ehsm.recore.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.ehsm.recore.R
import com.ehsm.recore.databinding.ActivitySplashBinding
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CV
import com.ehsm.recore.utils.ScreenLockTimer

class SplashActivity : BaseActivity<ActivitySplashBinding>() {

    private lateinit var mActivity: SplashActivity
    private lateinit var mHandler: Handler
    private lateinit var mRunnable: Runnable
    private var islogin: Boolean = false

    //test commit
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        CM.removeStatusBar(mActivity)
        //bindView(R.layout.activity_splash)
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_splash,
            getWindow().getDecorView().getRootView() as ViewGroup?,false)
        setContentView(mBinding.root)
        init()
    }

    private fun init() {

        mHandler = Handler()
        mRunnable = Runnable {

            islogin = CM.getSp(mActivity, CV.ISLOGIN, false) as Boolean
            if (islogin) {
                val intent = Intent(mActivity, PasscodeActivity::class.java)
                intent.putExtra("isFromSplash",true)
                CM.startActivityWithIntent(mActivity, intent)
            } else {
                CM.startActivity(mActivity, LoginActivity::class.java)
            }
            CM.finishActivity(mActivity)

        }
        mHandler.postDelayed(mRunnable, 3000)
    }

    override fun onDestroy() {
        super.onDestroy()
        mHandler.removeCallbacks(mRunnable)
    }

    override fun onRequestGranted(requestCode: Int, perms: List<String>) {
    }

    override fun onResume() {
        super.onResume()
        ScreenLockTimer.stopHandler()
    }
}
