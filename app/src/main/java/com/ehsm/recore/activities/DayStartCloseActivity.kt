package com.ehsm.recore.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.*
import android.util.Log
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.core.view.setPadding
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.ehsm.recore.R
import com.ehsm.recore.custom.keyboard.DateKeyboard
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.databinding.ActivityDayStartEndBinding
import com.ehsm.recore.model.*
import com.ehsm.recore.utils.*
import com.ehsm.recore.utils.CM.showToast
import com.ehsm.recore.utils.logger.LogUtil
import com.google.gson.Gson
import com.kcspl.divyangapp.viewmodel.*
import kotlinx.android.synthetic.main.custom_toast.*
import kotlinx.android.synthetic.main.header_layout.view.*
import java.math.BigDecimal
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class DayStartCloseActivity : BaseActivity<ActivityDayStartEndBinding>() {

    private var checkInDataViewModel: CheckInDataViewModel? = null
    private var checkInOutDataViewModel: CheckInOutDataViewModel? = null
    private var checkOutDataViewModel: CheckOutDataViewModel? = null
    var amount_check_out = 0.0
    private var isCheckIn = false
    private var last_action = ""
    private var pos_name = ""
    private var date_time = ""

    private var isCheckIn_status = false
    private var last_action_status = ""
    private var pos_name_status = ""
    private var date_time_status = ""

    var emp_id = 0
    var emp_name = ""
    var emp_role = ""
    private var lastActionViewModel: LastActionViewModel? = null
    private var lastStatusViewModel: LastStatusDayStartCloseViewModel? = null
    private lateinit var mDatabase: RecoreDB
    private var cash_in_drawer = ""
    private lateinit var mActivity: DayStartCloseActivity

    lateinit var mCheckOutData: CheckOutData
    var totalTransactionalAmount = "0.0"
    lateinit var otherpaymentlist :ArrayList<OtherPaymentModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        CM.removeStatusBar(this)
        //bindView(R.layout.activity_day_start_end)
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_day_start_end,
            getWindow().getDecorView().getRootView() as ViewGroup?,false)
        setContentView(mBinding.root)
        mDatabase = RecoreDB.getInstance(this)!!

        checkInDataViewModel = ViewModelProviders.of(this).get(CheckInDataViewModel::class.java)
        checkInOutDataViewModel =
            ViewModelProviders.of(this).get(CheckInOutDataViewModel::class.java)
        checkOutDataViewModel = ViewModelProviders.of(this).get(CheckOutDataViewModel::class.java)
        lastActionViewModel = ViewModelProviders.of(this).get(LastActionViewModel::class.java)
        lastStatusViewModel =
            ViewModelProviders.of(this).get(LastStatusDayStartCloseViewModel::class.java)


        setEmpView(mBinding.header.tvEmpName)
        setStoreNameView(mBinding.header.tvstorename, mBinding.header.tvposname)

        emp_id = CM.getSp(this, CV.EMP_ID, 0) as Int
        emp_name = CM.getSp(this, CV.EMP_NAME, "") as String
        emp_role = CM.getSp(this, CV.EMP_ROLE, "") as String

        mBinding.tvWelComeMsg.setText("Welcome " + emp_role + " " + emp_name + ",")

        mBinding.btnCheckIn.setOnClickListener {
            openCheckInCashierDialog()
        }

        mBinding.btnCheckout.setOnClickListener {
            val shiftStartEmpId = CM.getSp(this, CV.SHIFTSTARTEMPID, 0) as Int
            if (emp_id == shiftStartEmpId) {
                openCheckOutCashierDialog()
            } else {
                showToast(
                    mActivity,
                    getString(R.string.error_close_shift),
                    R.drawable.icon,
                    custom_toast_container
                )
            }
        }

        mBinding.header.ivLogo.setOnClickListener {
            CM.finishActivity(this)
        }

        mBinding.header.ivNewSale.setOnClickListener {
            if (last_action.equals(AppConstant.CLOCK_IN)) {
                if (last_action_status.equals(AppConstant.SHIFT_START)) {
                    if (!emp_role.equals(AppConstant.STAFF)) {
                        CM.navigateToDashboard(this)
                    } else {
                        openDenyTransactionDialog()
                    }
                } else {
                    openDenyDayStartDialog()
                }
            } else {
                openDenyTransactionDialog()
            }
        }

        mBinding.header.ivMore.setOnClickListener {
            CM.showPopup(mActivity, mBinding.header.ivMore, {
                syncWithServer()
            })
        }

        mBinding.header.reluser.setOnClickListener {
            callChangeMpin()
        }
        mBinding.header.ivNoSale.setOnClickListener {
            if (last_action.equals(AppConstant.CLOCK_IN)) {
                CM.openNosaleDialog(this, custom_toast_container)
            } else {
                openDenyTransactionDialog()
            }
        }

        ScreenLockTimer.registerLockListener(mBinding.header.ivLock,mActivity)
    }

    override fun onResume() {
        super.onResume()

        when (emp_role) {
            AppConstant.STAFF -> {
                mBinding.header.ivNoSale.visibility = View.GONE
                mBinding.header.ivNewSale.visibility = View.GONE
            }
            AppConstant.CASHIER -> {
                mBinding.header.ivNoSale.visibility = View.GONE
                mBinding.header.ivNewSale.visibility = View.VISIBLE
            }
            AppConstant.STORE_ADMIN -> {
                mBinding.header.ivNoSale.visibility = View.VISIBLE
                mBinding.header.ivNewSale.visibility = View.VISIBLE
            }
        }

        if (!CM.isInternetAvailable(this)) {
        } else {
            webCallGetLastAction()
        }
    }

    override fun onStart() {
        super.onStart()
        UserInteractionAwareCallback(window!!.callback,mActivity)
    }

    private fun webCallGetLastAction() {
        showKcsDialog()

        lastActionViewModel!!.getLastAction(emp_id)
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )

                    isCheckIn = true
                } else {

                    pos_name = loginResponseModel.data!!.data.posName
                    date_time = loginResponseModel.data!!.data.dateTimeCreated
                    isCheckIn =
                        !loginResponseModel.data!!.data.strTnxtype.equals(AppConstant.CLOCK_IN)

                    cash_in_drawer = loginResponseModel.data!!.data.dblCashInDrawer
                }

                if (isCheckIn) {
                    last_action = AppConstant.CLOCK_OUT
                } else {
                    last_action = AppConstant.CLOCK_IN
                }
                mBinding.tvMachineDateTimeValue.setText(
                    last_action + ", from " + pos_name + " on " + CM.convertDateFormate(
                        CV.WS_DATE_FORMATTE_AM_PM,
                        CV.DISPLAY_DATE_FORMATTE_AMPM, date_time
                    )
                )

                if (isCheckIn) {
                    //allow check in
                    mBinding.btnCheckIn.isEnabled = true
                    mBinding.btnCheckIn.background = resources.getDrawable(R.drawable.btn_bg)
                    mBinding.btnCheckout.isEnabled = false
                    mBinding.btnCheckout.background =
                        resources.getDrawable(R.drawable.btn_bg_gray)
                } else {
                    mBinding.btnCheckIn.isEnabled = false
                    mBinding.btnCheckIn.background =
                        resources.getDrawable(R.drawable.btn_bg_gray)
                    mBinding.btnCheckout.isEnabled = true
                    mBinding.btnCheckout.background = resources.getDrawable(R.drawable.btn_bg)

                }

                webCallGetLastStatus()
            })

    }

    private fun webCallGetLastStatus() {
        showKcsDialog()

        lastStatusViewModel!!.getLastActionDayStartClose()
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                Log.e("Drawer", "" + Gson().toJson(loginResponseModel))
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                    isCheckIn_status = true
                } else {
                    pos_name_status = loginResponseModel.data!!.data.posName
                    date_time_status = loginResponseModel.data!!.data.dateTimeCreated
                    isCheckIn_status =
                        !loginResponseModel.data!!.data.strTnxtype.equals(AppConstant.SHIFT_START)

                    cash_in_drawer = loginResponseModel.data!!.data.dblCashInDrawer
                }

                Log.e("isCheckIn_status", "" + isCheckIn_status)

                if (isCheckIn_status) {
                    last_action_status = AppConstant.SHIFT_CLOSE
                    NewSaleFragmentCommon.resetShiftDetails(this)
                } else {
                    NewSaleFragmentCommon.setShiftDetails(this, loginResponseModel)
                    last_action_status = AppConstant.SHIFT_START
                }
                var shiftByName = CM.getSp(this, CV.SHIFTSTARTEMPNAME, "")
                mBinding.tvDayStartCloseMachineDateTimeValue.setText(
                    last_action_status +" by "+shiftByName+" from " + pos_name_status + " on " + CM.convertDateFormate(
                        CV.WS_DATE_FORMATTE_AM_PM,
                        CV.DISPLAY_DATE_FORMATTE_AMPM, date_time_status
                    )
                )

                if (isCheckIn_status) {
                    //allow check in
                    mBinding.btnCheckIn.isEnabled = true
                    mBinding.btnCheckIn.background = resources.getDrawable(R.drawable.btn_bg)
                    mBinding.btnCheckout.isEnabled = false
                    mBinding.btnCheckout.background = resources.getDrawable(R.drawable.btn_bg_gray)
                } else {
                    mBinding.btnCheckIn.isEnabled = false
                    mBinding.btnCheckIn.background =
                        resources.getDrawable(R.drawable.btn_bg_gray)
                    mBinding.btnCheckout.isEnabled = true
                    mBinding.btnCheckout.background = resources.getDrawable(R.drawable.btn_bg)
                }
            })
    }

    private fun openCheckInCashierDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.setCallback(
                UserInteractionAwareCallback(
                        dialog.window!!.getCallback(),
                        mActivity
                )
        )
        dialog.setContentView(R.layout.dialog_cashier_checkin)

        dialog.setCancelable(false)
        dialog.show()

        val currency_symbol =
            CM.getSp(this@DayStartCloseActivity, CV.CURRENCY_CODE, "") as String

        val ivCancel = dialog.findViewById<ImageView>(R.id.ivCancel)
        val tvCashInDrawer = dialog.findViewById<TextView>(R.id.tvCashInDrawer)

        val btnSubmit = dialog.findViewById<Button>(R.id.btnSubmit)
        btnSubmit.setText(getString(R.string.confirm))


        ivCancel.setOnClickListener {
            dialog.dismiss()
        }

        val tvTotalCashInDrawer = dialog.findViewById<TextView>(R.id.tvTotalCashInDrawer)
        tvTotalCashInDrawer.setText(currency_symbol + " 0.00")
        val edtCashInDrawer = dialog.findViewById<EditText>(R.id.edtCashInDrawer)

        val keyboard: DateKeyboard = dialog.findViewById(R.id.keyboard) as DateKeyboard
        edtCashInDrawer.setRawInputType(InputType.TYPE_CLASS_TEXT)
        edtCashInDrawer.setTextIsSelectable(true)

        val ic = edtCashInDrawer.onCreateInputConnection(EditorInfo())
        keyboard.setInputConnection(ic)

        edtCashInDrawer.requestFocus()
        edtCashInDrawer.isFocusable = true
        edtCashInDrawer.addTextChangedListener(GenericTextWatcher(edtCashInDrawer))

        if (!CM.isInternetAvailable(this)) {
            //offline code
            tvCashInDrawer.setText(cash_in_drawer)
        } else {
            webCallGetCheckInData(tvCashInDrawer)
        }

        var edt_amount: Double
        var total = 0.0

        edtCashInDrawer.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {

                try {
                    val amount = tvCashInDrawer.text.toString().split(" ").get(1).toDouble()
                    if (!edtCashInDrawer.text.toString().equals("")) {
                        edt_amount = edtCashInDrawer.text.toString().toDouble()

                    } else {
                        edt_amount = 0.0

                    }
                    total = amount + edt_amount

                    tvTotalCashInDrawer.setText(
                        currency_symbol + " " + String.format(
                            "%.2f",
                            total
                        )
                    )
                } catch (e: NumberFormatException) {
                    edtCashInDrawer.setText("0.0")
                    showToast(
                        mActivity,
                        getString(R.string.msg_enter_proper_amount),
                        R.drawable.icon,
                        custom_toast_container
                    )
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })

        btnSubmit.setOnClickListener {
            if (TextUtils.isEmpty(edtCashInDrawer.text.toString())) {
                showToast(
                    mActivity,
                    getString(R.string.enter_amount),
                    R.drawable.icon,
                    custom_toast_container
                )
            } else if (edtCashInDrawer.text.toString() == "0.") {
                showToast(
                    mActivity,
                    getString(R.string.enter_valid_amount),
                    R.drawable.icon,
                    custom_toast_container
                )
            } else {
                if (!CM.isInternetAvailable(this)) {
                    addCheckInDataOffline(tvTotalCashInDrawer, dialog)
                } else {
                    webCallSendCheckInOutData(
                        edtCashInDrawer.text.toString().toDouble(),
                        0.0, 0.0,
                        tvTotalCashInDrawer.text.toString().split(" ").get(1).toDouble(),
                        AppConstant.SHIFT_START,
                        "",
                        dialog
                    )
                }
            }
        }

    }

    private fun addCheckInDataOffline(tvTotalCashInDrawer: TextView, dialog: Dialog) {
        //offline code
        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.WS_DATE_FORMATTE_AM_PM)

        val temp: ArrayList<EmployeeListWithLastAction> = ArrayList()
        val temp_last: ArrayList<LastActivityModel> = ArrayList()

        temp_last.add(
            LastActivityModel(
                "",
                sdf.format(date),
                CM.getSp(this, CV.POS_NAME, "") as String,
                emp_name,
                AppConstant.CLOCK_IN,
                0,
                tvTotalCashInDrawer.text.toString()
            )
        )

        temp.add(
            EmployeeListWithLastAction(
                emp_id, 0, "", "", emp_name, emp_name,
                "", "", "", "", "", "", "", 0, 0,
                "", "", 1, sdf.format(date), "", "", 0, "", 0,
                0, 0, emp_role, temp_last
            )
        )
        mDatabase.daoManualSync().insertItem(temp)
        dialog.dismiss()
        onResume()

    }

    private fun addCheckOutDataOffline(tvTotalCashInDrawer: TextView, dialog: Dialog) {
        //offline code
        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.WS_DATE_FORMATTE_AM_PM)

        val temp: ArrayList<EmployeeListWithLastAction> = ArrayList()
        val temp_last: ArrayList<LastActivityModel> = ArrayList()

        temp_last.add(
            LastActivityModel(
                "",
                sdf.format(date),
                CM.getSp(this, CV.POS_NAME, "") as String,
                emp_name,
                AppConstant.CLOCK_OUT,
                0,
                tvTotalCashInDrawer.text.toString()
            )
        )

        temp.add(
            EmployeeListWithLastAction(
                emp_id, 0, "", "", emp_name, emp_name,
                "", "", "", "", "", "", "", 0, 0,
                "", "", 1, sdf.format(date), "", "", 0, "", 0,
                0, 0, emp_role, temp_last
            )
        )
        mDatabase.daoManualSync().insertItem(temp)
        dialog.dismiss()
        onResume()

    }

    private fun openCheckOutCashierDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.setCallback(
                UserInteractionAwareCallback(
                        dialog.window!!.getCallback(),
                        mActivity
                )
        )
        dialog.setContentView(R.layout.dialog_cashier_checkout)

        dialog.setCancelable(false)
        dialog.show()

        val ivCancel = dialog.findViewById<ImageView>(R.id.ivCancel)

        val tvStartDay = dialog.findViewById<TextView>(R.id.tvStartDay)
        val tvCashInDrawer = dialog.findViewById<TextView>(R.id.tvCashInDrawer)
        val tvTotalSalesCash = dialog.findViewById<TextView>(R.id.tvTotalSalesCash)
        val tvManualCashWithdrawel = dialog.findViewById<TextView>(R.id.tvManualCashWithdrawel)
        val tvBalanceInDrawer = dialog.findViewById<TextView>(R.id.tvBalanceInDrawer)
        val tvTotalSalesNumber = dialog.findViewById<TextView>(R.id.tvTotalSalesNumber)
        val tvSalesByCard = dialog.findViewById<TextView>(R.id.tvSalesByCard)
        val tvTotalSalesCard = dialog.findViewById<TextView>(R.id.tvTotalSalesCard)
        val tvTotalSalesSplit = dialog.findViewById<TextView>(R.id.tvTotalSalesSplit)
        val tvSalesByCash = dialog.findViewById<TextView>(R.id.tvSalesByCash)
        val tvTotalSalesOther = dialog.findViewById<TextView>(R.id.tvTotalSalesOther)
        val tvTotalPayIn = dialog.findViewById<TextView>(R.id.tvTotalPayIn)
        val tvTotalPayOut = dialog.findViewById<TextView>(R.id.tvTotalPayOut)
        val tvTotalRefund = dialog.findViewById<TextView>(R.id.tvTotalRefund)
        val tvSalesByOther = dialog.findViewById<TextView>(R.id.tvSalesByOther)
        val tvTotalRefundCount = dialog.findViewById<TextView>(R.id.tvTotalRefundCount)
        val btnSubmit = dialog.findViewById<Button>(R.id.btnSubmit)
        btnSubmit.setText(getString(R.string.confirm))
        val edtCashWithdraw = dialog.findViewById<EditText>(R.id.edtCashWithdraw)
        val edtHighCash = dialog.findViewById<EditText>(R.id.edtHighCash)
        val edtShortCash = dialog.findViewById<EditText>(R.id.edtShortCash)
        val btnOpenCashbox = dialog.findViewById<Button>(R.id.btnOpenCashbox)
        val btnViewOtherPayment = dialog.findViewById<ImageView>(R.id.view_other_payment_btn)
        btnViewOtherPayment.setOnClickListener {
            if (otherpaymentlist.size>0) {
                openOtherPaymentDialog()
            }
        }

        val edtArray = listOf<EditText>(edtCashWithdraw, edtHighCash, edtShortCash)
        val keyboard: DateKeyboard = dialog.findViewById(R.id.keyboard) as DateKeyboard
        btnOpenCashbox.setOnClickListener {
            openCashBox()
        }

        for (edittext in edtArray) {
            edittext.setOnFocusChangeListener(object : View.OnFocusChangeListener {
                override fun onFocusChange(v: View?, hasFocus: Boolean) {
                    if (hasFocus) {
                        val ic = edittext.onCreateInputConnection(EditorInfo())
                        keyboard.setInputConnection(ic)
                        edittext.setRawInputType(InputType.TYPE_CLASS_TEXT)
                        edittext.setTextIsSelectable(true)
                        edittext.requestFocus()
                    }else{
                        if (edittext.text.isNullOrEmpty())
                        {
                            edittext.setText("0.00")
                        }
                    }
                }
            })
        }


        edtCashWithdraw.setRawInputType(InputType.TYPE_CLASS_TEXT)
        edtCashWithdraw.setTextIsSelectable(true)

        val ic = edtCashWithdraw.onCreateInputConnection(EditorInfo())
        keyboard.setInputConnection(ic)

        ivCancel.setOnClickListener {
            dialog.dismiss()
        }
        var edt_amount = 0.0
        var remaining = 0.0


        edtCashWithdraw.isFocusable = true

        edtHighCash.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                try {
                    if (!TextUtils.isEmpty(s.toString())) {

                        if (edtHighCash.text.isNotEmpty() && edtHighCash.text.toString()
                                .toDouble() > 0 && edtShortCash.text.isNotEmpty() && edtShortCash.text.toString()
                                .toDouble() > 0
                        ) {
                            edtShortCash.setText("0.00")
                        }
                        val inputString = s.toString()
                        edtHighCash.removeTextChangedListener(this)
                        val cleanString = inputString.replace("[.]".toRegex(), "")
                        val bigDecimal: BigDecimal = BigDecimal(cleanString).setScale(
                            2,
                            BigDecimal.ROUND_FLOOR
                        ).divide(BigDecimal(100), BigDecimal.ROUND_FLOOR)
                        val converted: String = bigDecimal.toString()
                        edtHighCash.setText(converted)
                        edtHighCash.setSelection(converted.length)
                        edtHighCash.addTextChangedListener(this)
                    }
                } catch (e: Exception) {
                    LogUtil.d("EdtHighCash", e.message)
                }

            }

        })

        edtCashWithdraw.setText("0.00")
        edtCashWithdraw.setSelection(edtCashWithdraw.text.length)

        edtShortCash.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                try {
                    if (!TextUtils.isEmpty(s.toString())) {

                        if (edtShortCash.text.isNotEmpty() && edtShortCash.text.toString()
                                .toDouble() > 0 && edtHighCash.text.isNotEmpty() && edtHighCash.text.toString()
                                .toDouble() > 0
                        ) {
                            edtHighCash.setText("0.00")
                        }

                        val inputString = s.toString()
                        edtShortCash.removeTextChangedListener(this)
                        val cleanString = inputString.replace("[.]".toRegex(), "")
                        val bigDecimal: BigDecimal = BigDecimal(cleanString).setScale(
                            2,
                            BigDecimal.ROUND_FLOOR
                        ).divide(BigDecimal(100), BigDecimal.ROUND_FLOOR)
                        val converted: String = bigDecimal.toString()
                        edtShortCash.setText(converted)
                        edtShortCash.setSelection(converted.length)
                        edtShortCash.addTextChangedListener(this)
                    }
                } catch (e: Exception) {
                    LogUtil.d("EdtShortCash", e.message)
                }

                if (!s.toString().equals("")) {
                    try {
                        edt_amount = edtShortCash.text.toString().toDouble()

                        remaining = amount_check_out - (edt_amount + edtCashWithdraw.text.toString()
                            .toDouble())

                        if (remaining >= 0) {
                            val currency_symbol =
                                CM.getSp(this@DayStartCloseActivity, CV.CURRENCY_CODE, "") as String

                            tvBalanceInDrawer.setText(
                                currency_symbol + " " + String.format(
                                    "%.2f",
                                    remaining
                                )
                            )
                        } else {
                            showToast(
                                mActivity,
                                "You can't withdraw more than " + amount_check_out,
                                R.drawable.icon,
                                custom_toast_container
                            )
                            keyboard.allClear()
                            edtShortCash.setText("0.00")
                        }
                    } catch (e: NumberFormatException) {
                        showToast(
                            mActivity,
                            getString(R.string.msg_enter_proper_amount),
                            R.drawable.icon,
                            custom_toast_container
                        )
                        edtShortCash.setText("0.0")
                        edtShortCash.setSelection(edtShortCash.text.length)
                    }
                } else {
                    try {
                        remaining = amount_check_out - edtCashWithdraw.text.toString().toDouble()

                        val currency_symbol =
                            CM.getSp(this@DayStartCloseActivity, CV.CURRENCY_CODE, "") as String

                        tvBalanceInDrawer.setText(
                            currency_symbol + " " + String.format(
                                "%.2f",
                                remaining
                            )
                        )
                    } catch (e: NumberFormatException) {
                        showToast(
                            mActivity,
                            getString(R.string.msg_enter_proper_amount),
                            R.drawable.icon,
                            custom_toast_container
                        )

                        edtShortCash.setText("0.0")
                        edtShortCash.setSelection(edtShortCash.text.length)
                    }

                }
            }

        })

        edtCashWithdraw.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {

                try {
                    if (!TextUtils.isEmpty(s.toString())) {
                        val inputString = s.toString()
                        edtCashWithdraw.removeTextChangedListener(this)
                        val cleanString = inputString.replace("[.]".toRegex(), "")
                        val bigDecimal: BigDecimal = BigDecimal(cleanString).setScale(
                            2,
                            BigDecimal.ROUND_FLOOR
                        ).divide(BigDecimal(100), BigDecimal.ROUND_FLOOR)
                        val converted: String = bigDecimal.toString()
                        edtCashWithdraw.setText(converted)
                        edtCashWithdraw.setSelection(converted.length)
                        edtCashWithdraw.addTextChangedListener(this)
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }

                if (!s.toString().equals("")) {
                    try {
                        edt_amount = edtCashWithdraw.text.toString().toDouble()

                        remaining = amount_check_out - (edt_amount + edtShortCash.text.toString()
                            .toDouble())

                        if (remaining >= 0) {
                            val currency_symbol =
                                CM.getSp(this@DayStartCloseActivity, CV.CURRENCY_CODE, "") as String

                            tvBalanceInDrawer.setText(
                                currency_symbol + " " + String.format(
                                    "%.2f",
                                    remaining
                                )
                            )
                        } else {
                            showToast(
                                mActivity,
                                "You can't withdraw more than " + amount_check_out,
                                R.drawable.icon,
                                custom_toast_container
                            )
                            keyboard.allClear()
                            edtCashWithdraw.setText("0.00")
                        }
                    } catch (e: NumberFormatException) {
                        showToast(
                            mActivity,
                            getString(R.string.msg_enter_proper_amount),
                            R.drawable.icon,
                            custom_toast_container
                        )
                        edtCashWithdraw.setText("0.0")
                        edtCashWithdraw.setSelection(edtCashWithdraw.text.length)
                    }
                } else {
                    try {
                        remaining = amount_check_out - edtShortCash.text.toString().toDouble()

                        val currency_symbol =
                            CM.getSp(this@DayStartCloseActivity, CV.CURRENCY_CODE, "") as String

                        tvBalanceInDrawer.setText(
                            currency_symbol + " " + String.format(
                                "%.2f",
                                remaining
                            )
                        )
                    } catch (e: NumberFormatException) {
                        showToast(
                            mActivity,
                            getString(R.string.msg_enter_proper_amount),
                            R.drawable.icon,
                            custom_toast_container
                        )

                        edtCashWithdraw.setText("0.0")
                        edtCashWithdraw.setSelection(edtCashWithdraw.text.length)
                    }

                }

            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })

        if (!CM.isInternetAvailable(this)) {
            //offline code
            //display data

            val itemListLiveData = mDatabase.daoManualSync().getAllItems(emp_id)

            tvStartDay.setText(cash_in_drawer)
            tvCashInDrawer.setText(itemListLiveData.last_activity[0].dblCashInDrawer)
            tvTotalSalesCash.setText("")
            tvManualCashWithdrawel.setText("")
            tvBalanceInDrawer.setText("")
            tvTotalSalesNumber.setText("")
            tvSalesByCard.setText("")
            tvSalesByCash.setText("")
            tvTotalSalesCard.setText("")
            tvTotalSalesSplit.setText("")

        } else {
            webCallGetCheckOutData(
                tvStartDay,
                tvCashInDrawer,
                tvTotalSalesCash,
                tvManualCashWithdrawel,
                tvBalanceInDrawer,
                tvTotalSalesNumber,
                tvSalesByCard,
                tvSalesByCash,
                tvTotalSalesCard,
                tvTotalSalesSplit,
                tvTotalSalesOther,
                tvTotalPayIn,
                tvTotalPayOut,
                tvTotalRefund,
                tvSalesByOther,
                tvTotalRefundCount
            )
        }

        btnSubmit.setOnClickListener {
            if (TextUtils.isEmpty(edtCashWithdraw.text.toString())) {
                showToast(
                    mActivity,
                    getString(R.string.enter_amount),
                    R.drawable.icon,
                    custom_toast_container
                )
            } else if (TextUtils.isEmpty(edtHighCash.text.toString())) {
                showToast(
                    mActivity,
                    getString(R.string.enter_highcash_amount),
                    R.drawable.icon,
                    custom_toast_container
                )
            } else if (TextUtils.isEmpty(edtShortCash.text.toString())) {
                showToast(
                    mActivity,
                    getString(R.string.enter_shortcash_amount),
                    R.drawable.icon,
                    custom_toast_container
                )
            } else if (edtCashWithdraw.text.toString() == "0.") {
                showToast(
                    mActivity,
                    getString(R.string.enter_valid_amount),
                    R.drawable.icon,
                    custom_toast_container
                )
            } else {
                totalTransactionalAmount = tvCashInDrawer.text as String
                if (!CM.isInternetAvailable(this)) {
                    //offline code
                    addCheckOutDataOffline(tvCashInDrawer, dialog)

                } else {
                    var totalCashInDrawer = 0.0
                    if (tvBalanceInDrawer.text.toString().isNotEmpty()) {
                        val strArray = tvBalanceInDrawer.text.toString().split(" ")
                        if (strArray.size > 0) {
                            totalCashInDrawer = strArray[1].toDouble()
                        }
                    }
                    webCallSendCheckInOutData(
                        edtCashWithdraw.text.toString().toDouble(),
                        edtHighCash.text.toString().toDouble(),
                        edtShortCash.text.toString().toDouble(),
                        totalCashInDrawer,
                        AppConstant.SHIFT_CLOSE,
                        "",
                        dialog
                    )
                }
            }
        }

    }

    @SuppressLint("SetTextI18n")
    private fun webCallGetCheckOutData(
        tvStartDay: TextView,
        tvCashInDrawer: TextView,
        tvTotalSalesCash: TextView,
        tvManualCashWithdrawel: TextView,
        tvBalanceInDrawer: TextView,
        tvTotalSalesNumber: TextView,
        tvSalesByCard: TextView,
        tvSalesByCash: TextView,
        tvTotalSalesCard: TextView,
        tvTotalSalesSplit: TextView,
        tvTotalSalesOther: TextView,
        tvTotalPayIn: TextView,
        tvTotalPayOut: TextView,
        tvTotalRefund: TextView,
        tvSalesByOther: TextView,
        tvTotalRefundCount: TextView

    ) {
        showKcsDialog()

        checkOutDataViewModel!!.getCheckOutData(emp_id)
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                } else {
                    mCheckOutData = loginResponseModel.data!!.data
                    otherpaymentlist=loginResponseModel.data!!.data.otherPaymentMethodDetails

                    val currency_symbol = CM.getSp(this, CV.CURRENCY_CODE, "") as String

                    tvStartDay.setText(
                        currency_symbol + " " + String.format(
                            "%.2f",
                            loginResponseModel.data!!.data.startDay.toDouble()
                        )
                    )
                    tvCashInDrawer.setText(
                        currency_symbol + " " + String.format(
                            "%.2f",
                            loginResponseModel.data!!.data.totalTxnAmount.toDouble()
                        )
                    )
                    CM.setSp(
                        mActivity,
                        CV.CASH_IN_DRAWER,
                        loginResponseModel.data!!.data.balanceCashInDrawer
                    )


                    tvTotalSalesCash.setText(currency_symbol + " " + loginResponseModel.data!!.data.salesCash)
                    tvManualCashWithdrawel.setText(currency_symbol + " " + loginResponseModel.data!!.data.manualCashWithdrawal)
                    tvBalanceInDrawer.setText(
                        currency_symbol + " " + String.format(
                            "%.2f",
                            loginResponseModel.data!!.data.balanceCashInDrawer.toDouble()
                        )
                    )

                    tvTotalSalesNumber.setText(" X " + loginResponseModel.data!!.data.totalSalesNumber)
                    tvSalesByCard.setText(" X " + loginResponseModel.data!!.data.totalSalesByCard)
                    tvSalesByCash.setText(" X " + loginResponseModel.data!!.data.totalSalesByCash)
                    tvTotalSalesCard.setText(
                        currency_symbol + " " + String.format(
                            "%.2f",
                            loginResponseModel.data!!.data.totalSalesAmountByCard.toDouble()
                        )
                    )
                    tvTotalSalesSplit.setText(
                        currency_symbol + " " + String.format(
                            "%.2f",
                            loginResponseModel.data!!.data.totalSplitAmount.toDouble()
                        )
                    )
                    tvTotalSalesOther.setText(
                        currency_symbol + " " + String.format(
                            "%.2f",
                            loginResponseModel.data!!.data.totalSalesByOther.toDouble()
                        )
                    )
                    tvTotalPayIn.setText(
                        currency_symbol + " " + String.format(
                            "%.2f",
                            loginResponseModel.data!!.data.totalPayInAmount.toDouble()
                        )
                    )
                    tvTotalPayOut.setText(
                        currency_symbol + " " + String.format(
                            "%.2f",
                            loginResponseModel.data!!.data.totalOutInAmount.toDouble()
                        )
                    )
                    tvTotalRefund.setText(
                        currency_symbol + " " + String.format(
                            "%.2f",
                            loginResponseModel.data!!.data.totalRefundAmount.toDouble()
                        )
                    )
                    tvSalesByOther.setText(" X " + loginResponseModel.data!!.data.totalSalesByOtherCount)
                    tvTotalRefundCount.setText(" X " + loginResponseModel.data!!.data.totalRefundCount.toString())

                    //   amount_check_out = tvBalanceInDrawer.text.toString().toDouble()
                    amount_check_out =
                        tvBalanceInDrawer.text.toString().split(" ").get(1).toDouble()

                }

            })
    }
    private fun openOtherPaymentDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.setCallback(
            UserInteractionAwareCallback(
                dialog.window!!.getCallback(),
                mActivity
            )
        )
        dialog.setContentView(R.layout.dialog_cashier_otherpayment_checkout)

        dialog.setCancelable(false)
        dialog.show()

        val ivCancel = dialog.findViewById<ImageView>(R.id.ivCancel)

        val other_list_layout = dialog.findViewById<LinearLayout>(R.id.other_list_layout)
        val currency_symbol = CM.getSp(this, CV.CURRENCY_CODE, "") as String
        if (otherpaymentlist.size>0)
        {
            for (paymentmodel in otherpaymentlist)
            {
                var inflater = LayoutInflater.from(this).inflate(R.layout.item_other_payment, null)
                var tvsalestitle=inflater.findViewById<TextView>(R.id.tvSalesTitle)
                var tvSalesByOther=inflater.findViewById<TextView>(R.id.tvSalesByOther)
                var tvTotalSalesOther=inflater.findViewById<TextView>(R.id.tvTotalSalesOther)
                tvsalestitle.text=paymentmodel.payment_method
                tvSalesByOther.setText("X"+paymentmodel.total_sale_number)
                tvTotalSalesOther.text=currency_symbol + " " + paymentmodel.total_sale_amount
                other_list_layout.addView(inflater)
            }
        }

        ivCancel.setOnClickListener {
            dialog.dismiss()
        }



    }

    private fun webCallSendCheckInOutData(
        cashInDrawer: Double,
        highCash: Double,
        shortCash: Double,
        totalCashInDrawer: Double,
        type: String,
        remark: String,
        dialog: Dialog?
    ) {

        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.WS_DATE_FORMATTE)
        showKcsDialog()

        checkInOutDataViewModel!!.checkInOutData(
            sdf.format(date),
            cashInDrawer,
            highCash,
            shortCash,
            type,
            emp_id,
            remark,
            totalCashInDrawer,
            emp_id,
            0,
            0
        )
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                } else {
                    showToast(
                        mActivity,
                        loginResponseModel.data!!.message,
                        R.drawable.icon,
                        custom_toast_container
                    )

                    CM.setSp(mActivity, CV.CASH_IN_DRAWER, totalCashInDrawer.toString())

                    dialog!!.dismiss()
                    if (type.equals(AppConstant.CLOCK_IN)) {
                        if (emp_role.equals(AppConstant.CASHIER)) {
                            CM.setSp(this, CV.ISCHECKIN, true)
                            CM.setSp(this, CV.CHECKINUSER, emp_id)
                        }
                    } else {
                        if (emp_role.equals(AppConstant.CASHIER)) {
                            CM.setSp(this, CV.ISCHECKIN, false)
                            CM.setSp(this, CV.CHECKINUSER, 0)
                        }
                    }


                    LogUtil.e("HARDWARE_DEBUG", "ALL HARDWARE DEBUG FROM DAYSTART/CLOSE")
                    LogUtil.e("HARDWARE_DEBUG", "type : " + type.toString())
                    val sdfd = SimpleDateFormat(CV.DISPLAY_DATE_FORMATTE_AMPM)
                    if (type.equals(AppConstant.SHIFT_START)) {
                        PrintDayStartReceipt(
                            cash_in_drawer,
                            cashInDrawer,
                            totalCashInDrawer,
                            sdfd.format(date)
                        )
                    } else if (type.equals(AppConstant.SHIFT_CLOSE)) {
                        PrintDayCloseReceipt(
                            cashInDrawer.toString(),
                            sdfd.format(date),
                            highCash.toString(),
                            shortCash.toString(),
                            totalCashInDrawer
                        )
                    }
                    if (AppConstant.SHIFT_START == type) {
                        openCashBox()
                    }
                    //openCashBox() /*no need to required open drawer while shift close*/
                    CM.finishActivity(this)
                }

            })
    }

    private fun PrintDayStartReceipt(
        CashInDrawer: String,
        AddAmount: Double,
        TotalCash: Double,
        DateTime: String
    ) {
        LogUtil.e("HARDWARE_DEBUG", "PrintDayStartReceipt  -               " + mPrinterController)
        var POSName = CM.getSp(this, CV.POS_NAME, "") as String
        val currency_symbol = CM.getSp(this@DayStartCloseActivity, CV.CURRENCY_CODE, "") as String
        val mPrinterDayStartModel = PrinterDayStartModel()

        mPrinterDayStartModel.setCashInDrawer(currency_symbol + " " + CashInDrawer)
        mPrinterDayStartModel.setAddCashAmount(currency_symbol + " " + AddAmount)
        mPrinterDayStartModel.setTotalCashIndrawer(currency_symbol + " " + TotalCash)

        mPrinterDayStartModel.setDayStartDate(DateTime)
        mPrinterDayStartModel.setCashierName(emp_name)
        mPrinterDayStartModel.setPOSStationNo(POSName)
        mPrinterDayStartModel.setEmpRole(emp_role)

        mPrinterController.printDayStartTicket(mPrinterDayStartModel)
    }

    private fun PrintDayCloseReceipt(
        CashWithdraw: String,
        mDateTime: String,
        highCash: String,
        shortCash: String,
        totalCashInDrawer: Double
    ) {
        LogUtil.e("HARDWARE_DEBUG", "PrintDayCloseReceipt  -               " + mPrinterController)
        var POSName = CM.getSp(this, CV.POS_NAME, "") as String
        val currency_symbol = CM.getSp(this@DayStartCloseActivity, CV.CURRENCY_CODE, "") as String

        val mPrinterDayCloseModel = PrinterDayCloseModel()

        mPrinterDayCloseModel.setDayOpningCash(currency_symbol + " " + mCheckOutData.dblCashInDrawer)

        mPrinterDayCloseModel.setTotalSaleByCash(currency_symbol + " " + mCheckOutData.salesCash)
        mPrinterDayCloseModel.setTotalSaleByCard(currency_symbol + " " + mCheckOutData.totalSalesAmountByCard)
        mPrinterDayCloseModel.setTotalSaleBySplit(currency_symbol + " " + mCheckOutData.totalSplitAmount)
        mPrinterDayCloseModel.setTotalSaleByOther(currency_symbol + " " + mCheckOutData.totalSalesByOther)
        mPrinterDayCloseModel.setTotalRefund(currency_symbol + " " + mCheckOutData.totalRefundAmount)
        mPrinterDayCloseModel.setTotalPayIn(currency_symbol + " " + mCheckOutData.totalPayInAmount)
        mPrinterDayCloseModel.setTotalPayOut(currency_symbol + " " + mCheckOutData.totalOutInAmount)
        mPrinterDayCloseModel.setWithdrawCash(currency_symbol + " " + CashWithdraw)


        mPrinterDayCloseModel.setTotalNumberofTransactionAmount(mCheckOutData.totalSalesNumber)
        mPrinterDayCloseModel.setTotalNumberofSaleByCard(" X " + mCheckOutData.totalSalesByCard)
        mPrinterDayCloseModel.setTotalNumberofSaleByCash(" X " + mCheckOutData.totalSalesByCash)
        mPrinterDayCloseModel.setTotalNumberofSaleByOther(" X " + mCheckOutData.totalSalesByOtherCount)
        mPrinterDayCloseModel.setTotalNumberofRefund(" X " + mCheckOutData.totalRefundCount)


        var TotalTransaction = totalTransactionalAmount.split(" ").get(1).toDouble()

        val mTotalTransaction = DecimalFormat("#.00").format(TotalTransaction).toDouble()

        mPrinterDayCloseModel.setTotalTransactionAmount(
            currency_symbol + " " + Math.abs(mTotalTransaction)
        )

        var DayCloseCash = ((mTotalTransaction.toDouble())).minus(CashWithdraw.toDouble())

        mPrinterDayCloseModel.setDayCloseCash(currency_symbol + " " + totalCashInDrawer)


        mPrinterDayCloseModel.setDayCloseDateTime(mDateTime)
        mPrinterDayCloseModel.setCashierName(emp_name)
        mPrinterDayCloseModel.setPOSStationNo(POSName)
        mPrinterDayCloseModel.setEmpRole(emp_role)
        mPrinterDayCloseModel.highCash = currency_symbol + " " + highCash
        mPrinterDayCloseModel.shortCash = currency_symbol + " " + shortCash

        mPrinterController.printDayCloseTicket(mPrinterDayCloseModel)

    }

    private fun webCallGetCheckInData(tvCashInDrawer: TextView) {
        showKcsDialog()

        checkInDataViewModel!!.getCheckInData(emp_id)
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                } else {
                    val currency_symbol =
                        CM.getSp(this@DayStartCloseActivity, CV.CURRENCY_CODE, "") as String
                    Log.e("CASH", "" + loginResponseModel.data!!.data.dblCashInDrawer)
                    cash_in_drawer= loginResponseModel.data!!.data.dblCashInDrawer
                    tvCashInDrawer.setText(currency_symbol + " " + loginResponseModel.data!!.data.dblCashInDrawer)
                    CM.setSp(
                        mActivity,
                        CV.CASH_IN_DRAWER,
                        loginResponseModel.data!!.data.dblCashInDrawer
                    )
                }

            })

    }

    override fun onRequestGranted(requestCode: Int, perms: List<String>) {

    }
}
