package com.ehsm.recore.activities

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ehsm.recore.R
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.databinding.ActivityPasscodeBinding
import com.ehsm.recore.model.CDSSalesModel
import com.ehsm.recore.utils.*
import com.ehsm.recore.utils.CM.showToast
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.kcspl.divyangapp.viewmodel.ManualSyncViewModel
import com.kcspl.divyangapp.viewmodel.VerifyMPinViewModel
import kotlinx.android.synthetic.main.custom_toast.*


class PasscodeActivity : BaseActivity<ActivityPasscodeBinding>(), DialogInterface.OnClickListener, View.OnClickListener {

    private lateinit var mActivity: PasscodeActivity
    private var pin = ""
    private var count = 0
    private var minusCount = false
    private var viewModelVerifyMPin: VerifyMPinViewModel? = null
    private var manualSyncViewModel: ManualSyncViewModel? = null
    private lateinit var mDatabase: RecoreDB
    var result = false
    var idFromPassCodeApi: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        mDatabase = RecoreDB.getInstance(mActivity)!!

        viewModelVerifyMPin = ViewModelProviders.of(this).get(VerifyMPinViewModel::class.java)
        manualSyncViewModel = ViewModelProviders.of(this).get(ManualSyncViewModel::class.java)

        CM.removeStatusBar(this)
        //bindView(R.layout.activity_passcode)
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_passcode,
            getWindow().getDecorView().getRootView() as ViewGroup?,false)
        setContentView(mBinding.root)
        FirebaseCrashlytics.getInstance().setUserId(CM.getSp(mActivity,CV.STORE_ID, 0).toString()+"   "+CM.getSp(mActivity,CV.POS_NAME, "").toString())
        FirebaseCrashlytics.getInstance().setCustomKey("Dataid",CM.getSp(mActivity,CV.STORE_ID, 0).toString()+"   "+CM.getSp(mActivity,CV.POS_NAME, "").toString())
        FirebaseCrashlytics.getInstance().log(CM.getSp(mActivity,CV.STORE_ID, 0).toString()+"   "+CM.getSp(mActivity,CV.POS_NAME, "").toString())
        CM.setSp(ScreenLockTimer.mActivity, CV.IS_PASSCODE_SUBMITED, false)

        if (intent.extras != null) {
            result = intent.extras!!.getBoolean("isFromSplash")
        }
        mBinding.btnOneCredential.setOnClickListener(this)
        mBinding.btnTwoCredential.setOnClickListener(this)
        mBinding.btnThreeCredential.setOnClickListener(this)
        mBinding.btnFourCredential.setOnClickListener(this)
        mBinding.btnFiveCredential.setOnClickListener(this)
        mBinding.btnSixCredential.setOnClickListener(this)
        mBinding.btnSevenCredential.setOnClickListener(this)
        mBinding.btnEightCredential.setOnClickListener(this)
        mBinding.btnNineCredential.setOnClickListener(this)
        mBinding.btnZeroCredential.setOnClickListener(this)
        mBinding.btnBackSpaceCredential.setOnClickListener(this)



        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) !== PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
            } else {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
            }
        } else {
            //Hardware Code For CDS Display -- Normal Image Display Case Invoke
            checkDrawOverlayPermission(AppConstant.CDS_PAUSE_SCREEN, "", false, "", CDSSalesModel(), "", false)
        }


    }

    override fun onClick(v: View?) {
        if (!isOverLayPermission()) {
            checkDrawOverlayPermission(AppConstant.CDS_PAUSE_SCREEN, "", false, "", CDSSalesModel(), "", false)
        } else {
            when (v) {
                mBinding.btnOneCredential -> {
                    countCall(count)
                    fillPasscode("1")
                }
                mBinding.btnTwoCredential -> {
                    countCall(count)
                    fillPasscode("2")
                }
                mBinding.btnThreeCredential -> {
                    countCall(count)
                    fillPasscode("3")
                }
                mBinding.btnFourCredential -> {
                    countCall(count)
                    fillPasscode("4")
                }
                mBinding.btnFiveCredential -> {
                    countCall(count)
                    fillPasscode("5")
                }
                mBinding.btnSixCredential -> {
                    countCall(count)
                    fillPasscode("6")
                }
                mBinding.btnSevenCredential -> {
                    countCall(count)
                    fillPasscode("7")
                }
                mBinding.btnEightCredential -> {
                    countCall(count)
                    fillPasscode("8")
                }
                mBinding.btnNineCredential -> {
                    countCall(count)
                    fillPasscode("9")
                }
                mBinding.btnZeroCredential -> {
                    countCall(count)
                    fillPasscode("0")
                }
                mBinding.btnBackSpaceCredential -> {
                    dropPasscode()
                    if (count > 0) {
                        count -= 1
                    }
                    minusCount = true
                    countCall(count)
                }

            }
        }
    }

    fun countCall(c: Int) {
        when (c) {
            0 -> if (minusCount) {
                count = 0
                mBinding.ivRoundOneCredential.setImageResource(R.drawable.roundblankbutton)
                mBinding.ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                mBinding.ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                mBinding.ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                minusCount = false
            } else {
                count += 1
                mBinding.ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                mBinding.ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                mBinding.ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

            }
            1 -> if (minusCount) {
                count = 1
                mBinding.ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                mBinding.ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                mBinding.ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                minusCount = false
            } else {
                mBinding.ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                mBinding.ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                count += 1
            }
            2 -> if (minusCount) {
                count = 2
                mBinding.ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                mBinding.ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                minusCount = false
            } else {
                mBinding.ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                count += 1
            }
            3 -> if (minusCount) {
                count = 3
                mBinding.ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                minusCount = false
            } else {
                mBinding.ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                count += 1
            }
            4 -> if (minusCount) {
                count = 4
                mBinding.ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                minusCount = false
            } else {
                mBinding.ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                count += 1
            }
            5 -> if (minusCount) {
                count = 5
                mBinding.ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                minusCount = false
            } else {
                mBinding.ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                mBinding.ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                count += 1
            }
        }
    }

    fun fillPasscode(code: String) {
        if (count <= 4 && count > 0) {
            pin = pin + code
        }
        if (count == 4) {
            try {
                if (!CM.isInternetAvailable(mActivity)) {
                    pin = ""
                    CM.showMessageOK(
                            mActivity,
                            "",
                            resources.getString(R.string.msg_network_error),
                            mActivity
                    )
                } else {
                    webcallVerifyMPin()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    private fun webcallVerifyMPin() {
        showKcsDialog()
        viewModelVerifyMPin!!.verifyMPin(pin)
                ?.observe(this, Observer { loginResponseModel ->
                    dismissKcsDialog()
                    if (loginResponseModel.data == null) {
                        loginResponseModel?.message?.let {
                            DialogUtility.showPinInvalidateDialog(
                                    this@PasscodeActivity,
                                    it,
                                    loginResponseModel.action
                            )
                        }
                        val shake =
                                AnimationUtils.loadAnimation(this, R.anim.shake)
                        mBinding.llPinRound.startAnimation(shake)
                        count = 0
                        mBinding.ivRoundOneCredential.setImageResource(R.drawable.roundblankbutton)
                        mBinding.ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                        mBinding.ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                        mBinding.ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                        minusCount = false
                        pin = ""

                    } else {
                        showToast(mActivity, loginResponseModel.data!!.message, R.drawable.icon, custom_toast_container)

                        CM.setSp(
                                this,
                                CV.EMP_NAME,
                                loginResponseModel.data!!.data.strFirstName + " " + loginResponseModel.data!!.data.strLastName
                        )
                        CM.setSp(this, CV.EMP_OPEN_DISCOUNT, loginResponseModel.data!!.data.tIsOpenDiscount)
                        CM.setSp(this, CV.EMP_IMAGE, loginResponseModel.data!!.data.strProfilePhoto)
                        CM.setSp(this, CV.EMP_ID, loginResponseModel.data!!.data.iCheckEmployeeId)
                        CM.setSp(this, CV.EMP_ROLE, loginResponseModel.data!!.data.strEmployeeRoleName)

                        idFromPassCodeApi = loginResponseModel.data!!.data.iCheckEmployeeId

                        webCallManualSync()
                    }
                })
    }

    private fun webCallManualSync() {
        val emp_id = CM.getSp(mActivity, CV.EMP_ID, 0) as Int

        showKcsDialog()
        manualSyncViewModel!!.manualsync(mDatabase, emp_id)
                ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                    dismissKcsDialog()
                    if (loginResponseModel.data == null) {
                        showToast(mActivity, loginResponseModel.message!!, R.drawable.icon, custom_toast_container)
                    } else {

                        val bundle = Bundle()
                        bundle.putString("test", "abc")
                        bundle.putString(
                                FirebaseAnalytics.Param.ITEM_NAME,
                                loginResponseModel.data!!.message
                        )
                        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)

                        ScreenLockTimer.stopHandler()
                        mActivity.mCDSController.onResumeCDSScreen(mActivity)
                        if (result || idFromPassCodeApi != AppConstant.EMP_ID) {
                            CM.setSp(this, CV.IS_PASSCODE_SUBMITED, true)
                            CM.startActivityWitClearTop(mActivity, LandingPageActivity::class.java)
                            CM.finishActivity(mActivity)
                        } else {
                            if (CM.getSp(mActivity, CV.ISLOGINCALLED, false) as Boolean) {
                                CM.setSp(this, CV.IS_PASSCODE_SUBMITED, true)
                                CM.startActivityWitClearTop(mActivity, LandingPageActivity::class.java)
                                CM.finishActivity(mActivity)
                                CM.setSp(this, CV.ISLOGINCALLED, false)
                            } else {
                                CM.setSp(this, CV.IS_PASSCODE_SUBMITED, true)
                                val returnIntent = Intent()
                                returnIntent.putExtra("result", result)
                                setResult(Activity.RESULT_OK, returnIntent)
                                CM.finishActivity(mActivity)
                            }
                        }


                    }
                })
    }


    fun dropPasscode() {
        if (count <= 4 && count > 0) {
            if (pin.length > 0) {
                pin = pin.substring(0, pin.length - 1)
            }
        }
    }

    override fun onRequestGranted(requestCode: Int, perms: List<String>) {

    }

    override fun onClick(dialog: DialogInterface?, which: Int) {
        dialog!!.dismiss()
        count = 0
        mBinding.ivRoundOneCredential.setImageResource(R.drawable.roundblankbutton)
        mBinding.ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
        mBinding.ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
        mBinding.ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)
    }

    override fun onRequestPermissionsResult(
            requestCode: Int, permissions: Array<String>,
            grantResults: IntArray
    ) {
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if ((ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) === PackageManager.PERMISSION_GRANTED)) {
                        showToast(
                                mActivity,
                                getString(R.string.permission_granted),
                                R.drawable.icon,
                                custom_toast_container
                        )

                    }
                } else {
                    showToast(
                            mActivity,
                            getString(R.string.permission_denied),
                            R.drawable.icon,
                            custom_toast_container
                    )
                }
                //Hardware Code For CDS Display -- Normal Image Display Case Invoke
                checkDrawOverlayPermission(AppConstant.CDS_PAUSE_SCREEN, "", false, "", CDSSalesModel(), "", false)
                return
            }
        }
    }

    override fun onBackPressed() {

    }

    fun isOverLayPermission(): Boolean {
        ScreenLockTimer.stopHandler()
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && Settings.canDrawOverlays(this)) {
            //Hardware Code For CDS Display -- Normal Image Display Case Invoke
            return true
        } else {
            return false
        }
    }


}
