package com.ehsm.recore.activities

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.ViewGroup
import android.view.Window
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ehsm.recore.R
import com.ehsm.recore.adapter.ForceCheckOutListAdapter
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.databinding.ActivityForceCheckoutBinding
import com.ehsm.recore.model.ForceCheckOutModel
import com.ehsm.recore.utils.*
import com.ehsm.recore.utils.CM.showToast
import com.kcspl.divyangapp.viewmodel.*
import kotlinx.android.synthetic.main.custom_toast.*
import kotlinx.android.synthetic.main.header_layout.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ForceCheckOutActivity : BaseActivity<ActivityForceCheckoutBinding>(),
    ForceCheckOutListAdapter.ItemClick {

    private lateinit var mActivity: ForceCheckOutActivity
    private var forceCheckOutListViewModel: ForceCheckOutListViewModel? = null
    private var viewModelVerifyMPin: VerifyMPinViewModel? = null
    private var checkOutDataViewModel: CheckOutDataViewModel? = null
    private var viewModelChangeMPin: ChangeMPinViewModel? = null

    private lateinit var mDatabase: RecoreDB
    private var forceCheckoutList = ArrayList<ForceCheckOutModel>()
    private lateinit var selected_force_checkout_row: ForceCheckOutModel
    private var emp_name = ""
    var amount_check_out = 0.0


    private lateinit var ivRoundOneCredential: ImageView
    private lateinit var ivRoundTwoCredential: ImageView
    private lateinit var ivRoundThreeCredential: ImageView
    private lateinit var ivRoundFourCredential: ImageView
    private lateinit var llPinRound: LinearLayout

    private var pin = ""
    private var count = 0
    private var minusCount = false
    private lateinit var dialog: Dialog

    private var store_admin_name = ""
    private var store_admin_id = 0
    private var checkInOutDataViewModel: CheckInOutDataViewModel? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        mDatabase = RecoreDB.getInstance(mActivity)!!
        CM.removeStatusBar(mActivity)
        forceCheckOutListViewModel =
            ViewModelProviders.of(this).get(ForceCheckOutListViewModel::class.java)
        checkOutDataViewModel = ViewModelProviders.of(this).get(CheckOutDataViewModel::class.java)
        checkInOutDataViewModel =
            ViewModelProviders.of(this).get(CheckInOutDataViewModel::class.java)
        viewModelChangeMPin = ViewModelProviders.of(this).get(ChangeMPinViewModel::class.java)

        viewModelVerifyMPin = ViewModelProviders.of(this).get(VerifyMPinViewModel::class.java)

        emp_name = CM.getSp(mActivity, CV.EMP_NAME, "") as String


        //bindView(R.layout.activity_force_checkout)
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_force_checkout,
            getWindow().getDecorView().getRootView() as ViewGroup?,false)
        setContentView(mBinding.root)

        setEmpView(mBinding.header.tvEmpName)
        setStoreNameView(mBinding.header.tvstorename, mBinding.header.tvposname)

        mBinding.header.ivLogo.setOnClickListener {
            CM.finishActivity(this)
        }

        mBinding.header.ivNewSale.setOnClickListener {
            CM.navigateToDashboard(this)
        }

        mBinding.header.ivNoSale.setOnClickListener {
            CM.openNosaleDialog(this, custom_toast_container)
        }

        mBinding.header.reluser.setOnClickListener {
            callChangeMpin()
        }
        mBinding.header.ivMore.setOnClickListener {
            CM.showPopup(mActivity, mBinding.header.ivMore, {
                syncWithServer()
            })
        }

        if (!CM.isInternetAvailable(mActivity)) {
            val stateListLiveData = mDatabase.daoForceCheckOutList().getAllForceCheckOutList()
            stateListLiveData.observe(this, androidx.lifecycle.Observer { statelist ->
                if (statelist != null) {
                    forceCheckoutList = statelist as ArrayList<ForceCheckOutModel>
                    mBinding.rvForceCheckOutList.adapter =
                        ForceCheckOutListAdapter(mActivity, forceCheckoutList, this)
                }
            })

        } else {
            val date = Calendar.getInstance().time
            val sdf = SimpleDateFormat(CV.WS_DATE_FORMATTE)
            webCallgetForceCheckOutList(sdf.format(date))
        }
    }

    private fun webCallgetForceCheckOutList(date: String) {
        showKcsDialog()
        forceCheckOutListViewModel!!.getForceCheckOutList(mDatabase, date)
            ?.observe(this, Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                } else {
                    forceCheckoutList = loginResponseModel.data!!.data
                    showToast(
                        mActivity,
                        loginResponseModel.data!!.message,
                        R.drawable.icon,
                        custom_toast_container
                    )
                    mBinding.rvForceCheckOutList.adapter =
                        ForceCheckOutListAdapter(mActivity, forceCheckoutList, this)


                }

            })

    }

    override fun onRequestGranted(requestCode: Int, perms: List<String>) {
    }

    override fun listItemClicked(position: Int, forceCheckOutModel: ForceCheckOutModel) {
        selected_force_checkout_row = forceCheckOutModel
        openPassCodeDialog()

    }

    private fun openPassCodeDialog() {
        dialog = Dialog(mActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setContentView(R.layout.dialog_passcode)

        dialog.setCancelable(false)
        dialog.show()

        val btnOneCredential = dialog.findViewById<Button>(R.id.btnOneCredential)
        val btnTwoCredential = dialog.findViewById<Button>(R.id.btnTwoCredential)
        val btnThreeCredential = dialog.findViewById<Button>(R.id.btnThreeCredential)
        val btnFourCredential = dialog.findViewById<Button>(R.id.btnFourCredential)
        val btnFiveCredential = dialog.findViewById<Button>(R.id.btnFiveCredential)
        val btnSixCredential = dialog.findViewById<Button>(R.id.btnSixCredential)
        val btnSevenCredential = dialog.findViewById<Button>(R.id.btnSevenCredential)
        val btnEightCredential = dialog.findViewById<Button>(R.id.btnEightCredential)
        val btnNineCredential = dialog.findViewById<Button>(R.id.btnNineCredential)
        val btnZeroCredential = dialog.findViewById<Button>(R.id.btnZeroCredential)

        val btnBackSpaceCredential = dialog.findViewById<Button>(R.id.btnBackSpaceCredential)
        val btnResetMpin = dialog.findViewById<Button>(R.id.btnResetMpin)

        ivRoundOneCredential = dialog.findViewById(R.id.ivRoundOneCredential)
        ivRoundTwoCredential = dialog.findViewById(R.id.ivRoundTwoCredential)
        ivRoundThreeCredential = dialog.findViewById(R.id.ivRoundThreeCredential)
        ivRoundFourCredential = dialog.findViewById(R.id.ivRoundFourCredential)

        llPinRound = dialog.findViewById(R.id.llPinRound)

        val tvClose = dialog.findViewById<TextView>(R.id.tvClose)
        tvClose.setOnClickListener {
            dialog.dismiss()
        }
        btnResetMpin.setOnClickListener {
            // openChangePasscodeDialog()
            dialog.dismiss()
        }
        btnOneCredential.setOnClickListener({
            countCall(count)
            fillPasscode("1")
        })

        btnTwoCredential.setOnClickListener({
            countCall(count)
            fillPasscode("2")
        })
        btnThreeCredential.setOnClickListener({
            countCall(count)
            fillPasscode("3")
        })
        btnFourCredential.setOnClickListener({
            countCall(count)
            fillPasscode("4")
        })
        btnFiveCredential.setOnClickListener({
            countCall(count)
            fillPasscode("5")
        })
        btnSixCredential.setOnClickListener({
            countCall(count)
            fillPasscode("6")
        })
        btnSevenCredential.setOnClickListener({
            countCall(count)
            fillPasscode("7")
        })
        btnEightCredential.setOnClickListener({
            countCall(count)
            fillPasscode("8")
        })
        btnNineCredential.setOnClickListener({
            countCall(count)
            fillPasscode("9")
        })
        btnZeroCredential.setOnClickListener({
            countCall(count)
            fillPasscode("0")
        })
        btnBackSpaceCredential.setOnClickListener({
            dropPasscode()
            if (count > 0) {
                count -= 1
            }
            minusCount = true
            countCall(count)
        })
    }

    fun fillPasscode(code: String) {
        if (count <= 4 && count > 0) {
            pin = pin + code
        }
        if (count == 4) {
            try {
                if (!CM.isInternetAvailable(mActivity)) {
                    CM.showMessageOK(
                        mActivity,
                        "",
                        resources.getString(R.string.msg_network_error),
                        null
                    )
                } else {
                    webcallVerifyMPin()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun dropPasscode() {
        if (count <= 4 && count > 0) {
            if (pin.length > 0) {
                pin = pin.substring(0, pin.length - 1)
            }
        }
    }

    fun countCall(c: Int) {
        when (c) {
            0 -> if (minusCount) {
                count = 0
                ivRoundOneCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                minusCount = false
            } else {
                count += 1
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

            }
            1 -> if (minusCount) {
                count = 1
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                minusCount = false
            } else {
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                count += 1
            }
            2 -> if (minusCount) {
                count = 2
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                minusCount = false
            } else {
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                count += 1
            }
            3 -> if (minusCount) {
                count = 3
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                minusCount = false
            } else {
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                count += 1
            }
            4 -> if (minusCount) {
                count = 4
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                minusCount = false
            } else {
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                count += 1
            }
            5 -> if (minusCount) {
                count = 5
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                minusCount = false
            } else {
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                count += 1
            }
        }
    }

    private fun webcallVerifyMPin() {
        showKcsDialog()
        viewModelVerifyMPin!!.verifyMPin(pin)
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
//                    Toast.makeText(mActivity, loginResponseModel.message, Toast.LENGTH_LONG).show()

                    loginResponseModel.message?.let {
                        DialogUtility.showPinInvalidateDialog(
                            mActivity,
                            it,
                            loginResponseModel.action
                        )
                    }
                    val shake =
                        AnimationUtils.loadAnimation(this, R.anim.shake)
                    llPinRound.startAnimation(shake)
                    count = 0
                    ivRoundOneCredential.setImageResource(R.drawable.roundblankbutton)
                    ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                    ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                    ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                    minusCount = false
                    pin = ""

                } else {
                    showToast(
                        mActivity,
                        loginResponseModel.data!!.message,
                        R.drawable.icon,
                        custom_toast_container
                    )

                    dialog.dismiss()
                    store_admin_name =
                        loginResponseModel.data!!.data.strFirstName + " " + loginResponseModel.data!!.data.strLastName
                    store_admin_id = loginResponseModel.data!!.data.iCheckEmployeeId
                    count = 0
                    minusCount = false
                    pin = ""
                    if (selected_force_checkout_row.strEmployeeRoleName == AppConstant.CASHIER) {
                        openCheckOutCashierDialog()
                    } else {
                        openCheckOutOtherDialog()
                    }


                }

            })
    }

    private fun openCheckOutOtherDialog() {
        val dialog = Dialog(mActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setContentView(R.layout.dialog_force_checkout_other)

        dialog.setCancelable(false)
        val tvUserNameLogin = dialog.findViewById<TextView>(R.id.tvUserNameLogin)
        val btnSubmit = dialog.findViewById<Button>(R.id.btnSubmit)
        val ivCancel = dialog.findViewById<ImageView>(R.id.ivCancel)
        val edtReason = dialog.findViewById<EditText>(R.id.edtReason)

        ivCancel.setOnClickListener {
            dialog.dismiss()
        }
        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.DISPLAY_DATE_FORMATTE_AMPM)
        tvUserNameLogin.setText(
            "Force Check Out " + "\n Staff " + emp_name + " By " + store_admin_name + "\n" + sdf.format(
                date
            )
        )

        btnSubmit.setOnClickListener {
            if (!CM.isInternetAvailable(mActivity)) {
                //offline code

            } else {
                if (edtReason.text.toString().equals("")) {
                    showToast(
                        mActivity,
                        getString(R.string.please_enter_reason),
                        R.drawable.icon,
                        custom_toast_container
                    )
                } else {
                    webCallSendCheckInOutData(
                        0.0,
                        0.0,
                        AppConstant.CLOCK_OUT,
                        edtReason.text.toString(),
                        dialog,
                        "normal",
                        0
                    )
                }
            }
        }

        dialog.show()
    }

    private fun openCheckOutCashierDialog() {
        val dialog = Dialog(mActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setContentView(R.layout.dialog_cashier_force_checkout)

        dialog.setCancelable(false)
        dialog.show()

        val tvUserNameLogin = dialog.findViewById<TextView>(R.id.tvUserNameLogin)
        val ivCancel = dialog.findViewById<ImageView>(R.id.ivCancel)
        val edtReason = dialog.findViewById<EditText>(R.id.edtReason)

        val rbtYes = dialog.findViewById<RadioButton>(R.id.rbtYes)

        val tvStartDay = dialog.findViewById<TextView>(R.id.tvStartDay)
        val tvCashInDrawer = dialog.findViewById<TextView>(R.id.tvCashInDrawer)
        val tvTotalSalesCash = dialog.findViewById<TextView>(R.id.tvTotalSalesCash)
        val tvManualCashWithdrawel = dialog.findViewById<TextView>(R.id.tvManualCashWithdrawel)
        val tvBalanceInDrawer = dialog.findViewById<TextView>(R.id.tvBalanceInDrawer)
        val tvTotalSalesNumber = dialog.findViewById<TextView>(R.id.tvTotalSalesNumber)
        val tvSalesByCard = dialog.findViewById<TextView>(R.id.tvSalesByCard)
        val tvSalesByCash = dialog.findViewById<TextView>(R.id.tvSalesByCash)
        val tvTotalSalesCard = dialog.findViewById<TextView>(R.id.tvTotalSalesCard)
        val btnSubmit = dialog.findViewById<Button>(R.id.btnSubmit)
        val edtCashWithdraw = dialog.findViewById<EditText>(R.id.edtCashWithdraw)

        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.DISPLAY_DATE_FORMATTE_AMPM)

        tvUserNameLogin.setText(
            "Force Day Close " + "\n Cashier " + emp_name + " By " + store_admin_name + "\n" + sdf.format(
                date
            )
        )
        ivCancel.setOnClickListener {
            dialog.dismiss()
        }
        var edt_amount = 0.0
        var remaining = 0.0

        edtCashWithdraw.filters = arrayOf<InputFilter>(DecimalInputFilter())

        edtCashWithdraw.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                edt_amount = edtCashWithdraw.text.toString().toDouble()
                remaining = amount_check_out - edt_amount
                tvBalanceInDrawer.setText(remaining.toString())
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })

        if (!CM.isInternetAvailable(mActivity)) {
            //offline code
        } else {
            webCallGetCheckOutData(
                tvStartDay,
                tvCashInDrawer,
                tvTotalSalesCash,
                tvManualCashWithdrawel,
                tvBalanceInDrawer,
                tvTotalSalesNumber,
                tvSalesByCard,
                tvSalesByCash,
                tvTotalSalesCard
            )
        }

        btnSubmit.setOnClickListener {
            if (!CM.isInternetAvailable(mActivity)) {
                //offline code

            } else {
                if (edtReason.text.toString().equals("")) {
                    showToast(
                        mActivity,
                        getString(R.string.please_enter_reason),
                        R.drawable.icon,
                        custom_toast_container
                    )
                } else {
                    var handover = 0
                    if (rbtYes.isChecked) {
                        handover = 1
                    } else {
                        handover = 0
                    }
                    webCallSendCheckInOutData(
                        edtCashWithdraw.text.toString().toDouble(),
                        tvBalanceInDrawer.text.toString().toDouble(),
                        AppConstant.SHIFT_CLOSE,
                        edtReason.text.toString(),
                        dialog,
                        "cashier",
                        handover
                    )
                }

            }
        }

    }

    private fun webCallSendCheckInOutData(
        cashInDrawer: Double,
        totalCashInDrawer: Double,
        type: String,
        remark: String,
        dialog: Dialog,
        from: String,
        value: Int
    ) {

        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.WS_DATE_FORMATTE)
        showKcsDialog()

        var forcecheckout = 0
        if (from.equals("normal")) {
            forcecheckout = 1
        } else {
            forcecheckout = 0
        }

        checkInOutDataViewModel!!.checkInOutData(
            sdf.format(date),
            cashInDrawer,
            0.0,
            0.0,
            type,
            selected_force_checkout_row.iCheckEmployeeId,
            remark,
            totalCashInDrawer,
            store_admin_id,
            forcecheckout,
            value
        )
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                } else {
                    showToast(
                        mActivity,
                        loginResponseModel.data!!.message,
                        R.drawable.icon,
                        custom_toast_container
                    )

                    dialog.dismiss()
                    if (forceCheckoutList.size == 1) {
                        //     if (forceCheckoutList.get(0).strEmployeeRoleName.equals("Cashier")) {
                        CM.setSp(this, CV.ISCHECKIN, false)
                        CM.setSp(this, CV.CHECKINUSER, 0)
                        //  }
                    }

                    if (from.equals("normal")) {
                        CM.finishActivity(mActivity)
                    } else {
                        openCheckOutDialog()
                    }
                }

            })
    }

    private fun openCheckOutDialog() {
        val dialog = Dialog(mActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setContentView(R.layout.dialog_force_checkout)

        dialog.setCancelable(false)
        dialog.show()

        val btnNo = dialog.findViewById<Button>(R.id.btnNo)
        val btnYes = dialog.findViewById<Button>(R.id.btnYes)
        btnYes.setOnClickListener {
            webCallSendCheckInOutData(
                0.0,
                0.0,
                AppConstant.CLOCK_OUT,
                "",
                dialog,
                "normal",
                0
            )
        }
        btnNo.setOnClickListener {
            dialog.dismiss()
            CM.finishActivity(mActivity)

        }
    }

    private fun webCallGetCheckOutData(
        tvStartDay: TextView,
        tvCashInDrawer: TextView,
        tvTotalSalesCash: TextView,
        tvManualCashWithdrawel: TextView,
        tvBalanceInDrawer: TextView,
        tvTotalSalesNumber: TextView,
        tvSalesByCard: TextView,
        tvSalesByCash: TextView,
        tvTotalSalesCard: TextView
    ) {
        showKcsDialog()

        checkOutDataViewModel!!.getCheckOutData(selected_force_checkout_row.iCheckEmployeeId)
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                } else {
                    showToast(
                        mActivity,
                        loginResponseModel.data!!.message,
                        R.drawable.icon,
                        custom_toast_container
                    )

                    tvStartDay.setText(loginResponseModel.data!!.data.startDay)
                    tvCashInDrawer.setText(loginResponseModel.data!!.data.dblCashInDrawer)
                    tvTotalSalesCash.setText(loginResponseModel.data!!.data.salesCash)
                    tvManualCashWithdrawel.setText(loginResponseModel.data!!.data.manualCashWithdrawal)
                    tvBalanceInDrawer.setText(loginResponseModel.data!!.data.balanceCashInDrawer)
                    tvTotalSalesNumber.setText(loginResponseModel.data!!.data.totalSalesNumber)
                    tvSalesByCard.setText(loginResponseModel.data!!.data.totalSalesByCard)
                    tvSalesByCash.setText(loginResponseModel.data!!.data.totalSalesByCash)
                    tvTotalSalesCard.setText(loginResponseModel.data!!.data.totalSalesAmountByCard)
                    amount_check_out = tvBalanceInDrawer.text.toString().toDouble()

                }

            })
    }
}
