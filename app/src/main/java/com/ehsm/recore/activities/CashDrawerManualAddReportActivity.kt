package com.ehsm.recore.activities

import android.os.Bundle
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ehsm.recore.R
import com.ehsm.recore.adapter.CashDrawerManualAddReportAdapter
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.databinding.ActivityCashDrawerManualAddReportBinding
import com.ehsm.recore.model.CashDrawerManualReportModel
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CM.showToast
import com.ehsm.recore.utils.CV
import com.kcspl.divyangapp.viewmodel.CashDrawerManualReportViewModel
import kotlinx.android.synthetic.main.custom_toast.*
import kotlinx.android.synthetic.main.header_layout.view.*

class CashDrawerManualAddReportActivity : BaseActivity<ActivityCashDrawerManualAddReportBinding>() {

    private lateinit var mActivity: CashDrawerManualAddReportActivity
    private lateinit var mDatabase: RecoreDB
    private var currency_symbol = ""

    private var cashDrawerManualListReport = ArrayList<CashDrawerManualReportModel>()
    private var cashDrawerManualReportViewModel: CashDrawerManualReportViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        mDatabase = RecoreDB.getInstance(mActivity)!!
        cashDrawerManualReportViewModel = ViewModelProviders.of(this).get(CashDrawerManualReportViewModel::class.java)

        currency_symbol = CM.getSp(this, CV.CURRENCY_CODE, "") as String

        CM.removeStatusBar(mActivity)
        //bindView(R.layout.activity_cash_drawer_manual_add_report)
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_cash_drawer_manual_add_report,
            getWindow().getDecorView().getRootView() as ViewGroup?,false)
        setContentView(mBinding.root)
        mBinding.tvAmount.setText("Amount (" + currency_symbol + ")")

        setEmpView(mBinding.header.tvEmpName)
        setStoreNameView(mBinding.header.tvstorename, mBinding.header.tvposname)
        mBinding.header.ivLogo.setOnClickListener {
            CM.finishActivity(this)
            CM.startActivity(mActivity, LandingPageActivity::class.java)
        }

        mBinding.header.ivNewSale.setOnClickListener {
            CM.navigateToDashboard(this)
        }

        mBinding.header.ivNoSale.setOnClickListener {
            CM.openNosaleDialog(this,custom_toast_container)
        }

        mBinding.header.reluser.setOnClickListener {
            callChangeMpin()
        }
        mBinding.header.ivMore.setOnClickListener {
            CM.showPopup(mActivity, mBinding.header.ivMore, {
                syncWithServer()
            })
        }

        if (!CM.isInternetAvailable(mActivity)) {

            val stateListLiveData =
                mDatabase.daoCashDrawerManualReportList().getCashDrawerManualData()
            stateListLiveData.observe(this, androidx.lifecycle.Observer { statelist ->
                if (statelist != null) {
                    cashDrawerManualListReport = statelist as ArrayList<CashDrawerManualReportModel>
                    CM.setViewHeightBasedOnItems(
                        mBinding.listView,
                        CashDrawerManualAddReportAdapter(mActivity, cashDrawerManualListReport),
                        "list"
                    )

                    mBinding.listView.adapter =
                        CashDrawerManualAddReportAdapter(mActivity, cashDrawerManualListReport)

                }
            })
        } else {
            webCallgetCashDrawerReport("")
        }
    }

    override fun onRequestGranted(requestCode: Int, perms: List<String>) {
    }

    private fun webCallgetCashDrawerReport(date: String) {
        showKcsDialog()
        cashDrawerManualReportViewModel!!.getCashDrawerManualReportData(mDatabase, date)
            ?.observe(this, Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(mActivity, loginResponseModel.message!!,R.drawable.icon,custom_toast_container)
                } else {
                    showToast(mActivity,loginResponseModel.data!!.message,R.drawable.icon,custom_toast_container)

                    cashDrawerManualListReport = loginResponseModel.data!!.data
                    CM.setViewHeightBasedOnItems(
                        mBinding.listView,
                        CashDrawerManualAddReportAdapter(mActivity, cashDrawerManualListReport),
                        "list"
                    )

                    mBinding.listView.adapter =
                        CashDrawerManualAddReportAdapter(mActivity, cashDrawerManualListReport)

                }

            })

    }


}
