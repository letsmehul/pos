package com.ehsm.recore.activities

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.ehsm.recore.R
import com.ehsm.recore.Recore
import com.ehsm.recore.databinding.ActivitySettingsBinding
import com.ehsm.recore.interfaces.KitchenprinterListener
import com.ehsm.recore.network.BoltApiHelper
import com.ehsm.recore.network.BoltRestAPIClient
import com.ehsm.recore.repository.PaymentRepository
import com.ehsm.recore.utils.*
import com.ehsm.recore.utils.CM.showToast
import com.ehsm.recore.viewmodel.PaymentViewModel
import com.ehsm.recore.viewmodel.TerminalViewModel
import com.ehsm.recore.viewmodelfactory.viewModelFactory
import kotlinx.android.synthetic.main.custom_toast.*
import java.util.*

class SettingsActivity : BaseActivity<ActivitySettingsBinding>(),KitchenprinterListener {
    private lateinit var mActivity: SettingsActivity
    private lateinit var paymentViewModel: PaymentViewModel
    private var terminalViewModel: TerminalViewModel? = null
    val timer = Timer()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        CM.removeStatusBar(mActivity)
        //bindView(R.layout.activity_settings)
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_settings,
            getWindow().getDecorView().getRootView() as ViewGroup?,false)
        setContentView(mBinding.root)
        val boltApiHelper = BoltApiHelper(BoltRestAPIClient.getService()!!)
        paymentViewModel = ViewModelProviders.of(this,
            viewModelFactory {
                val paymentViewModel1 =
                    PaymentViewModel(
                        mActivity.application,
                        PaymentRepository(boltApiHelper),
                        mActivity
                    )
                paymentViewModel1
            }
        ).get(PaymentViewModel::class.java)
        terminalViewModel = ViewModelProviders.of(this).get(TerminalViewModel::class.java)

        mBinding.toolbar.title = getString(R.string.settings)
        setSupportActionBar(mBinding.toolbar)

        /*ePOSPrinterController.getKitchenPrinterList()
        Log.e("KITCHEN_PRINTER", "Kitchen Printer stared discover new list")*/


        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        /*timer.schedule(object : TimerTask() {
            override fun run() {
                ePOSPrinterController.getKitchenPrinterList()
            }
        }, 20000, 20000)*/


        mBinding.toolbar.setNavigationOnClickListener {
            finish()
        }

        mBinding.llparentTerminal.setOnClickListener {

            if (ClickValidate.isValid) {
                webCallTerminal()
            }


        }
        Recore.getApp()?.applicationContext?.let {
            val sharedPref = CM.getEncryptedSharedPreferences(it)
            mBinding.tvLabelSelectValue.text = sharedPref.getString(AppConstant.HSN, "")
        }

        Recore.getApp()?.applicationContext?.let {
            val sharedPref = CM.getEncryptedSharedPreferences(it)
            if (sharedPref.getString(AppConstant.KITCHEN_PRINTER_IP, "").isNullOrEmpty()) {
                mBinding.tvLabelKitchenPrinterSelectValue.text = "Printer is not connected."
            } else {
                mBinding.tvLabelKitchenPrinterSelectValue.text =
                    sharedPref.getString(AppConstant.KITCHEN_PRINTER_IP, "")
            }
            Log.e(
                "KITCHEN_PRINTER",
                "Kitchen Printer from data" + sharedPref.getString(
                    AppConstant.KITCHEN_PRINTER_IP,
                    ""
                )
            )
        }

        mBinding.ivKitchenPrinter.setOnClickListener {
            showKcsDialog()
            ePOSPrinterController.getKitchenPrinterList()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    dismissKcsDialog()
                    if (!ePOSPrinterController.kitchenPrinterdata.isNullOrEmpty()) {
                        showKitchenPrinterDialog(ePOSPrinterController.kitchenPrinterdata) {
                            it.let {
                                mBinding.tvLabelKitchenPrinterSelectValue.text = it
                                KITCHEN_PRINTER = it
                            }
                        }
                    }else{
                        runOnUiThread(Runnable {
                            KitchenPrinterNotFoundDialog()
                        })
                    }
                }
            }, 8000)
            /*if (ClickValidate.isValid) {
                if (!ePOSPrinterController.kitchenPrinterdata.isNullOrEmpty()) {
                    showKitchenPrinterDialog(ePOSPrinterController.kitchenPrinterdata) {
                        it.let {
                            mBinding.tvLabelKitchenPrinterSelectValue.text = it
                            KITCHEN_PRINTER = it
                        }
                    }
                }
            }*/
        }
        mBinding.tvLabelKitchenPrinterSelectValue.setOnClickListener {
            showKcsDialog()
            ePOSPrinterController.getKitchenPrinterList()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    dismissKcsDialog()
                    if (!ePOSPrinterController.kitchenPrinterdata.isNullOrEmpty()) {
                        showKitchenPrinterDialog(ePOSPrinterController.kitchenPrinterdata) {
                            it.let {
                                mBinding.tvLabelKitchenPrinterSelectValue.text = it
                                KITCHEN_PRINTER = it
                            }
                        }
                    }else{
                        runOnUiThread(Runnable {
                            KitchenPrinterNotFoundDialog()
                        })
                    }
                }
            }, 8000)
            /*if (ClickValidate.isValid) {
                if (!ePOSPrinterController.kitchenPrinterdata.isNullOrEmpty()) {
                    showKitchenPrinterDialog(ePOSPrinterController.kitchenPrinterdata) {
                        it.let {
                            mBinding.tvLabelKitchenPrinterSelectValue.text = it
                            KITCHEN_PRINTER = it
                        }
                    }
                }
            }*/
        }
        mBinding.ivKitchenPrinterRefresh.setOnClickListener{
            Log.d("refresh_data","refresh_data")
            showKcsDialog()
            ePOSPrinterController.getKitchenPrinterList()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    dismissKcsDialog()
                    if (!ePOSPrinterController.kitchenPrinterdata.isNullOrEmpty()) {
                        showKitchenPrinterDialog(ePOSPrinterController.kitchenPrinterdata) {
                            it.let {
                                mBinding.tvLabelKitchenPrinterSelectValue.text = it
                                KITCHEN_PRINTER = it
                            }
                        }
                    }else{
                        runOnUiThread(Runnable {
                            KitchenPrinterNotFoundDialog()
                        })
                    }
                }
            }, 8000)

        }
        mBinding.btncheckprint.setOnClickListener {
            Log.d("KITCHEN_PRINTER", "ffffff")
            //updatebuttonstatus(false)

            /*if (mBinding.btncheckprint.isEnabled==true) {
                timer.schedule(object : TimerTask() {
                    override fun run() {
                        Log.d("KITCHEN_PRINTER","true")
                        runOnUiThread {
                            mBinding.btncheckprint.isEnabled = true
                        }
                    }
                }, 6000)
                mBinding.btncheckprint.isEnabled=false
            }*/
            var status= ePOSPrinterController.AsyncTaskRunner().execute(mActivity.getString(R.string.check_message)+"\n"+CM.getSp(applicationContext, CV.POS_NAME, "") as String)
            Log.d("status_data",status.toString())
            /*if (ePOSPrinterController.CheckKitchenReceipt( mActivity.getString(R.string.check_message)+"\n"+CM.getSp(applicationContext, CV.POS_NAME, "") as String )
            ) {

                Log.e("KITCHEN_PRINTER", "Kitchen Printer Working Fine")

            } else {
                KitchenPrinterNotFoundDialog() {
                    updatebuttonstatus(false)
                    Log.e(
                        "KITCHEN_PRINTER",
                        "Kitchen Printer not Working and stared discover new list"
                    )
                }
            }*/
        }


    }
    fun updatebuttonstatus(status:Boolean)
    {
        mBinding.btncheckprint.isEnabled=status
        mBinding.llparentTerminal.isEnabled=status
        mBinding.tvLabelKitchenPrinterSelectValue.isEnabled=status
        mBinding.ivKitchenPrinter.isEnabled=status
        mBinding.ivKitchenPrinterRefresh.isEnabled=status
    }

    override fun onPause() {
        super.onPause()
        ePOSPrinterController.clearsettingcontext()
        //timer.cancel()

    }

    override fun onResume() {
        super.onResume()
        ePOSPrinterController.setsettingcontext(mActivity)
    }

    fun KitchenPrinterNotFoundDialog() {
        if (mActivity != null) {
            val dialog = Dialog(mActivity)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.window!!.setCallback(
                UserInteractionAwareCallback(
                    dialog.window!!.getCallback(),
                    mActivity
                )
            )

            dialog.setContentView(R.layout.dialog_kitchen_printer_notfound)

            dialog.setCancelable(false)
            val btnYes = dialog.findViewById<Button>(R.id.btnYes)
            val btnNo = dialog.findViewById<Button>(R.id.btnNo)
            btnYes.setOnClickListener {
                mBinding.tvLabelKitchenPrinterSelectValue.text = "Printer is not connected."
                dialog.dismiss()
            }

            dialog.show()
        }
    }

    override fun onRequestGranted(requestCode: Int, perms: List<String>) {
    }

    private fun webCallTerminal() {
        terminalViewModel!!.getTerminalList()
            ?.observe(this, androidx.lifecycle.Observer { terminalnResponseModel ->
                dismissKcsDialog()
                if (terminalnResponseModel.data == null) {
                    showToast(
                        mActivity,
                        terminalnResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                } else {
                    showTerminalDialog(terminalnResponseModel.data!!.data) {
                        it?.let {
                            mBinding.tvLabelSelectValue.text = it.hsn
                        }
                    }
                }
            })
    }


    object ClickValidate {
        var lastClickTime: Long = 0
        val isValid: Boolean
            get() {
                val current = System.currentTimeMillis()
                val def = current - lastClickTime
                return if (def > 2000) {
                    lastClickTime = System.currentTimeMillis()
                    true
                } else false
            }
    }

    override fun KitchenprintSuccess(status: Boolean?) {
        if (status == true)
        {

            Log.d("Settingactivity","df");
            //updatebuttonstatus(status)
        }
    }
}