package com.ehsm.recore.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.ehsm.recore.R
import com.ehsm.recore.custom.keyboard.DateKeyboard
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.databinding.ActivityPayInOutBinding
import com.ehsm.recore.model.PrinterPayInOutModel
import com.ehsm.recore.utils.*
import com.ehsm.recore.utils.CM.showToast
import com.ehsm.recore.utils.logger.LogUtil
import com.kcspl.divyangapp.viewmodel.*
import kotlinx.android.synthetic.main.custom_toast.*
import kotlinx.android.synthetic.main.header_layout.view.*
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*


class PayInOutActivity : BaseActivity<ActivityPayInOutBinding>() {

    private var checkInDataViewModel: CheckInDataViewModel? = null
    private var checkInOutDataViewModel: CheckInOutDataViewModel? = null
    private var checkOutDataViewModel: CheckOutDataViewModel? = null
    var amount_check_out = 0.0
    private var isCheckIn = false
    private var last_action = ""
    private var pos_name = ""
    private var date_time = ""
    var emp_id = 0
    var emp_name = ""
    var emp_role = ""
    private var lastActionViewModel: LastActionViewModel? = null
    private var lastStatusViewModel: LastStatusDayStartCloseViewModel? = null
    private lateinit var mDatabase: RecoreDB
    private lateinit var dialog: Dialog
    private var currency_symbol = ""
    private var isCheckIn_status = false
    private var last_action_status = ""
    private lateinit var mActivity: PayInOutActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        CM.removeStatusBar(this)
        mActivity = this
        //bindView(R.layout.activity_pay_in_out)
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_pay_in_out,
            getWindow().getDecorView().getRootView() as ViewGroup?,false)
        setContentView(mBinding.root)
        mDatabase = RecoreDB.getInstance(this)!!
        currency_symbol = CM.getSp(this, CV.CURRENCY_CODE, "") as String

        checkInDataViewModel = ViewModelProviders.of(this).get(CheckInDataViewModel::class.java)
        checkInOutDataViewModel =
            ViewModelProviders.of(this).get(CheckInOutDataViewModel::class.java)
        checkOutDataViewModel = ViewModelProviders.of(this).get(CheckOutDataViewModel::class.java)
        lastActionViewModel = ViewModelProviders.of(this).get(LastActionViewModel::class.java)
        lastStatusViewModel =
            ViewModelProviders.of(this).get(LastStatusDayStartCloseViewModel::class.java)

        emp_id = CM.getSp(this, CV.EMP_ID, 0) as Int
        emp_name = CM.getSp(this, CV.EMP_NAME, "") as String
        emp_role = CM.getSp(this, CV.EMP_ROLE, "") as String

        dialog = Dialog(this)

        setEmpView(mBinding.header.tvEmpName)
        setStoreNameView(mBinding.header.tvstorename, mBinding.header.tvposname)

        mBinding.tvWelComeMsg.text = "Welcome $emp_role $emp_name,"

        mBinding.btnPayIn.setOnClickListener {
            openPayInCashierDialog()
        }

        mBinding.btnPayout.setOnClickListener {
            openPayOutCashierDialog()
        }

        mBinding.header.ivLogo.setOnClickListener {
            CM.finishActivity(this)
        }
        mBinding.header.ivMore.setOnClickListener {
            CM.showPopup(mActivity, mBinding.header.ivMore) {
                syncWithServer()
            }
        }

        mBinding.header.ivNewSale.setOnClickListener {
            if (last_action == AppConstant.CLOCK_IN) {
                if (last_action_status == AppConstant.SHIFT_START) {
                    if (emp_role != AppConstant.STAFF) {
                        CM.navigateToDashboard(this)
                    } else {
                        openDenyTransactionDialog()
                    }
                } else {
                    openDenyDayStartDialog()
                }
            } else {
                openDenyTransactionDialog()
            }
        }
        mBinding.header.reluser.setOnClickListener {
            callChangeMpin()
        }
        mBinding.header.ivNoSale.setOnClickListener {
            if (last_action == AppConstant.CLOCK_IN) {
                CM.openNosaleDialog(this, custom_toast_container)
            } else {
                openDenyTransactionDialog()
            }
        }

        ScreenLockTimer.registerLockListener(mBinding.header.ivLock,mActivity)
    }

    override fun onResume() {
        super.onResume()

//        mBinding.header.text_timer.text = ""

        when (emp_role) {
            AppConstant.STAFF -> {
                mBinding.header.ivNoSale.visibility = View.GONE
                mBinding.header.ivNewSale.visibility = View.GONE
            }
            AppConstant.CASHIER -> {
                mBinding.header.ivNoSale.visibility = View.GONE
                mBinding.header.ivNewSale.visibility = View.VISIBLE
            }
            AppConstant.STORE_ADMIN -> {
                mBinding.header.ivNoSale.visibility = View.VISIBLE
                mBinding.header.ivNewSale.visibility = View.VISIBLE
            }
        }

        if (!CM.isInternetAvailable(this)) {
            CM.showMessageOK(
                this,
                "",
                resources.getString(R.string.msg_network_error),
                null
            )
        } else {
            webCallGetLastAction()
        }
    }
    override fun onStart() {
        super.onStart()
        UserInteractionAwareCallback(window!!.callback,mActivity)
    }

    private fun webCallGetLastAction() {
        showKcsDialog()

        lastActionViewModel!!.getLastAction(emp_id)
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                    isCheckIn = true
                } else {
                    /*  Toast.makeText(this, loginResponseModel.data!!.message, Toast.LENGTH_LONG)
                          .show()*/
                    pos_name = loginResponseModel.data!!.data.posName
                    date_time = loginResponseModel.data!!.data.dateTimeCreated
                    if (loginResponseModel.data!!.data.strTnxtype.equals(AppConstant.CLOCK_IN)) {
                        isCheckIn = false
                    } else {
                        isCheckIn = true
                    }

                    if (isCheckIn) {
                        last_action = AppConstant.CLOCK_OUT
                    } else {
                        last_action = AppConstant.CLOCK_IN
                    }

                    if (isCheckIn) {
                        //allow check in
                        mBinding.btnPayIn.isEnabled = false
                        mBinding.btnPayIn.background = resources.getDrawable(R.drawable.btn_bg_gray)
                        mBinding.btnPayout.isEnabled = false
                        mBinding.btnPayout.background =
                            resources.getDrawable(R.drawable.btn_bg_gray)
                        mBinding.tvNotPerformPayInOut.visibility = View.VISIBLE
                    } else {
                        mBinding.btnPayIn.isEnabled = true
                        mBinding.btnPayIn.background = resources.getDrawable(R.drawable.btn_bg)
                        mBinding.btnPayout.isEnabled = true
                        mBinding.btnPayout.background = resources.getDrawable(R.drawable.btn_bg)
                        mBinding.tvNotPerformPayInOut.visibility = View.GONE
                    }
                    mBinding.tvMachineDateTimeValue.setText(
                        last_action + ", from " + pos_name + " on " + CM.convertDateFormate(
                            CV.WS_DATE_FORMATTE_AM_PM,
                            CV.DISPLAY_DATE_FORMATTE_AMPM, date_time
                        )
                    )

                    webCallGetLastStatus()
                }
            })
    }

    private fun webCallGetLastStatus() {
        showKcsDialog()

        lastStatusViewModel!!.getLastActionDayStartClose()
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                    isCheckIn_status = true
                } else {
                    isCheckIn_status =
                        !loginResponseModel.data!!.data.strTnxtype.equals(AppConstant.SHIFT_START)
                }

                if (isCheckIn_status) {
                    NewSaleFragmentCommon.resetShiftDetails(this)
                    last_action_status = AppConstant.SHIFT_CLOSE
                } else {
                    NewSaleFragmentCommon.setShiftDetails(this, loginResponseModel)
                    last_action_status = AppConstant.SHIFT_START
                }
            })
    }

    private fun openPayInCashierDialog() {

        val currency_symbol =
            CM.getSp(this@PayInOutActivity, CV.CURRENCY_CODE, "") as String

        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.setCallback(
                UserInteractionAwareCallback(
                        dialog.window!!.getCallback(),
                        mActivity
                )
        )
        dialog.setContentView(R.layout.dialog_cashier_payin)

        dialog.setCancelable(false)
        dialog.show()


        val ivCancel = dialog.findViewById<ImageView>(R.id.ivCancel)
        val tvCashInDrawer = dialog.findViewById<TextView>(R.id.tvCashInDrawer)

        val btnSubmit = dialog.findViewById<Button>(R.id.btnSubmit)
        btnSubmit.setText(getString(R.string.confirm))



        ivCancel.setOnClickListener {
            dialog.dismiss()
        }

        val tvTotalCashInDrawer = dialog.findViewById<TextView>(R.id.tvTotalCashInDrawer)
        tvTotalCashInDrawer.setText("$currency_symbol 0.00")

        val edtCashInDrawer = dialog.findViewById<EditText>(R.id.edtCashInDrawer)

        val keyboard: DateKeyboard = dialog.findViewById(R.id.keyboard) as DateKeyboard
        edtCashInDrawer.setRawInputType(InputType.TYPE_CLASS_TEXT)
        edtCashInDrawer.setTextIsSelectable(true)

        val ic = edtCashInDrawer.onCreateInputConnection(EditorInfo())
        keyboard.setInputConnection(ic)

        edtCashInDrawer.requestFocus()
        edtCashInDrawer.isFocusable = true
        edtCashInDrawer.addTextChangedListener(GenericTextWatcher(edtCashInDrawer))
        val edtReason = dialog.findViewById<EditText>(R.id.edtReason)

        edtReason.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                CM.onKeyboardClose(edtReason, mActivity)
            }
        }
        edtReason.addTextChangedListener {
            ScreenLockTimer.stopHandler()
            ScreenLockTimer.startHandler(mActivity)
            LogUtil.e("edtreason","fgfggf")
        }

        if (!CM.isInternetAvailable(this)) {
            CM.showMessageOK(
                this,
                "",
                resources.getString(R.string.msg_network_error),
                null
            )
        } else {
            if (last_action_status.equals(AppConstant.SHIFT_START)) {
                webCallGetPayOutData(tvCashInDrawer)
            } else {
                webCallGetPayInData(tvCashInDrawer)
            }
        }

        var edt_amount = 00.00
        var total = 0.0
        edtCashInDrawer.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                if (s.toString().length > 0) {
                    try {
                        edt_amount = edtCashInDrawer.text.toString().toDouble()
                        total =
                            tvCashInDrawer.text.toString().split(" ").get(1).toDouble() + edt_amount

                        tvTotalCashInDrawer.setText(
                            "$currency_symbol " + String.format(
                                "%.2f",
                                (total)
                            )
                        )

                    } catch (e: NumberFormatException) {
                        e.printStackTrace()
                        edtCashInDrawer.setText("0.0")
                        showToast(
                            mActivity,
                            getString(R.string.msg_enter_proper_amount),
                            R.drawable.icon,
                            custom_toast_container
                        )
                    }

                }

            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })

        btnSubmit.setOnClickListener {
            if (TextUtils.isEmpty(edtCashInDrawer.text.toString())) {
                showToast(
                    mActivity,
                    getString(R.string.enter_amount),
                    R.drawable.icon,
                    custom_toast_container
                )
            } else if (edtCashInDrawer.text.toString() == "0.") {
                showToast(
                    mActivity,
                    getString(R.string.enter_valid_amount),
                    R.drawable.icon,
                    custom_toast_container
                )
            } else if (edtCashInDrawer.text.toString().toDouble() <= 0) {
                showToast(
                    mActivity,
                    getString(R.string.enter_valid_amount),
                    R.drawable.icon,
                    custom_toast_container
                )
            } else if (edtCashInDrawer.text.toString().take(0).equals(".")) {
                showToast(
                    mActivity,
                    getString(R.string.enter_valid_amount),
                    R.drawable.icon,
                    custom_toast_container
                )
            } else if (TextUtils.isEmpty(edtReason.text.toString())) {
                showToast(
                    mActivity,
                    getString(R.string.please_enter_reason),
                    R.drawable.icon,
                    custom_toast_container
                )
            } else {
                if (!CM.isInternetAvailable(this)) {
                    CM.showMessageOK(
                        this,
                        "",
                        resources.getString(R.string.msg_network_error),
                        null
                    )
                } else {
                    try {
                        webCallSendPayInOutData(
                            edtCashInDrawer.text.toString().toDouble(),
                            tvTotalCashInDrawer.text.toString().split(" ").get(1).toDouble(),
                            "PayIn",
                            edtReason.text.toString(),
                            dialog
                        )
                    } catch (e: NumberFormatException) {
                        showToast(
                            mActivity,
                            getString(R.string.msg_enter_proper_amount),
                            R.drawable.icon,
                            custom_toast_container
                        )
                    }
                }
            }
        }

    }

    private fun openPayOutCashierDialog() {
        val currency_symbol =
            CM.getSp(this@PayInOutActivity, CV.CURRENCY_CODE, "") as String
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.setCallback(
                UserInteractionAwareCallback(
                        dialog.window!!.getCallback(),
                        mActivity
                )
        )
        dialog.setContentView(R.layout.dialog_cashier_payout)

        dialog.setCancelable(false)
        dialog.show()

        val ivCancel = dialog.findViewById<ImageView>(R.id.ivCancel)

        val tvCashInDrawer = dialog.findViewById<TextView>(R.id.tvCashInDrawer)
        val tvBalanceInDrawer = dialog.findViewById<TextView>(R.id.tvBalanceInDrawer)
        val btnSubmit = dialog.findViewById<Button>(R.id.btnSubmit)
        btnSubmit.setText(getString(R.string.confirm))
        val edtCashWithdraw = dialog.findViewById<EditText>(R.id.edtCashWithdraw)
        val edtReason = dialog.findViewById<EditText>(R.id.edtReason)

        val keyboard: DateKeyboard = dialog.findViewById(R.id.keyboard) as DateKeyboard
        edtCashWithdraw.setRawInputType(InputType.TYPE_CLASS_TEXT)
        edtCashWithdraw.setTextIsSelectable(true)

        val inputConnection = edtCashWithdraw.onCreateInputConnection(EditorInfo())
        keyboard.setInputConnection(inputConnection)

        tvBalanceInDrawer.text = "$currency_symbol 0.00"

        edtCashWithdraw.requestFocus()
        edtCashWithdraw.isFocusable = true


        edtReason.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                CM.onKeyboardClose(edtReason, mActivity)
            }
        }
        edtReason.addTextChangedListener {
            ScreenLockTimer.stopHandler()
            ScreenLockTimer.startHandler(mActivity)
            LogUtil.e("edtreason","fgfggf")
        }

        ivCancel.setOnClickListener {
            dialog.dismiss()
        }
        var edt_amount = 0.0
        var remaining = 0.0


//        edtCashWithdraw.addTextChangedListener(GenericTextWatcher(edtCashWithdraw))
        edtCashWithdraw.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {


                try {
                    if(!TextUtils.isEmpty(s.toString())){
                        val inputString = s.toString()
                        edtCashWithdraw.removeTextChangedListener(this)
                        val cleanString = inputString.replace("[.]".toRegex(), "")
                        val bigDecimal: BigDecimal = BigDecimal(cleanString).setScale(2, BigDecimal.ROUND_FLOOR).divide(
                            BigDecimal(100), BigDecimal.ROUND_FLOOR)
                        val converted: String = bigDecimal.toString()
                        edtCashWithdraw.setText(converted)
                        edtCashWithdraw.setSelection(converted.length)
                        edtCashWithdraw.addTextChangedListener(this)
                    }

                }catch (e : Exception){
                    e.printStackTrace()
                }

                if (!edtCashWithdraw.text.toString().equals("")) {
                    try {
                        edt_amount = edtCashWithdraw.text.toString().toDouble()
                        remaining = amount_check_out - edt_amount
                        if (remaining >= 0) {
                            tvBalanceInDrawer.setText(
                                "$currency_symbol " + String.format(
                                    "%.2f",
                                    (remaining)
                                )
                            )
                        } else {
                            showToast(mActivity, "You can't withdraw more than "+"$currency_symbol " + String.format(
                                "%.2f",
                                (amount_check_out)
                            ), R.drawable.icon,custom_toast_container)

                            keyboard.allClear()

                        }
                    } catch (e: NumberFormatException) {
                        edtCashWithdraw.setText("0.0")
                        showToast(
                            mActivity,
                            getString(R.string.msg_enter_proper_amount),
                            R.drawable.icon,
                            custom_toast_container
                        )
                    }
                } else {
                    try {
                        edt_amount = 0.0
                        remaining = amount_check_out - edt_amount
                        val currency_symbol =
                            CM.getSp(this@PayInOutActivity, CV.CURRENCY_CODE, "") as String

                        tvBalanceInDrawer.setText(
                            "$currency_symbol " + String.format(
                                "%.2f",
                                (remaining)
                            )
                        )
                    } catch (e: NumberFormatException) {
                        edtCashWithdraw.setText("0.0")
                        showToast(
                            mActivity,
                            getString(R.string.msg_enter_proper_amount),
                            R.drawable.icon,
                            custom_toast_container
                        )
                    }
                }

            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })

        if (!CM.isInternetAvailable(this)) {
            CM.showMessageOK(
                this,
                "",
                resources.getString(R.string.msg_network_error),
                null
            )
        } else {
            if (last_action_status.equals(AppConstant.SHIFT_START)) {
                webCallGetPayOutData(tvCashInDrawer)
            } else {
                webCallGetPayInData(tvCashInDrawer)

            }
        }

        btnSubmit.setOnClickListener {
            if (TextUtils.isEmpty(edtCashWithdraw.text.toString())) {
                showToast(
                    mActivity,
                    getString(R.string.enter_amount),
                    R.drawable.icon,
                    custom_toast_container
                )
            } else if (edtCashWithdraw.text.toString() == "0.") {
                showToast(
                    mActivity,
                    getString(R.string.enter_valid_amount),
                    R.drawable.icon,
                    custom_toast_container
                )
            } else if (edtCashWithdraw.text.toString().toDouble() <= 0) {
                showToast(
                    mActivity,
                    getString(R.string.enter_valid_amount),
                    R.drawable.icon,
                    custom_toast_container
                )
            } else if (TextUtils.isEmpty(edtReason.text.toString())) {
                showToast(
                    mActivity,
                    getString(R.string.please_enter_reason),
                    R.drawable.icon,
                    custom_toast_container
                )
            } else {
                if (!CM.isInternetAvailable(this)) {
                    //offline code
                } else {
                    try {
                        webCallSendPayInOutData(
                            edtCashWithdraw.text.toString().toDouble(),
                            tvBalanceInDrawer.text.toString().split(" ").get(1).toDouble(),
                            "PayOut",
                            edtReason.text.toString(),
                            dialog
                        )
                    } catch (e: NumberFormatException) {
                        showToast(
                            mActivity,
                            getString(R.string.msg_enter_proper_amount),
                            R.drawable.icon,
                            custom_toast_container
                        )
                    }
                }
            }
        }

    }

    @SuppressLint("SimpleDateFormat")
    private fun webCallSendPayInOutData(
        cashInDrawer: Double,
        totalCashInDrawer: Double,
        type: String,
        remark: String,
        dialog: Dialog
    ) {

        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.WS_DATE_FORMATTE)
        showKcsDialog()

        checkInOutDataViewModel!!.checkInOutData(
            sdf.format(date),
            cashInDrawer,
            0.0,
            0.0,
            type,
            emp_id,
            remark,
            totalCashInDrawer,
            emp_id,
            0, 0
        )
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                } else {
                    showToast(
                        mActivity,
                        loginResponseModel.data!!.message,
                        R.drawable.icon,
                        custom_toast_container
                    )
                    CM.setSp(mActivity, CV.CASH_IN_DRAWER, totalCashInDrawer.toString())

                    val sdfd = SimpleDateFormat(CV.DISPLAY_DATE_FORMATTE_AMPM)
                    if (type.equals("PayIn")) {

                        PrintPayInOutReceipt(
                            remark,
                            cashInDrawer.toString(),
                            sdfd.format(date),
                            "PayIn"
                        )

                    } else if (type.equals("PayOut")) {

                        PrintPayInOutReceipt(
                            remark,
                            cashInDrawer.toString(),
                            sdfd.format(date),
                            "PayOut"
                        )
                    }

                    openCashBox()
                    dialog.dismiss()
                    CM.finishActivity(this)
                }
            })
    }

    private fun PrintPayInOutReceipt(
        mReason: String,
        mCashInDrawer: String,
        mDateTime: String,
        mReceiptTitle: String
    ) {

        var POSName = CM.getSp(this, CV.POS_NAME, "") as String

        val mPrinterPayInOutModel = PrinterPayInOutModel()


        mPrinterPayInOutModel.setReason(mReason)
        mPrinterPayInOutModel.setPayInOutAmount(currency_symbol + " " + mCashInDrawer)

        mPrinterPayInOutModel.setDayStartDate(mDateTime)
        mPrinterPayInOutModel.setCashierName(emp_name)
        mPrinterPayInOutModel.setPOSStationNo(POSName)
        mPrinterPayInOutModel.setEmpRole(emp_role)

        mPrinterController.printPayInTicket(mPrinterPayInOutModel, mReceiptTitle)
    }


    private fun webCallGetPayInData(tvCashInDrawer: TextView) {
        showKcsDialog()

        checkInDataViewModel!!.getCheckInData(emp_id)
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                } else {

                    val currency_symbol = CM.getSp(this, CV.CURRENCY_CODE, "") as String
                    tvCashInDrawer.text =
                        currency_symbol + " " + loginResponseModel.data!!.data.dblCashInDrawer
                    CM.setSp(
                        mActivity,
                        CV.CASH_IN_DRAWER,
                        loginResponseModel.data!!.data.dblCashInDrawer
                    )

                    amount_check_out =
                        loginResponseModel.data!!.data.dblCashInDrawer.toDouble()
                }
            })

    }

    private fun webCallGetPayOutData(tvCashInDrawer: TextView) {
        showKcsDialog()

        checkOutDataViewModel!!.getCheckOutData(emp_id)
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                } else {
                    val currency_symbol = CM.getSp(this, CV.CURRENCY_CODE, "") as String
                    val cashInDrawer = loginResponseModel.data!!.data.balanceCashInDrawer.toDouble()
                    tvCashInDrawer.text =
                        currency_symbol + " " + String.format("%.2f", cashInDrawer)
                    amount_check_out =
                        loginResponseModel.data!!.data.balanceCashInDrawer.toDouble()
                    CM.setSp(
                        mActivity,
                        CV.CASH_IN_DRAWER,
                        loginResponseModel.data!!.data.balanceCashInDrawer
                    )

                }

            })
    }

    override fun onRequestGranted(requestCode: Int, perms: List<String>) {

    }

    override fun onDestroy() {
        super.onDestroy()
        dialog.dismiss()
    }
}
