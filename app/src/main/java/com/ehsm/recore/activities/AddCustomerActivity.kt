package com.ehsm.recore.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ehsm.recore.R
import com.ehsm.recore.adapter.CityListAdapter
import com.ehsm.recore.adapter.CustomerListAdapter
import com.ehsm.recore.adapter.StateListAdapter
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.databinding.ActivityAddCustomerBinding
import com.ehsm.recore.model.CityModel
import com.ehsm.recore.model.CustomerModel
import com.ehsm.recore.model.StateModel
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CM.showToast
import com.ehsm.recore.utils.CV
import com.ehsm.recore.utils.markRequiredInRed
import com.ehsm.recore.viewmodel.AddCustomerViewModel
import com.google.gson.Gson
import com.kcspl.divyangapp.viewmodel.CityListViewModel
import com.kcspl.divyangapp.viewmodel.CustomerListViewModel
import com.kcspl.divyangapp.viewmodel.StateListViewModel
import kotlinx.android.synthetic.main.activity_add_customer.*
import kotlinx.android.synthetic.main.custom_toast.*
import java.util.*
import kotlin.collections.ArrayList


class AddCustomerActivity : BaseActivity<ActivityAddCustomerBinding>(),
    CustomerListAdapter.ListItemClickedCustomer {

    private var lastChar = ""
    private lateinit var mActivity: AddCustomerActivity
    private var addCustomerViewModel: AddCustomerViewModel? = null
    private lateinit var mDatabase: RecoreDB
    private var customerList = ArrayList<CustomerModel>()
    private var customerListFilter = ArrayList<CustomerModel>()
    private var customerListViewModel: CustomerListViewModel? = null

    var filterProduct: ArrayList<CustomerModel> = ArrayList<CustomerModel>()

    private var stateList = ArrayList<StateModel>()
    private var stateListViewModel: StateListViewModel? = null

    private var cityList = ArrayList<CityModel>()
    private var cityListViewModel: CityListViewModel? = null

    private lateinit var selected_state: StateModel
    private lateinit var selected_city: CityModel

    private var selectedCustomerModel: CustomerModel? = null

    private var isUpdated = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        mDatabase = RecoreDB.getInstance(mActivity)!!

        addCustomerViewModel = ViewModelProviders.of(this).get(AddCustomerViewModel::class.java)
        customerListViewModel = ViewModelProviders.of(this).get(CustomerListViewModel::class.java)
        stateListViewModel = ViewModelProviders.of(this).get(StateListViewModel::class.java)
        cityListViewModel = ViewModelProviders.of(this).get(CityListViewModel::class.java)


        CM.removeStatusBar(mActivity)


        //bindView(R.layout.activity_add_customer)
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_add_customer,
            getWindow().getDecorView().getRootView() as ViewGroup?,false)
        setContentView(mBinding.root)

        textInputLayoutFirstName.markRequiredInRed()
       // mBinding.edtFirstName.setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_WORDS)

       // textInputLayoutLastName.markRequiredInRed()
        //mBinding.edtLastName.setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_WORDS)

        //textInputLayoutPhoneNumber.markRequiredInRed()

        //get state list
        if (!CM.isInternetAvailable(mActivity)) {
            val stateListLiveData = mDatabase.daoStateList().getStates()

            stateListLiveData.observe(this, androidx.lifecycle.Observer { statelist ->
                if (statelist != null) {
                    stateList = statelist as ArrayList<StateModel>
                    Log.e("====", "=== stateList =====" + stateList.size)
                    mBinding.spnState.adapter = StateListAdapter(mActivity, stateList)

                }
            })

        } else {
            webCallStateList()
        }

        //get customer list
        if (!CM.isInternetAvailable(mActivity)) {
            val customerListLiveData = mDatabase.daoCustomerList().getCustomers()
            customerListLiveData.observe(this, androidx.lifecycle.Observer { customerlist ->
                if (customerlist != null) {
                    customerList = customerlist as ArrayList<CustomerModel>
                    customerListFilter = customerlist
                    Log.e("====", "=== customerList =====" + customerList.size)

                    mBinding.rvCustomer.adapter = CustomerListAdapter(mActivity, customerList, this)
                }
            })

        } else {
            webCallCustomerList("")
        }

        mBinding.ivCancelCustomer.setOnClickListener {
            CM.finishActivity(mActivity)
        }

        mBinding.ivAddCustomer.setOnClickListener {
            if (mBinding.edtFirstName.text.toString().trim().equals("")) {
               showToast(mActivity,"Please Enter First Name",R.drawable.icon,custom_toast_container)
            } /*else if (mBinding.edtLastName.text.toString().trim().equals("")) {
                showToast(mActivity,"Please Enter Last Name",R.drawable.icon,custom_toast_container)
            } else if (mBinding.edtPhone.text.toString().trim().equals("")) {
                showToast(mActivity,"Please Enter Phone Number",R.drawable.icon,custom_toast_container)
            }*/ else {
                if (!mBinding.edtEmail.text.toString().equals("")) {
                    if (!mBinding.edtEmail.text.toString().isEmailValid()) {
                        showToast(mActivity,"Please Enter Valid Email",R.drawable.icon,custom_toast_container)
                    } else {
                        if (!CM.isInternetAvailable(mActivity)) {
                            CM.showMessageOK(
                                mActivity,
                                "",
                                resources.getString(R.string.msg_network_error),
                                null
                            )
                        } else {
                            webCallAddCustomer()
                        }
                    }
                } else {
                    if (!CM.isInternetAvailable(mActivity)) {
                        CM.showMessageOK(
                            mActivity,
                            "",

                            resources.getString(R.string.msg_network_error),
                            null
                        )
                    } else {
                        webCallAddCustomer()
                    }
                }
            }


        }

        mBinding.edtSearchCustomer.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                //    if (s.isNotEmpty()) filter(s.toString())
                //  filter(s.toString())
                if (s.isEmpty()) {
                    filterProduct.clear()
                    mBinding.rvCustomer.adapter = CustomerListAdapter(mActivity, customerListFilter, this@AddCustomerActivity)
                } else {
                    filter(s.toString())
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })

        mBinding.spnState.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                selected_state = stateList.get(position)
                if (!CM.isInternetAvailable(mActivity)) {
                    val cityListLiveData =
                        mDatabase.daoCityList().getCity(selected_state.iStateID)
                    cityListLiveData.observe(
                        this@AddCustomerActivity,
                        androidx.lifecycle.Observer { citylist ->
                            if (citylist != null) {
                                cityList = citylist as ArrayList<CityModel>
                                Log.e("====", "=== cityList =====" + cityList.size)
                                mBinding.spnCity.adapter = CityListAdapter(mActivity, cityList)
                            }
                        })
                } else {
                    webCallGetCity(selected_state.iStateID)
                }
            }
        }

        mBinding.spnCity.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                selected_city = cityList.get(position)
            }
        }

        mBinding.edtDob.setOnClickListener {
            showCalendar()
        }

//        mBinding.ivNewForm.setOnClickListener {
//            mActivity.finish()
//            CM.startActivity(mActivity, AddCustomerActivity::class.java)
//        }
    }

    fun String.isEmailValid(): Boolean {
        return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
    }

    private fun showCalendar() {
        var year = 0
        var month = 0
        var day = 0
        if (!mBinding.edtDob.text.toString().isEmpty()) {
            var dob = mBinding.edtDob.text.toString().split("-")
            //  val c = Calendar.getInstance()
            year = dob[2].toInt()
            month = dob[0].toInt() - 1
            day = dob[1].toInt()
        } else {
            val c = Calendar.getInstance()
            year = c.get(Calendar.YEAR)
            month = c.get(Calendar.MONTH)
            day = c.get(Calendar.DAY_OF_MONTH)
        }
        val dpd =
            DatePickerDialog(
                mActivity,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    //  mBinding.edtDob.setText(year.toString() + "-" + (if ((monthOfYear + 1) < 10) "0" + (monthOfYear + 1) else (monthOfYear + 1)) + "-" + (if (dayOfMonth < 10) "0" + dayOfMonth else dayOfMonth))
                    mBinding.edtDob.setText("" + ((if ((monthOfYear + 1) < 10) "0" + (monthOfYear + 1) else (monthOfYear + 1))) + "-" + (if (dayOfMonth < 10) "0" + dayOfMonth else dayOfMonth) + "-" + year.toString())

                },
                year,
                month,
                day
            )
        dpd.getDatePicker().setMaxDate(System.currentTimeMillis());

        dpd.show()
    }

    private fun webCallGetCity(state_id: Int) {
        cityListViewModel!!.getCity(mDatabase, state_id)
            ?.observe(this, Observer { loginResponseModel ->
                //                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(mActivity,loginResponseModel.message!!,R.drawable.icon,custom_toast_container)
                } else {
                    cityList = loginResponseModel.data!!.data
                    Log.e("====", "=== cityList ###=====" + cityList.size)
                    mBinding.spnCity.adapter = CityListAdapter(mActivity, cityList)

                    if (selectedCustomerModel != null) {
                        var pos_city = 0
                        for (i in cityList.indices) {
                            if (cityList[i].iCityID == selectedCustomerModel!!.iCityId) {
                                pos_city = i
                                break
                            }
                        }
                        mBinding.spnCity.setSelection(pos_city)
                    }
                }
            })
    }

    private fun webCallStateList() {
        // showKcsDialog()
        stateListViewModel!!.getStates(mDatabase)
            ?.observe(this, Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(mActivity,loginResponseModel.message!!,R.drawable.icon,custom_toast_container)
                } else {
                    stateList = loginResponseModel.data!!.data
                    Log.e("====", "=== stateList ###=====" + stateList.size)
                    mBinding.spnState.adapter = StateListAdapter(mActivity, stateList)
                }
            })
    }

    private fun webCallCustomerList(search: String) {
        showKcsDialog()
        customerListViewModel!!.getCustomer(search)
            ?.observe(this, Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(mActivity,loginResponseModel.message!!,R.drawable.icon,custom_toast_container)
                } else {
                    customerList = loginResponseModel.data!!.data
                    customerListFilter = loginResponseModel.data!!.data
                    Log.e("====", "=== customerList ####=====" + customerList.size)
                    mBinding.rvCustomer.adapter = CustomerListAdapter(mActivity, customerList, this)
                }
            })
    }

    private fun webCallAddCustomer() {
        var gender: String
        var status: String
        if (mBinding.rbtFemale.isChecked) {
            gender = "female"
        } else {
            gender = "male"
        }

        if (mBinding.rbtActive.isChecked) {
            status = "Active"
        } else {
            status = "Inactive"
        }
        showKcsDialog()

        var cust_id = ""
        if (selectedCustomerModel != null) {
            cust_id = selectedCustomerModel!!.iCustomerId.toString()
        }
        val dob = CM.convertDateFormate(
            CV.DISPLAY_DATE_FORMATTE,
            CV.WS_DATE_FORMATTE,
            edtDob.text.toString()
        )
        Log.e("===", "=== dob ===" + dob)
        addCustomerViewModel!!.addCustomer(
            mBinding.edtFirstName.text.toString().trim(),
            mBinding.edtLastName.text.toString().trim(),
            mBinding.edtEmail.text.toString().trim(),
            mBinding.edtPhone.text.toString().trim(),
            mBinding.edtRemarks.text.toString().trim(),
            gender,
            status,
            mBinding.edtAddressLine1.text.toString().trim(),
            mBinding.edtAddressLine2.text.toString().trim(),
            selected_city.iCityID,
            selected_state.iStateID,
            mBinding.edtZipcode.text.toString().trim(),
            dob,
            cust_id
        )
            ?.observe(this, Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(mActivity,loginResponseModel.message!!,R.drawable.icon,custom_toast_container)
                } else {
                    var customer_id = 0
                    if (!cust_id.equals("")) {
                        customer_id = cust_id.toInt()
                    } else {
                        customer_id = loginResponseModel.data!!.data.customer_id.toInt()
                    }
                    // CM.finishActivity(mActivity)
                    val data = Intent()
                    data.putExtra(
                        "customer",
                        Gson().toJson(
                            CustomerModel(
                                customer_id,
                                mBinding.edtFirstName.text.toString().trim(),
                                mBinding.edtLastName.text.toString().trim(),
                                mBinding.edtPhone.text.toString().trim(),
                                mBinding.edtEmail.text.toString().trim(),
                                mBinding.edtRemarks.text.toString().trim(),
                                gender,
                                status,
                                mBinding.edtAddressLine1.text.toString().trim(),
                                mBinding.edtAddressLine2.text.toString().trim(),
                                selected_city.iCityID,
                                selected_state.iStateID,
                                mBinding.edtZipcode.text.toString().trim(),
                                dob,
                                0
                            )
                        )
                    )
                    data.putExtra("isUpdated",isUpdated)
                    setResult(Activity.RESULT_OK, data)
                    finish()
                    overridePendingTransition(0, R.anim.push_out_to_left)
                }
            })
    }

    override fun onRequestGranted(requestCode: Int, perms: List<String>) {
    }


    fun filter(text: String) {
        filterProduct.clear()
        //looping through existing elements
        for (s in customerListFilter) {
            //if the existing elements contains the search input
            if (s.strFirstName.toLowerCase().contains(text.toLowerCase())
                || s.strLastName.toLowerCase().contains(text.toLowerCase())
                || s.strPhone.toLowerCase().contains(text.toLowerCase())
            ) {
                //adding the element to filtered list
                filterProduct.add(s)
            }
        }
        mBinding.rvCustomer.adapter = CustomerListAdapter(mActivity, filterProduct, this)
    }

    override fun listItemClickedCustomer(position: Int, category_id: Int) {
        CM.hideSoftKeyboard(mActivity)
        ivAddCustomer.setText("Update")
        isUpdated = true
        //selectedCustomerModel = customerList.get(position)
        if (filterProduct.size > 0) {
            selectedCustomerModel = filterProduct.get(position)
        } else {
            selectedCustomerModel = customerListFilter.get(position)
        }

        mBinding.edtFirstName.setText(selectedCustomerModel!!.strFirstName)
        mBinding.edtLastName.setText(selectedCustomerModel!!.strLastName)
        mBinding.edtPhone.setText(selectedCustomerModel!!.strPhone)
        mBinding.edtEmail.setText(selectedCustomerModel!!.strEmail)
        if (selectedCustomerModel!!.strGender.equals("male")) {
            mBinding.rbtMale.isChecked = true
        } else {
            mBinding.rbtFemale.isChecked = true
        }

        if(!selectedCustomerModel!!.dateDob.equals("")){
            mBinding.edtDob.setText(
                CM.convertDateFormate(
                    CV.WS_DATE_FORMATTE,
                    CV.DISPLAY_DATE_FORMATTE,
                    selectedCustomerModel!!.dateDob
                )
            )
        }else{
            mBinding.edtDob.setText("")
        }

        mBinding.edtAddressLine1.setText(selectedCustomerModel!!.strAddress1)
        mBinding.edtAddressLine2.setText(selectedCustomerModel!!.strAddress2)
        mBinding.edtZipcode.setText(selectedCustomerModel!!.strZipCode)
        var pos_state = 0

        for (i in stateList.indices) {
            if (stateList[i].iStateID == selectedCustomerModel!!.iStateId.toInt()) {
                pos_state = i
                break
            }
        }
        mBinding.spnState.setSelection(pos_state)

        webCallGetCity(selectedCustomerModel!!.iStateId)

        mBinding.edtRemarks.setText(selectedCustomerModel!!.strRemark)

        if (selectedCustomerModel!!.strStatus.equals("Active")) {
            mBinding.rbtActive.isChecked = true
        } else {
            mBinding.rbtInActive.isChecked = true
        }
    }
}
