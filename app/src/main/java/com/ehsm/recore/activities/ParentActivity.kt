package com.ehsm.recore.activities


import android.app.admin.DevicePolicyManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.ehsm.recore.R
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.databinding.ActivityParentBinding
import com.ehsm.recore.fragments.BaseFragment
import com.ehsm.recore.fragments.NewSalesFragment
import com.ehsm.recore.interfaces.FragmentKeyeventListener
import com.ehsm.recore.utils.*
import com.ehsm.recore.utils.CM.showToast
import com.ehsm.recore.utils.CV.orderTransList
import com.google.android.material.navigation.NavigationView
import com.kcspl.divyangapp.viewmodel.LastActionViewModel
import com.kcspl.divyangapp.viewmodel.LastStatusDayStartCloseViewModel
import kotlinx.android.synthetic.main.custom_toast.*
import kotlinx.android.synthetic.main.header_layout.view.*


class ParentActivity : BaseActivity<ActivityParentBinding>(),
    NavigationView.OnNavigationItemSelectedListener {

    lateinit var toolbar: Toolbar
    lateinit var drawerLayout: DrawerLayout
    lateinit var navView: NavigationView
    private lateinit var mActivity: ParentActivity
    private lateinit var mFragment: Fragment
    private var lastActionViewModel: LastActionViewModel? = null
    private var lastStatusViewModel: LastStatusDayStartCloseViewModel? = null

    private lateinit var mDatabase: RecoreDB

    private var last_action_status = ""
    private var isCheckIn_status = false
    private var isCheckIn = false
    private var last_action = ""
    private var fragmentKeyeventListener: FragmentKeyeventListener? = null

    var emp_id = 0
    var emp_role = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        mDatabase = RecoreDB.getInstance(mActivity)!!
        //  mDatabase.daoOrderTrans().deleteOrderTrans()
        lastActionViewModel = ViewModelProviders.of(this).get(LastActionViewModel::class.java)
        lastStatusViewModel =
            ViewModelProviders.of(this).get(LastStatusDayStartCloseViewModel::class.java)

        emp_id = CM.getSp(this, CV.EMP_ID, 0) as Int
        emp_role = CM.getSp(this, CV.EMP_ROLE, "") as String

        CM.removeStatusBar(mActivity)
        //bindView(R.layout.activity_parent)
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_parent,
            getWindow().getDecorView().getRootView() as ViewGroup?,false)
        setContentView(mBinding.root)

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        drawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)

        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, 0, 0
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)
        //default fragment
        mFragment = NewSalesFragment()

        replaceFragmentClearStack(mFragment)
        title = getString(R.string.new_sale)

        mBinding.header.ivLogo.setOnClickListener {
            if (orderTransList.size > 0) {
                showToast(mActivity, getString(R.string.msg_empty_cart_to_proceed), R.drawable.icon, custom_toast_container)
                return@setOnClickListener
            }
            CM.setSp(mActivity, CV.ORDER_ID, "")
            CM.finishActivity(mActivity)
            Log.e("====", "=== list size 1 ===" + orderTransList.size)
        }

        mBinding.header.ivMore.setOnClickListener {
            if (orderTransList.size > 0) {
                showToast(mActivity, getString(R.string.msg_empty_cart_to_proceed), R.drawable.icon, custom_toast_container)
                return@setOnClickListener
            }
            CM.showPopup(mActivity, mBinding.header.ivMore, {
                //webCallProductCategory()
                syncWithServer()
                (mFragment as NewSalesFragment).resetOrder()
                replaceFragmentClearStack(mFragment)
                addFragment(mFragment as BaseFragment, NewSalesFragment())
            })
        }

        mBinding.header.ivNoSale.setOnClickListener {
            if (!CM.isInternetAvailable(mActivity)) {
                CM.showMessageOK(
                    mActivity,
                    "",
                    resources.getString(R.string.msg_network_error),
                    null
                )
            }else{
                if (orderTransList.size > 0) {
                    showToast(mActivity, getString(R.string.msg_empty_cart_to_proceed), R.drawable.icon, custom_toast_container)
                    return@setOnClickListener
                }
                if (last_action.equals(AppConstant.CLOCK_IN)) {
                    CM.openNosaleDialog(this, custom_toast_container)
                } else {
                    openDenyTransactionDialog()
                }
            }
        }

        mBinding.header.ivLock.setOnClickListener {
            if (orderTransList.size <= 0) {
                CM.startActivity(ScreenLockTimer.mActivity, PasscodeActivity::class.java)
            } else {
                showToast(mActivity, getString(R.string.msg_empty_cart_to_proceed), R.drawable.icon, custom_toast_container)
                return@setOnClickListener
            }
        }

        mBinding.header.reluser.setOnClickListener {
            callChangeMpin()
        }

        setEmpView(mBinding.header.tvEmpName)
        mBinding.header.tvEmpName.requestFocus()
        setStoreNameView(mBinding.header.tvstorename, mBinding.header.tvposname)

        mBinding.header.ivNewSale.setOnClickListener {
            if (orderTransList.size > 0) {
                showToast(mActivity, getString(R.string.msg_empty_cart_to_proceed), R.drawable.icon, custom_toast_container)
                return@setOnClickListener
            }
            if (last_action.equals(AppConstant.CLOCK_IN)) {
                if (last_action_status.equals(AppConstant.SHIFT_START)) {
                    if (!emp_role.equals(AppConstant.STAFF)) {
                        CM.setSp(mActivity, CV.ORDER_ID, "")
                        CM.navigateToDashboard(this)
                    } else {
                        openDenyTransactionDialog()
                    }
                } else {
                    openDenyDayStartDialog()
                }
            } else {
                openDenyTransactionDialog()
            }
            CM.finishActivity(mActivity)
        }

//        if(orderTransList.size <= 0){
//            ScreenLockTimer.registerLockListener(mBinding.header.ivLock)
//        }
        discardsDiscount()

    }


    public fun setFragmentKeyeventListener(fragmentKeyeventListener: FragmentKeyeventListener?) {
        this.fragmentKeyeventListener = fragmentKeyeventListener
    }

    private val barcode = StringBuffer()

    override fun dispatchKeyEvent(event: KeyEvent?): Boolean {

        if (event?.action == KeyEvent.ACTION_DOWN) {
            val pressedKey = event.unicodeChar.toChar()
            barcode.append(pressedKey)
        }
        if (event?.action == KeyEvent.ACTION_DOWN && event?.keyCode == KeyEvent.KEYCODE_ENTER) {
            if (fragmentKeyeventListener != null) {
                fragmentKeyeventListener!!.onFragmentBarcodeResult(barcode.toString().trim())
            }
            barcode.delete(0, barcode.length)
        }

        return super.dispatchKeyEvent(event)
    }

    fun lockScreen() {
        var deviceManger =
            mActivity.getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
        var compName = ComponentName(mActivity, MyAdmin::class.java)

        var active = deviceManger.isAdminActive(compName)

        if (active) {
            deviceManger.lockNow()
        } else {
            var intent = Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN)
            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, compName)

            intent.putExtra(
                DevicePolicyManager.EXTRA_ADD_EXPLANATION,
                "You should enable the app!"
            )
            startActivityForResult(intent, 1)
        }
    }

    override fun onBackPressed() {
        if (orderTransList.size <= 0) {
            CM.setSp(mActivity, CV.ORDER_ID, "")
            CM.finishActivity(mActivity)
        } else {
            CM.showToast(mActivity, getString(R.string.msg_empty_cart_to_proceed), R.drawable.icon, custom_toast_container)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_new_sales -> {
                mFragment = NewSalesFragment()
                replaceFragmentClearStack(mFragment)
                title = getString(R.string.new_sale)
            }
            R.id.nav_dashboard -> {
                mActivity.finish()
            }
            R.id.nav_saved_sales -> {
            }
            R.id.nav_sales_history -> {
            }
            R.id.nav_register_shift_report -> {
            }
            R.id.nav_open_cash_drawer -> {
            }
            R.id.nav_settings -> {
            }
            R.id.nav_devices -> {
            }
            R.id.nav_sync -> {
            }
            R.id.nav_manual_sync -> {
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun webCallGetLastAction() {
        showKcsDialog()

        lastActionViewModel!!.getLastAction(emp_id)
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(mActivity, loginResponseModel.message!!, R.drawable.icon, custom_toast_container)
                    isCheckIn = true
                } else {
                    isCheckIn =
                        !loginResponseModel.data!!.data.strTnxtype.equals(AppConstant.CLOCK_IN)
                }

                if (isCheckIn) {
                    last_action = AppConstant.CLOCK_OUT
                } else {
                    last_action = AppConstant.CLOCK_IN
                }

                webCallGetLastStatus()
            })

    }

    private fun webCallGetLastStatus() {
        showKcsDialog()

        lastStatusViewModel!!.getLastActionDayStartClose()
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(mActivity, loginResponseModel.message!!, R.drawable.icon, custom_toast_container)

                    isCheckIn_status = true
                } else {
                    isCheckIn_status =
                        !loginResponseModel.data!!.data.strTnxtype.equals(AppConstant.SHIFT_START)
                }

                if (isCheckIn_status) {
                    last_action_status = AppConstant.SHIFT_CLOSE
                    NewSaleFragmentCommon.resetShiftDetails(this)
                } else {
                    NewSaleFragmentCommon.setShiftDetails(this, loginResponseModel)
                    last_action_status = AppConstant.SHIFT_START
                }
            })
    }

    override fun onRequestGranted(requestCode: Int, perms: List<String>) {
    }

    override fun onResume() {
        super.onResume()

        var emp_role = CM.getSp(mActivity, CV.EMP_ROLE, "") as String

        when (emp_role) {
            AppConstant.STAFF -> {
                mBinding.header.ivNoSale.visibility = View.GONE
                mBinding.header.ivNewSale.visibility = View.GONE
            }
            AppConstant.CASHIER -> {
                mBinding.header.ivNoSale.visibility = View.GONE
                mBinding.header.ivNewSale.visibility = View.VISIBLE
            }
            AppConstant.STORE_ADMIN -> {
                mBinding.header.ivNoSale.visibility = View.VISIBLE
                mBinding.header.ivNewSale.visibility = View.VISIBLE
            }
        }

        if (!CM.isInternetAvailable(this)) {
        } else {
            webCallGetLastAction()
        }
    }
}
