package com.ehsm.recore.activities

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.appcompat.widget.AppCompatEditText
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ehsm.recore.R
import com.ehsm.recore.adapter.*
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.databinding.ActivityDashboardBinding
import com.ehsm.recore.model.*
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CM.showToast
import com.ehsm.recore.utils.CV
import com.ehsm.recore.viewmodel.CategoryListViewModel
import com.kcspl.divyangapp.viewmodel.*
import kotlinx.android.synthetic.main.custom_toast.*
import java.util.*
import kotlin.collections.ArrayList


class DashboardActivity : BaseActivity<ActivityDashboardBinding>(),
    CategoryListAdapter.ItemClickCategory,
    SubCategoryListAdapter.ItemClickSubCategory {

    private lateinit var mActivity: DashboardActivity
    private lateinit var mDatabase: RecoreDB
    private var categoryListViewModel: CategoryListViewModel? = null
    private var subCategoryListViewModel: SubCategoryListViewModel? = null
    private var itemListViewModel: ItemListViewModel? = null
    private var customerListViewModel: CustomerListViewModel? = null
    private var viewModelChangeMPin: ChangeMPinViewModel? = null


    private var placeOrderViewModel: PlaceOrderViewModel? = null


    private var categoryList = ArrayList<CategoryModel>()

    private var subcategoryList = ArrayList<SubCategoryModel>()
    private var itemList = ArrayList<ItemModel>()
    private var customerList = ArrayList<CustomerModel>()
    private var customerListFilter = ArrayList<CustomerModel>()

    var orderTransList: ArrayList<OrderTransModel> = ArrayList()


    private var order_id = ""
    private var emp_name = ""
    private var emp_image = ""
    private var emp_id = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        mDatabase = RecoreDB.getInstance(mActivity)!!
        categoryListViewModel = ViewModelProviders.of(this).get(CategoryListViewModel::class.java)
        subCategoryListViewModel =
            ViewModelProviders.of(this).get(SubCategoryListViewModel::class.java)
        itemListViewModel = ViewModelProviders.of(this).get(ItemListViewModel::class.java)
        customerListViewModel = ViewModelProviders.of(this).get(CustomerListViewModel::class.java)
        viewModelChangeMPin = ViewModelProviders.of(this).get(ChangeMPinViewModel::class.java)

        placeOrderViewModel = ViewModelProviders.of(this).get(PlaceOrderViewModel::class.java)

        CM.removeStatusBar(mActivity)
        //bindView(R.layout.activity_dashboard)
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_dashboard,
            getWindow().getDecorView().getRootView() as ViewGroup?,false)
        setContentView(mBinding.root)

        emp_id = CM.getSp(mActivity, CV.EMP_ID, 0) as Int

        emp_name = CM.getSp(mActivity, CV.EMP_NAME, "") as String
        mBinding.tvEmpName.setText(emp_name)

        emp_image = CM.getSp(mActivity, CV.EMP_IMAGE, "") as String

        Glide.with(mActivity)
            .load(emp_image)
            .placeholder(R.drawable.icon)
            .error(R.drawable.user)
            .into(mBinding.ivUser)

        if (!CM.isInternetAvailable(mActivity)) {
            val categoryListLiveData = mDatabase.daoCategoryList().getCategories()
            categoryListLiveData.observe(this, androidx.lifecycle.Observer { categorylist ->
                if (categorylist != null) {
                    categoryList = categorylist as ArrayList<CategoryModel>
                    Log.e("====", "=== categories =====" + categoryList.size)
                    mBinding.rvCategory.adapter = CategoryListAdapter(mActivity, categoryList, this)
                    populateSubcategory(categoryList.get(0).iProductCategoryID)
                }
            })
        } else {
            webCallProductCategory()
        }

        mBinding.reluser.setOnClickListener {
            openChangePasscodeDialog()

        }


        mBinding.gridItem.setOnItemClickListener { adapterView, view, i, l ->
            itemAmountCalculation(itemList.get(i))

        }

        mBinding.ivAddCustomer.setOnClickListener {
            openSearchCustomerDialog()
        }

        mBinding.ivAdd.setOnClickListener {
            CM.startActivity(mActivity, AddCustomerActivity::class.java)

        }

        mBinding.btnCheckout.setOnClickListener {
            openCheckOutDialog()
        }

    }

    private fun openChangePasscodeDialog() {
        val dialogchangempin = Dialog(mActivity)
        dialogchangempin.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogchangempin.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialogchangempin.setContentView(R.layout.dialog_change_mpin)

        dialogchangempin.setCancelable(false)
        dialogchangempin.show()

        val tvUserNameLogin = dialogchangempin.findViewById<TextView>(R.id.tvUserNameLogin)
        tvUserNameLogin.setText("Logged in as " + emp_name)

        val edtReEnterMpin = dialogchangempin.findViewById<EditText>(R.id.edtReEnterMpin)

        val btnFinish = dialogchangempin.findViewById<Button>(R.id.btnFinish)
        btnFinish.setOnClickListener {
            if (!CM.isInternetAvailable(mActivity)) {
                CM.showMessageOK(
                    mActivity,
                    "",
                    resources.getString(R.string.msg_network_error),
                    null
                )

            } else {
                webCallChangeMpin(edtReEnterMpin.text.toString(), dialogchangempin)
            }
        }
        val ivCancel = dialogchangempin.findViewById<ImageView>(R.id.ivCancel)
        ivCancel.setOnClickListener {
            dialogchangempin.dismiss()
        }
    }

    private fun webCallChangeMpin(mpin: String, dialogchangempin: Dialog) {
        showKcsDialog()

        viewModelChangeMPin!!.changeMpin(emp_id, mpin.toInt(),0)
            ?.observe(this, Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(mActivity, loginResponseModel.message!!, R.drawable.icon,custom_toast_container)
                } else {
                    showToast(mActivity, loginResponseModel.data!!.message, R.drawable.icon,custom_toast_container)
                    dialogchangempin.dismiss()
                }
            })
    }

    private fun webCallCustomerList(search: String, dialogcheckout: Dialog) {
        showKcsDialog()
        customerListViewModel!!.getCustomer(search)
            ?.observe(this, Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(mActivity, loginResponseModel.message!!, R.drawable.icon,custom_toast_container)
                } else {
                    showToast(mActivity, loginResponseModel.data!!.message, R.drawable.icon,custom_toast_container)
                    customerList = loginResponseModel.data!!.data
                    customerListFilter = loginResponseModel.data!!.data
                    Log.e("====", "=== customerList ####=====" + customerList.size)
                    val rvCustomer = dialogcheckout.findViewById<RecyclerView>(R.id.rvCustomer)
                    rvCustomer.adapter = CustomerListAdapter(mActivity, customerList, null)
                }

            })

    }

    private fun openSearchCustomerDialog() {
        val dialogcheckout = Dialog(mActivity)
        dialogcheckout.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogcheckout.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialogcheckout.setContentView(R.layout.dialog_search_customer)

        dialogcheckout.setCancelable(true)
        dialogcheckout.show()

        val edtSearchCustomer =
            dialogcheckout.findViewById<AppCompatEditText>(R.id.edtSearchCustomer)
        val rvCustomer = dialogcheckout.findViewById<RecyclerView>(R.id.rvCustomer)

        edtSearchCustomer.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                if (s.isNotEmpty()) filter(s.toString(),rvCustomer)
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })


        //get customer list
        if (!CM.isInternetAvailable(mActivity)) {
            val customerListLiveData = mDatabase.daoCustomerList().getCustomers()
            customerListLiveData.observe(this, androidx.lifecycle.Observer { customerlist ->
                if (customerlist != null) {
                    customerList = customerlist as ArrayList<CustomerModel>
                    customerListFilter = customerlist
                    Log.e("====", "=== customerList =====" + customerList.size)


                    rvCustomer.adapter = CustomerListAdapter(mActivity, customerList, null)
                }
            })

        } else {
            webCallCustomerList("", dialogcheckout)
        }
    }



    private fun openCheckOutDialog() {
        val dialogcheckout = Dialog(mActivity)
        dialogcheckout.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogcheckout.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialogcheckout.setContentView(R.layout.dialog_checkout)

        dialogcheckout.setCancelable(false)
        dialogcheckout.show()

        val ivCancel = dialogcheckout.findViewById<ImageView>(R.id.ivCancel)
        val btnTender = dialogcheckout.findViewById<Button>(R.id.btnTender)

        val btnCash = dialogcheckout.findViewById<Button>(R.id.btnCash)
        val llCash = dialogcheckout.findViewById<LinearLayout>(R.id.llCash)
        val relCash = dialogcheckout.findViewById<RelativeLayout>(R.id.relCash)

        val btnExternal = dialogcheckout.findViewById<Button>(R.id.btnCredit)
        val llExternal = dialogcheckout.findViewById<LinearLayout>(R.id.llExternal)

        val btnPay = dialogcheckout.findViewById<Button>(R.id.btnProceed)

        val btnOther = dialogcheckout.findViewById<Button>(R.id.btnOther)

        btnCash.setOnClickListener {
            btnCash.setTextColor(resources.getColor(R.color.colorWhite))
            btnCash.background = resources.getDrawable(R.drawable.btn_bg)

            btnExternal.setTextColor(resources.getColor(R.color.colorPrimary))
            btnExternal.background = resources.getDrawable(R.drawable.edt_bg)

            btnOther.setTextColor(resources.getColor(R.color.colorPrimary))
            btnOther.background = resources.getDrawable(R.drawable.edt_bg)

            llCash.visibility = View.VISIBLE
            relCash.visibility = View.VISIBLE
            llExternal.visibility = View.GONE

            btnPay.visibility = View.GONE
        }

        btnExternal.setOnClickListener {
            btnExternal.setTextColor(resources.getColor(R.color.colorWhite))
            btnExternal.background = resources.getDrawable(R.drawable.btn_bg)

            btnCash.setTextColor(resources.getColor(R.color.colorPrimary))
            btnCash.background = resources.getDrawable(R.drawable.edt_bg)

            btnOther.setTextColor(resources.getColor(R.color.colorPrimary))
            btnOther.background = resources.getDrawable(R.drawable.edt_bg)


            llCash.visibility = View.GONE
            relCash.visibility = View.GONE
            llExternal.visibility = View.VISIBLE

            btnPay.visibility = View.VISIBLE
        }

        ivCancel.setOnClickListener {
            //    dialogcheckout.dismiss()
            openPaymentCancellationDialog(dialogcheckout)
        }
        btnTender.setOnClickListener {
            dialogcheckout.dismiss()
            openReceiptDialog()
        }
    }

    private fun openPaymentCancellationDialog(dialog_previous: Dialog) {
        val dialog = Dialog(mActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setContentView(R.layout.dialog_cancel_payment)

        dialog.setCancelable(false)
        dialog.show()

        val btnNo = dialog.findViewById<Button>(R.id.btnNo)
        val btnYes = dialog.findViewById<Button>(R.id.btnYes)
        btnNo.setOnClickListener {
            dialog.dismiss()
        }

        btnYes.setOnClickListener {
            dialog.dismiss()
            dialog_previous.dismiss()
        }

    }

    private fun openReceiptDialog() {
        val dialog = Dialog(mActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_reciept)

        dialog.setCancelable(false)
        dialog.show()

        val ivCancel = dialog.findViewById<ImageView>(R.id.ivCancel)
        val btnFinish = dialog.findViewById<Button>(R.id.btnFinish)

        ivCancel.setOnClickListener {
            // dialog.dismiss()
            openPaymentCancellationDialog(dialog)
        }
        btnFinish.setOnClickListener {
            dialog.dismiss()
        }
    }


    private fun itemAmountCalculation(item_model: ItemModel) {
        var quantity = 1

        if (CM.getSp(mActivity, CV.ORDER_ID, "").equals("")) {
            order_id = UUID.randomUUID().toString()
            CM.setSp(mActivity, CV.ORDER_ID, order_id)
        } else {
            order_id = CM.getSp(mActivity, CV.ORDER_ID, "") as String
        }

        val rate = item_model.dblRetailPrice.toDouble()
        val amount = quantity * rate
        val orderTransModel = OrderTransModel(
            UUID.randomUUID().toString(),
            order_id,
            item_model.iProductID,
            item_model.strProductName,
            rate,
            quantity,
            amount,
            "",
            "Cash",
            "",
            item_model.totalTax.toString(),
            ((amount * item_model.totalTax)/100).toString(),
            "",
            0,
            0.0,
            false,
            item_model.strProductType,
            0,
            "",
            0.0f,
            "",
            "",
            "",
            0,
            true,
            tIsOpenDiscount = item_model.tIsOpenDiscount
        )
        mDatabase.daoOrderTrans().insertOrderTrans(orderTransModel)
    }


    private fun populateSubcategory(iProductCategoryID: Int) {
        if (!CM.isInternetAvailable(mActivity)) {
            val subcategoryListLiveData =
                mDatabase.daoSubCategoryList().getSubCategories(iProductCategoryID)
            subcategoryListLiveData.observe(this, androidx.lifecycle.Observer { subcategorylist ->
                if (subcategorylist != null) {
                    subcategoryList = subcategorylist as ArrayList<SubCategoryModel>
                    Log.e("====", "=== subcategoryList =====" + subcategoryList.size)
                    mBinding.rvsubCategory.adapter =
                        SubCategoryListAdapter(mActivity, subcategoryList, null)
                    populateItem(subcategoryList.get(0).iProductCategoryID)

                }
            })

        } else {
       //     webCallSubProductCategory(iProductCategoryID)

        }
    }

    private fun populateItem(suCategoryId: Int) {
        if (!CM.isInternetAvailable(mActivity)) {
            // offline code

            val itemListLiveData = mDatabase.daoItemList().getItems(suCategoryId)
            itemListLiveData.observe(this, androidx.lifecycle.Observer { itemlist ->
                if (itemlist != null) {
                    itemList = itemlist as ArrayList<ItemModel>
                    Log.e("====", "=== itemList =====" + itemList.size)
                    mBinding.gridItem.adapter = ItemListAdapter(mActivity, itemList)


                }
            })


        } else {
      //      webCallItemList(suCategoryId)

        }

    }
    private fun webCallProductCategory() {
       /* showKcsDialog()
        categoryListViewModel!!.getCategories(mDatabase, catSyncDateTime)
            ?.observe(this, Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(mActivity, loginResponseModel.message!!, R.drawable.icon,custom_toast_container)
                } else {
                    showToast(mActivity, loginResponseModel.data!!.message, R.drawable.icon,custom_toast_container)
                    categoryList = loginResponseModel.data!!.data
                    Log.e("====", "=== categories ####=====" + categoryList.size)
                    mBinding.rvCategory.adapter = CategoryListAdapter(mActivity, categoryList, this)
                    populateSubcategory(categoryList.get(0).iProductCategoryID)

                }

            })*/
    }

    override fun onRequestGranted(requestCode: Int, perms: List<String>) {

    }


    override fun listItemClickedCategory(position: Int, category_id: Int) {
        populateSubcategory(category_id)
    }

    override fun listItemClickedSubCategory(position: Int, subcategory_id: Int) {
        populateItem(subcategory_id)

    }

    fun filter(text: String, rvCustomer: RecyclerView) {
        val filterProduct: java.util.ArrayList<CustomerModel> =
            java.util.ArrayList<CustomerModel>()
            filterProduct.clear()
        //looping through existing elements
        for (s in customerListFilter) {
            //if the existing elements contains the search input
            if (s.strFirstName.toLowerCase().contains(text.toLowerCase())
                || s.strLastName.toLowerCase().contains(text.toLowerCase())
                || s.strPhone.toLowerCase().contains(text.toLowerCase())
            ) {
                //adding the element to filtered list
                filterProduct.add(s)
            }
        }
        rvCustomer.adapter = CustomerListAdapter(mActivity, filterProduct, null)

    }

}
