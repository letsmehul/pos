package com.ehsm.recore.activities

import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.ehsm.recore.R
import com.ehsm.recore.Recore
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.databinding.ActivityLandingPageBinding
import com.ehsm.recore.model.CDSSalesModel
import com.ehsm.recore.model.ResponseModel
import com.ehsm.recore.network.BoltApiHelper
import com.ehsm.recore.network.BoltRestAPIClient
import com.ehsm.recore.network.RestClient_24_7
import com.ehsm.recore.repository.PaymentRepository
import com.ehsm.recore.services.CallBack
import com.ehsm.recore.services.OrderPostingService
import com.ehsm.recore.utils.*
import com.ehsm.recore.utils.AutoAPKInstaller.installLatestAPK
import com.ehsm.recore.utils.AutoAPKInstaller.webCallAPKVersionCheck
import com.ehsm.recore.utils.CM.showToast
import com.ehsm.recore.viewmodel.APKVersionCheckViewModel
import com.ehsm.recore.viewmodel.PaymentViewModel
import com.ehsm.recore.viewmodel.TerminalViewModel
import com.ehsm.recore.viewmodelfactory.viewModelFactory
import com.google.gson.Gson
import com.kcspl.divyangapp.network.CustomApiCallback
import com.kcspl.divyangapp.viewmodel.*
import kotlinx.android.synthetic.main.custom_toast.*
import kotlinx.android.synthetic.main.header_layout.view.*
import java.text.SimpleDateFormat
import java.util.*


class LandingPageActivity : BaseActivity<ActivityLandingPageBinding>(), CallBack {
    private lateinit var mDatabase: RecoreDB

    private var emp_name = ""
    private var emp_role = ""

    private var emp_id = 0
    private var last_action = ""
    private var isBreakStarted = false;

    private var last_action_status = ""
    private var pos_name_status = ""
    private var date_time_status = ""

    private var checkInDataViewModel: CheckInDataViewModel? = null
    private var checkInOutDataViewModel: CheckInOutDataViewModel? = null
    private var checkOutDataViewModel: CheckOutDataViewModel? = null
    private var viewModelChangeMPin: ChangeMPinViewModel? = null
    private var lastStatusViewModel: LastStatusDayStartCloseViewModel? = null

    private var terminalViewModel: TerminalViewModel? = null
    private var mAPKVersionCheckViewModel: APKVersionCheckViewModel? = null
    var amount_check_out = 0.0
    var amount_day_close = 0.0

    private var pin = ""
    private var count = 0
    private var minusCount = false

    private lateinit var ivRoundOneCredential: ImageView
    private lateinit var ivRoundTwoCredential: ImageView
    private lateinit var ivRoundThreeCredential: ImageView
    private lateinit var ivRoundFourCredential: ImageView
    private lateinit var llPinRound: LinearLayout

    private var dialog: Dialog? = null
    private var viewModelVerifyMPin: VerifyMPinViewModel? = null
    private var lastActionViewModel: LastActionViewModel? = null

    private lateinit var dialogNosale: Dialog

    private var pos_name = ""
    private var date_time = ""
    private var currency_symbol = ""

    private lateinit var mActivity: LandingPageActivity
    private lateinit var paymentViewModel: PaymentViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        mDatabase = RecoreDB.getInstance(mActivity)!!

        mActivity.checkDrawOverlayPermission(
            AppConstant.CDS_NORMAL_SCREEN,
            "",
            false,
            "",
            CDSSalesModel(),
            "",
            false
        )

        currency_symbol = CM.getSp(this, CV.CURRENCY_CODE, "") as String

        checkInDataViewModel = ViewModelProviders.of(this).get(CheckInDataViewModel::class.java)
        checkInOutDataViewModel =
            ViewModelProviders.of(this).get(CheckInOutDataViewModel::class.java)
        checkOutDataViewModel = ViewModelProviders.of(this).get(CheckOutDataViewModel::class.java)
        viewModelVerifyMPin = ViewModelProviders.of(this).get(VerifyMPinViewModel::class.java)
        viewModelChangeMPin = ViewModelProviders.of(this).get(ChangeMPinViewModel::class.java)
        lastActionViewModel = ViewModelProviders.of(this).get(LastActionViewModel::class.java)
        lastStatusViewModel =
            ViewModelProviders.of(this).get(LastStatusDayStartCloseViewModel::class.java)

        CM.removeStatusBar(mActivity)
        //bindView(R.layout.activity_landing_page)
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_landing_page,
            getWindow().getDecorView().getRootView() as ViewGroup?,false)
        setContentView(mBinding.root)
        emp_name = CM.getSp(mActivity, CV.EMP_NAME, "") as String
        emp_role = CM.getSp(mActivity, CV.EMP_ROLE, "") as String

        emp_id = CM.getSp(mActivity, CV.EMP_ID, 0) as Int

        if (emp_role.equals(AppConstant.CASHIER)) {
            mBinding.layoutForceCheckOut.visibility = View.GONE
            mBinding.layoutOrderHistory.visibility = View.GONE
        } else if (emp_role.equals("General")) {
            mBinding.layoutForceCheckOut.visibility = View.GONE
            mBinding.layoutDayClose.visibility = View.GONE
            mBinding.layoutNewSale.visibility = View.GONE
            mBinding.layoutPayInOut.visibility = View.GONE
            mBinding.layoutNoSale.visibility = View.GONE
            mBinding.layoutOrderHistory.visibility = View.GONE
            mBinding.layoutCashDrawer.visibility = View.GONE
        } else {
            mBinding.layoutForceCheckOut.visibility = View.GONE //VISIBLE
            mBinding.layoutOrderHistory.visibility = View.GONE
        }

        mBinding.layoutNewSale.setOnClickListener {
            if (isInternetAvailable()) {
                if (last_action.equals(AppConstant.CLOCK_IN)) {
                    if (last_action_status.equals(AppConstant.SHIFT_START)) {
                        if (isBreakStarted) {
                            openBreakInStartDialog()
                        } else {
                            if (!emp_role.equals(AppConstant.STAFF)) {
                                CM.startActivity(mActivity, ParentActivity::class.java)
                            } else {
                                showRestrictDialog(getString(R.string.you_do_not_have_the_rights))
                            }
                        }
                    } else {
                        openDenyDayStartDialog()
                    }
                } else {
                    openDenyTransactionDialog()
                }
            }
        }

        mBinding.layoutMannualSyncData.setOnClickListener {
            if (isBreakStarted) {
                openBreakInStartDialog()
            } else {
                ScreenLockTimer.stopHandler()
                SyncOperation.syncData(mActivity, false) { isSyncFinished, message ->
                    CM.setSp(mActivity, CV.ISSYNCOPERATIONFINISHED, true)
                    ScreenLockTimer.startHandler(mActivity)
                    if (isSyncFinished) {
                        CM.showToast(
                            mActivity,
                            message!!,
                            R.drawable.icon,
                            null
                        )
                        recreate()
                    } else {
                        CM.showToast(
                            mActivity,
                            message!!,
                            R.drawable.icon,
                            null
                        )
                    }
                }
            }
        }
        mBinding.layoutFullSyncData.setOnClickListener {
            if (isBreakStarted) {
                openBreakInStartDialog()
            } else {
                ScreenLockTimer.stopHandler()
                SyncOperation.syncData(mActivity, true) { isSyncFinished, message ->
                    CM.setSp(mActivity, CV.ISSYNCOPERATIONFINISHED, true)
                    ScreenLockTimer.startHandler(mActivity)
                    if (isSyncFinished) {
                        CM.showToast(
                            mActivity,
                            message!!,
                            R.drawable.icon,
                            null
                        )
                        recreate()
                    } else {
                        CM.showToast(
                            mActivity,
                            message!!,
                            R.drawable.icon,
                            null
                        )
                    }
                }
            }
        }

        startService(Intent(mActivity, OrderPostingService::class.java))

        mBinding.header.ivNoSale.setOnClickListener {
            if (isInternetAvailable()) {
                if (last_action.equals(AppConstant.CLOCK_IN)) {
                    if (isBreakStarted) {
                        openBreakInStartDialog()
                    } else {
                        openNosaleDialog()
                    }
                } else {
                    openDenyTransactionDialog()
                }
            }
        }

        mBinding.header.reluser.setOnClickListener {
            if (isBreakStarted) {
                openBreakInStartDialog()
            } else {
                callChangeMpin()
            }
        }
        mBinding.header.ivMore.setOnClickListener {
            if (isBreakStarted) {
                openBreakInStartDialog()
            } else {
                if (emp_role == AppConstant.STAFF) {
                    showToast(
                        mActivity,
                        getString(R.string.msg_access_menu),
                        R.drawable.icon,
                        custom_toast_container
                    )
                } else {
                    CM.showPopup(mActivity, mBinding.header.ivMore) {
                        syncWithServer()
                    }
                }
            }

        }

        mBinding.header.ivNewSale.setOnClickListener {
            if (isInternetAvailable()) {
                if (last_action.equals(AppConstant.CLOCK_IN)) {
                    if (last_action_status.equals(AppConstant.SHIFT_START)) {
                        if (isBreakStarted) {
                            openBreakInStartDialog()
                        } else {
                            if (!emp_role.equals(AppConstant.STAFF)) {
                                CM.startActivity(mActivity, ParentActivity::class.java)
                                CM.setSp(mActivity, CV.ORDER_ID, "")
                            } else {
                                showRestrictDialog(getString(R.string.you_do_not_have_the_rights))
                            }
                        }
                    } else {
                        openDenyDayStartDialog()
                    }
                } else {
                    openDenyTransactionDialog()
                }
            }
        }



        mBinding.layoutDayClose.setOnClickListener {

        }
        mBinding.layoutCheckInOut.setBackgroundResource(R.drawable.btn_bg)
        mBinding.layoutCheckInOut.setOnClickListener {
            if (isInternetAvailable()) {
                openPassCodeDialog(AppConstant.CHECK_IN_OUT, "")
            }
        }


        if (isPayInOutEnableByStoreAdmin()) {
            mBinding.layoutPayInOut.setBackgroundResource(R.drawable.btn_bg)
            mBinding.layoutPayInOut.setOnClickListener {
                if (isInternetAvailable()) {
                    if (isBreakStarted) {
                        openBreakInStartDialog()
                    } else {
                        if (last_action.equals(AppConstant.CLOCK_IN)) {
                            openPassCodeDialog(AppConstant.PAY_IN_OUT, "")
                        } else {
                            openDenyTransactionDialog()
                        }
                    }
                }
            }
        } else {
            mBinding.layoutPayInOut.setBackgroundResource(R.drawable.btn_bg_disable)
            mBinding.layoutPayInOut.setOnClickListener {
                CM.showMessageOK(
                    mActivity,
                    "",
                    getString(R.string.msg_disable_payInPayout_action),
                    null
                )
            }
        }


        mBinding.layoutCashDrawer.setOnClickListener {
            if (isInternetAvailable()) {
                if (isBreakStarted) {
                    openBreakInStartDialog()
                } else {
                    if (last_action.equals(AppConstant.CLOCK_IN)) {
                        openPassCodeDialog(AppConstant.DRAWER, "")
                    } else {
                        openDenyTransactionDialog()
                    }
                }
            }
        }

        mBinding.layoutNoSale.setOnClickListener {
            if (isInternetAvailable()) {
                if (isBreakStarted) {
                    openBreakInStartDialog()
                } else {
                    if (last_action.equals(AppConstant.CLOCK_IN)) {
                        openNosaleDialog()
                    } else {
                        openDenyTransactionDialog()
                    }
                }
            }
        }

        mBinding.layoutForceCheckOut.setOnClickListener {
            if (isInternetAvailable()) {
                if (last_action.equals(AppConstant.CLOCK_IN)) {
                    CM.startActivity(mActivity, ForceCheckOutActivity::class.java)
                } else {
                    openDenyTransactionDialog()
                }
            }
        }

        val boltApiHelper = BoltApiHelper(BoltRestAPIClient.getService()!!)
        paymentViewModel = ViewModelProviders.of(this,
            viewModelFactory {
                val paymentViewModel1 =
                    PaymentViewModel(mActivity.application, PaymentRepository(boltApiHelper),mActivity)
                paymentViewModel1
            }
        ).get(PaymentViewModel::class.java)


        ScreenLockTimer.registerLockListener(mBinding.header.ivLock,mActivity)
        //syncWithServer()
        syncInruptedOperation()
    }


    /**
     * Check sync opreration is not finished or any currupted data
     */
    private fun syncInruptedOperation() {
        var isSyncOperationFinished: Boolean = true
        isSyncOperationFinished = CM.getSp(mActivity, CV.ISSYNCOPERATIONFINISHED, true) as Boolean
        if (!isSyncOperationFinished) {
            val msg = resources.getString(R.string.msg_sync_intrupted)
            CM.showSyncErrorDialog(
                mActivity,
                msg
            ) { dialog ->
                dialog.dismiss()
                mBinding.layoutFullSyncData.performClick()
            }

        }
    }


    private fun doTerminalSelection() {
        var terminal: String? = ""
        Recore.getApp()?.applicationContext?.let {
            val sharedPref = CM.getEncryptedSharedPreferences(it)
            terminal = sharedPref.getString(AppConstant.HSN, "")
        }
        if (TextUtils.isEmpty(terminal)) {
            webCallTerminal()
        } else {
            selectKitchenPrinters()
        }

    }

    private fun webCallTerminal() {
        terminalViewModel = ViewModelProviders.of(this).get(TerminalViewModel::class.java)
        terminalViewModel?.getTerminalList()
            ?.observe(this, androidx.lifecycle.Observer { terminalnResponseModel ->
                dismissKcsDialog()
                if (terminalnResponseModel.data == null) {
                    showToast(
                        mActivity,
                        terminalnResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                    selectKitchenPrinters()
                } else {
                    showTerminalDialog(terminalnResponseModel.data!!.data) {
                        it.let {

                            Log.e("DEBUG", "Terminal selected -- " + it.name)

                            selectKitchenPrinters()
                        }
                    }
                }

            })
    }

    private fun selectKitchenPrinters() {

        val sharedPref = CM.getEncryptedSharedPreferences(mActivity)
        KITCHEN_PRINTER = sharedPref.getString(AppConstant.KITCHEN_PRINTER_IP, "")

        if (TextUtils.isEmpty(KITCHEN_PRINTER)) {

            if (!ePOSPrinterController.kitchenPrinterdata.isNullOrEmpty()) {
                showKitchenPrinterDialog(ePOSPrinterController.kitchenPrinterdata) {
                    it.let {
                        KITCHEN_PRINTER = it
                        webCallAPKVersionCheck(mActivity)
                    }
                }
            } else {
                webCallAPKVersionCheck(mActivity)
            }
        } else {
            webCallAPKVersionCheck(mActivity)
        }

    }
    private fun is24_7Available(){
        Log.d("24_7_url",CM.getSp(mActivity,CV.POS_ID,0).toString())
        RestClient_24_7.getService()?.getStatus(CM.getSp(mActivity,CV.POS_ID,0).toString())?.enqueue(object : CustomApiCallback<ResponseModel>() {
            override fun handleResponseData(data: ResponseModel?) {

            }
            override fun showErrorMessage(errormessage: String?) {

                if (errormessage=="Unable to function, Please contact administrator.") {
                    CM.logout(mActivity)
                    Log.e("=====", errormessage!!)
                }
            }
        })
    }

    private fun isInternetAvailable(): Boolean {
        if (!CM.isInternetAvailable(this@LandingPageActivity)) {

            CM.showMessageOK(
                mActivity,
                "",
                resources.getString(R.string.msg_network_error),
                null
            )
            return false
        }
        return true
    }

    private fun isPayInOutEnableByStoreAdmin() =
        CM.getSp(this, CV.IS_PAY_IN_OUT_ENABLE, true) as Boolean


    override fun onResume() {
        super.onResume()
        is24_7Available();
        emp_name = CM.getSp(mActivity, CV.EMP_NAME, "") as String
        emp_role = CM.getSp(mActivity, CV.EMP_ROLE, "") as String
        emp_id = CM.getSp(mActivity, CV.EMP_ID, 0) as Int
        setEmpView(mBinding.header.tvEmpName)
        setStoreNameView(mBinding.header.tvstorename, mBinding.header.tvposname)

        if (emp_role.equals(AppConstant.CASHIER)) {
            mBinding.layoutForceCheckOut.visibility = View.GONE
            mBinding.layoutDayClose.visibility = View.GONE //VISIBLE
            mBinding.layoutNewSale.visibility = View.VISIBLE
            mBinding.layoutPayInOut.visibility = View.VISIBLE
            mBinding.layoutReport.visibility = View.GONE //VISIBLE
            mBinding.layoutNoSale.visibility = View.GONE //VISIBLE
            mBinding.layoutOrderHistory.visibility = View.GONE
            mBinding.layoutCashDrawer.visibility = View.VISIBLE
            mBinding.layoutMannualSyncData.visibility = View.INVISIBLE
            mBinding.layoutFullSyncData.visibility = View.INVISIBLE
            mBinding.header.ivNoSale.visibility = View.GONE
            mBinding.header.ivNewSale.visibility = View.VISIBLE
        } else if (emp_role.equals(AppConstant.STAFF)) {
            mBinding.layoutForceCheckOut.visibility = View.GONE
            mBinding.layoutDayClose.visibility = View.GONE
            mBinding.layoutNewSale.visibility = View.GONE
            mBinding.layoutPayInOut.visibility = View.GONE
            mBinding.layoutReport.visibility = View.GONE
            mBinding.layoutNoSale.visibility = View.GONE
            mBinding.layoutOrderHistory.visibility = View.GONE
            mBinding.layoutCashDrawer.visibility = View.GONE
            mBinding.header.ivNoSale.visibility = View.GONE
            mBinding.header.ivNewSale.visibility = View.GONE
            mBinding.layoutMannualSyncData.visibility = View.INVISIBLE
            mBinding.layoutFullSyncData.visibility = View.INVISIBLE
        } else {
            mBinding.layoutForceCheckOut.visibility = View.GONE //VISIBLE
            mBinding.layoutDayClose.visibility = View.INVISIBLE //VISIBLE
            mBinding.layoutNewSale.visibility = View.VISIBLE
            mBinding.layoutPayInOut.visibility = View.VISIBLE
            mBinding.layoutReport.visibility = View.INVISIBLE //VISIBLE
            mBinding.layoutNoSale.visibility = View.VISIBLE
            mBinding.layoutOrderHistory.visibility = View.GONE
            mBinding.layoutCashDrawer.visibility = View.VISIBLE
            mBinding.header.ivNoSale.visibility = View.VISIBLE
            mBinding.header.ivNewSale.visibility = View.VISIBLE
            mBinding.layoutMannualSyncData.visibility = View.VISIBLE
            mBinding.layoutFullSyncData.visibility = View.VISIBLE
        }
        if (!CM.isInternetAvailable(this)) {
            //offline code
            getOfflineLastActivityData()
        } else {
            dismissTerminalDialog()
            dismissKitchenPrinterDialog()
            webCallGetLastAction()
        }

        //Hardware Code For CDS Display -- Normal Image Display Case Invoke
        checkDrawOverlayPermission(
            AppConstant.CDS_NORMAL_SCREEN,
            "",
            false,
            "",
            CDSSalesModel(),
            "",
            false
        )
    }
    override fun onStart() {
        super.onStart()
        UserInteractionAwareCallback(window!!.callback,mActivity)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private fun webCallGetLastAction() {
        showKcsDialog()

        lastActionViewModel!!.getLastAction(emp_id)
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->

                Log.e(
                    "LandingActivity",
                    "webCallGetLastAction: " + Gson().toJson(loginResponseModel)
                )
                if (loginResponseModel.data == null) {
                    dismissKcsDialog()
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                    last_action = AppConstant.CLOCK_OUT
                    isBreakStarted = false
                } else {

                    pos_name = loginResponseModel.data!!.data.posName
                    date_time = loginResponseModel.data!!.data.dateTimeCreated
                    isBreakStarted = loginResponseModel.data!!.data.isBreakStarted
                    if (loginResponseModel.data!!.data.strTnxtype.equals(AppConstant.CLOCK_IN)) {
                        last_action = AppConstant.CLOCK_IN
                    } else {
                        last_action = AppConstant.CLOCK_OUT
                    }

                    Log.e("LandingActivity", "webCallGetLastAction : " + last_action)
                    webCallGetLastStatus()
                }
            })

    }

    private fun webCallGetLastStatus() {


        lastStatusViewModel!!.getLastActionDayStartClose()
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                Log.e(
                    "LandingActivity",
                    "webCallGetLastStatus: " + Gson().toJson(loginResponseModel)
                )
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                    last_action_status = AppConstant.SHIFT_CLOSE
                } else {
                    /* Toast.makeText(this, loginResponseModel.data!!.message, Toast.LENGTH_LONG)
                         .show()*/

                    pos_name_status = loginResponseModel.data!!.data.posName
                    date_time_status = loginResponseModel.data!!.data.dateTimeCreated

                    if (loginResponseModel.data!!.data.strTnxtype.equals(AppConstant.SHIFT_START)) {
                        last_action_status = AppConstant.SHIFT_START
                        NewSaleFragmentCommon.setShiftDetails(this, loginResponseModel)
                    } else {
                        NewSaleFragmentCommon.resetShiftDetails(this)
                        last_action_status = AppConstant.SHIFT_CLOSE
                    }

                    Log.e(
                        "LandingActivity",
                        "strTnxtype : " + loginResponseModel.data!!.data.strTnxtype
                    )
                    Log.e("LandingActivity", "if last_action_status : " + last_action_status)
                }
                doTerminalSelection()
            })
    }


    private fun getOfflineLastActivityData() {
        val itemListLiveData = mDatabase.daoManualSync().getAllItems(emp_id)

        pos_name = itemListLiveData.last_activity.get(0).posName
        date_time = itemListLiveData.last_activity.get(0).dateTimeCreated
        if (itemListLiveData.last_activity.get(0).strTnxtype.equals(AppConstant.CLOCK_IN)) {
            last_action = AppConstant.CLOCK_IN
        } else {
            last_action = AppConstant.CLOCK_OUT
        }
    }

    private fun openNosaleDialog() {
        dialogNosale = Dialog(mActivity)
        dialogNosale.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogNosale.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialogNosale.setContentView(R.layout.dialog_no_sale)

        dialogNosale.setCancelable(false)
        dialogNosale.show()

        val tvMakeChange = dialogNosale.findViewById<TextView>(R.id.tvMakeChange)
        val tvCountDrawer = dialogNosale.findViewById<TextView>(R.id.tvCountDrawer)
        val tvOther = dialogNosale.findViewById<TextView>(R.id.tvOther)
        val ivCancel = dialogNosale.findViewById<ImageView>(R.id.ivCancel)
        ivCancel.setOnClickListener {
            dialogNosale.dismiss()
        }
        tvMakeChange.setOnClickListener {
            openPassCodeDialog(AppConstant.NO_SALE, "Make Change")
        }

        tvCountDrawer.setOnClickListener {
            openPassCodeDialog(AppConstant.NO_SALE, "Count Drawer")
        }
        tvOther.setOnClickListener {
            openPassCodeDialog(AppConstant.NO_SALE, "Other")
        }

    }

    private fun openPassCodeDialog(from: String, reason: String) {
        if (dialog != null && dialog!!.isShowing) {
            dialog!!.dismiss()
        }
        dialog = Dialog(mActivity)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.setContentView(R.layout.dialog_passcode)

        dialog!!.setCancelable(false)
        dialog!!.show()

        ScreenLockTimer.stopHandler()
        val btnOneCredential = dialog!!.findViewById<Button>(R.id.btnOneCredential)
        val btnTwoCredential = dialog!!.findViewById<Button>(R.id.btnTwoCredential)
        val btnThreeCredential = dialog!!.findViewById<Button>(R.id.btnThreeCredential)
        val btnFourCredential = dialog!!.findViewById<Button>(R.id.btnFourCredential)
        val btnFiveCredential = dialog!!.findViewById<Button>(R.id.btnFiveCredential)
        val btnSixCredential = dialog!!.findViewById<Button>(R.id.btnSixCredential)
        val btnSevenCredential = dialog!!.findViewById<Button>(R.id.btnSevenCredential)
        val btnEightCredential = dialog!!.findViewById<Button>(R.id.btnEightCredential)
        val btnNineCredential = dialog!!.findViewById<Button>(R.id.btnNineCredential)
        val btnZeroCredential = dialog!!.findViewById<Button>(R.id.btnZeroCredential)

        val btnBackSpaceCredential = dialog!!.findViewById<Button>(R.id.btnBackSpaceCredential)
        val btnResetMpin = dialog!!.findViewById<Button>(R.id.btnResetMpin)

        ivRoundOneCredential = dialog!!.findViewById(R.id.ivRoundOneCredential)
        ivRoundTwoCredential = dialog!!.findViewById(R.id.ivRoundTwoCredential)
        ivRoundThreeCredential = dialog!!.findViewById(R.id.ivRoundThreeCredential)
        ivRoundFourCredential = dialog!!.findViewById(R.id.ivRoundFourCredential)

        llPinRound = dialog!!.findViewById(R.id.llPinRound)

        val tvClose = dialog!!.findViewById<TextView>(R.id.tvClose)

        tvClose.setOnClickListener {
            dialog!!.dismiss()
            ScreenLockTimer.startHandler(mActivity)
        }
        btnResetMpin.setOnClickListener {
            dialog!!.dismiss()
            count = 0
            pin = ""
            //  openChangePasscodeDialog()
        }
        btnOneCredential.setOnClickListener {
            countCall(count)
            fillPasscode("1", from, reason)
        }

        btnTwoCredential.setOnClickListener(View.OnClickListener {
            countCall(count)
            fillPasscode("2", from, reason)
        })
        btnThreeCredential.setOnClickListener {
            countCall(count)
            fillPasscode("3", from, reason)
        }
        btnFourCredential.setOnClickListener {
            countCall(count)
            fillPasscode("4", from, reason)
        }
        btnFiveCredential.setOnClickListener {
            countCall(count)
            fillPasscode("5", from, reason)
        }
        btnSixCredential.setOnClickListener {
            countCall(count)
            fillPasscode("6", from, reason)
        }
        btnSevenCredential.setOnClickListener {
            countCall(count)
            fillPasscode("7", from, reason)
        }
        btnEightCredential.setOnClickListener {
            countCall(count)
            fillPasscode("8", from, reason)
        }
        btnNineCredential.setOnClickListener {
            countCall(count)
            fillPasscode("9", from, reason)
        }
        btnZeroCredential.setOnClickListener {
            countCall(count)
            fillPasscode("0", from, reason)
        }
        btnBackSpaceCredential.setOnClickListener {
            dropPasscode()
            if (count > 0) {
                count -= 1
            }
            minusCount = true
            countCall(count)
        }
    }

    private fun webcallVerifyMPin(from: String, reason: String) {
        showKcsDialog()
        viewModelVerifyMPin!!.verifyMPin(pin)
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {

                    loginResponseModel.message?.let {
                        DialogUtility.showPinInvalidateDialog(
                            mActivity,
                            it,
                            loginResponseModel.action
                        )
                    }
                    // Toast.makeText(mActivity, loginResponseModel.message, Toast.LENGTH_LONG).show()
                    val shake =
                        AnimationUtils.loadAnimation(this, R.anim.shake)
                    llPinRound.startAnimation(shake)
                    count = 0
                    ivRoundOneCredential.setImageResource(R.drawable.roundblankbutton)
                    ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                    ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                    ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                    minusCount = false
                    pin = ""

                } else {
                    dialog!!.dismiss()
                    if (from.equals(AppConstant.CHECK_IN_OUT)) {
                        CM.setSp(
                            this,
                            CV.EMP_NAME,
                            loginResponseModel.data!!.data.strFirstName + " " + loginResponseModel.data!!.data.strLastName
                        )
                        emp_id = loginResponseModel.data!!.data.iCheckEmployeeId
                        emp_role = loginResponseModel.data!!.data.strEmployeeRoleName
                        CM.setSp(
                            this,
                            CV.EMP_OPEN_DISCOUNT,
                            loginResponseModel.data!!.data.tIsOpenDiscount
                        )
                        CM.setSp(this, CV.EMP_IMAGE, loginResponseModel.data!!.data.strProfilePhoto)
                        CM.setSp(this, CV.EMP_ID, loginResponseModel.data!!.data.iCheckEmployeeId)
                        CM.setSp(
                            this,
                            CV.EMP_ROLE,
                            loginResponseModel.data!!.data.strEmployeeRoleName
                        )
                        if (loginResponseModel.data!!.data.strEmployeeRoleName.equals(AppConstant.CASHIER)) {
                            val isCheckIn = CM.getSp(mActivity, CV.ISCHECKIN, false) as Boolean
                            val emp_id_pref = CM.getSp(mActivity, CV.CHECKINUSER, 0) as Int

                            Log.e("LANDING_PAGE", "isCheckIn: " + isCheckIn)
                            Log.e("LANDING_PAGE", "emp_id_pref: " + emp_id_pref)
                            Log.e("LANDING_PAGE", "emp_id: " + emp_id)

                            CM.startActivity(mActivity, CheckInOutActivity::class.java)

                        } else {
                            CM.startActivity(mActivity, CheckInOutActivity::class.java)
                        }
                    } else if (from.equals(AppConstant.PAY_IN_OUT)) {
                        Log.e("====", "=== pay in out ===" + emp_role)
                        if (loginResponseModel.data!!.data.strEmployeeRoleName == AppConstant.STAFF) {
                            showRestrictDialog(getString(R.string.you_do_not_have_the_rights))
                            return@Observer
                        }
                        if (loginResponseModel.data!!.data.lastEmployeeStatus.equals(AppConstant.CLOCK_OUT)) {
                            count = 0
                            minusCount = false
                            pin = ""
                            showToast(
                                mActivity,
                                getString(R.string.msg_user_already_clock_out),
                                R.drawable.icon,
                                custom_toast_container
                            )
                            dialog!!.dismiss()
                            return@Observer
                        }
                        CM.setSp(
                            this,
                            CV.EMP_NAME,
                            loginResponseModel.data!!.data.strFirstName + " " + loginResponseModel.data!!.data.strLastName
                        )
                        emp_id = loginResponseModel.data!!.data.iCheckEmployeeId
                        emp_role = loginResponseModel.data!!.data.strEmployeeRoleName
                        CM.setSp(
                            this,
                            CV.EMP_OPEN_DISCOUNT,
                            loginResponseModel.data!!.data.tIsOpenDiscount
                        )
                        CM.setSp(this, CV.EMP_IMAGE, loginResponseModel.data!!.data.strProfilePhoto)
                        CM.setSp(this, CV.EMP_ID, loginResponseModel.data!!.data.iCheckEmployeeId)
                        CM.setSp(
                            this,
                            CV.EMP_ROLE,
                            loginResponseModel.data!!.data.strEmployeeRoleName
                        )
                        if (CM.getSp(this, CV.EMP_ROLE, "").equals(AppConstant.STAFF)) {
                            showRestrictDialog(getString(R.string.you_do_not_have_the_rights))
                        } else {
                            if (emp_role.equals(AppConstant.STORE_ADMIN)) {
                                CM.startActivity(mActivity, PayInOutActivity::class.java)
                            } else {
                                if (last_action.equals(AppConstant.CLOCK_IN) && last_action_status.equals(
                                        AppConstant.SHIFT_START
                                    )
                                ) {
                                    if (!emp_role.equals(AppConstant.STAFF)) {
                                        CM.startActivity(mActivity, PayInOutActivity::class.java)
                                    } else {
                                        openDenyTransactionDialog()
                                    }
                                } else {
                                    openDenyDayStartDialog()
                                }
                            }
                        }
                    } else if (from.equals(AppConstant.DRAWER)) {
                        if (loginResponseModel.data!!.data.strEmployeeRoleName == AppConstant.STAFF) {
                            showRestrictDialog(getString(R.string.you_do_not_have_the_rights))
                            return@Observer
                        }
                        if (loginResponseModel.data!!.data.lastEmployeeStatus.equals("Clock Out")) {
                            count = 0
                            minusCount = false
                            pin = ""
                            showToast(
                                mActivity,
                                getString(R.string.msg_user_already_clock_out),
                                R.drawable.icon,
                                custom_toast_container
                            )
                            dialog!!.dismiss()
                            return@Observer
                        }
                        CM.setSp(
                            this,
                            CV.EMP_NAME,
                            loginResponseModel.data!!.data.strFirstName + " " + loginResponseModel.data!!.data.strLastName
                        )
                        emp_id = loginResponseModel.data!!.data.iCheckEmployeeId
                        emp_role = loginResponseModel.data!!.data.strEmployeeRoleName
                        CM.setSp(
                            this,
                            CV.EMP_OPEN_DISCOUNT,
                            loginResponseModel.data!!.data.tIsOpenDiscount
                        )
                        CM.setSp(this, CV.EMP_IMAGE, loginResponseModel.data!!.data.strProfilePhoto)
                        CM.setSp(this, CV.EMP_ID, loginResponseModel.data!!.data.iCheckEmployeeId)
                        CM.setSp(
                            this,
                            CV.EMP_ROLE,
                            loginResponseModel.data!!.data.strEmployeeRoleName
                        )
                        if (CM.getSp(this, CV.EMP_ROLE, "").equals(AppConstant.STAFF)) {
                            showRestrictDialog(getString(R.string.you_do_not_have_the_rights))
                        } else {
                            CM.startActivity(mActivity, DayStartCloseActivity::class.java)
                        }
                    } else if (from.equals(AppConstant.NO_SALE)) {
                        if (loginResponseModel.data!!.data.strEmployeeRoleName == AppConstant.STAFF) {
                            showRestrictDialog(getString(R.string.you_do_not_have_the_rights))
                            return@Observer
                        }
                        if (loginResponseModel.data!!.data.lastEmployeeStatus.equals(AppConstant.CLOCK_OUT)) {
                            count = 0
                            minusCount = false
                            pin = ""
                            showToast(
                                mActivity,
                                getString(R.string.msg_user_already_clock_out),
                                R.drawable.icon,
                                custom_toast_container
                            )
                            dialog!!.dismiss()
                            return@Observer
                        }
                        /*
                        // no need to required save data while no sale option selected bcoz dashboard value changed.
                        CM.setSp(
                            this,
                            CV.EMP_NAME,
                            loginResponseModel.data!!.data.strFirstName + " " + loginResponseModel.data!!.data.strLastName
                        )
                        emp_id = loginResponseModel.data!!.data.iCheckEmployeeId
                        emp_role = loginResponseModel.data!!.data.strEmployeeRoleName
                        CM.setSp(this, CV.EMP_IMAGE, loginResponseModel.data!!.data.strProfilePhoto)
                        CM.setSp(this, CV.EMP_ID, loginResponseModel.data!!.data.iCheckEmployeeId)
                        CM.setSp(this, CV.EMP_ROLE, loginResponseModel.data!!.data.strEmployeeRoleName)*/
                        if (loginResponseModel.data!!.data.strEmployeeRoleName.equals(AppConstant.STORE_ADMIN)) {
                            // call API
                            if (reason.equals(AppConstant.OTHER)) {
                                //open pop up

                                val dialog = Dialog(mActivity)
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                                dialog.window!!
                                    .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                                dialog.setContentView(R.layout.dialog_other_reason)

                                dialog.setCancelable(true)
                                dialog.show()

                                val btnConfirmReason =
                                    dialog.findViewById<Button>(R.id.btnConfirmReason)
                                val edtReason = dialog.findViewById<EditText>(R.id.edtReason)
                                btnConfirmReason.setOnClickListener {
                                    if (edtReason.text.toString().trim().equals("")) {
                                        showToast(
                                            mActivity,
                                            getString(R.string.please_enter_reason),
                                            R.drawable.icon,
                                            custom_toast_container
                                        )
                                    } else {
                                        webCallSendCheckInOutData(
                                            0.0,
                                            0.0,
                                            AppConstant.NO_SALE,
                                            edtReason.text.toString(),
                                            dialog
                                        )
                                    }
                                }
                            } else {
                                webCallSendCheckInOutData(
                                    0.0,
                                    0.0,
                                    AppConstant.NO_SALE,
                                    reason,
                                    dialog!!
                                )
                            }
                        } else {
                            showRestrictDialog(getString(R.string.you_do_not_have_the_rights))
                        }
                    }

                    count = 0
                    minusCount = false
                    pin = ""
                }

            })
    }


    private fun fillPasscode(code: String, from: String, reason: String) {
        if (count in 1..4) {
            pin += code
        }
        if (count == 4) {
            try {
                if (!CM.isInternetAvailable(mActivity)) {
                    val itemListLiveData = mDatabase.daoEmployeeList().getAllEmployee()
                    var matchFound = false
                    for (i in itemListLiveData.indices) {
                        Log.e("=== emp name ==", "===" + itemListLiveData.get(i).strFirstName)
                        if (pin.equals(itemListLiveData.get(i).iMpin)) {
                            matchFound = true
                            Log.e(
                                "=== emp name ###==",
                                "###===" + itemListLiveData.get(i).strFirstName
                            )

                            CM.setSp(
                                this,
                                CV.EMP_NAME,
                                itemListLiveData.get(i).strFirstName + " " + itemListLiveData.get(i).strLastName
                            )
                            CM.setSp(this, CV.EMP_IMAGE, "")
                            CM.setSp(this, CV.EMP_ID, itemListLiveData.get(i).iCheckEmployeeId)
                            CM.setSp(this, CV.EMP_ROLE, itemListLiveData.get(i).strEmployeeRoleName)

                            break

                        }
                    }
                    if (matchFound) {
                        dialog!!.dismiss()
                        count = 0
                        ivRoundOneCredential.setImageResource(R.drawable.roundblankbutton)
                        ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                        ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                        ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                        minusCount = false
                        pin = ""
                        CM.startActivity(mActivity, CheckInOutActivity::class.java)
                    } else {
                        DialogUtility.showPinInvalidateDialog(mActivity)
                        val shake =
                            AnimationUtils.loadAnimation(this, R.anim.shake)
                        llPinRound.startAnimation(shake)
                        count = 0
                        ivRoundOneCredential.setImageResource(R.drawable.roundblankbutton)
                        ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                        ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                        ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                        minusCount = false
                        pin = ""

                    }
                } else {
                    ScreenLockTimer.startHandler(mActivity)
                    webcallVerifyMPin(from, reason)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun dropPasscode() {
        if (count <= 4 && count > 0) {
            if (pin.length > 0) {
                pin = pin.substring(0, pin.length - 1)
            }
        }
    }

    fun countCall(c: Int) {
        when (c) {
            0 -> if (minusCount) {
                count = 0
                ivRoundOneCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                minusCount = false
            } else {
                count += 1
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

            }
            1 -> if (minusCount) {
                count = 1
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                minusCount = false
            } else {
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                count += 1
            }
            2 -> if (minusCount) {
                count = 2
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.roundblankbutton)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                minusCount = false
            } else {
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                count += 1
            }
            3 -> if (minusCount) {
                count = 3
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.roundblankbutton)

                minusCount = false
            } else {
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                count += 1
            }
            4 -> if (minusCount) {
                count = 4
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                minusCount = false
            } else {
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                count += 1
            }
            5 -> if (minusCount) {
                count = 5
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                minusCount = false
            } else {
                ivRoundOneCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundTwoCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundThreeCredential.setImageResource(R.drawable.rounded_corner)
                ivRoundFourCredential.setImageResource(R.drawable.rounded_corner)

                count += 1
            }
        }
    }

    private fun webCallGetCheckInData(tvCashInDrawer: TextView) {
        showKcsDialog()

        checkInDataViewModel!!.getCheckInData(emp_id)
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                } else {
                    showToast(
                        mActivity,
                        loginResponseModel.data!!.message,
                        R.drawable.icon,
                        custom_toast_container
                    )

                    tvCashInDrawer.text = loginResponseModel.data!!.data.dblCashInDrawer
                }

            })

    }

    private fun webCallSendCheckInOutData(
        cashInDrawer: Double,
        totalCashInDrawer: Double,
        type: String,
        remark: String,
        dialog: Dialog
    ) {

        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.WS_DATE_FORMATTE)
        showKcsDialog()

        checkInOutDataViewModel!!.checkInOutData(
            sdf.format(date),
            cashInDrawer,
            0.0,
            0.0,
            type,
            emp_id,
            remark,
            totalCashInDrawer,
            emp_id,
            0,
            0
        )
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (type == AppConstant.NO_SALE) {

                    //Open Cashbox Drawer for NoSale Main Menu
                    openCashBox()

                    dialogNosale.dismiss()
                }
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                } else {
                    showToast(
                        mActivity,
                        loginResponseModel.data!!.message,
                        R.drawable.icon,
                        custom_toast_container
                    )
                    dialog.dismiss()
                }

            })
    }

    override fun onRequestGranted(requestCode: Int, perms: List<String>) {

    }

    override fun updateUI() {
        Log.e("====", "==== update UI ===")
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        if (requestCode == CV.INSTALL_APK_CODE) {
            if (resultCode == RESULT_OK) {
                showToast(
                    mActivity,
                    "App Installation Success",
                    R.drawable.icon,
                    custom_toast_container
                )
            } else {
                Log.e("DEBUG", "Cancel Button Callback")
                DialogUtility.openSingleActionDialog(
                    getString(R.string.auto_install_apk_msg),
                    "Install",
                    mActivity
                ) {
                    if (it) {
                        installLatestAPK(mActivity)
                    }
                }
            }

        }
        super.onActivityResult(requestCode, resultCode, data)
    }

}
