package com.ehsm.recore.activities

import android.os.Bundle
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil

import com.ehsm.recore.R
import com.ehsm.recore.databinding.ActivityEmployeeBinding


class EmployeeActivity : BaseActivity<ActivityEmployeeBinding>(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //bindView(R.layout.activity_employee)
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_employee,
            getWindow().getDecorView().getRootView() as ViewGroup?,false)
        setContentView(mBinding.root)

    }

    override fun onRequestGranted(requestCode: Int, perms: List<String>) {

    }

}
