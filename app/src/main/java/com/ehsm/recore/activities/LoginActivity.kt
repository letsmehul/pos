package com.ehsm.recore.activities

import android.app.Dialog
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.AsyncTask
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.ehsm.recore.BuildConfig
import com.ehsm.recore.R
import com.ehsm.recore.Recore
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.databinding.ActivityLoginBinding
import com.ehsm.recore.model.ResponseModel
import com.ehsm.recore.network.RestClient_24_7
import com.ehsm.recore.utils.*
import com.ehsm.recore.utils.CM.showToast
import com.ehsm.recore.viewmodel.CategoryListViewModel
import com.ehsm.recore.viewmodel.EmployeeListViewModel
import com.kcspl.divyangapp.network.CustomApiCallback
import com.kcspl.divyangapp.viewmodel.ForgotPasswordViewModel
import com.kcspl.divyangapp.viewmodel.ItemListViewModel
import com.kcspl.divyangapp.viewmodel.LoginViewModel
import com.kcspl.divyangapp.viewmodel.SubCategoryListViewModel
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.custom_toast.*
import java.net.NetworkInterface
import java.util.*


class LoginActivity : BaseActivity<ActivityLoginBinding>() {

    private lateinit var mActivity: LoginActivity
    private var viewModelLogin: LoginViewModel? = null
    private var viewModelForgotPassword: ForgotPasswordViewModel? = null
    private var categoryListViewModel: CategoryListViewModel? = null
    private var subCategoryListViewModel: SubCategoryListViewModel? = null
    private var itemListViewModel: ItemListViewModel? = null
    private var employeeListViewModel: EmployeeListViewModel? = null


    private lateinit var mDatabase: RecoreDB
    private var wifiAdrress: String = ""
    private var etherNet: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        mDatabase = RecoreDB.getInstance(mActivity)!!

        viewModelLogin = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        viewModelForgotPassword =
            ViewModelProviders.of(this).get(ForgotPasswordViewModel::class.java)
        subCategoryListViewModel =
            ViewModelProviders.of(this).get(SubCategoryListViewModel::class.java)
        itemListViewModel = ViewModelProviders.of(this).get(ItemListViewModel::class.java)
        categoryListViewModel = ViewModelProviders.of(this).get(CategoryListViewModel::class.java)
        employeeListViewModel = ViewModelProviders.of(this).get(EmployeeListViewModel::class.java)


        CM.removeStatusBar(mActivity)
        //bindView(R.layout.activity_login)
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_login,
            getWindow().getDecorView().getRootView() as ViewGroup?,false)
        setContentView(mBinding.root)
        var versionNmae = BuildConfig.VERSION_NAME.replace("-hardware", "")
        tvVersion.text = "Version - $versionNmae (${CV.SYMBOL})"
        wifiAdrress = getMACAddress("wlan0")
        etherNet = getMACAddress("eth0")

        tvEthMacAddress.text = "Ethernet :: $etherNet"
        tvMacAddress.text = "Wifi :: $wifiAdrress"

        mBinding.btnSignIn.setOnClickListener {
            if (mBinding.edtUserName.text!!.toString().equals("")) {
                showToast(
                    mActivity,
                    getString(R.string.please_enter_user_name),
                    R.drawable.icon,
                    custom_toast_container
                )
            } else if (mBinding.edtstorePassword.text!!.toString().equals("")) {
                showToast(
                    mActivity,
                    getString(R.string.please_enter_password),
                    R.drawable.icon,
                    custom_toast_container
                )
            } else {
                if (!CM.isInternetAvailable(mActivity)) {
                    CM.showMessageOK(
                        mActivity,
                        "",
                        resources.getString(R.string.msg_network_error),
                        null
                    )
                } else {
                    webCallLogin()
                    //is24_7Available()
                }
            }

        }

        mBinding.tvForgotPassword.setOnClickListener {
            openForgotPasswordDialog()
        }

        mBinding.tvWhatsMyMac.setOnClickListener {
            tvMacAddress.visibility = View.VISIBLE
            tvEthMacAddress.visibility = View.VISIBLE
            mBinding.tvWhatsMyMac.visibility = View.INVISIBLE
        }

    }

    private fun openForgotPasswordDialog() {
        val dialog = Dialog(mActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setContentView(R.layout.dialog_forgot_password)

        dialog.setCancelable(false)
        dialog.show()

        val ivCancel = dialog.findViewById<ImageView>(R.id.ivCancel)
        val btnSubmit = dialog.findViewById<Button>(R.id.btnSubmit)
        val edtEmail = dialog.findViewById<EditText>(R.id.edtEmail)

        ivCancel.setOnClickListener {
            dialog.dismiss()
        }
        btnSubmit.setOnClickListener {
            if (!CM.isInternetAvailable(mActivity)) {
                CM.showMessageOK(
                    mActivity,
                    "",
                    resources.getString(R.string.msg_network_error),
                    null
                )

            } else {
                if (edtEmail.text.toString().equals("")) {
                    showToast(
                        mActivity,
                        "Please Enter Email",
                        R.drawable.icon,
                        custom_toast_container
                    )
                } else {
                    if (!edtEmail.text.toString().isEmailValid()) {
                        showToast(
                            mActivity,
                            "Please Enter Valid Email",
                            R.drawable.icon,
                            custom_toast_container
                        )
                    } else {
                        webCallForgotPassword(edtEmail.text.toString(), dialog)
                    }
                }
            }
        }
    }

    fun String.isEmailValid(): Boolean {
        return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this)
            .matches()
    }

    private fun webCallForgotPassword(email: String, dialog: Dialog) {
        showKcsDialog()
        viewModelForgotPassword!!.forgotPassword(email)
            ?.observe(this, Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                } else {
                    showToast(
                        mActivity,
                        loginResponseModel.data!!.message,
                        R.drawable.icon,
                        custom_toast_container
                    )
                    dialog.dismiss()
                }

            })
    }

    private fun webCallLogin() {
        val mac_address = "$wifiAdrress,$etherNet"
        Log.e("===", "=== MAC Address ====" + mac_address)
        showKcsDialog()
        viewModelLogin!!.logIn(
            mBinding.edtUserName.text.toString(),
            mBinding.edtstorePassword.text.toString(),
            "",
            mac_address
        )
            ?.observe(this, Observer { loginResponseModel ->
                //dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    dismissKcsDialog()
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                } else {
                    CM.setSp(this, CV.ISLOGINCALLED, true)
                    CM.setSp(this, CV.API_KEY, loginResponseModel.data!!.data.strApiKey)
                    CM.setSp(this, CV.POS_NAME, loginResponseModel.data!!.data.posName)
                    CM.setSp(this, CV.STORE_ID, loginResponseModel.data!!.data.store_id)
                    CM.setSp(this, CV.POS_ID, loginResponseModel.data!!.data.iUserLogins)
                    CM.setSp(this, CV.STRSOCIALMEDIA, loginResponseModel.data!!.data.strSocialMedia)
                    loginResponseModel.data!!.data.strFacebook?.let {
                        CM.setSp(
                            this, CV.STRFACEBOOK,
                            it
                        )
                    }
                    loginResponseModel.data!!.data.strInstagram?.let {
                        CM.setSp(
                            this, CV.STRINSTAGRAM,
                            it
                        )
                    }
                    loginResponseModel.data!!.data.strTwitter?.let {
                        CM.setSp(
                            this, CV.STRTWITTER,
                            it
                        )
                    }
                    loginResponseModel.data!!.data.strYoutube?.let {
                        CM.setSp(
                            this, CV.STRYOUTUBE,
                            it
                        )
                    }

                    //   CM.setSp(this, CV.CURRENCY_CODE, loginResponseModel.data!!.data.strCurrencyCode)
                    CM.setSp(this, CV.PAYMENT_TYPE, loginResponseModel.data!!.data.strPaymentMethod)
                    CM.setSp(this, CV.LOCK_TIMER, loginResponseModel.data!!.data.strScreenLockTimer)
                    CM.setSp(
                        this,
                        CV.IS_ADMIN_APPROVAL_REQUIRED_FOR_TRANSACTION,
                        loginResponseModel.data!!.data.tIsTransactionalRights
                    )
                    CM.setSp(
                        this,
                        CV.SIGNATURE_AMOUNT,
                        loginResponseModel.data!!.data.dblSignatureAmount
                    )

                    CM.setSp(
                        this,
                        CV.CURRENCY_CODE,
                        loginResponseModel.data!!.data.strCurrencysymbol
                    )

                    CM.setSp(this, CV.STORE_NAME, loginResponseModel.data!!.data.strStoreName)
                    CM.setSp(
                        this,
                        CV.STORE_ADD_LINE1,
                        loginResponseModel.data!!.data.strAddressLine1
                    )
                    CM.setSp(
                        this,
                        CV.STORE_ADD_LINE2,
                        loginResponseModel.data!!.data.strAddressLine2
                    )
                    CM.setSp(this, CV.STORE_TEL_NUM, loginResponseModel.data!!.data.strPhone)
                    CM.setSp(this, CV.STORE_LOGO_URL, loginResponseModel.data!!.data.strLogo)
                    CM.setSp(
                        this,
                        CV.SQUARE_LOGO_PATH,
                        loginResponseModel.data!!.data.strSquareImageLogo
                    )
                    CM.setSp(this,CV.STRTIMEZONE,loginResponseModel.data!!.data.strTimeZone)

                    if (!TextUtils.isEmpty(loginResponseModel.data!!.data.strBoltMerchantId)) {
                        Recore.getApp()?.applicationContext?.let {
                            val sharedPref = CM.getEncryptedSharedPreferences(
                                it
                            )
                            sharedPref.edit().putString(
                                AppConstant.MERCHANTID,
                                loginResponseModel.data!!.data.strBoltMerchantId
                            ).commit()
                        }

                    }
                    if (!TextUtils.isEmpty(loginResponseModel.data!!.data.strBoltAuthKey)) {
                        Recore.getApp()?.applicationContext?.let {
                            val sharedPref = CM.getEncryptedSharedPreferences(
                                it
                            )
                            sharedPref.edit().putString(
                                AppConstant.HEADER_Authorization,
                                loginResponseModel.data!!.data.strBoltAuthKey
                            ).commit()
                        }
                    }


                    CM.setSp(
                        this,
                        CV.PROMO_CONTENT_TYPE,
                        loginResponseModel.data!!.data.strPromotionalAdsType
                    )
                    CM.setSp(
                        this,
                        CV.PROMO_CONTENT_PATH,
                        loginResponseModel.data!!.data.strPromotionalAds
                    )
                    CM.loadBitmaptoStore(
                        loginResponseModel.data!!.data.strLogo,
                        CV.STORE_LOGO_IMAGE_NAME,
                        CV.STORE_LOGO_LOCAL_PATH,
                        mActivity
                    )
                    CM.loadBitmaptoStore(
                        loginResponseModel.data!!.data.strFullScreen,
                        CV.FULL_SCREEN_IMAGE_NAME,
                        CV.FULL_SCREEN_LOCAL_PATH,
                        mActivity
                    )
                    CM.loadBitmaptoStore(
                        loginResponseModel.data!!.data.strSquareImageLogo,
                        CV.SQUARE_LOGO_IMAGE_NAME,
                        CV.SQUARE_LOGO_LOCAL_PATH,
                        mActivity
                    )

                    CM.setSp(
                        this,
                        CV.IS_PAY_IN_OUT_ENABLE,
                        1 == loginResponseModel.data!!.data.iPayInOut
                    )
                    if (loginResponseModel.data!!.data.strPromotionalAdsType.equals("Video")) {
                        DownloadFromUrlTask(
                            loginResponseModel.data!!.data.strPromotionalAds,
                            CV.PROMO_VIDEO_PATH
                        ).execute()
                    } else {
                        afterDataSyncRedirection()
                    }
                }

            })

    }
    private fun is24_7Available(){
        showKcsDialog()
        RestClient_24_7.getService()?.getStatus(CM.getSp(mActivity,CV.POS_ID,0).toString())?.enqueue(object : CustomApiCallback<ResponseModel>() {
            override fun handleResponseData(data: ResponseModel?) {
                if (data?.message=="Unable to function, Please contact administrator.")
                {
                    showToast(mActivity, data?.message, R.drawable.icon, null)
                }else {
                    webCallLogin()
                }


            }
            override fun showErrorMessage(errormessage: String?) {

                Log.e("=====", errormessage!!)
                if (errormessage=="Unable to function, Please contact administrator.") {
                    dismissKcsDialog()
                    showToast(mActivity, errormessage, R.drawable.icon, null)
                }else{
                    webCallLogin()
                }
            }
        })
    }
    private fun getMACAddress(type: String): String {
        try {
            val all = Collections.list(NetworkInterface.getNetworkInterfaces())
            for (nif in all) {
                if (!nif.getName().equals(type, ignoreCase = true)) continue

                val macBytes = nif.getHardwareAddress() ?: return ""

                val res1 = StringBuilder()
                for (b in macBytes) {
                    //res1.append(Integer.toHexString(b & 0xFF) + ":");
                    res1.append(String.format("%02X:", b))
                }

                if (res1.length > 0) {
                    res1.deleteCharAt(res1.length - 1)
                }
                return res1.toString()
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return "02:15:B2:00:00:00"
    }

    /**
     * This function used to redirect to application flow after login and data sync success
     */
    private fun afterDataSyncRedirection() {
        SyncOperation.syncData(mActivity, true) { isSyncFinished, message ->
            CM.setSp(mActivity, CV.ISSYNCOPERATIONFINISHED, true)
            if (isSyncFinished) {
                CM.setSp(this, CV.ISLOGIN, true)
                CM.startActivity(mActivity, PasscodeActivity::class.java)
                CM.finishActivity(mActivity)
            } else {
                CM.showToast(
                    mActivity,
                    message!!,
                    R.drawable.icon,
                    null
                )
            }
        }
    }

    internal inner class DownloadFromUrlTask(
        private val VideonURL: String?,
        private val fileName: String
    ) :
        AsyncTask<Int, Int, String>() {

        override fun doInBackground(vararg params: Int?): String {
            DownloadFromUrl(mActivity, VideonURL, fileName)

            return "Task Completed"
        }


        override fun onPostExecute(result: String) {
            super.onPostExecute(result)
            afterDataSyncRedirection()
        }
    }


    override fun onRequestGranted(requestCode: Int, perms: List<String>) {

    }

    override fun onResume() {
        super.onResume()
        ScreenLockTimer.stopHandler()
    }
}
