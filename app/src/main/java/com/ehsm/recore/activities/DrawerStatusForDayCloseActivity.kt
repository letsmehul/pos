package com.ehsm.recore.activities

import android.os.Bundle
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.ehsm.recore.R
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.databinding.ActivityDrawerStatusForDayCloseBinding
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CM.showToast
import com.ehsm.recore.utils.CV
import com.kcspl.divyangapp.viewmodel.CheckOutDataViewModel
import kotlinx.android.synthetic.main.custom_toast.*
import kotlinx.android.synthetic.main.header_layout.view.*

class DrawerStatusForDayCloseActivity : BaseActivity<ActivityDrawerStatusForDayCloseBinding>() {

    private lateinit var mActivity: DrawerStatusForDayCloseActivity
    private var checkOutDataViewModel: CheckOutDataViewModel? = null
    private var emp_id = 0
    private var currency_symbol = ""
    private lateinit var mDatabase: RecoreDB


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mActivity = this
        mDatabase = RecoreDB.getInstance(this)!!

        checkOutDataViewModel = ViewModelProviders.of(this).get(CheckOutDataViewModel::class.java)

        emp_id = CM.getSp(mActivity, CV.EMP_ID, 0) as Int
        currency_symbol = CM.getSp(this, CV.CURRENCY_CODE, "") as String


        CM.removeStatusBar(mActivity)

        //bindView(R.layout.activity_drawer_status_for_day_close)
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_drawer_status_for_day_close,
            getWindow().getDecorView().getRootView() as ViewGroup?,false)
        setContentView(mBinding.root)

        setEmpView(mBinding.header.tvEmpName)
        setStoreNameView(mBinding.header.tvstorename, mBinding.header.tvposname)

        mBinding.header.ivLogo.setOnClickListener {
            CM.finishActivity(this)
            CM.startActivity(mActivity, LandingPageActivity::class.java)
        }

        mBinding.header.ivNewSale.setOnClickListener {
            CM.navigateToDashboard(this)
        }

        mBinding.header.ivMore.setOnClickListener {
            CM.showPopup(mActivity,mBinding.header.ivMore,{
                syncWithServer()
            })
        }
        mBinding.header.ivNoSale.setOnClickListener {
            CM.openNosaleDialog(this,custom_toast_container)
        }
        if (!CM.isInternetAvailable(mActivity)) {
            //offline code
        } else {
            webCallGetDayCloseData()
        }
    }

    private fun webCallGetDayCloseData() {
        showKcsDialog()

        checkOutDataViewModel!!.getCheckOutData(emp_id)
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(mActivity,loginResponseModel.message!!,R.drawable.icon,custom_toast_container)
                } else {
                    showToast(mActivity,loginResponseModel.data!!.message,R.drawable.icon,custom_toast_container)

                    mBinding.tvStartDay.setText(currency_symbol + " " + loginResponseModel.data!!.data.startDay)
                    mBinding.tvCashInDrawer.setText(currency_symbol + " " + loginResponseModel.data!!.data.dblCashInDrawer)
                    mBinding.tvTotalSalesCash.setText(currency_symbol + " " + loginResponseModel.data!!.data.salesCash)
                    mBinding.tvWithDrawelCash.setText(currency_symbol + " " + loginResponseModel.data!!.data.manualCashWithdrawal)
                    mBinding.tvBalanceInDrawer.setText(currency_symbol + " " + loginResponseModel.data!!.data.balanceCashInDrawer)
                }
            })
    }

    override fun onRequestGranted(requestCode: Int, perms: List<String>) {

    }
}
