package com.ehsm.recore.activities


import android.os.Bundle
import android.view.MenuItem
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.ehsm.recore.R
import com.ehsm.recore.databinding.ActivityDiscountWebViewBinding
import com.ehsm.recore.utils.AppConstant
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CV


class DiscountWebViewActivity : BaseActivity<ActivityDiscountWebViewBinding>()  {

    private lateinit var mActivity: DiscountWebViewActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        CM.removeStatusBar(mActivity)
        //bindView(R.layout.activity_discount_web_view)
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_discount_web_view,
            getWindow().getDecorView().getRootView() as ViewGroup?,false)
        setContentView(mBinding.root)

        mBinding.toolbar.setTitle(getString(R.string.discounts))
        setSupportActionBar(mBinding.toolbar)

        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()?.setDisplayShowHomeEnabled(true)

        mBinding.toolbar.setNavigationOnClickListener {
            finish()
        }
        mBinding.webViewDiscount.getSettings().setJavaScriptEnabled(true)
        mBinding.webViewDiscount.loadUrl(AppConstant.DISCOUNT_WEBVIEW_URL + CM.getSp(this, CV.STORE_ID,38))

    }

    override fun onRequestGranted(requestCode: Int, perms: List<String>) {

    }
}
