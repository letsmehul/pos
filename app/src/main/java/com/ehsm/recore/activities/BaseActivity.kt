package com.ehsm.recore.activities

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.*
import android.provider.Settings
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.RadioButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProviders
import com.ehsm.recore.R
import com.ehsm.recore.Recore
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.fragments.BaseFragment
import com.ehsm.recore.interfaces.OnstatusChangeListener
import com.ehsm.recore.model.CDSSalesModel
import com.ehsm.recore.model.TerminalModel
import com.ehsm.recore.repository.DiscountListRepository
import com.ehsm.recore.utils.*
import com.ehsm.recore.utils.CM.showToast
import com.ehsm.recore.utils.logger.LogUtil
import com.ehsm.recore.viewmodel.DiscountListViewModel
import com.ehsm.recore.viewmodelfactory.viewModelFactory
import com.google.firebase.analytics.FirebaseAnalytics
import hardware.CDSController
import hardware.CashBoxController
import hardware.PrinterController
import hardware.ePOSPrinter
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import kotlinx.android.synthetic.main.custom_toast.*
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import java.io.*
import java.net.URL
import java.net.URLConnection
import java.util.*
import java.util.concurrent.ExecutionException

abstract class BaseActivity<DB : ViewDataBinding> : AppCompatActivity(),
    EasyPermissions.PermissionCallbacks,
    PrinterController.PrinterStatusChangeListener,OnstatusChangeListener {

    lateinit var mBinding: DB
    var kcsDialog: KcsProgressDialog? = null
    private var terminalDialog: TerminalDialog? = null
    private var mKitchenPrinterSelectionDialog: KitchenPrinterSelectionDialog? = null


    val localFragmentManager = supportFragmentManager
    lateinit var firebaseAnalytics: FirebaseAnalytics

    private lateinit var mCashBoxController: CashBoxController
    lateinit var mPrinterController: PrinterController
    lateinit var mCDSController: CDSController

    lateinit var ePOSPrinterController: ePOSPrinter
    var KITCHEN_PRINTER : String? = " "
    private var OVERLAY_PERMISSION_REQUEST_CODE = 10;

    private val START_TIME_IN_MILLIS: Long = 16000
    private val TOTAL_TIME_IN_MILLIS: Long = 60000
    private var TIME_IN_MILLIS: Long = 0L
    private var mCountDownTimer: CountDownTimer? = null
    var mTimerRunning = false
    private var mTimeLeftInMillis = START_TIME_IN_MILLIS
    var handler: Handler = Handler()

    private var discountListViewModel: DiscountListViewModel?=null
    protected fun bindView(layout: Int) {
        mBinding = DataBindingUtil.setContentView(this, layout)
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Obtain the FirebaseAnalytics instance.
        //    firebaseAnalytics = Firebase.analytics
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        mPrinterController = PrinterController.getInstance(applicationContext)
        mPrinterController.setPrinterStatusChangeLister(this)
        mPrinterController.connectPrinter()

        ePOSPrinterController = ePOSPrinter.getInstance(applicationContext,this)
        ePOSPrinterController.getKitchenPrinterList()

        discountListViewModel = ViewModelProviders.of(this,
            viewModelFactory {
                DiscountListViewModel(DiscountListRepository(RecoreDB.getInstance(this)))
            }
        ).get(DiscountListViewModel::class.java)


        val SDK_INT = Build.VERSION.SDK_INT
        if (SDK_INT > 8) {
            val policy: StrictMode.ThreadPolicy = StrictMode.ThreadPolicy.Builder()
                .permitAll().build()
            StrictMode.setThreadPolicy(policy)
            //your codes here
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d("BaseActivity","Onstart");
    }
    override fun onPause() {
        super.onPause()
        Log.d("BaseActivity","onPause");
    }
    override fun onStop() {
        super.onStop()
        Log.d("BaseActivity","OnStop");
    }

    fun addFragment(oldFragment: BaseFragment, newFragment: BaseFragment) {
        val transaction = localFragmentManager.beginTransaction()
        transaction.add(R.id.base_container, newFragment, newFragment::class.simpleName)
            .hide(oldFragment)
            .addToBackStack(null)
            .commit()
    }

    fun replaceFragment(newFragment: Fragment) {
        val transaction = localFragmentManager.beginTransaction()
        transaction.replace(R.id.base_container, newFragment, newFragment::class.simpleName)
            .addToBackStack(null)
            .commit()
    }

    fun replaceFragmentClearStack(newFragment: Fragment) {
        localFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        val transaction = localFragmentManager.beginTransaction()
        transaction.replace(R.id.base_container, newFragment, newFragment::class.simpleName)
            .commit()
    }

    abstract fun onRequestGranted(requestCode: Int, perms: List<String>)

    fun hasStoragePermission(): Boolean {
        return EasyPermissions.hasPermissions(
            this,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
    }

    fun hasCameraPermission(): Boolean {
        return EasyPermissions.hasPermissions(
            this,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
    }

    override fun OnPrinterStatusChangeLister(status: Int) {
        var message: String = when (status) {
            -1 -> {
                getString(R.string.print_status_usbunconnected)
            }
            0 -> {
                getString(R.string.print_status_usbconnected)
            }
            4 -> {
                getString(R.string.print_status_undetect)
            }
            8 -> {
                getString(R.string.print_status_overheat)
            }
            else -> {
                getString(R.string.print_status_unknown)
            }
        }

        LogUtil.e("HARDWARE_DEBUG", "PRINTER STATUS -               " + message)

        showToast(this, message, R.drawable.icon,custom_toast_container);
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            AppSettingsDialog.Builder(this).build().show()
        } else {
            CM.showMessageOK(this, "", resources.getString(R.string.permission_for_useapp), null)
        }
    }

    fun showKcsDialog() {
        if (isFinishing) {
            return
        }
        runOnUiThread(Runnable {
            if (isFinishing) {
                return@Runnable
            }

            //if dialog not dismiss then first dialog=null.so not to show anytimewhen activtiy finish...
            if (kcsDialog != null) {
                kcsDialog = null
            }
            if (kcsDialog == null)
                kcsDialog = KcsProgressDialog(this@BaseActivity, false)

           kcsDialog?.let {
                 if(!it.isShowing)
                     it.show()
             }

        })
    }

    fun dismissKcsDialog() {
        if (isFinishing) {
            return
        }
        if (kcsDialog != null && kcsDialog!!.isShowing) {
            kcsDialog!!.dismiss()
            kcsDialog = null
        }
    }

    fun showTerminalDialog(terminalList: List<TerminalModel>,callBack: (selectedTerminal: TerminalModel) -> Unit) {
        if (isFinishing) {
            return
        }
        runOnUiThread(Runnable {
            if (isFinishing) {
                return@Runnable
            }
            if (terminalDialog != null) {
                terminalDialog = null
            }
            if (terminalDialog == null)
                terminalDialog = TerminalDialog(this@BaseActivity, terminalList)

            if (terminalDialog != null && !terminalDialog!!.isShowing){
                terminalDialog!!.window!!.callback=UserInteractionAwareCallback(
                        terminalDialog!!.window!!.getCallback(),
                        this
                )
                terminalDialog!!.show()
                val buttonSubmit = terminalDialog!!.findViewById<AppCompatButton>(R.id.btnSubmitTerminal)
                buttonSubmit.setOnClickListener {
                    Recore.getApp()?.applicationContext?.let {

                        terminalDialog?.selectedItem?.let {
                            val sharedPref = CM.getEncryptedSharedPreferences(
                                terminalDialog!!.context
                            )
                            sharedPref.edit().putString(AppConstant.HSN, it.hsn).commit()
                            callBack(it)
                        }

                    }

                    dismissTerminalDialog()
                    dismissKcsDialog()
                }
            }
        })
    }
//    abstract fun showKitchenPrinterDialog(mPrinterdata: ArrayList<HashMap<String, String>>?)
    fun showKitchenPrinterDialog(mPrinterdata: ArrayList<HashMap<String, String>>, callBack: (selectedPrinter: String) -> Unit) {
        if (isFinishing) {
            return
        }
        runOnUiThread(Runnable {
            if (isFinishing) {
                return@Runnable
            }
            if (mKitchenPrinterSelectionDialog != null) {
                mKitchenPrinterSelectionDialog = null
            }
            if (mKitchenPrinterSelectionDialog == null)
                mKitchenPrinterSelectionDialog = KitchenPrinterSelectionDialog(this@BaseActivity, mPrinterdata)

            if (mKitchenPrinterSelectionDialog != null && !mKitchenPrinterSelectionDialog!!.isShowing){
                mKitchenPrinterSelectionDialog!!.window!!.callback=UserInteractionAwareCallback(
                        mKitchenPrinterSelectionDialog!!.window!!.getCallback(),
                        this
                )
                mKitchenPrinterSelectionDialog!!.show()
                val buttonSubmit = mKitchenPrinterSelectionDialog!!.findViewById<AppCompatButton>(R.id.btnSubmitTerminal)
                buttonSubmit.setOnClickListener {
                    Recore.getApp()?.applicationContext?.let {
                        if (mKitchenPrinterSelectionDialog!=null && mKitchenPrinterSelectionDialog!!.isShowing) {
                            mKitchenPrinterSelectionDialog!!.mKitchenPrinterIP?.let {
                                val sharedPref =
                                    CM.getEncryptedSharedPreferences(mKitchenPrinterSelectionDialog!!.context)
                                sharedPref.edit().putString(AppConstant.KITCHEN_PRINTER_IP, it)
                                    .commit()
                                callBack(it)
                            }
                        }
                    }

                    dismissKitchenPrinterDialog()
                    dismissKcsDialog()
                }
            }
        })
    }

    fun openRePrintDialog(callBack: (isPrintKitchenReceipt: Boolean, isPrintSaleReceipt: Boolean) -> Unit) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.callback=UserInteractionAwareCallback(
                dialog.window!!.getCallback(),
                this
        )
        dialog.setContentView(R.layout.dialog_reprint_reciept)

        dialog.setCancelable(false)
        dialog.show()

        val btnFinish = dialog.findViewById<Button>(R.id.btnFinish)

        val rbPrintYes = dialog.findViewById<RadioButton>(R.id.rbPrintYes)
        val rbKitchenPrintYes = dialog.findViewById<RadioButton>(R.id.rbKitchenPrintYes)

        val tvConfirmKitchenPrint = dialog.findViewById<TextView>(R.id.tv_confirm_kitchenoremail)
        tvConfirmKitchenPrint.text = "Do you want to print the kitchen receipt?"


        btnFinish.setOnClickListener {

            callBack(rbKitchenPrintYes.isChecked, rbPrintYes.isChecked)
            dialog.dismiss()
        }
    }


    fun dismissTerminalDialog() {
        if (isFinishing) {
            return
        }
        if (terminalDialog != null && terminalDialog!!.isShowing) {
            terminalDialog!!.dismiss()
            terminalDialog = null
        }
    }
    fun dismissKitchenPrinterDialog() {
        if (isFinishing) {
            return
        }
        if (mKitchenPrinterSelectionDialog != null && mKitchenPrinterSelectionDialog!!.isShowing) {
            mKitchenPrinterSelectionDialog!!.dismiss()
            mKitchenPrinterSelectionDialog = null
        }
    }

    fun openCashBox() {
        mCashBoxController = CashBoxController.getInstance(applicationContext)
        if (mCashBoxController != null) {
            mCashBoxController!!.openCashDrawer(object : CashBoxController.CashBoxStatusListener {
                override fun OnCashBoxOpen() {
                    showToast(this@BaseActivity, "Cashbox Opened Successfully", R.drawable.icon,custom_toast_container)
                    LogUtil.e("HARDWARE_DEBUG", "CASHBOX Opened Successfully")
                }
                override fun OnCashBoxError() {
                    showToast(this@BaseActivity, "Cashbox Failed to open", R.drawable.icon,custom_toast_container)
                    LogUtil.e("HARDWARE_DEBUG", "CASHBOX Failed to open")
                }
            })
        }
    }

    fun showRestrictDialog(displayMsg: String) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setContentView(R.layout.dialog_restrict)
        val tvLabel = dialog.findViewById<TextView>(R.id.tvLabel)
        tvLabel.setText(displayMsg)
        dialog.setCancelable(false)
        dialog.show()

        val btnok = dialog.findViewById<Button>(R.id.btnok)
        btnok.setOnClickListener {
            dialog.dismiss()
        }
    }


    fun openDenyDayStartDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setContentView(R.layout.dialog_restrict)
        val tvLabel = dialog.findViewById<TextView>(R.id.tvLabel)
        tvLabel.setText("In order to access this option \n kindly start your shift as a cashier")
        dialog.setCancelable(false)
        dialog.show()

        val btnok = dialog.findViewById<Button>(R.id.btnok)
        btnok.setOnClickListener {
            dialog.dismiss()
        }
    }
    fun openBreakInStartDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setContentView(R.layout.dialog_restrict)
        val tvLabel = dialog.findViewById<TextView>(R.id.tvLabel)
        tvLabel.setText("In order to access this option \n kindly end your break")
        dialog.setCancelable(false)
        dialog.show()

        val btnok = dialog.findViewById<Button>(R.id.btnok)
        btnok.setOnClickListener {
            dialog.dismiss()
        }
    }

    fun openDenyTransactionDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setContentView(R.layout.dialog_deny_transaction)

        dialog.setCancelable(false)
        dialog.show()

        val btnok = dialog.findViewById<Button>(R.id.btnok)
        btnok.setOnClickListener {
            dialog.dismiss()
        }
    }

    fun setEmpView(textView: AppCompatTextView) {
        if (textView != null) {
            val emp_name = CM.getSp(this, CV.EMP_NAME, "") as String
            textView.text = emp_name
        }
    }

    fun setStoreNameView(tvStoreName: TextView, tvPosName: TextView) {
        if (tvStoreName != null ) {
            val StoreName = CM.getSp(applicationContext, CV.STORE_NAME, "") as String
            val POSName = CM.getSp(applicationContext, CV.POS_NAME, "") as String
            tvStoreName.text = StoreName
            if(POSName.contains(CV.POS_NAME_SPLITTER)){
                tvPosName.text = " , "+ CV.POS_NAME_SPLITTER +" "+ POSName.split(CV.POS_NAME_SPLITTER).get(1)
            }else{
                tvPosName.text = POSName
            }
        }
    }

    fun getBitmapFromLocal(imagepath : String, maxSize: Int): Bitmap? {
        var mLogoBitmap: Bitmap? = null
        var mResizedBitmap: Bitmap? = null
        var imgpath = CM.getSp(this, imagepath, "....") as String
        Log.e("DEBUG", "BITMAP Path :$imgpath")
        val imgFile =
            File(imgpath)
        if (imgFile.exists()) {
            mLogoBitmap = BitmapFactory.decodeFile(imgFile.absolutePath)
        }

        if (mLogoBitmap != null && maxSize > 0) {
            return mLogoBitmap?.let { getResizedBitmap(it, maxSize) }
        }else{
            return mLogoBitmap
        }
//        return mResizedBitmap
    }

    fun getResizedBitmap(image: Bitmap, maxSize: Int): Bitmap? {
        var width = image.width
        var height = image.height
        val bitmapRatio = width.toFloat() / height.toFloat()
        if (bitmapRatio > 1) {
            width = maxSize
            height = (width / bitmapRatio).toInt()
        } else {
            height = maxSize
            width = (height * bitmapRatio).toInt()
        }
        return Bitmap.createScaledBitmap(image, width, height, true)
    }

    fun getBitmapFromBase64(signatureBase64: String): Bitmap? {
        Log.e("DEBUG", "signatureBase64 -- "+signatureBase64)
        var mSignatureBitmap: Bitmap? = null
        if (!TextUtils.isEmpty(signatureBase64)) {
            try {
                val decodedString = Base64.decode(signatureBase64, Base64.DEFAULT)
                mSignatureBitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
            } catch (e: ExecutionException) {
                e.printStackTrace()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        }



        Log.e("DEBUG", "signatureBase64 btmp-- "+getResizedBitmap(mSignatureBitmap!!, 200))
        return mSignatureBitmap?.let {
            getResizedBitmap(it, 200)
        }
    }

    open fun DownloadFromUrl(activity: Context, VideonURL: String?, fileName: String)
    {
        try {
            val url = URL(VideonURL)
            val cw = ContextWrapper(this.getApplicationContext())
            val directory: File = cw.getDir("imageDir", Context.MODE_PRIVATE)
            // Create imageDir
            val file = File(directory, fileName)
            CM.setSp(activity, fileName, file.absolutePath.toString())

            val startTime = System.currentTimeMillis()
            Log.d("VideoManager", "downloaded url:"+file.absolutePath.toString())
            val ucon: URLConnection = url.openConnection()

            val fis: InputStream = ucon.getInputStream()
            val bis = BufferedInputStream(fis)

            val buffer = ByteArrayOutputStream()
            val data = ByteArray(50)
            var current = 0
            while (bis.read(data, 0, data.size).also { current = it } != -1) {
                buffer.write(data, 0, current)
            }

            val fos = FileOutputStream(file)
            fos.write(buffer.toByteArray())
            fos.close()
            Log.d("VideoManager", "download ready in"+ (System.currentTimeMillis() - startTime) / 1000+ " sec")
        } catch (e: IOException) {
            Log.d("VideoManager", "Error: $e")
        }
    }

    private fun openTimerDialog() {
        val dialogTimer = Dialog(this@BaseActivity)
        dialogTimer.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogTimer.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialogTimer.setContentView(R.layout.dialog_timer)

        var dismissBtn = dialogTimer.findViewById<AppCompatButton>(R.id.btn_dismiss)

        dialogTimer.setOnCancelListener {
            mCountDownTimer!!.cancel()
            handler.removeCallbacks(runnableCode)
            resetTimer()
            getTimerTime()
            handler.postDelayed(runnableCode, TIME_IN_MILLIS)
        }

        dismissBtn.setOnClickListener {
            dialogTimer.dismiss()
            mCountDownTimer!!.cancel()
            handler.removeCallbacks(runnableCode)
            resetTimer()
            getTimerTime()
            handler.postDelayed(runnableCode, TIME_IN_MILLIS)
        }

        dialogTimer.setCancelable(true)
        dialogTimer.show()

        val tvTimer = dialogTimer.findViewById<AppCompatTextView>(R.id.text_timer)
        updateCountDownText(tvTimer)

        startTimer(tvTimer,dialogTimer)
    }

    private fun startTimer(
        timer: AppCompatTextView,
        dialogTimer: Dialog
    ) {
        mCountDownTimer = object : CountDownTimer(mTimeLeftInMillis, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                mTimeLeftInMillis = millisUntilFinished
                mTimerRunning = true
                updateCountDownText(timer)
                Log.e("BASE","Start")
            }

            override fun onFinish() {
                timer.setText("00:00")
                mTimerRunning = false
                dialogTimer.dismiss()
                AppConstant.EMP_ID = CM.getSp(this@BaseActivity, CV.EMP_ID, 0) as Int
                CM.startActivity(this@BaseActivity, PasscodeActivity::class.java)
                Log.e("BASE","Finish")
            }
        }.start()
    }

    fun stopTimer() {
        if(mTimerRunning){
            mCountDownTimer!!.cancel()
            mTimerRunning = false
            Log.e("BASE","stopTimer")
        }
    }

    private fun resetTimer() {
        mTimeLeftInMillis = START_TIME_IN_MILLIS
        updateCountDownText(null)
        Log.e("BASE","Reset: ")
    }

    fun updateCountDownText(timerTextView: AppCompatTextView?){
        val minutes = (mTimeLeftInMillis / 1000).toInt() / 60
        val seconds = (mTimeLeftInMillis / 1000).toInt() % 60
        val timeLeftFormatted: String =
            java.lang.String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds)
        Log.e("BASE","minutes: "+minutes)
        Log.e("BASE","seconds: "+seconds)

        timerTextView?.text = timeLeftFormatted
    }

    fun registerLockListener(lock : AppCompatImageView){
        lock.setOnClickListener {
            mTimerRunning = false
            stopHandler()
            AppConstant.EMP_ID = CM.getSp(this@BaseActivity, CV.EMP_ID, 0) as Int
            CM.startActivity(this@BaseActivity, PasscodeActivity::class.java)
            Log.e("BASE","OnLock")
        }
    }

    private val runnableCode = Runnable { // Do something here on the main thread
        openTimerDialog()
        Log.e("Handlers", "Called on main thread")
    }

    open fun stopHandler() {
        handler.removeCallbacks(runnableCode)
        stopTimer()
        resetTimer()
    }

    open fun startHandler() {
        handler.postDelayed(runnableCode, TIME_IN_MILLIS)
    }

    private fun getTimerTime(){
        TIME_IN_MILLIS = TOTAL_TIME_IN_MILLIS - START_TIME_IN_MILLIS
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        ScreenLockTimer.stopHandler()
        ScreenLockTimer.startHandler(this)
    }

    //** CDS Display Code Start **//
    fun checkDrawOverlayPermission(screenname: String, displayMsg: String, isReceiptPrint : Boolean,receiptemail: String, mCDSSalesModel : CDSSalesModel, mInvoiceNumber: String, isCancelDialogShow : Boolean) {
        /** check if we already  have permission to draw over other apps  */
        if (!Settings.canDrawOverlays(this@BaseActivity)) {
            /** if not construct intent to request permission  */
            val intent = Intent(
                Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:$packageName")
            )
            /** request permission via start activity for result  */
            startActivityForResult(intent, OVERLAY_PERMISSION_REQUEST_CODE)
        }else{
            showCDSData(screenname, displayMsg, isReceiptPrint, receiptemail, mCDSSalesModel, mInvoiceNumber, isCancelDialogShow)
        }
    }

    fun showCDSData(mScreenName: String, displayMsg: String, isReceiptPrint : Boolean, receiptemail: String, mCDSSalesModel : CDSSalesModel, mInvoiceNumber: String, misCancelDialogShow : Boolean){
        mCDSController = CDSController.getInstance(applicationContext)

        when (mScreenName) {
            AppConstant.CDS_NORMAL_SCREEN -> {
                mCDSController.DisplayImageData(this@BaseActivity)
            }
            AppConstant.CDS_NEWSALE_SCREEN -> {
                mCDSController.DisplayNewSaleData(this@BaseActivity,mScreenName, mCDSSalesModel, displayMsg, misCancelDialogShow, mInvoiceNumber,AppConstant.NEW_SALE,false)
            }
            AppConstant.CDS_PAYMENTPROCESS_SCREEN -> {
                Log.e("DEBUG", "PaymentProcess")
                mCDSController.DisplayNewSaleData(this@BaseActivity,mScreenName, mCDSSalesModel, displayMsg, misCancelDialogShow, mInvoiceNumber,AppConstant.NEW_SALE,true)
                //mCDSController.DisplayPaymentProcessScreen(this@BaseActivity, mInvoiceNumber)
            }
            AppConstant.CDS_PAYMENTDONE_SCREEN -> {
                mCDSController.DisplayPaymentDoneScreen(this@BaseActivity, displayMsg, isReceiptPrint, receiptemail, mInvoiceNumber,mCDSSalesModel.change_due,mCDSSalesModel.total_paid_amount)
            }
            AppConstant.CDS_REFUND_SCREEN -> {
                mCDSController.DisplayNewSaleData(this@BaseActivity, mScreenName, mCDSSalesModel, displayMsg, misCancelDialogShow, mInvoiceNumber,AppConstant.REFUND,false)
            }
            AppConstant.CDS_HOLD_SCREEN -> {
                mCDSController.DisplayNewSaleData(this@BaseActivity, mScreenName, mCDSSalesModel, displayMsg, misCancelDialogShow, mInvoiceNumber,AppConstant.REFUND,false)
            }
            AppConstant.CDS_COMPLETE_SCREEN-> {
                mCDSController.DisplayNewSaleData(this@BaseActivity, mScreenName, mCDSSalesModel, displayMsg, misCancelDialogShow, mInvoiceNumber, AppConstant.REFUND,false)
            }
            AppConstant.CDS_PAUSE_SCREEN-> {
                mCDSController.onPauseCDSScreen(this@BaseActivity)
            }
        }
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        /** check if received result code
         * is equal our requested code for draw permission   */
        if (requestCode == OVERLAY_PERMISSION_REQUEST_CODE) {
            if (Settings.canDrawOverlays(this)) {
                // continue here - permission was granted
                mCDSController = CDSController.getInstance(applicationContext);
                mCDSController.DisplayImageData(this@BaseActivity)
            }
        }
    }
    //** CDS Display Code End **//
    fun callChangeMpin(){
        CM.openChangePasscodeDialog(this,custom_toast_container)
    }

    fun syncWithServer(){
        discountListViewModel?.sychWithServer(CM.getSp(this, CV.EMP_ID, 0) as Int) { isDiscountSuccess, message ->

        }
    }

    fun discardsDiscount(){
        discountListViewModel?.deleteAppliedDiscountOnCart(CM.getSp(this, CV.ORDER_ID, "") as String)
    }

    fun Fragment.hideKeyboard() {
        view?.let { activity?.hideKeyboard(it) }
    }

    fun Activity.hideKeyboard() {
        hideKeyboard(currentFocus ?: View(this))
    }

    public fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun statuschangeListener(message: String) {
        if (!message.isEmpty()) {
            runOnUiThread(Runnable {
                showToast(this@BaseActivity, message, R.drawable.icon, custom_toast_container);
            })
        }
        Log.d("Debug",message)
    }

}