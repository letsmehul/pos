package com.ehsm.recore.activities

import android.os.Bundle
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ehsm.recore.R
import com.ehsm.recore.adapter.CashDrawerReportAdapter
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.databinding.ActivityCashDrawerReportBinding
import com.ehsm.recore.model.CashDrawerReportModel
import com.ehsm.recore.utils.CM
import com.ehsm.recore.utils.CM.showToast
import com.ehsm.recore.utils.CV
import com.kcspl.divyangapp.viewmodel.CashDrawerReportViewModel
import kotlinx.android.synthetic.main.custom_toast.*
import kotlinx.android.synthetic.main.header_layout.view.*

class CashDrawerReportActivity : BaseActivity<ActivityCashDrawerReportBinding>() {

    private lateinit var mActivity: CashDrawerReportActivity
    private var cashDrawerReportViewModel: CashDrawerReportViewModel? = null
    private var cashDrawerListReport = ArrayList<CashDrawerReportModel>()

    private lateinit var mDatabase: RecoreDB
    private var currency_symbol = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        mDatabase = RecoreDB.getInstance(mActivity)!!
        cashDrawerReportViewModel = ViewModelProviders.of(this).get(CashDrawerReportViewModel::class.java)

        currency_symbol = CM.getSp(this, CV.CURRENCY_CODE, "") as String

        CM.removeStatusBar(mActivity)
        //bindView(R.layout.activity_cash_drawer_report)
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_cash_drawer_report,
            getWindow().getDecorView().getRootView() as ViewGroup?,false)
        setContentView(mBinding.root)
        setEmpView(mBinding.header.tvEmpName)
        setStoreNameView(mBinding.header.tvstorename, mBinding.header.tvposname)
        mBinding.tvAmount.setText("Amount (" + currency_symbol + ")")
        mBinding.header.ivLogo.setOnClickListener {
            CM.finishActivity(this)
            CM.startActivity(mActivity, LandingPageActivity::class.java)
        }

        mBinding.header.ivNewSale.setOnClickListener {
            CM.navigateToDashboard(this)
        }

        mBinding.header.ivMore.setOnClickListener {
            CM.showPopup(mActivity,mBinding.header.ivMore,{
                syncWithServer()
            })
        }
        mBinding.header.ivNoSale.setOnClickListener {
            CM.openNosaleDialog(this,custom_toast_container)
        }
        if (!CM.isInternetAvailable(mActivity)) {
            val stateListLiveData = mDatabase.daoCashDrawerReportList().getCashDrawerData()
            stateListLiveData.observe(this, androidx.lifecycle.Observer { statelist ->
                if (statelist != null) {
                    cashDrawerListReport = statelist as ArrayList<CashDrawerReportModel>
                    CM.setViewHeightBasedOnItems(
                        mBinding.listView,
                        CashDrawerReportAdapter(mActivity, cashDrawerListReport),
                        "list"
                    )

                    mBinding.listView.adapter =
                        CashDrawerReportAdapter(mActivity, cashDrawerListReport)

                }
            })
        } else {
            webCallgetCashDrawerReport("")
        }
    }

    private fun webCallgetCashDrawerReport(date: String) {
        showKcsDialog()
        cashDrawerReportViewModel!!.getCashDrawerReportData(mDatabase, date)
            ?.observe(this, Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(mActivity, loginResponseModel.message!!,R.drawable.icon,custom_toast_container)
                } else {
                    cashDrawerListReport = loginResponseModel.data!!.data
                    showToast(mActivity, loginResponseModel.data!!.message,R.drawable.icon,custom_toast_container)

                    CM.setViewHeightBasedOnItems(
                        mBinding.listView,
                        CashDrawerReportAdapter(mActivity, cashDrawerListReport),
                        "list"
                    )

                    mBinding.listView.adapter =
                        CashDrawerReportAdapter(mActivity, cashDrawerListReport)

                }

            })

    }

    override fun onRequestGranted(requestCode: Int, perms: List<String>) {
    }


}
