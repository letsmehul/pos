package com.ehsm.recore.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.ehsm.recore.R
import com.ehsm.recore.adapter.CategoryListAdapter
import com.ehsm.recore.adapter.OrderItemInvoiceListAdapter
import com.ehsm.recore.adapter.OrderItemListAdapter
import com.ehsm.recore.adapter.ReceiptListAdapter
import com.ehsm.recore.databinding.ActivityReceiptBinding
import com.ehsm.recore.model.CategoryModel
import com.ehsm.recore.model.OrdeItemModel
import com.ehsm.recore.model.ReceiptModel
import java.util.ArrayList

class ReceiptActivity : BaseActivity<ActivityReceiptBinding>(),ReceiptListAdapter.ItemClickCategory,OrderItemInvoiceListAdapter.ItemClick {

    private val receiptList = ArrayList<ReceiptModel>()
    private val orderItemList = ArrayList<OrdeItemModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //bindView(R.layout.activity_receipt)
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_receipt,
            getWindow().getDecorView().getRootView() as ViewGroup?,false)
        setContentView(mBinding.root)

        //add receipt
        val receipt1 = ReceiptModel("1", "#101", "100","11 P.M.")
        val receipt2 = ReceiptModel("2", "#102", "200","10 P.M.")

        receiptList.add(receipt1)
        receiptList.add(receipt2)

        mBinding.rvReceipt.adapter = ReceiptListAdapter(this, receiptList,this)

        //add cart items
        val orderItem1 = OrdeItemModel("1", "Tea", "", "@10.00", "","2","20.00")
        val orderItem2 = OrdeItemModel("2", "Coffee", "", "@20.00", "","2","40.00")
        val orderItem3 = OrdeItemModel("3", "Margerita", "", "@100.00", "","2","200.00")

        orderItemList.add(orderItem1)
        orderItemList.add(orderItem2)
        orderItemList.add(orderItem3)

        mBinding.rvItems.adapter = OrderItemInvoiceListAdapter(this, orderItemList, this)
    }

    override fun onRequestGranted(requestCode: Int, perms: List<String>) {

    }

    override fun listItemClickedCategory(position: Int, category_id: String) {

    }

    override fun listItemClicked(position: Int, category_id: String) {

    }
}
