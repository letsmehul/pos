package com.ehsm.recore.activities

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.ehsm.recore.R
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.databinding.ActivityCheckInOutBinding
import com.ehsm.recore.model.ClockOutDataResponseModel
import com.ehsm.recore.model.DataWrapper
import com.ehsm.recore.model.PrinterClockOutModel
import com.ehsm.recore.utils.*
import com.ehsm.recore.utils.CM.showToast
import com.ehsm.recore.utils.NewSaleFragmentCommon.resetShiftDetails
import com.ehsm.recore.utils.NewSaleFragmentCommon.setShiftDetails
import com.kcspl.divyangapp.viewmodel.*
import hardware.PrinterController
import kotlinx.android.synthetic.main.activity_check_in_out.*
import kotlinx.android.synthetic.main.custom_toast.*
import kotlinx.android.synthetic.main.header_layout.view.*
import java.text.SimpleDateFormat
import java.util.*


class CheckInOutActivity : BaseActivity<ActivityCheckInOutBinding>() {

    private var checkInDataViewModel: CheckInDataViewModel? = null
    private var checkInOutDataViewModel: CheckInOutDataViewModel? = null
    private var checkOutDataViewModel: CheckOutDataViewModel? = null
    var amount_check_out = 0.0
    private var isCheckIn = false
    private var last_action = ""
    private var pos_name = ""
    private var date_time = ""
    var emp_id = 0
    var emp_name = ""
    var emp_role = ""
    private var lastActionViewModel: LastActionViewModel? = null
    private var breakActionViewModel: BreakViewModel? = null
    private lateinit var mDatabase: RecoreDB
    private var cash_in_drawer = ""
    private var isCheckIn_status = false
    private var last_action_status = ""
    private var lastStatusViewModel: LastStatusDayStartCloseViewModel? = null
    private lateinit var dialog: Dialog
    private lateinit var mActivity: CheckInOutActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this

        CM.removeStatusBar(this)

        //bindView(R.layout.activity_check_in_out)
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_check_in_out,
            getWindow().getDecorView().getRootView() as ViewGroup?,false)
        setContentView(mBinding.root)
        mDatabase = RecoreDB.getInstance(this)!!
        setEmpView(mBinding.header.tvEmpName)
        setStoreNameView(mBinding.header.tvstorename, mBinding.header.tvposname)
        checkInDataViewModel = ViewModelProviders.of(this).get(CheckInDataViewModel::class.java)
        checkInOutDataViewModel =
            ViewModelProviders.of(this).get(CheckInOutDataViewModel::class.java)
        checkOutDataViewModel = ViewModelProviders.of(this).get(CheckOutDataViewModel::class.java)
        lastActionViewModel = ViewModelProviders.of(this).get(LastActionViewModel::class.java)
        breakActionViewModel = ViewModelProviders.of(this).get(BreakViewModel::class.java)
        lastStatusViewModel =
            ViewModelProviders.of(this).get(LastStatusDayStartCloseViewModel::class.java)

        mPrinterController = PrinterController.getInstance(applicationContext);
        mPrinterController.setPrinterStatusChangeLister(this)
        mPrinterController.connectPrinter()

        emp_id = CM.getSp(this, CV.EMP_ID, 0) as Int
        emp_name = CM.getSp(this, CV.EMP_NAME, "") as String
        emp_role = CM.getSp(this, CV.EMP_ROLE, "") as String

        mBinding.tvWelComeMsg.setText("Welcome " + emp_role + " " + emp_name + ",")

        dialog = Dialog(this)

        mBinding.btnCheckIn.setOnClickListener {
            webCallSendCheckInOutData(
                0.0,
                0.0,
                AppConstant.CLOCK_IN,
                "",
                Dialog(this)
            )
        }

        mBinding.btnCheckout.setOnClickListener {
            if (!mBinding.btnBreakEnd.isEnabled) {
                val shiftStartEmpId = CM.getSp(this, CV.SHIFTSTARTEMPID, 0) as Int
                if (emp_id == shiftStartEmpId) {
                    dialog = Dialog(this)
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                    dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                    dialog.window!!.setCallback(
                            UserInteractionAwareCallback(
                                    dialog.window!!.getCallback(),
                                    mActivity
                            )
                    )
                    dialog.setContentView(R.layout.dialog_restrict)
                    val tvLabel = dialog.findViewById<TextView>(R.id.tvLabel)
                    tvLabel.setText(getString(R.string.end_day_before_checkout))
                    dialog.setCancelable(false)
                    dialog.show()

                    val btnok = dialog.findViewById<Button>(R.id.btnok)
                    btnok.setOnClickListener {
                        dialog.dismiss()
                    }
                } else {
                    webCallSendCheckInOutData(
                        0.0,
                        0.0,
                        AppConstant.CLOCK_OUT,
                        "",
                        Dialog(this)
                    )
                }
            } else {
                showToast(
                    mActivity,
                    getString(R.string.break_end_restrict),
                    R.drawable.icon,
                    custom_toast_container
                )
            }
        }
        mBinding.btnBreakStart.setOnClickListener {
            webCallSendBreakInOutData(AppConstant.BREAK_START, Dialog(this))
        }
        mBinding.btnBreakEnd.setOnClickListener {
            webCallSendBreakInOutData(AppConstant.BREAK_END, Dialog(this))
        }

        mBinding.header.ivLogo.setOnClickListener {
            CM.finishActivity(this)
        }

        mBinding.header.reluser.setOnClickListener {
            callChangeMpin()
        }

        mBinding.header.ivNewSale.setOnClickListener {
            if (last_action.equals(AppConstant.CLOCK_IN)) {
                if (last_action_status.equals(AppConstant.SHIFT_START)) {
                    if (!emp_role.equals(AppConstant.STAFF)) {
                        CM.navigateToDashboard(this)
                    } else {
                        openDenyTransactionDialog()
                    }
                } else {
                    openDenyDayStartDialog()
                }
            } else {
                openDenyTransactionDialog()
            }
        }

        mBinding.header.ivNoSale.setOnClickListener {
            if (last_action.equals(AppConstant.CLOCK_IN)) {
                CM.openNosaleDialog(this, custom_toast_container)
            } else {
                openDenyTransactionDialog()
            }
        }

        ScreenLockTimer.registerLockListener(mBinding.header.ivLock,mActivity)
        mBinding.header.ivMore.setOnClickListener {
            CM.showPopup(mActivity, mBinding.header.ivMore, {
                syncWithServer()
            })
        }
    }

    private fun webCallGetLastAction() {
        showKcsDialog()

        lastActionViewModel!!.getLastAction(emp_id)
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    showToast(
                        mActivity,
                        loginResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )
                    isCheckIn = true
                } else {
                    pos_name = loginResponseModel.data!!.data.posName
                    date_time = loginResponseModel.data!!.data.dateTimeCreated
                    isCheckIn =
                        !loginResponseModel.data!!.data.strTnxtype.equals(AppConstant.CLOCK_IN)

                    if (isCheckIn) {
                        last_action = AppConstant.CLOCK_OUT
                    } else {
                        last_action = AppConstant.CLOCK_IN
                    }
                    mBinding.tvMachineDateTimeValue.setText(
                        last_action + ", from " + pos_name + " on " + CM.convertDateFormate(
                            CV.WS_DATE_FORMATTE_AM_PM,
                            CV.DISPLAY_DATE_FORMATTE_AMPM, date_time
                        )
                    )
                    cash_in_drawer = loginResponseModel.data!!.data.dblCashInDrawer
                }

                if (isCheckIn) {
                    //allow check in
                    llBreak.visibility = View.GONE
                    mBinding.btnCheckIn.isEnabled = true
                    mBinding.btnCheckIn.background = resources.getDrawable(R.drawable.btn_bg)
                    mBinding.btnCheckout.isEnabled = false
                    mBinding.btnCheckout.background =
                        resources.getDrawable(R.drawable.btn_bg_gray)
                } else {
                    llBreak.visibility = View.VISIBLE
                    mBinding.btnCheckIn.isEnabled = false
                    mBinding.btnCheckIn.background =
                        resources.getDrawable(R.drawable.btn_bg_gray)
                    mBinding.btnCheckout.isEnabled = true
                    mBinding.btnCheckout.background = resources.getDrawable(R.drawable.btn_bg)
                    webCallGetBreakActionData()
                }

                webCallGetLastStatus()
            })
    }

    private fun webCallGetLastStatus() {
        showKcsDialog()

        lastStatusViewModel!!.getLastActionDayStartClose()
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                dismissKcsDialog()
                if (loginResponseModel.data == null) {
                    isCheckIn_status = true
                } else {
                    isCheckIn_status =
                        !loginResponseModel.data!!.data.strTnxtype.equals(AppConstant.SHIFT_START)
                }

                last_action_status = if (isCheckIn_status) {
                    resetShiftDetails(this)
                    AppConstant.SHIFT_CLOSE
                } else {
                    setShiftDetails(this, loginResponseModel)
                    AppConstant.SHIFT_START
                }
            })
    }

    private fun webCallGetBreakActionData() {
        //showKcsDialog()

        breakActionViewModel!!.getBreakInBreakOut(emp_id)
            ?.observe(this, androidx.lifecycle.Observer { loginResponseModel ->
                //dismissKcsDialog()
                if (loginResponseModel?.data != null) {
                    llBreak.visibility = View.VISIBLE
                    loginResponseModel.data!!.breakStartData.let {
                        val strArray = loginResponseModel.data!!.breakStartData.split("\n");
                        val strTemp: ArrayList<String> = ArrayList()
                        for (date_time in strArray) {
                            strTemp.add(
                                CM.convertDateFormate(
                                    CV.WS_DATE_FORMATTE_AM_PM,
                                    CV.DISPLAY_DATE_FORMATTE_AMPM, date_time
                                )
                            )
                        }
                        tvBreakInData.text =
                            strTemp.joinToString(separator = "\n")
                    }
                    loginResponseModel.data!!.breakEndData.let {
                        val strArray = loginResponseModel.data!!.breakEndData.split("\n");
                        val strTemp: ArrayList<String> = ArrayList()
                        for (date_time in strArray) {
                            strTemp.add(
                                CM.convertDateFormate(
                                    CV.WS_DATE_FORMATTE_AM_PM,
                                    CV.DISPLAY_DATE_FORMATTE_AMPM, date_time
                                )
                            )
                        }
                        tvBreakOutData.text =
                            strTemp.joinToString(separator = "\n")
                    }
                    if (loginResponseModel.data!!.isBreakStart) {
                        mBinding.btnBreakStart.isEnabled = false
                        mBinding.btnBreakStart.background =
                            resources.getDrawable(R.drawable.btn_bg_gray)
                    } else {
                        mBinding.btnBreakStart.isEnabled = true
                        mBinding.btnBreakStart.background =
                            resources.getDrawable(R.drawable.btn_bg)
                    }
                    if (loginResponseModel.data!!.isBreakEnd) {
                        mBinding.btnBreakEnd.isEnabled = false
                        mBinding.btnBreakEnd.background =
                            resources.getDrawable(R.drawable.btn_bg_gray)
                    } else {
                        mBinding.btnBreakEnd.isEnabled = true
                        mBinding.btnBreakEnd.background =
                            resources.getDrawable(R.drawable.btn_bg)
                    }
                } else {
                    llBreak.visibility = View.GONE
                }
            })
    }

    private fun webCallSendBreakInOutData(
        type: String,
        dialog: Dialog?
    ) {
        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.DISPLAY_DATE_FORMATTE_AMPM)
        showKcsDialog()

        breakActionViewModel!!.breakInOutData(
            sdf.format(date),
            type,
            emp_id
        )
            ?.observe(this, androidx.lifecycle.Observer { mbreakInOutDataResponseModel ->
                dismissKcsDialog()

                if (mbreakInOutDataResponseModel.data != null) {
                    dialog!!.dismiss()

                    val dialog = Dialog(this)
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                    dialog.getWindow()!!
                        .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                    dialog.setContentView(R.layout.dialog_chechin_out)

                    val tvLabel = dialog.findViewById<TextView>(R.id.tvLabel)
                    tvLabel.setText("You have " + type + " Successfully")
                    dialog.setCancelable(false)
                    dialog.show()

                    val btnok = dialog.findViewById<Button>(R.id.btnok)
                    btnok.setOnClickListener {
                        dialog.dismiss()
                        CM.finishActivity(this)
                    }
                } else {
                    mbreakInOutDataResponseModel.message?.let {
                        showToast(
                            mActivity,
                            mbreakInOutDataResponseModel.message!!,
                            R.drawable.icon,
                            custom_toast_container
                        )
                    }
                }
            })
    }

    private fun getOfflineLastActivityData() {
        val itemListLiveData = mDatabase.daoManualSync().getAllItems(emp_id)

        pos_name = itemListLiveData.last_activity.get(0).posName
        date_time = itemListLiveData.last_activity.get(0).dateTimeCreated
        if (itemListLiveData.last_activity.get(0).strTnxtype.equals(AppConstant.CLOCK_IN)) {
            isCheckIn = false
        } else {
            isCheckIn = true
        }

        if (isCheckIn) {
            last_action = AppConstant.CLOCK_OUT
        } else {
            last_action = AppConstant.CLOCK_IN
        }

        if (isCheckIn) {
            //allow check in
            llBreak.visibility = View.GONE
            mBinding.btnCheckIn.isEnabled = true
            mBinding.btnCheckIn.background = resources.getDrawable(R.drawable.btn_bg)
            mBinding.btnCheckout.isEnabled = false
            mBinding.btnCheckout.background = resources.getDrawable(R.drawable.btn_bg_gray)

        } else {
            llBreak.visibility = View.VISIBLE
            mBinding.btnCheckIn.isEnabled = false
            mBinding.btnCheckIn.background = resources.getDrawable(R.drawable.btn_bg_gray)
            mBinding.btnCheckout.isEnabled = true
            mBinding.btnCheckout.background = resources.getDrawable(R.drawable.btn_bg)

        }
        mBinding.tvMachineDateTimeValue.setText(
            last_action + ", from " + pos_name + " on " + CM.convertDateFormate(
                CV.WS_DATE_FORMATTE_AM_PM,
                CV.DISPLAY_DATE_FORMATTE_AMPM, date_time
            )
        )
    }

    private fun webCallSendCheckInOutData(
        cashInDrawer: Double,
        totalCashInDrawer: Double,
        type: String,
        remark: String,
        dialog: Dialog?
    ) {
        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.WS_DATE_FORMATTE)
        showKcsDialog()

        Log.e("DEBUG", "  sdf.format(date)  - " + sdf.format(date))
        Log.e("DEBUG", "cashInDrawer  -  " + cashInDrawer)
        Log.e("DEBUG", "type - " + type)
        Log.e("DEBUG", "emp_id - " + emp_id)
        Log.e("DEBUG", "remark - " + remark)
        Log.e("DEBUG", "totalCashInDrawer - " + totalCashInDrawer)


        checkInOutDataViewModel!!.checkInOutData(
            sdf.format(date),
            cashInDrawer,
            0.0,
            0.0,
            type,
            emp_id,
            remark,
            totalCashInDrawer,
            emp_id,
            0,
            0
        )
            ?.observe(this, androidx.lifecycle.Observer { mClockOutDataResponseModel ->
                dismissKcsDialog()

                Log.e(
                    "DEBUG",
                    "loginResponseModel.data -- " + mClockOutDataResponseModel.data.toString()
                )
                if (mClockOutDataResponseModel.data == null && !mClockOutDataResponseModel.status) {
                    showToast(
                        mActivity,
                        mClockOutDataResponseModel.message!!,
                        R.drawable.icon,
                        custom_toast_container
                    )

                } else {
                    showToast(
                        mActivity,
                        mClockOutDataResponseModel.data!!.message,
                        R.drawable.icon,
                        custom_toast_container
                    )

                    Log.e(
                        "DEBUG",
                        "loginResponseModel.data!!.message -- " + mClockOutDataResponseModel.data!!.message
                    )

                    dialog!!.dismiss()
                    when (type) {
                        AppConstant.CLOCK_IN -> {
                            if (emp_role.equals(AppConstant.CASHIER)) {
                                CM.setSp(this, CV.ISCHECKIN, isCheckIn)
                                CM.setSp(this, CV.CHECKINUSER, emp_id)
                            }
                        }
                        AppConstant.CLOCK_OUT -> {
                            if (emp_role.equals(AppConstant.CASHIER)) {
                                CM.setSp(this, CV.ISCHECKIN, isCheckIn)
                                CM.setSp(this, CV.CHECKINUSER, 0)

                            }

                            PrintClockOutReceipt(mClockOutDataResponseModel)
                        }
                    }

                    Log.e("CHECK_IN_OUT", "isCheckIn: " + isCheckIn)


                    //  CM.finishActivity(this)
                    val dialog = Dialog(this)
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                    dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                    dialog.setContentView(R.layout.dialog_chechin_out)

                    val tvLabel = dialog.findViewById<TextView>(R.id.tvLabel)
                    tvLabel.setText("You have " + type + " Successfully")
                    dialog.setCancelable(false)
                    dialog.show()

                    val btnok = dialog.findViewById<Button>(R.id.btnok)
                    btnok.setOnClickListener {
                        dialog.dismiss()
                        CM.finishActivity(this)
                    }
                }
            })
    }


    override fun onRequestGranted(requestCode: Int, perms: List<String>) {
    }

    override fun onDestroy() {
        super.onDestroy()
        dialog.dismiss()
    }

    override fun onResume() {
        super.onResume()

        when (emp_role) {
            AppConstant.STAFF -> {
                mBinding.header.ivNoSale.visibility = View.GONE
                mBinding.header.ivNewSale.visibility = View.GONE
            }
            AppConstant.CASHIER -> {
                mBinding.header.ivNoSale.visibility = View.GONE
                mBinding.header.ivNewSale.visibility = View.VISIBLE
            }
            AppConstant.STORE_ADMIN -> {
                mBinding.header.ivNoSale.visibility = View.VISIBLE
                mBinding.header.ivNewSale.visibility = View.VISIBLE
            }
        }

        if (!CM.isInternetAvailable(this)) {
            getOfflineLastActivityData()
        } else {
            webCallGetLastAction()
        }
    }

    private fun PrintClockOutReceipt(mClockOutDataResponseModel: DataWrapper<ClockOutDataResponseModel>?) {

        val date = Calendar.getInstance().time
        val sdf = SimpleDateFormat(CV.DISPLAY_DATE_FORMATTE)

        val mPrinterClockOutModel = PrinterClockOutModel()

        mPrinterClockOutModel.setPOSStationNo(mClockOutDataResponseModel!!.data!!.data.posStationName)

        mPrinterClockOutModel.setDayCloseDate(sdf.format(date))

        mPrinterClockOutModel.setEmpName(mClockOutDataResponseModel!!.data!!.data.employeeName)
        mPrinterClockOutModel.setEmpRole(mClockOutDataResponseModel!!.data!!.data.employeeRole)

        mPrinterClockOutModel.setClockInTime(mClockOutDataResponseModel!!.data!!.data.clockInTime)

        mPrinterClockOutModel.setClockOutTime(mClockOutDataResponseModel!!.data!!.data.clockOutTime)
        mPrinterClockOutModel.setTotalHours(mClockOutDataResponseModel!!.data!!.data.totalWorkedHours)

        mPrinterController.PrintClockOutReceipt(mPrinterClockOutModel)
    }
}
