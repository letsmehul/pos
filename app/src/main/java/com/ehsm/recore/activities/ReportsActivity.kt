package com.ehsm.recore.activities

import android.os.Bundle
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.ehsm.recore.R
import com.ehsm.recore.database.RecoreDB
import com.ehsm.recore.databinding.ActivityReportsBinding
import com.ehsm.recore.utils.CM
import kotlinx.android.synthetic.main.custom_toast.*
import kotlinx.android.synthetic.main.header_layout.view.*

class ReportsActivity : BaseActivity<ActivityReportsBinding>() {

    private lateinit var mActivity: ReportsActivity
    private lateinit var mDatabase: RecoreDB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        mDatabase = RecoreDB.getInstance(mActivity)!!

        CM.removeStatusBar(mActivity)
        //bindView(R.layout.activity_reports)
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_reports,
            getWindow().getDecorView().getRootView() as ViewGroup?,false)
        setContentView(mBinding.root)

        setEmpView(mBinding.header.tvEmpName)
        setStoreNameView(mBinding.header.tvstorename, mBinding.header.tvposname)

        mBinding.cardDrawerReport.setOnClickListener {
            CM.startActivity(mActivity, CashDrawerReportActivity::class.java)
        }
        mBinding.cardDrawerManualAdd.setOnClickListener {
            CM.startActivity(mActivity, CashDrawerManualAddReportActivity::class.java)
        }
        mBinding.cardDrawerStatusForDayClose.setOnClickListener {
            CM.startActivity(mActivity, DrawerStatusForDayCloseActivity::class.java)
        }

        mBinding.header.ivLogo.setOnClickListener {
            CM.finishActivity(this)
        }

        mBinding.header.ivNewSale.setOnClickListener {
            CM.navigateToDashboard(this)
        }

        mBinding.header.ivNoSale.setOnClickListener {
            CM.openNosaleDialog(this, custom_toast_container)
        }

        mBinding.header.reluser.setOnClickListener {
            callChangeMpin()
        }

        mBinding.header.ivMore.setOnClickListener {
            CM.showPopup(mActivity, mBinding.header.ivMore, {
                syncWithServer()
            })
        }
    }

    override fun onRequestGranted(requestCode: Int, perms: List<String>) {

    }
}
