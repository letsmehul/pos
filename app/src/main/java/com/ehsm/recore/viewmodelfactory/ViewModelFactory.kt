package com.ehsm.recore.viewmodelfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ehsm.recore.database.dao.ItemListDAO
import com.ehsm.recore.network.BoltApiHelper
import com.ehsm.recore.repository.PaymentRepository
import com.ehsm.recore.viewmodel.PaymentViewModel
import com.ehsm.recore.viewmodel.SearchViewModel

@Suppress("UNCHECKED_CAST")
class ViewModelFactory(private val itemListDao: ItemListDAO) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SearchViewModel::class.java))
            return SearchViewModel(itemListDao) as T
        throw IllegalArgumentException("Unknown View Model class")
    }
}

inline fun <VM : ViewModel> viewModelFactory(crossinline f: () -> VM) =
    object : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(aClass: Class<T>):T = f() as T
 }