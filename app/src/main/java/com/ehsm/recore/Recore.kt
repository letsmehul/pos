package com.ehsm.recore

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import android.util.Log
import com.ehsm.recore.network.BoltRestAPIClient
import com.ehsm.recore.network.RestClient
import com.ehsm.recore.network.RestClient_24_7
import com.ehsm.recore.utils.ScreenLockTimer


class Recore : Application() , Application.ActivityLifecycleCallbacks{

    companion object {
        private var recore: Recore? = null

        fun getApp(): Recore? {
            return recore
        }
    }
    override fun onCreate() {
        super.onCreate()
        recore = this
        RestClient_24_7.init(this)
        RestClient.init(this)
        BoltRestAPIClient.init(this)
        registerActivityLifecycleCallbacks(this)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
    }

    override fun onActivityPaused(activity: Activity) {
        Log.e("RECORE", "onActivityPaused")
        ScreenLockTimer.stopHandler()
    }

    override fun onActivityStarted(activity: Activity) {
    }

    override fun onActivityDestroyed(activity: Activity) {
    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
    }

    override fun onActivityStopped(activity: Activity) {
    }

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
    }

    override fun onActivityResumed(activity: Activity) {
        Log.e("RECORE", "onActivityResumed")
        ScreenLockTimer.startHandler(activity)
    }
}
