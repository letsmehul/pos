package com.ehsm.recore.hardware;

import android.content.Context;
import android.util.Log;

import com.ehsm.recore.utils.logger.LogUtil;
import com.lztek.toolkit.Lztek;


public class BBV2_CashboxController {
	private static  BBV2_CashboxController cashboxController = null;
	private static final String TAG = "CashboxController";

	private static int gpio_number = 245;
	private static Lztek lztek = null;

	public static BBV2_CashboxController getInstance(Context context) {
		Log.i(TAG, "getInstance");
		if(android.os.Build.VERSION.SDK_INT >= 23)
			gpio_number = 237;

		if (null == cashboxController) {
			lztek = Lztek.create(context);
			lztek.gpioEnable(gpio_number);
			lztek.setGpioOutputMode(gpio_number);
			cashboxController =  new BBV2_CashboxController();
		}

		return cashboxController;
	}

	/**
	 * 功能：操作设备
	 * 
	 * @return 0:成功，-1:失败
	 */
	public int CashboxController_Controller(int value) {
		int res = -1;
		if(lztek == null)
			return res;
//		if(android.os.Build.VERSION.SDK_INT >= 23)
//			return -1;
		LogUtil.e("HARDWARE_DEBUG","CashboxController_Controller Value :    "+value);
		if(value==0)
			lztek.setGpioValue(gpio_number,0);
		else
			lztek.setGpioValue(gpio_number,1);
		return 0;
	}

	public int GetCashboxControllerStatus(){

		return lztek.getGpioValue(gpio_number);
	}




}
