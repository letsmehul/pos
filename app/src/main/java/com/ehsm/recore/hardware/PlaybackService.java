package com.ehsm.recore.hardware;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import com.ehsm.recore.utils.logger.LogUtil;

import java.io.IOException;

import static com.ehsm.recore.hardware.VideoPlayService.SERVICE_STOP_ACTION;

public class PlaybackService extends Service implements MediaPlayer.OnPreparedListener {
    private MediaPlayer player;
    private String path;
    private final IBinder musicBind = new VideoBinder();

    MsgReceiver mMsgReceiver;

    @Override
    public void onCreate() {
        Log.d("PlaybackService", "onCreate");
        super.onCreate();
        player = new MediaPlayer();
        initMusicPlayer();
        registMyReceiver();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return musicBind;
    }

    @Override
    public boolean onUnbind(Intent intent) {
//        player.stop();
//        player.release();
        return false;
    }

    public void initMusicPlayer() {
        player.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        player.setOnPreparedListener(this);
    }

    public void playVideo() {
        try {
            player.setDataSource(path);
            player.prepareAsync();
        } catch (IOException e) {
        }
    }

    public void stopVideo() {
        if(player.isPlaying()){
            player.stop();
            player.reset();
        }
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mediaPlayer.start();
        mediaPlayer.setLooping(true);
    }

    public void setUrl(String url) {

        path = url;
        Log.e("DEBUG", "loccal path --"+path);
    }

    public MediaPlayer getMediaPlayer() {
        return player;
    }

    public class VideoBinder extends Binder {
        public PlaybackService getService() {
            return PlaybackService.this;
        }
    }
    public class MsgReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                LogUtil.d("TAG", "receive the MsgReceiver message...");

                unregisterReceiver(mMsgReceiver);

                stopSelf();
            }
        }
    }

    private void registMyReceiver() {
        mMsgReceiver = new MsgReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SERVICE_STOP_ACTION);

        registerReceiver(mMsgReceiver, intentFilter);
    }
}