package com.ehsm.recore.hardware;

import android.app.Presentation;
import android.content.Context;
import android.os.Bundle;
import android.view.Display;
import android.view.SurfaceView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.recyclerview.widget.RecyclerView;

import com.ehsm.recore.R;

/**
 * Created by Administrator on 2016/12/24.
 */

public class MediaPresentation extends Presentation {

    private ImageView mIVFullDisplay, mSquareLogo, mPromoImage, mProcessSquareLogo, mThankyouSquareLogo,
            mReceiptPrintedtick;
    private LinearLayout mParentContainer, mLayoutPaymentProgress, mLayoutPaymentDone,
            llPaymentCancelDialog, ll_totalsaving_parent,layoutDetails,layoutError,ll_payment_progress1;
    private TextView tvOrderId, tvWalkinCustomer, tvCashierName, tvOrderDateTime, tvTotalQty, tvSubtotal, tvTax, tvDiscount,
            tv_total_saving, tvDiscountRemark, tvTotal, tvGrandtotal
            ,tvLabelProcess, tvReceipt, tvEmail, tvDisplayMsg, tvSuccessMsg, tvNewssaleOrderid, tvReceiptPrinted,
            tvErrorMsg, tvTotalcart, tvLabelScreenName,tvChangeDue,tvTotalpaidAmount,tv_change_due_sale;
    private ProgressBar pbPayment;

    private RecyclerView rvNewSale;
    private SurfaceView presentSurface;

    RelativeLayout mLayoutNewSale;




    public MediaPresentation(Context outerContext, Display display, boolean b) {
        super(outerContext, display);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.secondary_screen);


        mParentContainer = findViewById(R.id.ll_parent_container);
        presentSurface = (SurfaceView) findViewById(R.id.present_surface);

        mIVFullDisplay = (ImageView) findViewById(R.id.iv_full_display);

        tvDisplayMsg = findViewById(R.id.tv_display_msg);

        //Views for New Sales Screen
        mLayoutNewSale = findViewById(R.id.ll_cds_newsale);

        rvNewSale = findViewById(R.id.rv_cds_sale);

        mSquareLogo = findViewById(R.id.img_square_logo);
        mPromoImage = findViewById(R.id.img_promo);

        tvOrderId = findViewById(R.id.tv_order_id);
        tvWalkinCustomer = findViewById(R.id.tv_walkin_customer);
        tvCashierName = findViewById(R.id.tv_cashier_name);
        tvOrderDateTime = findViewById(R.id.tv_order_date_time);

        tvTotalQty = findViewById(R.id.tv_totalqty);
        tvSubtotal = findViewById(R.id.tv_subtotal);
        tvTax = findViewById(R.id.tv_tax);
        tvDiscount = findViewById(R.id.tv_discount);
        tv_total_saving = findViewById(R.id.tv_total_saving);
        ll_totalsaving_parent = findViewById(R.id.ll_totalsaving_parent);
        tvDiscountRemark = findViewById(R.id.tv_discount_remark);
        tvTotal = findViewById(R.id.tv_total);
        tvGrandtotal = findViewById(R.id.tv_grandtotal);
        ll_payment_progress1=findViewById(R.id.ll_payment_progress1);
        tv_change_due_sale=findViewById(R.id.tv_change_due_sale);


        //Views for Payment Processing Screen
        mLayoutPaymentProgress = findViewById(R.id.ll_payment_progress);
        tvLabelProcess = findViewById(R.id.tv_label_process);
        pbPayment = findViewById(R.id.pb_payment);
        mProcessSquareLogo = findViewById(R.id.img_process_square_logo);

        mLayoutPaymentDone = findViewById(R.id.ll_payment_done);
        tvReceipt = findViewById(R.id.tv_receipt);
        tvEmail = findViewById(R.id.tv_email);
        tvSuccessMsg = findViewById(R.id.tv_success_msg);
        mThankyouSquareLogo = findViewById(R.id.img_thankyou_square_logo);
        tvChangeDue=findViewById(R.id.tv_change_due);
        tvTotalpaidAmount=findViewById(R.id.tv_total_paid_amount);

        tvNewssaleOrderid = findViewById(R.id.tv_newssale_orderid);

        tvReceiptPrinted = findViewById(R.id.tv_receipt);
        mReceiptPrintedtick = findViewById(R.id.img_receipt_tick);

        llPaymentCancelDialog = findViewById(R.id.ll_pament_cancel_dialog);
        tvErrorMsg = findViewById(R.id.tv_error_msg);
        tvTotalcart = findViewById(R.id.tv_total_cart);

        tvLabelProcess = findViewById(R.id.tv_label_process);
        tvLabelScreenName = findViewById(R.id.tv_label_screen_name);

        layoutDetails = findViewById(R.id.layout_show_details);
        layoutError = findViewById(R.id.layout_error);
    }

    public LinearLayout getLayoutParent(){
        return mParentContainer;
    }

    public SurfaceView getSurface() {
        return presentSurface;
    }

    public ImageView getImageView(){
        return mIVFullDisplay;
    }

    public RecyclerView getRecyclerViewNewSale(){
        return rvNewSale;
    }

    public ImageView getSquareLogoImageView(){
        return mSquareLogo;
    }

    public ImageView getPromotionImageView(){
        return mPromoImage;
    }

    public RelativeLayout getLayoutNewSale(){
        return mLayoutNewSale;
    }

    public TextView getOrderIdTV(){
        return tvOrderId;
    }

    public TextView getWalkinCustomerTV(){
        return tvWalkinCustomer;
    }

    public TextView getCashierNameTV(){
        return tvCashierName;
    }

    public TextView getOrderDateTimeTV(){
        return tvOrderDateTime;
    }
    public TextView getTotalQtyTV(){
        return tvTotalQty;
    }
    public TextView getSubtotalTV(){
        return tvSubtotal;
    }
    public TextView getTaxTV(){
        return tvTax;
    }
    public TextView getDiscountTV(){
        return tvDiscount;
    }

    public TextView getTotalSavings(){
        return tv_total_saving;
    }

    public LinearLayout getLayoutTotalSaving(){
        return ll_totalsaving_parent;
    }


    public TextView getDiscountRemarkTV(){
        return tvDiscountRemark;
    }
    public TextView getTotalTV(){
        return tvTotal;
    }
    public TextView getGrandtotalTV(){
        return tvGrandtotal;
    }

    public LinearLayout getLayoutPaymentProgress(){
        return mLayoutPaymentProgress;
    }

    public ImageView getProcessSquareImageView(){
        return mProcessSquareLogo;
    }

    public TextView getPaymentProgressTV(){
        return tvLabelProcess;
    }

    public ProgressBar getPaymentProgressBar(){
        return pbPayment;
    }

    public LinearLayout getLayoutPaymentDone(){
        return mLayoutPaymentDone;
    }

    public TextView getReceiptTV(){
        return tvReceipt;
    }
    public TextView getEmailTV(){
        return tvEmail;
    }
    public TextView getSuccessMsgTV(){
        return tvSuccessMsg;
    }
    public TextView getDisplayMsgTV(){
        return tvDisplayMsg;
    }
    public TextView getNewSaleOrderIDTV(){
        return tvNewssaleOrderid;
    }
    public TextView getTvChangeDue(){return tvChangeDue;}
    public TextView getTvTotalpaidAmount(){return tvTotalpaidAmount;}
    public TextView getTvChangeDueSale(){return tv_change_due_sale;}
    public LinearLayout getLlPaymentProgress1(){return ll_payment_progress1;}

    public ImageView getThankyouSquareImageView(){
        return mThankyouSquareLogo;
    }

    public TextView getReceiptPrintedTV(){
        return tvReceiptPrinted;
    }
    public ImageView getReceiptPrinttickImage(){
        return mReceiptPrintedtick;
    }

    public TextView getProcessLabelTV(){
        return tvLabelProcess;
    }
    public TextView getLabelScreenNameTV(){
        return tvLabelScreenName;
    }

    public LinearLayout getPaymentCanceDialoglLayout(){
        return llPaymentCancelDialog;
    }

    public TextView getPaymentErrorMsgTV(){
        return tvErrorMsg;
    }

    public TextView geTotalCartTV(){
        return tvTotalcart;
    }

    public LinearLayout getLayoutDetails(){
        return layoutDetails;
    }

    public LinearLayout getLayoutError(){
        return layoutError;
    }
}
