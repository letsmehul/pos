package com.ehsm.recore.hardware;


import com.ehsm.recore.utils.logger.LogUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CDSFileUtils {
    private static final String TAG = "FileUtils";

    public static List<String> getPaths(String path) {
        List<String> paths = new ArrayList<String>();
        File file = new File(path);
        File[] fs = null;
        LogUtil.d(TAG, "file address:"+ file.getAbsolutePath());
        
        if (file.exists() && file != null && file.isDirectory()) {
            fs = file.listFiles();
        }
        
        if (fs != null && fs.length > 0) {
            for (int i = 0; i < fs.length; i++) {
                paths.add(fs[i].getAbsoluteFile().toString());
                LogUtil.d(TAG, "file i " + i + " " + fs[i].getAbsoluteFile().toString());
            }
        }
        return paths;
    }
    
}
