package com.ehsm.recore.hardware;

import android.app.Presentation;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.hardware.display.DisplayManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.ehsm.recore.R;
import com.ehsm.recore.utils.logger.LogUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/12/15 0015.
 */

public class VideoPlayService extends Service {

    private static final String TAG = "MediaService";
    public static final String SERVICE_STOP_ACTION = "com.jiebao.stopvideo.actions";
    public final static String HDMI_PATH = "/sdcard/Movies";

    private List<String> mHdmiVideoPath = new ArrayList<String>();
    private MediaPlayer mBackgroundPlayer;
    MsgReceiver mMsgReceiver;

    private SurfaceView presentSurface;
    private MediaPresentation myPresentation;

    private DisplayManager mDisplayManager;
    private SharedPreferences sharedPreferences;

    private int vWidth, vHeight;
    private Display currDisplay;

    private int nowHdmiPosition = 0;

    public class MsgReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                LogUtil.d(TAG, "receive the MsgReceiver message...");

                myPresentation.dismiss();
                unregisterReceiver(mMsgReceiver);

                stopSelf();
            }
        }
    }

    class MyVideoFinishListener implements MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener {

        @Override
        public void onCompletion(MediaPlayer mp) {
            LogUtil.d(TAG, "MyVideoFinishListener::onCompletion");

            nowHdmiPosition++;
            if (nowHdmiPosition >= mHdmiVideoPath.size()) {
                nowHdmiPosition = 0;
            }

            playVideo(mp, nowHdmiPosition, false);
        }

        @Override
        public void onPrepared(MediaPlayer player) {
            vWidth = player.getVideoWidth();
            vHeight = player.getVideoHeight();
            Log.e(TAG, "vWidth" + vWidth + " vHeight " + vHeight);
            if (vWidth > currDisplay.getWidth() || vHeight > currDisplay.getHeight()) {
                float wRatio = (float) vWidth / (float) currDisplay.getWidth();
                float hRatio = (float) vHeight / (float) currDisplay.getHeight();
                float ratio = Math.max(wRatio, hRatio);
                vWidth = (int) Math.ceil((float) vWidth / ratio);
                vHeight = (int) Math.ceil((float) vHeight / ratio);
                presentSurface.setLayoutParams(new LinearLayout.LayoutParams(vWidth, vHeight));
                player.start();
            }
        }

        @Override
        public boolean onError(MediaPlayer player, int whatError, int extra) {
            Log.e(TAG, "onError called");
            switch (whatError) {
                case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                    LogUtil.d(TAG, "MEDIA_ERROR_SERVER_DIED");
                    break;
                case MediaPlayer.MEDIA_ERROR_UNKNOWN:
                    LogUtil.d(TAG, "MEDIA_ERROR_UNKNOWN");
                    break;
                default:
                    break;
            }
            return false;
        }
    }


    class MySurfaceCallback implements SurfaceHolder.Callback {

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            LogUtil.d(TAG, "surfaceCreated");

            mBackgroundPlayer.setDisplay(holder);
            playVideo(mBackgroundPlayer, nowHdmiPosition, true);
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        initViewSurface();

        sharedPreferences = getSharedPreferences("play_time", 0);
        updateContents();
        registMyReceiver();
    }

    public class MyBinder extends Binder {

        public VideoPlayService getService(){
            return VideoPlayService.this;
        }
    }

    private MyBinder mBinder = new MyBinder();

    public Presentation getPresentation(){
        return myPresentation;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        myPresentation.dismiss();
        // myPresentation = null;

        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // mBackgroundPlayer.release();
    }

    private void initViewSurface() {
        mBackgroundPlayer = new MediaPlayer();
        mBackgroundPlayer.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
        mHdmiVideoPath = CDSFileUtils.getPaths(HDMI_PATH);
        WindowManager mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);

        WindowManager.LayoutParams wmParams = new WindowManager.LayoutParams();
        wmParams.type = WindowManager.LayoutParams.TYPE_PHONE;
        wmParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        wmParams.format = PixelFormat.RGBA_8888;
        wmParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;
        wmParams.gravity = Gravity.LEFT | Gravity.TOP;
        wmParams.x = 0;
        wmParams.y = 0;
        wmParams.width = 0;
        wmParams.height = 0;
        LayoutInflater inflater = LayoutInflater.from(this);
        LinearLayout presentationLayout = (LinearLayout) inflater.inflate(R.layout.secondary_screen, null);
        presentationLayout.setFocusable(false);
        mWindowManager.addView(presentationLayout, wmParams);
    }

    public void updateContents() {
        mDisplayManager = (DisplayManager) getSystemService(Context.DISPLAY_SERVICE);
        Display[] displays = mDisplayManager.getDisplays();
        LogUtil.d(TAG, "updateContents:displays.length=" + displays.length);

        if (displays.length >= 2) {
            currDisplay = displays[1];
            showPresentation(displays[1]);
        }
    }

    private void showPresentation(Display display) {
        myPresentation = new MediaPresentation(this, display, true);
        myPresentation.setOnDismissListener(new DialogInterface.OnDismissListener() {

            @Override
            public void onDismiss(DialogInterface dialog) {
                // 监听消失，保存当前播放位置。
                sharedPreferences.edit().putInt("index", nowHdmiPosition).commit();
                sharedPreferences.edit().putInt("position", mBackgroundPlayer.getCurrentPosition()).commit();

                mBackgroundPlayer.release();
            }
        });
        myPresentation.getWindow().setType(WindowManager.LayoutParams.TYPE_PHONE);
        myPresentation.show();
        presentSurface = myPresentation.getSurface();
        presentSurface.getHolder().addCallback(new MySurfaceCallback());
        presentSurface.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        // presentSurface.setActivated(false);
    }

    private void registMyReceiver() {
        mMsgReceiver = new MsgReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SERVICE_STOP_ACTION);

        registerReceiver(mMsgReceiver, intentFilter);
    }

    /**
     * @param mediaPlayer
     * @param pathIndex
     * @param isFirstPlay 是否首次播放视频文件
     */
    private void playVideo(MediaPlayer mediaPlayer, int pathIndex, boolean isFirstPlay) {
        LogUtil.d(TAG, "pathIndex " + pathIndex + " isFirstPlay " + isFirstPlay);

        if (isFirstPlay) {
            mediaPlayer.setOnCompletionListener(new MyVideoFinishListener());

            try {
                nowHdmiPosition = sharedPreferences.getInt("index", 0);
                mediaPlayer.setDataSource(mHdmiVideoPath.get(sharedPreferences.getInt("index", 0)));
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                mediaPlayer.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                //  拔hdmi会出现异常。
            }
            mediaPlayer.start();
            mediaPlayer.seekTo(sharedPreferences.getInt("position", 0));
        } else {
            mediaPlayer.reset();

            mediaPlayer.setOnCompletionListener(new VideoPlayService.MyVideoFinishListener());

            try {
                mediaPlayer.setDataSource(mHdmiVideoPath.get(pathIndex));
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                mediaPlayer.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                //  拔hdmi会出现异常。
            }

            mediaPlayer.start();
        }
    }
}
