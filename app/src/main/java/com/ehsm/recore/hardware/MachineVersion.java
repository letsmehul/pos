package com.ehsm.recore.hardware;

public class MachineVersion {
	

	public static String getMachineVersion() {
		try {
		    // [ro.build.display.id]: [rk3288-eng 5.1.1 LMY49F eng.jiebao.20170823.214452 test-keys]
			String version = android.os.Build.DISPLAY;
			version = version.substring(0, 7);
			return version;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

}
